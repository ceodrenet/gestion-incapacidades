-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-11-2022 a las 16:18:51
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `test_incapacidades`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gi_entidad_atiende_ips`
--

CREATE TABLE `gi_entidad_atiende_ips` (
  `ei_id` int(11) NOT NULL,
  `ei_ent_id` int(11) NOT NULL,
  `ei_ips_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Indices de la tabla `gi_entidad_atiende_ips`
--
ALTER TABLE `gi_entidad_atiende_ips`
  ADD PRIMARY KEY (`ei_id`);

--
-- AUTO_INCREMENT de la tabla `gi_entidad_atiende_ips`
--
ALTER TABLE `gi_entidad_atiende_ips`
  MODIFY `ei_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- query para agregar nuevo campo a la tabla gi_incapacidad
ALTER TABLE `gi_incapacidad` ADD `inc_pertenece_red` TINYINT(1) NULL AFTER `inc_donde_se_genera`;