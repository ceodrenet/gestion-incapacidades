CREATE VIEW `reporte_consolidado_inc_test` AS SELECT `gi_incapacidad`.`inc_id` AS `inc_id`, 
`gi_empresa`.`emp_nombre` AS `emp_nombre`, 
`gi_empleados`.`emd_cedula` AS `emd_cedula`, 
`gi_empleados`.`emd_nombre` AS `emd_nombre`, 
date_format(`gi_incapacidad`.`inc_fecha_inicio`,'%d/%m/%Y') AS `inc_fecha_inicio`, 
date_format(`gi_incapacidad`.`inc_fecha_final`,'%d/%m/%Y') AS `inc_fecha_final`, 
to_days(`gi_incapacidad`.`inc_fecha_final`) - to_days(`gi_incapacidad`.`inc_fecha_inicio`) + 1 AS `dias`, 
`gc`.`cla_descripcion` AS `inc_clasificacion`, 
`gi_incapacidad`.`inc_diagnostico` AS `inc_diagnostico`, 
`gi_origen`.`ori_descripcion_v` AS `inc_origen`, 
`gi_incapacidad`.`inc_valor_pagado_empresa` AS `inc_valor_pagado_empresa`, 
`gi_incapacidad`.`inc_valor_pagado_eps` AS `inc_valor_pagado_eps`, 
`gi_incapacidad`.`inc_valor_pagado_ajuste` AS `inc_valor_pagado_ajuste`, 
date_format(`gi_incapacidad`.`inc_fecha_pago_nomina`,'%d/%m/%Y') AS `inc_fecha_pago_nomina`, 
date_format(`gi_incapacidad`.`inc_fecha_recepcion_d`,'%d/%m/%Y') AS `inc_fecha_recepcion_d`, 
date_format(`gi_incapacidad`.`inc_fecha_generada`,'%d/%m/%Y') AS `inc_fecha_generada`, 
date_format(`gi_incapacidad`.`inc_fecha_radicacion`,'%d/%m/%Y') AS `inc_fecha_radicacion`, 
`gi_incapacidad`.`inc_numero_radicado_v` AS `inc_numero_radicado_v`, 
date_format(`gi_incapacidad`.`inc_fecha_solicitud`,'%d/%m/%Y') AS `inc_fecha_solicitud`, 
`gi_incapacidad`.`inc_valor` AS `inc_valor`, 
date_format(`gi_incapacidad`.`inc_fecha_pago`,'%d/%m/%Y') AS `inc_fecha_pago`, 
date_format(`gi_incapacidad`.`inc_fecha_negacion_`,'%d/%m/%Y') AS `inc_fecha_negacion_`, 
`sue`.`set_descripcion_v` AS `set_descripcion_v`, 
`gi_estado_tramite`.`est_tra_desc_i` AS `inc_estado_tramite`, 
CASE WHEN `gi_incapacidad`.`inc_estado_tramite` = 1 THEN date_format(`gi_incapacidad`.`inc_fecha_generada`,'%d/%m/%Y') WHEN `gi_incapacidad`.`inc_estado_tramite` = 2 THEN date_format(`gi_incapacidad`.`inc_fecha_generada`,'%d/%m/%Y') WHEN `gi_incapacidad`.`inc_estado_tramite` = 3 THEN date_format(`gi_incapacidad`.`inc_fecha_radicacion`,'%d/%m/%Y') WHEN `gi_incapacidad`.`inc_estado_tramite` = 4 THEN date_format(`gi_incapacidad`.`inc_fecha_solicitud`,'%d/%m/%Y') WHEN `gi_incapacidad`.`inc_estado_tramite` = 5 THEN date_format(`gi_incapacidad`.`inc_fecha_pago`,'%d/%m/%Y') WHEN `gi_incapacidad`.`inc_estado_tramite` = 6 THEN date_format(`gi_incapacidad`.`inc_fecha_negacion_`,'%d/%m/%Y') WHEN `gi_incapacidad`.`inc_estado_tramite` = 7 THEN date_format(`gi_incapacidad`.`inc_fecha_negacion_`,'%d/%m/%Y') END AS `inc_Fecha_estado`, 
`gi_empleados`.`emd_codigo_nomina` AS `emd_codigo_nomina`, 
`gi_empleados`.`emd_cargo` AS `emd_cargo`, 
`gi_empleados`.`emd_salario` AS `emd_salario`, 
`gee`.`est_emp_desc_v` AS `est_emp_desc_v`, 
`ip2`.`ips_nombre` AS `inc_ips_afiliado`, 
`ip4`.`ips_nombre` AS `inc_afp_afiliado`, 
`ip3`.`ips_nombre` AS `inc_arl_afiliado`, 
`gi_medicos`.`med_nombre` AS `med_nombre`,
`gi_medicos`.`med_num_registro` AS `med_num_registro`,
CASE WHEN `gi_medicos`.`med_estado_rethus` = 0 THEN 'NO VALIDADO'
WHEN `gi_medicos`.`med_estado_rethus` = 1 THEN 'REGISTRADO'
WHEN `gi_medicos`.`med_estado_rethus` = 2 THEN 'NO REGISTRADO' END AS `med_estado_rethus`,
`gi_entidad_atiende`.`enat_nombre_v` AS `enat_nombre_v`,
`gi_entidad_atiende`.`enat_nit_v` AS `enat_nit_v`,
CASE WHEN `gi_incapacidad`.`inc_pertenece_red` = 0 THEN 'NO PERTENECE'
WHEN `gi_incapacidad`.`inc_pertenece_red` = 1 THEN 'PERTENECE' END AS `inc_pertenece_red`,
`gd`.`doc_descripcion_v` AS `inc_tipo_generacion`, 
`gi_atencion`.`atn_descripcion` AS `atn_descripcion`, 
`a`.`user_nombre` AS `creador`, 
`b`.`user_nombre` AS `editor`, 
CASE WHEN `gi_incapacidad`.`inc_ruta_incapacidad` is not null THEN 'SI' WHEN `gi_incapacidad`.`inc_ruta_incapacidad` is null THEN 'NO' WHEN `gi_incapacidad`.`inc_ruta_incapacidad` = ' ' THEN 'NO' END AS `inc_ruta_incapacidad`, 
CASE WHEN `gi_incapacidad`.`inc_ruta_incapacidad_transcrita` is not null THEN 'SI' WHEN `gi_incapacidad`.`inc_ruta_incapacidad_transcrita` is null THEN 'NO' WHEN `gi_incapacidad`.`inc_ruta_incapacidad_transcrita` = ' ' THEN 'NO' END AS `inc_ruta_incapacidad_transcrita`, 
`gi_incapacidad`.`inc_registrada_entidad_i` AS `inc_registrada_entidad_i`, 
CASE WHEN `gi_incapacidad`.`inc_estado` = '1' THEN 'ACEPTADA' WHEN `gi_incapacidad`.`inc_estado` = '0' THEN 'INCOMPLETA' END AS `inc_estado`, 
`gi_motivos_rechazo`.`mot_desc_v` AS `mot_desc_v`, 
`gi_empleados`.`emd_sede` AS `emd_sede`, 
`gi_empleados`.`emd_centro_costos` AS `emd_centro_costos`, 
`gi_empleados`.`emd_subcentro_costos` AS `emd_subcentro_costos`, 
`gi_empleados`.`emd_ciudad` AS `emd_ciudad`, 
`gi_motivos_rechazo`.`mot_observacion_v` AS `mot_observacion_v`, 
`gi_incapacidad`.`inc_empresa` AS `inc_empresa` 
FROM (((((((((((((((((`gi_incapacidad` join `gi_empresa` on(`gi_empresa`.`emp_id` = `gi_incapacidad`.`inc_empresa`)) 
join `gi_empleados` on(`gi_empleados`.`emd_id` = `gi_incapacidad`.`inc_emd_id`))
join `gi_ips` `ip2` on(`ip2`.`ips_id` = `gi_incapacidad`.`inc_ips_afiliado`))
left join `gi_ips` `ip3` on(`ip3`.`ips_id` = `gi_incapacidad`.`inc_arl_afiliado`)) 
left join `gi_ips` `ip4` on(`ip4`.`ips_id` = `gi_incapacidad`.`inc_afp_afiliado`))
left join `gi_medicos`on(`gi_medicos`.`med_id` = `gi_incapacidad`.`inc_profesional_responsable`))
left join `gi_entidad_atiende`on(`gi_entidad_atiende`.`enat_id_i` = `gi_incapacidad`.`inc_donde_se_genera`))
left join `gi_atencion` on(`gi_atencion`.`atn_id` = `gi_incapacidad`.`inc_atn_id`)) 
left join `gi_usuario` `a` on(`a`.`user_id` = `gi_incapacidad`.`inc_user_id_i`)) 
left join `gi_usuario` `b` on(`b`.`user_id` = `gi_incapacidad`.`inc_edi_user_id_i`)) 
left join `gi_motivos_rechazo` on(`gi_motivos_rechazo`.`mot_id_i` = `gi_incapacidad`.`inc_mot_rech_i`)) 
left join `gi_origen` on(`gi_origen`.`ori_id_i` = `gi_incapacidad`.`inc_origen`)) 
left join `gi_estado_tramite` on(`gi_estado_tramite`.`est_tra_id_i` = `gi_incapacidad`.`inc_estado_tramite`)) 
left join `gi_clasificacion` `gc` on(`gc`.`cla_id` = `gi_incapacidad`.`inc_clasificacion`)) 
left join `gi_documentos` `gd` on(`gd`.`doc_id_i` = `gi_incapacidad`.`inc_tipo_generacion`)) 
left join `gi_estado_empleados` `gee` on(`gee`.`est_emp_id_i` = `gi_empleados`.`emd_estado`)) 
left join `gi_sub_estado_tramite` `sue` on(`sue`.`set_id_i` = `gi_incapacidad`.`inc_sub_estado_i`));