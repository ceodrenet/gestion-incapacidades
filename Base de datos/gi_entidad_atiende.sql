-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-10-2022 a las 16:51:31
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `incapacidades`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gi_entidad_atiende`
--

CREATE TABLE `gi_entidad_atiende` (
  `enat_id_i` int(11) NOT NULL,
  `enat_nombre_v` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `enat_nit_v` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `enat_fecha_creacion_d` date NOT NULL DEFAULT current_timestamp(),
  `enat_estado` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `gi_entidad_atiende`
--

INSERT INTO `gi_entidad_atiende` (`enat_id_i`, `enat_nombre_v`, `enat_nit_v`, `enat_fecha_creacion_d`, `enat_estado`) VALUES
(6, 'CLINICA DE LA COSTA', '89987231-2', '2022-10-12', 0),
(7, 'CLINICA DEL CARIBE', '89984671-2', '2022-10-12', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `gi_entidad_atiende`
--
ALTER TABLE `gi_entidad_atiende`
  ADD PRIMARY KEY (`enat_id_i`),
  ADD UNIQUE KEY `enat_nit_v` (`enat_nit_v`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `gi_entidad_atiende`
--
ALTER TABLE `gi_entidad_atiende`
  MODIFY `enat_id_i` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
