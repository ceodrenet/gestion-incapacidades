select 
`incapacidades`.`gi_incapacidad`.`inc_id` AS `inc_id`,
`incapacidades`.`gi_empresa`.`emp_nombre` AS `emp_nombre`,
`incapacidades`.`gi_empleados`.`emd_cedula` AS `emd_cedula`,
`incapacidades`.`gi_empleados`.`emd_nombre` AS `emd_nombre`,
date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_inicio`,'%Y/%m/%d') AS `inc_fecha_inicio`,
date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_final`,'%Y/%m/%d') AS `inc_fecha_final`,
((to_days(`incapacidades`.`gi_incapacidad`.`inc_fecha_final`) - to_days(`incapacidades`.`gi_incapacidad`.`inc_fecha_inicio`)) + 1) AS `dias`,
`gc`.`cla_descripcion` AS `inc_clasificacion`,
`incapacidades`.`gi_incapacidad`.`inc_diagnostico` AS `inc_diagnostico`,
`incapacidades`.`gi_origen`.`ori_descripcion_v` AS `inc_origen`,
`incapacidades`.`gi_incapacidad`.`inc_valor_pagado_empresa` AS `inc_valor_pagado_empresa`,
`incapacidades`.`gi_incapacidad`.`inc_valor_pagado_eps` AS `inc_valor_pagado_eps`,
`incapacidades`.`gi_incapacidad`.`inc_valor_pagado_ajuste` AS `inc_valor_pagado_ajuste`,
date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_pago_nomina`,'%Y/%m/%d') AS `inc_fecha_pago_nomina`,
date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_recepcion_d`,'%Y/%m/%d') AS `inc_fecha_recepcion_d`,
date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_generada`,'%Y/%m/%d') AS `inc_fecha_generada`,
date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_radicacion`,'%Y/%m/%d') AS `inc_fecha_radicacion`,
`incapacidades`.`gi_incapacidad`.`inc_numero_radicado_v` AS `inc_numero_radicado_v`,
date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_solicitud`,'%Y/%m/%d') AS `inc_fecha_solicitud`,
`incapacidades`.`gi_incapacidad`.`inc_valor` AS `inc_valor`,
date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_pago`,'%Y/%m/%d') AS `inc_fecha_pago`,
date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_negacion_`,'%Y/%m/%d') AS `inc_fecha_negacion_`,
`sue`.`set_descripcion_v` AS `set_descripcion_v`,
`incapacidades`.`gi_estado_tramite`.`est_tra_desc_i` AS `inc_estado_tramite`,
(case when (`incapacidades`.`gi_incapacidad`.`inc_estado_tramite` = 1) then date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_generada`,'%Y/%m/%d') 
when (`incapacidades`.`gi_incapacidad`.`inc_estado_tramite` = 2) then date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_generada`,'%Y/%m/%d') 
when (`incapacidades`.`gi_incapacidad`.`inc_estado_tramite` = 3) then date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_radicacion`,'%Y/%m/%d') 
when (`incapacidades`.`gi_incapacidad`.`inc_estado_tramite` = 4) then date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_solicitud`,'%Y/%m/%d') 
when (`incapacidades`.`gi_incapacidad`.`inc_estado_tramite` = 5) then date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_pago`,'%Y/%m/%d') 
when (`incapacidades`.`gi_incapacidad`.`inc_estado_tramite` = 6) then date_format(`incapacidades`.`gi_incapacidad`.`inc_fecha_negacion_`,'%Y/%m/%d') 
end) AS `inc_Fecha_estado`,
`incapacidades`.`gi_empleados`.`emd_codigo_nomina` AS `emd_codigo_nomina`,
`incapacidades`.`gi_empleados`.`emd_cargo` AS `emd_cargo`,
`incapacidades`.`gi_empleados`.`emd_salario` AS `emd_salario`,
`gee`.`est_emp_desc_v` AS `est_emp_desc_v`,
`ip2`.`ips_nombre` AS `inc_ips_afiliado`,
`ip4`.`ips_nombre` AS `inc_afp_afiliado`,
`ip3`.`ips_nombre` AS `inc_arl_afiliado`,
`incapacidades`.`gi_incapacidad`.`inc_profesional_responsable` AS `inc_profesional_responsable`,
`incapacidades`.`gi_incapacidad`.`inc_prof_registro_medico` AS `inc_prof_registro_medico`,
`incapacidades`.`gi_incapacidad`.`inc_donde_se_genera` AS `inc_donde_se_genera`,
`gd`.`doc_descripcion_v` AS `inc_tipo_generacion`,
`incapacidades`.`gi_atencion`.`atn_descripcion` AS `atn_descripcion`,
`a`.`user_nombre` AS `creador`,
`b`.`user_nombre` AS `editor`,
(case when (`incapacidades`.`gi_incapacidad`.`inc_ruta_incapacidad` IS NOT NULL) then 'SI' 
when (`incapacidades`.`gi_incapacidad`.`inc_ruta_incapacidad`  IS NULL) then 'NO' 
when (`incapacidades`.`gi_incapacidad`.`inc_ruta_incapacidad` = " ") then 'NO' END) AS `inc_ruta_incapacidad`,
(case when (`incapacidades`.`gi_incapacidad`.`inc_ruta_incapacidad_transcrita`  IS NOT NULL) then 'SI' 
when (`incapacidades`.`gi_incapacidad`.`inc_ruta_incapacidad_transcrita`  IS NULL) then 'NO' 
when (`incapacidades`.`gi_incapacidad`.`inc_ruta_incapacidad_transcrita` = " ") then 'NO' END) AS `inc_ruta_incapacidad_transcrita`,
`incapacidades`.`gi_incapacidad`.`inc_registrada_entidad_i` AS `inc_registrada_entidad_i`,
`incapacidades`.`gi_incapacidad`.`inc_estado` AS `inc_estado`,
`incapacidades`.`gi_motivos_rechazo`.`mot_desc_v` AS `mot_desc_v`,
`incapacidades`.`gi_empleados`.`emd_sede` AS `emd_sede`,
`incapacidades`.`gi_empleados`.`emd_centro_costos` AS `emd_centro_costos`,
`incapacidades`.`gi_empleados`.`emd_subcentro_costos` AS `emd_subcentro_costos`,
`incapacidades`.`gi_empleados`.`emd_ciudad` AS `emd_ciudad`,
`incapacidades`.`gi_motivos_rechazo`.`mot_observacion_v` AS `mot_observacion_v`
from (((((((((((((((`incapacidades`.`gi_incapacidad` join `incapacidades`.`gi_empresa` on((`incapacidades`.`gi_empresa`.`emp_id` = `incapacidades`.`gi_incapacidad`.`inc_empresa`))) join `incapacidades`.`gi_empleados` on((`incapacidades`.`gi_empleados`.`emd_id` = `incapacidades`.`gi_incapacidad`.`inc_emd_id`))) join `incapacidades`.`gi_ips` `ip2` on((`ip2`.`ips_id` = `incapacidades`.`gi_empleados`.`emd_eps_id`))) left join `incapacidades`.`gi_ips` `ip3` on((`ip3`.`ips_id` = `incapacidades`.`gi_empleados`.`emd_arl_id`))) left join `incapacidades`.`gi_ips` `ip4` on((`ip4`.`ips_id` = `incapacidades`.`gi_empleados`.`emd_afp_id`))) left join `incapacidades`.`gi_atencion` on((`incapacidades`.`gi_atencion`.`atn_id` = `incapacidades`.`gi_incapacidad`.`inc_atn_id`))) left join `incapacidades`.`gi_usuario` `a` on((`a`.`user_id` = `incapacidades`.`gi_incapacidad`.`inc_user_id_i`))) left join `incapacidades`.`gi_usuario` `b` on((`b`.`user_id` = `incapacidades`.`gi_incapacidad`.`inc_edi_user_id_i`))) left join `incapacidades`.`gi_motivos_rechazo` on((`incapacidades`.`gi_motivos_rechazo`.`mot_id_i` = `incapacidades`.`gi_incapacidad`.`inc_mot_rech_i`))) left join `incapacidades`.`gi_origen` on((`incapacidades`.`gi_origen`.`ori_id_i` = `incapacidades`.`gi_incapacidad`.`inc_origen`))) left join `incapacidades`.`gi_estado_tramite` on((`incapacidades`.`gi_estado_tramite`.`est_tra_id_i` = `incapacidades`.`gi_incapacidad`.`inc_estado_tramite`))) left join `incapacidades`.`gi_clasificacion` `gc` on((`gc`.`cla_id` = `incapacidades`.`gi_incapacidad`.`inc_clasificacion`))) left join `incapacidades`.`gi_documentos` `gd` on((`gd`.`doc_id_i` = `incapacidades`.`gi_incapacidad`.`inc_tipo_generacion`))) left join `incapacidades`.`gi_estado_empleados` `gee` on((`gee`.`est_emp_id_i` = `incapacidades`.`gi_empleados`.`emd_estado`))) left join `incapacidades`.`gi_sub_estado_tramite` `sue` on((`sue`.`set_id_i` = `incapacidades`.`gi_incapacidad`.`inc_sub_estado_i`)))