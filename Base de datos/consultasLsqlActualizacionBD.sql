update gi_empleados SET emd_estado = 2 WHERE emd_estado = 0;

UPDATE gi_incapacidad SET inc_origen = 1 WHERE inc_origen = 'ENFERMEDAD GENERAL';
UPDATE gi_incapacidad SET inc_origen = 5 WHERE inc_origen = 'ACCIDENTE TRANSITO';
UPDATE gi_incapacidad SET inc_origen = 2 WHERE inc_origen = 'LICENCIA DE MATERNIDAD';
UPDATE gi_incapacidad SET inc_origen = 3 WHERE inc_origen = 'LICENCIA DE PATERNIDAD';
UPDATE gi_incapacidad SET inc_origen = 4 WHERE inc_origen = 'ACCIDENTE LABORAL';

UPDATE gi_incapacidad SET inc_estado_tramite = 1 WHERE inc_estado_tramite = 'EMPRESA';
UPDATE gi_incapacidad SET inc_estado_tramite = 2 WHERE inc_estado_tramite = 'POR RADICAR';
UPDATE gi_incapacidad SET inc_estado_tramite = 3 WHERE inc_estado_tramite = 'RADICADA';
UPDATE gi_incapacidad SET inc_estado_tramite = 4 WHERE inc_estado_tramite = 'SOLICITUD DE PAGO';
UPDATE gi_incapacidad SET inc_estado_tramite = 5 WHERE inc_estado_tramite = 'PAGADA';
UPDATE gi_incapacidad SET inc_estado_tramite = 6 WHERE inc_estado_tramite = 'SIN RECONOCIMIENTO';
UPDATE gi_incapacidad SET inc_estado_tramite = 6 WHERE inc_estado_tramite = 'NEGADA';
UPDATE gi_incapacidad SET inc_estado_tramite = 5 WHERE inc_estado_tramite = 'PAGADO';

UPDATE gi_incapacidad SET inc_fecha_recepcion_d = inc_fecha_generada;

