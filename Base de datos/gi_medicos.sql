-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-10-2022 a las 17:10:34
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `test_incapacidades`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gi_medicos`
--

CREATE TABLE `gi_medicos` (
  `med_id` int(11) NOT NULL,
  `med_nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `med_identificacion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `med_num_registro` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `med_fecha_creacion` datetime NOT NULL DEFAULT current_timestamp(),
  `med_estado_rethus` int(3) NOT NULL DEFAULT 0,
  `med_estado` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `gi_medicos`
--

INSERT INTO `gi_medicos` (`med_id`, `med_nombre`, `med_identificacion`, `med_num_registro`, `med_fecha_creacion`, `med_estado_rethus`, `med_estado`) VALUES
(13, 'demo1', '123456789', '321654', '2022-10-11 15:28:34', 1, 1),
(14, 'demo2', '852147963', '852741', '2022-10-11 15:29:15', 0, 1),
(16, 'demo3', '854785478', '8549654', '2022-10-12 10:27:47', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `gi_medicos`
--
ALTER TABLE `gi_medicos`
  ADD PRIMARY KEY (`med_id`),
  ADD UNIQUE KEY `med_identificacion` (`med_identificacion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `gi_medicos`
--
ALTER TABLE `gi_medicos`
  MODIFY `med_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
