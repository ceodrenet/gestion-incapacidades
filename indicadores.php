<?php
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    date_default_timezone_set('America/Bogota');

     /* extender */
    require_once 'controladores/mail.controlador.php';
    require_once 'controladores/plantilla.controlador.php'; 
    require_once 'modelos/dao.modelo.php';

    /*Borro la tabla*/
    ModeloDAO::mdlBorrar('gi_indicadores_semanales', null);
    /*Obtnemos las Empresas*/
    $getEmpresa = ControladorPlantilla::getData('gi_empresa', null, null);
    foreach($getEmpresa as $key => $value){
        /*Ya tebemos las empresas ahora hay que tener los valores por estado tramite*/
        $getCampo = "SUM(inc_valor_pagado_eps) as total ";
        $getTabla = "gi_incapacidad";
        $getCondi = "inc_empresa = ".$value['emp_id']. " AND inc_fecha_pago_nomina BETWEEN '".date('Y')."-01-01' AND '".date('Y')."-12-31' ";

        /*Por estadoTramite*/
        $whereSumEmpresa = $getCondi ." AND inc_estado_tramite = 1";
        $whereSumRadicar = $getCondi ." AND inc_estado_tramite = 2";
        $whereSumRadicas = $getCondi ." AND inc_estado_tramite = 3";
        $whereSumSolicit = $getCondi ." AND inc_estado_tramite = 4";
        $whereSumNegadas = $getCondi ." AND inc_estado_tramite = 6";
        $whereSumRechaza = $getCondi ." AND inc_estado_tramite = 7";
        $whereSumPagoDir = $getCondi ." AND inc_estado_tramite = 8";

        $resEmpresa = ModeloDAO::mdlMostrarUnitario($getCampo, $getTabla, $whereSumEmpresa);
        $resRadicar = ModeloDAO::mdlMostrarUnitario($getCampo, $getTabla, $whereSumRadicar);
        $resRadicas = ModeloDAO::mdlMostrarUnitario($getCampo, $getTabla, $whereSumRadicas);
        $resSolicit = ModeloDAO::mdlMostrarUnitario($getCampo, $getTabla, $whereSumSolicit);
        $resNegadas = ModeloDAO::mdlMostrarUnitario($getCampo, $getTabla, $whereSumNegadas);
        $resRechaza = ModeloDAO::mdlMostrarUnitario($getCampo, $getTabla, $whereSumRechaza);
        $resPagoDir = ModeloDAO::mdlMostrarUnitario($getCampo, $getTabla, $whereSumPagoDir);
        $resTotalIn = ModeloDAO::mdlMostrarUnitario($getCampo, $getTabla, $getCondi);

        $getCampoPagado = "SUM(inc_valor) as total ";
        $getTablaPagado = "gi_incapacidad";
        $getCondiPagado = "inc_empresa = ".$value['emp_id']." AND inc_fecha_pago_nomina BETWEEN '".date('Y')."-01-01' AND '".date('Y')."-08-31' AND inc_estado_tramite = 5";
        $resPagadas = ModeloDAO::mdlMostrarUnitario($getCampoPagado, $getTablaPagado, $getCondiPagado);
        
        $date = date('Y-m-d');

        $insertEmpresa = 0;
        $insertPagadas = 0;
        $insertRadicar = 0;
        $insertRadicas = 0;
        $insertSolicit = 0;
        $insertNegadas = 0;
        $insertRechaza = 0;
        $insertPagoDir = 0;
        $insertTotalIn = 0;
        if($resEmpresa != false && $resEmpresa != null && $resEmpresa['total'] != '') {
            $insertEmpresa = $resEmpresa['total'];
        }
        if($resPagadas != false && $resPagadas != null && $resPagadas['total'] != ''){
            $insertPagadas = $resPagadas['total'];
        }
        if($resRadicar != false && $resRadicar != null && $resRadicar['total'] != ''){
            $insertRadicar = $resRadicar['total'];
        }
        if($resRadicas != false && $resRadicas != null && $resRadicas['total'] != ''){
            $insertRadicas = $resRadicas['total'];
        }
        if($resSolicit != false && $resSolicit != null && $resSolicit['total'] != ''){
            $insertSolicit = $resSolicit['total'];
        }
        if($resNegadas != false && $resNegadas != null && $resNegadas['total'] != ''){
            $insertNegadas = $resNegadas['total'];
        }
        if($resRechaza != false && $resRechaza != null && $resRechaza['total'] != ''){
            $insertRechaza = $resRechaza['total'];
            print_r($resRechaza);
        }
        if($resPagoDir != false && $resPagoDir != null && $resPagoDir['total'] != ''){
            $insertPagoDir = $resPagoDir['total'];
        }
        if($resTotalIn != false && $resTotalIn != null && $resTotalIn['total'] != ''){
            $insertTotalIn = $resTotalIn['total'];
        }

        $camposInsertar = "ind_emp_id_i, ind_pag_i, ind_prad_i, ind_rad_i, ind_emp_i, ind_neg_i, ind_rech_i, ind_fecha_semana_d, ind_solp_i, ind_total_inc_i";

        $valoresInsetar = $value['emp_id'].", ".$insertPagadas.",".$insertRadicar.",".$insertRadicas.",".$insertEmpresa.",".$insertNegadas.",".$insertRechaza.",'".$date."', ".$insertSolicit.",".$insertTotalIn;
        
        $resp = ModeloDAO::mdlCrear('gi_indicadores_semanales', $camposInsertar, $valoresInsetar);
        
        //print_r($resp);
    }