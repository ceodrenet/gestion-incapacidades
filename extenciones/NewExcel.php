<?php 

    require '../vendor/autoload.php';
    
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


    class Excel extends PHPExcel{
        public function __construct()
        {
            parent::__construct();
        }
    }

/* End of file */
/* Location: ./application/libraries/ */
?>