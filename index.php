<?php
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    date_default_timezone_set('America/Bogota');

    /* extender */
    require_once 'controladores/mail.controlador.php';
	require_once 'controladores/plantilla.controlador.php'; 
	//require_once 'controladores/Auth.Controller.php'; 

	require_once 'controladores/archivos.controlador.php';
	require_once 'controladores/calendario.controlador.php';
	require_once 'controladores/clientes.controlador.php';
	require_once 'controladores/diagnostico.controlador.php';
	require_once 'controladores/empleados.controlador.php';
	require_once 'controladores/entidades.controlador.php';
	require_once 'controladores/incapacidades.controlador.php';
	require_once 'controladores/ips.controlador.php';
	require_once 'controladores/perfiles.controlador.php';
	require_once 'controladores/seguridad.controlador.php';
	require_once 'controladores/tesoreria.controlador.php';
	require_once 'controladores/usuarios.controlador.php';
	require_once 'controladores/cartera.controlador.php';
	require_once 'controladores/usuariosEps.controlador.php';
	require_once 'controladores/nomina.controlador.php';
	require_once 'controladores/actividades.controlador.php';
	require_once 'controladores/medicos.controlador.php';
	require_once 'controladores/entidadesRegistradas.controlador.php';

	require_once 'modelos/dao.modelo.php';
	require_once 'modelos/archivos.modelo.php';
	require_once 'modelos/calendario.modelo.php'; 
	require_once 'modelos/clientes.modelo.php';
	require_once 'modelos/diagnostico.modelo.php';
	require_once 'modelos/empleados.modelo.php';
	require_once 'modelos/entidades.modelo.php';
	require_once 'modelos/incapacidades.modelo.php';
	require_once 'modelos/ips.modelo.php';
	require_once 'modelos/perfiles.modelo.php';
	require_once 'modelos/seguridad.modelo.php';
	require_once 'modelos/tesoreria.modelo.php';
	require_once 'modelos/usuarios.modelo.php';
	require_once 'modelos/cartera.modelo.php';
	require_once 'modelos/usuariosEps.modelo.php';
	require_once 'modelos/medicos.modelos.php';
	require_once 'modelos/entidadesRegistradas.modelo.php';

	require_once 'extenciones/Excel.php';
	
	require 'vendor/autoload.php';
	
	
	$plantilla = new ControladorPlantilla();
	$plantilla->ctrPlantilla();
