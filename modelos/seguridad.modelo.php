<?php
	require_once "conexion.php";
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloSeguridad{
		/*=============================================
					MOSTRAR Pagasdas
		=============================================*/

		static public function mdlMostrarGeneralPagos($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlMostrarGeneralPagos_byCliente($tabla, $item, $valor){
			
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetchAll();
			
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlDiagnosticosTop($tabla, $item, $valor, $fechaInicio = null, $fechaFinal = null){
			$whereFecha = null;
			if($fechaInicio != null && $fechaFinal != null){
				$whereFecha = " AND inc_fecha_inicio BETWEEN '".$fechaInicio."' AND '".$fechaFinal."' ";
			}
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT count(inc_diagnostico) as total, inc_diagnostico FROM $tabla WHERE inc_diagnostico IS NOT NULL AND inc_diagnostico != '' AND $item = :$item ".$whereFecha." group by inc_diagnostico ORDER BY total DESC LIMIT 7");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT count(inc_diagnostico) as total, inc_diagnostico FROM $tabla WHERE inc_diagnostico IS NOT NULL AND inc_diagnostico != '' ".$whereFecha." group by inc_diagnostico ORDER BY total DESC LIMIT 7");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlDiagnosticosMasRepetido($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT count(inc_diagnostico) as total, inc_diagnostico FROM $tabla WHERE (inc_diagnostico IS NOT NULL AND inc_diagnostico != '') AND $item = :$item group by inc_diagnostico ORDER BY total DESC LIMIT 1");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT count(inc_diagnostico) as total, inc_diagnostico FROM $tabla WHERE (inc_diagnostico IS NOT NULL AND inc_diagnostico != '') group by inc_diagnostico ORDER BY total DESC LIMIT 1");
				$stmt -> execute();
				return $stmt -> fetch();
			}
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlTotalIncapacidad($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE inc_diagnostico IS NOT NULL AND inc_diagnostico != '' AND $item = :$item ");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> rowCount();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE inc_diagnostico IS NOT NULL AND inc_diagnostico != '' ");
				$stmt -> execute();
				return $stmt ->rowCount();
			}
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlNombreDiagnostico($tabla, $item, $valor){
			$stmt = Conexion::conectar()->prepare("SELECT dia_codigo as id, dia_descripcion as name  FROM $tabla WHERE dia_codigo = :$item LIMIT 1");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			//ar_dump($stmt);
			$stmt -> execute();
			return $stmt -> fetch();
			$stmt -> close();
			$stmt = null;
		}


	}