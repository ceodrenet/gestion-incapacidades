<?php

require_once "conexion.php";

class ModeloMedicos extends ModeloDAO{

    static public function mdlMostrarMedicos($tabla, $item, $valor){
        if($item != null){
            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
            $stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt -> fetch(); 

        }else{
            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
            $stmt -> execute();
            return $stmt -> fetchAll();
        }
        //$stmt -> close();
        //$stmt = null;
    }

    static public function mdlIngresarMedicos($tabla, $datos){
        $pdo = Conexion::conectar();
        $stmt = $pdo->prepare("INSERT INTO $tabla (med_nombre, med_identificacion, med_num_registro, med_estado_rethus, med_usuario_crea) VALUES ( :med_nombre, :med_identificacion, :med_num_registro, :med_estado_rethus, :med_usuario_crea)");
        $stmt->bindParam(":med_nombre", 		$datos['med_nombre'], PDO::PARAM_STR);
        $stmt->bindParam(":med_identificacion", $datos['med_identificacion'], PDO::PARAM_STR);
        $stmt->bindParam(":med_num_registro", 	$datos['med_num_registro'], PDO::PARAM_STR);
        $stmt->bindParam(":med_estado_rethus", 	$datos['med_estado_rethus'], PDO::PARAM_STR);
        $stmt->bindParam(":med_usuario_crea", 	$datos['med_usuario_crea'], PDO::PARAM_STR);

        if($stmt->execute()){
            return $pdo->lastInsertId();
        }else{
            return 'Error';
        }
        //$stmt->close();
        //$stmt 
    }

    static public function mdlEditarMedicos($tabla, $datos){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET med_nombre = :med_nombre, 	med_identificacion = :med_identificacion, med_num_registro = :med_num_registro, med_estado_rethus = :med_estado_rethus, med_estado = :med_estado, med_fecha_edicion = :med_fecha_edicion, med_usuario_edita = :med_usuario_edita WHERE med_id = :med_id");
        $stmt->bindParam(":med_nombre", 		$datos['med_nombre'], PDO::PARAM_STR);
        $stmt->bindParam(":med_identificacion", $datos['med_identificacion'], PDO::PARAM_STR);
        $stmt->bindParam(":med_num_registro", 	$datos['med_num_registro'], PDO::PARAM_STR);
        $stmt->bindParam(":med_estado_rethus", 	$datos['med_estado_rethus'], PDO::PARAM_STR);
        $stmt->bindParam(":med_estado", 		$datos['med_estado'], PDO::PARAM_STR);
        $stmt->bindParam(":med_fecha_edicion", 	$datos['med_fecha_edicion'], PDO::PARAM_STR);
        $stmt->bindParam(":med_usuario_edita", 	$datos['med_usuario_edita'], PDO::PARAM_STR);
        $stmt->bindParam(":med_id", 		    $datos['med_id'], PDO::PARAM_STR);
        
        if($stmt->execute()){
            return 'ok';
        }else{
            return $stmt->errorInfo();
        }
        
        //$stmt->close();
        //$stmt = null;
    }

    static public function mdlEditarValorAuditoria($tabla, $campo, $dato, $id){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $campo = :valor WHERE med_id = :med_id");
        $stmt->bindParam(":valor", $dato, PDO::PARAM_INT);
        $stmt->bindParam(":med_id", $id, PDO::PARAM_INT);
        if($stmt -> execute()){
            return "ok";
        }else{
            print_r($stmt->errorInfo());
            return $stmt->errorInfo();	
        }
    }

    static public function mdlBorrarMedicos($tabla, $id){
        $stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE med_id = :med_id");
        $stmt -> bindParam(":med_id", $id, PDO::PARAM_INT);
        if($stmt -> execute()){
            return "ok";
        }else{
            print_r($stmt->errorInfo());
            return $stmt->errorInfo();	
        }
        //$stmt->close();
        //$stmt = null;
    }

    static public function mdlBuscarMedicosByNombre($tabla, $valor){
        $stmt = Conexion::conectar()->prepare("SELECT med_id as id, med_identificacion as ident, med_nombre as name FROM $tabla where med_nombre LIKE '%".$valor."%' OR med_identificacion LIKE '%".$valor."%' ORDER BY med_nombre ASC LIMIT 100");
        $stmt->execute();
        return $stmt->fetchAll();
    }

    static public function mdlCountMedicos($year = null, $tipo = null){
        $fecha1 = $year."-01-01";
		$fecha2 = $year."-12-31";
        $lsql = "SELECT COUNT(*) as total FROM gi_medicos ";

        if($year != null){
            $lsql .= " WHERE med_fecha_creacion BETWEEN '".$fecha1."' AND '".$fecha2."'";
        }

        if($tipo != null){
            if ($year != null) {
                $lsql .=  " AND med_estado_rethus = '".$tipo."'";
            } else {
                $lsql .=  " WHERE med_estado_rethus = '".$tipo."'";
            }
        }

        $stmt = Conexion::conectar()->prepare($lsql);
		$stmt -> execute();
		return $stmt->fetch();
    }

    static public function mdlFiltrarByYear(){
        $query = "SELECT DISTINCT EXTRACT(YEAR FROM med_fecha_creacion) AS anho FROM gi_medicos ORDER BY anho DESC";
        $stmt = Conexion::conectar()->prepare($query);
		$stmt -> execute();
		return $stmt->fetchAll();
    }
}