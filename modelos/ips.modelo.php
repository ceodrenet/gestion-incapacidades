<?php
	require_once "conexion.php";
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloIps
	{	
		static public function mdlMostrarIps($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlMostrarIpsById($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlIngresarIps($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (ips_nombre, ips_nit, ips_tipo, ips_codigo, ips_correo_cartera, ips_correo_radicacion, ips_correo_pqr_v, ips_correo_not_v) VALUES(:ips_nombre, :ips_nit, :ips_tipo, :ips_codigo, :ips_correo_cartera, :ips_correo_radicacion, :ips_correo_pqr_v, :ips_correo_not_v)");

			$stmt->bindParam(":ips_nombre", 	$datos['ips_nombre'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ips_nit", 		$datos['ips_nit'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ips_tipo", 		$datos['ips_tipo'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ips_codigo", 	$datos['ips_codigo'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ips_correo_cartera", $datos['ips_correo_cartera'], PDO::PARAM_STR);
			$stmt->bindParam(":ips_correo_radicacion", $datos['ips_correo_radicacion'],PDO::PARAM_STR);
			$stmt->bindParam(":ips_correo_pqr_v",$datos['ips_correo_pqr_v'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ips_correo_not_v",$datos['ips_correo_not_v'], 	PDO::PARAM_STR);

			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}

		static public function mdlEditarIps($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla  SET ips_nombre = :ips_nombre, ips_nit = :ips_nit, ips_tipo = :ips_tipo, ips_codigo = :ips_codigo, ips_correo_cartera = :ips_correo_cartera,  ips_correo_radicacion = :ips_correo_radicacion, ips_correo_pqr_v = :ips_correo_pqr_v, ips_correo_not_v = :ips_correo_not_v WHERE ips_id = :ips_id");

			$stmt->bindParam(":ips_nombre", 	$datos['ips_nombre'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ips_nit", 		$datos['ips_nit'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ips_tipo", 		$datos['ips_tipo'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ips_codigo", 	$datos['ips_codigo'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ips_correo_cartera",$datos['ips_correo_cartera'], PDO::PARAM_STR);
			$stmt->bindParam(":ips_correo_radicacion",$datos['ips_correo_radicacion'],PDO::PARAM_STR);
			$stmt->bindParam(":ips_correo_pqr_v",$datos['ips_correo_pqr_v'], PDO::PARAM_STR);
			$stmt->bindParam(":ips_id",	$datos['ips_id'], PDO::PARAM_STR);
			$stmt->bindParam(":ips_correo_not_v",$datos['ips_correo_not_v'], PDO::PARAM_STR);

			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}


		static public function mdlBorrarIps($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE ips_id = :ips_id");
			$stmt -> bindParam(":ips_id", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}
	}