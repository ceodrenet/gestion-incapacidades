<?php
	require_once "conexion.php";
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloDiagnostico{
		/*=============================================
					MOSTRAR Diagnosticos
		=============================================*/

		static public function mdlMostrarDiagnostico($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT dia_codigo as id, dia_descripcion as name  FROM $tabla WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT dia_codigo as id, dia_descripcion as name FROM $tabla");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlCompletarDiagnostico($tabla, $valor){
			$stmt = Conexion::conectar()->prepare("SELECT dia_codigo as id, dia_descripcion as name  FROM $tabla WHERE dia_descripcion LIKE '%".$valor."%' OR dia_codigo LIKE '%".$valor."%' ORDER BY dia_descripcion ASC LIMIT 100");
			//$stmt -> bindParam(":busqueda", $preparedVariable, PDO::PARAM_STR);
			//ar_dump($stmt);
			$stmt -> execute();
			return $stmt -> fetchAll();
			$stmt -> close();
			$stmt = null;
		}
	}