<?php
    
    $objPHPExcel = new PHPExcel();

    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Incapacidades")
                             ->setSubject("Incapacidades Actuales")
                             ->setDescription("Descarga del historico de incapacidades registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Incapacidades");


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:AF1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:AF1')->getFont()->setBold( true );


    $objPHPExcel->getActiveSheet()->setTitle('INCAPACIDADES');

    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Identificación");    
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Apellidos y Nombres"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Cargo"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Salario"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Sede"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Centro de Costos"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "SubCentro de Costos"); 
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Ciudad");
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Fecha Inicio"); 
    $objPHPExcel->getActiveSheet()->setCellValue("L1", "Fecha Final"); 
    $objPHPExcel->getActiveSheet()->setCellValue("M1", "Días"); 
    $objPHPExcel->getActiveSheet()->setCellValue("N1", "Clasificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("O1", "Diagnostico"); 
    $objPHPExcel->getActiveSheet()->setCellValue("P1", "Origen"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Q1", "Valor Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("R1", "Valor Administradora"); 
    $objPHPExcel->getActiveSheet()->setCellValue("S1", "Valor Ajuste"); 
    $objPHPExcel->getActiveSheet()->setCellValue("T1", "Fecha Pago En Nomina");  
    $objPHPExcel->getActiveSheet()->setCellValue("U1", "Fecha Generación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("V1", "Estado Tramite");
    $objPHPExcel->getActiveSheet()->setCellValue("W1", "Fecha de Estado");
    $objPHPExcel->getActiveSheet()->setCellValue("X1", "Valor Pagado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Y1", "Fecha Pago"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Z1", "EPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AA1", "AFP"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AB1", "Profesional Responsable"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AC1", "Registro Medico"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AD1", "Entidad-IPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AE1", "Observación");
    $objPHPExcel->getActiveSheet()->setCellValue("AF1", "Estado Empleado"); 
     

    $item = null;
    $valor = null;
    if($_SESSION['cliente_id'] != 0){
        $item = 'inc_empresa';
        $valor = $_SESSION['cliente_id'];
    }
    $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);
    $i = 2;
    foreach ($incapacidades as $key => $value) {

        $strClasificacion = "PRORROGA";
        if($value["inc_clasificacion"] == 1){
            $strClasificacion = "INICIAL";
        }
        $strFecha = null;
        if($value["inc_estado_tramite"] == 'POR RADICAR'){
            $strFecha = $value["inc_fecha_generada"];
        }else if($value["inc_estado_tramite"] == 'RADICADA'){
            $strFecha = $value["inc_fecha_radicacion"];
        }else if($value["inc_estado_tramite"] == 'SOLICITUD DE PAGO'){
            $strFecha = $value["inc_fecha_solicitud"];
        }else if($value["inc_estado_tramite"] == 'PAGADA'){
            $strFecha = $value["inc_fecha_pago"];
        }else if($value["inc_estado_tramite"] == 'SIN RECONOCIMIENTO'){
            $strFecha = $value["inc_fecha_negacion_"];
        }else{
            $strFecha = $value["inc_fecha_generada"];
        }

       
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["emd_cedula"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["emd_cargo"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["emd_salario"]);  
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["emd_sede"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value["emd_centro_costos"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value["emd_subcentro_costos"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["emd_ciudad"]);
        $date = new DateTime($value["inc_fecha_inicio"]);
        $date2 = new DateTime($value["inc_fecha_final"]);
        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, PHPExcel_Shared_Date::PHPToExcel($date)); 
        $objPHPExcel->getActiveSheet()->setCellValue("L".$i, PHPExcel_Shared_Date::PHPToExcel($date2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("M".$i, dias_transcurridos($value["fecha1"] , $value["fecha2"]));
        $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $strClasificacion);
        $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $value["inc_diagnostico"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("P".$i, $value["inc_origen"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$i, $value["inc_valor_pagado_empresa"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("R".$i, $value["inc_valor_pagado_eps"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("S".$i, $value["inc_valor_pagado_ajuste"]); 
        $date3 = new DateTime($value["inc_fecha_pago_nomina"]);
        $objPHPExcel->getActiveSheet()->setCellValue("T".$i, PHPExcel_Shared_Date::PHPToExcel($date3));
        $date4 = new DateTime($value["inc_fecha_generada"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("U".$i, PHPExcel_Shared_Date::PHPToExcel($date4));
        $objPHPExcel->getActiveSheet()->setCellValue("V".$i, $value["inc_estado_tramite"]);
        $date5 = new DateTime($strFecha);
        $objPHPExcel->getActiveSheet()->setCellValue("W".$i, PHPExcel_Shared_Date::PHPToExcel($date5));
        $objPHPExcel->getActiveSheet()->setCellValue("X".$i, $value["inc_valor"]); 
        if($value["inc_fecha_pago"] != null && $value["inc_fecha_pago"]!= ''){
            $date6 = new DateTime($value["inc_fecha_pago"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("Y".$i, PHPExcel_Shared_Date::PHPToExcel($date6)); 
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("Y".$i, $value["inc_fecha_pago"]); 
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue("Z".$i, $value["inc_ips_afiliado"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AA".$i, $value["inc_afp_afiliado"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AB".$i, $value["inc_profesional_responsable"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AC".$i, $value["inc_prof_registro_medico"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AD".$i, $value["inc_donde_se_genera"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AE".$i, $value["inc_observacion"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AF".$i, $value["emd_estado"]);
        $i++;         
    }

    foreach(range('A','AF') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }

    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Informe_consolidado.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');
    

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }