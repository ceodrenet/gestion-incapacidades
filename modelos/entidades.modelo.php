<?php
	require_once "conexion.php";
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloEntidades{
		/*=============================================
					MOSTRAR Entidades
		=============================================*/

		static public function mdlMostrarEntidades($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}

		

		static public function mdlIngresarEntidad($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (ent_nombres, ent_codigo, ent_ciudad, ent_direccion, ent_telefono, ent_horario, ent_observacion, ent_formato_3, ent_formato_2, ent_formato_1, ent_estado) VALUES(:ent_nombres, :ent_codigo, :ent_ciudad, :ent_direccion, :ent_telefono, :ent_horario, :ent_observacion, :ent_formato_3, :ent_formato_2, :ent_formato_1, :ent_estado)");

			$stmt->bindParam(":ent_nombres", 	$datos['ent_nombres'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ent_codigo", 	$datos['ent_codigo'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ent_ciudad", 	$datos['ent_ciudad'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ent_direccion", 	$datos['ent_direccion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ent_telefono", 	$datos['ent_telefono'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ent_horario", 	$datos['ent_horario'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ent_observacion",$datos['ent_observacion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ent_formato_3", 	$datos['ent_formato_3'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ent_formato_2",	$datos['ent_formato_2'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ent_formato_1", 	$datos['ent_formato_1'],	PDO::PARAM_STR);
			$stmt->bindParam(":ent_estado",		$datos['ent_estado'], 		PDO::PARAM_STR);
			

			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}

		static public function mdlEditarEntidades($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla  SET ent_nombres = :ent_nombres, ent_codigo = :ent_codigo, ent_ciudad = :ent_ciudad, ent_direccion = :ent_direccion, ent_telefono = :ent_telefono,  ent_horario = :ent_horario, ent_observacion = :ent_observacion, ent_formato_3 = :ent_formato_3, ent_formato_2 = :ent_formato_2, ent_formato_1 = :ent_formato_1, ent_estado = :ent_estado WHERE ent_id = :ent_id");

			$stmt->bindParam(":ent_nombres", 	$datos['ent_nombres'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ent_codigo", 	$datos['ent_codigo'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ent_ciudad", 	$datos['ent_ciudad'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ent_direccion", 	$datos['ent_direccion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ent_telefono", 	$datos['ent_telefono'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ent_horario", 	$datos['ent_horario'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ent_observacion",$datos['ent_observacion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ent_formato_3", 	$datos['ent_formato_3'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ent_formato_2",	$datos['ent_formato_2'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ent_formato_1", 	$datos['ent_formato_1'],	PDO::PARAM_STR);
			$stmt->bindParam(":ent_estado",		$datos['ent_estado'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ent_id",			$datos['ent_id'], 		PDO::PARAM_STR);
			

			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}


		static public function mdlBorrarEntidad($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE ent_id = :ent_id");
			$stmt -> bindParam(":ent_id", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}
	}