<?php
	require_once "conexion.php";
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloPerfiles
	{
		
		static public function mdlMostrarPerfiles($tabla, $item, $valor){
			if(!is_null($item)){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item AND perfiles_superadin = 0");
				$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt->execute();
				return $stmt->fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE perfiles_superadin = 0");
				$stmt->execute();
				return $stmt->fetchAll();
			}
			
			$stmt->close();
			$stmt = null;

		}

		static public function mdlMostrarMenusPermisos($tabla, $item, $valor){
			if(!is_null($item)){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla JOIN sys_menus ON perfiles_permisos_menu_id_i = menus_id_i WHERE $item = :$item ORDER BY menus_orden_i ASC");
				$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt->execute();
				return $stmt->fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
				$stmt->execute();
				return $stmt->fetchAll();
			}
			
			$stmt->close();
			$stmt = null;

		}


		static public function mdlMostrarMenusPermisosReportes($tabla, $item, $valor){

			if(!is_null($item)){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
				$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt->execute();
				return $stmt->fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
				$stmt->execute();
				return $stmt->fetchAll();
			}
			
		}

		static public function mdlMostrarMenus($tabla, $item, $valor){
			if(!is_null($item)){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY menus_orden_i ASC");
				$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt->execute();
				return $stmt->fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE menus_reporte_i = 0 ORDER BY menus_orden_i ASC");
				$stmt->execute();
				return $stmt->fetchAll();
			}
			
			$stmt->close();
			$stmt = null;

		}


		static public function mdlMostrarMenusReportes($tabla, $reporte){
		
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE menus_reporte_i = 1 AND menus_id_i = $reporte");
			$stmt->execute();
			return $stmt->fetch();
						
			$stmt->close();
			$stmt = null;
		}


		static public function mdlMostrarOpciones($tabla, $item, $valor){
			if(!is_null($item)){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY submenu_id ASC");
				$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt->execute();
				return $stmt->fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY submenu_id ASC");
				$stmt->execute();
				return $stmt->fetchAll();
			}
			
			$stmt->close();
			$stmt = null;

		}


		/*mostrar menu reporte de acuerdo a los que tenga en ela tabla eperfiels reportes*/
		static public function mdlMostrarOpcionesPerfilReporte($perfil, $menu){
			$stmt = Conexion::conectar()->prepare("SELECT DISTINCT perfiles_permisos_reportes_submenu_id_i, submenu_href, submenu_nombre FROM sys_submenu JOIN sys_perfiles_reportes ON perfiles_permisos_reportes_submenu_id_i = submenu_id WHERE perfiles_permisos_reportes_perfil_id_i = :perfil and submenu_menu_id = :menu ORDER BY submenu_nombre ASC");
			$stmt->bindParam(":perfil", $perfil, PDO::PARAM_STR);
			$stmt->bindParam(":menu", $menu, PDO::PARAM_STR);
			$stmt->execute();
			$retur =  $stmt->fetchAll();
			$stmt = null;
			return $retur;
		}


		/* eliminar Perfiles */
		
		static public function mdlBorrarPerfil($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE perfiles_id_i = :perfiles_id_i");
			$stmt -> bindParam(":perfiles_id_i", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}

		/* activar o desactivar y por si acaso */
		
		static public function mdlActualizarPerfiles($tabla, $item1, $valor1, $item2, $valor2){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  $item1 = :$item1 WHERE $item2 = :$item2");
			$stmt->bindParam(":".$item1, 	$valor1, 	PDO::PARAM_STR);
			$stmt->bindParam(":".$item2, 	$valor2, 	PDO::PARAM_STR);
			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}


		static public function mdlIngresarPerfiles($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (perfiles_clientes_id_i, perfiles_descripcion_v, perfilies_estado_i, perfiles_adiciona_i, perfiles_edita_i, perfiles_elimina_i) VALUES(:perfiles_clientes_id_i, :perfiles_descripcion_v, :perfilies_estado_i, :perfiles_adiciona_i, :perfiles_edita_i, :perfiles_elimina_i)");
			$stmt->bindParam(":perfiles_clientes_id_i", $datos['perfiles_clientes_id_i'], PDO::PARAM_STR);
			$stmt->bindParam(":perfiles_descripcion_v", $datos['perfiles_descripcion_v'], PDO::PARAM_STR);
			$stmt->bindParam(":perfilies_estado_i", 	$datos['perfilies_estado_i'], PDO::PARAM_STR);
			$stmt->bindParam(":perfiles_adiciona_i", 	$datos['perfiles_adiciona_i'], PDO::PARAM_STR);
			$stmt->bindParam(":perfiles_edita_i", 		$datos['perfiles_edita_i'], PDO::PARAM_STR);
			$stmt->bindParam(":perfiles_elimina_i", 	$datos['perfiles_elimina_i'], PDO::PARAM_STR);
			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}


		static public function mdlEditarPerfiles($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET perfiles_clientes_id_i = :perfiles_clientes_id_i, perfiles_descripcion_v = :perfiles_descripcion_v, perfilies_estado_i = :perfilies_estado_i ,  perfiles_adiciona_i = :perfiles_adiciona_i, perfiles_edita_i = :perfiles_edita_i, perfiles_elimina_i = :perfiles_elimina_i WHERE perfiles_id_i = :perfiles_id_i ");

			$stmt->bindParam(":perfiles_clientes_id_i", $datos['perfiles_clientes_id_i'], PDO::PARAM_STR);
			$stmt->bindParam(":perfiles_descripcion_v", $datos['perfiles_descripcion_v'], PDO::PARAM_STR);
			$stmt->bindParam(":perfilies_estado_i", 	$datos['perfilies_estado_i'], PDO::PARAM_STR);
			$stmt->bindParam(":perfiles_id_i", 			$datos['perfiles_id_i'], PDO::PARAM_STR);
			$stmt->bindParam(":perfiles_adiciona_i", 	$datos['perfiles_adiciona_i'], PDO::PARAM_STR);
			$stmt->bindParam(":perfiles_edita_i", 		$datos['perfiles_edita_i'], PDO::PARAM_STR);
			$stmt->bindParam(":perfiles_elimina_i", 	$datos['perfiles_elimina_i'], PDO::PARAM_STR);
			
			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}


		/* ingrease nuevos menus */
		static public function mdlIngresarMenu($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (perfiles_permisos_perfil_id_i, perfiles_permisos_menu_id_i) VALUES(:perfiles_permisos_perfil_id_i, :perfiles_permisos_menu_id_i)");
			$stmt->bindParam(":perfiles_permisos_perfil_id_i",  $datos['perfiles_permisos_perfil_id_i'], PDO::PARAM_STR);
			$stmt->bindParam(":perfiles_permisos_menu_id_i",	$datos['perfiles_permisos_menu_id_i'], PDO::PARAM_STR);
			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}

		/* ingrease nuevos menus Rpeortes */
		static public function mdlIngresarMenuReportes($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (perfiles_permisos_reportes_perfil_id_i, perfiles_permisos_reportes_submenu_id_i) VALUES(:perfiles_permisos_reportes_perfil_id_i, :perfiles_permisos_reportes_submenu_id_i)");
			$stmt->bindParam(":perfiles_permisos_reportes_perfil_id_i",  $datos['perfiles_permisos_reportes_perfil_id_i'], PDO::PARAM_STR);
			$stmt->bindParam(":perfiles_permisos_reportes_submenu_id_i",	$datos['perfiles_permisos_reportes_submenu_id_i'], PDO::PARAM_STR);
			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}


		static public function mdlBorrarPermisos($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE perfiles_permisos_perfil_id_i = :perfiles_permisos_perfil_id_i");
			$stmt -> bindParam(":perfiles_permisos_perfil_id_i", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}

		

	}