<?php
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloCartera extends ModeloDAO{

		static public function mdlMostrarDetalleCartera($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}
		}

		static public function mdlMostrarCarteraDeglosada($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla JOIN gi_empresa ON emp_id = car_emp_id JOIN gi_ips ON ips_id = car_eps_id WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla JOIN gi_empresa ON emp_id = car_emp_id JOIN gi_ips ON ips_id = car_eps_id");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlIngresarCartera($tabla, $datos){
			$pdo  = Conexion::conectar();
			$stmt = $pdo->prepare("INSERT INTO $tabla (car_emp_id, car_eps_id, car_paz_salvo, car_fecha_rad, car_fecha_cre, car_fecha_res, car_respuesta, car_valor, car_observacion, car_ruta_pdf) VALUES(:car_emp_id, :car_eps_id, :car_paz_salvo, :car_fecha_rad, :car_fecha_cre, :car_fecha_res, :car_respuesta, :car_valor, :car_observacion, :car_ruta_pdf)");
			$stmt->bindParam(":car_emp_id", 	$datos['car_emp_id'], 		PDO::PARAM_STR);
			$stmt->bindParam(":car_eps_id", 	$datos['car_eps_id'], 		PDO::PARAM_STR);
			$stmt->bindParam(":car_paz_salvo", 	$datos['car_paz_salvo'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_fecha_rad", 	$datos['car_fecha_rad'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_fecha_cre", 	$datos['car_fecha_cre'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_fecha_res", 	$datos['car_fecha_res'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_respuesta",	$datos['car_respuesta'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_valor", 		$datos['car_valor'], 		PDO::PARAM_STR);
			$stmt->bindParam(":car_observacion",$datos['car_observacion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_ruta_pdf",	$datos['car_ruta_pdf'], 	PDO::PARAM_STR);

				
			if($stmt->execute()){
				return $pdo->lastInsertId();
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}

		static public function mdlEditarCartera($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET car_emp_id = :car_emp_id, car_eps_id = :car_eps_id, car_paz_salvo = :car_paz_salvo, car_fecha_rad = :car_fecha_rad, car_fecha_cre = :car_fecha_cre, car_fecha_res = :car_fecha_res, car_respuesta = :car_respuesta, car_valor = :car_valor, car_observacion = :car_observacion, car_ruta_pdf = :car_ruta_pdf WHERE car_id = :car_id");
			$stmt->bindParam(":car_emp_id", 	$datos['car_emp_id'], 		PDO::PARAM_STR);
			$stmt->bindParam(":car_eps_id", 	$datos['car_eps_id'], 		PDO::PARAM_STR);
			$stmt->bindParam(":car_paz_salvo", 	$datos['car_paz_salvo'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_fecha_rad", 	$datos['car_fecha_rad'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_fecha_cre", 	$datos['car_fecha_cre'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_fecha_res", 	$datos['car_fecha_res'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_respuesta",	$datos['car_respuesta'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_valor", 		$datos['car_valor'], 		PDO::PARAM_STR);
			$stmt->bindParam(":car_observacion",$datos['car_observacion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_ruta_pdf",	$datos['car_ruta_pdf'], 	PDO::PARAM_STR);
			$stmt->bindParam(":car_id",$datos['car_id'], 	PDO::PARAM_INT);
				
			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}

		static public function mdlBorrarCartera($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE car_id = :car_id");
			$stmt -> bindParam(":car_id", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}

		static public function mdlIngresarDetalleCartera($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (ci_nombre, ci_periodo, ci_valor, ci_car_id, ci_identificacion) VALUES(:ci_nombre, :ci_periodo, :ci_valor, :ci_car_id, :ci_identificacion)");
			$stmt->bindParam(":ci_nombre", 	$datos['ci_nombre'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ci_periodo", $datos['ci_periodo'], 		PDO::PARAM_STR);
			$stmt->bindParam(":ci_valor", 	$datos['ci_valor'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ci_car_id", 	$datos['ci_car_id'], 	PDO::PARAM_STR);
			$stmt->bindParam(":ci_identificacion", 	$datos['ci_identificacion'], 	PDO::PARAM_STR);

			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}

		static public function mdlBorrarDetalleCartera($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE ci_car_id = :ci_car_id");
			$stmt -> bindParam(":ci_car_id", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}
	}