<?php
	require_once "conexion.php";

	class ModeloTesoreria extends ModeloDAO{
		/*=============================================
					MOSTRAR Pagasdas
		=============================================*/

		static public function mdlMostrarGeneralPagos($tabla, $item, $valor, $fecha1 = null, $fecha2 = null ){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY inc_fecha_pago DESC");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				if($fecha1 == null && $fecha2 == null){
					$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
					$stmt -> execute();
					return $stmt -> fetchAll();
				}else{
					$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE inc_fecha_pago BETWEEN :fecha1 AND :fecha2 ORDER BY inc_fecha_pago DESC");
					$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
					$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
					$stmt -> execute();
					return $stmt -> fetchAll();
				}	
			}
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlMostrarGeneralPagos_byCliente($tabla, $item, $valor, $fecha1 = null, $fecha2 = null){
			if($fecha1 == null && $fecha2 == null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY inc_fecha_pago DESC");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item AND inc_fecha_pago BETWEEN :fecha1 AND :fecha2 ORDER BY inc_fecha_pago DESC");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}			
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlMostrarGeneralPagos_byClienteFactura($tabla, $item, $valor, $fecha1, $fecha2){
			if($item == null){
				$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, inc_fecha_emision_factura, inc_fecha_pago, inc_numero_factura FROM $tabla WHERE inc_fecha_emision_factura BETWEEN '$fecha1' AND '$fecha2' group by inc_numero_factura");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{

				$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, inc_fecha_emision_factura, inc_fecha_pago, inc_numero_factura FROM $tabla WHERE inc_empresa = $valor AND inc_fecha_emision_factura BETWEEN '$fecha1' AND '$fecha2' group by inc_numero_factura");
				
				$stmt -> execute();
				var_dump($stmt);
				return $stmt -> fetchAll();
			}			
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlValorIncapacidad($tabla, $item, $valor,  $fecha1 = null, $fecha2 = null){
			if($item != null){
				if($fecha1 == null && $fecha2 == null){
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, ips_nombre, SUM(inc_valor_pagado_nomina) as nominas FROM $tabla WHERE $item = :$item group by ips_nombre");
					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> execute();
					return $stmt -> fetchAll();
				}else{
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, ips_nombre, SUM(inc_valor_pagado_nomina) as nominas FROM $tabla WHERE $item = :$item AND inc_fecha_pago BETWEEN :fecha1 AND :fecha2 group by ips_nombre");
					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
					$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
					$stmt -> execute();
					return $stmt -> fetchAll();
				}
				$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, ips_nombre FROM $tabla WHERE $item = :$item group by ips_nombre");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{
				if($fecha1 == null && $fecha2 == null){
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, ips_nombre , SUM(inc_valor_pagado_nomina) as nominas FROM $tabla group by ips_nombre");
					$stmt -> execute();
					return $stmt -> fetchAll();
				}else{
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, ips_nombre, SUM(inc_valor_pagado_nomina) as nominas FROM $tabla WHERE inc_fecha_pago BETWEEN :fecha1 AND :fecha2 group by ips_nombre");
					$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
					$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
					$stmt -> execute();
					return $stmt -> fetchAll();
				}
			}
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlValorIncapacidad_All($tabla, $item, $valor,  $fecha1 = null, $fecha2 = null){
			if($item != null){
				if($fecha1 == null && $fecha2 == null){
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, ips_nombre, SUM(inc_valor_pagado_nomina) as nominas FROM $tabla WHERE $item = :$item group by ips_nombre");
					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> execute();
					return $stmt -> fetchAll();
				}else{
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, ips_nombre, SUM(inc_valor_pagado_nomina) as nominas FROM $tabla WHERE $item = :$item AND inc_fecha_pago BETWEEN :fecha1 AND :fecha2 group by ips_nombre");
					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
					$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
					$stmt -> execute();
					return $stmt -> fetchAll();
				}
				$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, ips_nombre FROM $tabla WHERE $item = :$item group by ips_nombre");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{
				if($fecha1 == null && $fecha2 == null){
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, ips_nombre , SUM(inc_valor_pagado_nomina) as nominas FROM $tabla group by ips_nombre");
					$stmt -> execute();
					return $stmt -> fetchAll();
				}else{
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total, ips_nombre, , SUM(inc_valor_pagado_nomina) as nominas FROM $tabla WHERE inc_fecha_pago BETWEEN :fecha1 AND :fecha2 group by ips_nombre");
					$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
					$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
					$stmt -> execute();
					return $stmt -> fetchAll();
				}
			}
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlTotalIncapacidad($tabla, $item, $valor, $fecha1 = null, $fecha2 = null){
			if($item != null){
				if($fecha1 == null && $fecha2 == null){
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total FROM $tabla WHERE $item = :$item ");
					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> execute();
					return $stmt -> fetch();
				}else{
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total FROM $tabla WHERE $item = :$item AND inc_fecha_pago BETWEEN :fecha1 AND :fecha2");
					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
					$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
					$stmt -> execute();
					return $stmt -> fetch();
				}
			}else{
				if($fecha1 == null && $fecha2 == null){
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total FROM $tabla ");
					$stmt -> execute();
					return $stmt -> fetch();
				}else{
					$stmt = Conexion::conectar()->prepare("SELECT SUM(inc_valor) as total FROM $tabla WHERE inc_fecha_pago BETWEEN :fecha1 AND :fecha2");
					$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
					$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
					$stmt -> execute();
					return $stmt -> fetch();
				}
				
			}
			$stmt -> close();
			$stmt = null;
		}

		
		static public function mdlMostrarGroupAndOrderX($campo,$tabla,$condicion, $groupBy = null, $orderBy = null){
			if($condicion==""){
				//si no tiene condicion
				$stmt = Conexion::conectar()->prepare("SELECT $campo FROM $tabla $groupBy $orderBy");
				$stmt->execute();
				return $stmt->fetch(PDO::FETCH_ASSOC);
			}else{
				//si tiene condicion
				$stmt = Conexion::conectar()->prepare("SELECT $campo FROM $tabla WHERE $condicion $groupBy $orderBy");
				$stmt->execute();
				return $stmt->fetch(PDO::FETCH_ASSOC);
			}
			
			$stmt->close();
			$stmt = null;

		}


		static public function mdlCrearFallas($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (filas_fallo_fila, fila_fallo_mensaje, fila_fallo_session_id, fila_fallo_texto_error_v, fila_fallo_cedula) VALUES ( :filas_fallo_fila, :fila_fallo_mensaje, :fila_fallo_session_id, :fila_fallo_texto_error_v, :fila_fallo_cedula)");

			$stmt->bindParam(":filas_fallo_fila", 			$datos['filas_fallo_fila'], 		PDO::PARAM_INT);
			$stmt->bindParam(":fila_fallo_mensaje", 		$datos['fila_fallo_mensaje'], 		PDO::PARAM_STR);
			$stmt->bindParam(":fila_fallo_session_id", 		$datos['fila_fallo_session_id'], 	PDO::PARAM_STR);
			$stmt->bindParam(":fila_fallo_texto_error_v",   $datos['fila_fallo_texto_error_v'], 	PDO::PARAM_STR);
			$stmt->bindParam(":fila_fallo_cedula",   $datos['fila_fallo_cedula'], 	PDO::PARAM_STR);

			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;

		}

	}