<?php
	require_once "conexion.php";
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloUsuarios extends ModeloDAO
	{
		static public function mdlMostrarUsuariosIndex($tabla, $item, $valor){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla JOIN sys_perfiles ON perfiles_id_i = user_perfil_id ");
			$stmt->execute();
			return $stmt->fetchAll();
		}

		static public function mdlMostrarUsuarios($tabla, $item, $valor){
			
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla LEFT JOIN gi_empresa ON user_cliente_id = emp_id LEFT JOIN sys_perfiles ON perfiles_id_i = user_perfil_id WHERE $item = :$item");
				$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt->execute();
				return $stmt->fetch();
				$stmt->close();
				$stmt = null;

			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla LEFT JOIN gi_empresa ON user_cliente_id = emp_id LEFT JOIN sys_perfiles ON perfiles_id_i = user_perfil_id ");
				$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt->execute();
				return $stmt->fetchAll();
				$stmt->close();
				$stmt = null;
			}
		}

		static public function mdlMostrarUsuarios_Clientes_Internos($tabla, $item, $valor){
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla LEFT JOIN gi_empresa ON user_cliente_id = emp_id LEFT JOIN sys_perfiles ON perfiles_id_i = user_perfil_id WHERE $item = :$item AND  usu_correo_v LIKE '%@incapacidades.co%' ");
			$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt->execute();
			return $stmt->fetchAll();
			$stmt->close();
			$stmt = null;
		}

		static public function mdlMostrarUsuarios_Clientes_Externos($tabla, $item, $valor){
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla LEFT JOIN gi_empresa ON user_cliente_id = emp_id LEFT JOIN sys_perfiles ON perfiles_id_i = user_perfil_id WHERE $item = :$item AND  usu_correo_v NOT LIKE '%@incapacidades.co%' ");
			$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt->execute();
			return $stmt->fetchAll();
			$stmt->close();
			$stmt = null;
		}

		

		static public function mdlIngresarUsuario($tabla, $datos){
			$pdo = Conexion::conectar();
			$stmt = $pdo->prepare("INSERT INTO $tabla (user_nombre, usu_correo_v, user_password, user_perfil_id, user_cliente_id, user_ruta_imagen_v) VALUES(:user_nombre, :usu_correo_v, :user_password, :user_perfil_id, :user_cliente_id, :user_ruta_imagen_v)");
			$stmt->bindParam(":user_nombre", 	$datos['user_nombre'], 	PDO::PARAM_STR);
			$stmt->bindParam(":usu_correo_v", 	$datos['usu_correo_v'], 	PDO::PARAM_STR);
			$stmt->bindParam(":user_password", 	$datos['user_password'], PDO::PARAM_STR);
			$stmt->bindParam(":user_perfil_id", 	$datos['user_perfil_id'], 	PDO::PARAM_STR);
			$stmt->bindParam(":user_cliente_id", 	$datos['user_cliente_id'], 	PDO::PARAM_STR);
			$stmt->bindParam(":user_ruta_imagen_v", 	$datos['user_ruta_imagen_v'], 	PDO::PARAM_STR);
			if($stmt->execute()){
				return $pdo->lastInsertId();
			}else{
				return 'Error';//$stmt->errorInfo();
			}
			$stmt->close();
			$stmt = null;
		}

		static public function mdlRegistrarIngreso($datos){
			$pdo  = Conexion::conectar();
			$stmt = $pdo->prepare("INSERT INTO 
			gi_usuario(user_nombre,
						usu_correo_v,
						user_password,
						user_perfil_id,
						user_cod_validacion_v,
						user_estado_i
					)
				VALUES(	
					:user_nombre,
					:usu_correo_v,
					:user_password,
					:user_perfil_id,
					:user_cod_validacion_v,
					:user_estado_i)");
			$stmt->bindParam(":user_nombre", $datos['user_nombre'], PDO::PARAM_STR);
			$stmt->bindParam(":usu_correo_v", $datos['usu_correo_v'], PDO::PARAM_STR);
			$stmt->bindParam(":user_password", $datos['user_password'], PDO::PARAM_STR);
			$stmt->bindParam(":user_perfil_id", $datos['user_perfil_id'], PDO::PARAM_STR);
			$stmt->bindParam(":user_cod_validacion_v", $datos['user_cod_validacion_v'], PDO::PARAM_STR);
			$stmt->bindParam(":user_estado_i", $datos['user_estado_i'], PDO::PARAM_STR);
			if($stmt->execute()){
				$stmt = null;
				return $pdo->lastInsertId();
			}else{
				return 'error';
			}	
		}

		

		static public function mdlEditaruser_usuario($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET user_nombre = :user_nombre, usu_correo_v = :usu_correo_v, user_password = :user_password, user_perfil_id = :user_perfil_id , user_cliente_id = :user_cliente_id, user_ruta_imagen_v = :user_ruta_imagen_v WHERE user_id = :user_id ");
			$stmt->bindParam(":user_nombre", 	$datos['user_nombre'], 	PDO::PARAM_STR);
			$stmt->bindParam(":usu_correo_v", 	$datos['usu_correo_v'], 	PDO::PARAM_STR);
			$stmt->bindParam(":user_password", 	$datos['user_password'], PDO::PARAM_STR);
			$stmt->bindParam(":user_perfil_id", 	$datos['user_perfil_id'], 	PDO::PARAM_STR);
			$stmt->bindParam(":user_id", 			$datos['user_id'], PDO::PARAM_INT);
			$stmt->bindParam(":user_cliente_id", 	$datos['user_cliente_id'], 	PDO::PARAM_STR);
			$stmt->bindParam(":user_ruta_imagen_v", 	$datos['user_ruta_imagen_v'], 	PDO::PARAM_STR);
			
			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}

		/* Actualizar user_usuario */
		
		static public function mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  $item1 = :$item1 WHERE $item2 = :$item2");
			$stmt->bindParam(":".$item1, 	$valor1, 	PDO::PARAM_STR);
			$stmt->bindParam(":".$item2, 	$valor2, 	PDO::PARAM_STR);
			if($stmt->execute()){
				return 'ok';
			}else{
				return $stmt->errorInfo();
			}
			$stmt->close();
			$stmt = null;
		}

		static public function mdlBorrarUsuario($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE user_id = :user_id");
			$stmt -> bindParam(":user_id", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}
		static public function mdlValidarEmail($correo){
			$stmt = Conexion::conectar()->prepare("SELECT user_usuario FROM gi_usuario WHERE usu_correo_v =
				'$correo'");
			$stmt->execute();
			return $stmt->fetch();
			$stmt->close();
			$stmt = null;
		}

		static public function mdlValidarCodigo($codigo){
			$stmt = Conexion::conectar()->prepare("SELECT user_id,user_nombre,usu_correo_v,user_estado_i  FROM gi_usuario WHERE user_cod_validacion_v = '$codigo'");
			$stmt->execute();
			return $stmt->fetch();
			$stmt->close();
			$stmt = null;
		}



	}