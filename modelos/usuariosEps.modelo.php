<?php
	require_once "conexion.php";
	/**
	* Manipular los usuarios de las EPS del sistema
	*/
	class ModeloUsuariosEps extends ModeloDAO{
		/*=============================================
					MOSTRAR USUARIOS EPS
		=============================================*/
		static public function mdlMostrarUsuariosEps($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch(); 
	
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlIngresarUsuariosEps($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (use_ips_id_i, use_usuario_v, use_password_v,  use_url_v, use_emp_id_i, use_observacion_v) VALUES ( :use_ips_id_i, :use_usuario_v, :use_password_v, :use_url_v, :use_emp_id_i, :use_observacion_v)");
			$stmt->bindParam(":use_ips_id_i", 		$datos['use_ips_id_i'], 	PDO::PARAM_STR);
			$stmt->bindParam(":use_usuario_v", 		$datos['use_usuario_v'], 	PDO::PARAM_STR);
			$stmt->bindParam(":use_password_v", 	$datos['use_password_v'], 	PDO::PARAM_STR);
			$stmt->bindParam(":use_url_v", 			$datos['use_url_v'], 		PDO::PARAM_STR);
			$stmt->bindParam(":use_emp_id_i", 		$datos['use_emp_id_i'], 	PDO::PARAM_STR);
			$stmt->bindParam(":use_observacion_v", 	$datos['use_observacion_v'], 	PDO::PARAM_STR);

			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			//$stmt->close();
			//$stmt = null;
		}

		static public function mdlEditarUsuariosEps($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET use_ips_id_i = :use_ips_id_i, 	use_usuario_v = :use_usuario_v, use_password_v = :use_password_v, use_url_v = :use_url_v , use_observacion_v = :use_observacion_v WHERE use_id_i = :use_id_i");
			$stmt->bindParam(":use_ips_id_i", 		$datos['use_ips_id_i'], 	PDO::PARAM_STR);
			$stmt->bindParam(":use_usuario_v", 		$datos['use_usuario_v'], 	PDO::PARAM_STR);
			$stmt->bindParam(":use_password_v", 	$datos['use_password_v'], 	PDO::PARAM_STR);
			$stmt->bindParam(":use_url_v", 			$datos['use_url_v'], 		PDO::PARAM_STR);
			$stmt->bindParam(":use_id_i" , 			$datos['use_id_i'], 		PDO::PARAM_STR);
			$stmt->bindParam(":use_observacion_v", 	$datos['use_observacion_v'],PDO::PARAM_STR);
			
			if($stmt->execute()){
				return 'ok';
			}else{
				return $stmt->errorInfo();
			}
			$stmt->close();
			$stmt = null;
		}

		static public function mdlBorrarUsuariosEps($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE use_id_i = :use_id_i");
			$stmt -> bindParam(":use_id_i", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}
	}