<?php
	require_once "conexion.php";
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloEmpleados extends ModeloDAO{
		/*	MOSTRAR Empleados */

		static public function mdlMostrarEmpleados($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla LEFT JOIN gi_ips ON ips_id = emd_eps_id WHERE $item = :$item ");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla LEFT JOIN gi_ips ON ips_id = emd_eps_id");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}
		
		/*Aqui richard	filtrar por nombre */
		static public function mdlMostrarNombres($tabla, $item, $valor){
			if($item != null){
				//AND si  no se  cumplen las dos cndiciones no vale
				//OR si uno va la otra no importa
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla LEFT JOIN gi_ips ON ips_id = emd_eps_id WHERE $item = :$item OR emd_nombre LIKE '%".$valor."%'");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlMostrarEmpleadosTotales($tabla){
			
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla JOIN gi_ips ON ips_id = emd_eps_id JOIN gi_empresa ON emp_id = emd_emp_id");
			$stmt -> execute();
			return $stmt -> fetchAll();
		
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlMostrarEmpleadosX2($tabla, $item, $valor, $valor2){
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item AND emd_emp_id = :emd_emp_id");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":emd_emp_id" , $valor2, PDO::PARAM_INT);
			$stmt -> execute();
			return $stmt -> fetch();
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlMostrarEmpleados_incapacidad($tabla, $item, $valor, $empresa){
			if($_SESSION['cliente_id'] != 0){
				if($item != null){
					$stmt = Conexion::conectar()->prepare("SELECT emd.*, gi_empresa.emp_nombre, b.ips_nombre as eps, c.ips_nombre as arl, est.est_emp_desc_v FROM $tabla as emd JOIN gi_empresa ON emp_id = emd_emp_id LEFT JOIN gi_ips b on b.ips_id = emd.emd_eps_id LEFT JOIN gi_ips c on c.ips_id = emd.emd_arl_id JOIN gi_estado_empleados est ON est.est_emp_id_i = emd.emd_estado WHERE $item = :$item AND emp_id = ".$empresa);
					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> execute();
		 			return $stmt -> fetch();
				}
				$stmt -> close();
				$stmt = null;
			}else{
				if($item != null){
					$stmt = Conexion::conectar()->prepare("SELECT emd.*, gi_empresa.emp_nombre, b.ips_nombre as eps, c.ips_nombre as arl, est.est_emp_desc_v FROM $tabla as emd JOIN gi_empresa ON emp_id = emd_emp_id LEFT JOIN gi_ips b on b.ips_id = emd.emd_eps_id LEFT JOIN gi_ips c on c.ips_id = emd.emd_arl_id JOIN gi_estado_empleados est ON est.est_emp_id_i = emd.emd_estado WHERE $item = :$item ");
					$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
					$stmt -> execute();
					return $stmt -> fetch();
				}
				$stmt -> close();
				$stmt = null;	
			}
		}

		static public function mdlMostrarEmpleados_byEmpresa($tabla, $item, $valor){
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla LEFT JOIN gi_ips ON ips_id = emd_eps_id WHERE $item = :$item ORDER BY emd_id DESC");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetchAll();
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlMostrarEmpleados_byEmpresax($tabla, $item, $valor){
			$stmt = Conexion::conectar()->prepare("SELECT emp_nombre, emd_tipo_identificacion, emd_cedula, emd_nombre, emd_fecha_ingreso, emd_salario, emd_salario_promedio, emd_estado, emd_fecha_retiro, a.ips_nombre as ips_nombre, emd_fecha_afiliacion_eps, b.ips_nombre as emd_arl_id, c.ips_nombre as emd_afp_id, emd_fecha_calificacion_PCL, emd_entidad_calificadora, emd_diagnostico, emd_cargo, emd_sede, emd_genero, emd_tipo_empleado, emd_centro_costos, emd_subcentro_costos, emd_ciudad, emd_codigo_nomina, emd_telefono_v, emd_correo_v FROM $tabla LEFT JOIN gi_ips a ON a.ips_id = emd_eps_id LEFT JOIN gi_ips b ON b.ips_id = emd_arl_id LEFT JOIN gi_ips c ON c.ips_id = emd_afp_id JOIN gi_empresa ON emp_id = emd_emp_id WHERE $item = :$item  AND emd_estado = 1 ORDER BY emd_nombre ASC");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetchAll();
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlMostrarEmpleados_byEmpresaY($tabla, $item, $valor){
			$stmt = Conexion::conectar()->prepare("SELECT emp_nombre, emd_tipo_identificacion, emd_cedula, emd_nombre, emd_fecha_ingreso, emd_salario, emd_salario_promedio, emd_estado, emd_fecha_retiro, a.ips_nombre as ips_nombre, emd_fecha_afiliacion_eps, b.ips_nombre as emd_arl_id, c.ips_nombre as emd_afp_id, emd_fecha_calificacion_PCL, emd_entidad_calificadora, emd_diagnostico, emd_cargo, emd_sede, emd_genero, emd_tipo_empleado, emd_centro_costos, emd_subcentro_costos, emd_ciudad, emd_codigo_nomina , emd_telefono_v, emd_correo_v FROM $tabla LEFT JOIN gi_ips a ON a.ips_id = emd_eps_id LEFT JOIN gi_ips b ON b.ips_id = emd_arl_id LEFT JOIN gi_ips c ON c.ips_id = emd_afp_id JOIN gi_empresa ON emp_id = emd_emp_id WHERE $item = :$item  AND emd_estado = 2 ORDER BY emd_nombre ASC");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetchAll();
			$stmt -> close();
			$stmt = null;
		}

		

		/* Ingresar los empleados */
		//RFB---- Se han adicionado dos campos para validar suspencion de salario 01062022
		
		static public function mdlIngresarEmpleados($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (emd_nombre, emd_cedula, emd_emp_id, emd_fecha_ingreso, emd_salario, emd_tipo_identificacion, emd_fecha_retiro, emd_salario_promedio, emd_fecha_nacimiento, emd_eps_id, emd_fecha_afiliacion_eps, emd_afp_id, emd_arl_id, emd_fecha_calificacion_PCL, emd_entidad_calificadora, emd_diagnostico, emd_cargo, emd_sede, emd_ruta_cedula, emd_ruta_otro, emd_genero, emd_tipo_empleado, emd_codigo_nomina, emd_centro_costos, emd_ciudad, emd_subcentro_costos, emd_estado,emd_telefono_v, emd_correo_v,emd_susp_salario_i,emd_fech_susp_salario) VALUES(:emd_nombre, :emd_cedula, :emd_emp_id, :emd_fecha_ingreso, :emd_salario, :emd_tipo_identificacion, :emd_fecha_retiro, :emd_salario_promedio, :emd_fecha_nacimiento, :emd_eps_id, :emd_fecha_afiliacion_eps, :emd_afp_id, :emd_arl_id, :emd_fecha_calificacion_PCL, :emd_entidad_calificadora, :emd_diagnostico, :emd_cargo, :emd_sede , :emd_ruta_cedula, :emd_ruta_otro, :emd_genero, :emd_tipo_empleado, :emd_codigo_nomina, :emd_centro_costos, :emd_ciudad, :emd_subcentro_costos, :emd_estado,:emd_telefono_v, :emd_correo_v, :emd_susp_salario_i, :emd_fech_susp_salario)");

			$stmt->bindParam(":emd_nombre", 		$datos['emd_nombre'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_cedula", 		$datos['emd_cedula'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_emp_id", 		$datos['emd_emp_id'], 	PDO::PARAM_INT);
			$stmt->bindParam(":emd_fecha_ingreso", 	$datos['emd_fecha_ingreso'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_salario", 		$datos['emd_salario'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_tipo_identificacion", 	$datos['emd_tipo_identificacion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_fecha_retiro", 	$datos['emd_fecha_retiro'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_salario_promedio", 		$datos['emd_salario_promedio'], 	PDO::PARAM_INT);
			$stmt->bindParam(":emd_fecha_nacimiento", 	$datos['emd_fecha_nacimiento'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_eps_id", 		$datos['emd_eps_id'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_fecha_afiliacion_eps",$datos['emd_fecha_afiliacion_eps'],PDO::PARAM_STR);
			$stmt->bindParam(":emd_afp_id", 	$datos['emd_afp_id'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_arl_id", 	$datos['emd_arl_id'], 	PDO::PARAM_INT);
			$stmt->bindParam(":emd_fecha_calificacion_PCL", $datos['emd_fecha_calificacion_PCL'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_entidad_calificadora", $datos['emd_entidad_calificadora'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_diagnostico", $datos['emd_diagnostico'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_cargo", $datos['emd_cargo'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_sede", $datos['emd_sede'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_ruta_cedula", $datos['emd_ruta_cedula'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_ruta_otro", 	 $datos['emd_ruta_otro'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_genero", 		 $datos['emd_genero'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_tipo_empleado", 	 $datos['emd_tipo_empleado'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_codigo_nomina", 	 $datos['emd_codigo_nomina'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_centro_costos", 	 $datos['emd_centro_costos'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_ciudad", 		 $datos['emd_ciudad'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_subcentro_costos", 	$datos['emd_subcentro_costos'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_estado", 		$datos['emd_estado'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_caso_especial_i", 	$datos['emd_caso_especial_i'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_telefono_v", 		$datos['emd_telefono_v'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_correo_v", 		$datos['emd_correo_v'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_susp_salario_i", 		$datos['emd_susp_salario_i'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_fech_susp_salario", 		$datos['emd_fech_susp_salario'], PDO::PARAM_STR);
			print_r($stmt);
			if($stmt->execute()){
				return 'ok';
			}else{
				print_r($stmt->errorInfo());
				return $stmt->errorInfo();
			}
			$stmt->close();
			$stmt = null;
		}
		//RFB---- Se han adicionado dos campos para validar suspencion de salario 01062022
		static public function mdlIngresarEmpleados_return($tabla, $datos){
			$pdo = Conexion::conectar();
			$stmt = $pdo->prepare("INSERT INTO $tabla (emd_nombre, emd_cedula, emd_emp_id, emd_fecha_ingreso, emd_salario, emd_tipo_identificacion, emd_fecha_retiro, emd_salario_promedio, emd_fecha_nacimiento, emd_eps_id, emd_fecha_afiliacion_eps, emd_afp_id, emd_arl_id, emd_fecha_calificacion_PCL, emd_entidad_calificadora, emd_diagnostico, emd_cargo, emd_sede, emd_ruta_cedula, emd_ruta_otro, emd_genero, emd_tipo_empleado, emd_codigo_nomina, emd_centro_costos, emd_ciudad, emd_subcentro_costos, emd_estado, emd_caso_especial_i,emd_telefono_v, emd_correo_v,emd_susp_salario_i,emd_fech_susp_salario) VALUES(:emd_nombre, :emd_cedula, :emd_emp_id, :emd_fecha_ingreso, :emd_salario, :emd_tipo_identificacion, :emd_fecha_retiro, :emd_salario_promedio, :emd_fecha_nacimiento, :emd_eps_id, :emd_fecha_afiliacion_eps, :emd_afp_id, :emd_arl_id, :emd_fecha_calificacion_PCL, :emd_entidad_calificadora, :emd_diagnostico, :emd_cargo, :emd_sede , :emd_ruta_cedula, :emd_ruta_otro, :emd_genero, :emd_tipo_empleado, :emd_codigo_nomina, :emd_centro_costos, :emd_ciudad, :emd_subcentro_costos, :emd_estado, :emd_caso_especial_i,:emd_telefono_v, :emd_correo_v,:emd_susp_salario_i,:emd_fech_susp_salario)");
			
			//$stmt->bindParam(":emd_emp_id", 		$datos['emd_emp_id'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_nombre", 		$datos['emd_nombre'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_cedula", 		$datos['emd_cedula'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_emp_id", 		$datos['emd_emp_id'], 	PDO::PARAM_INT);
			$stmt->bindParam(":emd_fecha_ingreso", 	$datos['emd_fecha_ingreso'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_salario", 		$datos['emd_salario'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_tipo_identificacion", 	$datos['emd_tipo_identificacion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_fecha_retiro", 	$datos['emd_fecha_retiro'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_salario_promedio", 		$datos['emd_salario_promedio'], 	PDO::PARAM_INT);
			$stmt->bindParam(":emd_fecha_nacimiento", 	$datos['emd_fecha_nacimiento'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_eps_id", 		$datos['emd_eps_id'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_fecha_afiliacion_eps",$datos['emd_fecha_afiliacion_eps'],PDO::PARAM_STR);
			$stmt->bindParam(":emd_afp_id", 	$datos['emd_afp_id'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_arl_id", 	$datos['emd_arl_id'], 	PDO::PARAM_INT);
			$stmt->bindParam(":emd_fecha_calificacion_PCL", $datos['emd_fecha_calificacion_PCL'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_entidad_calificadora", $datos['emd_entidad_calificadora'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_diagnostico", $datos['emd_diagnostico'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_cargo", $datos['emd_cargo'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_sede", $datos['emd_sede'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_ruta_cedula", $datos['emd_ruta_cedula'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_ruta_otro", 	 $datos['emd_ruta_otro'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_genero", 		 $datos['emd_genero'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_tipo_empleado", 	 $datos['emd_tipo_empleado'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_codigo_nomina", 	 $datos['emd_codigo_nomina'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_centro_costos", 	 $datos['emd_centro_costos'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_ciudad", 		 $datos['emd_ciudad'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_subcentro_costos", 	$datos['emd_subcentro_costos'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_estado", 		$datos['emd_estado'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_caso_especial_i", 		$datos['emd_caso_especial_i'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_telefono_v", $datos['emd_telefono_v'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_correo_v", $datos['emd_correo_v'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_susp_salario_i", $datos['emd_susp_salario_i'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_fech_susp_salario", $datos['emd_fech_susp_salario'], PDO::PARAM_STR);
			
			//$stmt->execute();
			//die(var_dump( $datos));
			if($stmt->execute()){
				return $pdo->lastInsertId();
			}else{
				print_r($stmt->errorInfo());
				return "error";
			}
			$stmt->close();
			$stmt = null;
		}
		
		/* editar los empleados */
		//RFB---- Se han adicionado dos campos para validar suspencion de salario 01062022
		
		static public function mdlEditarEmpleados($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  emd_nombre = :emd_nombre, emd_cedula = :emd_cedula, emd_emp_id = :emd_emp_id, emd_fecha_ingreso = :emd_fecha_ingreso, emd_salario = :emd_salario, emd_tipo_identificacion = :emd_tipo_identificacion, emd_fecha_retiro = :emd_fecha_retiro, emd_salario_promedio = :emd_salario_promedio, emd_fecha_nacimiento = :emd_fecha_nacimiento, emd_eps_id = :emd_eps_id, emd_fecha_afiliacion_eps = :emd_fecha_afiliacion_eps, emd_afp_id = :emd_afp_id, emd_arl_id = :emd_arl_id, emd_fecha_calificacion_PCL =:emd_fecha_calificacion_PCL, emd_entidad_calificadora = :emd_entidad_calificadora, emd_diagnostico = :emd_diagnostico, emd_cargo = :emd_cargo, emd_sede = :emd_sede , emd_ruta_cedula = :emd_ruta_cedula, emd_ruta_otro = :emd_ruta_otro, emd_genero = :emd_genero, emd_tipo_empleado = :emd_tipo_empleado, emd_codigo_nomina = :emd_codigo_nomina, emd_centro_costos = :emd_centro_costos, emd_ciudad = :emd_ciudad, emd_subcentro_costos = :emd_subcentro_costos, emd_estado = :emd_estado, emd_telefono_v = :emd_telefono_v, emd_correo_v = :emd_correo_v, emd_caso_especial_i = :emd_caso_especial_i,emd_susp_salario_i = :emd_susp_salario_i,emd_fech_susp_salario = :emd_fech_susp_salario WHERE emd_id = :emd_id");

			$stmt->bindParam(":emd_nombre", 		$datos['emd_nombre'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_cedula", 		$datos['emd_cedula'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_emp_id", 		$datos['emd_emp_id'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_fecha_ingreso", 	$datos['emd_fecha_ingreso'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_salario", 		$datos['emd_salario'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_id", 			$datos['emd_id'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_tipo_identificacion", 	$datos['emd_tipo_identificacion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_fecha_retiro", 	$datos['emd_fecha_retiro'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_salario_promedio", 		$datos['emd_salario_promedio'], 	PDO::PARAM_INT);
			$stmt->bindParam(":emd_fecha_nacimiento", 	$datos['emd_fecha_nacimiento'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emd_eps_id", 			$datos['emd_eps_id'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_fecha_afiliacion_eps",$datos['emd_fecha_afiliacion_eps'],PDO::PARAM_STR);
			$stmt->bindParam(":emd_afp_id", 		$datos['emd_afp_id'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_arl_id", 		$datos['emd_arl_id'], 	PDO::PARAM_INT);
			$stmt->bindParam(":emd_fecha_calificacion_PCL", $datos['emd_fecha_calificacion_PCL'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_entidad_calificadora", $datos['emd_entidad_calificadora'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_diagnostico", 	$datos['emd_diagnostico'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_cargo", 			$datos['emd_cargo'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_sede", 			$datos['emd_sede'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_ruta_cedula", 	$datos['emd_ruta_cedula'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_ruta_otro", 	 	$datos['emd_ruta_otro'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_genero", 		 $datos['emd_genero'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_tipo_empleado", 	 $datos['emd_tipo_empleado'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_codigo_nomina", 	 $datos['emd_codigo_nomina'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_centro_costos", 	 $datos['emd_centro_costos'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_ciudad", 		 $datos['emd_ciudad'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_subcentro_costos", 	$datos['emd_subcentro_costos'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_estado", 		$datos['emd_estado'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_telefono_v", 		$datos['emd_telefono_v'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_correo_v", 		$datos['emd_correo_v'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_caso_especial_i", 		$datos['emd_caso_especial_i'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_susp_salario_i", 		$datos['emd_susp_salario_i'], PDO::PARAM_STR);
			$stmt->bindParam(":emd_fech_susp_salario", 		$datos['emd_fech_susp_salario'], PDO::PARAM_STR);

			if($stmt->execute()){
				return 'ok';
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}

		/* Borrar empleado */
		
		static public function mdlBorrarEmpleado($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE emd_id = :emd_id");
			$stmt -> bindParam(":emd_id", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}

		/* Actualizar Empleados */
		static public function mdlActualizarEmpleado($tabla, $item1, $valor1, $item2, $valor2){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  $item1 = :$item1 WHERE $item2 = :$item2");
			$stmt->bindParam(":".$item1, 	$valor1, 	PDO::PARAM_STR);
			$stmt->bindParam(":".$item2, 	$valor2, 	PDO::PARAM_STR);
			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}

		static function mdlGetCantidadEmpleadosByEmpresaByTipo($empresa, $condicion){
			$stmt = Conexion::conectar()->prepare("SELECT count(*) as estado FROM gi_empleados JOIN gi_empresa ON emp_id = emd_emp_id WHERE emd_emp_id = ".$empresa." ".$condicion);
			$stmt -> execute();
			return $stmt -> fetch();
		}

		static public function mdlBuscarEmpleadoBySelect($valor, $idCliente){
			$stmt = Conexion::conectar()->prepare("SELECT emd_id as id, emd_cedula as ident, emd_nombre as name FROM gi_empleados where emd_emp_id = $idCliente AND emd_nombre LIKE '%".$valor."%' OR emd_cedula LIKE '%".$valor."%' ORDER BY emd_nombre ASC LIMIT 100");
			$stmt->execute();
			return $stmt->fetchAll();
		}

		/* Borrar casos especiales */
		
		/*static public function mdlBorrarCasosEspeciales($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE emd_id = :emd_id");
			$stmt -> bindParam(":emd_id", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}*/
	}