<?php
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloIncapacidades extends ModeloDAO
	{
		/*=============================================
				Mostrar Incapacidades
		=============================================*/

		static public function mdlMostrarIncapacidades($tabla, $item, $valor){
			
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY inc_id DESC");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				//return $stmt->debugDumpParams();
				$stmt -> execute();
				return $stmt -> fetch();

			}else{
				$stmt = Conexion::conectar()->prepare("SELECT inc_id, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado , ip4.ips_nombre as inc_afp_afiliado,  emd_nombre as inc_nombres_afiliado, inc_estado_tramite as inc_estado, inc_diagnostico, emp_nombre as inc_empresa, inc_ruta_incapacidad_transcrita, inc_ruta_incapacidad , inc_fecha_inicio , inc_origen, inc_clasificacion FROM $tabla LEFT JOIN gi_ips ip2 ON ip2.ips_id = inc_ips_afiliado  JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id ORDER BY inc_id DESC");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}

		
		static public function mdlMostrarIncapacidadesClientes($tabla, $item, $valor){
			
			$stmt = Conexion::conectar()->prepare("SELECT inc_id, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado,  ip4.ips_nombre as inc_afp_afiliado, emd_cedula,	emd_nombre as inc_nombres_afiliado, inc_estado_tramite as inc_estado, inc_diagnostico, emp_nombre as inc_empresa, inc_ruta_incapacidad_transcrita, inc_ruta_incapacidad , inc_fecha_inicio, inc_fecha_final   , inc_origen , inc_clasificacion, inc_estado as estadoInc, inc_numero_notificaciones_i, emd_estado FROM $tabla LEFT JOIN gi_ips as ip2 ON ip2.ips_id = inc_ips_afiliado  JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id  LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id WHERE $item = :$item ORDER BY inc_id DESC LIMIT 10");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetchAll();
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlMostrarIncapacidadesClientesBetween($tabla, $item, $valor, $fecha1, $fecha2){
			
			$session = '';
			if($_SESSION['cliente_id'] != 0){ 
				$session = "AND inc_empresa = ".$_SESSION['cliente_id']."";
			}

			$stmt = Conexion::conectar()->prepare("SELECT inc_id, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado,  ip4.ips_nombre as inc_afp_afiliado, emd_cedula,	emd_nombre as inc_nombres_afiliado, inc_estado_tramite as inc_estado, inc_diagnostico, emp_nombre as inc_empresa, inc_ruta_incapacidad_transcrita, inc_ruta_incapacidad , inc_fecha_inicio , inc_origen , inc_clasificacion FROM $tabla LEFT JOIN gi_ips as ip2 ON ip2.ips_id = inc_ips_afiliado  JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id  LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id WHERE $item = :$item AND inc_fecha_pago_nomina BETWEEN :fecha1 AND :fecha2 ".$session." ORDER BY inc_id DESC ");
			
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
			$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);

			$stmt -> execute();
			return $stmt -> fetchAll();
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlGetDatoNotificacionById($idIncapacidad){
			/*cONSULTA SQL*/
			$Lsql = "SELECT inc_numero_notificaciones_i,inc_fecha_primera_notificacion_d FROM gi_incapacidad WHERE inc_id =$idIncapacidad";
			$stmt = Conexion::conectar()->prepare($Lsql);
			$stmt -> execute();
			return $stmt -> fetch();
		}

		static public function mdlMostrarIncapacidadesExportar($tabla, $item, $valor, $fechaInicial = null, $fechaFinal = null, $incompletaas = null, $formato=null, $estadoTramite = null, $fechaPago = null){
			if($formato == null){
				$formato = '%d/%m/%Y';
			}	

			if($item != null){
				$Lsql = "
SELECT 
	inc_id, 
	emp_nombre,
	emd_codigo_nomina, 
	emd_cedula, 
	emd_nombre, 
	emd_cargo,
	emd_salario,
	Date_format(inc_fecha_inicio,'$formato') as inc_fecha_inicio, 
	Date_format(inc_fecha_final,'$formato') as inc_fecha_final,
 	(DATEDIFF(inc_fecha_final,inc_fecha_inicio) +1) as dias,
 	gc.cla_descripcion as inc_clasificacion ,
	inc_diagnostico, 
 	ori_descripcion_v as inc_origen,  	
 	inc_valor_pagado_empresa, 
 	inc_valor_pagado_eps, 
 	inc_valor_pagado_ajuste, 
 	Date_format(inc_fecha_pago_nomina,'$formato') as inc_fecha_pago_nomina, 
 	Date_format(inc_fecha_generada,'$formato') as inc_fecha_generada, 
 	est_tra_desc_i as inc_estado_tramite,
 	case 
 		when (inc_estado_tramite = 1) then  Date_format(inc_fecha_generada,'$formato')  
 		when (inc_estado_tramite = 2) then  Date_format(inc_fecha_generada,'$formato')  
 		when (inc_estado_tramite = 3) then  Date_format(inc_fecha_radicacion,'$formato')  
 		when (inc_estado_tramite = 4) then  Date_format(inc_fecha_solicitud,'$formato')  
 		when (inc_estado_tramite = 5) then  Date_format(inc_fecha_pago,'$formato')  
 		when (inc_estado_tramite = 6) then  Date_format(inc_fecha_negacion_,'$formato')  
 	end as inc_Fecha_estado,  
 	inc_valor,
	Date_format(inc_fecha_pago,'$formato') as inc_fecha_pago, 
 	Date_format(inc_fecha_radicacion,'$formato') as inc_fecha_radicacion,
 	Date_format(inc_fecha_solicitud,'$formato') as inc_fecha_solicitud,
 	Date_format(inc_fecha_negacion_,'$formato') as inc_fecha_negacion_ ,
 	Date_format(inc_fecha_recepcion_d,'$formato') as inc_fecha_recepcion_d ,
 	Date_format(inc_fecha_pago,'$formato') as inc_fecha_pago, 
 	gee.est_emp_desc_v,
 	ip2.ips_nombre as inc_ips_afiliado,
 	ip4.ips_nombre as inc_afp_afiliado ,
 	emd_sede,
 	emd_centro_costos, 
 	emd_subcentro_costos, 
 	emd_ciudad,
 	inc_profesional_responsable,
 	inc_prof_registro_medico,
 	inc_donde_se_genera,
 	gd.doc_descripcion_v as inc_tipo_generacion , 
 	gi_atencion.atn_descripcion,
 	a.user_nombre as creador,
 	b.user_nombre as editor ,
 	inc_numero_radicado_v,
 	mot_desc_v,
 	mot_observacion_v, 
 	inc_ruta_incapacidad ,
 	inc_ruta_incapacidad_transcrita,
	set_descripcion_v,
	inc_estado,
	Date_format(inc_fecha_consiliacion_d,'$formato') as inc_fecha_consiliacion_d,   
	inc_condicion_id_i ,
	Date_format(inc_fecha_fecturacion_d,'$formato') as inc_fecha_fecturacion_d,  
	inc_numero_factura ,
	Date_format(inc_fecha_pago_factura,'$formato') as inc_fecha_pago_factura
 FROM 
 	gi_incapacidad 
	JOIN gi_empresa ON emp_id = inc_empresa 
	JOIN gi_empleados ON emd_id = inc_emd_id 
	JOIN gi_ips ip2 ON ip2.ips_id = emd_eps_id 
	LEFT JOIN gi_ips ip3 ON ip3.ips_id = emd_arl_id 
	LEFT JOIN gi_ips ip4 ON ip4.ips_id = emd_afp_id 
	LEFT JOIN gi_atencion ON atn_id = inc_atn_id 
	LEFT JOIN gi_usuario a ON a.user_id = inc_user_id_i 
	LEFT JOIN gi_usuario b ON b.user_id = inc_edi_user_id_i 
	LEFT JOIN gi_motivos_rechazo ON mot_id_i = inc_mot_rech_i 
	LEFT JOIN gi_origen on ori_id_i= inc_origen 
	LEFT JOIN gi_estado_tramite on est_tra_id_i= inc_estado_tramite
	LEFT join gi_clasificacion gc on gc.cla_id  = inc_clasificacion 
	LEFT join gi_documentos gd on gd.doc_id_i  = inc_tipo_generacion 
	LEFT join gi_estado_empleados gee on gee.est_emp_id_i  = emd_estado 
	LEFT JOIN gi_sub_estado_tramite sue on sue.set_id_i = inc_sub_estado_i
WHERE $item = :$item ";

				if($fechaInicial != null && $fechaFinal != null){
					if($fechaPago != null){
						$Lsql .= " AND inc_fecha_pago BETWEEN '".$fechaInicial."' AND '".$fechaFinal."' ";
					}else{
						$Lsql .= " AND inc_fecha_pago_nomina BETWEEN '".$fechaInicial."' AND '".$fechaFinal."' ";
					}
					
				}
				//echo "Esta es incompletaas =>".$incompletaas;	
				if(!is_null($incompletaas)){
					$Lsql .= " AND inc_estado = 0 ";
				}else{
					//$Lsql .= " AND inc_estado = 1 ";
				}

				//validaciones estado tramite RFB 300622
				if(!is_null($estadoTramite)){
					$Lsql .=" AND inc_estado_tramite = ".$estadoTramite;
				}
				
				$Lsql .= " ORDER BY emd_nombre ASC, inc_fecha_inicio ASC";
				//echo $Lsql;
				$stmt = Conexion::conectar()->prepare($Lsql);
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt->fetchAll(PDO::FETCH_ASSOC);

			}else{
				$Lsql = "
SELECT 
	inc_id, 
	emp_nombre,
	emd_codigo_nomina, 
	emd_cedula, 
	emd_nombre, 
	emd_cargo,
	emd_salario,
	Date_format(inc_fecha_inicio,'$formato') as inc_fecha_inicio, 
	Date_format(inc_fecha_final,'$formato') as inc_fecha_final,
 	(DATEDIFF(inc_fecha_final,inc_fecha_inicio) +1) as dias,
 	gc.cla_descripcion as inc_clasificacion ,
	inc_diagnostico, 
 	ori_descripcion_v as inc_origen,  	
 	inc_valor_pagado_empresa, 
 	inc_valor_pagado_eps, 
 	inc_valor_pagado_ajuste, 
 	Date_format(inc_fecha_pago_nomina,'$formato') as inc_fecha_pago_nomina, 
 	Date_format(inc_fecha_generada,'$formato') as inc_fecha_generada, 
 	est_tra_desc_i as inc_estado_tramite,
 	case 
 		when (inc_estado_tramite = 1) then  Date_format(inc_fecha_generada,'$formato')  
 		when (inc_estado_tramite = 2) then  Date_format(inc_fecha_generada,'$formato')  
 		when (inc_estado_tramite = 3) then  Date_format(inc_fecha_radicacion,'$formato')  
 		when (inc_estado_tramite = 4) then  Date_format(inc_fecha_solicitud,'$formato')  
 		when (inc_estado_tramite = 5) then  Date_format(inc_fecha_pago,'$formato')  
 		when (inc_estado_tramite = 6) then  Date_format(inc_fecha_negacion_,'$formato')  
 	end as inc_Fecha_estado, 
 	inc_valor,
 	Date_format(inc_fecha_pago,'$formato') as inc_fecha_pago, 
 	Date_format(inc_fecha_radicacion,'$formato') as inc_fecha_radicacion,
 	Date_format(inc_fecha_solicitud,'$formato') as inc_fecha_solicitud,
 	Date_format(inc_fecha_negacion_,'$formato') as inc_fecha_negacion_ ,
 	Date_format(inc_fecha_recepcion_d,'$formato') as inc_fecha_recepcion_d ,
 	gee.est_emp_desc_v,
 	ip2.ips_nombre as inc_ips_afiliado,
 	ip4.ips_nombre as inc_afp_afiliado ,
 	emd_sede,
 	emd_centro_costos, 
 	emd_subcentro_costos, 
 	emd_ciudad,
 	inc_profesional_responsable,
 	inc_prof_registro_medico,
 	inc_donde_se_genera,
 	gd.doc_descripcion_v as inc_tipo_generacion , 
 	gi_atencion.atn_descripcion,
 	a.user_nombre as creador,
 	b.user_nombre as editor ,
 	inc_numero_radicado_v,
 	mot_desc_v,
 	mot_observacion_v, 
 	inc_ruta_incapacidad ,
 	inc_ruta_incapacidad_transcrita,
 	set_descripcion_v,
 	inc_estado,
	Date_format(inc_fecha_consiliacion_d,'$formato') as inc_fecha_consiliacion_d,   
	inc_condicion_id_i ,
	Date_format(inc_fecha_fecturacion_d,'$formato') as inc_fecha_fecturacion_d,  
	inc_numero_factura ,
	Date_format(inc_fecha_pago_factura,'$formato') as inc_fecha_pago_factura
 FROM 
 	gi_incapacidad 
	JOIN gi_empresa ON emp_id = inc_empresa 
	JOIN gi_empleados ON emd_id = inc_emd_id 
	JOIN gi_ips ip2 ON ip2.ips_id = emd_eps_id 
	LEFT JOIN gi_ips ip3 ON ip3.ips_id = emd_arl_id 
	LEFT JOIN gi_ips ip4 ON ip4.ips_id = emd_afp_id 
	LEFT JOIN gi_atencion ON atn_id = inc_atn_id 
	LEFT JOIN gi_usuario a ON a.user_id = inc_user_id_i 
	LEFT JOIN gi_usuario b ON b.user_id = inc_edi_user_id_i 
	LEFT JOIN gi_motivos_rechazo ON mot_id_i = inc_mot_rech_i 
	LEFT JOIN gi_origen on ori_id_i= inc_origen 
	LEFT JOIN gi_estado_tramite on est_tra_id_i= inc_estado_tramite
	LEFT join gi_clasificacion gc on gc.cla_id  = inc_clasificacion 
	LEFT join gi_documentos gd on gd.doc_id_i  = inc_tipo_generacion 
	LEFT join gi_estado_empleados gee  on gee.est_emp_id_i  = emd_estado 
	LEFT JOIN gi_sub_estado_tramite sue on sue.set_id_i = inc_sub_estado_i
WHERE $item = :$item ";
				if($fechaInicial != null && $fechaFinal != null){
					$Lsql .= " AND inc_fecha_pago_nomina BETWEEN '".$fechaInicial."' AND '".$fechaFinal."' ";
				}
				if(!is_null($incompletaas)){
					$Lsql .= " AND inc_estado = 0 ";
				}else{
					$Lsql .= " AND inc_estado = 1 ";
				}

				//validaciones estado tramite RFB 300622
				if(!is_null($estadoTramite)){
					$Lsql .=" AND inc_estado_tramite = ".$estadoTramite;
				}
				$Lsql .= "ORDER BY emd_nombre ASC, inc_fecha_inicio ASC";
				$stmt = Conexion::conectar()->prepare($Lsql);
				$stmt -> execute();
				return $stmt->fetchAll(PDO::FETCH_ASSOC);
			}
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlMostrarConsolidado($tabla, $item, $valor, $valor2 = NULL){
			
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT emp_nombre, emd_cedula, emd_nombre, inc_diagnostico, inc_origen, inc_clasificacion, inc_fecha_inicio, inc_fecha_final, inc_tipo_generacion, inc_profesional_responsable, inc_prof_registro_medico, inc_donde_se_genera, inc_fecha_generada, inc_observacion, est_tra_desc_i as  inc_estado_tramite, inc_fecha_radicacion, inc_fecha_solicitud, inc_fecha_pago, inc_valor, inc_fecha_pago_nomina, inc_valor_pagado_nomina, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado ,  ip4.ips_nombre as inc_afp_afiliado FROM $tabla LEFT JOIN gi_ips ip2 ON ip2.ips_id = inc_ips_afiliado LEFT JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id JOIN gi_estado_tramite on est_tra_id_i = inc_estado_tramite WHERE inc_empresa = :inc_empresa AND inc_fecha_pago_nomina BETWEEN :valor1 AND :valor2 ORDER BY inc_id DESC");
				$stmt -> bindParam(":inc_empresa", $item, PDO::PARAM_STR);
				$stmt -> bindParam(":valor1", $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":valor2", $valor2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();

			}else{
				$stmt = Conexion::conectar()->prepare("SELECT emp_nombre, emd_cedula, emd_nombre, inc_diagnostico, inc_origen, inc_clasificacion, inc_fecha_inicio, inc_fecha_final, inc_tipo_generacion, inc_profesional_responsable, inc_prof_registro_medico, inc_donde_se_genera, inc_fecha_generada, inc_observacion, inc_estado_tramite, inc_fecha_radicacion, inc_fecha_solicitud, inc_fecha_pago, inc_valor, inc_fecha_pago_nomina, inc_valor_pagado_nomina, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado ,  ip4.ips_nombre as inc_afp_afiliado FROM $tabla LEFT JOIN gi_ips ip2 ON ip2.ips_id = inc_ips_afiliado LEFT JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id WHERE inc_fecha_pago_nomina BETWEEN :valor1 AND :valor2");
				$stmt -> bindParam(":valor1", $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":valor2", $valor2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}


		static public function mdlMostrarConsolidadoEstados($tabla, $item, $valor3, $valor=null, $valor2 = NULL ){
			
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT emp_nombre, emd_cedula, emd_nombre, inc_diagnostico, inc_origen, inc_clasificacion, inc_fecha_inicio, inc_fecha_final, inc_tipo_generacion, inc_profesional_responsable, inc_prof_registro_medico, inc_donde_se_genera, inc_fecha_generada, inc_observacion, inc_estado_tramite, inc_fecha_radicacion, inc_fecha_solicitud, inc_fecha_pago, inc_valor, inc_fecha_pago_nomina, inc_valor_pagado_nomina, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado ,  ip4.ips_nombre as inc_afp_afiliado , inc_fecha_negacion_ FROM $tabla LEFT JOIN gi_ips as ip2 ON ip2.ips_id = inc_ips_afiliado LEFT JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id WHERE inc_empresa = :inc_empresa AND inc_estado_tramite = :inc_estado_tramite  AND inc_fecha_inicio BETWEEN :valor1 AND :valor2");
				$stmt -> bindParam(":inc_empresa", $item, PDO::PARAM_INT);
				$stmt -> bindParam(":valor1", $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":valor2", $valor2, PDO::PARAM_STR);
				$stmt -> bindParam(":inc_estado_tramite", $valor3, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();

			}else{
				$stmt = Conexion::conectar()->prepare("SELECT emp_nombre, emd_cedula, emd_nombre, inc_diagnostico, inc_origen, inc_clasificacion, inc_fecha_inicio, inc_fecha_final, inc_tipo_generacion, inc_profesional_responsable, inc_prof_registro_medico, inc_donde_se_genera, inc_fecha_generada, inc_observacion, inc_estado_tramite, inc_fecha_radicacion, inc_fecha_solicitud, inc_fecha_pago, inc_valor, inc_fecha_pago_nomina, inc_valor_pagado_nomina, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado ,  ip4.ips_nombre as inc_afp_afiliado, inc_fecha_negacion_ FROM $tabla LEFT JOIN gi_ips ip2 ON ip2.ips_id = inc_ips_afiliado LEFT JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id WHERE inc_estado_tramite = :inc_estado_tramite AND  inc_fecha_inicio BETWEEN :valor1 AND :valor2");
				$stmt -> bindParam(":valor1", $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":valor2", $valor2, PDO::PARAM_STR);
				$stmt -> bindParam(":inc_estado_tramite", $valor3, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}

		/*=============================================
				Mostrar para edicion
		=============================================*/

		static public function mdlMostrarIncapacidades_edicion($tabla, $item, $valor){
			
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT inc.*, emd.*, gi_empresa.emp_nombre, b.ips_nombre as eps, c.ips_nombre as arl, est.est_emp_desc_v, dia.dia_codigo, dia.dia_descripcion, m.med_id, m.med_nombre, m.med_num_registro, m.med_estado_rethus, ent.enat_id_i, ent.enat_nombre_v, ent.enat_nit_v, usu.user_nombre FROM $tabla as inc JOIN gi_empresa ON emp_id = inc.inc_empresa JOIN gi_empleados emd ON emd.emd_id = inc.inc_emd_id LEFT JOIN gi_diagnostico dia ON dia_codigo = inc.inc_diagnostico LEFT JOIN gi_medicos m ON med_id = inc.inc_profesional_responsable LEFT JOIN gi_entidad_atiende ent ON enat_id_i = inc.inc_donde_se_genera LEFT JOIN gi_ips b ON b.ips_id = emd.emd_eps_id LEFT JOIN gi_ips c ON c.ips_id = emd.emd_arl_id JOIN gi_estado_empleados est ON est.est_emp_id_i = emd.emd_estado LEFT JOIN gi_usuario usu ON inc.inc_user_id_i = usu.user_id  WHERE $item = :$item ORDER BY inc_id DESC");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();

			}
			$stmt -> close();
			$stmt = null;
		}

		/*=============================================
				Registro de Incapaciodades
		=============================================*/
		static public function mdlIngresarIncapacidad($tabla, $datos){
			$pdo  = Conexion::conectar();
			date_default_timezone_set('America/Bogota');

			$stmt = $pdo->prepare("INSERT INTO $tabla(inc_ips_atiende,	inc_ips_afiliado,inc_afp_afiliado,inc_arl_afiliado,	inc_cc_afiliado, inc_diagnostico, inc_origen, inc_clasificacion, inc_fecha_inicio, inc_fecha_final,	inc_tipo_generacion, inc_profesional_responsable, inc_donde_se_genera, inc_pertenece_red, inc_estado, inc_observacion, inc_empresa, inc_emd_id, inc_estado_tramite, inc_valor , inc_fecha_pago, inc_ruta_incapacidad, inc_ruta_incapacidad_transcrita, inc_fecha_pago_nomina, inc_valor_solicitado, inc_valor_pagado_empresa, inc_valor_pagado_eps, inc_valor_pagado_ajuste, inc_ivl_v, inc_atn_id, inc_user_id_i, inc_fecha_radicacion, inc_fecha_solicitud,inc_fecha_negacion_, inc_observacion_neg, inc_numero_radicado_v, inc_mot_rech_i, inc_observacion_rech_t, inc_fecha_recepcion_d, inc_fecha_generada, inc_sub_estado_i, inc_registrada_entidad_i) VALUES (:inc_ips_atiende,	:inc_ips_afiliado,:inc_afp_afiliado,:inc_arl_afiliado, :inc_cc_afiliado,  :inc_diagnostico, :inc_origen,	:inc_clasificacion,	:inc_fecha_inicio,	:inc_fecha_final,	:inc_tipo_generacion,	:inc_profesional_responsable,	:inc_donde_se_genera, :inc_pertenece_red, :inc_estado, :inc_observacion, :inc_empresa, :inc_emd_id, :inc_estado_tramite, :inc_valor , :inc_fecha_pago, :inc_ruta_incapacidad, :inc_ruta_incapacidad_transcrita, :inc_fecha_pago_nomina, :inc_valor_solicitado , :inc_valor_pagado_empresa, :inc_valor_pagado_eps, :inc_valor_pagado_ajuste, :inc_ivl_v, :inc_atn_id, ".$_SESSION['id_Usuario'].", :inc_fecha_radicacion, :inc_fecha_solicitud,:inc_fecha_negacion_, :inc_observacion_neg, :inc_numero_radicado_v, :inc_mot_rech_i, :inc_observacion_rech_t, :inc_fecha_recepcion_d, '".date('Y-m-d H-i-s')."', :inc_sub_estado_i, :inc_registrada_entidad_i)");

			$stmt->bindParam(":inc_ips_atiende", 	$datos["inc_ips_atiende"], 	PDO::PARAM_INT);
			$stmt->bindParam(":inc_ips_afiliado", 	$datos["inc_ips_afiliado"], PDO::PARAM_INT);
			$stmt->bindParam(":inc_afp_afiliado", 	$datos["inc_afp_afiliado"], PDO::PARAM_INT);
			$stmt->bindParam(":inc_arl_afiliado", 	$datos["inc_arl_afiliado"], PDO::PARAM_INT);
			$stmt->bindParam(":inc_cc_afiliado", 	$datos["inc_cc_afiliado"],	 PDO::PARAM_STR);
			$stmt->bindParam(":inc_diagnostico", 	$datos["inc_diagnostico"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_origen", 		$datos["inc_origen"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_clasificacion",	$datos["inc_clasificacion"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_inicio", 	$datos["inc_fecha_inicio"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_final", 	$datos["inc_fecha_final"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_tipo_generacion",	$datos["inc_tipo_generacion"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_profesional_responsable",	$datos["inc_profesional_responsable"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_donde_se_genera", 	$datos["inc_donde_se_genera"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_pertenece_red", 	$datos["inc_pertenece_red"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_estado", 			$datos["inc_estado"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_observacion", 		$datos["inc_observacion"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_empresa", 			$datos["inc_empresa"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_emd_id", 			$datos["inc_emd_id"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_estado_tramite", 	$datos["inc_estado_tramite"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_ruta_incapacidad", 	$datos["inc_ruta_incapacidad"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_ruta_incapacidad_transcrita", $datos["inc_ruta_incapacidad_transcrita"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_ruta_incapacidad", 	$datos["inc_ruta_incapacidad"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_ruta_incapacidad_transcrita", $datos["inc_ruta_incapacidad_transcrita"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor", 	$datos["inc_valor"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_pago", $datos["inc_fecha_pago"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_pago_nomina", $datos["inc_fecha_pago_nomina"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_solicitado", $datos["inc_valor_solicitado"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_pagado_empresa", $datos["inc_valor_pagado_empresa"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_pagado_eps", $datos["inc_valor_pagado_eps"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_pagado_ajuste", $datos["inc_valor_pagado_ajuste"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_ivl_v", $datos["inc_ivl_v"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_atn_id", $datos["inc_atn_id"], PDO::PARAM_STR);

			$stmt->bindParam(":inc_fecha_radicacion", 	$datos["inc_fecha_radicacion"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_solicitud", 	$datos["inc_fecha_solicitud"],	 PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_negacion_", 	$datos["inc_fecha_negacion_"],	 PDO::PARAM_STR);
			$stmt->bindParam(":inc_observacion_neg", 	$datos['inc_observacion_neg'], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_numero_radicado_v", 	$datos['inc_numero_radicado_v'], 	PDO::PARAM_STR);

			$stmt->bindParam(":inc_mot_rech_i",  $datos['inc_mot_rech_i'], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_observacion_rech_t", $datos['inc_observacion_rech_t'], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_recepcion_d", $datos['inc_fecha_recepcion_d'], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_sub_estado_i", $datos['inc_sub_estado_i'], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_registrada_entidad_i", $datos['inc_registrada_entidad_i'], 	PDO::PARAM_STR);



			if($stmt->execute()){
				return $pdo->lastInsertId();
				//return 'OK';
			}else{
				print_r($stmt->errorInfo());
				return 'Error';
			}
			$stmt->close();
			$stmt = null;

		}

		/*=============================================
				Editar datos de las Incapaciodades
		=============================================*/
		static public function mdlEditarIncapacidad($tabla, $datos){
			date_default_timezone_set('America/Bogota');
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET inc_ips_atiende = :inc_ips_atiende,inc_ips_afiliado = :inc_ips_afiliado,inc_afp_afiliado = :inc_afp_afiliado,inc_arl_afiliado = :inc_arl_afiliado,inc_cc_afiliado = :inc_cc_afiliado,	inc_diagnostico = :inc_diagnostico,	inc_origen = :inc_origen, inc_clasificacion = :inc_clasificacion,	inc_fecha_inicio = :inc_fecha_inicio,	inc_fecha_final = :inc_fecha_final,	inc_tipo_generacion = :inc_tipo_generacion,	inc_profesional_responsable = :inc_profesional_responsable,	inc_donde_se_genera = :inc_donde_se_genera, inc_pertenece_red = :inc_pertenece_red, inc_estado = :inc_estado, inc_observacion = :inc_observacion, inc_empresa = :inc_empresa, inc_emd_id = :inc_emd_id , inc_ruta_incapacidad= :inc_ruta_incapacidad , inc_ruta_incapacidad_transcrita = :inc_ruta_incapacidad_transcrita , inc_fecha_pago_nomina = :inc_fecha_pago_nomina , inc_valor_solicitado = :inc_valor_solicitado, inc_valor_pagado_empresa = :inc_valor_pagado_empresa, inc_valor_pagado_eps = :inc_valor_pagado_eps, inc_valor_pagado_ajuste = :inc_valor_pagado_ajuste, inc_ivl_v = :inc_ivl_v, inc_atn_id = :inc_atn_id, inc_edi_user_id_i = ".$_SESSION['id_Usuario']." , inc_valor = :inc_valor, inc_fecha_pago = :inc_fecha_pago, inc_estado_tramite = :inc_estado_tramite, inc_fecha_radicacion = :inc_fecha_radicacion, inc_fecha_solicitud = :inc_fecha_solicitud, inc_observacion = :inc_observacion, inc_fecha_negacion_ = :inc_fecha_negacion_ , inc_observacion_neg = :inc_observacion_neg, inc_valor_solicitado = :inc_valor_solicitado, inc_numero_radicado_v = :inc_numero_radicado_v, inc_mot_rech_i = :inc_mot_rech_i, inc_observacion_rech_t = :inc_observacion_rech_t, inc_fecha_recepcion_d = :inc_fecha_recepcion_d, inc_fecha_edicion = '".date('Y-m-d H-i-s')."', inc_sub_estado_i = :inc_sub_estado_i, inc_registrada_entidad_i = :inc_registrada_entidad_i WHERE inc_id = :inc_id ");

			$stmt->bindParam(":inc_ips_atiende", 		$datos["inc_ips_atiende"], 	PDO::PARAM_INT);
			$stmt->bindParam(":inc_ips_afiliado", 		$datos["inc_ips_afiliado"], PDO::PARAM_INT);
			$stmt->bindParam(":inc_afp_afiliado", 		$datos["inc_afp_afiliado"], PDO::PARAM_INT);
			$stmt->bindParam(":inc_arl_afiliado", 		$datos["inc_arl_afiliado"], PDO::PARAM_INT);
			$stmt->bindParam(":inc_cc_afiliado", 		$datos["inc_cc_afiliado"],	 PDO::PARAM_STR);
			$stmt->bindParam(":inc_diagnostico", 		$datos["inc_diagnostico"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_origen", 			$datos["inc_origen"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_clasificacion",		$datos["inc_clasificacion"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_inicio", 		$datos["inc_fecha_inicio"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_final", 		$datos["inc_fecha_final"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_tipo_generacion",	$datos["inc_tipo_generacion"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_profesional_responsable",	$datos["inc_profesional_responsable"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_donde_se_genera", 	$datos["inc_donde_se_genera"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_pertenece_red", 	$datos["inc_pertenece_red"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_estado", 			$datos["inc_estado"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_observacion", 		$datos["inc_observacion"], PDO::PARAM_STR);
			
			$stmt->bindParam(":inc_empresa", 			$datos["inc_empresa"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_emd_id", 			$datos["inc_emd_id"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_ruta_incapacidad", 	$datos["inc_ruta_incapacidad"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_ruta_incapacidad_transcrita", $datos["inc_ruta_incapacidad_transcrita"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_pago_nomina",   $datos["inc_fecha_pago_nomina"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_id", 				 $datos["inc_id"], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_solicitado", $datos["inc_valor_solicitado"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_pagado_empresa", $datos["inc_valor_pagado_empresa"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_pagado_eps", $datos["inc_valor_pagado_eps"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_pagado_ajuste", $datos["inc_valor_pagado_ajuste"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_ivl_v", $datos["inc_ivl_v"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_atn_id", $datos["inc_atn_id"], PDO::PARAM_STR);
			
			$stmt->bindParam(":inc_valor", 			$datos["inc_valor"], 	PDO::PARAM_INT);
			$stmt->bindParam(":inc_fecha_pago", 	$datos["inc_fecha_pago"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_estado_tramite", $datos["inc_estado_tramite"],	 PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_radicacion", 	$datos["inc_fecha_radicacion"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_solicitud", 	$datos["inc_fecha_solicitud"],	 PDO::PARAM_STR);
			$stmt->bindParam(":inc_observacion", 		$datos['inc_observacion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_negacion_", 	$datos["inc_fecha_negacion_"],	 PDO::PARAM_STR);
			$stmt->bindParam(":inc_observacion_neg", 	$datos['inc_observacion_neg'], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_solicitado", 	$datos['inc_valor_solicitado'], 	PDO::PARAM_STR);

			$stmt->bindParam(":inc_numero_radicado_v", 	$datos['inc_numero_radicado_v'], 	PDO::PARAM_STR);

			$stmt->bindParam(":inc_mot_rech_i",  $datos['inc_mot_rech_i'], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_observacion_rech_t", $datos['inc_observacion_rech_t'], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_recepcion_d", $datos['inc_fecha_recepcion_d'], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_sub_estado_i", $datos['inc_sub_estado_i'], 	PDO::PARAM_STR);
			$stmt->bindParam(":inc_registrada_entidad_i", $datos['inc_registrada_entidad_i'], 	PDO::PARAM_STR);
			if($stmt->execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();
			}
			$stmt->close();
			$stmt = null;

		}


		/*=============================================
				Eliminar datos de las Incapaciodades
		=============================================*/
		static public function mdlBorrarIncapacidades($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE inc_id = :inc_id");
			$stmt -> bindParam(":inc_id", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}


		/*=============================================
				Editar datos Economicos de las Incapaciodades
		=============================================*/
		static function mdlAgregarDatosEconomicos($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET inc_valor = :inc_valor, inc_fecha_pago = :inc_fecha_pago, inc_estado_tramite = :inc_estado_tramite, inc_fecha_radicacion = :inc_fecha_radicacion, inc_fecha_solicitud = :inc_fecha_solicitud, inc_observacion = :inc_observacion, inc_fecha_negacion_ = :inc_fecha_negacion_ , inc_observacion_neg = :inc_observacion_neg, inc_valor_solicitado = :inc_valor_solicitado , inc_edi_user_id_i = ".$_SESSION['id_Usuario'].", inc_fecha_pago_nomina = :inc_fecha_pago_nomina, inc_valor_pagado_empresa = :inc_valor_pagado_empresa, inc_valor_pagado_eps = :inc_valor_pagado_eps, inc_numero_radicado_v = :inc_numero_radicado_v  WHERE inc_id = :inc_id");

			$stmt->bindParam(":inc_valor", 	$datos["inc_valor"], 	PDO::PARAM_INT);
			$stmt->bindParam(":inc_fecha_pago", $datos["inc_fecha_pago"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_estado_tramite", $datos["inc_estado_tramite"],	 PDO::PARAM_STR);
			$stmt->bindParam(":inc_id", $datos['inc_id'], 	PDO::PARAM_INT);
			$stmt->bindParam(":inc_fecha_radicacion", $datos["inc_fecha_radicacion"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_solicitud", $datos["inc_fecha_solicitud"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_observacion", 	$datos['inc_observacion'], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_negacion_", $datos["inc_fecha_negacion_"], PDO::PARAM_STR);
			$stmt->bindParam(":inc_observacion_neg", $datos['inc_observacion_neg'], PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_solicitado", $datos['inc_valor_solicitado'], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_pago_nomina", $datos['inc_fecha_pago_nomina'], PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_pagado_eps", $datos['inc_valor_pagado_eps'], PDO::PARAM_STR);
			$stmt->bindParam(":inc_valor_pagado_empresa", $datos['inc_valor_pagado_empresa'], PDO::PARAM_STR);
			$stmt->bindParam(":inc_numero_radicado_v", $datos['inc_numero_radicado_v'], PDO::PARAM_STR);
			

			if($stmt->execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();
			}
			$stmt->close();
			$stmt = null;
		}


		/*=============================================
				Editar datos Facturacion de las Incapaciodades
		=============================================*/
		static function mdlAgregarDatosFacturacion($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET inc_numero_factura = :inc_numero_factura,  inc_fecha_emision_factura = :inc_fecha_emision_factura, inc_fecha_pago_factura  = :inc_fecha_pago_factura, inc_edi_user_id_i = ".$_SESSION['id_Usuario']." WHERE inc_empresa = :empresa AND inc_fecha_pago BETWEEN :fecha1 AND :fecha2");

			$stmt->bindParam(":inc_numero_factura", 		$datos["inc_numero_factura"], 			PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_emision_factura",  $datos["inc_fecha_emision_factura"],	PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_pago_factura", 	$datos["inc_fecha_pago_factura"], 		PDO::PARAM_STR);

			$stmt->bindParam(":fecha1", 	$datos["fecha1"],	PDO::PARAM_STR);
			$stmt->bindParam(":fecha2", 	$datos['fecha2'], 	PDO::PARAM_STR);
			$stmt->bindParam(":empresa", 	$datos['empresa'], 	PDO::PARAM_STR);

			if($stmt->execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();
			}
			$stmt->close();
			$stmt = null;
		}

		static function mdlAgregarDatosFacturacionT($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET inc_numero_factura = :inc_numero_factura,  inc_fecha_emision_factura = :inc_fecha_emision_factura, inc_fecha_pago_factura  = :inc_fecha_pago_factura, inc_edi_user_id_i = ".$_SESSION['id_Usuario']." WHERE  inc_fecha_pago BETWEEN :fecha1 AND :fecha2");

			$stmt->bindParam(":inc_numero_factura", 		$datos["inc_numero_factura"], 			PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_emision_factura",  $datos["inc_fecha_emision_factura"],	PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_pago_factura", 	$datos["inc_fecha_pago_factura"], 		PDO::PARAM_STR);

			$stmt->bindParam(":fecha1", 	$datos["fecha1"],	PDO::PARAM_STR);
			$stmt->bindParam(":fecha2", 	$datos['fecha2'], 	PDO::PARAM_STR);

			if($stmt->execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();
			}
			$stmt->close();
			$stmt = null;
		}

		static function mdlAgregarDatosFacturacionById($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET inc_numero_factura = :inc_numero_factura,  inc_fecha_emision_factura = :inc_fecha_emision_factura, inc_fecha_pago_factura  = :inc_fecha_pago_factura, inc_edi_user_id_i = ".$_SESSION['id_Usuario'].", inc_fecha_consiliacion_d =:inc_fecha_consiliacion_d, inc_fecha_fecturacion_d = :inc_fecha_fecturacion_d, inc_condicion_id_i = :inc_condicion_id_i WHERE inc_id = :incapacidadesID");

			$stmt->bindParam(":inc_numero_factura", 		$datos["inc_numero_factura"], 			PDO::PARAM_STR);
			/*$stmt->bindParam(":inc_fecha_emision_factura",  $datos["inc_fecha_emision_factura"],	PDO::PARAM_STR);*/
			$stmt->bindParam(":inc_fecha_pago_factura",$datos["inc_fecha_pago_factura"], PDO::PARAM_STR);
			$stmt->bindParam(":incapacidadesID",$datos['incapacidadesID'], PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_consiliacion_d", $datos['inc_fecha_consiliacion_d'],PDO::PARAM_STR);
			$stmt->bindParam(":inc_fecha_fecturacion_d", $datos['inc_fecha_fecturacion_d'],PDO::PARAM_STR);
			$stmt->bindParam(":inc_condicion_id_i", $datos['inc_condicion_id_i'], 	PDO::PARAM_STR);

			if($stmt->execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();
			}
			$stmt->close();
			$stmt = null;
		}
		


		/* traer informacion por estados Chart */
		static public function mdlVerEstadosChart( $item, $valor){
			if(!is_null($item)){
				$stmt = Conexion::conectar()->prepare("SELECT count(inc_estado_tramite) as estado , inc_estado_tramite FROM gi_incapacidad  WHERE $item = :$item GROUP BY inc_estado_tramite");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT count(inc_estado_tramite) as estado , inc_estado_tramite FROM gi_incapacidad GROUP BY inc_estado_tramite");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			
			$stmt -> close();
			$stmt = null;
		}


		/* traer informacion por estados Chart */
		static public function mdlVerEstadosChart_w( $item, $valor){
			if(!is_null($item)){
				$stmt = Conexion::conectar()->prepare("SELECT count(inc_estado_tramite) as estado  FROM gi_incapacidad  WHERE $item = :$item ");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT count(*) as estado FROM gi_incapacidad");
				$stmt -> execute();
				return $stmt -> fetch();
			}
			
			$stmt -> close();
			$stmt = null;
		}



		static public function mdlVerEstadosChart_c( $item, $valor, $item2 = null, $valor2 = null){
			if(!is_null($item2)){
				$stmt = Conexion::conectar()->prepare("SELECT count(inc_estado_tramite) as estado  FROM gi_incapacidad  WHERE $item = :$item  AND $item2 = :$item2 ");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT count(*) as estado FROM gi_incapacidad WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);	 
				$stmt -> execute();
				return $stmt -> fetch();
			}
			
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlVerEstadosChart_cx(){
			
			$stmt = Conexion::conectar()->prepare("SELECT count(*) as estado FROM gi_incapacidad"); 
			$stmt -> execute();
			return $stmt -> fetch();
			
			
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlVerEstadovalorPagado_e( $item, $valor, $item2 = null, $valor2 = null){
			if(!is_null($item2)){
				$stmt = Conexion::conectar()->prepare("SELECT sum(inc_valor_pagado_nomina) as total  FROM gi_incapacidad  WHERE $item = :$item AND $item2 = :$item2 ");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT sum(inc_valor_pagado_nomina) as total FROM gi_incapacidad WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);	 
				$stmt -> execute();
				return $stmt -> fetch();
			}
			
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlVerEstadovalorPagado( $item, $valor){
			if(!is_null($item)){
				$stmt = Conexion::conectar()->prepare("SELECT sum(inc_valor_pagado_nomina) as total  FROM gi_incapacidad  WHERE $item = :$item ");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT sum(inc_valor_pagado_nomina) as total FROM gi_incapacidad");
				$stmt -> execute();
				return $stmt -> fetch();
			}
			
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlVerEstadovalorPagadoR( $item, $valor){
			if(!is_null($item)){
				$stmt = Conexion::conectar()->prepare("SELECT sum(inc_valor) as total  FROM gi_incapacidad  WHERE $item = :$item ");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT sum(inc_valor) as total FROM gi_incapacidad");
				$stmt -> execute();
				return $stmt -> fetch();
			}
			
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlVerEstadovalorPagadoRC( $item, $valor, $item2, $valor2){
			
			$stmt = Conexion::conectar()->prepare("SELECT sum(inc_valor) as total  FROM gi_incapacidad  WHERE $item = :$item AND $item2 = :$item2");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetch();
			
			
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlVerEstados_Originales_Chart_c( $item, $valor){
			if(!is_null($item)){
				$stmt = Conexion::conectar()->prepare("SELECT count(inc_origen) as estado , inc_origen FROM gi_incapacidad  WHERE $item = :$item GROUP BY inc_origen");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT count(inc_origen) as estado , inc_origen FROM gi_incapacidad GROUP BY inc_origen");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			
			$stmt -> close();
			$stmt = null;
		}

		/* otener las incapacidades de 15 dias de generacion */
		static public function mdlVerIncapacidades15Dias($item1, $valor, $item2, $valor2){
			$stmt = Conexion::conectar()->prepare("SELECT count(inc_estado_tramite) as estado  FROM 15dias  WHERE $item1 = :$item1 AND $item2 = :$item2");
			$stmt -> bindParam(":".$item1, $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetch();
			$stmt -> close();
			$stmt = null;
		}

		/* otener las incapacidades de 30 dias de generacion */
		static public function mdlVerIncapacidades30Dias($item1, $valor, $item2, $valor2){
			$stmt = Conexion::conectar()->prepare("SELECT count(inc_estado_tramite) as estado  FROM 30dias  WHERE $item1 = :$item1 AND $item2 = :$item2");
			$stmt -> bindParam(":".$item1, $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetch();
			$stmt -> close();
			$stmt = null;
		}

		/* otener las incapacidades de 30 dias de generacion */
		static public function mdlVerIncapacidades45Dias($item1, $valor, $item2, $valor2){
			$stmt = Conexion::conectar()->prepare("SELECT count(inc_estado_tramite) as estado  FROM 45dias  WHERE $item1 = :$item1 AND $item2 = :$item2");
			$stmt -> bindParam(":".$item1, $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetch();
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlVerIncapacidadesMax($item1, $valor, $item2, $valor2){
			$stmt = Conexion::conectar()->prepare("SELECT count(inc_estado_tramite) as estado , SUM(inc_valor) as total FROM gi_incapacidad  WHERE $item1 = :$item1 AND $item2 = :$item2");
			$stmt -> bindParam(":".$item1, $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetch();
			$stmt -> close();
			$stmt = null;
		}

		/* para validar si existe la incapaciodad o algo por el estilo */
		static public function mdlValidarIncapacidades($item1, $valor, $item2, $valor2, $empresa){
			$stmt = Conexion::conectar()->prepare("SELECT * FROM gi_incapacidad  WHERE $item1 = :$item1 AND $item2 = :$item2 AND inc_empresa = :empresa LIMIT 1");
			$stmt -> bindParam(":".$item1, $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
			$stmt -> bindParam(":empresa", $empresa, PDO::PARAM_STR);
			$stmt -> execute();
			$data_exists = ($stmt->fetchColumn() > 0) ? true : false;
			return $data_exists;
			$stmt -> close();
			$stmt = null;
		}	

		static public function mdlVerIdIncapacidades($item1, $valor, $item2, $valor2, $empresa ){
			$stmt = Conexion::conectar()->prepare("SELECT inc_id FROM gi_incapacidad  WHERE $item1 = :$item1 AND $item2 = :$item2 AND inc_empresa = ".$empresa." LIMIT 1");
			$stmt -> bindParam(":".$item1, $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt->fetch();
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlVerValoresIncapacidades($item1, $valor, $item2, $valor2){
			$stmt = Conexion::conectar()->prepare("SELECT inc_valor_pagado_empresa, inc_valor_pagado_eps, inc_fecha_pago_nomina, inc_estado_tramite, inc_valor, inc_valor_solicitado, inc_fecha_pago, inc_fecha_solicitud, inc_fecha_radicacion, inc_fecha_negacion_, inc_observacion, inc_observacion_neg, inc_numero_radicado_v FROM gi_incapacidad  WHERE $item1 = :$item1 AND $item2 = :$item2 LIMIT 1");
			$stmt -> bindParam(":".$item1, $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt->fetch();

		}

		/* Obtener los valores de los ultimos seis meses*/
		static public function mdlValoresIncapacidadesx6($item, $valor, $anho=null){
			$fechaFinal = $anho."-12-31";
			$fechaInici = $anho."-01-01";

			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT sum(inc_valor) as valor, MONTH(inc_fecha_pago) AS MES FROM gi_incapacidad WHERE  $item = :$item AND inc_fecha_pago BETWEEN :fecha1 AND :fecha2 GROUP BY MONTH(inc_fecha_pago) ORDER BY MONTH(inc_fecha_pago) ASC ;");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha1", $fechaInici, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fechaFinal, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT sum(inc_valor)  as valor, MONTH(inc_fecha_pago) AS MES FROM gi_incapacidad WHERE inc_fecha_pago BETWEEN :fecha1 AND :fecha2 GROUP BY MONTH(inc_fecha_pago) ORDER BY MONTH(inc_fecha_pago) ASC;");
				$stmt -> bindParam(":fecha1", $fechaInici, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fechaFinal, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
		}

		static public function mdlValoresIncapacidadesx62($item, $valor, $fechaInici=null, $fechaFinal = null){

			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT sum(inc_valor) as valor, MONTH(inc_fecha_pago) AS MES , YEAR(inc_fecha_pago) AS ANO FROM gi_incapacidad WHERE  $item = :$item AND inc_fecha_pago BETWEEN :fecha1 AND :fecha2 GROUP BY MONTH(inc_fecha_pago), YEAR(inc_fecha_pago) ORDER BY YEAR(inc_fecha_pago) , MONTH(inc_fecha_pago) ASC ;");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha1", $fechaInici, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fechaFinal, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT sum(inc_valor)  as valor, MONTH(inc_fecha_pago) AS MES , YEAR(inc_fecha_pago) AS ANO FROM gi_incapacidad WHERE inc_fecha_pago BETWEEN :fecha1 AND :fecha2 GROUP BY MONTH(inc_fecha_pago) , YEAR(inc_fecha_pago) ORDER BY YEAR(inc_fecha_pago), MONTH(inc_fecha_pago) ASC;");
				$stmt -> bindParam(":fecha1", $fechaInici, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fechaFinal, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
		}


		/* Obtener los 10 eempleados mas incapacitados */
		static public function mdlGetTopTenIncapacitados($item, $valor, $fecha1, $fecha2){

			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT COUNT(inc_cc_afiliado) as total, inc_cc_afiliado, SUM(DATEDIFF(inc_fecha_final,inc_fecha_inicio))  as total_dias FROM gi_incapacidad WHERE $item = :$item  AND inc_fecha_inicio BETWEEN :fecha1 AND :fecha2  AND inc_origen != 'LICENCIA DE MATERNIDAD' GROUP BY inc_cc_afiliado ORDER BY total_dias DESC LIMIT 10;");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT COUNT(inc_cc_afiliado) as total, inc_cc_afiliado, SUM(DATEDIFF(inc_fecha_final,inc_fecha_inicio))  as total_dias FROM gi_incapacidad WHERE inc_fecha_inicio BETWEEN :fecha1 AND :fecha2 AND inc_origen != 'LICENCIA DE MATERNIDAD' GROUP BY inc_cc_afiliado ORDER BY total_dias DESC LIMIT 10;");
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}
		}

		/* Obtener los 10 eempleados mas incapacitados de materenidad */
		static public function mdlGetTopTenIncapacitadosByMaternidad($item, $valor, $fecha1, $fecha2){

			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT COUNT(inc_cc_afiliado) as total, inc_cc_afiliado, SUM(DATEDIFF(inc_fecha_final,inc_fecha_inicio))  as total_dias FROM gi_incapacidad WHERE $item = :$item  AND inc_fecha_inicio BETWEEN :fecha1 AND :fecha2  AND inc_origen = 'LICENCIA DE MATERNIDAD' GROUP BY inc_cc_afiliado ORDER BY total_dias DESC LIMIT 10;");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT COUNT(inc_cc_afiliado) as total, inc_cc_afiliado, SUM(DATEDIFF(inc_fecha_final,inc_fecha_inicio))  as total_dias FROM gi_incapacidad WHERE inc_fecha_inicio BETWEEN :fecha1 AND :fecha2 AND inc_origen = 'LICENCIA DE MATERNIDAD' GROUP BY inc_cc_afiliado ORDER BY total_dias DESC LIMIT 10;");
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}
		}

		/* Obtener los 10 eempleados mas incapacitados */
		static public function mdlGetTopTenIncapacitadosByday($item, $valor, $fecha1, $fecha2){

			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT COUNT(inc_cc_afiliado) as total, inc_cc_afiliado, SUM(DATEDIFF(inc_fecha_final,inc_fecha_inicio))  as total_dias FROM gi_incapacidad WHERE $item = :$item  AND inc_fecha_inicio BETWEEN :fecha1 AND :fecha2 GROUP BY inc_cc_afiliado ORDER BY total DESC LIMIT 10;");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT COUNT(inc_cc_afiliado) as total, inc_cc_afiliado, SUM(DATEDIFF(inc_fecha_final,inc_fecha_inicio))  as total_dias FROM gi_incapacidad WHERE inc_fecha_inicio BETWEEN :fecha1 AND :fecha2 GROUP BY inc_cc_afiliado ORDER BY total DESC LIMIT 10;");
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}
		}

		/* Obtener los 10 eempleados mas incapacitados */
		static public function mdlGetTopTenIncapacitadosSinLimites($item, $valor, $fecha1, $fecha2){

			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT inc_fecha_inicio, inc_fecha_final, DATEDIFF(inc_fecha_final,inc_fecha_inicio) as total_dias FROM gi_incapacidad WHERE $item = :$item  AND inc_fecha_inicio BETWEEN :fecha1 AND :fecha2 ORDER BY total_dias;");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT inc_fecha_inicio, inc_fecha_fina FROM gi_incapacidad WHERE inc_fecha_inicio BETWEEN :fecha1 AND :fecha2 ;");
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}
		}

		/* Obtener los 10 eempleados mas incapacitados */
		static public function mdlGetTopTenDiagnosticos($item, $valor, $fecha1, $fecha2){

			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT COUNT(inc_diagnostico) as total, inc_diagnostico FROM gi_incapacidad WHERE $item = :$item  AND inc_fecha_inicio BETWEEN :fecha1 AND :fecha2 GROUP BY inc_diagnostico ORDER BY total DESC LIMIT 10;");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT COUNT(inc_diagnostico) as total, inc_diagnostico FROM gi_incapacidad WHERE inc_fecha_inicio BETWEEN :fecha1 AND :fecha2 GROUP BY inc_diagnostico ORDER BY total DESC LIMIT 10;");
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}
		}

		static public function mdlconsolidarEstadosIncapacidades($item, $valor, $fecha1, $fecha2){
			
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT count(*) as total, inc_origen FROM gi_incapacidad WHERE  $item = :$item  AND  inc_fecha_inicio BETWEEN :fecha1 AND :fecha2 GROUP BY inc_origen ORDER BY total DESC;");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT count(*) as total, inc_origen FROM gi_incapacidad WHERE inc_fecha_inicio BETWEEN :fecha1 AND :fecha2 GROUP BY inc_origen ORDER BY total DESC;");
				$stmt -> bindParam(":fecha1", $fecha1, PDO::PARAM_STR);
				$stmt -> bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
				$stmt -> close();
				$stmt = null;
			}
		}

		static public function mdlGetPagosIncapacidades($tabla, $item, $valor, $fechaInicial, $fechaFinal){
			$Lsql = "SELECT emp_nombre, emd_cedula, emd_nombre, inc_diagnostico, inc_origen, inc_clasificacion, inc_fecha_inicio, inc_fecha_final, inc_fecha_inicio AS fecha1, inc_fecha_final as fecha2, inc_tipo_generacion, inc_profesional_responsable, inc_prof_registro_medico, inc_donde_se_genera, inc_fecha_generada, inc_observacion, inc_estado_tramite, inc_fecha_radicacion, inc_fecha_solicitud, inc_fecha_pago, inc_valor,inc_fecha_pago_nomina, inc_valor_pagado_nomina, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado ,  ip4.ips_nombre as inc_afp_afiliado , inc_valor_solicitado, inc_valor_pagado_empresa, inc_valor_pagado_eps, inc_valor_pagado_ajuste, emd_centro_costos, emd_ciudad, emd_sede, emd_salario, emd_cargo,  emd_subcentro_costos, inc_fecha_negacion_,  CASE emd_estado WHEN 1 THEN 'ACTIVO' WHEN 0 THEN 'RETIRADO' END AS emd_estado, emd_codigo_nomina, dia_descripcion, CASE inc_tipo_generacion WHEN 0 THEN 'ORIGINAL' WHEN 1 THEN 'ORIGINAL' WHEN 2 THEN 'TRANSCRITA' WHEN 3 THEN 'DIGITAL' WHEN 4 THEN 'COPIA' END AS inc_tipo_generacion , atn_descripcion, a.user_nombre as creador, b.user_nombre as editor , inc_observacion_neg, inc_numero_radicado_v, inc_id, inc_estado, mot_desc_v, mot_observacion_v  FROM $tabla LEFT JOIN gi_ips ip2 ON ip2.ips_id = inc_ips_afiliado LEFT JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id LEFT JOIN gi_diagnostico ON dia_codigo = inc_diagnostico LEFT JOIN gi_atencion ON atn_id = inc_atn_id LEFT JOIN gi_usuario a ON a.user_id = inc_user_id_i LEFT JOIN gi_usuario b ON b.user_id = inc_edi_user_id_i LEFT JOIN gi_motivos_rechazo ON mot_id_i = inc_mot_rech_i WHERE $item = :$item AND inc_estado_tramite = 'PAGADA' ";
			if($fechaInicial != null && $fechaFinal != null){
				$Lsql .= " AND inc_fecha_pago BETWEEN '".$fechaInicial."' AND '".$fechaFinal."' ";
			}
			$Lsql .= "ORDER BY emd_nombre ASC, inc_fecha_inicio ASC";
			$stmt = Conexion::conectar()->prepare($Lsql);
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetchAll();
		}

		static public function getSumIncapacidades($anho = null, $tipo = null, $empresa){
			$fecha1 = $anho."-01-01";
			$fecha2 = $anho."-12-31";
			$inc_valor = 'inc_valor_pagado_eps';
			if($tipo == 5){
				$inc_valor = 'inc_valor';
			}
			$Lsql = "SELECT SUM(".$inc_valor.") as Total FROM gi_incapacidad ";
			
			if($anho != null){
			 	$Lsql .= " WHERE inc_fecha_pago_nomina BETWEEN '".$fecha1."' AND '".$fecha2."' AND inc_empresa = ".$empresa;
			}else{
				$Lsql .= " WHERE inc_empresa = ".$empresa;
			}

			if($tipo != null){
				$Lsql .=  " AND inc_estado_tramite = '".$tipo."'";
			}
			$stmt = Conexion::conectar()->prepare($Lsql);
			$stmt -> execute();
			return $stmt->fetch();
		}

		static public function getCountIncapacidades($anho = null, $tipo = null, $empresa){
			$fecha1 = $anho."-01-01";
			$fecha2 = $anho."-12-31";
			$Lsql = "SELECT COUNT(*) as Total FROM gi_incapacidad ";

			if($anho != null){
			 	$Lsql .= " WHERE inc_fecha_pago_nomina BETWEEN '".$fecha1."' AND '".$fecha2."' AND inc_empresa = ".$empresa;
			}else{
				$Lsql .= " WHERE inc_empresa = ".$empresa;
			}

			if($tipo != null){
				$Lsql .=  " AND inc_estado_tramite = '".$tipo."'";
			}
			
			$stmt = Conexion::conectar()->prepare($Lsql);
			$stmt -> execute();
			return $stmt->fetch();
		}

		static public function getCountIncapacidadesIncompletas($empresa){
			$Lsql = "SELECT COUNT(*) as Total FROM gi_incapacidad WHERE inc_estado = 0 AND inc_empresa = ".$empresa;
			$stmt = Conexion::conectar()->prepare($Lsql);
			$stmt -> execute();
			return $stmt->fetch();
		}

		static public function getSumIncapacidadesIncompletas($empresa){
			$Lsql = "SELECT SUM(inc_valor_pagado_eps) as Total FROM gi_incapacidad WHERE inc_estado = 0 AND inc_empresa = ".$empresa;
			$stmt = Conexion::conectar()->prepare($Lsql);
			$stmt -> execute();
			return $stmt->fetch();
		}

		static public function getTotalEps($tipo = null, $empresa){

			$Lsql = "SELECT ips_id, ips_nombre, SUM(inc_valor_pagado_eps) as total FROM gi_incapacidad JOIN gi_empleados ON gi_empleados.emd_id = gi_incapacidad.inc_emd_id JOIN gi_ips ON gi_empleados.emd_eps_id = gi_ips.ips_id WHERE inc_empresa = ".$empresa;
			$Lsql .= "  GROUP BY ips_id order by ips_nombre ";
			$stmt = Conexion::conectar()->prepare($Lsql);
			$stmt -> execute();
			return $stmt->fetchAll();
		}

		static public function getTotalEpsIndividual($tipo = null, $eps, $empresa){

			$Lsql = "SELECT SUM(inc_valor_pagado_eps) as total FROM gi_incapacidad JOIN gi_empleados ON gi_empleados.emd_id = gi_incapacidad.inc_emd_id JOIN gi_ips ON gi_empleados.emd_eps_id = gi_ips.ips_id WHERE inc_empresa = ".$empresa." AND ips_id = ".$eps;
			if($tipo != null){
				$Lsql .=  " AND inc_estado_tramite = '".$tipo."'";
			}
			$stmt = Conexion::conectar()->prepare($Lsql);
			$stmt -> execute();
			return $stmt->fetch();
		}

		static public function getSumValorEntranteAnual($anho, $empresa){
			$inicial = $anho."-01-01";
			$final = $anho."-12-31";
			$Lsql = "SELECT SUM(inc_valor) as total FROM gi_incapacidad WHERE inc_empresa = $empresa AND inc_fecha_pago BETWEEN '$inicial' AND '$final'";
			$stmt = Conexion::conectar()->prepare($Lsql);
			$stmt->execute();
			return $stmt->fetch();
		}
	}