<?php
	/**
	* Manipular las entidades registradas del sistema
	*/
	class ModeloEntidadAtiende extends ModeloDAO{

        //Funcion para registrar las entidades nuevas
        static public function mdlIngresarEntidadesNuevas($tabla,$datos){
			$pdo = Conexion::conectar();
            $stmt = $pdo->prepare("INSERT INTO $tabla (enat_nombre_v,enat_nit_v) VALUES (:enat_nombre_v,:enat_nit_v )");
			$stmt->bindParam(":enat_nombre_v", 	            $datos['enat_nombre_v'], 	PDO::PARAM_STR);
			$stmt->bindParam(":enat_nit_v", 		        $datos['enat_nit_v'], 	PDO::PARAM_STR);
			//$stmt->bindParam(":enat_fecha_creacion_d", 		$datos['enat_fecha_creacion_d'], 	PDO::PARAM_STR);
			//$stmt->bindParam(":enat_estado", 			    $datos['enat_estado'], 	PDO::PARAM_STR);
			

			if($stmt->execute()){
				$stmt = null;
				return $pdo->lastInsertId();
			}else{
				self::logError('2404', "Error mdlIngresarEntidadesNuevas entidadesRegistradas.modelo.php => ".$stmt->errorInfo());
				$stmt = null;
				return 'Error';
			}
		}
        //Funcion para Actualizar datos de las entidades creadas
        static public function mdlEditarEntidadesRegistradas($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET enat_nombre_v = :enat_nombre_v, 	enat_nit_v = :enat_nit_v, enat_estado = :enat_estado WHERE enat_id_i = :enat_id_i;");
			$stmt->bindParam(":enat_nombre_v",          	$datos['enat_nombre_v'], 	PDO::PARAM_STR);
			$stmt->bindParam(":enat_nit_v", 		        $datos['enat_nit_v'], 	PDO::PARAM_STR);
			//$stmt->bindParam(":enat_fecha_creacion_d", 		$datos['enat_fecha_creacion_d'], 	PDO::PARAM_STR);
			$stmt->bindParam(":enat_estado", 			    $datos['enat_estado'], 	PDO::PARAM_STR);
			$stmt->bindParam(":enat_id_i", 			        $datos['enat_id_i'], 	PDO::PARAM_STR);
		
			if($stmt->execute()){
				return 'ok';
				$stmt = null;
			}else{
				self::logError('2404', "Error mdlEditarEntidadesRegistradas entidadesRegistradas.modelo.php => ".$stmt->errorInfo());
				$stmt = null;
				return 'Error';
			}
		}
        //Funcion para ocultar las entidades creadas
        static public function mdlBorrarEntidadesRegistradas($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE enat_id_i = :enat_id_i");
			$stmt -> bindParam(":enat_id_i", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				$stmt = null;
				return "ok";
			}else{
				self::logError('2404', "Error mdlBorrarEntidadesRegistradas entidadesRegistradas.modelo.php => ".$stmt->errorInfo());
				$stmt = null;
				return $stmt->errorInfo();
			}
		}
        static public function mdlListarEntidades($tabla,$item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				$return = $stmt -> fetchAll();
				$stmt = null;
				return $return;
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
				$stmt -> execute();
				$return = $stmt -> fetchAll();
				$stmt = null;
				return $return;
			}
           // return $stmt -> fetchAll();
        
    }

	// valida que no se ingrese un codigo(NIT) repetido
	static public function mdlValidarEntidad($tabla,$item,$valor){
		if($item != null){		
			$stmt = Conexion::conectar()->prepare("SELECT $item FROM $tabla WHERE $item = :$item");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt -> execute();
			$return = $stmt -> fetch();
			$stmt = null;
			return $return;
		}
	}

	static public function mdlBuscarEntidadesIpsByNombre($tabla, $valor){
        $stmt = Conexion::conectar()->prepare("SELECT enat_id_i as id, enat_nit_v  as nit, enat_nombre_v as name FROM $tabla where enat_nombre_v LIKE '%".$valor."%' OR enat_nit_v LIKE '%".$valor."%' ORDER BY enat_nombre_v ASC LIMIT 100");
        $stmt->execute();
        return $stmt->fetchAll();
    }
        
}  
        
      