<?php
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloClientes extends ModeloDAO{
		static public function mdlMostrarClientesLimit($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT COUNT(inc_empresa) as empresas , emp_nombre, emp_id FROM gi_incapacidad JOIN gi_empresa ON emp_id = inc_empresa  WHERE $item = :$item group by inc_empresa ORDER BY empresas DESC LIMIT 10;");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetchAll();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT COUNT(inc_empresa) as empresas , emp_nombre, emp_id FROM gi_incapacidad JOIN gi_empresa ON emp_id = inc_empresa group by inc_empresa ORDER BY empresas DESC LIMIT 10;");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlIngresarCliente($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (emp_nombre, emp_direccion, emp_nit, emp_telefono, emp_email, emp_representante, emp_cc_representante, emp_fecha_pago_seguridad, emp_contacto_gestion, emp_celular , emp_ruta_camara_comercio, emp_ruta_cedula_representante, emp_ruta_cuenta_bancaria, emp_ruta_rut, emp_email_gestion, emp_contacto_tesoreria, emp_telefono_tesoreria, emp_email_tesoreria, emp_telefono_gestion, emp_celular_tesoreria, emp_liquidacion_automatica_i, emp_porcentaje_i, emp_correo_1, emp_correo_2, emp_correo_3, emp_correo_4, emp_correo_incapacidades_v,emp_crv_v,emp_fecha_venc_cam_com_d,emp_fecha_venc_cert_ban_d) VALUES(:emp_nombre, 
				:emp_direccion, 
				:emp_nit, 
				:emp_telefono, 
				:emp_email, 
				:emp_representante, 
				:emp_cc_representante, 
				:emp_fecha_pago_seguridad, 
				:emp_contacto_gestion, 
				:emp_celular ,
				:emp_ruta_camara_comercio,
				:emp_ruta_cedula_representante, 
				:emp_ruta_cuenta_bancaria, 
				:emp_ruta_rut, 
				:emp_email_gestion, 
				:emp_contacto_tesoreria, 
				:emp_telefono_tesoreria, 
				:emp_email_tesoreria, 
				:emp_telefono_gestion, 
				:emp_celular_tesoreria,
				:emp_liquidacion_automatica_i, 
				:emp_porcentaje_i, 
				:emp_correo_1, 
				:emp_correo_2, 
				:emp_correo_3, 
				:emp_correo_4, 
				:emp_correo_incapacidades_v,
				:emp_crv_v,
				/*-------nuevos campos----*/
				:emp_fecha_venc_cam_com_d,
				:emp_fecha_venc_cert_ban_d )");

			$stmt->bindParam(":emp_nombre", 	$datos['emp_nombre'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_direccion", 	$datos['emp_direccion'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_nit", 		$datos['emp_nit'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_telefono", 	$datos['emp_telefono'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_email", 		$datos['emp_email'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_representante", 	$datos['emp_representante'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_cc_representante",$datos['emp_cc_representante'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_fecha_pago_seguridad", $datos['emp_fecha_pago_seguridad'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_contacto_gestion",$datos['emp_contacto_gestion'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_celular", $datos['emp_celular'],PDO::PARAM_STR);

			$stmt->bindParam(":emp_ruta_camara_comercio",$datos['emp_ruta_camara_comercio'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_ruta_cedula_representante", $datos['emp_ruta_cedula_representante'],PDO::PARAM_STR);
			$stmt->bindParam(":emp_contacto_gestion",$datos['emp_contacto_gestion'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_ruta_cuenta_bancaria", $datos['emp_ruta_cuenta_bancaria'],PDO::PARAM_STR);
			$stmt->bindParam(":emp_ruta_rut",$datos['emp_ruta_rut'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_email_gestion", $datos['emp_email_gestion'],PDO::PARAM_STR);

			$stmt->bindParam(":emp_contacto_tesoreria",$datos['emp_contacto_tesoreria'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_telefono_tesoreria", $datos['emp_telefono_tesoreria'],PDO::PARAM_STR);
			$stmt->bindParam(":emp_email_tesoreria",$datos['emp_email_tesoreria'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_telefono_gestion", $datos['emp_telefono_gestion'],PDO::PARAM_STR);
			$stmt->bindParam(":emp_celular_tesoreria", $datos['emp_celular_tesoreria'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_liquidacion_automatica_i", $datos['emp_liquidacion_automatica_i'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_porcentaje_i", $datos['emp_porcentaje_i'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_correo_1", $datos['emp_correo_1'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_correo_2",$datos['emp_correo_2'],  PDO::PARAM_STR);
			$stmt->bindParam(":emp_correo_3", $datos['emp_correo_3'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_correo_4", $datos['emp_correo_4'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_correo_incapacidades_v", $datos['emp_correo_incapacidades_v'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_crv_v", $datos['emp_crv_v'], PDO::PARAM_STR);
			/*---------Nuevos campos-----------*/
			$stmt->bindParam(":emp_fecha_venc_cam_com_d", $datos['emp_fecha_venc_cam_com_d'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_fecha_venc_cert_ban_d", $datos['emp_fecha_venc_cert_ban_d'], PDO::PARAM_STR);
				
			if($stmt->execute()){
				return 'ok';
			}else{
				print_r($stmt->errorInfo());
				return $stmt->errorInfo();
			}
			$stmt->close();
			$stmt = null;
		}

		static public function mdlEditarClientes($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  emp_nombre = :emp_nombre, emp_direccion = :emp_direccion, emp_nit = :emp_nit, emp_telefono = :emp_telefono, emp_email = :emp_email, emp_representante = :emp_representante, emp_cc_representante = :emp_cc_representante , emp_fecha_pago_seguridad = :emp_fecha_pago_seguridad, emp_contacto_gestion = :emp_contacto_gestion, emp_celular = :emp_celular , emp_ruta_camara_comercio = :emp_ruta_camara_comercio, emp_ruta_cedula_representante = :emp_ruta_cedula_representante, emp_ruta_cuenta_bancaria = :emp_ruta_cuenta_bancaria, emp_ruta_rut = :emp_ruta_rut, emp_email_gestion = :emp_email_gestion, emp_contacto_tesoreria = :emp_contacto_tesoreria, emp_telefono_tesoreria = :emp_telefono_tesoreria, emp_email_tesoreria = :emp_email_tesoreria, emp_telefono_gestion = :emp_telefono_gestion, emp_celular_tesoreria = :emp_celular_tesoreria, emp_liquidacion_automatica_i = :emp_liquidacion_automatica_i, emp_porcentaje_i = :emp_porcentaje_i, emp_correo_1 = :emp_correo_1, emp_correo_2 = :emp_correo_2, emp_correo_3 = :emp_correo_3, emp_correo_4 = :emp_correo_4, emp_correo_incapacidades_v = :emp_correo_incapacidades_v, emp_crv_v = :emp_crv_v, emp_fecha_venc_cam_com_d = :emp_fecha_venc_cam_com_d, emp_fecha_venc_cert_ban_d = :emp_fecha_venc_cert_ban_d WHERE emp_id = :emp_id");
			$stmt->bindParam(":emp_nombre", 	$datos['emp_nombre'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_direccion", 	$datos['emp_direccion'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_nit", 		$datos['emp_nit'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_telefono", 	$datos['emp_telefono'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_email", 		$datos['emp_email'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_representante", 		$datos['emp_representante'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_cc_representante", 	$datos['emp_cc_representante'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_id", 		$datos['emp_id'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_fecha_pago_seguridad", 	$datos['emp_fecha_pago_seguridad'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_contacto_gestion", 	$datos['emp_contacto_gestion'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_celular", $datos['emp_celular'], 	PDO::PARAM_STR);
			$stmt->bindParam(":emp_ruta_camara_comercio",$datos['emp_ruta_camara_comercio'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_ruta_cedula_representante", $datos['emp_ruta_cedula_representante'],PDO::PARAM_STR);
			$stmt->bindParam(":emp_contacto_gestion",$datos['emp_contacto_gestion'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_ruta_cuenta_bancaria", $datos['emp_ruta_cuenta_bancaria'],PDO::PARAM_STR);
			$stmt->bindParam(":emp_ruta_rut",$datos['emp_ruta_rut'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_email_gestion", $datos['emp_email_gestion'],PDO::PARAM_STR);

			$stmt->bindParam(":emp_contacto_tesoreria",$datos['emp_contacto_tesoreria'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_telefono_tesoreria", $datos['emp_telefono_tesoreria'],PDO::PARAM_STR);
			$stmt->bindParam(":emp_email_tesoreria",$datos['emp_email_tesoreria'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_telefono_gestion", $datos['emp_telefono_gestion'],PDO::PARAM_STR);
			$stmt->bindParam(":emp_celular_tesoreria", $datos['emp_celular_tesoreria'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_liquidacion_automatica_i", $datos['emp_liquidacion_automatica_i'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_porcentaje_i", $datos['emp_porcentaje_i'], PDO::PARAM_STR);
			
			$stmt->bindParam(":emp_correo_1", $datos['emp_correo_1'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_correo_2", $datos['emp_correo_2'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_correo_3", $datos['emp_correo_3'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_correo_4", $datos['emp_correo_4'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_correo_incapacidades_v", $datos['emp_correo_incapacidades_v'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_crv_v", $datos['emp_crv_v'], PDO::PARAM_STR);
			/*-----Nuevos campos-------- */
			$stmt->bindParam(":emp_fecha_venc_cam_com_d", $datos['emp_fecha_venc_cam_com_d'], PDO::PARAM_STR);
			$stmt->bindParam(":emp_fecha_venc_cert_ban_d", $datos['emp_fecha_venc_cert_ban_d'], PDO::PARAM_STR);
			if($stmt->execute()){
				return 'ok';
			}else{
				print_r($stmt->errorInfo());
				return $stmt->errorInfo();
			}
			$stmt->close();
			$stmt = null;
		}


		static public function mdlBorrarCliente($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE emp_id = :emp_id");
			$stmt -> bindParam(":emp_id", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}

		/* Actualizar usuario */
		static public function mdlActualizarCliente($tabla, $item1, $valor1, $item2, $valor2){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  $item1 = :$item1 WHERE $item2 = :$item2");
			$stmt->bindParam(":".$item1, 	$valor1, 	PDO::PARAM_STR);
			$stmt->bindParam(":".$item2, 	$valor2, 	PDO::PARAM_STR);
			if($stmt->execute()){
				return 'ok';
			}else{
				return 'Error';
			}
			$stmt->close();
			$stmt = null;
		}
		//------------------------ Función fecha de vencimiento Camara de comercio --------------	
		static public function mdlAlertavencimientocc($tabla, $valor){
			
			$stmt = Conexion::conectar()->prepare("Select datediff(now(), emp_fecha_venc_cam_com_d ) as vencimiento FROM $tabla WHERE emp_id = :emp_id AND emp_fecha_venc_cam_com_d IS NOT NULL");
			$stmt -> bindParam(":emp_id", $valor, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetch();
			
			$stmt -> close();
			$stmt = null;
		}
		
		static public function mdlAlertavencimientocuentab($tabla, $valor){
			
			$stmt = Conexion::conectar()->prepare("Select datediff(now(), emp_fecha_venc_cert_ban_d ) as vencimiento FROM $tabla WHERE emp_id = :emp_id AND emp_fecha_venc_cert_ban_d IS NOT NULL");
			$stmt -> bindParam(":emp_id", $valor, PDO::PARAM_STR);
			$stmt -> execute();
			return $stmt -> fetch();
			
			$stmt -> close();
			$stmt = null;
		}


	}
	