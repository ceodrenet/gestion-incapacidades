<?php

	class ModeloArchivos extends ModeloDAO {
		/**
		*Desc.  => Mostrar todas los archivos existentes por una empresa o por todo
		*params => $tabla = gi_archivos, $item = campo a buscar, $valor = valor a buscar
		*Return => array {}
		**/
		static public function mdlMostrarArchivos($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				$return = $stmt -> fetchAll();
				$stmt = null;
				return $return;
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
				$stmt -> execute();
				$return = $stmt -> fetchAll();
				$stmt = null;
				return $return;
			}
		}

		/**
		*Desc.  => Mostrar datos de un archivos
		*params => $tabla = gi_archivos, $item = campo a buscar, $valor = valor a buscar
		*Return => array {}
		**/
		static public function mdlMostrarArchivos_e($tabla, $item, $valor){
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt -> execute();
			$return = $stmt -> fetch();
			$stmt = null;
			return $return;
		}

		/**
		*Desc.  => Crear un nuevo registro
		*params => $tabla = gi_archivos, $datos = array de datos a insertar
		*Return => String (ok, error);
		**/
		static public function mdlIngresarArchivo($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (arc_descripcion, arc_entidad, arc_origen,  arc_fecha,  arc_ruta_1, arc_ruta_2, arc_fecha_registro, arc_empresa_id_i) VALUES ( :arc_descripcion, :arc_entidad, :arc_origen, :arc_fecha, :arc_ruta_1, :arc_ruta_2, :arc_fecha_registro, :arc_empresa_id_i)");
			$stmt->bindParam(":arc_descripcion", 	$datos['arc_descripcion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_entidad", 		$datos['arc_entidad'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_origen", 		$datos['arc_origen'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_fecha", 			$datos['arc_fecha'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_ruta_1", 		$datos['arc_ruta_1'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_ruta_2", 		$datos['arc_ruta_2'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_fecha_registro", $datos['arc_fecha_registro'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_empresa_id_i" , $datos['arc_empresa_id_i'], PDO::PARAM_STR);

			if($stmt->execute()){
				$stmt = null;
				return 'ok';
			}else{
				self::logError('2404', "Error mdlIngresarArchivo archivos.modelo.php => ".$stmt->errorInfo());
				$stmt = null;
				return 'Error';
			}
		}

		/**
		*Desc.  => Editar un registro
		*params => $tabla = gi_archivos, $datos = array de datos a editar
		*Return => String (ok, error);
		**/
		static public function mdlEditarArchivos($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET arc_descripcion = :arc_descripcion, 	arc_entidad = :arc_entidad, arc_origen = :arc_origen, arc_fecha = :arc_fecha, arc_ruta_1 = :arc_ruta_1, arc_ruta_2 = :arc_ruta_2, arc_empresa_id_i = :arc_empresa_id_i WHERE arc_id = :arc_id;");
			$stmt->bindParam(":arc_descripcion", 	$datos['arc_descripcion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_entidad", 		$datos['arc_entidad'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_origen", 		$datos['arc_origen'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_fecha", 			$datos['arc_fecha'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_ruta_1", 		$datos['arc_ruta_1'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_ruta_2", 		$datos['arc_ruta_2'], 	PDO::PARAM_STR);
			$stmt->bindParam(":arc_id", 			$datos['arc_id'], 		PDO::PARAM_STR);
			$stmt->bindParam(":arc_empresa_id_i" , $datos['arc_empresa_id_i'], PDO::PARAM_STR);

			if($stmt->execute()){
				return 'ok';
				$stmt = null;
			}else{
				self::logError('2404', "Error mdlEditarArchivos archivos.modelo.php => ".$stmt->errorInfo());
				$stmt = null;
				return 'Error';
			}
		}

		/**
		*Desc.  => Eliminar un registro
		*params => $tabla = gi_archivos, $datos = id de archivo a eliminar
		*Return => String (ok, error);
		**/
		static public function mdlBorrarArchivo($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE arc_id = :arc_id");
			$stmt -> bindParam(":arc_id", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				$stmt = null;
				return "ok";
			}else{
				self::logError('2404', "Error mdlBorrarArchivo archivos.modelo.php => ".$stmt->errorInfo());
				$stmt = null;
				return $stmt->errorInfo();
			}
		}

		/* Actualizar usuario */
		static public function mdlActualizarArchivo($tabla, $item1, $valor1, $item2, $valor2){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  $item1 = :$item1 WHERE $item2 = :$item2");
			$stmt->bindParam(":".$item1, 	$valor1, 	PDO::PARAM_STR);
			$stmt->bindParam(":".$item2, 	$valor2, 	PDO::PARAM_STR);
			if($stmt->execute()){
				$stmt = null;
				return 'ok';
			}else{
				self::logError('2404', "Error mdlActualizarArchivo archivos.modelo.php => ".$stmt->errorInfo());
				$stmt = null;
				return 'Error';
			}
			
		}
	}