<?php
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloAudtoria extends ModeloDAO{
        
        static public function mdlListarAuditoria(){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM gi_auditoria JOIN gi_usuario ON aud_id_usuario_i= user_id LEFT JOIN gi_empresa ON aud_empresa_v = emp_id  WHERE  1");
				$stmt -> execute();
				return $stmt -> fetchAll();
            
        }
		static public function mdlUltimoMovimiento($idSession){
			$stmt = Conexion::conectar()->prepare("SELECT aud_hora_d FROM gi_auditoria WHERE aud_id_session = '".$idSession."' ORDER BY aud_hora_d DESC LIMIT 1");
				$stmt -> execute();
				return $stmt -> fetch();

		}
    }