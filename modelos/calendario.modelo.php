<?php
	require_once "conexion.php";
	/**
	* Manipular los usuarios del sistema
	*/
	class ModeloCalendario{

		static public function mdlMostrarEventos($tabla, $item, $valor){
			if($item != null){
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
				$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);
				$stmt -> execute();
				return $stmt -> fetch();
			}else{
				$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
				$stmt -> execute();
				return $stmt -> fetchAll();
			}
			$stmt -> close();
			$stmt = null;
		}

		static public function mdlIngresarEvento($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (evn_asunto, evn_fecha_inicio, evn_hora_inicio, evn_fecha_final, evn_hora_final, evn_organizador, evn_lugar, evn_duracion, evn_observacion , evn_tododia) VALUES(:evn_asunto, :evn_fecha_inicio, :evn_hora_inicio, :evn_fecha_final, :evn_hora_final, :evn_organizador, :evn_lugar, :evn_duracion, :evn_observacion , :evn_tododia)");

			$stmt->bindParam(":evn_asunto", 		$datos['evn_asunto'], 		PDO::PARAM_STR);
			$stmt->bindParam(":evn_fecha_inicio", 	$datos['evn_fecha_inicio'], PDO::PARAM_STR);
			$stmt->bindParam(":evn_hora_inicio", 	$datos['evn_hora_inicio'], 	PDO::PARAM_STR);
			$stmt->bindParam(":evn_fecha_final", 	$datos['evn_fecha_final'], 	PDO::PARAM_STR);
			$stmt->bindParam(":evn_hora_final", 	$datos['evn_hora_final'], 	PDO::PARAM_STR);
			$stmt->bindParam(":evn_organizador", 	$datos['evn_organizador'], 	PDO::PARAM_STR);
			$stmt->bindParam(":evn_lugar",			$datos['evn_lugar'], 		PDO::PARAM_STR);
			$stmt->bindParam(":evn_duracion",		$datos['evn_duracion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":evn_observacion", 	$datos['evn_observacion'],	PDO::PARAM_STR);
			$stmt->bindParam(":evn_tododia",		$datos['evn_tododia'], 		PDO::PARAM_STR);
			
			if($stmt->execute()){
				return 'ok';
			}else{
				return $stmt->errorInfo();
			}

			$stmt->close();
			$stmt = null;
		}


		static public function mdlEditarEvento($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("UPDATE  $tabla SET evn_asunto = :evn_asunto, evn_fecha_inicio = :evn_fecha_inicio, evn_hora_inicio = :evn_hora_inicio, evn_fecha_final = :evn_fecha_final, evn_hora_final = :evn_hora_final, evn_organizador = :evn_organizador, evn_lugar = :evn_lugar, evn_duracion = :evn_duracion, evn_observacion = :evn_observacion , evn_tododia = :evn_tododia WHERE evn_id = :evn_id");

			$stmt->bindParam(":evn_asunto", 		$datos['evn_asunto'], 		PDO::PARAM_STR);
			$stmt->bindParam(":evn_fecha_inicio", 	$datos['evn_fecha_inicio'], PDO::PARAM_STR);
			$stmt->bindParam(":evn_hora_inicio", 	$datos['evn_hora_inicio'], 	PDO::PARAM_STR);
			$stmt->bindParam(":evn_fecha_final", 	$datos['evn_fecha_final'], 	PDO::PARAM_STR);
			$stmt->bindParam(":evn_hora_final", 	$datos['evn_hora_final'], 	PDO::PARAM_STR);
			$stmt->bindParam(":evn_organizador", 	$datos['evn_organizador'], 	PDO::PARAM_STR);
			$stmt->bindParam(":evn_lugar",			$datos['evn_lugar'], 		PDO::PARAM_STR);
			$stmt->bindParam(":evn_duracion",		$datos['evn_duracion'], 	PDO::PARAM_STR);
			$stmt->bindParam(":evn_observacion", 	$datos['evn_observacion'],	PDO::PARAM_STR);
			$stmt->bindParam(":evn_tododia",		$datos['evn_tododia'], 		PDO::PARAM_STR);
			$stmt->bindParam(":evn_id",				$datos['evn_id'], 			PDO::PARAM_INT);

			if($stmt->execute()){
				return 'ok';
			}else{
				return $stmt->errorInfo();
			}

			$stmt->close();
			$stmt = null;
		}

		static public function mdlBorrarEvento($tabla, $datos){
			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE evn_id = :evn_id");
			$stmt -> bindParam(":evn_id", $datos, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{
				return $stmt->errorInfo();	
			}
			$stmt->close();
			$stmt = null;
		}

	}