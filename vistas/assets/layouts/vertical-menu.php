<style type="text/css">
    .jose{
        width: 95%;
        margin: 0px auto;
    }

    @media (max-width: 400px) {
        .jose{
            width: 70%;
            margin: 0px auto;
        }
    }

    @media (max-width: 600px) {
        .jose{
            width: 70%;
            margin: 0px auto;
        }
    }
</style>
<header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <a href="inicio" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="vistas/img/plantilla/LCortado.png" class="img-responsive" height="24">
                    </span>
                    <span class="logo-lg">
                        <img src="vistas/img/plantilla/LOGO_1.png" class="img-responsive jose" >   
                    </span>
                </a>

                <a href="inicio" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="vistas/img/plantilla/LCortado.png" class="img-responsive" height="24">
                    </span>
                    <span class="logo-lg">
                        <img src="vistas/img/plantilla/LOGO_1.png" class="img-responsive jose" >   
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-16 header-item" id="vertical-menu-btn">
                <i class="fa fa-fw fa-bars"></i>
            </button>

            <!-- App Search-->
            <!--<form class="app-search d-none d-lg-block">
                <div class="position-relative">
                    <input type="text" class="form-control" placeholder="Busqueda">
                    <button class="btn btn-primary" type="button"><i class="bx bx-search-alt align-middle"></i></button>
                </div>
            </form>-->
        </div>

        <div class="d-flex">

            <div class="dropdown d-inline-block d-lg-none ms-2">
                <button type="button" class="btn header-item" id="page-header-search-dropdown"
                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i data-feather="search" class="icon-lg"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                    aria-labelledby="page-header-search-dropdown">
        
                    <form class="p-3">
                        <div class="form-group m-0">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="<?php echo $language["Search"]; ?>" aria-label="Search Result">

                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="dropdown d-none d-sm-inline-block">

            <div class="dropdown d-none d-sm-inline-block">
                <button type="button" class="btn header-item" id="mode-setting-btn">
                    <i data-feather="moon" class="icon-lg layout-mode-dark"></i>
                    <i data-feather="sun" class="icon-lg layout-mode-light"></i>
                </button>
            </div>


            

            <div class="dropdown d-inline-block">
                <button type="button" title="Cambiar de empresa" class="btn header-item" data-bs-toggle="modal" data-bs-target="#modalChangeEmpresa">
                    <i data-feather="refresh-ccw" class="icon-lg"></i>
                </button>
            </div>

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item bg-soft-light border-start border-end" id="page-header-user-dropdown"
                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php
                        $imagen = 'vistas/img/usuarios/default/anonymous.png';
                        if($_SESSION['imagen'] != null && $_SESSION['imagen'] != ''){
                            $imagen = $_SESSION['imagen'];
                        }
                    ?>
                    <img class="rounded-circle header-profile-user" src="<?php echo $imagen; ?>"
                        alt="Header Avatar">
                    <span class="d-none d-xl-inline-block ms-1 fw-medium">
                        <?php 
                            if(isset($_SESSION['nombres'])){
                                echo $_SESSION['nombres'];
                            }
                        ?>  
                    </span>
                    <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    <!-- item-->
                    <a class="dropdown-item" href="#"><i class="mdi mdi-face-profile font-size-16 align-middle me-1"></i> Perfil</a>
                    <a class="dropdown-item" href="#"><i class="mdi mdi-lock font-size-16 align-middle me-1"></i> Bloquear Pantalla </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="salir"><i class="mdi mdi-logout font-size-16 align-middle me-1"></i> Salir </a>
                </div>
            </div>

        </div>
    </div>
</header>

<!-- ========== Left Sidebar Start ========== -->
<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" data-key="t-menu"><?php echo $language["Menu"]; ?></li>

                <?php 
                $menuspermisos = ControladorPerfiles::ctrMostrarMenusPermisos('perfiles_permisos_perfil_id_i',$_SESSION['perfil']);
                //echo $_SESSION['cliente_id'] ;
                foreach($menuspermisos as $item){
                    $menus = ControladorPerfiles::ctrMostrarMenus('menus_id_i',$item['perfiles_permisos_menu_id_i']);
                    if($menus['menus_reporte_i'] == 0){
                        if($menus['menus_es_treview'] == 1){
                            echo '
                                <li id="'.$menus['menus_id_v'].'">
                                    <a href="javascript: void(0);" class="has-arrow">
                                        <i data-feather="grid"></i>
                                        <span data-key="t-apps">'.mb_strtoupper($menus['menus_nombre_v']).'</span>
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">';
                                    
                                    $submenus = ControladorPerfiles::ctrMostrarOpciones('submenu_menu_id', $menus['menus_id_i']);
                                    //var_dump($submenus);
                                    foreach ($submenus as $key) {
                                        echo '<li id="'.$key['submenu_id_v'].'">
                                                <a href="'.$key['submenu_href'].'">
                                                    <span data-key="t-calendar">'.$key['submenu_nombre'].'</span>
                                                </a>
                                            </li>';
                                    }
                            echo '  </ul>
                                </li>';
                        }else{
                            echo '
                                <li id="'.$menus['menus_id_v'].'">
                                    <a href="'.$menus['menus_html_href_v'].'">
                                        <i data-feather="grid"></i>
                                        <span data-key="'.$menus['menus_html_icon_v'].'">'.mb_strtoupper($menus['menus_nombre_v']).'</span>
                                    </a>
                            </li>';
                        }   
                    }
                } 
            ?>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->

<div id="modalChangeEmpresa" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">OPCIONES PARA VISUALIZAR INFORMACIÓN</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="mb-3">
                            <label class="form-label">SELECCIONE UNA EMPRESA</label>
                            <select class="form-control proyectos" style="width: 100%;" id="selEmpresas">
                                <?php
                                    $campos = 'emp_id, emp_nombre';
                                    $tablas = 'gi_empresa';
                                    if($_SESSION['perfil'] == 7){
                                        $condiciones = '';
                                        echo '<option value="0">TODAS</option>';
                                    }else{
                                        $condiciones = 'emp_id IN (SELECT emu_id_empresa FROM gi_empresa_usuarios WHERE emu_id_usuario = '.$_SESSION['id_Usuario'].')';
                                    }
                                    $proyectos = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones, '', 'ORDER BY emp_nombre ASC');
                                    $activo = 1;
                                    
                                    foreach ($proyectos as $key => $value) {
                                        if($_SESSION['cliente_id'] == $value['emp_id']){
                                            echo '<option selected value="'.$value["emp_id"].'">'.$value["emp_nombre"].'</option>';
                                        }else{
                                            echo '<option value="'.$value["emp_id"].'">'.$value["emp_nombre"].'</option>';
                                        }
                                        
                                    }
                                ?>
                            </select>  
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>