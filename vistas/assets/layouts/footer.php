<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>document.write(new Date().getFullYear())</script> © Incapacidades.co.
            </div>
            <div class="col-sm-6">
                <div class="text-sm-end d-none d-sm-block">
                    Diseñado & Desarrollado Por <a href="https://incapacidades.co" class="text-decoration-underline">Incapacidades.co.</a>
                </div>
            </div>
        </div>
    </div>
</footer>