<?php
    function getDataAlerta($alerta, $diasProcesar){
        $numeroAlertas = 0;
        foreach ($alerta as $key => $value) {
                                 
            $camposX = "inc_fecha_inicio, inc_fecha_final";
            $tablasX = "gi_incapacidad";
            $whereXX = "inc_cc_afiliado = ".$value['inc_cc_afiliado']." AND inc_empresa = ".$_SESSION['cliente_id'];
            $verificar = ModeloTesoreria::mdlMostrarGroupAndOrder($camposX, $tablasX, $whereXX, '', 'ORDER BY inc_fecha_inicio ASC');
            $valida = false;
            $dias_transcurridos = 0;
            $inc_fecha_inicio_Final = ''; 
            foreach ($verificar as $incapacidades => $incapacidad) {
                if($inc_fecha_inicio_Final != ''){
                    //tebemos que validar que la fecha inicio sea mayor a 30 dias de la final pasada
                    $dias = 0;
                    $dias = ControladorIncapacidades::dias_transcurridos( $inc_fecha_inicio_Final, $incapacidad['inc_fecha_inicio']);
                    if($dias <= 30){
                        $valida = true;
                        $dias_transcurridos += $dias;
                    }
                }
                $inc_fecha_inicio_Final = $incapacidad['inc_fecha_final'];
            }
            if($dias_transcurridos >= $diasProcesar){
                $numeroAlertas++;
            }
        }
            
        $sinS = 'Incapacidad';
        if($numeroAlertas > 1){
            $sinS = 'Incapacidades';
        }
        if($numeroAlertas > 0){                        
            $amostrar = '<a href="'.$diasProcesar.'dias" class="text-reset notification-item">
                            <div class="d-flex">
                                <div class="flex-shrink-0 me-3">
                                    <img src="vistas/assets/assets/images/users/avatar-3.jpg" class="rounded-circle avatar-sm" alt="user-pic">
                                </div>
                                <div class="flex-grow-1">
                                    <h6 class="mb-1">'.$numeroAlertas.' '.$sinS.' de mas '.$diasProcesar.' días</h6>
        
                                </div>
                            </div>
                        </a>';
            return $amostrar;    
        }else{
            return '';
        }
        
    }
    $campos = 'inc_cc_afiliado';
    $tablas = 'reporte_alarmas';

    $condiciones  = 'Tiempo_dias > 90 AND Tiempo_dias < 120';
    $condiciones1 = 'Tiempo_dias > 120 AND Tiempo_dias < 180';
    $condiciones2 = 'Tiempo_dias > 180 AND Tiempo_dias < 540';
    $condiciones3 = 'Tiempo_dias > 540';

    if($_SESSION['cliente_id'] != 0){
        $condiciones .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
        $condiciones1 .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
        $condiciones2 .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
        $condiciones3 .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
    }
    $alarma1 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones, '', '');
    $alarma2 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones1, '', '');
    $alarma3 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones2, '', '');
    $alarma4 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones3, '', '');
  
    $numeroAlarmas = 0;
    $amostrar = '';
    if($alarma1 != null && $alarma1 != false){
        $alertasString = getDataAlerta($alarma1, 90);
        if($alertasString != ''){
            $numeroAlarmas++;
            $amostrar .= $alertasString;
        }
        
    }

    if($alarma2 != null && $alarma2 != false){
        $alertasString = getDataAlerta($alarma2, 120);
        if($alertasString != ''){
            $numeroAlarmas++;
            $amostrar .= $alertasString;
        }
    }
    if($alarma3 != null && $alarma3 != null){
        $alertasString = getDataAlerta($alarma3, 180);
        if($alertasString != ''){
            $numeroAlarmas++;
            $amostrar .= $alertasString;
        }
    }
    if($alarma4 != null && $alarma4 != false){
        $alertasString = getDataAlerta($alarma4, 540);
        if($alertasString != ''){
            $numeroAlarmas++;
            $amostrar .= $alertasString;
        }
    }

    $campos = 'count(*) as total';
    $tablas = 'gi_empresa_usuarios';

    $condiciones  = 'emu_id_usuario = '.$_SESSION['id_Usuario'];

    $empresas = ModeloTesoreria::mdlMostrarUnitario($campos, $tablas, $condiciones);

?>
<div class="dropdown d-inline-block">
                <button type="button" class="btn header-item noti-icon position-relative" id="page-header-notifications-dropdown"
                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i data-feather="bell" class="icon-lg"></i>
                    <span class="badge bg-danger rounded-pill"><?php echo $numeroAlarmas;?></span>
                </button>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                    aria-labelledby="page-header-notifications-dropdown">
                    <div class="p-3">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="m-0"> Notificaciones </h6>
                            </div>
                            <div class="col-auto">
                                <?php if($numeroAlarmas > 0) { ?>
                                <a href="#!" class="small text-reset text-decoration-underline"> Tienes (<?php echo $numeroAlarmas;?>) Alertas</a>
                                <?php }else { ?>
                                <a href="#!" class="small text-reset text-decoration-underline"> </a>
                                <?php } ?>
                                
                            </div>
                        </div>
                    </div>
                    <div data-simplebar style="max-height: 230px;">
                        <?php
                            echo $amostrar;
                        ?>
                    </div>
                </div>
            </div>