<!-- preloader css -->
<link rel="stylesheet" href="vistas/assets/assets/css/preloader.min.css" type="text/css" />

<!-- Bootstrap Css -->
<link href="vistas/assets/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<!-- Icons Css -->
<link href="vistas/assets/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
<!-- App Css-->
<link href="vistas/assets/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />