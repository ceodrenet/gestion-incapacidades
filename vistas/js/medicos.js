var edicion =  '<div class="btn-group dropstart" role="group">';
edicion += '<button id="btnGroupVerticalDrop1" type="button"  class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
edicion += '<i class="fa fa-info-circle"></i>';
edicion += '</button>';
edicion += '<ul class="dropdown-menu" role="menu">';
edicion += '<li><a class="dropdown-item btnEditarMedico" title="Editar Medico" idMedico data-bs-toggle="modal" data-bs-target="#modalEditarMedico" href="#">EDITAR</a></li>';
edicion += '<li class="dropdown-divider"></li>';
edicion += '<li><a class="dropdown-item btnEliminarMedico" title="Eliminar Medico" idMedico href="#">ELIMINAR</a></li>';
edicion +='</ul>';
edicion +='</div>';

var tablaMedicos= $('#tablaMedicos').DataTable({
    "ajax": "ajax/medicos.ajax.php?getMedicos",
    "columnDefs": [
    
        {
            "targets": -1,
            "data": null,
            "className": "text-center",
            "defaultContent": edicion
        }
    ],
    
    "language" : {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    } 

});

$('#tablaMedicos tbody').on( 'click', 'a', function () {
    var current_row = $(this).parents('tr');//Get the current row
    if (current_row.hasClass('child')) {//Check if the current row is a child row
        current_row = current_row.prev();//If it is, then point to the row before it (its 'parent')
    }
    var data = tablaMedicos.row( current_row ).data();
    $(this).attr("idMedico", data[4]);
   
} );

//validar identificación
$("#NuevaIdentificacion").change(function(){
    var medico = $(this).val();
    var datos = new FormData();
    datos.append('validarIdentificacion', medico);
    $.ajax({
        url   : 'ajax/medicos.ajax.php',
        method: 'post',
        data  : datos,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        success     : function(respuesta){    
            if(respuesta != false){
                $("#NuevaIdentificacion").val('');
                $("#btnGuardarMedico").attr('disabled', true);
                alertify.error('Esta identificación ya existe');
                $("#NuevaIdentificacion").focus();
            } else {
                $("#btnGuardarMedico").attr('disabled', false);
            }
        }
    });
});


/* Eliminar Entidades */
$('#tablaMedicos tbody').on("click", ".btnEliminarMedico", function () {
    var x = $(this).attr('idMedico');
    swal.fire({
        title: '¿Está seguro de borrar el usuario?',
        text: "¡Si no lo está puede cancelar la accíón!",
        /* type: 'warning',*/
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borralo!'
    }).then(function (result) {
        if (result.value) {
            //window.location = "index.php?ruta=usuariosEps&id_usuario_eps="+x ;
            var datas = new FormData();
            datas.append('id_medico', x);
            $.ajax({
                url: 'ajax/medicos.ajax.php',
                method: 'post',
                data: datas,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                beforeSend: function () {
                    $.blockUI({
                        baseZ: 2000,
                        css: {
                            border: 'none',
                            padding: '1px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
                },
                complete: function () {
                    tablaMedicos.ajax.reload();
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function (data) {
                    if (data.code == '1') {
                        alertify.success(data.desc);

                    } else {
                        alertify.error(data.desc);
                    }
                },
                //si ha ocurrido un error
                error: function () {
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        }
    })
});


/* Editar Usuario*/
$('#tablaMedicos tbody').on("click", ".btnEditarMedico", function () {
    var x = $(this).attr('idMedico');
    var datas = new FormData();
    datas.append('EditarMedicoId', x);
    $.ajax({
        url: 'ajax/medicos.ajax.php',
        method: 'post',
        data: datas,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (data) {

            $("#EditarNombreMedico").val(data.med_nombre);
            $("#EditarIdentificacion").val(data.med_identificacion);
            $("#EditarNumRegistro").val(data.med_num_registro);
            $("#EditarEstadoRethus").val(data.med_estado_rethus);
            $("#EditarEstadoMedico").val(data.med_estado);
            $("#EditarMedicoID").val(data.med_id);

            var datos = new FormData();
            datos.append('idMedicoEnt', x);
            $.ajax({
                url   : 'ajax/medicos.ajax.php',
                method: 'post',
                data  : datos,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(datosMedEnt){
                    var listMedEnt = []
                    datosMedEnt.forEach(element => {   
                        listMedEnt.push(element['ment_id_entidad_ips']);
                    });
                    $("#EditarEntidadIps").val(listMedEnt);
                }
            });
        }
    });
});

//Editar medicos
$("#btnEditarMedico").click(function () {
    var formData = new FormData($("#frmEditarMedico")[0]);
    $.ajax({
        url: 'ajax/medicos.ajax.php',
        method: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function () {
            $.blockUI({
                baseZ: 2000,
                css: {
                    border: 'none',
                    padding: '1px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        },
        complete: function () {
            tablaMedicos.ajax.reload();
            $("#modalEditarMedico").modal('hide');
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function (data) {
            if (data.code == '1') {
                alertify.success(data.desc);
            } else{
                alertify.error(data.desc);
            }
        },
        //si ha ocurrido un error
        error: function () {
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});

$("#exportarMedicos").click(function(){
    $.ajax({
        url    : 'ajax/pythonServices.ajax.php',
        type   : 'post',
        dataType : 'json',
        data   : { exportarDatosMed: true },
        success : function(data){
            var $a = $("<a>");
            $a.attr("href",data.ruta);
            $("body").append($a);
            $a.attr("download",data.archivo);
            $a[0].click();
            $a.remove();
        },
        beforeSend:function(){
            $.blockUI({ 
                message : '<h3>Un momento por favor....</h3>',
                baseZ: 2000,
                css: { 
                    border: 'none', 
                    padding: '1px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                } 
            }); 
        },
        complete:function(){
            $.unblockUI();
        }
    });
});

$("#filtrosMedicosByYear").on('change', function(){
    var _miValor = $(this).val();
    $.ajax({
        url : 'vistas/modulos/medicos/medicosFiltroAnual.php',
        type: 'post',
        data: {
            yearFilter : _miValor
        },
        success : function(data){
            $("#FieldsFilters").html(data);
        }
    });
});

$("#cargarDatos").click(function(){
    if($("#fileMedicos").val() == ''){
        alertify.error("¡No se ha seleccionado un archivo!");
    }else{
    
        //Se crean un array con los datos a enviar, apartir del formulario 
        var formData = new FormData($("#frmCargueMasivoMedicos")[0]);
        $.ajax({
            url: 'https://incapacidades.app/services/exports/medicos/importar',
            type  : 'post',
            data: formData,
            dataType : 'json',
            cache: false,
            contentType: false,
            processData: false,
            beforeSend:function(){
                $.blockUI({ 
                    message : '<h3>Un momento por favor....</h3>',
                    baseZ: 2000,
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff' 
                    } 
                }); 
            },
            complete:function(){
                $.unblockUI();
            },
            //una vez finalizado correctamente
            success: function(data){
                alertify.success(data.msg);

                // if(data.fallaron > 0){
                //     window.location.href = 'index.php?exportar=fallosIncapacidad';
                // }   

                $("#frmCargueMasivoMedicos")[0].reset();  
                $("#modalCargarMedicos").modal('hide');   

                tablaMedicos.ajax.reload();
            },
            //si ha ocurrido un error
            error: function(){
                //after_save_error();
                alertify.error('Error al realizar el proceso');
            }
        });
    }
});