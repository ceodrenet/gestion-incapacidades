$(function(){

    //getDatosAuditoria();
    var NuevoFechaFinal2 = flatpickr('#NuevoFechaFinal2', {
        altInput: true,
        locale: "es",
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        onChange: function(selectedDates, dateStr, instance){
            var minDate = dateStr;
            var fecha1 = moment($("#NuevoFechaInicio2").val());
            var fecha2 = moment(minDate);
            $("#EditarNumeroDias").val( (fecha2.diff(fecha1, 'days') + 1) + " Días");
        }
    });

    var NuevoFechaInicio2 = flatpickr('#NuevoFechaInicio2', {
            altInput: true,
            locale: "es",
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            onChange: function(selectedDates, dateStr, instance) {
                NuevoFechaFinal2.set("minDate", dateStr);
                NuevoFechaFinal2.setDate(dateStr);
            },
    });

    const element = document.querySelector('#buscarUsuario');
    const selEmpresa = new Choices(element);

    const element1 = document.querySelector('#buscarEmpresa');
    const selUsuario = new Choices(element1);

    $("#btnBuscarAuditoria").click(function(){
        getDatosAuditoria();
    });

    function getDatosAuditoria(){
        var fechaInicio = $("#NuevoFechaInicio2").val();
        var fechaFinal = $("#NuevoFechaFinal2").val();
        var paso = 0;
        if(fechaInicio.length < 0){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha inicial es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
    
        } 
    
        if(paso == 0){
            $.ajax({
                url    : 'ajax/filtrarAuditorias.ajax.php',
                type   : 'post',
                data   :{
                    NuevoFechaInicio2  : fechaInicio,
                    NuevoFechaFinal2  : fechaFinal,
                    buscarUsuario    : $("#buscarUsuario").val(),
                    buscarEmpresa    : $("#buscarEmpresa").val(),
                },
              //  dataType : 'html',
                beforeSend:function(){
                    $.blockUI({ 
                       baseZ: 2000,
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                    //$("#btnExportarReporteGerencial").attr('href', 'index.php?exportar=pdfGerencial&NuevoFechaInicial='+$("#NuevoFechaInicio2").val()+'&NuevoFechaFinal='+$("#NuevoFechaInicio3").val());
                },
                complete:function(){
                    $.unblockUI();
                },
                success : function(data){
                    $("#resultados").html(data);
                    
                }
            })
        } 
    }
});

