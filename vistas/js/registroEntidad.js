var edicion =  '<div class="btn-group dropstart" role="group">';
edicion += '<button id="btnGroupVerticalDrop1" type="button"  class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
edicion += '<i class="fa fa-info-circle"></i>';
//edicion += '<span class="sr-only">Toggle Dropdown</span>';
edicion += '</button>';
edicion += '<ul class="dropdown-menu" role="menu">';
edicion += '<li><a class="dropdown-item btnEditarEntidad" title="Editar Entidad" idEntidad data-bs-toggle="modal" data-bs-target="#modalEditarEntidades" href="#">EDITAR</a></li>';
edicion += '<li class="dropdown-divider"></li>';
edicion += '<li><a class="dropdown-item btnEliminarEntidad" title="Eliminar Entidad" idEntidad href="#">ELIMINAR</a></li>';
edicion +='</ul>';
edicion +='</div>';

/*===============================================
=               DATATABLES                      =
================================================*/

var tablaRegistroEntidad= $('#tablaRegistroEntidad').DataTable({
    "ajax": "ajax/registroEntidad.ajax.php?getEntidades",
    "columnDefs": [
    
        {
            "targets": -1,
            "data": null,
            "className": "text-center",
            "defaultContent": edicion
        }
    ],
    
    "language" : {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    } 

});

$('#tablaRegistroEntidad tbody').on( 'click', 'a', function () {
    var current_row = $(this).parents('tr');//Get the current row
    if (current_row.hasClass('child')) {//Check if the current row is a child row
        current_row = current_row.prev();//If it is, then point to the row before it (its 'parent')
    }
    var data = tablaRegistroEntidad.row( current_row ).data();
    $(this).attr("idEntidad", data[4]);
   
} );

//validar identificación 
$("#IngresarNit").change(function(){ 
    var entidad = $(this).val(); 
    var datos = new FormData(); 
    datos.append('validarNit', entidad); 
    $.ajax({ 
        url   : 'ajax/registroEntidad.ajax.php', 
        method: 'post', 
        data  : datos, 
        cache : false, 
        contentType : false, 
        processData : false, 
        dataType    : 'json', 
        success     : function(respuesta){     
            if(respuesta != false){ 
                $("#IngresarNit").val(''); 
                $("#btnGuardarEntidad").attr('disabled', true); 
                alertify.error('Esta entidad ya existe'); 
                $("#IngresarNit").focus(); 
            } else { 
                $("#btnGuardarEntidad").attr('disabled', false); 
            } 
        } 
    }); 
});


// mostrar informacion en la tabla principal
$('#tablaRegistroEntidad tbody').on("click", ".btnEditarEntidad" , function(){
    var x = $(this).attr('idEntidad');
    var datas = new FormData();
    datas.append('EditarEntidadId', x);
    $.ajax({
        url   : 'ajax/registroEntidad.ajax.php',
        method: 'post',
        data  : datas,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        success     : function(data){
            $("#EditarNombre").val(data.enat_nombre_v); 
            $("#EditarNit").val(data.enat_nit_v);
            $("#EditarEstado").val(data.enat_estado);
            $("#EditarEntidadID").val(data.enat_id_i);
            
            var datos = new FormData();
            datos.append('idEntidadRed', x);
            $.ajax({
                url   : 'ajax/registroEntidad.ajax.php',
                method: 'post',
                data  : datos,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(datosRed){
                    var listIdEps = []
                    datosRed.forEach(element => {   
                        listIdEps.push(element['ei_ips_id']);
                    });
                    
                    $("#EditarRedToPertenece").val(listIdEps);
                }
            });
       }
    }); 
});

$("#btnEditarEntidad").click(function(){
    var formData = new FormData($("#frmEditarEntidad")[0]);
    $.ajax({
        url   : 'ajax/registroEntidad.ajax.php',
        method: 'post',
        data  : formData,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend:function(){
            $.blockUI({ 
                baseZ: 2000,
                css: { 
                    border: 'none', 
                    padding: '1px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                } 
            }); 
        },
        complete:function(){
            tablaRegistroEntidad.ajax.reload();
            $("#modalEditarEntidades").modal('hide');  
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data.code == '1'){
                alertify.success( data.desc,'Hola' );    
            }else{
                alertify.error( data.desc );
            } 
        },
        //si ha ocurrido un error
        error: function(){
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});

$('#tablaRegistroEntidad tbody').on("click", ".btnEliminarEntidad", function(){
    var x = $(this).attr('idEntidad');
    swal.fire({
        title: '¿Está seguro de borrar la entidad seleccionada ?',
        text: "¡Si no lo está puede cancelar la accíón!",
       /* type: 'warning',*/
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borralo!'
    }).then(function (result) {
        if (result.value) {
            //window.location = "index.php?ruta=usuariosEps&id_usuario_eps="+x ;
            var datas = new FormData();
            datas.append('id_Entidad', x);
            $.ajax({
                url   : 'ajax/registroEntidad.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    tablaRegistroEntidad.ajax.reload();
                    $.unblockUI();
                },
                success: function(data){
                    if(data.code == '1'){
                        alertify.success( data.desc);   
                      
                    }else{
                        alertify.error( data.desc );
                    } 
                },

            });
        }
    })
});

