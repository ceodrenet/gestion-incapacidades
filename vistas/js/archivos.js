
/* validar los tipos de imagenes y/o archivos */
$(".NuevaFoto").change(function(){
	var imax = $(this).attr('valor');
	var imagen = this.files[0];
	console.log(imagen);
	/* Validar el tipo de imagen */
	if(imagen['type'] != 'image/jpeg' && imagen['type'] != 'image/png' && imagen['type'] != "application/pdf" ){
		$(".NuevaFoto").val('');
		swal.fire({
			title : "Error al subir el archivo",
			text  : "El archivo debe estar en formato PNG , JPG, PDF",
			type  : "error",
			confirmButtonText : "Cerrar"
		});
	}else if(imagen['size'] > 2000000 ) {
		$(".NuevaFoto").val('');
		swal({
			title : "Error al subir el archivo",
			text  : "El archivo no debe pesar mas de 2MB",
			type  : "error",
			confirmButtonText : "Cerrar"
		});
	}else{
		if(imagen['type'] == 'image/jpeg' || imagen['type'] == 'image/png' || imagen['type'] != "application/pdf"){
			var datosImagen = new FileReader();
			datosImagen.readAsDataURL(imagen);

			$(datosImagen).on("load", function(event){
				var rutaimagen = event.target.result;
				if(imax == '0'){
					$(".previsualizar").attr('src', rutaimagen);
				}else{
					$(".previsualizar_"+imax).attr('src', rutaimagen);
				}
				
				
			});	
		}
		
	}	
});



var edicion =  '<div class="btn-group dropstart" role="group">';
edicion += '<button type="button" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
edicion += '<i class="fa fa-info-circle"></i>';
//edicion += '<span class="sr-only">Toggle Dropdown</span>';
edicion += '</button>';
edicion += '<ul class="dropdown-menu" role="menu">';
edicion += '<li><a class="dropdown-item btnEditarArchivo" title=Editar Archivo "idArchivo data-bs-toggle="modal" data-bs-target="#modalEditarArchivos" href="#">EDITAR</a></li>';
edicion += '<li class="divider"></li>';
edicion += '<li><a class="dropdown-item btnEliminarArchivo" title="Eliminar Archivo" idArchivo href="#">ELIMINAR</a></li>';





var tabla_archivos = $('#tablaArchivos').DataTable({
    "ajax": "ajax/archivos.ajax.php?dame-Archivos-inicio=owi",
    "columnDefs": [
        {
            "targets": -3,
            "data": null,
            "className": "text-center",
            "defaultContent": '<a class="hrefPrimerAdjunto" target="_blank"><img class="img-thumbnail imgAdj1" style="width:40px;height:40px;"></a>'

        },
        {
            "targets": -2,
            "data": null,
            "className": "text-center",
            "defaultContent": '<a class="hrefSegundoAdjunto" target="_blank"><img class="img-thumbnail imgAdj2" style="width:40px;height:40px;"></a>'

        },
        {
            "targets": -1,
            "data": null,
            "className": "text-center",
            "defaultContent": edicion
        }
    ],
    "language" : {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    } 
});

/*Proceso para cargar las imagenes */
$('#tablaArchivos tbody').on('click', 'a', function () {
    var data = tabla_archivos.row( $(this).parents('tr') ).data();
    $(this).attr("idArchivo", data[6]);
} );

/* Editar Entidades */
$('#tablaArchivos tbody').on("click", ".btnEditarArchivo", function(){
	var x = $(this).attr('idArchivo');
    var datas = new FormData();
    datas.append('EditarArchivoId', x);
    $.ajax({
        url   : 'ajax/archivos.ajax.php',
        method: 'post',
        data  : datas,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        success     : function(data){
            $("#EditarEntidad").val(data.arc_entidad); 
			$("#EditarOrigen").val(data.arc_origen);
			$("#EditarOrigen").val(data.arc_origen).change();		

			$("#EditarFecha").val(data.arc_fecha);
			$("#EditarDescripcion").val(data.arc_descripcion);

            $("#Editararc_empresa_id_i").val(data.arc_empresa_id_i);
            $("#Editararc_empresa_id_i").val(data.arc_empresa_id_i).val();

			$("#EditarId").val(data.arc_id);
			
			if(data.arc_ruta_1 != null && data.arc_ruta_1 != ''){
				$("#formato1").val(data.arc_ruta_1);
				var ruta = data.arc_ruta_1;
				var tipoArchivo  = ruta.split('.')[1];
                console.log(tipoArchivo);           
                if(tipoArchivo == 'pdf'){
                   $(".previsualizar_2").attr('src', "vistas/img/plantilla/pdf.png");
                }else{
                	$(".previsualizar_2").attr('src', data.arc_ruta_1);   
                }
		
			}

			if(data.arc_ruta_2 != null && data.arc_ruta_2 != ''){
				$("#formato2").val(data.arc_ruta_2);
				var ruta = data.arc_ruta_2;
				var tipoArchivo  = ruta.split('.')[1];
                                        
                if(tipoArchivo == 'pdf'){
                   $(".previsualizar_3").attr('src', "vistas/img/plantilla/pdf.png");
                }else{
                	$(".previsualizar_3").attr('src', data.arc_ruta_2);   
                }
			}
        }

    });
});


/* Eliminar archivos */
$('#tablaArchivos tbody').on("click", ".btnEliminarArchivo", function(){
    var x = $(this).attr('idArchivo');
    swal.fire({
        title: '¿Está seguro de borrar la entidad?',//titulo de alerta
        text: "¡Si no lo está, puede cancelar la accíón!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrala!'
    }).then(function (result) {
        if (result.value) {
          
            var datas = new FormData();
            datas.append('id_Archivo', x);
            $.ajax({
                url   : 'ajax/archivos.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
            complete:function(){
                $.unblockUI();
            },
            success: function(data){
                if(data.code == '1'){
                    alertify.success( data.desc );    
                }else{
                    alertify.error( data.desc );
                } 
            },
            error: function(){
                //after_save_error();
                alertify.error('Error al realizar el proceso');
            }
        });
        }
    })
});


/*Nuevo Archivo*/
  /*Implementar la funcion AJax la de aqui es NuevoEntidad*/
    //Se crean un array con los datos a enviar, apartir del formulario
$("#btnNuevoArchivo").click(function(){
    var formData = new FormData($("#AgragrNuevoArchivo")[0]);
    $.ajax({
        url   : 'ajax/archivos.ajax.php',
        method: 'post',
        data  : formData,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend:function(){
            $.blockUI({ 
                baseZ: 2000,
                css: { 
                    border: 'none', 
                    padding: '1px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                } 
            }); 
        },
        complete:function(){
            tabla_archivos.ajax.reload();
            $("#modalAgregarArchivos").modal('hide');  
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data.code == '1'){
                alertify.success( data.desc );    
            }else{
                alertify.error( data.desc );
            } 
        },
        //si ha ocurrido un error
        error: function(){
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});

/*Editar Archivo*/
/*Implementar la funcion AJax la de aqui es EditarEntidad*/
$("#btnEditarArchivo").click(function(){
    var formData = new FormData($("#EditarArchivoExistente")[0]);
    $.ajax({
        url   : 'ajax/archivos.ajax.php',
        method: 'post',
        data  : formData,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend:function(){
            $.blockUI({ 
                baseZ: 2000,
                css: { 
                    border: 'none', 
                    padding: '1px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                } 
            }); 
        },
        complete:function(){
            tabla_archivos.ajax.reload();
            $("#modalEditarArchivos").modal('hide');  
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data.code == '1'){
                alertify.success( data.desc );    
            }else{
                alertify.error( data.desc );
            } 
        },
        //si ha ocurrido un error
        error: function(){
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});
 

function cargarImagenes(){

    var imgTabla = $(".imgAdj1");
    var hrefPrimerDoc = $(".hrefPrimerAdjunto");

    for(var i = 0; i < imgTabla.length; i ++){

        var data = tabla_archivos.row( $(imgTabla[i]).parents("tr")).data(); 
        if(data[4] != null && data[4] != '0' && data[4] != ''){
            $(imgTabla[i]).attr("src", 'vistas/img/plantilla/pdf.png');
        }
    }

    for(var i = 0; i < hrefPrimerDoc.length; i ++){
        var data2 = tabla_archivos.row( $(hrefPrimerDoc[i]).parents("tr")).data(); 
        if(data2[4] != null && data2[4] != '0' && data2[4] != ''){
            $(hrefPrimerDoc[i]).attr("href", data2[4]);
        }
    }

    
    var imgLog = $(".imgAdj2");
    var hrefSegundoDoc = $(".hrefSegundoAdjunto");
    for(var i = 0; i < imgLog.length; i ++){
        var data = tabla_archivos.row( $(imgLog[i]).parents("tr")).data(); 
        if(data[5] != null && data[5] != '0' && data[5] != ''){
            $(imgLog[i]).attr("src", 'vistas/img/plantilla/pdf.png');
        }
    }

    for(var i = 0; i < hrefSegundoDoc.length; i ++){
        var data2 = tabla_archivos.row( $(hrefSegundoDoc[i]).parents("tr")).data(); 
        if(data2[5] != null && data2[5] != '0' && data2[5] != ''){
            $(hrefSegundoDoc[i]).attr("href", data2[8]);
        }
    }
}

/*=============================================
CARGAMOS LAS IMÁGENES CUANDO ENTRAMOS A LA PÁGINA POR PRIMERA VEZ
=============================================*/
setTimeout(function(){
    cargarImagenes();
},1000);

/*=============================================
CARGAMOS LAS IMÁGENES CUANDO INTERACTUAMOS CON EL PAGINADOR
=============================================*/
$(".dataTables_paginate").click(function(){
    cargarImagenes();
});

/*=============================================
CARGAMOS LAS IMÁGENES CUANDO INTERACTUAMOS CON EL BUSCADOR
=============================================*/
$("input[aria-controls='tablaArchivos']").focus(function(){
    $(document).keyup(function(event){
        event.preventDefault();
        cargarImagenes();
    });
})

// this one now works with the '.dt' appended to the end.
tabla_archivos.on('responsive-resize.dt', function ( e, datatable, columns ) {
    var count = columns.reduce( function (a,b) {
        return b === false ? a+1 : a;
    }, 0 );
});
// this now works as well with the '.dt' appended
tabla_archivos.on( 'responsive-display.dt', function ( e, datatable, row, showHide, update ) {
    console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
});

/*=============================================
CARGAMOS LAS IMÁGENES CUANDO INTERACTUAMOS CON EL FILTRO DE CANTIDAD
=============================================*/
$("select[name='tablaArchivos_length']").change(function(){
    console.log('LLegamos');
    cargarImagenes();
});

/*=============================================
CARGAMOS LAS IMÁGENES CUANDO INTERACTUAMOS CON EL FILTRO DE ORDENAR
=============================================*/

$(".sorting").click(function(){
    cargarImagenes();
});




