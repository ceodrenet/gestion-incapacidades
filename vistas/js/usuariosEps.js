/*$("#NuevoIpsId").select2({
    dropdownParent: $("#modalAdicionarUsuarioEps")
});*/

var edicion =  '<div class="btn-group dropstart" role="group">';
edicion += '<button id="btnGroupVerticalDrop1" type="button"  class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
edicion += '<i class="fa fa-info-circle"></i>';
//edicion += '<span class="sr-only">Toggle Dropdown</span>';
edicion += '</button>';
edicion += '<ul class="dropdown-menu" role="menu">';
edicion += '<li><a class="dropdown-item btnEditarUsuarioDeEps" title="Editar Usuario" idUsuarioEps data-bs-toggle="modal" data-bs-target="#modalEditarUsuarioEps" href="#">EDITAR</a></li>';
edicion += '<li class="dropdown-divider"></li>';
edicion += '<li><a class="dropdown-item btnEliminarUsuarioDeEps" title="Eliminar Usuario" idUsuarioEps href="#">ELIMINAR</a></li>';
edicion +='</ul>';
edicion +='</div>';

var tablaUsuariosEps = $('#tablaUsuariosEps').DataTable({
    "ajax": 'ajax/usuariosEps.ajax.php?getDataUsuariosEps',
    "columnDefs": [
        {
            "targets": -2,
            "data": null,
            render: {
                display: function (data, type, row) {
                    
                    var edicion = '<a href="'+row[3]+'" target="_blank">Link Pagina Administradora</a>';
                    return edicion;
                }
            }
        },
        {
            "targets": -1,
            "data": null,
            render: {
                display: function (data, type, row) {

                    return edicion;
                }
            }
        }
    ],
    "language" : {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    } 
});

$('#tablaUsuariosEps tbody').on( 'click', 'a', function () {
    var current_row = $(this).parents('tr');//Get the current row
    if (current_row.hasClass('child')) {//Check if the current row is a child row
        current_row = current_row.prev();//If it is, then point to the row before it (its 'parent')
    }
    var data = tablaUsuariosEps.row( current_row ).data();
    $(this).attr("idUsuarioEps", data[4]);
   
} );

/* Eliminar Entidades */
$('#tablaUsuariosEps tbody').on("click", ".btnEliminarUsuarioDeEps", function () {
    var x = $(this).attr('idUsuarioEps');
    swal.fire({
        title: '¿Está seguro de borrar el usuario?',
        text: "¡Si no lo está puede cancelar la accíón!",
        /* type: 'warning',*/
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borralo!'
    }).then(function (result) {
        if (result.value) {
            //window.location = "index.php?ruta=usuariosEps&id_usuario_eps="+x ;
            var datas = new FormData();
            datas.append('id_usuario_eps', x);
            $.ajax({
                url: 'ajax/usuariosEps.ajax.php',
                method: 'post',
                data: datas,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                beforeSend: function () {
                    $.blockUI({
                        baseZ: 2000,
                        css: {
                            border: 'none',
                            padding: '1px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
                },
                complete: function () {
                    tablaUsuariosEps.ajax.reload();
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function (data) {
                    if (data.code == '1') {
                        alertify.success(data.desc);

                    } else {
                        alertify.error(data.desc);
                    }
                },
                //si ha ocurrido un error
                error: function () {
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        }
    })
});




/* Editar Usuario*/
$('#tablaUsuariosEps tbody').on("click", ".btnEditarUsuarioDeEps", function () {
    var x = $(this).attr('idUsuarioEps');
    var datas = new FormData();
    datas.append('EditarUsuarioEpsId', x);
    $.ajax({
        url: 'ajax/usuariosEps.ajax.php',
        method: 'post',
        data: datas,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (data) {

            $("#EditarIpsId").val(data.use_ips_id_i);
            $("#EditarIpsId").val(data.use_ips_id_i).change();
            $("#EditarNombreUsuario").val(data.use_usuario_v);
            $("#EditarPasswoUsuario").val(data.use_password_v);
            $("#EditarUrl___Usuario").val(data.use_url_v);
            $("#EditarIdUsuario").val(data.use_id_i);
            $("#editarObservacion").val(data.use_observacion_v);
            $("#editarEmpresa").val(data.use_emp_id_i).change();
        }

    });
});


//guardar usuarios
$("#btnNuevoUsuarioEps").click(function () {
    var formData = new FormData($("#AgragrNuevoUsuarioEps")[0]);
    $.ajax({
        url: 'ajax/usuariosEps.ajax.php',
        method: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function () {
            $.blockUI({
                baseZ: 2000,
                css: {
                    border: 'none',
                    padding: '1px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        },
        complete: function () {
            tablaUsuariosEps.ajax.reload();
            $("#modalAdicionarUsuarioEps").modal('hide');
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function (data) {
            if (data.code == '1') {
                alertify.success(data.desc);
            } else {
                alertify.error(data.desc);
            }
        },
        //si ha ocurrido un error
        error: function () {
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});


//editar usuarios
$("#btnEditarUsuarioDeEps").click(function () {
    var formData = new FormData($("#EditarUsuarioEps")[0]);
    $.ajax({
        url: 'ajax/usuariosEps.ajax.php',
        method: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        beforeSend: function () {
            $.blockUI({
                baseZ: 2000,
                css: {
                    border: 'none',
                    padding: '1px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        },
        complete: function () {
            tablaUsuariosEps.ajax.reload();
            $("#modalEditarUsuarioEps").modal('hide');
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function (data) {
            if (data.code == '1') {
                alertify.success(data.desc);
            } else {
                alertify.error(data.desc);
            }
        },
        //si ha ocurrido un error
        error: function () {
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});
