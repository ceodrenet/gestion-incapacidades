var edicion =  '<div class="btn-group dropstart" role="group">';
edicion += '<button id="btnGroupVerticalDrop1" type="button"  class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
edicion += '<i class="fa fa-info-circle"></i>';
edicion += '</button>';
edicion += '<ul class="dropdown-menu" role="menu">';
edicion += '<li><a class="dropdown-item btnEditarEmpleado" title="Editar Empleado" idEmpleado data-bs-toggle="modal" data-bs-target="#modalEditarEmpleado" href="#">EDITAR</a></li>';
edicion += '<li class="dropdown-divider"></li>';
edicion += '<li><a class="dropdown-item btnEliminarEmpleado" title="Eliminar Empleado" idEmpleado href="#">ELIMINAR</a></li>';
edicion +='</ul>';
edicion +='</div>';

var table = $('#tablaEmpleadosX').DataTable({
    "ajax": 'ajax/datatables.ajax.php',
    "language": {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});

$('#tablaEmpleadosX tbody').on('click', 'button', function() {
    var data = table.row($(this).parents('tr')).data();
    $(this).attr("id_empleado", data[8]);
});


/* Editar Empleados */
$('#tablaEmpleadosX tbody').on("click", ".btnEditarEmpleado", function() {
    var x = $(this).attr('id_empleado');
    var datas = new FormData();
    datas.append('EditarEmpleadoId', x);
    $.ajax({
        url: 'ajax/empleados.ajax.php',
        method: 'post',
        data: datas,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
            $("#EditarNombre").val(data.emd_nombre);
            $("#EditarCedula").val(data.emd_cedula);
            $("#EditarFechaIngreso").val(data.emd_fecha_ingreso);
            $("#EditarSalario").val(data.emd_salario);
            $("#Editar_idEmpleado").val(data.emd_id);


            $("#EditarTipoIdentificacion").val(data.emd_tipo_identificacion).change();

            $("#EditarGenero").val(data.emd_genero).change();
            $("#EditarTipoEmpleado").val(data.emd_tipo_empleado).change();


            $("#EditarFechaRetiro").val(data.emd_fecha_retiro);
            $("#EditarSalarioPromedio").val(data.emd_salario_promedio);
            $("#EditarFechaNacimiento").val(data.emd_fecha_nacimiento);
            $("#EditarCargo").val(data.emd_cargo);
            $("#EditarSede").val(data.emd_sede);
            $("#EditarEps").val(data.emd_eps_id);

            $("#EditarFechaAfiliacionEps").val(data.emd_fecha_afiliacion_eps);
            $("#EditarAfp").val(data.emd_afp_id);
            $("#EditarArl").val(data.emd_arl_id);
            $("#EditarFechaCalificacionPCL").val(data.emd_fecha_calificacion_PCL);
            $("#EditarEntidadCalificadora").val(data.emd_entidad_calificadora);
            $("#EditarDiagnostico").val(data.emd_diagnostico);


            $("#EditarCodigoNomina").val(data.emd_codigo_nomina);
            $("#EditarCentroDecostos").val(data.emd_centro_costos);
            $("#EditarSubcentroCostos").val(data.emd_subcentro_costos);
            $("#EditarCiudadNomina").val(data.emd_ciudad);
            $("#EditarEstado").val(data.emd_estado);
            $("#EditarEstado").val(data.emd_estado).change();


            if (data.emd_ruta_cedula != null && data.emd_ruta_cedula != '') {
                $("#ruta_cedula").val(data.emd_ruta_cedula);
                ruta = data.emd_ruta_cedula.split(".")[1];
                if (ruta == 'pdf') {
                    $(".previsualizar").attr('src', 'vistas/img/plantilla/pdf.png');
                } else {
                    $(".previsualizar").attr('src', data.emd_ruta_cedula);
                }

            }

            if (data.emd_ruta_otro != null && data.emd_ruta_otro != '') {
                $("#ruta_otroDocumento").val(data.emd_ruta_otro);
                ruta = data.emd_ruta_otro.split(".")[1];
                if (ruta == 'pdf') {
                    $(".previsualizar_2").attr('src', 'vistas/img/plantilla/pdf.png');
                } else {
                    $(".previsualizar_2").attr('src', data.emd_ruta_otro);
                }

            }



        }

    });
});


/* Eliminar Empleados */
$('#tablaEmpleadosX tbody').on("click", ".btnEliminarEmpleado", function() {
    var x = $(this).attr('id_empleado');
    var n = $('#Nuevo_idEmpresa').val();
    var i = $('#Nuevo_NombreEmpresa').val();
    swal({
        title: '¿Está seguro de borrar el Empleado?',
        text: "¡Si no lo está puede cancelar la accíón!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar empleado!'
    }, function(isConfirm) {
        if (isConfirm) {
            window.location = "index.php?ruta=empleados&id_Empleado=" + x + "&id=" + n + "&nombre=" + i;
        }
    })
});


/* Activar Empleados */
$('#tablaEmpleadosX tbody').on("click", ".btnDocumentosEmpleado", function() {
    var idUsuario = $(this).attr('id_Empleado');
    var datos = new FormData();
    datos.append("EditarEmpleadoId", idUsuario);
    $.ajax({
        url: 'ajax/empleados.ajax.php',
        type: 'post',
        data: datos,
        cache: false,
        contentType: false,
        dataType: 'json',
        processData: false,
        success: function(data) {

            if (data.emd_ruta_cedula != null && data.emd_ruta_cedula != '') {
                tipoArchivo = data.emd_ruta_cedula.split('.')[1];
                if (tipoArchivo == 'pdf') {
                    $("#cedula").attr('href', data.emd_ruta_cedula);
                    $(".previsualizar_M").attr('src', 'vistas/img/plantilla/pdf.png');
                } else {
                    $("#cedula").attr('href', data.emd_ruta_cedula);
                    $(".previsualizar_M").attr('src', data.emd_ruta_cedula);
                }
            }

            if (data.emd_ruta_otro != null && data.emd_ruta_otro != '') {
                tipoArchivo = data.emd_ruta_otro.split('.')[1];
                if (tipoArchivo == 'pdf') {
                    $("#cedula").attr('href', data.emd_ruta_otro);
                    $(".previsualizar_M").attr('src', 'vistas/img/plantilla/pdf.png');
                } else {
                    $("#cedula").attr('href', data.emd_ruta_cedula);
                    $(".previsualizar_2_M").attr('src', data.emd_ruta_otro);
                }
            }
        }
    });
});