
/* visualizar las fotos al cargarlas */
/* validar los tipos de imagenes y/o archivos */
$(".NuevaFoto").change(function(){
    var imax = $(this).attr('valor');
    var imagen = this.files[0];
    console.log(imagen);
    /* Validar el tipo de imagen */
    if(imagen['type'] != 'image/jpeg' && imagen['type'] != 'image/png' && imagen['type'] != "application/pdf" ){
        $(".NuevaFoto").val('');
        alertify.error("El archivo debe estar en formato PNG , JPG, PDF");
    }else if(imagen['size'] > 2000000 ) {
        $(".NuevaFoto").val('');
        alertify.error("El archivo no debe pesar mas de 2MB");
    }else{
        if(imagen['type'] == 'image/jpeg' || imagen['type'] == 'image/png' || imagen['type'] != "application/pdf"){
            var datosImagen = new FileReader();
            datosImagen.readAsDataURL(imagen);

            $(datosImagen).on("load", function(event){
                var rutaimagen = event.target.result;
                if(imax == '0'){
                    $(".previsualizar").attr('src', rutaimagen);
                }else{
                    $(".previsualizar"+imax).attr('src', rutaimagen);
                }
                
                
            }); 
        }
        
    }   
});

/* validar los tipos de imagenes y/o archivos */
$(".NuevoExell").change(function(){
    var imax = $(this).attr('valor');
    var imagen = this.files[0];
    console.log(imagen);
    /* Validar el tipo de imagen */
    if(imagen['type'] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ){
        $(".NuevoExell").val('');
        alertify.error("El archivo debe estar en formato XLS");
    }else if(imagen['size'] > 3000000 ) {
        $(".NuevoExell").val('');
        alertify.error("El archivo no debe pesar mas de 3M");
    }else{
        if(imagen['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
  
        }
    }
});


 let edicion = '<div class="btn-group dropstart" role="group">';
edicion += '<button type="button" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
edicion += '<i class="fa fa-info-circle"></i>';
edicion += '</button>';
edicion += '<div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">';
edicion += '<a class="dropdown-item btnDocumentosEmpleado" id_Incapacidad title="Documentos del empleado" id_empleado data-bs-toggle="modal" data-bs-target="#modalDocumentosEmpleado">DOCUMENTOS</a>';
edicion += '<div class="dropdown-divider"></div>';


if($("#editar").val() == '1'){
    edicion += '<a class="dropdown-item btnEditarEmpleado" title="Editar Empleado" id_empleado data-bs-toggle="modal" data-bs-target="#modalEditarEmpleado">EDITAR</a>';
    edicion += '<div class="dropdown-divider"></div>';
}

var eliminacion = '';
if($("#elimina").val() == '1'){
    edicion += '<a class="dropdown-item btnEliminarEmpleado" title="Eliminar Empleado" id_empleado codigo imagen>ELIMINAR</a>';
}


var table = $('#tablaEmpleados').DataTable({
    "ajax": 'ajax/datatables.ajax.php?empresId='+$("#session").val(),
    "info": true,
    "autoWidth": false,
    "responsive": true,
    "columnDefs": [
        {
            "className": "text-center",
            "targets": -1,
            "data": null,
            "defaultContent": edicion
        },
        {
            "targets": 3,
            "render": $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD/MM/YYYY')
        }
    ],
    "language" : {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    preDrawCallback: function() {
        let el = $('div.dataTables_filter label');
        if(!el.parent('form').length) {
            el.wrapAll('<form></form>').parent()
            .attr('autocomplete', false)
            .attr('autofill', false);
        }
    }
});


$('#tablaEmpleados tbody').on( 'click', 'a', function () {
    var current_row = $(this).parents('tr');//Get the current row
    if (current_row.hasClass('child')) {//Check if the current row is a child row
        current_row = current_row.prev();//If it is, then point to the row before it (its 'parent')
    }
    var data = table.row( current_row ).data();
    $(this).attr("id_empleado", data[7]);
});


/*Fechas***/
var NuevoFechaRetiro = flatpickr('#NuevoFechaRetiro', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});

var NuevoFechaIngreso = flatpickr('#NuevoFechaIngreso', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d",
    onChange: function(selectedDates, dateStr, instance) {
        NuevoFechaRetiro.set("minDate", dateStr);
    },
});
var NuevoFechaAfiliacionEps = flatpickr('#NuevoFechaAfiliacionEps', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
var NuevoFechaNacimiento = flatpickr('#NuevoFechaNacimiento', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
var NuevoFechaCalificacionPCL = flatpickr('#NuevoFechaCalificacionPCL', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});

var NuevoFechaSusp = flatpickr('#NuevoFechaSusp', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});

/**JGM-------FECHAS CASOS ESPECIALES**/
var NuevoFechaCaliciacion = flatpickr('#NuevoFechaCaliciacion', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
var NuevoFechaCalificacionJR = flatpickr('#NuevoFechaCalificacionJR', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
var NuevoFechaCalificacionJN = flatpickr('#NuevoFechaCalificacionJN', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
var EditarFechaSusp = flatpickr('#EditarFechaSusp', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
/*************************************************************************/
/*************JGM--FECHAS DE EDICION**************************************/
var EditarFechaRetiro = flatpickr('#EditarFechaRetiro', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
var EditarFechaNacimiento = flatpickr('#EditarFechaNacimiento', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});

var EditarFechaIngreso = flatpickr('#EditarFechaIngreso', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});



var EditarFechaAfiliacionEps = flatpickr('#EditarFechaAfiliacionEps', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
var EditarFechaCalificacionPCL = flatpickr('#EditarFechaCalificacionPCL', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
var EditarFechaCalificacionPCL = flatpickr('#EditarFechaCalificacionPCL', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});

var EditarFechaCalificacionPCL = flatpickr('#EditarFechaCalificacionPCL', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});

/***************************CASS ESPECIALES FECHAS ************************/
/**JGM-------FECHAS CASOS ESPECIALES**/
var EditarFechaConcepto = flatpickr('#EditarFechaConcepto', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
var EditarFechaCaliciacion = flatpickr('#EditarFechaCaliciacion', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
var EditarFechaCalificacionJR = flatpickr('#EditarFechaCalificacionJR', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});
var EditarFechaCalificacionJN = flatpickr('#EditarFechaCalificacionJN', {
    altInput: true,
    locale: "es",
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d"
});

/* Editar Empleados */
$('#tablaEmpleados tbody').on("click", ".btnEditarEmpleado", function(){
	var x = $(this).attr('id_empleado');
    var datas = new FormData();
    datas.append('EditarEmpleadoId', x);
    $.ajax({
        url   : 'ajax/empleados.ajax.php',
        method: 'post',
        data  : datas,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        success     : function(data){
            $("#EditarNombre").val(data.emd_nombre); 
			$("#EditarCedula").val(data.emd_cedula);			
			$("#EditarFechaIngreso").val(data.emd_fecha_ingreso);		
            EditarFechaIngreso.setDate(data.emd_fecha_ingreso);
			$("#EditarSalario").val(data.emd_salario);
			$("#Editar_idEmpleado").val(data.emd_id);
            $("#EditarTipoIdentificacion").val(data.emd_tipo_identificacion).change();
            $("#EditarGenero").val(data.emd_genero).change();
            $("#EditarTipoEmpleado").val(data.emd_tipo_empleado).change();
            $("#EditarFechaRetiro").val(data.emd_fecha_retiro);    
            EditarFechaRetiro.setDate(data.emd_fecha_retiro) ;       
            $("#EditarSalarioPromedio").val(data.emd_salario_promedio);       
            $("#EditarFechaNacimiento").val(data.emd_fecha_nacimiento);
            EditarFechaNacimiento.setDate(data.emd_fecha_nacimiento);
            $("#EditarCargo").val(data.emd_cargo);
            $("#EditarSede").val(data.emd_sede);
            $("#EditarEps").val(data.emd_eps_id);
            $("#EditarFechaAfiliacionEps").val(data.emd_fecha_afiliacion_eps); 
            EditarFechaAfiliacionEps.setDate(data.emd_fecha_afiliacion_eps);
            $("#EditarAfp").val(data.emd_afp_id);            
            $("#EditarArl").val(data.emd_arl_id);       
            $("#EditarFechaCalificacionPCL").val(data.emd_fecha_calificacion_PCL);
            $("#EditarEntidadCalificadora").val(data.emd_entidad_calificadora);
            $("#EditarDiagnostico").val(data.emd_diagnostico);
            $("#EditarTelefono").val(data.emd_telefono_v);
            $("#EditarCorreo").val(data.emd_correo_v);
            $("#EditarCodigoNomina").val(data.emd_codigo_nomina);            
            $("#EditarCentroDecostos").val(data.emd_centro_costos); 
            $("#EditarSubcentroCostos").val(data.emd_subcentro_costos);    
            $("#EditarCiudadNomina").val(data.emd_ciudad);
            $("#EditarEstado").val(data.emd_estado);
            $("#EditarEstado").val(data.emd_estado).change();
            $("#EditarFechaSusp").val(data.emd_fech_susp_salario);
             flatpickr('#EditarFechaSusp', {
                altInput: true,
                locale: "es",
                altFormat: "d/m/Y",
                dateFormat: "Y-m-d"
            });
            console.log(data.emd_fech_susp_salario);
             
          
            if (data.emd_caso_especial_i == "1"){
                $("#EditarCasosEspeciales").attr( "checked", true );
                $(".casosEspeciales").show();
            }else{
                $("#EditarCasosEspeciales").attr( "checked", false ); 
                $(".casosEspeciales").hide();
            }

            if (data.emd_susp_salario_i == "1"){
                $("#EditarSuspSalario").attr( "checked", true );
                $("#txtfechaSusp_editar").show();
                
            }else{
                $("#EditarSuspSalario").attr( "checked", false ); 
                $("#txtfechaSusp_editar").hide();
            }
           
    
            if(data.emd_ruta_cedula != null && data.emd_ruta_cedula != ''){
                $("#ruta_cedula").val(data.emd_ruta_cedula);
                ruta = data.emd_ruta_cedula.split(".")[1];
                if(ruta == 'pdf'){
                    $(".previsualizar").attr('src', 'vistas/img/plantilla/pdf.png');
                }else{
                    $(".previsualizar").attr('src', data.emd_ruta_cedula); 
                }
                
            }

            if(data.emd_ruta_otro != null && data.emd_ruta_otro != ''){
                $("#ruta_otroDocumento").val(data.emd_ruta_otro);
                ruta = data.emd_ruta_otro.split(".")[1];
                if(ruta == 'pdf'){
                    $(".previsualizar_2").attr('src', 'vistas/img/plantilla/pdf.png');
                }else{
                    $(".previsualizar_2").attr('src', data.emd_ruta_otro); 
                }
            }
        }
    });

    var datax = new FormData();
    datax.append("verificarCasoEspecial", x);
    $.ajax({
        url   : 'ajax/empleados.ajax.php',
        method: 'post',
        data  : datax,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        success     : function(data){
            if(data != false){
  
                    $(".casosEspeciales").show();
                    $("#EditarConceptoRehabilitacion").val(data.cae_con_rea_v);
                    $("#EditarConceptoRehabilitacion").val(data.cae_con_rea_v).change(); 
                    $("#EditarFechaConcepto").val(data.cae_xon_fecha_d);            
                    $("#EditarDiagnosticoConcepto").val(data.cae_condia_id_i);       
                    $("#EditarEntidadConcepto").val(data.cae_con_entidad_id_i);
                    $("#EditarEntidadConcepto").val(data.cae_con_entidad_id_i).change();
                    $("#EditarCalificacionPCLCS").val(data.cae_cal_v);
                    $("#EditarFechaCaliciacion").val(data.cae_cal_fecha_d);
                    $("#EditarDiagnosticoPCL").val(data.cae_cal_dia_id_i);
                    $("#EditarEntidadPCL").val(data.cae_cal_entidad_i).change();;
                    $("#EditarJuntaRegional").val(data.cae_cal_jur_v);            
                    $("#EditarFechaCalificacionJR").val(data.cae_cal_jur_fecha_d);       
                    $("#EditarDiagnosticoJR").val(data.cae_cal_jur_dia_id_i);
                    $("#EditarEntidadJR").val(data.cae_cal_jur_entidad_v);
                    $("#EditarCalificacionJN").val(data.cae_cal_jun_v);
                    $("#EditarFechaCalificacionJN").val(data.cae_cal_jun_fecha_d);
                    $("#EditarDiagnosticoJN").val(data.cae_cal_jun_dia_id_i); 
                    $("#EditarEntidadJN").val(data.cae_cal_jun_entidad_v);   
                    $("#NuevoTipoDecasoEdicion").val(data.cae_tic_id_i);    
                /*}else{
                    
                }*/
                //$("#EditarCasosEspeciales").isCheck('check');
               
               
            }                   
        }

    });
});


/* Eliminar Empleados */
$('#tablaEmpleados tbody').on("click", ".btnEliminarEmpleado", function(){
    var x = $(this).attr('id_empleado');
    var n = $('#Nuevo_idEmpresa').val();
    Swal.fire({
        title: '¿Está seguro de borrar el Empleado?',
        text: "¡Si no lo está puede cancelar la accíón!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar empleado!'
    }).then(function (result) {
        if (result.value) {
            var datas = new FormData();
            datas.append('id_Empleado', x);
            $.ajax({
                url   : 'ajax/empleados.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    
                    if(data.code == '1'){
                        alertify.success(data.desc);
                    }else{
                        alertify.error(data.desc);
                    }

                },
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    table.ajax.reload();
                    $.unblockUI();
                }

            });
        }
    })
});


/* Activar Empleados */
$('#tablaEmpleados tbody').on("click", ".btnDocumentosEmpleado", function(){
	var idUsuario = $(this).attr('id_Empleado');
	var datos = new FormData();
	datos.append("EditarEmpleadoId", idUsuario);
	$.ajax({
		url  : 'ajax/empleados.ajax.php',
		type : 'post',
		data : datos,
		cache: false,
		contentType:false,
        dataType : 'json',
		processData : false,
		success: function(data){

			if(data.emd_ruta_cedula != null && data.emd_ruta_cedula != ''){
                tipoArchivo  =  data.emd_ruta_cedula.split('.')[1];
                if(tipoArchivo == 'pdf'){
                    $("#cedula").attr('href' , data.emd_ruta_cedula);
                    $(".previsualizar_M").attr('src', 'vistas/img/plantilla/pdf.png');
                }else{
                    $("#cedula").attr('href' , data.emd_ruta_cedula);
                    $(".previsualizar_M").attr('src', data.emd_ruta_cedula);
                }
            }

            if(data.emd_ruta_otro != null && data.emd_ruta_otro != ''){
                tipoArchivo  = data.emd_ruta_otro.split('.')[1];
                if(tipoArchivo == 'pdf'){
                    $("#cedula").attr('href' , data.emd_ruta_otro);
                    $(".previsualizar_M").attr('src', 'vistas/img/plantilla/pdf.png');
                }else{
                    $("#cedula").attr('href' , data.emd_ruta_cedula);
                    $(".previsualizar_2_M").attr('src', data.emd_ruta_otro);
                }
            }
		}
	});
});


/* validar que el Nit no esta repetido */
$("#NuevoCedula").change(function(){
	$(".alert").remove();
	var usuario = $(this).val();
	var datos = new FormData();
	datos.append('validarNit', usuario);
    datos.append('empresaValidate', $("#empresa").val());
	$.ajax({
		url   : 'ajax/empleados.ajax.php',
		method: 'post',
		data  : datos,
		cache : false,
		contentType : false,
		processData : false,
		dataType    : 'json',
		success     : function(respuesta){
			console.log(respuesta);
			if(respuesta != false){
				$("#NuevoCedula").parent().after('<div class="alert alert-warning">Esta identificación ya existe en la base de datos</div>');
				$("#NuevoCedula").val('');
			}
		}

	})

});

//$(".numerico").numeric();


/*$(document).ready(function() 
{
    $('#formularioCargaMassivaEmpleados').submit(function() 
    {
        $('#loader').show();
    }) 
})*/


/*
Carga massiva de empleados
 */

$("#cargarDatosEmpleados").click(function(){
     if($("#NuevoEmpleados").val() == ''){
        alertify.error("No se ha seleccionado un archivo!");
    }else{
        var form = $("#formularioCargaMassivaEmpleados");
        //Se crean un array con los datos a enviar, apartir del formulario 
        var formData = new FormData($("#formularioCargaMassivaEmpleados")[0]);
        //url: 'http://127.0.0.1:8000/empleados/importar'
        $.ajax({
            url: 'https://incapacidades.app/services/exports/empleados/importar',
            type  : 'post',
            data: formData,
            dataType : 'json',
            cache: false,
            contentType: false,
            processData: false,
            beforeSend:function(){
                $.blockUI({ 
                    desc : '<h3>Un momento por favor....</h3>',
                    baseZ: 2000,
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff' 
                    } 
                }); 
            },
            complete:function(){
                $.unblockUI();
            },
            //una vez finalizado correctamente
            success: function(data){

                alertify.success(data.msg);
               
                /*if(data.fallaron > 0){
                    window.location.href = 'index.php?exportar=fallosIncapacidad';
                } */  

                $("#formularioCargaMassivaEmpleados")[0].reset();  
                $("#modalAgregarEmpleados").modal('hide');   

                setTimeout(function(){
                    window.location.reload(true);
                },3000);
            },
            //si ha ocurrido un error
            error: function(){
                //after_save_error();
                alertify.error('Error al realizar el proceso');
            }
        });
    }
});

    $("#cargarIblEmpleados").click(function(){
        if($("#NuevoEmpleados").val() == ''){
            swal({
                title: 'Espera!',
                text: "¡No se ha seleccionado un archivo!",
                type: 'warning',
                showConfirmButton: true,
                confirmButtonText: "Cerrar",
                closeOnConfirm: false
            });
        }else{
            var form = $("#formularioIblassivaEmpleados");
            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#formularioIblassivaEmpleados")[0]);
            // http://127.0.0.1:8000/empleados/editar-importar
            $.ajax({
                url: 'https://incapacidades.app/services/exports/empleados/editar-importar',
                type  : 'post',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                   
                    // alertify.success('Proceso terminado, total registros '+ data.total +', se cargaron con exito '+ data.exito +', Datos Errados '+ data.fallaron+ ", No existen "+data.Noexiste );
                    alertify.success(data.msg);

                    $("#formularioIblassivaEmpleados")[0].reset();  
                    $("#modalEditarIblEmpleados").modal('hide'); 
                    setTimeout(function(){
                        window.location.reload(true);
                    },3000);
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        } 
    });

    $("#btnGuardarEmpleados").click(function(){
        var form = $("#formGuardarEmpleados");
        //Se crean un array con los datos a enviar, apartir del formulario 
        var formData = new FormData($("#formGuardarEmpleados")[0]);
        $.ajax({
            url: 'ajax/empleados.ajax.php',
            type  : 'post',
            data: formData,
            dataType : 'json',
            cache: false,
            contentType: false,
            processData: false,
            beforeSend:function(){
                $.blockUI({ 
                    baseZ: 2000,
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff'
                    } 
                }); 
            },
            complete:function(){
                table.ajax.reload();
                $("#modalAgregarEmpleado").modal('hide');
                $.unblockUI();
            },
            //una vez finalizado correctamente
            success: function(data){
                if(data.code == '1'){
                    alertify.success( data.desc );    
                }else{
                    alertify.error( data.desc );
                }
            },
            //si ha ocurrido un error
            error: function(){
                //after_save_error();
                alertify.error('Error al realizar el proceso');
            }
        });  
    });

    $("#btnEditarEmpleados").click(function(){
        var form = $("#formEditarEmpleados");
        //Se crean un array con los datos a enviar, apartir del formulario 
        var formData = new FormData($("#formEditarEmpleados")[0]);
        $.ajax({
            url: 'ajax/empleados.ajax.php',
            type  : 'post',
            data: formData,
            dataType : 'json',
            cache: false,
            contentType: false,
            processData: false,
            beforeSend:function(){
                $.blockUI({ 
                    baseZ: 2000,
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff'
                    } 
                }); 
            },
            complete:function(){
                table.ajax.reload();
                $("#modalEditarEmpleado").modal('hide');
                $.unblockUI();
            },
            //una vez finalizado correctamente
            success: function(data){
                if(data.code == '1'){
                    alertify.success( data.desc );    
                }else{
                    alertify.error( data.desc );
                }
            },
            //si ha ocurrido un error
            error: function(){
                //after_save_error();
                alertify.error('Error al realizar el proceso');
            }
        });
    });

    $("#ExportarEmpleadosInc").click(function(){
        $.ajax({
            url    : 'index.php?exportar=incEmpleadosInc',
            type   : 'post',
            dataType : 'json',
            success : function(data){
                var currentdate = new Date();
                var fecha_hora = currentdate.getFullYear() + rellenar((currentdate.getMonth()+1), 2) +  rellenar(currentdate.getDate(), 2) + "_" + rellenar(currentdate.getHours(), 2) +  rellenar(currentdate.getMinutes(), 2) + rellenar(currentdate.getSeconds(), 2);
                var $a = $("<a>");
                $a.attr("href",data.file);
                $("body").append($a);
                $a.attr("download","consolidado_empleados_"+fecha_hora+".xlsx");
                $a[0].click();
                $a.remove();
            },
            beforeSend:function(){
                $.blockUI({
                    message : '<h3>Un momento por favor....</h3>', 
                    baseZ: 2000,
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff' 
                    } 
                }); 
            },
            complete:function(){
                $.unblockUI();
            }
        });
    });

    $("#EditarCasosEspeciales").on('change', function(event) {
        if($(this).is(':checked')){
            $(".casosEspeciales").show();
        }else{
            $(".casosEspeciales").hide();
        }
    });

    $("#NuevoCasosEspeciales").on('change', function() {
        if($(this).is(':checked')){
            $(".casosEspeciales").show();
        }else{
            $(".casosEspeciales").hide();
        }
    });

    $("#NuevoSuspSalario").on('change', function() {
        if($(this).is(':checked')){
            $("#txtfechaSusp").show();
        }else{
            $("#txtfechaSusp").hide();
        }
    });

    $("#EditarSuspSalario").on('change', function() {
        if($(this).is(':checked')){
            $("#txtfechaSusp_editar").show();
        }else{
            $("#txtfechaSusp_editar").hide();
        }
    });

    function rellenar (str, max) {
        str = str.toString();
        return str.length < max ? rellenar("0" + str, max) : str;
    }