

    $.fn.datepicker.dates['es'] = {
        days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
        daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        today: "Today",
        clear: "Clear",
        format: "yyyy-mm-dd", 
        weekStart: 0
    };

    $("#NuevoFechaInicio_CE").datepicker({
        language: "es",
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#NuevoFechaFinal_CE').datepicker('setStartDate', minDate);
    });


    $("#NuevoFechaFinal_CE").datepicker({
        language: "es",
        autoclose: true,
        todayHighlight: true
    });


    $("#btnBuscarRangos").click(function(){
 
        var fechaInicio = $("#NuevoFechaInicio_CE").val();
        var fechaFinal  = $("#NuevoFechaFinal_CE").val();
        var paso = 0;
        if(fechaFinal.length < 0){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha final es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });

        }   

        if(fechaInicio.length < 1){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha Inicial es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
        }

        if(paso == 0){
            $.ajax({
                url    : 'vistas/modulos/nomina/consolidado_general.php',
                type   : 'post',
                data   :{
                    fechaInicial  : fechaInicio,
                    fechaFinal    : fechaFinal,
                    cliente_id    : $("#session").val()
                },
                dataType : 'html',
                success : function(data){
                    $("#resultados").html(data);
                    $("#btnExportarRangos").attr('href', 'index.php?exportarNomina=true&fechaInicial='+ fechaInicio +'&fechaFinal='+ fechaFinal)
                    $("#descargador").show();
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            })
        }
    });

