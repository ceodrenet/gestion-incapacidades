/*Validacion de Session*/

$(function(){
    const element = document.querySelector('#selEmpresas');
    const selEmpresa = new Choices(element);

    $('#selEmpresas').change(function(){
        var proyecto = $(this).val();
        var datos = new FormData();
        datos.append('idempresas_session', proyecto);
        setDatosSession(datos);
    });

    $('#selAnhos').change(function(){
        var anho = $(this).val();
        var datos = new FormData();
        datos.append('anho_session', anho);
        setDatosSession(datos);
    });



    function setDatosSession(formData){
        $.ajax({
            url  : 'vistas/clientes.php',
            type : 'post',
            data : formData,
            cache: false,
            contentType:false,
            processData : false,
            success: function(data){
                location.reload();
            },
            beforeSend:function(){
                $.blockUI({ 
                    message : '<h3>Un momento por favor....</h3>',
                    baseZ: 2000,
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff' 
                    } 
                }); 
            },
            complete:function(){
                $.unblockUI();
            }
        });
    }

    function getDatoSessionActiva(){
        $.ajax({
            url   : 'ajax/validarSession.ajax.php',
            type  : 'post',
            dataType:'json',
            success : function(data){              
                if(data.code == 0 ) {
                    window.location = "ingreso";
                } 
            },
            error : function (){
                /*console.log('ocurrio un error , en archivo, revisa que viene con error 500');*/
            }
        });
    }

    let varValidacion = setInterval(getDatoSessionActiva,30000);
}); 
       