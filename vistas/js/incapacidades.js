var contadorFilas = 0;  
/*choises.js para poder buscar por AJAX*/
var config = {
    placeholderValue: 'Inicie escribiendo la palabra',
    shouldSort: false,
    searchFloor: 3,        // or whatever value makes sense for your data / API
    searchChoices: false,  // see bullet point 2 above
    duplicateItems: false,  // this is ignored, see bullet point 3 above
    removeItemButton: true,
};

var choices = new Choices("#NuevoDiagnostico", config);
var choiceE = new Choices("#EditarDiagnostico", config);
const selMedicos = new Choices('#NuevoProfesional', config);
const selMedicosEdit = new Choices('#EditarProfesional', config);
const selInstituciones = new Choices('#NuevoInstitucionGenera', config);
const selInstitucionesEdit = new Choices('#EditarInstitucionGenera', config);
var apiUrl = 'ajax/diagnosticos.ajax.php?getDiagnosticos=true';
var apiUrlMed = 'ajax/medicos.ajax.php?getMedicosSelect=true';
var apiUrlInt = 'ajax/registroEntidad.ajax.php?getEntidadesSelect=true';
var lookupDelay = 250;
var lookupTimeout = null;
var lookupCache = {};


$(function(){

    /**Editar**/
    /*Esto es lo que reemlaza al datepicker*/ 
    /*Lo que tiene en #ya sabes que es un ID */
        var EditarFechaFinal = flatpickr('#EditarFechaFinal', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
          onChange: function(selectedDates, dateStr, instance){
            var minDate = dateStr;
            var fecha1 = moment($("#EditarFechaInicio").val());
            var fecha2 = moment(minDate);
            $("#EditarNumeroDias").val( (fecha2.diff(fecha1, 'days') + 1) + " Días");
          }
        });

        var EditarFechaInicio = flatpickr('#EditarFechaInicio', {
            altInput: true,
            locale: "es",
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            onChange: function(selectedDates, dateStr, instance) {
                EditarFechaFinal.set("minDate", dateStr);
                EditarFechaFinal.setDate(dateStr);
            },
        });

        /*Hasta qui tienes paralos dos del reporte**/
        var EditarFechaRecepcion = flatpickr('#EditarFechaRecepcion', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        });

        var EditartxtFechaPagoNomina = flatpickr('#EditartxtFechaPagoNomina', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        });

        var EditarFechaPagoIncapacidad = flatpickr('#EditarFechaPagoIncapacidad', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        });

        var EditarFechaRadicacionIncapacidad = flatpickr('#EditarFechaRadicacionIncapacidad', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        }); 

        var EditarFechaSolicitudIncapacidad = flatpickr('#EditarFechaSolicitudIncapacidad', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        });
            
        var EditarFechaSinReconocimiento = flatpickr('#EditarFechaSinReconocimiento', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        });
        
    
    $("#filtrosIncapacidadAnual").on('change', function(){
        var _miValor = $(this).val();
        $.ajax({
            url : 'vistas/modulos/incapacidades/incapacidades_filtroAnual.php',
            type: 'post',
            data: {
                yearFilter : _miValor
            },
            success : function(data){
                $("#FieldsFilters").html(data);
            }
        });
    });

    var tablaIncapa = $('#tbl_Incapacidades').DataTable({
        "processing": true,
        "serverSide": true,
        "info": true,
        "ajax": {
            "url": 'ajax/dataTablesIncapacidades.ajax.php?getAllData',
            "type": "POST"
        },
        "columns": [
            { "data": "inc_id" },
            { "data": "emd_cedula" },
            { "data": "emd_nombre" },
            { "data": "ips_nombre" },
            { "data": "inc_diagnostico" },
            { "data": "inc_fecha_inicio" },
            { "data": "inc_fecha_final" },
            { "data": "ori_sigla_v" },
            { "data": "per_descripcion_v", "className": "dt-center"},
            { "data": "est_tra_desc_i" },
            {}
        ],
        "columnDefs": [
            {
                "targets": 1,
                "data": null,
                render: {
                     display: function (data, type, row) {
                        if ( row.emd_estado !== '1' ) {
                            return '<span style="color:red;" title="COLABORADOR RETIRADO">'+row.emd_cedula+'</span>';
                        }else{
                            return '<span>'+row.emd_cedula+'</span>';
                        }
                    }
                }
            },
            {
                "targets": -3,
                "data": null,
                render: {
                     display: function (data, type, row) {
                        if ( row.per_descripcion_v === 'NO PERTENECE' ) {
                            return '<span style="color:red; font-size:16px !important;margin:0 auto 0;"><i class="fas fa-tired" title="NO PERTENECE"></i></span>';
                        }if ( row.per_descripcion_v === 'PERTENECE' ) {
                            return '<span style="color:green;font-size:16px !important;margin:0 auto 0;"><i title="PERTENECE" class="fas fa-smile-beam"></i></span>';
                        }else{
                            return '<span style="color:gray;font-size:16px !important;margin:0 auto 0;"><i title="NO VALIDADO" class="far fa-flushed"></i></span>';
                        }
                    }
                }
            },
            {
                "targets": -2,
                "data": null,
                render: {
                     display: function (data, type, row) {
                        if ( row.inc_estado === '0' ) {
                            return '<span style="color:red;">INCOMPLETA</span>';
                        }else{
                            return '<span>'+row.est_tra_desc_i+'</span>';
                        }
                    }
                }
            },
            {
                "targets": -1,
                "data": null,
                render: {
                    display: function (data, type, row) {
                        if ( row.inc_estado === '0' ) {
                            let edicion = '<div class="btn-group dropstart" role="group">';
                            edicion += '<button type="button" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                            edicion += '<i class="fa fa-info-circle"></i>';
                            edicion += '</button>';
                            edicion += '<div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">';
                            edicion += '<a class="dropdown-item btnVerIncapacidad" id_Incapacidad href="#" data-bs-toggle="modal" data-bs-target="#modalEditarrIncapacidadver">VER</a>';
                            edicion += '<div class="dropdown-divider"></div>';


                            if($("#editar").val() == '1'){
                                edicion += '<a class="dropdown-item btnEditarIncapacidad" title="Editar" id_Incapacidad data-bs-toggle="modal" data-bs-target="#modalEditarrIncapacidad" href="#">EDITAR</a></li>';
                            }

                            if($("#elimina").val() == '1'){
                                edicion += '<div class="dropdown-divider"></div>';
                                edicion += '<a class="dropdown-item btnEliminarIncapacidad" title="Eliminar" id_Incapacidad href="#">ELIMINAR</a></li>';
                            }
                            if(row.inc_ruta_incapacidad !== '' && row.inc_ruta_incapacidad !== null){
                                edicion += '<div class="dropdown-divider"></div>';
                                edicion += '<a class="dropdown-item" target="_blank" title="Ver Incapacidad Original" href="'+row.inc_ruta_incapacidad+'">VER SOPORTE</a>';
                            }

                            if(row.inc_ruta_incapacidad_transcrita !== '' && row.inc_ruta_incapacidad_transcrita !== null){
                                edicion += '<div class="dropdown-divider"></div>';
                                edicion += '<a class="dropdown-item" target="_blank" title="Ver Incapacidad Transcrita" href="'+row.inc_ruta_incapacidad_transcrita+'">VER TRANSCRIPCIÓN</a>';
                            }

                            edicion += '</div>';
                            edicion += '</div>';

                            let correoI = '<div class="btn-group dropstart" role="group">';
                            correoI += '<button type="button" type="button" class="btn btn-sm btn-outline-primary waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                            correoI += '<i class="fa fa-envelope"></i>';
                            correoI += '</button>';
                            correoI += '<div class="dropdown-menu  dropdownmenu-primary" aria-labelledby="btnGroupVerticalDrop1">';
                            correoI += '<a class="dropdown-item btnSendEpsCorreo" id_Incapacidad title="Radicar Incapacidad" href="#">RADICAR</a>';
                            correoI += '<div class="dropdown-divider"></div>';
                            correoI += '<a class="dropdown-item btnSendEpsSolicitud" id_Incapacidad title="Solicitar el pago" href="#">SOLICITUD</a>';
                            correoI += '<div class="dropdown-divider"></div>';
                            correoI += '<a class="dropdown-item btnSendSuperSalud" id_Incapacidad title="Enviar Incapacidad a SuperSalud" href="#">SUPERSALUD</a>';
                            correoI += '<div class="dropdown-divider"></div>';
                            correoI += '<a class="dropdown-item btnSendNotificacionEps" id_Incapacidad title="Notificar a Caja De Compensación" href="#">NOTIFICAR CC</a>';
                            correoI += '<div class="dropdown-divider"></div>';
                            correoI += '<a class="dropdown-item btnSendNotificacionIncompleta" id_Incapacidad title="Notificar Incapacidad Incompleta, Total Envios '+row.inc_numero_notificaciones_i+'" href="#">INCOMPLETA</a>';
                            correoI += '<div class="dropdown-divider"></div>';
                            correoI += '<a class="dropdown-item btnGeneratePQR" id_Incapacidad title="Generar PQR automatica" href="#">GENERAR PQR</a>';

                            correoI += '</div></div>';

                            return edicion+' '+correoI;

                        } else {
                            let edicionI = '<div class="btn-group dropstart" role="group">';
                            edicionI += '<button type="button" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                            edicionI += '<i class="fa fa-info-circle"></i>';
                            edicionI += '</button>';
                            edicionI += '<div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">';
                            edicionI += '<a class="dropdown-item btnVerIncapacidad" id_Incapacidad href="#" data-bs-toggle="modal" data-bs-target="#modalEditarrIncapacidadver">VER</a>';
                            edicionI += '<div class="dropdown-divider"></div>';


                            if($("#editar").val() == '1'){
                                edicionI += '<a class="dropdown-item btnEditarIncapacidad" title="Editar" id_Incapacidad data-bs-toggle="modal" data-bs-target="#modalEditarrIncapacidad" href="#">EDITAR</a>';
                            }

                            if($("#elimina").val() == '1'){
                                edicionI += '<div class="dropdown-divider"></div>';
                                edicionI += '<a class="dropdown-item btnEliminarIncapacidad" title="Eliminar" id_Incapacidad href="#">ELIMINAR</a>';
                            }

                            if(row.inc_ruta_incapacidad !== '' && row.inc_ruta_incapacidad !== null){
                                edicionI += '<div class="dropdown-divider"></div>';
                                edicionI += '<a class="dropdown-item" target="_blank" title="Ver Incapacidad Original" href="'+row.inc_ruta_incapacidad+'">VER SOPORTE</a>';
                            }

                            if(row.inc_ruta_incapacidad_transcrita !== '' && row.inc_ruta_incapacidad_transcrita !== null){
                                edicionI += '<div class="dropdown-divider"></div>';
                                edicionI += '<a class="dropdown-item" target="_blank" title="Ver Incapacidad Transcrita" href="'+row.inc_ruta_incapacidad_transcrita+'">VER TRANSCRIPCIÓN</a>';
                            }


                            edicionI += '</div>';
                            edicionI += '</div>';

                            let correo = '<div class="btn-group dropstart">';
                            correo += '<button type="button" type="button" class="btn btn-sm btn-outline-primary waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                            correo += '<i class="fa fa-envelope"></i>';
                            correo += '</button>';
                            correo += '<div class="dropdown-menu  dropdownmenu-primary" aria-labelledby="btnGroupVerticalDrop1">';
                            correo += '<a class="dropdown-item btnSendEpsCorreo" id_Incapacidad title="Radicar Incapacidad" href="#">RADICAR</a>';
                            correo += '<div class="dropdown-divider"></div>';
                            correo += '<a class="dropdown-item btnSendEpsSolicitud" id_Incapacidad title="Solicitar el pago" href="#">SOLICITUD</a>';
                            correo += '<div class="dropdown-divider"></div>';
                            correo += '<a class="dropdown-item btnSendNotificacionEps" id_Incapacidad title="Notificar a Caja De Compensación" href="#">NOTIFICAR CC</a>';
                            correo += '<div class="dropdown-divider"></div>';
                            correo += '<a class="dropdown-item btnSendSuperSalud" id_Incapacidad title="Enviar Incapacidad a SuperSalud" href="#">SUPERSALUD</a>';
                            correo += '<div class="dropdown-divider"></div>';
                            correo += '<a class="dropdown-item btnGeneratePQR" id_Incapacidad title="Generar PQR automatica" href="#">GENERAR PQR</a>';

                            correo += '</div></div>';
                            return edicionI+'&nbsp;'+correo;
                        }
                    },
                },
            },
            {
                "targets": 6,
                "render": $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD/MM/YYYY')
            } ,
            {
                "targets": 5,
                "render": $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD/MM/YYYY')
            } 
        ],
        "order": [[ 0, "desc" ]],
        "scrollX": true,
        "language" : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        preDrawCallback: function() {
            let el = $('div.dataTables_filter label');
            if(!el.parent('form').length) {
                el.wrapAll('<form></form>').parent()
                .attr('autocomplete', false)
                .attr('autofill', false);
            }
        }
    });


    $('#tbl_Incapacidades tbody').on( 'click', 'a', function () {
        var current_row = $(this).parents('tr');//Get the current row
        if (current_row.hasClass('child')) {//Check if the current row is a child row
            current_row = current_row.prev();//If it is, then point to the row before it (its 'parent')
        }
        var data = tablaIncapa.row( current_row ).data();
        $(this).attr("id_Incapacidad", data.inc_id);
       
    } );



    /* visualizar las fotos al cargarlas */
    /* validar los tipos de imagenes y/o archivos */
    $(".NuevaFoto").change(function(){
        var imax = $(this).attr('valor');
        var imagen = this.files[0];
        console.log(imagen);
        /* Validar el tipo de imagen */
        if(imagen['type'] != 'image/jpeg' && imagen['type'] != 'image/png' && imagen['type'] != "application/pdf" ){
            $(".NuevaFoto").val('');
            swal({
                title : "Error al subir el archivo",
                text  : "El archivo debe estar en formato PNG , JPG, PDF",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
        }else if(imagen['size'] > 2000000 ) {
            $(".NuevaFoto").val('');
            swal({
                title : "Error al subir el archivo",
                text  : "El archivo no debe pesar mas de 2MB",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
        }else{

            if(imagen['type'] == 'image/jpeg' || imagen['type'] == 'image/png' || imagen['type'] != "application/pdf"){
                var datosImagen = new FileReader();
                datosImagen.readAsDataURL(imagen);

                $(datosImagen).on("load", function(event){
                    var rutaimagen = event.target.result;
                    if(imax == '0'){
                        $(".previsualizar").attr('src', rutaimagen);
                    }else{
                        $(".previsualizar"+imax).attr('src', rutaimagen);
                    }    
                }); 
            }
            
        }   
    });

    /* validar los tipos de imagenes y/o archivos */
    $(".NuevoExell").change(function(){
        var imax = $(this).attr('valor');
        var imagen = this.files[0];
        console.log(imagen);
        /* Validar el tipo de imagen */
        /*if(imagen['type'] != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ){
            $(".NuevoExell").val('');
            alertify.error('El formato del archivo es xlsx');
        }else if(imagen['size'] > 3000000 ) {
            $(".NuevoExell").val('');
            alertify.error('El archivo debe pesar maximo 3 MB');
        }else{
            if(imagen['type'] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
      
            }
        } */
    });


        /*$(".Valores").numeric();*/

        

        /*
        Dehabilitar el valor empresa cuando no sea inicial
         */
        $("#EditarClasificacion").change(function(){
            if($(this).val() == '1'){
                $("#EditarValorPagadoEmpresa").attr('readonly', false);
            }else{
                $("#EditarValorPagadoEmpresa").attr('readonly', true);
            }
        });

        $("#NuevoClasificacion").change(function(){
            if($(this).val() == '1' || $(this).val() == '6' ){
                $("#NuevoValorPagadoEmpresa").attr('readonly', false);
            }else{
                $("#NuevoValorPagadoEmpresa").attr('readonly', true);
            }
        });

        /**Visualizar La información*/
        //Zona Correos
        

        $('#tbl_Incapacidades tbody').on("click", ".btnSendEpsSolicitud", function(){
            var x = $(this).attr('id_Incapacidad');
            var datas = new FormData();
            datas.append('sendMailSolicituIncapacidades', x);
            $.ajax({
                url   : 'ajax/enviarCorreoEpsSolicitud.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    if(data.code == '1'){
                        alertify.success('Correo enviado!');
                    }else{
                        alertify.error('No se pudo enviar el correo');
                    }
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }

            });
        });

        $('#tbl_Incapacidades tbody').on("click", ".btnSendSuperSalud", function(){
            var x = $(this).attr('id_Incapacidad');
            var datas = new FormData();
            datas.append('sendMailIncapacidades', x);
            $.ajax({
                url   : 'ajax/enviarCorreoSuperSalud.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    if(data.code == '1'){
                        alertify.success('Correo enviado!');
                    }else{
                        alertify.error('No se pudo enviar el correo');
                    }
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }

            });
        });

        $("#tbl_Incapacidades tbody").on('click', ".btnSendNotificacionEps", function(){
            var x = $(this).attr('id_Incapacidad');
            var datas = new FormData();
            datas.append('sendMailIncapacidades', x);
            $.ajax({
                url   : 'ajax/enviarCorreoCajaCompensacion.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    if(data.code == '1'){
                        alertify.success('Correo enviado!');
                    }else{
                        alertify.error('No se pudo enviar el correo');
                    }
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }

            });
        });

        $("#tbl_Incapacidades tbody").on('click', ".btnGeneratePQR", function(){
            var x = $(this).attr('id_Incapacidad');
            window.open('ajax/pqr.ajax.php?idIncapacidad='+x, "_blank");
        });
        
        
        /* Esta parte es para traer los datos de la edicion */
        $('#tbl_Incapacidades tbody').on("click", ".btnVerIncapacidad", function(){
            var x = $(this).attr('id_Incapacidad');
            var datas = new FormData();
            datas.append('VerDetalleIncapacidades', x);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'html',
                success     : function(data){
                   $("#cuerpoDetalleIncapacidad").html(data);
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }

            });
        });

        /* Esta parte es para traer los datos de la edicion */
        $('#tbl_Incapacidades tbody').on("click", ".btnEditarIncapacidad", function(){
            var x = $(this).attr('id_Incapacidad');
            var datas = new FormData();
            datas.append('EditarIncapacidades', x);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    //$("#EditarIpsAfiliado").val(data.eps);
                    $("#EditarNombreEmpresa").val(data.emp_nombre);
                    $("#EditarNombreIncapacidad").val(data.emd_nombre);
                    $("#EditarCedulaIncapacidad").val(data.emd_cedula);
                    $("#EditarCedulaIncapacidad").attr('readonly', true);
                    $("#EditarCargoEmpleado").val(data.emd_cargo);
                    $("#EditarIvlempleado").val(data.emd_salario);
                    $("#EditarFechaAfiliacionEps").val(data.emd_fecha_afiliacion_eps);
                    //$("#EditarARLEmpleado").val(data.inc_arl_afiliado);
                    $("#EditarEstadoEmpleado").val(data.est_emp_desc_v);
                    $("#EditarFechaIngreso").val(data.emd_fecha_ingreso);
                    $("#EditarFechaRetiro").val(data.emd_fecha_retiro);
                    $("#EditarFechaRegistro").val(data.inc_fecha_generada);
                    $("#EditarUsuario").val(data.user_nombre);
                    EditarFechaRecepcion.setDate(data.inc_fecha_recepcion_d);
                    $("#EditarRegistradoEntidad").val(data.inc_registrada_entidad_i);

                    $(".empleadoClassEdicion").removeClass('is-valid is-invalid was-validated');
                    $("#estadoEmpleadoDiv").removeClass('valid-feedback invalid-feedback');
                    if(data.emd_estado == 1){
                        $(".empleadoClassEdicion").addClass('is-valid was-validated');
                        $("#estadoEmpleadoDiv").addClass("valid-feedback");
                    }else{
                        $(".empleadoClassEdicion").addClass('is-invalid was-validated');
                        $("#estadoEmpleadoDiv").addClass("invalid-feedback");
                    }

                    $("#EditarOrigen").val(data.inc_origen).change();
                    $("#EditarAtencion").val(data.inc_atn_id).change();

                    $("#EditarValorPagadoIncapaNomina").val(data.inc_valor_pagado_nomina);
                    EditartxtFechaPagoNomina.setDate(data.inc_fecha_pago_nomina);

                    var optionX = "<option value='"+data.dia_codigo+"'>"+data.dia_descripcion+"</option>";
                    choiceE.setChoices([{id:data.dia_codigo, text: data.dia_descripcion, selected: true }], 'id', 'text', true);
                    //$("#EditarOrigen").val(data.inc_origen);

                    $("#EditarClasificacion").val(data.inc_clasificacion);
                    $("#EditarClasificacion").val(data.inc_clasificacion).change();
                    if(data.inc_fecha_inicio != null && data.inc_fecha_inicio != ''){
                       // $("#EditarFechaInicio").val(data.inc_fecha_inicio.split(' ')[0]);
                        EditarFechaInicio.setDate(data.inc_fecha_inicio.split(' ')[0]);

                    }

                    if(data.inc_fecha_final != null && data.inc_fecha_final != ''){
                       // $("#EditarFechaFinal").val(data.inc_fecha_final.split(' ')[0]);
                        EditarFechaFinal.setDate(data.inc_fecha_final);
                        var minDate = data.inc_fecha_final;
                        var fecha1 = moment(data.inc_fecha_inicio);
                        var fecha2 = moment(minDate);
                        var fechaRes = fecha2.diff(fecha1, 'days');
                        $("#EditarNumeroDias").val((fechaRes + 1) + " Días");
            
                    }

                    $("#EditarTipoGeneracion").val(data.inc_tipo_generacion);
                    $("#EditarTipoGeneracion").val(data.inc_tipo_generacion).change();
                    $("#EditarPrescripcion").val(data.inc_nro_prescripcion);
                    
                    selMedicosEdit.setChoices([{id:data.med_id, text: data.med_nombre, selected: true }], 'id', 'text', true);

                    $("#EditarRegistroMedico").val(data.med_num_registro);

                    if (data.med_estado_rethus == 1) {
                        $("#EditarEstadoRethus").val("REGISTRA");
                        $(".validRethus").addClass('is-valid was-validated');
                    } else if (data.med_estado_rethus == 2) {
                        $("#EditarEstadoRethus").val("NO REGISTRA");
                        $(".validRethus").addClass('is-invalid was-validated');
                    } else if(data.med_estado_rethus == 0){
                        $("#EditarEstadoRethus").val("NO VALIDADO");
                        $(".validRethus").addClass('border border-warning');
                    } else {
                        $("#EditarEstadoRethus").val("");
                        $(".validRethus").removeClass('is-valid is-invalid was-validated border border-warning');
                    }
                     
                    selInstitucionesEdit.setChoices([{id:data.enat_id_i, text: data.enat_nombre_v, selected: true }], 'id', 'text', true);
                
                    $("#EditarNitEntidad").val(data.enat_nit_v);
                    if (data.inc_donde_se_genera != 0) {
                        if (data.inc_pertenece_red == 1){                                                                                                                       
                            $("#EditarPertRed").val("PERTENECE");
                            $(".validEntidad").addClass('is-valid was-validated');
                        } else if(data.inc_pertenece_red == 2) {
                            $("#EditarPertRed").val("NO PERTENECE");
                            $(".validEntidad").addClass('is-invalid was-validated');
                        } else {
                            $("#EditarPertRed").val("");
                            $(".validEntidad").removeClass('is-valid is-invalid was-validated');
                        }   
                    } 

                    // $("#EditarObservacion").val(data.inc_observacion);
                    $("#id_incapacidad").val(data.inc_id);
                    $("#EditarEmpresa").val(data.inc_empresa);
                    $("#EditarEmpleado").val(data.inc_emd_id);
                    $("#EditarIpsAfiliado").val(data.inc_ips_afiliado);
                    $("#EditarAfpAfiliado").val(data.inc_afp_afiliado);
                    $("#EditarArlAfiliado").val(data.inc_arl_afiliado);
                    $("#EditarValorPagadoEmpresa").val(data.inc_valor_pagado_empresa);
                    $("#EditarValorPagoEPS").val(data.inc_valor_pagado_eps);
                    $("#EditarValorPagoAjuste").val(data.inc_valor_pagado_ajuste);
                        
                    if(data.inc_ruta_incapacidad != null && data.inc_ruta_incapacidad != ''){
                        $("#ruta_incapacidad").val(data.inc_ruta_incapacidad);
                        ruta = data.inc_ruta_incapacidad.split(".")[1];
                        if(ruta == 'pdf'){
                            $(".previsualizar").attr('src', 'vistas/img/plantilla/pdf.png');
                        }else{
                            $(".previsualizar").attr('src', data.inc_ruta_incapacidad); 
                        }
                        
                    }

                    if(data.inc_ruta_incapacidad_transcrita != null && data.inc_ruta_incapacidad_transcrita != ''){
                        $("#ruta_otroincapacidad").val(data.inc_ruta_incapacidad_transcrita);
                        ruta = data.inc_ruta_incapacidad_transcrita.split(".")[1];
                        if(ruta == 'pdf'){
                            $(".previsualizar_2").attr('src', 'vistas/img/plantilla/pdf.png');
                        }else{
                            $(".previsualizar_2").attr('src', data.inc_ruta_incapacidad_transcrita); 
                        }
                        
                    }

                    if(data.inc_fecha_radicacion != null && data.inc_fecha_radicacion != ''){
                        //$("#EditarFechaRadicacionIncapacidad").val(data.inc_fecha_radicacion.split(' ')[0]);
                        EditarFechaRadicacionIncapacidad.setDate(data.inc_fecha_radicacion.split(' ')[0]);
                    }

                    if(data.inc_fecha_solicitud != null && data.inc_fecha_solicitud != ''){
                        //$("#EditarFechaSolicitudIncapacidad").val(data.inc_fecha_solicitud.split(' ')[0]);
                        EditarFechaSolicitudIncapacidad.setDate(data.inc_fecha_solicitud.split(' ')[0]);
                    }

                    if(data.inc_fecha_pago != null && data.inc_fecha_pago != ''){
                       // $("#EditarFechaPagoIncapacidad").val(data.inc_fecha_pago.split(' ')[0]);
                        EditarFechaPagoIncapacidad.setDate(data.inc_fecha_pago.split(' ')[0]);
                    }

                   /* if(data.inc_fecha_generada != null && data.inc_fecha_generada != ''){
                        //$("#EditarFechaIncapacidad").val(data.inc_fecha_generada.split(' ')[0]);
                        //EditarFechaIncapacidad.setDate(data.inc_fecha_pago.split(' ')[0]);
                    }*/

                    if(data.inc_valor != null && data.inc_valor != ''){
                        $("#EditarValorIncapacidad").val(data.inc_valor.split(' ')[0]);
                    }

                    $("#EditarEstadoTramite").val(data.inc_estado_tramite).change();
                    $("#EditarSubEstadoTramite").val(data.inc_sub_estado_i).change();
                    $("#EditarObservacion_2").val(data.inc_observacion);
                    $("#id_incapacidadValores").val(data.inc_id);

                    $("#EditarValorSolicitud").val(data.inc_valor_solicitado);

                    $("#EditarObservacion_3").val(data.inc_observacion_neg);
                    if(data.inc_fecha_negacion_ != null && data.inc_fecha_negacion_ != ''){
                        //$("#EditarFechaIncapacidad").val(data.inc_fecha_generada.split(' ')[0]);
                        EditarFechaSinReconocimiento.setDate(data.inc_fecha_negacion_.split(' ')[0]);
                    }


                    $("#EditarNumeroRadicado").val(data.inc_numero_radicado_v);

                    $("#EditarEstadoIncapacidad").val(data.inc_estado);
                    $("#EditarEstadoIncapacidad").val(data.inc_estado).change();
                    if(data.inc_estado == '0'){
                        $("#EditarMotivosRechazo").show();
                    }else{
                        $("#EditarMotivosRechazo").hide();
                    }

                    $("#EditarMotivo").val(data.inc_mot_rech_i);
                    $("#EditarMotivo").val(data.inc_mot_rech_i).change();


                    
                    $("#txtObservaciones").val(data.inc_observacion_rech_t);
                    getComentariosIncapacidades(x, $("#tablaComentarios"), 'editar');
                    getComentariosIncapacidades(x, $("#tablaComentarios_r"), 'editar');
                    
                    var datos = new FormData();
                    datos.append('getIncapaciadesAnteriores', data.inc_cc_afiliado);
                    $.ajax({
                        url   : 'ajax/incapacidades.ajax.php',
                        method: 'post',
                        data  : datos,
                        cache : false,
                        contentType : false,
                        processData : false,
                        dataType    : 'html',
                        success     : function(respuesta){
                            $("#tableIncapAnteriorE").html(respuesta);
                        }
                    });
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }

            });

            
        });

        /* Eliminar Clientes */
        $('#tbl_Incapacidades tbody').on("click", ".btnEliminarIncapacidad", function(){
            var x = $(this).attr('id_Incapacidad');
            Swal.fire({
                title: '¿Está seguro de borrar la incapacidad?',
                text: "¡Si no lo está puede cancelar la accíón!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, borrar incapacidad!'
            }).then(function (result) {
                if (result.value) {
                    var datas = new FormData();
                    datas.append('borrarIncapacidad', x);
                    $.ajax({
                        url   : 'ajax/incapacidades.ajax.php',
                        method: 'post',
                        data  : datas,
                        cache : false,
                        contentType : false,
                        processData : false,
                        dataType    : 'json',
                        success     : function(data){
                            tablaIncapa.ajax.reload();
                            if(data.code == '1'){
                                alertify.success(data.message);
                            }else{
                                alertify.error(data.message);
                            }

                        },
                        beforeSend:function(){
                            $.blockUI({ 
                                baseZ: 2000,
                                message : '<h3>Un momento por favor....</h3>',
                                css: { 
                                    border: 'none', 
                                    padding: '1px', 
                                    backgroundColor: '#000', 
                                    '-webkit-border-radius': '10px', 
                                    '-moz-border-radius': '10px', 
                                    opacity: .5, 
                                    color: '#fff' 
                                } 
                            }); 
                        },
                        complete:function(){
                            $.unblockUI();
                        }

                    });
                }
            });
        });

        /* Traer los documentos de la incapacidad */
        $('#tbl_Incapacidades tbody').on("click", ".verDocumentos", function(){
            var x = $(this).attr('id_Incapacidad');
            var datas = new FormData();
            datas.append('EditarIncapacidades', x);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    $("#CuerpoDocumentos").html('');

                    var cuerpo = '';
                    if(data.inc_ruta_incapacidad != null && data.inc_ruta_incapacidad != '' && data.inc_ruta_incapacidad != '0'){
                        cuerpo += '<tr><td style="text-align:center;">1</td>';
                        cuerpo += '<td style="text-align:center;"><img src="vistas/img/plantilla/pdf.png" style="width:40px; heigth:40px;"></td>';
                        cuerpo += '<td style="text-align:center;"><a target="_blank" href="'+ data.inc_ruta_incapacidad +'" >Copia de la incapacidad</a></td>';
                        cuerpo += '</tr>';
                    }

                    if(data.inc_ruta_incapacidad_transcrita != null && data.inc_ruta_incapacidad_transcrita != '' && data.inc_ruta_incapacidad_transcrita != '0'){
                        cuerpo += '<tr><td style="text-align:center;">2</td>';
                        cuerpo += '<td style="text-align:center;"><img src="vistas/img/plantilla/pdf.png" style="width:40px; heigth:40px;"></td>';
                        cuerpo += '<td style="text-align:center;"><a target="_blank" href="'+ data.inc_ruta_incapacidad_transcrita +'" >Copia de la incapacidad transcrita</a></td>';
                        cuerpo += '</tr>';
                    }


                    if(cuerpo == ''){
                        cuerpo += '<td colspan="3" style="text-align:center;">No tiene documentos asociados a la incapacidad</td>';
                    }

                    $("#CuerpoDocumentos").html(cuerpo);
                    $("#modalDocumentosIncapacidad").modal();
                }

            });
        });

        /*Notificar a la empresa que la incapacidad esta rechazada*/
        $("#tbl_Incapacidades tbody").on("click", ".btnSendNotificacionIncompleta", function(){
            var x = $(this).attr('id_Incapacidad');
            var datas = new FormData();
            datas.append('sendMailIncapacidades', x);
            $.ajax({
                url   : 'ajax/enviar_correos.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    
                    if(data.code == '1'){
                        alertify.success("Correo enviado");
                    }else{
                        alertify.error("Correo no enviado");
                    }
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    tablaIncapa.ajax.reload();
                    $.unblockUI();
                }
            });
        });
        
        /* validar que la cedual exista */
        $("#NuevoCedulaIncapacidad").change(function(){
            var usuario = $(this).val();
            var datos = new FormData();
            datos.append('validarCedula', usuario);
            datos.append('empresa', $("#session").val());
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datos,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(respuesta){
                    
                    if(respuesta != false){
                        $("#NuevoNombreEmpresa").val(respuesta.emp_nombre);
                        $("#NuevoNombreIncapacidad").val(respuesta.emd_nombre);
                        $("#NuevoIpsAfiliado").val(respuesta.emd_eps_id);
                        $("#NuevoIvlempleado").val(respuesta.emd_salario_promedio);
                        $("#NuevoEmpresa").val(respuesta.emd_emp_id);
                        $("#NuevoEmpleado").val(respuesta.emd_id);
                        $("#NuevoAfpAfiliado").val(respuesta.emd_afp_id);
                        $("#NuevoArlAfiliado_2").val(respuesta.emd_arl_id);
                        $("#NuevoIpsAfiliado_2").val(respuesta.emd_eps_id);
                        $("#NuevoCargoEmpleado").val(respuesta.emd_cargo);
                        $("#btnGuardarInc").attr('disabled', false);
                        $("#NuevoFechaAfiliacionEps").val(respuesta.emd_fecha_afiliacion_eps);
                        $("#NuevoArlAfiliado").val(respuesta.emd_arl_id);
                        $("#NuevoEstadoEmpleado").val(respuesta.est_emp_desc_v);
                        $("#NuevoFechaIngreso").val(respuesta.emd_fecha_ingreso);
                        $("#NuevoFechaRetiro").val(respuesta.emd_fecha_retiro);
                        if(respuesta.emd_caso_especial_i != 0){
                            $("#NuevoNovedad").val("CASO ESPECIAL");
                        }else{
                            $("#NuevoNovedad").val("");
                        }
                        $(".empleadoClass").removeClass('is-valid is-invalid was-validated');
                        $("#estadoEmpleadoDiv").removeClass('valid-feedback invalid-feedback');
                        if(respuesta.emd_estado == 1){
                            $(".empleadoClass").addClass('is-valid was-validated');
                            $("#estadoEmpleadoDiv").addClass("valid-feedback");
                        }else{
                            $(".empleadoClass").addClass('is-invalid was-validated');
                            $("#estadoEmpleadoDiv").addClass("invalid-feedback");
                        }

                        if(respuesta.emd_ruta_cedula != null && respuesta.emd_ruta_cedula != ''){
                            $("#cedulaColaborador").show();
                            $("#cedulaColaborador").attr('href', respuesta.emd_ruta_cedula);
                        }else{
                            $("#cedulaColaborador").hide();
                            $("#cedulaColaborador").attr('href', "#");
                        }


                        if(respuesta.emd_ruta_otro != null && respuesta.emd_ruta_otro != ''){
                            $("#polizaColaborador").show();
                            $("#polizaColaborador").attr('href', respuesta.emd_ruta_otro);
                        }else{
                            $("#polizaColaborador").attr('href', "#");
                            $("#polizaColaborador").hide();
                        }

                        var datos = new FormData();
                        datos.append('getIncapaciadesAnteriores', usuario);
                        $.ajax({
                            url   : 'ajax/incapacidades.ajax.php',
                            method: 'post',
                            data  : datos,
                            cache : false,
                            contentType : false,
                            processData : false,
                            dataType    : 'html',
                            success     : function(respuesta){
                                $("#tableIncapAnterior").html(respuesta);
                            }
                        });
                    }else{
                        $("#NuevoCedulaIncapacidad").val('');
                        $("#btnGuardarInc").attr('disabled', true);
                        alertify.error('Esta cedula no existe');
                        $("#NuevoCedulaIncapacidad").focus();
                    }
                }

            });
        });
 
        //validar medico
        $("#NuevoProfesional").change(function(){
            var medicoId = $(this).val();
            var datos = new FormData();
            datos.append('validarMedico', medicoId);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datos,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(respuesta){ 
                    $(".validRethus").removeClass('is-valid is-invalid was-validated border border-warning');
                    if(respuesta != false){
                        $("#NuevoRegistroMedico").val(respuesta.med_num_registro);
                        if (respuesta.med_estado_rethus == 1) {
                            $("#NuevoEstadoRethus").val("REGISTRA");
                            $(".validRethus").addClass('is-valid was-validated');
                        } else if (respuesta.med_estado_rethus == 2) {
                            $("#NuevoEstadoRethus").val("NO REGISTRA");
                            $(".validRethus").addClass('is-invalid was-validated');
                        } else {
                            $("#NuevoEstadoRethus").val("NO VALIDADO");
                            $(".validRethus").addClass('border border-warning');
                        }
                    } else {
                        $("#NuevoRegistroMedico").val('');
                        $("#NuevoEstadoRethus").val('');
                    }
                }
            });
        });
        
        //Editar Medico
        $("#EditarProfesional").change(function(){
            var medicoId = $(this).val();
            var datos = new FormData();
            datos.append('validarMedico', medicoId);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datos,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(respuesta){
                    $(".validRethus").removeClass('is-valid is-invalid was-validated border border-warning');
                    if(respuesta != false){
                        $("#EditarRegistroMedico").val(respuesta.med_num_registro);
                        if (respuesta.med_estado_rethus == 1) {
                            $("#EditarEstadoRethus").val("REGISTRA");
                            $(".validRethus").addClass('is-valid was-validated');
                        } else if (respuesta.med_estado_rethus == 2) {
                            $("#EditarEstadoRethus").val("NO REGISTRA");
                            $(".validRethus").addClass('is-invalid was-validated');
                        } else {
                            $("#EditarEstadoRethus").val("NO VALIDADO");
                            $(".validRethus").addClass('border border-warning');
                        }
                    } else {
                        $("#EditarRegistroMedico").val('');
                        $("#EditarEstadoRethus").val('');
                    }
                }
            });
        });

        //validar Entidad  
        $("#NuevoInstitucionGenera").change(function(){
            idEntidad = $(this).val();

            var datos = new FormData();
            datos.append('validarEntidad', idEntidad);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datos,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(respuesta){
                    if(respuesta != false){
                        $("#NuevoNitEntidad").val(respuesta.enat_nit_v);
                    } else {
                        $("#NuevoNitEntidad").val('');
                        $("#NuevoPertRed").val('');
                        $(".validEntidad").removeClass('is-valid is-invalid was-validated border');
                    }
                }
            });

            var currentIdEps = $("#NuevoIpsAfiliado").val();
            var data = new FormData();
            data.append('idEntidadRed', idEntidad);
            $.ajax({
                url   : 'ajax/registroEntidad.ajax.php',
                method: 'post',
                data  : data,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(datosRed){
                    $(".validEntidad").removeClass('is-valid is-invalid was-validated');

                    if (datosRed != false || datosRed.length == 0) {
                        var listIdEps = []
                        datosRed.forEach(element => {   
                            listIdEps.push(element['ei_ips_id']);   
                        });
                        if (listIdEps.indexOf(currentIdEps) != -1) {
                            $("#NuevoPertRed").val("PERTENECE");
                            $(".validEntidad").addClass('is-valid was-validated');
                            $("#NuevoPerteneceRed").val("1");
                        } else {
                            $("#NuevoPertRed").val("NO PERTENECE");
                            $(".validEntidad").addClass('is-invalid was-validated');
                            $("#NuevoPerteneceRed").val("2");
                        }
                    } else {
                        $("#NuevoPertRed").val("NO PERTENECE");
                        $(".validEntidad").addClass('is-invalid was-validated');
                        $("#NuevoPerteneceRed").val("2");
                    }
                }
            });
        });

        $("#NuevoIpsAfiliado").change(function(){
            var currentIdEps = $(this).val();
            var idEntidad = $("#NuevoInstitucionGenera").val();

            if (idEntidad != 0) {
                var data = new FormData();
                data.append('idEntidadRed', idEntidad);
                $.ajax({
                    url   : 'ajax/registroEntidad.ajax.php',
                    method: 'post',
                    data  : data,
                    cache : false,
                    contentType : false,
                    processData : false,
                    dataType    : 'json',
                    success     : function(datosRed){
                        $(".validEntidad").removeClass('is-valid is-invalid was-validated border');
                        if (datosRed != false || datosRed.length == 0) {
                            var listIdEps = []
                            datosRed.forEach(element => {   
                                listIdEps.push(element['ei_ips_id']);   
                            });
                            if (listIdEps.indexOf(currentIdEps) != -1) {
                                $("#NuevoPertRed").val("PERTENECE");
                                $(".validEntidad").addClass('is-valid was-validated');
                                $("#NuevoPerteneceRed").val("1");
                            } else {
                                $("#NuevoPertRed").val("NO PERTENECE");
                                $(".validEntidad").addClass('is-invalid was-validated');
                                $("#NuevoPerteneceRed").val("2");
                            }
                        } else {
                            $("#NuevoPertRed").val("NO PERTENECE");
                            $(".validEntidad").addClass('is-invalid was-validated');
                            $("#NuevoPerteneceRed").val("2");
                        }
                        }
                });
            } else {
                $("#NuevoPertRed").val('');
                $(".validEntidad").removeClass('is-valid is-invalid was-validated border');
            }
        })

        //Editar Entidad
        $("#EditarInstitucionGenera").change(function(){
            var entidadId = $(this).val();
            var datos = new FormData();
            datos.append('validarEntidad', entidadId);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datos,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(respuesta){
                    if(respuesta != false){
                        $("#EditarNitEntidad").val(respuesta.enat_nit_v);
                    } else {
                        $("#EditarNitEntidad").val('');
                        $("#EditarPertRed").val('');
                        $(".validEntidad").removeClass('is-valid is-invalid was-validated border');
                    }
                }
            });

            var data = new FormData();
            var currentIdEps = $("#EditarIpsAfiliado").val();
            data.append('idEntidadRed', entidadId);
            $.ajax({
                url   : 'ajax/registroEntidad.ajax.php',
                method: 'post',
                data  : data,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(datosRed){
                    $(".validEntidad").removeClass('is-valid is-invalid was-validated');
                    if (datosRed != false || datosRed.length == 0) {
                        var listIdEps = []
                        datosRed.forEach(element => {   
                            listIdEps.push(element['ei_ips_id']);   
                        });
                        if (listIdEps.indexOf(currentIdEps) != -1) {
                            $("#EditarPertRed").val("PERTENECE");
                            $(".validEntidad").addClass('is-valid was-validated');
                            $("#EditarPerteneceRed").val("1");
                        } else {
                            $("#EditarPertRed").val("NO PERTENECE");
                            $(".validEntidad").addClass('is-invalid was-validated');
                            $("#EditarPerteneceRed").val("2");
                        }
                    } else {
                        $("#EditarPertRed").val("NO PERTENECE");
                        $(".validEntidad").addClass('is-invalid was-validated');
                        $("#EditarPerteneceRed").val("2");
                    }
                }
            });
        });

        $("#EditarIpsAfiliado").change(function(){
            var currentIdEps = $(this).val();
            var idEntidad = $("#EditarInstitucionGenera").val();

            var data = new FormData();
            data.append('idEntidadRed', idEntidad);
            $.ajax({
                url   : 'ajax/registroEntidad.ajax.php',
                method: 'post',
                data  : data,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(datosRed){
                    $(".validEntidad").removeClass('is-valid is-invalid was-validated border');
                    if (datosRed != false || datosRed.length == 0) {
                        var listIdEps = []
                        datosRed.forEach(element => {   
                            listIdEps.push(element['ei_ips_id']);   
                        });
                        if (listIdEps.indexOf(currentIdEps) != -1) {
                            $("#EditarPertRed").val("PERTENECE");
                            $(".validEntidad").addClass('is-valid was-validated');
                            $("#EditarPerteneceRed").val("1");
                        } else {
                            $("#EditarPertRed").val("NO PERTENECE");
                            $(".validEntidad").addClass('is-invalid was-validated');
                            $("#EditarPerteneceRed").val("2");
                        }
                    } else {
                        $("#EditarPertRed").val("NO PERTENECE");
                        $(".validEntidad").addClass('is-invalid was-validated');
                        $("#EditarPerteneceRed").val("2");
                    }
                }
            });
        })

        $("#verIncapacidadesAnteriores").click(function(){
            if($("#rowIncapacidades").is(":visible")){
               $("#rowIncapacidades").hide(); 
            } else{
                $("#rowIncapacidades").show();
            }
        });

        $("#NuevoFechaInicio").change(function(){
            $(".alert").remove();
            var cedula = $("#NuevoCedulaIncapacidad").val();
            var fecha = $(this).val();
            var datoa = new FormData();
            datoa.append('validarIncapacidad', 'si');
            datoa.append('txtCedula', cedula);
            datoa.append('txtFechaInicio', fecha);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datoa,
                cache: false,
                contentType: false,
                processData: false,
                success     : function(respuesta){
                  
                    if(respuesta != 1){
                        $("#btnGuardarInc").attr('disabled', false);
                    }else{
                        $("#NuevoFechaInicio").parent().after('<div class="alert alert-warning">Existe una incapacidad con esta fecha e identificación</div>');
                        $("#NuevoFechaInicio").val('');
                        $("#btnGuardarInc").attr('disabled', true);
                    }
                }

            });
        });

        $("#EditarCedulaIncapacidad").change(function(){
            $(".alert").remove();
            var usuario = $(this).val();
            var datos = new FormData();
            datos.append('validarCedula', usuario);
            datos.append('empresa', $("#session").val());
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datos,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(respuesta){
                  
                    if(respuesta != false){
                        $("#EditarNombreEmpresa").val(respuesta.emp_nombre);
                        $("#EditarNombreIncapacidad").val(respuesta.emd_nombre);
                        $("#EditarEmpresa").val(respuesta.emd_emp_id);
                        $("#EditarEmpleado").val(respuesta.emd_id);
                        console.log("Soy Batman ::> respuesta.emd_estado ::> "+ respuesta.emd_estado)
                        if(respuesta.emd_estado == '1'){
                            console.log("Soy Batman ::> seguimiento ::> "+ respuesta.emd_estado)
                            $("#spanHelblokEditar").html('Empleado valido');
                            $("#divInputCedulaEditar").removeClass( "has-error has-warning" ).addClass("has-success");
                        }else{
                            console.log("Soy Batman ::> seguimiento ::> "+ respuesta.emd_estado)
                            $("#spanHelblokEditar").html('Empleado valido pero no activo, Fecha de retiro '+ respuesta.emd_fecha_retiro+" EPS "+respuesta.ips_nombre);
                            $("#divInputCedulaEditar").removeClass( "has-error has-success" ).addClass("has-warning");
                        }

                    }else{
                       // $("#EditarCedulaIncapacidad").parent().after('<div class="alert alert-warning">Esta identificación no existe en la base de datos</div>');
                        $("#EditarCedulaIncapacidad").val('');
                        alertify.error('Esta cedula no existe');
                        $("#EditarCedulaIncapacidad").focus();
                        $("#spanHelblokEditar").html('Identificación no registrada');
                        $("#divInputCedulaEditar").removeClass( "has-warning has-success" ).addClass("has-error");                    
                    }
                }

            })

            var datos = new FormData();
            datos.append('getIncapaciadesAnteriores', usuario);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datos,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'html',
                success     : function(respuesta){
                    $("#tableIncapAnteriorE").html(respuesta);
                }
            });
        });

        $("#verIncapacidadesAnterioresE").click(function(){
            if($("#rowIncapacidadesE").is(":visible")){
               $("#rowIncapacidadesE").hide(); 
            } else{
                $("#rowIncapacidadesE").show();
            }
        });

        $("#EditarEstadoTramite").change(function(){
            $("#divSubEstadoTramite_E").hide();
            $("#divFechaRegistro").hide();
            $("#divFechaSolicitud").hide();
            $("#divFechaRadicacion").hide();
            $("#divFechaPago").hide();
            $("#divNumeroRadicacion").hide();
            $("#divValorIncapacidad").hide();
            $("#divEditarInformacion").hide();
            $("#divValorSolicitado").hide();
            $("#divEnvioDatoCorreo").hide();
            $("#divFechaNoreconocida").hide();
            $("#divEditarInformacionSinReconocer").hide();        
            //$("#nuevoEnvioCorreo").iCheck('uncheck');
            $("#adicionaPagoNuevo").hide();
            $("#divMultiPagos").hide();
            $("#CuerpoTablasPagosX").html('');
            $("#divNumeroRadicacion").show();
            if($(this).val() == '3'){
                $("#divFechaRadicacion").show();
            }else if($(this).val() == '4'){
                $("#divFechaSolicitud").show();
                $("#divValorSolicitado").show();
            }else if($(this).val() == '5'){
                $("#divFechaPago").show();
                $("#divValorIncapacidad").show();
                $("#divEnvioDatoCorreo").show();
                $("#adicionaPagoNuevo").show();
                $("#divMultiPagos").show();
            }else if($(this).val() == 'INCOMPLETA'){
                $("#divFechaRegistro").show();
                $("#divEditarInformacion").show();
            }else if($(this).val() == '6' || $(this).val() == '7'){
                //$("#divEditarInformacionSinReconocer").show();
                $("#divFechaNoreconocida").show();
                $("#divSubEstadoTramite_E").show();
            }else{
                $("#divFechaRegistro").show();
                $("#divFechaRadicacion").show();
            }
        });


        $("#NuevoEstadoTramite").change(function(){
            $("#divSubEstadoTramite_N").hide();
            $("#divFechaRegistro_N").hide();
            $("#divFechaSolicitud_N").hide();
            $("#divFechaRadicacion_N").hide();
            $("#divFechaPago_N").hide();
            $("#divNumeroRadicacion_N").hide();
            $("#divValorIncapacidad_N").hide();
            $("#divEditarInformacion_N").hide();
            $("#divValorSolicitado_N").hide();
            $("#divEnvioDatoCorreo_N").hide();
            $("#divFechaNoreconocida_N").hide();
            $("#divNuevoInformacionSinReconocer_N").hide();        
            //$("#nuevoEnvioCorreo_N").iCheck('uncheck');
            $("#adicionaPagoNuevo_N").hide();
            $("#divMultiPagos_N").hide();
            $("#CuerpoTablasPagosX_N").html('');

            if($(this).val() == '3'){
                $("#divFechaRadicacion_N").show();
                $("#divNumeroRadicacion_N").show();
            }else if($(this).val() == '4'){
                $("#divNumeroRadicacion_N").show();
                $("#divFechaSolicitud_N").show();
                $("#divValorSolicitado_N").show();
            }else if($(this).val() == '5'){
                $("#divNumeroRadicacion_N").show();
                $("#divFechaPago_N").show();
                $("#divValorIncapacidad_N").show();
                //$("#divEnvioDatoCorreo_N").show();
                $("#adicionaPagoNuevo_N").show();
                $("#divMultiPagos_N").show();
               
            }else if($(this).val() == 'INCOMPLETA'){
                $("#divNumeroRadicacion_N").show();
                $("#divFechaRegistro_N").show();
                $("#divEditarInformacion_N").show();
            }else if($(this).val() == '6' || $(this).val() == '7'){
                //$("#divEditarInformacionSinReconocer_N").show();
                $("#divNumeroRadicacion_N").show();
                $("#divFechaNoreconocida_N").show();
                $("#divSubEstadoTramite_N").show();
            }else if($(this).val() == '1'){
                $("#divFechaRadicacion_N").show();
                $("#divNumeroRadicacion_N").show();
            }else{
                $("#divFechaRadicacion_N").show();
                $("#divNumeroRadicacion_N").show();
            }
        });
        
        
        $("#cargarDatos").click(function(){
            if($("#NuevoIncapacidad").val() == ''){
                alertify.error("¡No se ha seleccionado un archivo!");
            }else{
                var form = $("#foermularioMaxivo");
                //Se crean un array con los datos a enviar, apartir del formulario 
                var formData = new FormData($("#foermularioMaxivo")[0]);
                $.ajax({
                    url: 'https://incapacidades.app/services/exports/incapacidades/importar',
                    type  : 'post',
                    data: formData,
                    dataType : 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){
                        $.blockUI({ 
                            message : '<h3>Un momento por favor....</h3>',
                            baseZ: 2000,
                            css: { 
                                border: 'none', 
                                padding: '1px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff' 
                            } 
                        }); 
                    },
                    complete:function(){
                        $.unblockUI();
                    },
                    //una vez finalizado correctamente
                    success: function(data){
                        alertify.success(data.msg);

                        // if(data.fallaron > 0){
                        //     window.location.href = 'index.php?exportar=fallosIncapacidad';
                        // }   

                        $("#foermularioMaxivo")[0].reset();  
                        $("#modalAgregarIncapacidadesMasivamente").modal('hide');   

                        tablaIncapa.ajax.reload();
                    },
                    //si ha ocurrido un error
                    error: function(){
                        //after_save_error();
                        alertify.error('Error al realizar el proceso');
                    }
                });
            }
        });

        $("#AddcargaInformacion").click(function(){
            var campos = "<div class='row' id='row_"+contadorFilas+"'>";
            campos += "<div class='col-md-3'>";
            campos += "<div class='form-group' id='divInputCedula_"+contadorFilas+"'><input type='text' class='form-control cedulasI' name='Editarcedula_"+contadorFilas+"' numero='"+contadorFilas+"' id='Editarcedula_"+contadorFilas+"' placeholder='Identificación'><span class='help-block' id='spanHelblok_"+contadorFilas+"'></span></div>";
            campos += "</div>";
            campos += "<div class='col-md-2'>";
            campos += "<div class='form-group'><input type='text' class='form-control fechasinicio' name='EditarFechaInicio_"+contadorFilas+"' id='EditarFechaInicio_"+contadorFilas+"' placeholder='Fecha Inicio'></div>";
            campos += "</div>";
            campos += "<div class='col-md-3'>";
            campos += "<div class='form-group'><input type='file' class='form-control cedulasI' name='EditarFilePrimerImagen_"+contadorFilas+"' id='EditarFilePrimerImagen_"+contadorFilas+"' placeholder='Identificación'></div>";
            campos += "</div>";
            campos += "<div class='col-md-3'>";
            campos += "<div class='form-group'><input type='file' class='form-control cedulasI' name='EditarSegundaImagen_"+contadorFilas+"' id='EditarSegundaImagen_"+contadorFilas+"' placeholder='Identificación'></div>";
            campos += "</div>";
            campos += "<div class='col-md-1'>";
            campos += "<button type='button' class='btn btn-danger eliminarImagenes' id = 'borrar_"+contadorFilas+"' idfila='"+contadorFilas+"' title='Borrar fila'><i class='fa fa-trash-o'></i></button>";
            campos += "</div>";
     
            $("#filas").append(campos);
     
            $("#borrar_"+contadorFilas).click(function(){
                var id = $(this).attr('idfila');
                $("#row_"+id).remove();
            });
     
            $("#Editarcedula_"+contadorFilas).change(function(){
                $(".alert").remove();
                var usuario = $(this).val();
                var numero = $(this).attr("numero");
                var datos = new FormData();
                datos.append('validarCedula', usuario);
                datos.append('empresa', $("#session").val());
                $.ajax({
                    url   : 'ajax/incapacidades.ajax.php',
                    method: 'post',
                    data  : datos,
                    cache : false,
                    contentType : false,
                    processData : false,
                    dataType    : 'json',
                    success     : function(respuesta){
                        if(respuesta == false){
                            alertify.error('Esta cedula no existe');
                            $("#Editarcedula_"+numero).focus();
                            $("#spanHelblok_"+numero).html('Identificación no registrada');
                            $("#divInputCedula_"+numero).removeClass( "has-warning has-success" ).addClass("has-error");
                        }else{
                            //console.log("Soy Batman ::> respuesta.emd_estado ::> "+ respuesta.emd_estado)
                            if(respuesta.emd_estado == '1'){
                                console.log("Soy Batman ::> seguimiento ::> "+ respuesta.emd_estado)
                               $("#spanHelblok_"+numero).html('Empleado valido');
                                $("#divInputCedula_"+numero).removeClass( "has-error has-warning" ).addClass("has-success");
                            }else{
                                //console.log("Soy Batman ::> seguimiento ::> "+ respuesta.emd_estado)
                                $("#spanHelblok_"+numero).html('Empleado valido pero no activo, Fecha de retiro '+ respuesta.emd_fecha_retiro+" EPS "+respuesta.ips_nombre);
                                $("#divInputCedula_"+numero).removeClass( "has-error has-success" ).addClass("has-warning");
                            }
                        }
                    }
     
                })
            });
            
            $('#EditarFechaInicio_'+contadorFilas).datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true    
            });
            contadorFilas++;
        });

        $("#cargarInformacionDocumentosMax").click(function(){
            var form = $("#foermularioMaxivoDocu");
            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#foermularioMaxivoDocu")[0]);
            formData.append('contados', contadorFilas);
            $.ajax({
                url: 'ajax/solo_cargar_imagenes.php',
                type  : 'post',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                   
                    alertify.success('Proceso terminado, total registros '+ data.total +', se cargaron con exito '+ data.exito +', Datos Errados '+ data.fallas );
                    
                    if(data.fallas == '0'){
                        $("#foermularioMaxivoDocu")[0].reset();  
                        $("#modalAgregarIncapacidadesMasivamente").modal('hide'); 
                    }
                       
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            }); 
        });

        var i = 1;

        $("#adicionaPagoNuevo").click(function(){
            
            var tr = "<tr id='filaNum"+i+"'>";
            tr += "<td>"+i+"</td>";
            tr += "<td><div class=\"form-group\">";
            tr += "<div class=\"input-group\">";
            tr += "<span class=\"input-group-addon\">";
            tr += "<i class=\"fa fa-usd\"></i>";
            tr += "</span>";
            tr += "<input class=\"form-control\" type=\"text\" name=\"EditarValorSolicitudNueva_"+i+"\" id=\"EditarValorSolicitudNueva_"+i+"\" placeholder=\"Ingresar Valor Pagado\">";
            tr += "</div>";
            tr += "</div></td>";
            tr += "<td><div class=\"form-group\">";
            tr += "<div class=\"input-group\">";
            tr += "<span class=\"input-group-addon\">";
            tr += "<i class=\"fa fa-usd\"></i>";
            tr += "</span>";
            tr += "<input class=\"form-control fechaNuevaPago\" type=\"text\" name=\"EditarFechaPagoNueva_"+i+"\" id=\"EditarFechaPagoNueva_"+i+"\" placeholder=\"Fecha de Pago\">";
            tr += "</div>";
            tr += "</div></td>";
            tr += "<td><button class='btn btn-danger btn-sm' id='EliminarFila_"+i+"' type='button' idFila='"+i+"'><i class='fa fa-trash-o'></i></button></td>";
            tr += "</tr>";
            $("#CuerpoTablasPagosX").append(tr);
            
            $(".fechaNuevaPago").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#EliminarFila_"+i).click(function(){
                var id = $(this).attr('idFila');
                $("#filaNum"+id).remove();
            });
            $("#contadorDePagos").val(i);
            i++;
        });


        function addCommasIncapa(nStr)
        {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }
        //foermularioM

        function getDatosParavalores(valor){
            var datas = new FormData();
            datas.append('EditarIncapacidades', valor);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    if(data.inc_fecha_radicacion != null && data.inc_fecha_radicacion != ''){
                        $("#EditarFechaRadicacionIncapacidad").val(data.inc_fecha_radicacion.split(' ')[0]);
                    }

                    if(data.inc_fecha_solicitud != null && data.inc_fecha_solicitud != ''){
                        $("#EditarFechaSolicitudIncapacidad").val(data.inc_fecha_solicitud.split(' ')[0]);
                    }

                    if(data.inc_fecha_pago != null && data.inc_fecha_pago != ''){
                        $("#EditarFechaPagoIncapacidad").val(data.inc_fecha_pago.split(' ')[0]);
                    }

                    if(data.inc_fecha_generada != null && data.inc_fecha_generada != ''){
                        $("#EditarFechaIncapacidad").val(data.inc_fecha_generada.split(' ')[0]);
                    }

                    if(data.inc_valor != null && data.inc_valor != ''){
                        $("#EditarValorIncapacidad").val(data.inc_valor.split(' ')[0]);
                    }

                    $("#EditarEstadoTramite").val(data.inc_estado_tramite).change();
                    $("#EditarObservacion_2").val(data.inc_observacion);
                    $("#id_incapacidadValores").val(data.inc_id);

                    $("#EditarValorSolicitud").val(data.inc_valor_solicitado);

                    $("#EditarObservacion_3").val(data.inc_observacion_neg);
                    $("#EditarFechaSinReconocimiento").val(data.inc_fecha_negacion_);

                    
                }

            });


            var datas = new FormData();
            datas.append('getDatosPagoInca', valor);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    var tr = '';
                    var ix = 1;
                    $.each(data, function(i, item){
                        tr = "<tr id=\"PagoNum"+item.pag_id_i+"\">";
                        tr += "<td>"+ix+"</td>";
                        tr += "<td>"+addCommasIncapa(item.pag_valor_pagado_v)+"</td>";
                        tr += "<td>"+item.pag_fecha_pago_d+"</td>";
                        tr += "<td><button class='btn btn-danger btn-sm' inc='"+item.pag_inc_id_i+"' id='EliminarPago_"+item.pag_id_i+"' type='button' idPago='"+item.pag_id_i+"'><i class='fa fa-trash-o'></i></button></td>";
                        tr += "</tr>";
                        $("#CuerpoTablasPagosX").append(tr);
                        ix++;
                        $("#EliminarPago_"+item.pag_id_i+"").click(function(){
                            var id = $(this).attr('idPago');
                            var inc = $(this).attr('inc');
                            var datas = new FormData();
                            datas.append('eliminaDatosPago', id);
                            $.ajax({
                                url   : 'ajax/incapacidades.ajax.php',
                                method: 'post',
                                data  : datas,
                                cache : false,
                                contentType : false,
                                processData : false,
                                dataType    : 'json',
                                success: function(data){
                                    if(data.code == 1){
                                        $("#PagoNum"+id).remove();
                                        getDatosParavalores(inc);
                                    }else{
                                        alertify.error(data.msg);
                                    }
                                    
                                },
                                beforeSend:function(){
                                    $.blockUI({ 
                                        message : '<h3>Un momento por favor....</h3>',
                                        baseZ: 2000,
                                        css: { 
                                            border: 'none', 
                                            padding: '1px', 
                                            backgroundColor: '#000', 
                                            '-webkit-border-radius': '10px', 
                                            '-moz-border-radius': '10px', 
                                            opacity: .5, 
                                            color: '#fff'
                                        } 
                                    }); 
                                },
                                complete:function(){
                                    $.unblockUI();
                                }
                            })
                            
                        })
                    });
                }  
            });
        }

        $(".showAddInc").on('click', function(){
            $("#NuevoClasificacion_2").hide();
            $("#NuevoClasificacion").show();
            $(".validRethus").removeClass('is-valid is-invalid was-validated border');
            $(".validEntidad").removeClass('is-valid is-invalid was-validated');
            //$("#EditarPertRed").val("");
        });

        $("#NuevoOrigen").on('change', function(){
            if($(this).val() == '2' || $(this).val() == '3'){
                $("#NuevoClasificacion").hide();
                $("#NuevoClasificacion_2").show();

                $("#NuevoClasificacion option").each(function(){
                    if ($(this).text() == 'Inicial') {
                        //console.log($(this).val());
                        $("#NuevoClasificacion").val($(this).val());
                        return
                    }
                })
            }else{
                $("#NuevoClasificacion").show();
                $("#NuevoClasificacion_2").hide();
            }
            $("#NuevoValorPagadoEmpresa").attr('readonly', false);
        });


        // $("#NuevoClasificacion").on('change', function(){
        //     $("#NuevoClasificacion_2").val($(this).val());
        // });
    

        $("#LiquidarIncapa").click(function(){
            if($("#NuevoFechaInicio").val().length < 1 || $("#NuevoFechaFinal").val().length < 1){
                alertify.error("Por Favor selecciona las fechas de Inicio y/o Final de la incapacidad a Liquidar");
            }else{
                var strOrigen = $("#NuevoOrigen").val();
                var strClasif = $("#NuevoClasificacion").val();
                var intIdEmpl = $("#NuevoEmpleado").val();
                var datas = new FormData();
                datas.append('liquidarIncapacidad', intIdEmpl);
                datas.append('origenIncapacidad', strOrigen);
                datas.append('clasifIncapacidad', strClasif);
                datas.append('NuevoFechaInicio', $("#NuevoFechaInicio").val());
                datas.append('NuevoFechaFinal', $("#NuevoFechaFinal").val());
                datas.append('diasIncapacidad', $("#NumeroDias").val());
                $("#NuevoValorPagoEPS").val('');
                $("#NuevoValorPagadoEmpresa").val('');
                $.ajax({
                    url   : 'ajax/incapacidades.ajax.php',
                    method: 'post',
                    data  : datas,
                    cache : false,
                    contentType : false,
                    processData : false,
                    dataType    : 'json',
                    success: function(data){
                        if(data.code == 1){
                            if(data.tipo == 1){
                                $("#NuevoValorPagadoEmpresa").val(data.valE);
                            }else{
                                if(data.valE != 0){
                                    $("#NuevoValorPagadoEmpresa").val(data.valE);
                                }
                                
                                $("#NuevoValorPagoEPS").val(data.valI);
                            }
                        }else{
                            alertify.error(data.message);
                        }
                        
                    },
                    beforeSend:function(){
                        $.blockUI({ 
                            message : '<h3>Un momento por favor....</h3>',
                            baseZ: 2000,
                            css: { 
                                border: 'none', 
                                padding: '1px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff'
                            } 
                        }); 
                    },
                    complete:function(){
                        $.unblockUI();
                    }
                })
            } 
        });

        $("#LiquidarIncapaE").click(function(){
            var strOrigen = $("#EditarOrigen").val();
            var strClasif = $("#EditarClasificacion").val();
            var intIdEmpl = $("#EditarEmpleado").val();
            var datas = new FormData();
            datas.append('liquidarIncapacidad', intIdEmpl);
            datas.append('origenIncapacidad', strOrigen);
            datas.append('clasifIncapacidad', strClasif);
            datas.append('NuevoFechaInicio', $("#EditarFechaInicio").val());
            datas.append('NuevoFechaFinal', $("#EditarFechaFinal").val());
            datas.append('diasIncapacidad', $("#EditarNumeroDias").val());
            $("#EditarValorPagoEPS").val('');
            $("#EditarValorPagadoEmpresa").val('');
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success: function(data){
                    if(data.code == 1){
                        if(data.tipo == 1){
                            $("#EditarValorPagadoEmpresa").val(data.valE);
                        }else{
                            $("#EditarValorPagadoEmpresa").val(data.valE);
                            $("#EditarValorPagoEPS").val(data.valI);
                        }
                    }else{
                        alertify.error(data.message);
                    }
                    
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            }) 
        });

        $("#btnGuardarInc").click(function(){
            var form = $("#formCreacionIncapacidades");
            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#formCreacionIncapacidades")[0]);
            $.ajax({
                url: 'ajax/incapacidades.ajax.php',
                type  : 'post',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    tablaIncapa.ajax.reload();
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    if(data.code == '1'){
                        $("#formCreacionIncapacidades")[0].reset();
                        $("#NuevoMotivosRechazo").hide();
                        $("#NuevoOcultarRechazado").show();
                        $("#NuevoDiagnostico").val(0).change();
                        // $("#EnvioCorreoRechazado").iCheck('uncheck');
                        $(".alert").remove();
                        $("#spanHelblokNuevo").html('');
                        $("#divInputCedulaNuevo").removeClass("has-success");
                        $("#modalAgregarIncapacidad").modal('hide');
                        alertify.success( data.message );    
                    }else{
                        alertify.error( data.message );
                    }
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        });

        $("#btnEditarIncapacidad").click(function(){
            var form = $("#EditarIncapacidadForm");
            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#EditarIncapacidadForm")[0]);
            $.ajax({
                url: 'ajax/incapacidades.ajax.php',
                type  : 'post',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    tablaIncapa.ajax.reload();
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    if(data.code == '1'){
                        $("#EditarIncapacidadForm")[0].reset();
                        $("#editarMotivosRechazo").hide();
                        $("#EditarPosAceptar").show();
                        $(".alert").remove();
                        $("#modalEditarrIncapacidad").modal('hide');  
                        alertify.success( data.message );    
                    }else{
                        alertify.error( data.message );
                    } 
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        });

        $("#NuevoEstadoIncapacidad").on('change', function () {
            if($(this).val() == '0'){
               
                $("#NuevoMotivosRechazo").show();
            }else{
              
                $("#NuevoMotivosRechazo").hide();
            }
            
        });

        $("#EditarEstadoIncapacidad").on('change', function () {
            if($(this).val() == '0'){
                $("#EditarMotivosRechazo").show();
            }else{
                $("#EditarMotivosRechazo").hide();
            }
            
        });


        function addCommas(nStr)
        {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }

        function getComentariosIncapacidades(idIncapacidad, tabla, opcion){
            var datas = new FormData();
            datas.append('getComentariosIncapacidad', idIncapacidad);
            datas.append('opcion', opcion);
            $.ajax({
                url   : 'ajax/incapacidades.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'html',
                success     : function(data){
                    tabla.html(data);
                    $(".btnEliminarComentario").click(function(){
                        var comenrai = $(this).attr('idObservacion');
                        var datas = new FormData();
                        datas.append('delComentarios', comenrai);
                        $.ajax({
                            url   : 'ajax/incapacidades.ajax.php',
                            method: 'post',
                            data  : datas,
                            cache : false,
                            contentType : false,
                            processData : false,
                            dataType    : 'json',
                            success     : function(data){
                                if(data.code == '1'){
                                    /*Bien*/
                                    getComentariosIncapacidades(idIncapacidad, tabla, opcion);
                                }else{
                                    alertify.error("No se pudo borrar el comentario");
                                }
                            },
                            beforeSend:function(){
                                $.blockUI({ 
                                    message : '<h3>Un momento por favor....</h3>',
                                    baseZ: 2000,
                                    css: { 
                                        border: 'none', 
                                        padding: '1px', 
                                        backgroundColor: '#000', 
                                        '-webkit-border-radius': '10px', 
                                        '-moz-border-radius': '10px', 
                                        opacity: .5, 
                                        color: '#fff' 
                                    } 
                                }); 
                            },
                            complete:function(){
                                $.unblockUI();
                                $("#txtComentario").val('');
                                getComentarios($("#txtIdCaso").val());
                            }
                        });   
                    });

                    $(".btnEditarComentario").click(function(){
                        var comenrai = $(this).attr('idObservacion');
                        var datas = new FormData();
                        datas.append('getOneComentari', comenrai);
                        $.ajax({
                            url   : 'ajax/incapacidades.ajax.php',
                            method: 'post',
                            data  : datas,
                            cache : false,
                            contentType : false,
                            processData : false,
                            dataType    : 'json',
                            success     : function(data){
                                if(data.code != false){
                                    //Bien
                                    $("#EditarObservacion").val(data.incm_comentario_t);
                                    $("#idObservacionEditar").val(data.incm_id_i);
                                }
                            },
                            beforeSend:function(){
                                $.blockUI({ 
                                    message : '<h3>Un momento por favor....</h3>',
                                    baseZ: 2000,
                                    css: { 
                                        border: 'none', 
                                        padding: '1px', 
                                        backgroundColor: '#000', 
                                        '-webkit-border-radius': '10px', 
                                        '-moz-border-radius': '10px', 
                                        opacity: .5, 
                                        color: '#fff' 
                                    } 
                                }); 
                            },
                            complete:function(){
                                $.unblockUI();
                            }
                        });   
                    });
                }
            });
        }

        /*Exportar consolidados*/

        $("#ExportarComentariosConsol").click(function(){
            $.ajax({
                url    : 'ajax/pythonServices.ajax.php',
                type   : 'post',
                dataType : 'json',
                data   : { getConsolidadoComentarios: true },
                success : function(data){
                    var $a = $("<a>");
                    $a.attr("href",data.ruta);
                    $("body").append($a);
                    $a.attr("download",data.archivo);
                    $a[0].click();
                    $a.remove();
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            });
        });

        $("#incapacidadesExportacion").click(function(){
            /*var url = 'index.php?exportar=inc&tipodescargas='+$("#selestadotramite").val();
            if($("#totalChechedCompleta").val() == '2'){
                url += '&fechaInicio='+$("#FechaInicioTotal").val()+'&fechaFinal='+$("#FechaInicioFinal").val()
            }
            window.open(url);
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 18000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });*/
            $.ajax({
                url    : 'ajax/pythonServices.ajax.php',
                type   : 'post',
                dataType : 'json',
                data   : { getConsolidado: true },
                success : function(data){
                    var $a = $("<a>");
                    $a.attr("href",data.ruta);
                    $("body").append($a);
                    $a.attr("download",data.archivo);
                    $a[0].click();
                    $a.remove();
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            });
        });


        $("#incapacidadesExportacionIncompletas").click(function(){
            var url = 'index.php?exportar=incompletas';
            if($("#totalChechedIncompleta").val() == '2'){
                url += '&fechaInicio='+$("#FechaInicioIncompleta").val()+'&fechaFinal='+$("#FechaInicioFinalIncompleta").val()
            }
            $.ajax({
                url    : url,
                type   : 'post',
                dataType : 'json',
                success : function(data){
                    var currentdate = new Date();
                    var fecha_hora = currentdate.getFullYear() + rellenar((currentdate.getMonth()+1), 2) +  rellenar(currentdate.getDate(), 2) + "_" + rellenar(currentdate.getHours(), 2) +  rellenar(currentdate.getMinutes(), 2) + rellenar(currentdate.getSeconds(), 2);
                    var $a = $("<a>");
                    $a.attr("href",data.file);
                    $("body").append($a);
                    $a.attr("download","consolidado_incapacidades_Incompletas"+fecha_hora+".xlsx");
                    $a[0].click();
                    $a.remove();
                },
                beforeSend:function(){
                    $.blockUI({
                        message : '<h3>Un momento por favor....</h3>', 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            });
        });

        /*Exportar consolidados Hasta aqui*/

        $("#cargarDatosComents").click(function(){
            if($("#NuevoIncapacidadComent").val() == ''){
                swal({
                    title: 'Espera!',
                    text: "¡No se ha seleccionado un archivo!",
                    type: 'warning',
                    showConfirmButton: true,
                    confirmButtonText: "Cerrar",
                    closeOnConfirm: false
                });
            }else{
                var form = $("#foermularioMaxivoComents");
                //Se crean un array con los datos a enviar, apartir del formulario 
                var formData = new FormData($("#foermularioMaxivoComents")[0]);
                $.ajax({
                    url: 'ajax/solo_cargar_comentarios.php',
                    type  : 'post',
                    data: formData,
                    dataType : 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend:function(){
                        $.blockUI({ 
                            message : '<h3>Un momento por favor....</h3>',
                            baseZ: 2000,
                            css: { 
                                border: 'none', 
                                padding: '1px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff' 
                            } 
                        }); 
                    },
                    complete:function(){
                        $.unblockUI();
                    },
                    //una vez finalizado correctamente
                    success: function(data){
                        if(data.check == 0){
                            alertify.success('Proceso terminado, total registros '+ data.total +', se cargaron con exito '+ data.exito +', fallaron '+ data.fallaron +', incapacidades existentes '+ data.totalesAciertosNo );
                        }else{
                            alertify.success('Proceso terminado, total registros '+ data.total +', se cargaron con exito '+ data.exito +', fallaron '+ data.fallaron +', incapacidades no existentes '+ data.totalesAciertosNo );
                        } 

                        /*if(data.fallaron > 0){
                            window.location.href = 'index.php?exportar=fallosIncapacidad';
                        }   */

                        $("#foermularioMaxivoComents")[0].reset();  
                        $("#modalAgregarComentariosMasivamente").modal('hide');   

                        tablaIncapa.ajax.reload();
                    },
                    //si ha ocurrido un error
                    error: function(){
                        //after_save_error();
                        alertify.error('Error al realizar el proceso');
                    }
                });
            }
        });
        

        function rellenar (str, max) {
            str = str.toString();
            return str.length < max ? rellenar("0" + str, max) : str;
        }

        

           /*===================================
            =           DATEPICKER              =
            ===================================*/
        var FechaFinalIngreso = flatpickr('#NuevoFechaFinal', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
          onChange: function(selectedDates, dateStr, instance){
            var minDate = dateStr;
            var fecha1 = moment($("#NuevoFechaInicio").val());
            var fecha2 = moment(minDate);
            $("#NumeroDias").val( (fecha2.diff(fecha1, 'days') + 1) + " Días");
          }
        });

        flatpickr('#NuevoFechaInicio', {
            altInput: true,
            locale: "es",
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            onChange: function(selectedDates, dateStr, instance) {
                FechaFinalIngreso.set("minDate", dateStr);
                FechaFinalIngreso.setDate(dateStr);
            },
        });

        flatpickr('#NuevoFechaRecepcion', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        });

        flatpickr('#NuevotxtFechaPagoNomina', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        });

        flatpickr('#NuevoFechaPagoIncapacidad', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        });

        flatpickr('#NuevoFechaRadicacionIncapacidad', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        }); 

        flatpickr('#NuevoFechaSolicitudIncapacidad', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        });
            
        flatpickr('#NuevoFechaSinReconocimiento', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
        });
        
        /*PARA EXPORTAR INCAPACIDADES*/
        $("#totalChechedCompleta").on('change', function(){
            if($(this).val() == '1'){
                $("#fecha1X").hide();
                $("#fecha2X").hide();
            }else{
                $("#fecha1X").show();
                $("#fecha2X").show();
            }
        });

        var FECHAFINALNUEVA = flatpickr('#FechaInicioFinal', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d"
        });

        flatpickr('#FechaInicioTotal', {
            altInput: true,
            locale: "es",
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            onChange: function(selectedDates, dateStr, instance) {
                FECHAFINALNUEVA.set("minDate", dateStr);
                FECHAFINALNUEVA.setDate(dateStr);
            },
        });

        var FECHAFINALNUEVAInc = flatpickr('#FechaInicioFinalIncompleta', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d"
        });

        flatpickr('#FechaInicioIncompleta', {
            altInput: true,
            locale: "es",
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            onChange: function(selectedDates, dateStr, instance) {
                FECHAFINALNUEVAInc.set("minDate", dateStr);
                FECHAFINALNUEVAInc.setDate(dateStr);
            },
        });

        $("#totalChechedIncompleta").on('change', function(){
            if($(this).val() == '1'){
                $("#fecha3X").hide();
                $("#fecha4X").hide();
            }else{
                $("#fecha3X").show();
                $("#fecha4X").show();
            }
        });

    
        /*choises.js para poder buscar por AJAX - DIAGNOSTICO*/
        var serverLookup = function() {
            var query = choices.input.value;
            $.post( apiUrl, {query: query}, function(event) {
                var results = JSON.parse(event);
                choices['setChoices'](results, 'id', 'text', true);
            });
        }
        // Trigger an API lookup when the user pauses after typing.
        document.getElementById("NuevoDiagnostico").addEventListener('search', function(event) {
            clearTimeout(lookupTimeout);
            lookupTimeout = setTimeout(serverLookup, lookupDelay);
        });

        // We want to clear the API-provided options when a choice is selected.
        document.getElementById("NuevoDiagnostico").addEventListener('choice', function(event) {
            choices.setChoices([], 'id', 'text', true);
        });

        var serverLookupEdicion = function() {
            var query = choiceE.input.value;
            $.post( apiUrl, {query: query}, function(event) {
                var results = JSON.parse(event);
                choiceE['setChoices'](results, 'id', 'text', true);
            });
        }

        // Trigger an API lookup when the user pauses after typing.
        document.getElementById("EditarDiagnostico").addEventListener('search', function(event) {
            clearTimeout(lookupTimeout);
            lookupTimeout = setTimeout(serverLookupEdicion, lookupDelay);
        });

        /*choises.js para poder buscar por AJAX - MÉDICOS*/
        var serverLookupMed = function() {
            var query = selMedicos.input.value;
            $.post( apiUrlMed, {query: query}, function(event) {
                var results = JSON.parse(event);
                selMedicos['setChoices'](results, 'id', 'text', true);
            });
        }

        document.getElementById("NuevoProfesional").addEventListener('search', function(event) {
            clearTimeout(lookupTimeout);
            lookupTimeout = setTimeout(serverLookupMed, lookupDelay);
        });

        document.getElementById("NuevoProfesional").addEventListener('choice', function(event) {
            selMedicos.setChoices([], 'id', 'text', true);
        });

        var serverLookupMedEdit = function() {
            var query = selMedicosEdit.input.value;
            $.post( apiUrlMed, {query: query}, function(event) {
                var results = JSON.parse(event);
                selMedicosEdit['setChoices'](results, 'id', 'text', true);
            });
        }

        document.getElementById("EditarProfesional").addEventListener('search', function(event) {
            clearTimeout(lookupTimeout);
            lookupTimeout = setTimeout(serverLookupMedEdit, lookupDelay);
        });

        /*choises.js para poder buscar por AJAX - ENTIDADES IPS*/
        var serverLookupInt = function() {
            var query = selInstituciones.input.value;
            $.post( apiUrlInt, {query: query}, function(event) {
                var results = JSON.parse(event);
                selInstituciones['setChoices'](results, 'id', 'text', true);
            });
        }

        document.getElementById("NuevoInstitucionGenera").addEventListener('search', function(event) {
            clearTimeout(lookupTimeout);
            lookupTimeout = setTimeout(serverLookupInt, lookupDelay);
        });

        document.getElementById("NuevoInstitucionGenera").addEventListener('choice', function(event) {
            selInstituciones.setChoices([], 'id', 'text', true);
        });

        var serverLookupIntEdit = function() {
            var query = selInstitucionesEdit.input.value;
            $.post( apiUrlInt, {query: query}, function(event) {
                var results = JSON.parse(event);
                selInstitucionesEdit['setChoices'](results, 'id', 'text', true);
            });
        }

        document.getElementById("EditarInstitucionGenera").addEventListener('search', function(event) {
            clearTimeout(lookupTimeout);
            lookupTimeout = setTimeout(serverLookupIntEdit, lookupDelay);
        });


        
       
});