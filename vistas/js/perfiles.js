/*===============================================
=               DATATABLES                      =
================================================*/
$('#tbl_Perfils').DataTable({
    "language" : {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    } 
});



$('#tbl_Perfils tbody').on("click", ".btnEditarPerfiles" , function(){
	var x = $(this).attr('idPerfiles');
    var datas = new FormData();
    datas.append('EditarPerfilId', x);
    $.ajax({
        url   : 'ajax/perfiles.ajax.php',
        method: 'post',
        data  : datas,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        success     : function(data){

            $("#EditarNombre").val(data.datosPerfil.perfiles_descripcion_v); 
			$("#EditarCliente").val(data.datosPerfil.perfiles_clientes_id_i);
			$("#EditarCliente").val(data.datosPerfil.perfiles_clientes_id_i).change();	
			$("#EditarPerfilId").val(data.datosPerfil.perfiles_id_i);		

            if(data.datosPerfil.perfiles_adiciona_i == 1){
                $("#EditarAdicionar").attr('checked', true);    
            }else{
                $("#EditarAdicionar").attr('checked', false);
            }

            if(data.datosPerfil.perfiles_edita_i == 1){
                $("#EditarEditar").attr('checked', true);    
            }else{
                $("#EditarEditar").attr('checked', false);
            }

            if(data.datosPerfil.perfiles_elimina_i == 1){
                $("#EditarEliminar").attr('checked', true);    
            }else{
                $("#EditarEliminar").attr('checked', false);
            }
                   

			$.each(data.datosPermisos, function(i, item) {
				//$("#EditarMenus_"+item.perfiles_permisos_menu_id_i).attr('checked', true);
				$("#EditarMenus_"+item.perfiles_permisos_menu_id_i).attr('checked', true); 
			});

            $.each(data.datosReportes, function(i, item) {
                //$("#EditarMenus_"+item.perfiles_permisos_menu_id_i).attr('checked', true);
                $("#EditarMenusReportes_"+item.perfiles_permisos_reportes_submenu_id_i).attr('checked', true); 
            });
       }

    });
});


/* Eliminar usuarios */
$('#tbl_Perfils tbody').on("click", ".btnEiminarPerfiles", function(){
    var x = $(this).attr('idPerfiles');
    Swal.fire({
        title: '¿Está seguro de borrar el perfil?',
        text: "¡Si no lo está puede cancelar la accíón!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar este perfil!'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                url: 'ajax/perfiles.ajax.php',
                type  : 'post',
                data: {id_perfiles: x},
                dataType : 'json',
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    if(data.code == '1'){
                        alertify.success( data.desc );  
                        location.reload(true);  
                    }else{
                        alertify.error( data.desc );
                    }
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        }
    })
});

$("#guardarPerfil").click(function(){
    var form = $("#formCrearPerfiles");
    //Se crean un array con los datos a enviar, apartir del formulario 
    var formData = new FormData($("#formCrearPerfiles")[0]);
    $.ajax({
        url: 'ajax/perfiles.ajax.php',
        type  : 'post',
        data: formData,
        dataType : 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:function(){
            $.blockUI({ 
                message : '<h3>Un momento por favor....</h3>',
                baseZ: 2000,
                css: { 
                    border: 'none', 
                    padding: '1px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                } 
            }); 
        },
        complete:function(){
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data.code == '1'){
                $("#formCrearPerfiles")[0].reset();
                alertify.success( data.desc ); 
                location.reload(true);     
            }else{
                alertify.error( data.desc );
            }
        },
        //si ha ocurrido un error
        error: function(){
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});

$("#editaperfilButton").click(function(){
    var form = $("#formEditarPerfil");
    //Se crean un array con los datos a enviar, apartir del formulario 
    var formData = new FormData($("#formEditarPerfil")[0]);
    $.ajax({
        url: 'ajax/perfiles.ajax.php',
        type  : 'post',
        data: formData,
        dataType : 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend:function(){
            $.blockUI({ 
                message : '<h3>Un momento por favor....</h3>',
                baseZ: 2000,
                css: { 
                    border: 'none', 
                    padding: '1px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                } 
            }); 
        },
        complete:function(){
            tablaIncapa.ajax.reload();
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data.code == '1'){
                $("#formEditarPerfil")[0].reset();
                alertify.success( data.desc );    
                location.reload(true);  
            }else{
                alertify.error( data.desc );
            } 
        },
        //si ha ocurrido un error
        error: function(){
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});



