/* validar los tipos de imagenes y/o archivos */
//$("#Nuevocliente").select2();
//$("#Editarcliente").select2();
$(".NuevaFotos").change(function(){
    var imax = $(this).attr('valor');
    var imagen = this.files[0];
    //console.log(imagen);
    /* Validar el tipo de imagen */
    if(imagen['type'] != 'image/jpeg' && imagen['type'] != 'image/png'  ){
        $(".NuevaFoto").val('');
        swal({
            title : "Error al subir el archivo",
            text  : "El archivo debe estar en formato PNG , JPG",
            type  : "error",
            confirmButtonText : "Cerrar"
        });
    }else if(imagen['size'] > 2000000 ) {
        $(".NuevaFoto").val('');
        swal({
            title : "Error al subir el archivo",
            text  : "El archivo no debe pesar mas de 2MB",
            type  : "error",
            confirmButtonText : "Cerrar"
        });
    }else{
        if(imagen['type'] == 'image/jpeg' || imagen['type'] == 'image/png'){
            var datosImagen = new FileReader();
            datosImagen.readAsDataURL(imagen);

            $(datosImagen).on("load", function(event){
                var rutaimagen = event.target.result;
                $(".previsualizar").attr('src', rutaimagen);
            }); 
        }
        
    }   
});

var edicion =  '<div class="btn-group dropstart" role="group">';
edicion += '<button id="btnGroupVerticalDrop1" type="button"  class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
edicion += '<i class="fa fa-info-circle"></i>';
//edicion += '<span class="sr-only">Toggle Dropdown</span>';
edicion += '</button>';
edicion += '<ul class="dropdown-menu" role="menu">';
edicion += '<li><a class="dropdown-item btnEditarUsuarios" title="Editar Usuario" idUsuario data-bs-toggle="modal" data-bs-target="#modalEditarUsuarios" href="#">EDITAR</a></li>';
edicion += '<li class="dropdown-divider"></li>';
edicion += '<li><a class="dropdown-item btnEiminarUsuarios" title="Eliminar Usuario" idUsuario href="#">ELIMINAR</a></li>';
edicion +='</ul>';
edicion +='</div>';


/*===============================================
=               DATATABLES                      =
================================================*/


var tablaUsuarios= $('#tablaUsuarios').DataTable({
    "ajax": "ajax/usuarios.ajax.php?dame-Usuarios-inicio=owi",
    "columnDefs": [
    
        {
            "targets": -1,
            "data": null,
            "className": "text-center",
            "defaultContent": edicion
        }
    ],
    
    "language" : {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    } 

});

 $('#tablaUsuarios tbody').on( 'click', 'a', function () {
        var current_row = $(this).parents('tr');//Get the current row
        if (current_row.hasClass('child')) {//Check if the current row is a child row
            current_row = current_row.prev();//If it is, then point to the row before it (its 'parent')
        }
        var data = tablaUsuarios.row( current_row ).data();
        $(this).attr("idUsuario", data[5]);
       
    } );

/* Eliminar usuarios */
$('#tablaUsuarios tbody').on("click", ".btnEiminarUsuarios", function(){
    var x = $(this).attr('idUsuario');
    swal.fire({
        title: '¿Está seguro de borrar el usuario?',
        text: "¡Si no lo está puede cancelar la accíón!",
       /* type: 'warning',*/
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borralo!'
    }).then(function (result) {
        if (result.value) {
            //window.location = "index.php?ruta=usuariosEps&id_usuario_eps="+x ;
            var datas = new FormData();
            datas.append('id_Usuario', x);
            $.ajax({
                url   : 'ajax/usuarios.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    tablaUsuarios.ajax.reload();
                    $.unblockUI();
                }

            });
        }
    })
});



$('#tablaUsuarios tbody').on("click", ".btnEditarUsuarios" , function(){
    var x = $(this).attr('idUsuario');
    var datas = new FormData();
    datas.append('EditarUsuarioId', x);
    $.ajax({
        url   : 'ajax/usuarios.ajax.php',
        method: 'post',
        data  : datas,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        success     : function(data){
            $("#EditarNombre").val(data.user_nombre); 
            $("#EditarUsuario").val(data.usu_correo_v);    
            //$("#EditarPassword").val(data.usu_correo_v);       
            $("#EditarPerfil").val(data.user_perfil_id).change();
            $("#passwordActual").val(data.user_password);
            $("#EditarUserID").val(data.user_id);
            $("#RecordatorioActual").val(data.user_recordatorio_v);
            if(data.user_ruta_imagen_v != '' && data.user_ruta_imagen_v != null ){
                $(".previsualizar").attr('src', data.user_ruta_imagen_v);
                $("#FotoActual").val(data.user_ruta_imagen_v);
            }else{
                $(".previsualizar").attr('src', 'vistas/img/usuarios/default/anonymous.png');
            }
       }
    });

    $.ajax({
        url   : 'ajax/usuarios.ajax.php',
        method: 'post',
        data: {'idUsuario':x},
        dataType    : 'json',
        success     : function(datax){
            var selectedValues = new Array();
            $.each(datax, function(i, item){
                console.log(i);
                console.log(item);
                selectedValues[i] = item.emu_id_empresa; 
            });

            $("#Editarcliente").val(selectedValues).change();
        }
    });
});
/*Nuevo Usuario*/
  /*Implementar la funcion AJax la de aqui es NuevoEntidad*/
    //Se crean un array con los datos a enviar, apartir del formulario
$("#btnGuardarUsuario").click(function(){
    var formData = new FormData($("#frmGuardarNuevoUsuario")[0]);
    $.ajax({
        url   : 'ajax/usuarios.ajax.php',
        method: 'post',
        data  : formData,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend:function(){
            $.blockUI({ 
                baseZ: 2000,
                css: { 
                    border: 'none', 
                    padding: '1px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                } 
            }); 
        },
       complete:function(){
            tablaUsuarios.ajax.reload();
            $("#frmGuardarNuevoUsuario")[0].reset(); 
            $("#modalAgregarUsuario").modal('hide');  
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data.code == '1'){
                alertify.success( data.desc );    
            }else{
                alertify.error( data.desc );
            } 
        },
        //si ha ocurrido un error
        error: function(){
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});



//editar archivo rf

$("#btnEditarUsuario").click(function(){
    var formData = new FormData($("#frmEditarNuevoUsuario")[0]);
    $.ajax({
        url   : 'ajax/usuarios.ajax.php',
        method: 'post',
        data  : formData,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend:function(){
            $.blockUI({ 
                baseZ: 2000,
                css: { 
                    border: 'none', 
                    padding: '1px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                } 
            }); 
        },
        complete:function(){
            tablaUsuarios.ajax.reload();
            $("#modalEditarUsuarios").modal('hide');  
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data.code == '1'){
                alertify.success( data.desc,'Hola' );    
            }else{
                alertify.error( data.desc );
            } 
        },
        //si ha ocurrido un error
        error: function(){
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});
 

/* Activar usuarios */
$('#tablaUsuarios tbody').on("click", ".btnActivar", function(){
    var idUsuario = $(this).attr('idUsuario');
    var estado = $(this).attr('estado');
    var datos = new FormData();
    datos.append("ActivarId", idUsuario);
    datos.append("estado", estado);
    $.ajax({
        url  : 'ajax/usuarios.ajax.php',
        type : 'post',
        data : datos,
        cache: false,
        contentType:false,
        processData : false,
        success: function(data){
            
        }
    });

    if(estado == 0){
        $(this).removeClass('btn-success');
        $(this).addClass('btn-danger');
        $(this).html('Desactivado');
        $(this).attr('estado', 1);
    }else{
        $(this).removeClass('btn-danger');
        $(this).addClass('btn-success');
        $(this).html('Activado');
        $(this).attr('estado', 0);
    }
});

$('#tablaUsuarios tbody').on("click", ".btnEditarPaswordCliente", function(){
    var idUsuario = $(this).attr('idUsuario');
    var email = $(this).attr('gestion');
    var usuario = $(this).attr('user');
    if(email != '' && email != null){
        swal({
            title: '¿Está seguro de esta acción?',
            text: "¡Si no lo está puede cancelar!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, cambiar password!'
        },function(isConfirm) {
            if (isConfirm) {
               var datos = new FormData();
                datos.append("enviarCorreoPass", idUsuario);
                datos.append("email", email);
                datos.append("usuario", usuario);
                $.ajax({
                    url  : 'ajax/clientes.ajax.php',
                    type : 'post',
                    data : datos,
                    cache: false,
                    contentType:false,
                    processData : false,
                    success: function(data){
                        if(data == 'ok'){
                            alertify.success('Se ha enviado un correo con la información, al email de gestion humana');
                        }else{
                            alertify.error('un error a ocurrido!');
                        }
                    }
                });
            }
        })
        
    }else{
        swal({
            title  : 'Accion no permitida, no tiene correo de gestión , registrado!',
            type   : 'error',
            configButtonText : "Cerrar",
            closeOnConfirm: true
        });
    }
});


