
/* validar los tipos de imagenes y/o archivos */
$(".NuevaFoto").change(function(){
	var imax = $(this).attr('valor');
	var imagen = this.files[0];
	console.log(imagen);
	/* Validar el tipo de imagen */
	if(imagen['type'] != 'image/jpeg' && imagen['type'] != 'image/png' && imagen['type'] != "application/pdf" ){
		$(".NuevaFoto").val('');
		swal({
			title : "Error al subir el archivo",
			text  : "El archivo debe estar en formato PNG , JPG, PDF",
			type  : "error",
			confirmButtonText : "Cerrar"
		});
	}else if(imagen['size'] > 2000000 ) {
		$(".NuevaFoto").val('');
		swal({
			title : "Error al subir el archivo",
			text  : "El archivo no debe pesar mas de 2MB",
			type  : "error",
			confirmButtonText : "Cerrar"
		});
	}else{
		if(imagen['type'] == 'image/jpeg' || imagen['type'] == 'image/png' || imagen['type'] != "application/pdf"){
			var datosImagen = new FileReader();
			datosImagen.readAsDataURL(imagen);

			$(datosImagen).on("load", function(event){
				var rutaimagen = event.target.result;
				if(imax == '0'){
					$(".previsualizar").attr('src', rutaimagen);
				}else{
					$(".previsualizar_"+imax).attr('src', rutaimagen);
				}
				
				
			});	
		}
		
	}	
});


var tablaEntidades = $('#tablaEntidades').DataTable({
    "ajax": 'ajax/entidades.ajax.php?getDataEntidades',
    "columnDefs": [
        {
            "targets": -1,
            "data": null,
            render: {
                display: function (data, type, row) {
                    
                    let edicion = '<div class="btn-group dropstart" role="group">';
                    edicion += '<button type="button" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    edicion += '<i class="fa fa-info-circle"></i>';
                    //edicion += '<span class="sr-only">Toggle Dropdown</span>';
                    edicion += '</button>';
                    edicion += '<ul class="dropdown-menu" role="menu">';
                    edicion += '<li><a class="dropdown-item btnEditarEntidade" title="Editar Entidad" idEntidades " data-bs-toggle="modal" data-bs-target="#modalEditarEntidades" href="#">EDITAR</a></li>';
                    edicion += '<li class="divider"></li>';
                    edicion += '<li><a class="dropdown-item btnEliminarEntidades" title="Eliminar Entidad" idEntidades href="#">ELIMINAR</a></li>';
                    
                    if(row[7] != ''){
                        edicion += '<li class="divider"></li>';
                        edicion += '<li><a class="dropdown-item" title="Descargar Formato Para Radicación De Incapacidades" target="_blank" href="'+row[7] +'">VER FORMATO 1</a></li>';
                    }

                    if(row[8] != ''){
                        edicion += '<li class="divider"></li>';
                        edicion += '<li><a class="dropdown-item" title="Descargar Formato Solicitud De Pago Incapcacidades" target="_blank" href="'+row[8] +'">VER FORMATO 2</a></li>';
                    }

                    if(row[9] != ''){
                        edicion += '<li class="divider"></li>';
                        edicion += '<li><a class="dropdown-item" title="Descargar Otros Formatos" target="_blank" href="'+row[9] +'">VER FORMATO 3</a></li>';
                    }

                    edicion += '</ul>';
                    edicion += '</div>';
                    return edicion;
                },
            },
        }
    ],
    "language" : {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    } 
});

$('#tablaEntidades tbody').on( 'click', 'a', function () {
    
    var data = tablaEntidades.row( $(this).parents('tr') ).data();
    $(this).attr("idEntidades", data[6]);
   
} );

/* Editar Entidades */
$('#tablaEntidades tbody').on("click", ".btnEditarEntidade", function(){
	var x = $(this).attr('idEntidades');
    var datas = new FormData();
    datas.append('EditarEntidadId', x);
    $.ajax({
        url   : 'ajax/entidades.ajax.php',
        method: 'post',
        data  : datas,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        success     : function(data){
            $("#EditarNombre").val(data.ent_nombres); 
			$("#EditarCodigo").val(data.ent_codigo);		

			$("#EditarCiudad").val(data.ent_ciudad);
			$("#EditarDireccion").val(data.ent_direccion);
			$("#EditarTelefono").val(data.ent_telefono);
			
			$("#EditarHorario").val(data.ent_horario);
			$("#EditarId").val(data.ent_id);
			$("#EditarObservacion").val(data.ent_observacion);
			
			if(data.ent_formato_1 != null && data.ent_formato_1 != ''){
				$("#formato1").val(data.ent_formato_1);
				$(".previsualizar").attr('src', data.ent_formato_1);
			}

			if(data.ent_formato_2 != null && data.ent_formato_2 != ''){
				$("#formato2").val(data.ent_formato_2);
				$(".previsualizar_2").attr('src', data.ent_formato_2);
			}

			if(data.ent_formato_3 != null && data.ent_formato_3 != ''){
				$("#formato3").val(data.ent_formato_3);
				$(".previsualizar_3").attr('src', data.ent_formato_3);
			}
        }

    });
});

 //Nueva entidad
$("#btnNuevaEntidad").click(function(){
    var formData = new FormData($("#AgragrNuevaEntidad")[0]);
    $.ajax({
        url   : 'ajax/entidades.ajax.php',
        method: 'post',
        data  : formData,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend:function(){
            $.blockUI({ 
                baseZ: 2000,
                css: { 
                    border: 'none', 
                    padding: '1px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                } 
            }); 
        },
        complete:function(){
            tablaEntidades.ajax.reload();
            $("#modalAgregarEntidades").modal('hide');  
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data.code == '1'){
                alertify.success( data.message );    
            }else{
                alertify.error( data.message );
            } 
        },
        //si ha ocurrido un error
        error: function(){
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});


/* Eliminar Entidades */
$('#tablaEntidades tbody').on("click", ".btnEliminarEntidades", function(){
    var x = $(this).attr('idEntidades');//obtengo el id a eliminar
    swal.fire({//creo la alerta con el plugin swall
        title: '¿Está seguro de borrar la entidad?',//titulo de alerta
        text: "¡Si no lo está, puede cancelar la accíón!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrala!'
    }).then(function (result) {
        if (result.value) {
          
            var datas = new FormData();
            datas.append('id_entidad', x);
            $.ajax({
                url   : 'ajax/entidades.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                   // tabla_usuariosEps.ajax.reload();
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    if(data.code == '1'){
                        alertify.success( data.desc );    
                    }else{
                        alertify.error( data.desc );
                    } 
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        }
    })
});

/*Editar Entidad*/
$("#btnEditarEntidade").click(function(){
    var formData = new FormData($("#EditarEntidadExistente")[0]);
    $.ajax({
        url   : 'ajax/entidades.ajax.php',
        method: 'post',
        data  : formData,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend:function(){
            $.blockUI({ 
                baseZ: 2000,
                css: { 
                    border: 'none', 
                    padding: '1px', 
                   backgroundColor: '#000', 
                   '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                   color: '#fff'
               } 
            }); 
        },
        complete:function(){
            tablaEntidades.ajax.reload();
            $("#modalEditarEntidades").modal('hide');  
            $.unblockUI();
        },
            // una vez finalizado correctamente
             success: function(data){
            if(data.code == '1'){
               alertify.success( data.message );    
           }else{
              alertify.error( data.message );
           } 
        },
            // si ha ocurrido un error
             error: function(){
            after_save_error();
           alertify.error('Error al realizar el proceso');
        }
    });
});

/* Activar Entidades */
$('#tablaEntidades tbody').on("click", ".btnActivar", function(){
	var idUsuario = $(this).attr('id_cliente');
	var estado = $(this).attr('estado');
	var datos = new FormData();
	datos.append("ActivarId", idUsuario);
	datos.append("estado", estado);
	$.ajax({
		url  : 'ajax/Entidades.ajax.php',
		type : 'post',
		data : datos,
		cache: false,
		contentType:false,
		processData : false,
		success: function(data){
			
		}
	});

	if(estado == 0){
		$(this).removeClass('btn-success');
		$(this).addClass('btn-danger');
		$(this).html('Desactivado');
		$(this).attr('estado', 1);
	}else{
		$(this).removeClass('btn-danger');
		$(this).addClass('btn-success');
		$(this).html('Activado');
		$(this).attr('estado', 0);
	}
});

