
/* validar los tipos de imagenes y/o archivos */
$(".NuevaFoto").change(function(){
	var imax = $(this).attr('valor');
	var imagen = this.files[0];
	console.log(imagen);
	/* Validar el tipo de imagen */
	if(imagen['type'] != 'image/jpeg' && imagen['type'] != 'image/png' && imagen['type'] != "application/pdf" ){
		$(".NuevaFoto").val('');
		swal({
			title : "Error al subir el archivo",
			text  : "El archivo debe estar en formato PNG , JPG, PDF",
			type  : "error",
			confirmButtonText : "Cerrar"
		});
	}else if(imagen['size'] > 2000000 ) {
		$(".NuevaFoto").val('');
		swal({
			title : "Error al subir el archivo",
			text  : "El archivo no debe pesar mas de 2MB",
			type  : "error",
			confirmButtonText : "Cerrar"
		});
	}else{
		if(imagen['type'] == 'image/jpeg' || imagen['type'] == 'image/png' || imagen['type'] != "application/pdf"){
			var datosImagen = new FileReader();
			datosImagen.readAsDataURL(imagen);

			$(datosImagen).on("load", function(event){
				var rutaimagen = event.target.result;
				if(imax == '0'){
					$(".previsualizar").attr('src', rutaimagen);
				}else{
					$(".previsualizar"+imax).attr('src', rutaimagen);
				}
				
				
			});	
		}
		
	}	
});


var tablaClientes = $('#tablaClientes').DataTable({
    "language" : {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    } 
});

$("#btnGuardarCliente").click(function () {
	//Se crean un array con los datos a enviar, apartir del formulario 
	var formData = new FormData($("#frmRegistroCliente")[0]);
	$.ajax({
		url: 'ajax/clientes.ajax.php',
		type  : 'post',
		data: formData,
		dataType : 'json',
		cache: false,
		contentType: false,
		processData: false,
		beforeSend:function(){
			$.blockUI({ 
				message : '<h3>Un momento por favor....</h3>',
				baseZ: 2000,
				css: { 
					border: 'none', 
					padding: '1px', 
					backgroundColor: '#000', 
					'-webkit-border-radius': '10px', 
					'-moz-border-radius': '10px', 
					opacity: .5, 
					color: '#fff'
				} 
			}); 
		},
		complete:function(){
			$.unblockUI();
		},
		success: function(data){
			if(data.code == '1'){
				$("#frmRegistroCliente")[0].reset();
				$("#modalAgregarCliente").modal('hide');
				alertify.success( data.desc );    
			}else{
				alertify.error( data.desc );
			}
			location.reload();
		},
		//si ha ocurrido un error
		error: function(){
			//after_save_error();
			alertify.error('Error al realizar el proceso');
		}
	});
});

/* Editar clientes */
// $('#tablaClientes tbody').on("click", ".btnEditarCliente", function(){
// 	var x = $(this).attr('idCliente');
	
//     editClient(x);
// 	return;

// 	var datas = new FormData();
//     datas.append('EditarClienteId', x);
//     $.ajax({
//         url   : 'ajax/clientes.ajax.php',
//         method: 'post',
//         data  : datas,
//         cache : false,
//         contentType : false,
//         processData : false,
//         dataType    : 'json',
//         success     : function(data){
//             $("#EditarNombre").val(data.emp_nombre); 
// 			$("#EditarDireccion").val(data.emp_direccion);	

// 			if(data.emp_nit != null && data.emp_nit != ''){
// 				var t = data.emp_nit;
// 				var div = t.split('-');
// 				if(div.length > 1){
// 					$("#EditarNit").val(div[0]);
// 					$("#EditarNitVerificacion").val(div[1]);
// 				}else{
// 					$("#EditarNit").val(data.emp_nit);
// 				}
// 			}
					

// 			$("#EditarTelefono").val(data.emp_telefono);
// 			$("#EditarEmail").val(data.emp_email);
// 			$("#EditarRepresentante").val(data.emp_representante);
// 			$("#EditarCCRepresentante").val(data.emp_cc_representante);
// 			$("#Editar_idEmpresa").val(data.emp_id);
// 			$("#EditarFechaPagoSeguridad").val(data.emp_fecha_pago_seguridad);
// 			$("#EditarContacto").val(data.emp_contacto_gestion);
// 			$("#EditarCelular").val(data.emp_celular);


// 			$("#EditarCorreoContacto1").val(data.emp_correo_1);
// 			$("#EditarCorreoContacto2").val(data.emp_correo_2);
// 			$("#EditarCorreoContacto3").val(data.emp_correo_3);
// 			$("#EditarCorreoContacto4").val(data.emp_correo_4);

// 			$("#emp_correo_incapacidades_v").val(data.emp_correo_incapacidades_v);
// 			$("#emp_crv_v").val(data.emp_crv_v);

// 			$("#EditarEmailGestion").val(data.emp_email_gestion);
// 			$("#EditarContactoTesoreria").val(data.emp_contacto_tesoreria);
// 			var telefonoTeso = data.emp_telefono_tesoreria;
// 			if(telefonoTeso != null && telefonoTeso != ''){
// 				var i = telefonoTeso.split('ext');
// 				$("#EditarTelefonoTesoreria").val(i[0]);
// 				$("#EditarTelefonoTesoreriaExt").val(i[1]);	
// 			}

// 			if(data.emp_liquidacion_automatica_i == '1'){
// 				$("#EditarLiquidacionAutomatica").iCheck('check');    
//             }else{
//                 $("#EditarLiquidacionAutomatica").iCheck('uncheck');
//             }

//             $("#EditarPorcentajeGananciaInc").val(data.emp_porcentaje_i);
			
// 			var telefonoGesti = data.emp_telefono_gestion;
// 			if(telefonoGesti != null && telefonoGesti != ''){
// 				var i = telefonoGesti.split('ext');
// 				$("#EditarTelefonoGestion").val(i[0]);
// 				$("#EditarTelefonoGestionExt").val(i[1]);	
// 			}
			
// 			$("#EditarEmailTesoreria").val(data.emp_email_tesoreria);
// 			$("#EditarCelularTesoreria").val(data.emp_celular_tesoreria);
			
// 			if(data.emp_ruta_rut != null && data.emp_ruta_rut != ''){
// 				$("#rutarutActual").val(data.emp_ruta_rut);
// 				ruta = data.emp_ruta_rut.split(".")[1];
// 				if(ruta == 'pdf'){
// 					$(".previsualizar").attr('src', 'vistas/img/plantilla/pdf.png');
// 				}else{
// 					$(".previsualizar").attr('src', data.emp_ruta_rut);	
// 				}
				
// 			}

// 			if(data.emp_ruta_camara_comercio != null && data.emp_ruta_camara_comercio != ''){
// 				$("#rutaCamaracomercioActual").val(data.emp_ruta_camara_comercio);
// 				ruta = data.emp_ruta_camara_comercio.split(".")[1];
// 				console.log(ruta);
// 				if(ruta == 'pdf'){
// 					$(".previsualizar_2").attr('src', 'vistas/img/plantilla/pdf.png');
// 				}else{
// 					$(".previsualizar_2").attr('src', data.emp_ruta_camara_comercio);	
// 				}
// 			}

// 			if(data.emp_ruta_cedula_representante != null && data.emp_ruta_cedula_representante != ''){
// 				$("#rutaCedulaRepresentanteActual").val(data.emp_ruta_cedula_representante);
// 				ruta = data.emp_ruta_cedula_representante.split(".")[1];
// 				if(ruta == 'pdf'){
// 					$(".previsualizar_3").attr('src', 'vistas/img/plantilla/pdf.png');
// 				}else{
// 					$(".previsualizar_3").attr('src', data.emp_ruta_cedula_representante);	
// 				}
				
// 			}

// 			if(data.emp_ruta_cuenta_bancaria != null && data.emp_ruta_cuenta_bancaria != ''){
// 				$("#rutaCuentaBancariaActual").val(data.emp_ruta_cuenta_bancaria);
// 				ruta = data.emp_ruta_cuenta_bancaria.split(".")[1];
// 				if(ruta == 'pdf'){
// 					$(".previsualizar_4").attr('src', 'vistas/img/plantilla/pdf.png');
// 				}else{
// 					$(".previsualizar_4").attr('src', data.emp_ruta_cuenta_bancaria);	
// 				}
				
// 			}

//         }

//     });




// });

/* Eliminar Clientes */
$('#tablaClientes tbody').on("click", ".btnEliminarCliente", function(){
	var x = $(this).attr('idCliente');
	Swal.fire({
		title: '¿Está seguro de borrar este cliente?',
		text: "¡Si no lo está puede cancelar la accíón!",
		icon: "warning",
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		cancelButtonText: 'Cancelar',
		confirmButtonText: 'Si, borrar cliente!'
	}).then(function (result) {
		if (result.value) {
			var datas = new FormData();
			datas.append('borrarCliente', x);
			$.ajax({
				url   : 'ajax/clientes.ajax.php',
				method: 'post',
				data  : datas,
				cache : false,
				contentType : false,
				processData : false,
				dataType    : 'json',
				beforeSend:function(){
					$.blockUI({ 
						baseZ: 2000,
						message : '<h3>Un momento por favor....</h3>',
						css: { 
							border: 'none', 
							padding: '1px', 
							backgroundColor: '#000', 
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .5, 
							color: '#fff' 
						} 
					}); 
				},
				complete:function(){
					$.unblockUI();
				},
				success     : function(data){
					location.reload();
					if(data.code == '1'){
						alertify.success(data.desc);
					}else{
						alertify.error(data.desc);
					}
				}
			});
		}
	});
});


/* Activar Clientes */
$('#tablaClientes tbody').on("click", ".btnActivar", function(){
	var idUsuario = $(this).attr('id_cliente');
	var estado = $(this).attr('estado');
	var datos = new FormData();
	datos.append("ActivarId", idUsuario);
	datos.append("estado", estado);
	$.ajax({
		url  : 'ajax/clientes.ajax.php',
		type : 'post',
		data : datos,
		cache: false,
		contentType:false,
		processData : false,
		success: function(data){
			
		}
	});

	if(estado == 0){
		$(this).removeClass('btn-success');
		$(this).addClass('btn-danger');
		$(this).html('Desactivado');
		$(this).attr('estado', 1);
	}else{
		$(this).removeClass('btn-danger');
		$(this).addClass('btn-success');
		$(this).html('Activado');
		$(this).attr('estado', 0);
	}
});

/* Enviar cambio de password */

/* validar que el Nit no esta repetido */
$("#NuevoNit").change(function(){
	$(".alert").remove();
	var usuario = $(this).val();
	var datos = new FormData();
	datos.append('validarNit', usuario);
	$.ajax({
		url   : 'ajax/clientes.ajax.php',
		method: 'post',
		data  : datos,
		cache : false,
		contentType : false,
		processData : false,
		dataType    : 'json',
		success     : function(respuesta){
			console.log(respuesta);
			if(respuesta != false){
				$("#NuevoNit").parent().after('<div class="alert alert-warning">Esta identificación / Nit ya existe en la base de datos</div>');
				$("#NuevoNit").val('');
			}
		}
	})

});


/* Usuarios de EPS Clientes */
$('#tablaClientes tbody').on("click", ".btnVerUsuariosEmpresa", function(){
    $("#modalVerUsuarioEps").modal();
});


// $.fn.datepicker.dates['es'] = {
//     days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
//     daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
//     daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
//     months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
//     monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
//     today: "Today",
//     clear: "Clear",
//     format: "yyyy-mm-dd",
//     weekStart: 0
// };


// $("#EditarFechaVenCamarac").datepicker({
//     language: "es",
//     autoclose: true,
//     todayHighlight: true
// });

// $("#EditarFechaVenCuentab").datepicker({
//     language: "es",
//     autoclose: true,
//     todayHighlight: true
// });


// $("#EditarFechaPagoSeguridad").datepicker({
//     language: "es",
//     autoclose: true,
//     todayHighlight: true
// });

flatpickr('.flatpickr-format', {
	altInput: true,
	locale: "es",
	altFormat: "d/m/Y",
	dateFormat: "Y-m-d",
});