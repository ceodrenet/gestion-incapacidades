

    $.fn.datepicker.dates['es'] = {
        days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
        daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        today: "Today",
        clear: "Clear",
        format: "yyyy-mm-dd", 
        weekStart: 0
    };

    $("#NuevoFechaInicio").datepicker({
        language: "es",
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#NuevoFechaFinal').datepicker('setStartDate', minDate);
    });


    $("#NuevoFechaFinal").datepicker({
        language: "es",
        autoclose: true,
        todayHighlight: true
    });


    $("#btnBuscarRangosEstados").click(function(){
 
        var fechaInicio = $("#NuevoFechaInicio").val();
        var fechaFinal  = $("#NuevoFechaFinal").val();
        var estadoTramite = $("#EditarEstadoTramite").val();
        var paso = 0;
        
        if(estadoTramite == '0'){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "El estado es necesario",
                type  : "error",
                confirmButtonText : "Cerrar"
            });

        }

        if(fechaFinal.length < 0){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha final es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });

        }   

        if(fechaInicio.length < 1){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha Inicial es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
        }

        if(paso == 0){
            $.ajax({
                url    : 'vistas/modulos/nomina/consolidado_estados.php',
                type   : 'post',
                data   :{
                    fechaInicial  : fechaInicio,
                    fechaFinal    : fechaFinal,
                    cliente_id    : $("#session").val(),
                    estado        : estadoTramite
                },
                dataType : 'html',
                success : function(data){
                    $("#resultados").html(data);
                   //$("#btnExportarRangos").attr('href', 'index.php?exportarNomina=true&fechaInicial='+ fechaInicio +'&fechaFinal='+ fechaFinal)
                    //$("#descargador").show();
                }
            })
        }
    });

