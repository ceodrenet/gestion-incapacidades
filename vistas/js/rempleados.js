	
	$("#btnBuscarEmpleados").click(function(){
        var empleadoId = $("#txtCedula").val();
        var paso = 0;
        
        if(empleadoId.length < 1){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La identificación es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });

        }


        if(paso == 0){
            $.ajax({
                url    : 'vistas/modulos/nomina/consolidado_empleados.php',
                type   : 'post',
                data   :{
                    empleado        : empleadoId,
                    cliente_id    : $("#session").val()
                },
                dataType : 'html',
                success : function(data){
                    $("#resultados").html(data);
                    $("#btnexportarEmpleados").attr('href', 'index.php?exportar=pdfEmpleadosconsRepor&cc='+$("#txtCedula").val());
                    $("#btnexportarEmpleados").show();
                    $("#btnexportarEmpleadosE").show();
                    $("#btnexportarEmpleadosE").attr('href', 'index.php?exportar=excelEmpleadosconsRepor&cc='+$("#txtCedula").val());
                   
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            })
        }
    });

