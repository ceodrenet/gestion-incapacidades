$(function(){
	editClient();

	$("#modalEditarCliente").click(function() {
    	editClient(localStorage.getItem("cliente_id"));
	});

	$("#enviarClientesInfo").click(function(){
	 	var form = $("#infoCliente");
        //Se crean un array con los datos a enviar, apartir del formulario 
        var formData = new FormData($("#infoCliente")[0]);
        $.ajax({
            url: 'ajax/clientes.ajax.php',
            type  : 'post',
            data: formData,
            dataType : 'json',
            cache: false,
            contentType: false,
            processData: false,
            beforeSend:function(){
                $.blockUI({ 
                    baseZ: 2000,
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff'
                    } 
                }); 
            },
            complete:function(){
                $.unblockUI();
            },
            //una vez finalizado correctamente
            success: function(data){
                if(data.code == '1'){
                    alertify.success( data.desc ); 
					window.location.reload();
                }else{
                    alertify.error( data.desc );
                }
                
            },
            //si ha ocurrido un error
            error: function(){
                //after_save_error();
                alertify.error('Error al realizar el proceso');
            }
        });
	});
});

function editClient() {
    
	var datas = new FormData();
    datas.append('EditarClienteId', $("#clienteLogieado").val());
    $.ajax({
        url   : 'ajax/clientes.ajax.php',
        method: 'post',
        data  : datas,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        success     : function(data){
            $("#EditarNombre").val(data.emp_nombre); 
			$("#EditarDireccion").val(data.emp_direccion);	

			if(data.emp_nit != null && data.emp_nit != ''){
				var t = data.emp_nit;
				var div = t.split('-');
				if(div.length > 1){
					$("#EditarNit").val(div[0]);
					$("#EditarNitVerificacion").val(div[1]);
				}else{
					$("#EditarNit").val(data.emp_nit);
				}
			}
					
			$("#EditarTelefono").val(data.emp_telefono);
			$("#EditarEmail").val(data.emp_email);
			$("#EditarRepresentante").val(data.emp_representante);
			$("#EditarCCRepresentante").val(data.emp_cc_representante);
			$("#Editar_idEmpresa").val(data.emp_id);
			$("#EditarFechaPagoSeguridad").val(data.emp_fecha_pago_seguridad);
			$("#EditarContacto").val(data.emp_contacto_gestion);
			$("#EditarCelular").val(data.emp_celular);

			$("#EditarFechaVenCuentab").val(data.emp_fecha_venc_cert_ban_d);
			$("#EditarFechaVenCamarac").val(data.emp_fecha_venc_cam_com_d);

			$("#EditarCorreoContacto1").val(data.emp_correo_1);
			$("#EditarCorreoContacto2").val(data.emp_correo_2);
			$("#EditarCorreoContacto3").val(data.emp_correo_3);
			$("#EditarCorreoContacto4").val(data.emp_correo_4);

			$("#emp_correo_incapacidades_v").val(data.emp_correo_incapacidades_v);
			$("#emp_crv_v").val(data.emp_crv_v);

			$("#EditarEmailGestion").val(data.emp_email_gestion);
			$("#EditarContactoTesoreria").val(data.emp_contacto_tesoreria);
			var telefonoTeso = data.emp_telefono_tesoreria;
			if(telefonoTeso != null && telefonoTeso != ''){
				var i = telefonoTeso.split('ext');
				$("#EditarTelefonoTesoreria").val(i[0]);
				$("#EditarTelefonoTesoreriaExt").val(i[1]);	
			}

			if(data.emp_liquidacion_automatica_i == '1'){
				$("#EditarLiquidacionAutomatica").attr('checked', true);    
            }else{
                $("#EditarLiquidacionAutomatica").attr('checked', false); 
            }

            $("#EditarPorcentajeGananciaInc").val(data.emp_porcentaje_i);
			
			var telefonoGesti = data.emp_telefono_gestion;
			if(telefonoGesti != null && telefonoGesti != ''){
				var i = telefonoGesti.split('ext');
				$("#EditarTelefonoGestion").val(i[0]);
				$("#EditarTelefonoGestionExt").val(i[1]);	
			}
			
			$("#EditarEmailTesoreria").val(data.emp_email_tesoreria);
			$("#EditarCelularTesoreria").val(data.emp_celular_tesoreria);
			
			if(data.emp_ruta_rut != null && data.emp_ruta_rut != ''){
				$("#rutarutActual").val(data.emp_ruta_rut);
				ruta = data.emp_ruta_rut.split(".")[1];
				if(ruta == 'pdf'){
					$(".previsualizar").attr('src', 'vistas/img/plantilla/pdf.png');
					$(".rut").attr('href', data.emp_ruta_rut);
				}else{
					$(".previsualizar").attr('src', data.emp_ruta_rut);	
					$(".rut").attr('href', data.emp_ruta_rut);
				}
			}

			if(data.emp_ruta_camara_comercio != null && data.emp_ruta_camara_comercio != ''){
				$("#rutaCamaracomercioActual").val(data.emp_ruta_camara_comercio);
				ruta = data.emp_ruta_camara_comercio.split(".")[1];
				if(ruta == 'pdf'){
					$(".previsualizar_2").attr('src', 'vistas/img/plantilla/pdf.png');
					$(".camaraComercio").attr('href', data.emp_ruta_camara_comercio);
				}else{
					$(".previsualizar_2").attr('src', data.emp_ruta_camara_comercio);
					$(".camaraComercio").attr('href', data.emp_ruta_camara_comercio);	
				}
			}

			if(data.emp_ruta_cedula_representante != null && data.emp_ruta_cedula_representante != ''){
				$("#rutaCedulaRepresentanteActual").val(data.emp_ruta_cedula_representante);
				ruta = data.emp_ruta_cedula_representante.split(".")[1];
				if(ruta == 'pdf'){
					$(".previsualizar_3").attr('src', 'vistas/img/plantilla/pdf.png');
					$(".cedulaRepresentante").attr('href', data.emp_ruta_cedula_representante);
				}else{
					$(".previsualizar_3").attr('src', data.emp_ruta_cedula_representante);	
					$(".cedulaRepresentante").attr('href', data.emp_ruta_cedula_representante);
				}
				
			}

			if(data.emp_ruta_cuenta_bancaria != null && data.emp_ruta_cuenta_bancaria != ''){
				$("#rutaCuentaBancariaActual").val(data.emp_ruta_cuenta_bancaria);
				ruta = data.emp_ruta_cuenta_bancaria.split(".")[1];
				if(ruta == 'pdf'){
					$(".previsualizar_4").attr('src', 'vistas/img/plantilla/pdf.png');
					$(".cuentaBancaria").attr('href', data.emp_ruta_cuenta_bancaria);
				}else{
					$(".previsualizar_4").attr('src', data.emp_ruta_cuenta_bancaria);
					$(".cuentaBancaria").attr('href', data.emp_ruta_cuenta_bancaria);	
				}
				
			}

        }

    });
}


