var totalDetalleCarteraNuevo = 0;

var tablaCartera = $('#tabalCartera').DataTable({
    "ajax": 'ajax/cartera.ajax.php?getDataCartera',
    "columnDefs": [
        {
            "targets": -1,
            "data": null,
            render: {
                display: function (data, type, row) {
                    
                    let edicion = '<div class="btn-group">';
                    edicion += '<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">';
                    edicion += '<i class="fa fa-info-circle"></i>';
                    edicion += '<span class="sr-only">Toggle Dropdown</span>';
                    edicion += '</button>';
                    edicion += '<ul class="dropdown-menu" role="menu">';
                    edicion += '<li><a class="dropdown-item btnEditarCartera" data-toggle="modal" idCartera title="Editar Cartera" data-target="#modalEditarCartera" href="#">EDITAR</a></li>';
                    edicion += '<li class="divider"></li>';
                    edicion += '<li><a class="dropdown-item btnEliminarCartera" title="Eliminar Cartera" idCartera href="#">ELIMINAR</a></li>';
                    edicion += '<li class="divider"></li>';
                    edicion += '<li><a class="dropdown-item btnEexportarCartera" title="Exportar cartera deglosada" idCartera href="#">EXPORTAR</a></li>';
    
                    if(row[9] != ''){
                        edicion += '<li class="divider"></li>';
                        edicion += '<li><a class="dropdown-item" title="Descargar PDF" target="_blank" href="'+row[9] +'">VER PDF</a></li>';
                    }

                    edicion += '</ul>';
                    edicion += '</div>';
                    return edicion;
                },
            },
        }
    ],
    "language" : {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    } 
});


$('#tabalCartera tbody').on( 'click', 'a', function () {
    
    var data = tablaCartera.row( $(this).parents('tr') ).data();
    $(this).attr("idCartera", data[8]);
   
} );

$.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Today",
    clear: "Clear",
    format: "yyyy-mm-dd", 
    weekStart: 0
};

$("#NuevoFechaSolicitud").datepicker({
    language: "es",
    autoclose: true,
    todayHighlight: true
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#NuevoFechaRadicacion').datepicker('setStartDate', minDate);
});


$("#NuevoFechaRadicacion").datepicker({
    language: "es",
    autoclose: true,
    todayHighlight: true
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#NuevoFechaRespuesta').datepicker('setStartDate', minDate);
});


$("#NuevoFechaRespuesta").datepicker({
    language: "es",
    autoclose: true,
    todayHighlight: true
});

$("#EditarFechaSolicitud").datepicker({
    language: "es",
    autoclose: true,
    todayHighlight: true
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#EditarFechaRadicacion').datepicker('setStartDate', minDate);
});


$("#EditarFechaRadicacion").datepicker({
    language: "es",
    autoclose: true,
    todayHighlight: true
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#EditarFechaRespuesta').datepicker('setStartDate', minDate);
});


$("#EditarFechaRespuesta").datepicker({
    language: "es",
    autoclose: true,
    todayHighlight: true
});

/*$("#NuevoEmpresa").select2({
    dropdownParent: $("#modalAgregarCartera"),
});*/

$("#NuevoValor").numeric();
$("#EditarValor").numeric();

/* Editar Cartera */
$('#tabalCartera tbody').on("click", ".btnEditarCartera", function(){
    var x = $(this).attr('idCartera');
    var datas = new FormData();
    datas.append('getCartera', x);
    $.ajax({
        url   : 'ajax/cartera.ajax.php',
        method: 'post',
        data  : datas,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        success     : function(data){
            $("#EditarEps").val(data.cartera.car_eps_id); 
            $("#EditarEps").val(data.cartera.car_eps_id).change();                   
            $("#EditarFechaSolicitud").val(data.cartera.car_fecha_cre);
            $("#EditarFechaRadicacion").val(data.cartera.car_fecha_rad);
            $("#EditarValor").val(data.cartera.car_valor);
            $("#EditarObservacion").val(data.cartera.car_observacion);
            if(data.cartera.car_paz_salvo == 'SI'){
                $("#EditarPazSalvo").iCheck('check');
            }else{
                $("#EditarPazSalvo").iCheck('uncheck');
            }

            if(data.cartera.car_respuesta == 'SI'){
                $("#EditarRespuesta").iCheck('check');
                $("#EditarFechaRespuesta").attr('disabled', false);
            }else{
                $("#EditarRespuesta").iCheck('uncheck');
                $("#EditarFechaRespuesta").attr('disabled', true);
            }
            
            $("#EditarFechaRespuesta").val(data.cartera.car_fecha_res);
            $("#EditarEmpresa").val(data.cartera.car_emp_id);
            $("#EditarEmpresa").val(data.cartera.car_emp_id).change();
            $("#idCartera").val(data.cartera.car_id);
            $("#rutaEvidencia").val(data.cartera.car_ruta_pdf);


            $.each(data.detalle, function(i, item){
                var cuerpo = '<tr id="tr_'+i+'">';
                cuerpo += '<td><input type="text" class="form-control" placeholder="Nombres" name="NuevoNombreDetalle_'+i+'" value="'+item.ci_nombre+'"></td>';
                cuerpo += '<td><input type="text" class="form-control" placeholder="C&eacute;dula" name="NuevoCedulaDetalle_'+i+'" value="'+item.ci_identificacion+'"></td>';
                cuerpo += '<td><input type="text" class="form-control" placeholder="Periodo" name="NuevoPeriodoDetalle_'+i+'" value="'+item.ci_periodo+'"></td>';
                cuerpo += '<td><input type="text" class="form-control" placeholder="Valor" name="NuevoValorDetalle_'+i+'" value="'+item.ci_valor+'"></td>';
                cuerpo += '<td><button type="button" id="eliminarDetalle_'+i+'" miNum="'+item.ci_id+'" title="Eliminar Detalle de Cartera" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></td>';
                cuerpo += '</tr>';
                $("#tbodyParaCarteraEditar").append(cuerpo);
                $("#eliminarDetalle_"+i).click(function(){
                    var num = $(this).attr('miNum');
                    $("#tr_"+num).remove();
                });
                $("#NumeroDetalleEditar").val(i+1);
            });
        }

    });
});

/*Nueva Cartera*/
    $("#btnNuevaCartera").click(function(){
        var formData = new FormData($("#AgragrNuevaCartera")[0]);
                                        
        $.ajax({
            url   : 'ajax/cartera.ajax.php',
            method: 'post',
            data  : formData,
            cache : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            beforeSend:function(){
                $.blockUI({ 
                    baseZ: 2000,
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff'
                    } 
                }); 
            },
            complete:function(){
                tablaCartera.ajax.reload();
                $("#modalAgregarCartera").modal('hide');  
                $.unblockUI();
            },
            //una vez finalizado correctamente
            success: function(data){
                if(data.code == '1'){
                    alertify.success( data.desc );    
                }else{
                    alertify.error( data.desc );
                } 
            },
            //si ha ocurrido un error
            error: function(){
                //after_save_error();
                alertify.error('Error al realizar el proceso');
            }
        });
    });


/* Eliminar Clientes 
$('#tabalCartera tbody').on("click", ".btnEliminarCartera", function(){
    var x = $(this).attr('idCartera');
    swal({
        title: '¿Está seguro de borrar la cartera?',
        text: "¡Si no lo está puede cancelar la accíón!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar cartera!'
    },function(isConfirm) {
        if (isConfirm) {
            window.location = "index.php?ruta=cartera&id_Cartera="+x ;
        }
    })
});*/

/* Eliminar Cartera */
$('#tabalCartera tbody').on("click", ".btnEliminarCartera", function(){
    var x = $(this).attr('idCartera');
    swal({
        title: '¿Está seguro de borrar la cartera?',
        text: "¡Si no lo está, puede cancelar la accíón!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar cartera!'
    },function(isConfirm) {
        if (isConfirm) {
            var datas = new FormData();
            datas.append('id_Cartera', x);
            $.ajax({
                url   : 'ajax/cartera.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    tablaCartera.ajax.reload();
                    if(data.code == '1'){
                        alertify.success(data.desc);
                    }else{
                        alertify.error(data.desc);
                    }
            },
            beforeSend:function(){
                $.blockUI({ 
                    message : '<h3>Un momento por favor....</h3>',
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff' 
                    } 
                }); 
            },
            complete:function(){
                $.unblockUI();
            }

        });
        }
    })
});

/*Editar Cartera*/
$("#btnEditarCartera").click(function(){
    var formData = new FormData($("#EditarCarteraExistente")[0]);
    $.ajax({
        url   : 'ajax/cartera.ajax.php',
        method: 'post',
        data  : formData,
        cache : false,
        contentType : false,
        processData : false,
        dataType    : 'json',
        beforeSend:function(){
            $.blockUI({ 
                baseZ: 2000,
                css: { 
                    border: 'none', 
                    padding: '1px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff'
                } 
            }); 
        },
        complete:function(){
            tablaCartera.ajax.reload();
            $("#modalEditarCartera").modal('hide');  
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data.code == '1'){
                alertify.success( data.desc );    
            }else{
                alertify.error( data.desc );
            } 
        },
        //si ha ocurrido un error
        error: function(){
            //after_save_error();
            alertify.error('Error al realizar el proceso');
        }
    });
});

$("#NuevoRespuesta").on('ifChecked', function () { 
    $("#NuevoFechaRespuesta").attr('disabled', false);
});

$("#NuevoRespuesta").on('ifUnchecked', function () { 
    $("#NuevoFechaRespuesta").attr('disabled', true);
});


$("#EditarRespuesta").on('ifChecked', function () { 
    $("#EditarFechaRespuesta").attr('disabled', false);
});

$("#EditarRespuesta").on('ifUnchecked', function () { 
    $("#EditarFechaRespuesta").attr('disabled', true);
});


$("#btnAddIncapacidad").click(function(){
    var cuerpo = '<tr id="tr_'+totalDetalleCarteraNuevo+'">';
    cuerpo += '<td><input type="text" class="form-control" placeholder="Nombres" name="NuevoNombreDetalle_'+totalDetalleCarteraNuevo+'"></td>';
    cuerpo += '<td><input type="text" class="form-control" placeholder="C&eacute;dula" name="NuevoCedulaDetalle_'+totalDetalleCarteraNuevo+'"></td>';
    cuerpo += '<td><input type="text" class="form-control" placeholder="Periodo" name="NuevoPeriodoDetalle_'+totalDetalleCarteraNuevo+'"></td>';
    cuerpo += '<td><input type="text" class="form-control" placeholder="Valor" name="NuevoValorDetalle_'+totalDetalleCarteraNuevo+'"></td>';
    cuerpo += '<td><button type="button" id="eliminarDetalle_'+totalDetalleCarteraNuevo+'" miNum="'+totalDetalleCarteraNuevo+'" title="Eliminar Detalle de Cartera" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></td>';
    cuerpo += '</tr>';
    $("#tbodyParaCartera").append(cuerpo);
    $("#eliminarDetalle_"+totalDetalleCarteraNuevo).click(function(){
        var num = $(this).attr('miNum');
        $("#tr_"+num).remove();
    });
    totalDetalleCarteraNuevo++;
    $("#NumeroDetalleNuevo").val(totalDetalleCarteraNuevo);
});

$("#btnEditIncapacidad").click(function(){
    var totalDetalleCarteraEdicion = $("#NumeroDetalleEditar").val();
    var cuerpo = '<tr id="tr_'+totalDetalleCarteraEdicion+'">';
    cuerpo += '<td><input type="text" class="form-control" placeholder="Nombres" name="NuevoNombreDetalle_'+totalDetalleCarteraEdicion+'"></td>';
    cuerpo += '<td><input type="text" class="form-control" placeholder="C&eacute;dula" name="NuevoCedulaDetalle_'+totalDetalleCarteraEdicion+'"></td>';
    cuerpo += '<td><input type="text" class="form-control" placeholder="Periodo" name="NuevoPeriodoDetalle_'+totalDetalleCarteraEdicion+'"></td>';
    cuerpo += '<td><input type="text" class="form-control" placeholder="Valor" name="NuevoValorDetalle_'+totalDetalleCarteraEdicion+'"></td>';
    cuerpo += '<td><button type="button" id="eliminarDetalle_'+totalDetalleCarteraEdicion+'" miNum="'+totalDetalleCarteraEdicion+'" title="Eliminar Detalle de Cartera" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></td>';
    cuerpo += '</tr>';
    $("#tbodyParaCarteraEditar").append(cuerpo);
    $("#eliminarDetalle_"+totalDetalleCarteraEdicion).click(function(){
        var num = $(this).attr('miNum');
        $("#tr_"+num).remove();
    });
    totalDetalleCarteraEdicion++;
    $("#NumeroDetalleNuevo").val(totalDetalleCarteraEdicion);
});




$(".NuevaFotoCartera").change(function(){
    var imax = $(this).attr('valor');
    var imagen = this.files[0];
   
    if(imagen['type'] != 'image/jpeg' && imagen['type'] != 'image/png' && imagen['type'] != "application/pdf" ){
        $(".NuevaFotoCartera").val('');
        swal({
            title : "Error al subir el archivo",
            text  : "El archivo debe estar en formato PDF, PNG, JPG",
            type  : "error",
            confirmButtonText : "Cerrar"
        });
    }else if(imagen['size'] > 2000000 ) {
        $(".NuevaFotoCartera").val('');
        swal({
            title : "Error al subir el archivo",
            text  : "El archivo no debe pesar mas de 2MB",
            type  : "error",
            confirmButtonText : "Cerrar"
        });
    }else{
        if(imagen['type'] == 'image/jpeg' || imagen['type'] == 'image/png' || imagen['type'] != "application/pdf"){
            var datosImagen = new FileReader();
            datosImagen.readAsDataURL(imagen);; 
        }
        
    }   
});

$("#NuevoEmpresaExportarPDF").on('change', function(){
    var idEmpresa = $(this).val();
    $("#exportarLinkPdf").attr('href', 'index.php?exportar=pdfCartera&idEmpresa='+idEmpresa);
});