<?php
    session_start();
    $_SESSION['start'] = time();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    if(isset($_SESSION['iniciarSession']) && $_SESSION['iniciarSession'] == 'ok'){//if de validacion
    if(isset($_GET['exportar'])){
        ControladorPlantilla::setAuditoria('ControladorPlantilla', 'Exportar', $_SERVER['SERVER_NAME'].'/index.php?exportar='.$_GET['exportar'], 'Archivo');

        if($_GET['exportar'] == 'inc'){
            /*--REF-- Cambio de ruta para MVC -- 14/03/2022*/
            include "modulos/incapacidades/exportarIncapacidades.php";
            //include "modulos/incapacidades/exportarIncapacidadesSput.php";
        }

        if($_GET['exportar'] == 'incompletas'){
            include "modulos/exportar/exportarIncapacidadesIncompletas.php";
        }

        if($_GET['exportar'] == 'exportarFacturacion'){
            include "modulos/exportar/exportarIncapacidadesFacturadas.php";
        }

        if($_GET['exportar'] == 'incEmpleados'){
            include "modulos/nomina/exportarConsolidadoEmpleados.php";
        }

        if($_GET['exportar'] == 'incEmpleadosInc'){
            include "modulos/exportar/exportarEmpleadosIncapacitados.php";
        }

        if($_GET['exportar'] == 'empleados'){
            include "modulos/exportar/exportarEmpleados.php";
        }

        if($_GET['exportar'] == 'fallosIncapacidad'){
            include "modulos/exportar/exportarFalloIncapacidades.php";
        } 

        if($_GET['exportar'] == 'pdfGerencial'){
            include "modulos/exportar/pdfGerencial.php";
        }

        if($_GET['exportar'] == 'pdfGerencialV2'){
            include "modulos/exportar/pdfGerencialV2.php";
        }  
    
        if($_GET['exportar'] == 'cartera'){
            if($_SESSION['cliente_id'] != 0){
                include "modulos/exportar/exportarCartera.php";
            }else{
                include "modulos/exportar/exportarCarteraEmpresa.php";
            }
        }   

        if($_GET['exportar'] == 'carteraAntigua'){
            include "modulos/exportar/exportarCarteraAntiguaExcel.php";
        }

        if($_GET['exportar'] == 'pdfCartera'){
            include "modulos/exportar/exportarCarteraPdf.php";
        } 

        if($_GET['exportar'] == 'pdfEmpleadosconsRepor'){
            include "modulos/exportar/exportarIncapacidadesEmpleados.php";
        }

        if($_GET['exportar'] == 'excelEmpleadosconsRepor'){
            include "modulos/exportar/exportarIncapacidadesEmpleadosExcel.php";
        }

        if($_GET['exportar'] == 'pdfReportAdministradoras'){
            include "modulos/exportar/exportarIncapacidadesAdministradora.php";
        }

        if($_GET['exportar'] == 'pdfReportepagosgenerales'){
            include "modulos/exportar/exportarpdfReportepagosgenerales.php";
        }

        if($_GET['exportar'] == 'pdfReportepagosgeneralesExecell'){
            include "modulos/exportar/exportarpdfReportepagosgeneralesExecell.php";
        }

        if($_GET['exportar'] == 'pdfReportepagosEmpleados'){
            include "modulos/exportar/exportarpdfReportepagosEmpleados.php";
        }

        if($_GET['exportar'] == 'excelReportepagosEmpleados'){
            include "modulos/exportar/excelReportepagosEmpleados.php";   
        }
        
        if($_GET['exportar'] == 'pdfReportFacturas'){
            include "modulos/exportar/exportarFacturaIncapacidades.php";
        }

        if($_GET['exportar'] == 'pdfPagosxmesGeneral'){
            include "modulos/exportar/exportarPdfPagosxmesGenerals.php";
        }

        if($_GET['exportar'] == 'usuariosAdministradoraExcel'){
            include "modulos/exportar/exportarExcelUsuariosAdministradora.php";
        }

        if($_GET['exportar'] == 'pdfUsuariosAdministradora'){
            include "modulos/exportar/exportarPdfUsuariosAdministradora.php";
        }

        if($_GET['exportar'] == 'incNominaExc'){
            include "modulos/exportar/exportarIncapacidadesNominaExcel.php";
        }

        if($_GET['exportar'] == 'incNominaPdf'){
            include "modulos/exportar/exportarIncapacidadesNominaPdf.php";
        }


        if($_GET['exportar'] == 'gananExcel'){
            include "modulos/exportar/exportarGananciasGeinExcel.php";
        }

        if($_GET['exportar'] == 'gananPdf'){
            include "modulos/exportar/exportarGananciasGeinPDF.php";
        }        

        if($_GET['exportar'] == 'horasLaboradas'){
            include "modulos/exportar/exportarHorasSessiones.php";
        }      

        if($_GET['exportar'] == 'horasLaboradasPdf'){
            include "modulos/exportar/exportarHorasSessionesPdf.php";
        }  

        if($_GET['exportar'] == 'exportarCasosEspeciales'){
            include 'modulos/casosespeciales/exportarExcel.php';            
        }  

        if($_GET['exportar'] == 'exportarCasosEspecialesConsolidado'){
            include 'modulos/casosespeciales/exportarExcelConsolidado.php';  
        }

        if($_GET['exportar'] == 'exportarCasosEspecialesPDF'){
           include 'modulos/casosespeciales/exportarPdf.php'; 
        }

         if($_GET['exportar'] == 'exportarCasosEspecialesUnitarios'){
           include 'modulos/casosespeciales/exportarExcelUnitario.php'; 
        }

        if($_GET['exportar'] == 'exportarNominaPdfNew'){
            include 'modulos/nomina/exportarNominaReportePdf.php';
        }
        
        if($_GET['exportar'] == 'incapacidadIndividual'){
            include 'modulos/exportar/exportarIncapacidadesUnitariasPDF.php';
        }
        
    }else if(isset($_GET['exportarNomina'])){
        include "modulos/nomina/exportarIncapacidades.php";
    }else if(isset($_GET['exportarAlertas'])){
        include "modulos/exportar/exportarAlertas.php";
    }else if(isset($_GET['exportarAdministradoraValores'])){
        include "modulos/exportar/exportarValoresAdministradora.php";
    }else{
?>
<!DOCTYPE html>
<html>
    
    <?php include 'assets/layouts/head-main.php'; ?>

    <head>
        <title>DashBoard | I-Reports</title>
        <link rel="shortcut icon" href="vistas/img/plantilla/LCortado.png">
        <?php include 'assets/layouts/head.php'; ?>
        
        <?php include 'assets/layouts/head-style.php'; ?>
    </head>
    <?php flush(); ?> 
    <body class="pace-done sidebar-enable" style data-sidebar-size="sm">
        <script src="vistas/assets/assets/libs/jquery/jquery.min.js"></script>
    <?php include 'assets/layouts/menu.php'; ?>

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <?php 
                    if(isset($_GET['ruta'])){
                        switch ($_GET['ruta']) {
                            case 'inicio':
                                if($_SESSION['cliente_id'] == 0){
                                    //include "modulos/inicio.php";
                                }else{
                                    include "modulos/dashboard/dashboard.php";
                                }
                                break;

                            case 'usuarios':
                               
                                include "modulos/usuarios.php";
                                echo "<script>$('#usuarios').addClass('active');$('#usuariosInternos').addClass('active')</script>";
                                break;

                            case 'usuariosExt':
                               
                                include "modulos/usuariosExternos.php";
                                echo "<script>$('#usuarios').addClass('active');$('#usuariosExt').addClass('active')</script>";
                                break;

                            case 'incapacidades':
                                include "modulos/incapacidades/incapacidades.php";
                                break;

                            case 'incapacidadesExportTotal':
                                include "modulos/incapacidades/exportarIncapacidadesTotalEmpresas.php";
                                break;

                            case 'incapacidadesnomina':
                                include "modulos/incapacidadesnomina.php";
                                echo "<script>
                                    $('#incapacidadesTre').addClass('active');
                                    $('#incapacidadesnomina').addClass('active');
                                    </script>";
                                
                                break;

                            case 'entidades':
                                include "modulos/entidades.php";
                                echo "<script>$('#entidades').addClass('active');</script>";
                                break;

                            case 'auditoria':
                                include "modulos/auditoria/auditoria.php";
                                echo "<script>$('#auditoria').addClass('active');</script>";
                                break;

                            case 'registrar-entidades':
                                include "modulos/registroEntidades/registrarEntidades.php";
                               echo "<script>$('#registrarEntidades').addClass('active');</script>";
                                break;

                            case 'notificaciones':
                                include "modulos/entiadesConfiguracion.php";
                                echo "<script>$('#notificaciones').addClass('active');</script>";
                                break;

                            case 'perfiles':
                                if($_SESSION['perfilN'] == 'SuperAdministrador' || $_SESSION['perfilN'] == 'Administrador'){
                                    include "modulos/perfiles.php";
                                }else{
                                    include "modulos/404.php";    
                                }
                                break;

                            case 'facturas':
                               
                                include "modulos/facturas.php";
                                echo "<script>$('#facturas').addClass('active');</script>";
                                break;

                            case 'clientes':
                                
                                /* ********* Visualiza formulario de editar cliente en caso que sea una empresa ********** */

                                if($_SESSION['cliente_id'] != 0){
                                    include "modulos/clientes/editar_cliente.php";
                                 }else{
                                    include "modulos/clientes/clientes.php";
                                }
                                break;

                                /* *********************************************************************************** */     

                            case 'usuariosadministradora':
                                include "modulos/usuariosEps.php";
                                echo "<script>$('#usuariosadministradora').addClass('active');</script>";
                                break;

                            case 'reportes':
                                include "modulos/reportes/reportes.php";
                              // pendiente echo "<script>$('#reportes').addClass('active');</script>";
                                break;

                            case 'soporteradicaciones':
                                include "modulos/soporteradicaciones.php";
                                echo "<script>$('#soporteradicaciones').addClass('active');</script>";
                                break;

                            case 'empleados':
                                if($_SESSION['cliente_id'] != 0){
                                    include "modulos/empleados/empleados.php";
                                }else{
                                    include "modulos/empleados/empleadosEmpresa.php";
                                }
                                break;

                            case 'archivos':
                                include "modulos/archivos.php";
                                echo "<script>$('#archivos').addClass('active');</script>";
                                break;

                                //RFB-11072022
                                case 'consultas':
                                    include "modulos/consultas/index.php";
                                    echo "<script>$('#consultas').addClass('active');</script>";
                                    break;

                                    case 'recepcion':
                                        include "modulos/recepcion/index.php";
                                        echo "<script>$('#recepcion').addClass('active');</script>";
                                        break;

                            case 'cartera':
                                include "modulos/cartera.php";
                                echo "<script>$('#cartera').addClass('active');</script>";
                                break;

                            case 'adminganancias':
                                include "modulos/ganancias.php";
                                echo "<script>$('#adminganancias').addClass('active');</script>";
                                break;

                            case 'salir':
                               
                                include "modulos/salir.php";
                                break;

                            case 'gerencial':
                                include 'modulos/gerencial/gerencial.php';
                             //pendiente   echo "<script>$('#reportes').addClass('active');</script>";
                                break;

                            case 'gerencialNew':
                                include 'modulos/NewGerencial/index.php';
                                 echo '<script>$("#reportes").addClass("active");</script>';
                                break;

                            case 'casosespeciales':
                                include 'modulos/casosespeciales/index.php';
                                echo '<script>$("#casosespeciales").addClass("active");</script>';
                                break;

                            case 'casosespeciales-permanentes':
                                include 'modulos/casosespeciales/incapacitadosPermanentes.php';
                                echo '<script>$("#casosespeciales").addClass("active");</script>';
                                break;
                            
                            case 'informeGerencial':
                                include 'modulos/reporteGerencial/index.php';
                                echo '<script>$("#informeGerencial").addClass("active");</script>';
                                break;


                            case 'infoGesEsp':
                                include 'modulos/reporteEspecialGes/index.php';
                                echo '<script>$("#infoGesEsp").addClass("active");</script>';
                                break;

                            case 'infoGesEspAnu':
                                include 'modulos/reporteEspecialGes/infoGesEspAnu.php';
                                echo '<script>$("#infoGesEspAnu").addClass("active");</script>';
                                break;

                                

                            case 'crm':
                                include "modulos/crm/dashboard.php";
                                echo "<script>$('#crm').addClass('active');</script>";
                                break;

                            /* reportes de tesoreria */
                            case 'generalpagos':
                                include "modulos/reportesTesoreria/filtros_general.php";
                            //pendiente  echo "<script>$('#reportes').addClass('active');</script>";
                                break;

                            case 'pagosempleados':
                                include "modulos/reportesTesoreria/filtrosEmpleados.php";
                                //pendiente  echo "<script>$('#reportes').addClass('active');</script>";  
                                break;

                            case 'detpagoxmes':
                                include "modulos/reportesTesoreria/detallepagoxmes.php";
                              //pendiente  echo "<script>$('#reportes').addClass('active');</script>";   
                                break;

                            case 'consoliadosaldos':
                                include "modulos/reportesTesoreria/filtrosConsolidadosSaldos.php";
                              //pendiente echo "<script>$('#reportes').addClass('active');</script>";   
                                break;

                            case 'informeSaldos':
                                include "modulos/reportesTesoreria/informeSaldo.php";
                                break;

                            case 'informeSaldosEntidad':
                                include "modulos/reportesTesoreria/informeSaldoEntidad.php";
                                break;

                            case 'informeRecepcion':
                                include "modulos/nomina/informeRecepcion.php";
                                break;
                            
                            case 'consolidadoPQRS':
                                include "modulos/nomina/consolidadoPQRS.php";
                                break;

                            case 'incByEmpleado':
                                include "modulos/nomina/histIncapacidadesByEmpleado.php";
                                break;
                                
                            /* Reportes seguridad y Salud */
                            case 'diagnosticofrecuente':
                                include "modulos/seguridadysalud/diagnosticofrecuente.php";
                               //pendiente  echo "<script>$('#reportes').addClass('active');</script>";
                                break;

                            case 'diagnosticoincapacidades':
                                include "modulos/seguridadysalud/diagnosticoincapacidades.php";
                              //pendiente  echo "<script>$('#reportes').addClass('active');</script>";
                                break;
                                
                            case 'estadisticas':
                                include "modulos/seguridadysalud/estadisticasIncapacidades.php";
                            //pendiente   echo "<script>$('#reportes').addClass('active');</script>";
                                break;

                            case 'mesincapacidades':
                                include "modulos/seguridadysalud/mesincapacidades.php";
                             //pendiente   echo "<script>$('#reportes').addClass('active');</script>";
                                break;

                            /* Reportes de nomina */
                            case 'consolidadoinc':
                                include "modulos/nomina/consolidadoinc.php";
                             //pendiente    echo "<script>$('#reportes').addClass('active');</script>";
                                break;

                            case 'incapacidadesestado':
                                include "modulos/nomina/incapacidadesestado.php";
                             //pendiente    echo "<script>$('#reportes').addClass('active');</script>";
                                break;

                            case 'incapacidadesEmpleados':
                                include "modulos/nomina/incapacidadesEmpleados.php";
                              //pendiente   echo "<script>$('#reportes').addClass('active');</script>";
                                break;

                            case 'administradoras':
                                include "modulos/nomina/administradoras.php";
                               //pendiente  echo "<script>$('#reportes').addClass('active');</script>";
                                break;

                            case 'centrocostoRep':
                                include "modulos/nomina/centrocosto.php";
                                //pendiente echo "<script>$('#reportes').addClass('active');</script>";
                                break;

                            case 'repCarNom':
                                include "modulos/nomina/reporteNomina.php";
                                //pendiente echo "<script>$('#reportes').addClass('active');</script>";
                                break;
                            
                            case '90dias':
                                include "modulos/alertas/mas90dias.php";
                                break;

                            case '120dias':
                                include "modulos/alertas/mas120dias.php";
                                break;

                            case '180dias':
                                include "modulos/alertas/mas180dias.php";
                                break;

                            case '540dias':
                                include "modulos/alertas/mas540dias.php";
                                break;

                            case 'calendario':
                                include "modulos/alertas/calendario.php";
                                echo "<script>$('#micalendario').addClass('menu-open active');</script>";
                                break;

                            case 'actividades':
                                include "modulos/alertas/actividadesDiarias.php";
                                echo "<script>$('#micalendario').addClass('menu-open active');</script>";
                                break;

                            case 'info-cartera-antigua':
                                include "modulos/reportesTesoreria/carteraAntigua.php";
                                echo "<script>$('#info-cartera-antigua').addClass('menu-open active');</script>";
                                break;

                            case 'infoGestionPBI':
                                include "modulos/consulta/infoGestionPBI.php";
                                echo "<script>$('#consulta').addClass('active');</script>";
                                break;

                            case 'infoGestionReIn':
                                include "modulos/consulta/infoGestionReIn.php";
                                echo "<script>$('#consulta').addClass('active');</script>";
                                break;

                            case 'infoGestionML':
                                include "modulos/consulta/infoGestionML.php";
                                echo "<script>$('#consulta').addClass('active');</script>";
                                break;

                                case 'infoGestionPRE':
                                    include "modulos/consulta/infoGestionPRE.php";
                                    echo "<script>$('#consulta').addClass('active');</script>";
                                    break;


                            case 'consulta':
                                include "modulos/consulta/index.php";
                                echo "<script>$('#consulta').addClass('active');</script>";
                                break;

                            case 'medicos':
                                include "modulos/medicos/medicos.php";
                                echo "<script>$('#medicos').addClass('active');</script>";
                                break;


                            default:
                                include "modulos/404.php";
                                break;
                        }
                    }else{
                        if($_SESSION['cliente_id'] == 0){
                            //include "modulos/inicio.php";
                        }else{
                            include "modulos/dashboard/dashboard.php";
                        }
                        //echo "<script>$('#inicio').addClass('active');</script>";
                    }

                ?>                
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <?php include 'assets/layouts/footer.php'; ?>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->

<!-- Right Sidebar -->
<?php include 'assets/layouts/right-sidebar.php'; ?>
<!-- /Right-bar -->

<!-- JAVASCRIPT -->
<?php include 'assets/layouts/vendor-scripts.php'; ?>


<!-- App js -->
<script src="vistas/assets/assets/js/app.js"></script>
<!-- Blokear pr ajax -->
<script src="vistas/plugins/blockui/blockUi.js"></script>
<script src="vistas/js/plantilla.js"></script>
<?php /*
<script type="text/javascript" src="vistas/js/plantilla.js?v=<?php echo rand();?>"></script>
<script type="text/javascript" src="vistas/js/incapacidades.js?v=<?php echo rand();?>"></script>

<script type="text/javascript" src="vistas/js/clientes.js?v=<?php echo rand();?>"></script>
<script type="text/javascript" src="vistas/js/empleados.js?v=<?php echo rand();?>"></script>
<script type="text/javascript" src="vistas/js/perfiles.js?v=<?php echo rand();?>"></script>
<script type="text/javascript" src="vistas/js/entidades.js?v=<?php echo rand();?>"></script>
<script type="text/javascript" src="vistas/js/usuarios.js?v=<?php echo rand();?>"></script>
<script type="text/javascript" src="vistas/js/consolidado.js?v=<?php echo rand();?>"></script>
<script type="text/javascript" src="vistas/js/estados.js?v=<?php echo rand();?>"></script>
<script type="text/javascript" src="vistas/js/rempleados.js?v=<?php echo rand();?>"></script>
<script type="text/javascript" src="vistas/js/archivos.js?v=<?php echo rand();?>"></script>
<script type="text/javascript" src="vistas/js/cartera.js?v=<?php echo rand();?>"></script>
<script type="text/javascript" src="vistas/js/usuariosEps.js?v=<?php echo rand();?>"></script>

<!-- DWY -->
<script type="text/javascript" src="vistas/js/edit_client.js?v=<?php echo rand();?>"></script>
*/?>
    </body>
</html>
<?php
    }
}else{ //Else de if de validacion
    if(isset($_GET['ruta']) && $_GET['ruta'] == 'recuperarPassWord'){ //If de ruta de ingreso son logiun
        require_once 'modulos/recuperarPassword.php';
    }else if(isset($_GET['ruta']) && $_GET['ruta'] == 'registro'){ //else de ruta de ingreso son logiun
        require_once 'modulos/registro/index.php';
    }else if(isset($_GET['ruta']) && $_GET['ruta'] == 'mensaje'){ //else de ruta de ingreso son logiun
        require_once 'modulos/registro/mensaje.php';
    }else{//If de ruta de ingreso son logiun
        require_once 'modulos/login.php';    
    }//fin de If de ruta de ingreso son logiun
}//fin de if de validacion
?>