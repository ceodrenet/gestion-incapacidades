<?php
	session_start();
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../../controladores/mail.controlador.php';
    require_once '../../controladores/plantilla.controlador.php';
	require_once '../../controladores/incapacidades.controlador.php';
    require_once '../../controladores/tesoreria.controlador.php';
    require_once '../../modelos/dao.modelo.php';
    require_once '../../modelos/incapacidades.modelo.php';
    require_once '../../modelos/tesoreria.modelo.php';


    $filtro = $_POST['filtro'];
    $clasif = $_POST['clasificacion'];
    $fecha1 = $_POST['inc_fecha_inicio'];
    $fecha2 = $_POST['inc_fecha_final'];

    echo '<table id="tbl_Incapacidades" style="width:100%;" class="table table-striped table-bordered dt-responsive">
            <thead>
                <tr>
                    <th style="width: 1%;">#</th>';
                    if($_SESSION['cliente_id'] == 0){ 
                     	echo '<th style="width: 10%;">Empresa</th>';
                    }else{ 
                     	echo '<th style="width: 10%;">Cédula</th>';
                    }
                    
                    echo '<th style="width: 15%;">Nombre</th>
                    <th style="width: 15%;">Administradora</th>
                    <th style="width: 3%;">Diag</th>
                    <th style="width: 7%;">F. Inicio</th>
                    <th style="width: 5%;">Estado</th>
                </tr>
            </thead>
            <tbody>';
    $item = null;
    $valor = null;        
    if($clasif == '0'){
    	/*Procedemos por el filtro*/
    	$item = 'inc_estado_tramite';
    	$valor = $filtro;
    	
    }else{

    	if($clasif == 'X'){
	    	/*Procedemos por el origen*/
	    	$item = 'inc_origen';
	    	$valor = $filtro;

	    }else{
	    	/*Procedemos por clasificacion*/
	    	$item = 'inc_clasificacion';
	    	$valor = $clasif;
	    }	
	}

    $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_ClientesBetween($item, $valor,  $fecha1, $fecha2);

	foreach ($incapacidades as $key => $value) {
		echo '<tr>';
		echo '<td>'.($key+1).'</td>';
		if($_SESSION['cliente_id'] == 0){ 
			echo '<td>'.$value["inc_empresa"].'</td>';
		}else{
			echo '<td>'.$value["emd_cedula"].'</td>';
		}
		
		echo '<td>'.$value["inc_nombres_afiliado"].'</td>';

		if($value['inc_clasificacion'] != 4){
            if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                echo '<td>'.($value["inc_arl_afiliado"]).'</td>'; 
            }else{
                echo '<td>'.($value["inc_ips_afiliado"]).'</td>'; 
            }
        }else{
            echo '<td>'.($value["inc_afp_afiliado"]).'</td>';    
        }

		echo '<td>'.($value["inc_diagnostico"]).'</td>';
		echo '<td>'.explode(' ', $value["inc_fecha_inicio"])[0].'</td>';  
		echo '<td>'.$value["inc_estado"].'</td>'; 
		echo '</tr>';
	}
	echo '</tbody>
        <tfoot>
		    <tr>
                <th style="width: 1%;">#</th>';
                if($_SESSION['cliente_id'] == 0){ 
                 	echo '<th style="width: 10%;">Empresa</th>';
                }else{ 
                 	echo '<th style="width: 10%;">Cédula</th>';
                }
                
                echo '<th style="width: 15%;">Nombre</th>
                <th style="width: 15%;">Administradora</th>
                <th style="width: 3%;">Diag</th>
                <th style="width: 7%;">Fecha</th>
                <th style="width: 5%;">Estado</th>
            </tr>
        </tfoot>
    </table>

    <form id="formDescargaIncapacidades" method="post" action="vistas/modulos/exportar/exportarIncapacidadesConsolidado.php">  
        <input type="hidden" name="filtro" value="'.$filtro.'">
        <input type="hidden" name="clasificacion" value="'.$clasif.'">
        <input type="hidden" name="inc_fecha_inicio" value="'.$fecha1.'">
        <input type="hidden" name="inc_fecha_final" value="'.$fecha2.'">
    </form>
    ';

    echo '
    	<script type="text/javascript">
    		$("#tbl_Incapacidades").DataTable({
			    "language" : {
			        "sProcessing":     "Procesando...",
			        "sLengthMenu":     "Mostrar _MENU_ registros",
			        "sZeroRecords":    "No se encontraron resultados",
			        "sEmptyTable":     "Ningún dato disponible en esta tabla",
			        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
			        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
			        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			        "sInfoPostFix":    "",
			        "sSearch":         "Buscar:",
			        "sUrl":            "",
			        "sInfoThousands":  ",",
			        "sLoadingRecords": "Cargando...",
			        "oPaginate": {
			            "sFirst":    "<<",
			            "sLast":     ">>",
			            "sNext":     ">",
			            "sPrevious": "<"
			        },
			        "oAria": {
			            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			        }
			    }
			});
    	</script>

    ';