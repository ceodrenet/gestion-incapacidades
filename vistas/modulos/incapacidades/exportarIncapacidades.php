<?php
    
    //session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    

    $objPHPExcel = new Spreadsheet();
    
 
    $objPHPExcel->getActiveSheet()->getStyle('A1:AY1')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->setTitle('INCAPACIDADES');

    $objPHPExcel->getActiveSheet()
        ->getStyle('A1:AY1')
        ->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A1:AY1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('A1:AY1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

    $objPHPExcel->getActiveSheet()->getStyle('A1:AY1')->getFont()->setBold( true );


    $objPHPExcel->getActiveSheet()->setTitle('INCAPACIDADES');

    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa");
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Cod. Nomina"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Identificación");    
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Apellidos y Nombres"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Cargo"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Salario"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Sede"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Centro de Costos"); 
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "SubCentro de Costos"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Ciudad");
    $objPHPExcel->getActiveSheet()->setCellValue("L1", "Fecha Inicio"); 
    $objPHPExcel->getActiveSheet()->setCellValue("M1", "Fecha Final"); 
    $objPHPExcel->getActiveSheet()->setCellValue("N1", "Días"); 
    $objPHPExcel->getActiveSheet()->setCellValue("O1", "Clasificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("P1", "Diagnostico"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Q1", "Descripcion"); 
    $objPHPExcel->getActiveSheet()->setCellValue("R1", "Origen"); 
    $objPHPExcel->getActiveSheet()->setCellValue("S1", "Valor Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("T1", "Valor Administradora"); 
    $objPHPExcel->getActiveSheet()->setCellValue("U1", "Valor Ajuste"); 
    $objPHPExcel->getActiveSheet()->setCellValue("V1", "Fecha Pago En Nomina"); 

    $objPHPExcel->getActiveSheet()->setCellValue("W1", "Fecha Recepción"); 
    $objPHPExcel->getActiveSheet()->setCellValue("X1", "Fecha Generación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Y1", "Fecha Radicación");
    $objPHPExcel->getActiveSheet()->setCellValue("Z1", "Fecha Solicitud");
    $objPHPExcel->getActiveSheet()->setCellValue("AA1", "Fecha Negacion");

    $objPHPExcel->getActiveSheet()->setCellValue("AB1", "Estado Tramite");
    $objPHPExcel->getActiveSheet()->setCellValue("AC1", "Sub Estado Tramite");
    $objPHPExcel->getActiveSheet()->setCellValue("AD1", "Fecha de Estado");
    $objPHPExcel->getActiveSheet()->setCellValue("AE1", "Valor Pagado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AF1", "Fecha Pago"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AG1", "EPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AH1", "AFP"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AI1", "Profesional Responsable"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AJ1", "Registro Medico"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AK1", "Entidad-IPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AL1", "Documentos"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AM1", "Atención"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AN1", "Estado Empleado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AO1", "Creado Por"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AP1", "Editado Por"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AQ1", "N. Radicado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AR1", "Motivo Rechazo"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AS1", "Observacion 1"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AT1", "Observacion 2"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AU1", "Observacion 3"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AV1", "Observacion 4"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AW1", "Observacion 5"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AX1", "Soporte"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AY1", "Transcripción"); 

    $item = null;
    $valor = null;
    if($_SESSION['cliente_id'] != 0){
        $item = 'inc_empresa';
        $valor = $_SESSION['cliente_id'];
    }
    /*RFB----- validacion exportar estado tramite --- 300622*/
    $estadoTramite = null;
    if(isset($_GET['tipodescargas']) && $_GET['tipodescargas'] != 0 ){
        $estadoTramite = $_GET['tipodescargas'];
    }
    //$incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);
    if(isset($_GET['fechaInicio']) && isset($_GET['fechaFinal']) && $_GET['fechaInicio'] != null && $_GET['fechaFinal'] != null){
        $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor, $_GET['fechaInicio'], $_GET['fechaFinal'], null, '%Y-%m-%d', $estadoTramite);
    }else{
        $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor, null, null, null,'%Y-%m-%d',$estadoTramite);     
    }
   
    $i = 2;

    foreach ($incapacidades as $key => $value) {

        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["emd_codigo_nomina"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_cedula"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["emd_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["emd_cargo"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["emd_salario"]);  
        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value["emd_sede"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value["emd_centro_costos"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["emd_subcentro_costos"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $value["emd_ciudad"]);
        $date = new DateTime($value["inc_fecha_inicio"]);
        $date2 = new DateTime($value["inc_fecha_final"]);
        $objPHPExcel->getActiveSheet()->setCellValue("L".$i, PHPExcel_Shared_Date::PHPToExcel($date));
        $objPHPExcel->getActiveSheet()->getStyle("L".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");  
        $objPHPExcel->getActiveSheet()->setCellValue("M".$i, PHPExcel_Shared_Date::PHPToExcel($date2)); 
        $objPHPExcel->getActiveSheet()->getStyle("M".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $value['dias']);
        $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $value['inc_clasificacion']);
        $objPHPExcel->getActiveSheet()->setCellValue("P".$i, $value["inc_diagnostico"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$i, "NA"); 
        $objPHPExcel->getActiveSheet()->setCellValue("R".$i, $value["inc_origen"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("S".$i, $value["inc_valor_pagado_empresa"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("T".$i, $value["inc_valor_pagado_eps"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("U".$i, $value["inc_valor_pagado_ajuste"]);

        $date3 = new DateTime($value["inc_fecha_pago_nomina"]);
        $objPHPExcel->getActiveSheet()->setCellValue("V".$i, PHPExcel_Shared_Date::PHPToExcel($date3));
        $objPHPExcel->getActiveSheet()->getStyle("V".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");

        /*Fecha Recepción*/
        if($value["inc_fecha_recepcion_d"] != null){
            $inc_fecha_recepcion_d = new DateTime($value["inc_fecha_recepcion_d"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("W".$i, PHPExcel_Shared_Date::PHPToExcel($inc_fecha_recepcion_d));
            $objPHPExcel->getActiveSheet()->getStyle("W".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }
            
        /*Fecha generacion*/
        if($value["inc_fecha_generada"] != null){
            $inc_fecha_generada = new DateTime($value["inc_fecha_generada"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("X".$i, PHPExcel_Shared_Date::PHPToExcel($inc_fecha_generada));
            $objPHPExcel->getActiveSheet()->getStyle("X".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }
            

        /*Fecha Radicación*/
        if($value["inc_fecha_radicacion"] != null){
            $inc_fecha_radicacion = new DateTime($value["inc_fecha_radicacion"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("Y".$i, PHPExcel_Shared_Date::PHPToExcel($inc_fecha_radicacion));
            $objPHPExcel->getActiveSheet()->getStyle("Y".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }
            

        /*Fecha Solicitud*/
        if($value["inc_fecha_solicitud"] != null){
            $inc_fecha_solicitud = new DateTime($value["inc_fecha_solicitud"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("Z".$i, PHPExcel_Shared_Date::PHPToExcel($inc_fecha_solicitud));
            $objPHPExcel->getActiveSheet()->getStyle("Z".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }

        /*Fecha Negacion*/
        if($value["inc_fecha_negacion_"] != null){
            $inc_fecha_negacion_ = new DateTime($value["inc_fecha_negacion_"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("AA".$i, PHPExcel_Shared_Date::PHPToExcel($inc_fecha_negacion_));
            $objPHPExcel->getActiveSheet()->getStyle("AA".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }
        
        $estadoIncapacidad = '';        
        if($value['inc_estado'] == '0'){
            $estadoIncapacidad = 'INCOMPLETA';
        }else{
            $estadoIncapacidad = $value["inc_estado_tramite"];
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue("AB".$i, $estadoIncapacidad);

        $objPHPExcel->getActiveSheet()->setCellValue("AC".$i, $value['set_descripcion_v']);
        if($value["inc_Fecha_estado"] != null){
            $date5 = new DateTime($value['inc_Fecha_estado']);
            $objPHPExcel->getActiveSheet()->setCellValue("AD".$i, PHPExcel_Shared_Date::PHPToExcel($date5));
            $objPHPExcel->getActiveSheet()->getStyle("AD".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
            $objPHPExcel->getActiveSheet()->setCellValue("AE".$i, $value["inc_valor"]); 
        }
        
        if($value["inc_fecha_pago"] != null && $value["inc_fecha_pago"] != ''){
            $date6 = new DateTime($value["inc_fecha_pago"]); 
            
            $objPHPExcel->getActiveSheet()->setCellValue("AF".$i, PHPExcel_Shared_Date::PHPToExcel($date6)); 
            
            $objPHPExcel->getActiveSheet()->getStyle("AF".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
      
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("AF".$i, $value["inc_fecha_pago"] ); 
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue("AG".$i, $value["inc_ips_afiliado"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AH".$i, $value["inc_afp_afiliado"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AI".$i, $value["inc_profesional_responsable"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AJ".$i, $value["inc_prof_registro_medico"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AK".$i, $value["inc_donde_se_genera"]); 
        //$objPHPExcel->getActiveSheet()->setCellValue("AG".$i, ); 
        $objPHPExcel->getActiveSheet()->setCellValue("AL".$i, $value["inc_tipo_generacion"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AM".$i, $value["atn_descripcion"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AN".$i, $value["est_emp_desc_v"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AO".$i, $value["creador"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AP".$i, $value["editor"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AQ".$i, $value["inc_numero_radicado_v"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AR".$i, $value["mot_desc_v"]);


        $campos = "incm_comentario_t" ;
        $tabla = "gi_incapacidades_comentarios";
        $where = "inm_inc_id_i = ".$value['inc_id'];
        
        $respuesta = ModeloIncapacidades::mdlMostrarGroupAndOrder($campos,$tabla,$where, null, 'ORDER BY incm_fecha_comentario ASC');
        $contComentario = 0;
        foreach ($respuesta as $key => $valuex) {
            if($contComentario == 0){
                $objPHPExcel->getActiveSheet()->setCellValue("AS".$i, $valuex["incm_comentario_t"]);
            }else if($contComentario == 1){
                $objPHPExcel->getActiveSheet()->setCellValue("AT".$i, $valuex["incm_comentario_t"]);
            }else if($contComentario == 2){
                $objPHPExcel->getActiveSheet()->setCellValue("AU".$i, $valuex["incm_comentario_t"]);    
            }else if($contComentario == 3){
                $objPHPExcel->getActiveSheet()->setCellValue("AV".$i, $valuex["incm_comentario_t"]);
            }
            $contComentario++;
        }
        $objPHPExcel->getActiveSheet()->setCellValue("AW".$i, $value["mot_observacion_v"]);
        $valueTrans = "NO";
        $valueSopor = "NO";
        if($value["inc_ruta_incapacidad"] != null && $value["inc_ruta_incapacidad"] != ""){
            $valueSopor = "SI";
        }
        if($value["inc_ruta_incapacidad_transcrita"] != null && $value["inc_ruta_incapacidad_transcrita"] != ""){
            $valueTrans = "SI";
        }
        $objPHPExcel->getActiveSheet()->setCellValue("AX".$i, $valueSopor); 
        $objPHPExcel->getActiveSheet()->setCellValue("AY".$i, $valueTrans); 
        $i++;                 
    }

    foreach(range('A','AY') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }
    /*$objPHPExcel->getActiveSheet()->getStyle('H')->getNumberFormat()->setFormatCode("dd/mm/yyyy");
    $objPHPExcel->getActiveSheet()->getStyle('I')->getNumberFormat()->setFormatCode("dd/mm/yyyy");*/
    

    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    
    
   // ob_start();
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Informe_consolidado.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $writer = new Xlsx($objPHPExcel);
    $writer->save("php://output");
    /*$writer = new Xlsx($objPHPExcel);
    $writer->save("php://output");
    $xlsData = ob_get_contents();
    ob_end_clean(); 

    $response =  array(
        'op' => 'ok',
        'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
    );*/

    //echo json_encode($response);

