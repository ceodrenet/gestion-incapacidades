<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">INCAPACIDADES - EMPRESA <?php echo mb_strtoupper(ModeloIncapacidades::getDatos('gi_empresa', 'emp_id', $_SESSION['cliente_id'])['emp_nombre']); ?></h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Administrar Incapacidades</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row" id="FieldsFilters">
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">POR RADICAR</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo ControladorIncapacidades::getCountIncapacidades(date("Y"), "2"); ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger"><?php echo number_format(ControladorIncapacidades::getSumIncapacidades(date("Y"), "2"), 0, ',', '.'); ?></span>
                    <span class="ms-1 text-muted font-size-13">VALOR TOTAL ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">INCOMPLETAS</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo ControladorIncapacidades::getCountIncapacidadesIncompletas(); ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger"><?php echo number_format(ControladorIncapacidades::getSumIncapacidadesIncompletas(), 0, ',', '.'); ?></span>
                    <span class="ms-1 text-muted font-size-13">VALOR TOTAL ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col-->

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">RADICADAS</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo ControladorIncapacidades::getCountIncapacidades(date("Y"), "3"); ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-success text-success"><?php echo number_format(ControladorIncapacidades::getSumIncapacidades(date("Y"), "3"), 0, ',', '.'); ?></span>
                    <span class="ms-1 text-muted font-size-13">VALOR TOTAL ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-12">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">FILTRAR POR AÑOS</span>
                        <h4 class="mb-3">
                           <select id="filtrosIncapacidadAnual" class="form-control">
                               <?php 
                                    $i = 0;
                                    while ($i < 7) {
                                        echo "<option value='".(date('Y') - $i)."'>".(date('Y') - $i)."</option>";
                                        $i++;
                                    }
                               ?>
                           </select>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-primary text-primary"><?php echo date('Y');?></span>
                    <span class="ms-1 text-muted font-size-13">ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->
</div><!-- end row-->
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    <div class="row">
                        <div class="col-lg-11">
                            
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">
                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <a class="dropdown-item showAddInc" title="Agregar Incapacidad" data-bs-toggle="modal" data-bs-target="#modalAgregarIncapacidad" href="#">AGREGAR</a>
                                    <div class="dropdown-divider"></div>
                                    <?php } ?>
                            
                                    <!--<a class="dropdown-item" title="Exportar Consolidado de Incapacidades a Excel" data-bs-toggle="modal" data-bs-target="#modalExportarFechasIncapacidadesTotal" href="#">EXPORTAR</a></li>-->
                                    <a class="dropdown-item" title="Exportar Consolidado de Incapacidades a Excel" href="#" id="ExportarIncapacidadesLink">EXPORTAR</a></li>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" title="Exportar Consolidado de incapacidades con comentario" href="#" id="ExportarComentariosLink">EXP COMENTARIOS</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" title="Exportar Consolidado de Incapacidades Incompletas a Excel" data-bs-toggle="modal" data-bs-target="#modalExportarFechasIncapacidadesIncompletas" href="#">EXP. INCOMPLETAS</a>
                                    
                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#" title="Cargar Incapacidades Masivamente" data-bs-toggle="modal" data-bs-target="#modalAgregarIncapacidadesMasivamente">CARGAR INC</a>
                                    <?php } ?>

                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#" title="Cargar Comentarios Masivamente" data-bs-toggle="modal" data-bs-target="#modalAgregarComentariosMasivamente">CARGAR COM</a>
                                    <?php } ?>

                                    <?php if($_SESSION['edita'] == 1){ ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#"  title="Cargar Documentos Incapacidades Masivamente" data-bs-toggle="modal" data-bs-target="#modalAgregarDocumentosIncapacidadesMasivamente">CARGAR DOCS</a>
                                    <?php } ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
                <input type="hidden" name="session" id="session" value="<?php if(isset($_SESSION['cliente_id'])){ echo $_SESSION['cliente_id']; } else { echo 0; } ?>">
                <input type="hidden" id="editar" value="<?php echo $_SESSION['edita'];?>">
                <input type="hidden" id="elimina" value="<?php echo $_SESSION['elimina'];?>">

                <table id="tbl_Incapacidades" style="width:100%;" class="table table-bordered table-condensed display nowrap">  
                    <thead>
                        <tr>
                            <th style="width: 5%;"># INC.</th>
                            <th style="width: 10%;">CÉDULA</th>
                            <th style="width: 15%;">NOMBRES Y APELLIDOS</th>
                            <th style="width: 15%;">ADMINISTRADORA</th>
                            <th style="width: 5%;">DIAG.</th>
                            <th style="width: 10%;">FECHA INICIAL</th>
                            <th style="width: 10%;">FECHA FINAL</th>
                            <th style="width: 8%;">ORIGEN</th>
                            <th style="width: 8%;">RED</th>
                            <th style="width: 8%;">ESTADO</th>
                            <th style="width: 5%;">OPCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                    <tfoot>
                        <tr>
                            <th style="width: 5%;"># INC.</th>
                            <th style="width: 10%;">CÉDULA</th>
                            <th style="width: 15%;">NOMBRES Y APELLIDOS</th>
                            <th style="width: 15%;">ADMINISTRADORA</th>
                            <th style="width: 5%;">DIAG.</th>
                            <th style="width: 10%;">FECHA INICIAL</th>
                            <th style="width: 10%;">FECHA FINAL</th>
                            <th style="width: 8%;">ORIGEN</th>
                            <th style="width: 8%;">RED</th>
                            <th style="width: 8%;">ESTADO</th>
                            <th style="width: 5%;">OPCIONES</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Then put toasts within -->
<!--<div aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center w-100" style="min-height: 200px;">
    <div class="toast fade show" role="alert" aria-live="assertive" data-bs-autohide="false" aria-atomic="true">
        <div class="toast-header">
            <img src="assets/images/logo-sm.svg" alt="" class="me-2" height="18">
            <strong class="me-auto">Minia</strong>
            <small>9 min ago</small>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            <div class="spinner-border text-primary m-1" role="status">
                <span class="sr-only">Hello, world! This is a toast message.</span>
            </div>Estamos Generando el Reporte
        </div>
    </div>
</div>-->

<!-- Modal agregar incapacidad -->
<div id="modalAgregarIncapacidad"  class="modal fade"  data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" id="formCreacionIncapacidades" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar Incapacidad</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3" id="divInputCedulaNuevo">
                                    <label class="form-label">Ingresar Identificación</label>
                                    <input class="form-control empleadoClass" type="text" name="NuevoCedulaIncapacidad" id="NuevoCedulaIncapacidad" placeholder="Ingresar Identificación" required="true">
                                    <div id="estadoEmpleadoDiv">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Nombre Colaborador</label>
                                    <input class="form-control empleadoClass" type="text" name="NuevoNombreIncapacidad" id="NuevoNombreIncapacidad" readonly="true" placeholder="Nombre Afiliado" required="true">
                                </div>
                            </div>

                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Cargo del Colaborador</label>
                                    <input class="form-control empleadoClass " type="text" name="NuevoCargoEmpleado" id="NuevoCargoEmpleado" readonly="true" placeholder="Cargo del empleado" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Nombre Empresa</label>
                                    <input class="form-control empleadoClass" type="text" name="NuevoNombreEmpresa" id="NuevoNombreEmpresa" readonly='true' placeholder="Nombre Empresa" required="true">
                                </div>
                            </div>
                            
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">EPS</label>
                                    <!--<input class="form-control empleadoClass" type="text" name="NuevoIpsAfiliado" id="NuevoIpsAfiliado" readonly='true' placeholder="EPS" required="true">-->
                                    <select class="form-control empleadoClass" type="text" name="NuevoIpsAfiliado" id="NuevoIpsAfiliado" placeholder="EPS" required="true">
                                        <?php 
                                            $camppos ="ips_id , ips_nombre";
                                            $tabla__ = "gi_ips";
                                            $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,"ips_tipo = 'EPS'", null, 'ORDER BY ips_nombre ASC ');
                                            foreach ($atencion as $key => $value) {
                                                echo '<option value="'.$value['ips_id'].'">'.$value['ips_nombre'].'</option>';
                                            }
                                        ?>
                                    </select> 
                                </div> 
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha Afiliación EPS</label>
                                    <input class="form-control empleadoClass" readonly='true' type="text" name="NuevoFechaAfiliacionEps" id="NuevoFechaAfiliacionEps"  placeholder="Fecha Afiliación EPS" required="true">
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">ARL</label>
                                    <!--<input class="form-control empleadoClass" type="text" name="NuevoARLEmpleado" id="NuevoARLEmpleado" readonly='true' placeholder="ARL" required="true">-->
                                    <select class="form-control empleadoClass" type="text" name="NuevoArlAfiliado" id="NuevoArlAfiliado" placeholder="ARL" required="true">
                                        <?php 
                                            $camppos ="ips_id , ips_nombre";
                                            $tabla__ = "gi_ips";
                                            $arl = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,"ips_tipo = 'ARL'", null, 'ORDER BY ips_nombre ASC ');
                                            foreach ($arl as $key => $value) {
                                                echo '<option value="'.$value['ips_id'].'">'.$value['ips_nombre'].'</option>';
                                            }
                                        ?>
                                    </select>  
                                </div>
                            </div>
                        </div>
                        <div class="row">                        
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Estado Colaborador</label>
                                    <input class="form-control empleadoClass" type="text" name="NuevoEstadoEmpleado" id="NuevoEstadoEmpleado" readonly='true' placeholder="Administradora" required="true"> 
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha Ingreso</label>
                                    <input class="form-control empleadoClass" readonly='true' type="text" name="NuevoFechaIngreso" id="NuevoFechaIngreso"  placeholder="Fecha Ingreso" required="true">
                                </div> 
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha Retiro</label>
                                    <input class="form-control empleadoClass" readonly='true' type="text" name="NuevoFechaRetiro" id="NuevoFechaRetiro"  placeholder="Fecha Retiro" required="true">
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Novedad</label>
                                    <input class="form-control empleadoClass" readonly='true' type="text" name="NuevoNovedad" id="NuevoNovedad"  placeholder="Novedad" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha de Registro</label>
                                    <input class="form-control" readonly='true' type="text" name="nuevoFechaRegistro" id="nuevoFechaRegistro"  placeholder="Fecha de Registro" required="true" value="<?php echo date('d/m/Y');?>">
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Usuario</label>
                                    <input class="form-control" readonly='true' type="text" name="NuevoUsuario" id="NuevoUsuario" value="<?php echo $_SESSION['nombres'];?>"  placeholder="Usuario" required="true">
                                </div> 
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Ingresar IBC Colaborador</label>
                                    <input class="form-control empleadoClass" type="text" name="NuevoIvlempleado" id="NuevoIvlempleado"  placeholder="IBC Colaborador" required="true">
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha Recepción Incapacidad</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaRecepcion" id="NuevoFechaRecepcion"  placeholder="Fecha Recepción Incapacidad" required="true">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Origen</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                          <button type="button" title="Ver Incapacidades Anteriores" id="verIncapacidadesAnteriores" class="btn btn-outline-danger waves-effect waves-light"><i class='fa fa-th'></i></button>
                                        </span>
                                        <select class="form-control  " required="true" name="NuevoOrigen" id="NuevoOrigen" required="true">
                                            <?php 
                                                $camppos ="ori_id_i, ori_descripcion_v";
                                                $tabla__ = "gi_origen";
                                                $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                                foreach ($atencion as $key => $value) {
                                                    echo '<option value="'.$value['ori_id_i'].'">'.$value['ori_descripcion_v'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Clasificación</label>
                                    <select class="form-control" required="true"  id="NuevoClasificacion" name="NuevoClasificacion" required="true">
                                        <?php 
                                            $camppos ="cla_id, cla_descripcion";
                                            $tabla__ = "gi_clasificacion";
                                            $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                            foreach ($atencion as $key => $value) {
                                                echo '<option value="'.$value['cla_id'].'">'.$value['cla_descripcion'].'</option>';
                                            }
                                        ?>
                                    </select>
                                    <input class="form-control" type="text" name="NuevoClasificacion_2" id="NuevoClasificacion_2" value="Inicial" readonly>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Atención</label>
                                    <select class="form-control  " required="true"  name="NuevoAtencion" required="true">
                                        <option value="">Seleccionar Atención</option>
                                        <?php
                                            $camppos ="atn_id, atn_descripcion";
                                            $tabla__ = "gi_atencion";
                                            $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                            foreach ($atencion as $key => $value) {
                                                echo '<option value="'.$value['atn_id'].'">'.$value['atn_descripcion'].'</option>';
                                            }
                                        ?>                                          
                                    </select> 
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Documentos</label>
                                        <select class="form-control  " name="NuevoTipoGeneracion" placeholder="Tipo de generación" required="true">
                                            <?php
                                                $camppos ="doc_id_i, doc_descripcion_v";
                                                $tabla__ = "gi_documentos";
                                                $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                                foreach ($atencion as $key => $value) {
                                                    echo '<option value="'.$value['doc_id_i'].'">'.$value['doc_descripcion_v'].'</option>';
                                                }
                                            ?>   
                                        </select> 
                                    </div>
                                </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha inicio</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaInicio" id="NuevoFechaInicio" placeholder="Fecha inicio" required="true">   
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha final</label>
                                    <input class="form-control flatpickr-input " type="text" name="NuevoFechaFinal" id="NuevoFechaFinal" placeholder="Fecha final" required="true"> 
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Días</label>
                                    <input class="form-control  " type="text" readonly="true" name="NumeroDias" id="NumeroDias" placeholder="Días" >
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Estado Incapacidad</label>
                                    <select class="form-control"  name="NuevoEstadoIncapacidad" id="NuevoEstadoIncapacidad">
                                        <option value="1">ACEPTADA</option>
                                        <option value="0">INCOMPLETA</option>
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                        <div id="NuevoMotivosRechazo" style="display:none;">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class='mb-3'>
                                        <label class="form-label">Motivo Rechazo</label>
                                        <select class='form-control' name='NuevoMotivo' id='NuevoMotivo'>
                                            <option value="0">Seleccione</option>
                                            <?php 
                                                $campo = "*";
                                                $tabla = "gi_motivos_rechazo";

                                                $datos = ModeloIncapacidades::mdlMostrarGroupAndOrder($campo,$tabla,null, null, " ORDER BY mot_desc_v ASC");
                                                $option = "<option value='0'>Selecione</option>";
                                                foreach ($datos as $key => $value) {
                                                    echo "<option value='".$value['mot_id_i']."'>".$value['mot_desc_v']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class='mb-3'>
                                        <label class="form-label">Observaciones Incapacidad Incompleta</label>
                                        <input class="form-control" id="NuevoObservacion_segunda" name="NuevoObservacion_segunda" placeholder="Observaciones Incapacidad Incompleta" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="rowIncapacidades" style="display: none;">
                            <div class="col-lg-12 col-xs-12 table-responsive">
                                <div style="height: 200px; overflow-y: auto;">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <th>#</th>
                                            <th>Fecha Inicial</th>
                                            <th>Fecha Final</th>
                                            <th>Dias</th>
                                            <th>Origen</th>
                                            <th>Clase</th>
                                            <th>Desc. Diagnóstico</th>
                                        </thead>
                                        <tbody id="tableIncapAnterior">
                                            
                                        </tbody>                                    
                                    </table>
                                </div>
                            </div>  
                        </div>

                        

                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha de Pago Nómina</label>
                                    <input class="form-control flatpickr-input " type="text" name="NuevotxtFechaPagoNomina"  required id="NuevotxtFechaPagoNomina" placeholder="Fecha de Pago Nómina" >
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Valor Empresa</label>  
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" title="Liquidar Incapacidad" id="LiquidarIncapa" class="btn btn-outline-danger waves-effect waves-light"><i class='fa fa-money-bill-wave'></i></button>
                                        </span> 
                                        <input class="form-control  Valores" type="text" name="NuevoValorPagadoEmpresa" id="NuevoValorPagadoEmpresa" placeholder="Valor Empresa" >
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Valor Administradora</label>
                                    <input class="form-control  Valores" type="text" name="NuevoValorPagoEPS" id="NuevoValorPagoEPS" >    
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Valor Ajuste</label>
                                    <input class="form-control  Valores" type="text" name="NuevoValorPagoAjuste" id="NuevoValorPagoNomina" placeholder="Valor Ajuste" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Diagnóstico</label>
                                    <select class="form-control " style="width: 100%;" required="true" name="NuevoDiagnostico" id="NuevoDiagnostico">
                                        <option value="0">Selecionar Diagnóstico</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Profesional Responsable <a class="text-danger" data-bs-toggle="modal" data-bs-target="#modalAgregarMedico" href="#"><i class='fa fa-plus-circle'></i></a></label>          
                                    <select class="form-control" name="NuevoProfesional" id="NuevoProfesional" required>
                                        <option value="0">Selecionar Médico</option>
                                    </select>  
                                    
                                    <!--<input class="form-control" type="text" name="NuevoProfesional" placeholder="Profesional responsable" >-->  
                                </div>
                            </div>

                            <div class="col-lg-2 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Registro Médico</label>
                                    <input class="form-control " type="text" name="NuevoRegistroMedico" id="NuevoRegistroMedico" placeholder="Registro Medico" readonly>   
                                </div>
                            </div>

                            <div class="col-lg-2 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Estado Rethus</label>
                                    <input class="form-control validRethus" type="text" name="NuevoEstadoRethus" id="NuevoEstadoRethus" placeholder="Rethus" readonly>   
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Institución que genera Responsable <a class="text-danger" data-bs-toggle="modal" data-bs-target="#modalAgregarEntidades" href="#"><i class='fa fa-plus-circle'></i></a></label>          
                                    <select class="form-control" name="NuevoInstitucionGenera" id="NuevoInstitucionGenera" required>
                                        <option value="0">Selecionar Institución</option>
                                    </select>  
                                </div>
                            </div>
                            <div class="col-lg-2 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">NIT - Entidad</label>
                                    <input class="form-control " type="text" name="NuevoNitEntidad" id="NuevoNitEntidad" placeholder="NIT" readonly>   
                                </div>
                            </div>
                            <div class="col-lg-2 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">¿Pertenece a la Red?</label>
                                    <input class="form-control validEntidad" type="text" name="NuevoPertRed" id="NuevoPertRed" placeholder="¿Pertenece a la Red?" readonly>   
                                </div>
                            </div>
                        </div>

                        <div id="NuevoOcultarRechazado"> 
                            <div class="row">
                                <div class="col-lg-2 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Registrado en Entidad</label>
                                        <select class="form-control" name="NuevoRegistradoEntidad" id ="NuevoRegistradoEntidad" >
                                            <option value="0">No</option>  
                                            <option value="1">Si</option>     
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Estado Tramite</label>
                                        <select class="form-control  "  name="NuevoEstadoTramite" id="NuevoEstadoTramite">
                                            <?php
                                                $camppos ="est_tra_id_i, est_tra_desc_i";
                                                $tabla__ = "gi_estado_tramite";
                                                $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                                foreach ($atencion as $key => $value) {
                                                    echo '<option value="'.$value['est_tra_id_i'].'">'.$value['est_tra_desc_i'].'</option>';
                                                }
                                            ?>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12"  id="divSubEstadoTramite_N" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Sub Estado</label>
                                        <select class="form-control"  name="NuevoSubEstadoTramite" id="NuevoSubEstadoTramite">
                                            <?php
                                                $camppos ="set_id_i, set_descripcion_v";
                                                $tabla__ = "gi_sub_estado_tramite";
                                                $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                                foreach ($atencion as $key => $value) {
                                                    echo '<option value="'.$value['set_id_i'].'">'.$value['set_descripcion_v'].'</option>';
                                                }
                                            ?>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12"  id="divNumeroRadicacion_N" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Numero de Radicado</label>
                                        <input class="form-control " type="text" name="NuevoNumeroRadicado" id="NuevoNumeroRadicado" placeholder="Ingresar Numero de Radicado">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12" id="divValorIncapacidad_N" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Valor de la incapacidad</label>
                                        <input class="form-control " type="text" name="NuevoValorIncapacidad" id="NuevoValorIncapacidad" placeholder="Ingresar Valor de la incapacidad">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12" id="divValorSolicitado_N" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Valor Solicitado</label>
                                        <input class="form-control " type="text" name="NuevoValorSolicitud" id="NuevoValorSolicitud" placeholder="Ingresar Valor Solicitado">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12"  id="divFechaPago_N" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Fecha de Pago</label>
                                        <input class="form-control flatpickr-input" type="text" name="NuevoFechaPagoIncapacidad" id="NuevoFechaPagoIncapacidad" placeholder="Ingresar Fecha de Pago">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12"  id="divFechaRadicacion_N" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Fecha de Radicado</label>
                                        <input class="form-control fecha" type="text" name="NuevoFechaRadicacionIncapacidad" id="NuevoFechaRadicacionIncapacidad" placeholder="Ingresar Fecha de Radicado">
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-xs-12" id="divFechaSolicitud_N" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Fecha de Solicitud de Pago</label>
                                        <input class="form-control flatpickr-input" type="text" name="NuevoFechaSolicitudIncapacidad" id="NuevoFechaSolicitudIncapacidad" placeholder="Ingresar Fecha de Solicitud de Pago">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12" id="divFechaNoreconocida_N" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Fecha de Negación</label>
                                        <input class="form-control flatpickr-input" type="text" name="NuevoFechaSinReconocimiento" id="NuevoFechaSinReconocimiento" placeholder="Ingresar Fecha de Negación">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12" id="divEnvioDatoCorreo_N" style="display: none;">
                                    <div class="mb-3">
                                        <div class="checkbox">
                                            <label style="visibility:hidden;">
                                                Notificar al correo de la empresa
                                            </label>
                                            <label class="form-label">
                                                <input type="checkbox" name="nuevoEnvioCorreo_N" id="nuevoEnvioCorreo_N" value="1"> Notificar pago, al correo de la empresa
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divNuevoInformacion_N" style="display: none;">
                                <div class="col-lg-12 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Observación</label>
                                        <textarea class="form-control  " id="NuevoObservacion_2" name="NuevoObservacion" placeholder="Observación"></textarea> 
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divNuevoInformacionSinReconocer_N" style="display: none;">
                                <div class="col-lg-12 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Observación Incapacidad Sin Reconocimiento</label>
                                        <textarea class="form-control  " id="NuevoObservacion_3" name="NuevoObservacion_3" placeholder="Observación"></textarea>    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Observación</label>
                                        <textarea class="form-control  " id="NuevoObservacion" name="NuevoObservacion" placeholder="Observación"></textarea>   
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row panel">
                            <div class="col-md-5 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Adjuntar Incapacidad</label>
                                    <input type="file" class="NuevaFoto" valor='0' name="NuevoFotoIncapacidad">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar pull-right">
                                </div>
                            </div>

                            <div class="col-md-5 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Adjuntar transcripción</label>
                                    <input type="file" class="NuevaFoto" valor='_2' name="NuevoOtroIncapacidad">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2 pull-right">
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <div class="mb-3">
                                    <table class="table table-hover table-bordered">
                                        <tr>
                                            <td><a href="#" style="display: none;" target="_blank" id="cedulaColaborador">Ver Identificación Colaborador</td>
                                        </tr>
                                        <tr>
                                            <td><a href="#" style="display: none;" target="_blank" id="polizaColaborador">Ver Poliza Colaborador</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                    <input type="hidden" name="NuevoNombreEmpresa" id="NuevoEmpresa">
                    <input type="hidden" name="NuevoEmpleado" id="NuevoEmpleado">
                    <input type="hidden" name="NuevoAfpAfiliado" id="NuevoAfpAfiliado">
                    <input type="hidden" name="NuevoArlAfiliado_2" id="NuevoArlAfiliado_2">
                    <input type="hidden" name="NuevoIpsAfiliado_2" id="NuevoIpsAfiliado_2">
                    <input type="hidden" name="NuevoPerteneceRed" id="NuevoPerteneceRed">
                    <button type="button" id="btnGuardarInc" class="btn btn-primary">Guardar Incapacidad</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<?php 
    $moduloInc = true;
    include './vistas/modulos/medicos/modalAddMedico.php'; 
    include './vistas/modulos/registroEntidades/modalAddEntidadIps.php'; 
?> 

<!-- Modal agregar usuario -->
<div id="modalEditarrIncapacidad"  class="modal fade"  data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" id="EditarIncapacidadForm" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Editar Incapacidad</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3" id="divInputCedulaEditar">
                                    <label class="form-label">Ingresar Identificación</label>
                                    <input class="form-control  empleadoClassEdicion " type="text" name="EditarCedulaIncapacidad" id="EditarCedulaIncapacidad" placeholder="Ingresar Identificación" required="true">
                                    <span class='help-block' id='spanHelblokEditar'></span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Nombre Colaborador</label>
                                    <input class="form-control empleadoClassEdicion " type="text" name="EditarNombreIncapacidad" id="EditarNombreIncapacidad" readonly="true" placeholder="Nombre Colaborador" required="true">
                                </div>
                            </div>

                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Cargo del Colaborador</label>
                                    <input class="form-control empleadoClassEdicion " type="text" name="EditarCargoEmpleado" id="EditarCargoEmpleado" readonly="true" placeholder="Cargo del Colaborador" required="true">
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Nombre Empresa</label>
                                    <input class="form-control empleadoClassEdicion " type="text" name="EditarNombreEmpresa" id="EditarNombreEmpresa" readonly='true' placeholder="Nombre Empresa" required="true">
                                </div>
                            </div>
                            
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">EPS</label>
                                    <!--<input class="form-control empleadoClassEdicion" type="text" name="EditarIpsAfiliado" id="EditarIpsAfiliado" readonly='true' placeholder="EPS" required="true">-->
                                    <select class="form-control empleadoClass" type="text" name="EditarIpsAfiliado" id="EditarIpsAfiliado" placeholder="EPS" required="true">
                                        <?php 
                                            $camppos ="ips_id , ips_nombre";
                                            $tabla__ = "gi_ips";
                                            $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,"ips_tipo = 'EPS'", null, 'ORDER BY ips_nombre ASC ');
                                            foreach ($atencion as $key => $value) {
                                                echo '<option value="'.$value['ips_id'].'">'.$value['ips_nombre'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div> 
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha Afiliación EPS</label>
                                    <input class="form-control empleadoClassEdicion" readonly='true' type="text" name="EditarFechaAfiliacionEps"                         id="EditarFechaAfiliacionEps"  placeholder="Fecha Afiliación EPS" required="true">
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">ARL</label>
                                    <!--<input class="form-control empleadoClassEdicion" type="text" name="EditarARLEmpleado" id="EditarARLEmpleado" readonly='true' placeholder="ARL" required="true">--> 
                                    <select class="form-control empleadoClass" type="text" name="EditarArlAfiliado" id="EditarArlAfiliado" placeholder="ARL" required="true">
                                        <?php 
                                            $camppos ="ips_id , ips_nombre";
                                            $tabla__ = "gi_ips";
                                            $arl = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,"ips_tipo = 'ARL'", null, 'ORDER BY ips_nombre ASC ');
                                            foreach ($arl as $key => $value) {
                                                echo '<option value="'.$value['ips_id'].'">'.$value['ips_nombre'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Estado Colaborador</label>
                                    <input class="form-control empleadoClassEdicion" type="text" name="EditarEstadoEmpleado" id="EditarEstadoEmpleado" readonly='true' placeholder="Administradora" required="true">
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha Ingreso</label>
                                    <input class="form-control empleadoClassEdicion" readonly='true' type="text" name="EditarFechaIngreso" id="EditarFechaIngreso"  placeholder="Fecha Ingreso" required="true">
                                </div> 
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha Retiro</label>
                                    <input class="form-control empleadoClassEdicion" readonly='true' type="text" name="EditarFechaRetiro" id="EditarFechaRetiro"  placeholder="Fecha Retiro" required="true">
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Novedad</label>
                                    <input class="form-control empleadoClassEdicion" readonly='true' type="text" name="EditarNovedad" id="EditarNovedad"  placeholder="Novedad" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha de Registro</label>
                                    <input class="form-control" readonly='true' type="text" name="EditarFechaRegistro" id="EditarFechaRegistro"  placeholder="Fecha de Registro" required="true">
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Usuario</label>
                                    <input class="form-control" readonly='true' type="text" name="EditarUsuario" id="EditarUsuario" value=""  placeholder="Usuario" required="true">
                                </div> 
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Ingresar IBC Colaborador</label>
                                    <input class="form-control empleadoClassEdicion" type="text" name="EditarIvlempleado" id="EditarIvlempleado"  placeholder="IBC Colaborador" required="true">
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha Recepción Incapacidad</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaRecepcion" id="EditarFechaRecepcion"  placeholder="Fecha Recepción Incapacidad" required="true">
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Origen</label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" title="Liquidar Incapacidad" id="verIncapacidadesAnterioresE" class="btn btn-outline-danger waves-effect waves-light"><i class='fa fa-money-bill-wave'></i></button>
                                        </span> 
                                        <select class="form-control  " required="true" name="EditarOrigen" id="EditarOrigen" required="true">
                                            <?php 
                                                $camppos ="ori_id_i, ori_descripcion_v";
                                                $tabla__ = "gi_origen";
                                                $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                                foreach ($atencion as $key => $value) {
                                                    echo '<option value="'.$value['ori_id_i'].'">'.$value['ori_descripcion_v'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Clasificación</label>
                                    <select class="form-control  " required="true"  id="EditarClasificacion" name="EditarClasificacion" required="true">
                                        <?php 
                                            $camppos ="cla_id, cla_descripcion";
                                            $tabla__ = "gi_clasificacion";
                                            $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                            foreach ($atencion as $key => $value) {
                                                echo '<option value="'.$value['cla_id'].'">'.$value['cla_descripcion'].'</option>';
                                            }
                                        ?>
                                    </select>   
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Atención</label>
                                    <select class="form-control  " required="true"  name="EditarAtencion" id="EditarAtencion" required="true">
                                        <option value="">Seleccionar Atención</option>
                                        <?php
                                            $camppos ="atn_id, atn_descripcion";
                                            $tabla__ = "gi_atencion";
                                            $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                            foreach ($atencion as $key => $value) {
                                                echo '<option value="'.$value['atn_id'].'">'.$value['atn_descripcion'].'</option>';
                                            }
                                        ?>                                          
                                    </select> 
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Documentos</label>
                                        <select class="form-control  " name="EditarTipoGeneracion" id="EditarTipoGeneracion" placeholder="Tipo de generación" required="true">
                                            <?php
                                                $camppos ="doc_id_i, doc_descripcion_v";
                                                $tabla__ = "gi_documentos";
                                                $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                                foreach ($atencion as $key => $value) {
                                                    echo '<option value="'.$value['doc_id_i'].'">'.$value['doc_descripcion_v'].'</option>';
                                                }
                                            ?>   
                                        </select> 
                                    </div>
                                </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha inicio</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaInicio" id="EditarFechaInicio" placeholder="Fecha inicio" required="true">   
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha final</label>
                                    <input class="form-control  flatpickr-input" type="text" name="EditarFechaFinal" id="EditarFechaFinal" placeholder="Fecha final" required="true"> 
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Días</label>
                                    <input class="form-control  " type="text" readonly="true" name="EditarNumeroDias" id="EditarNumeroDias" placeholder="Días" >
                                    <!--<span class="input-group-btn">
                                      <button type="button" title="Ver Incapacidades Anteriores" id="verIncapacidadesAnteriores" class="btn btn-info btn-flat"><i class='fa fa-th'></i></button>
                                    </span>-->
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Estado Incapacidad</label>
                                    <select class="form-control"  name="EditarEstadoIncapacidad" id="EditarEstadoIncapacidad">
                                        <option value="1">ACEPTADA</option>
                                        <option value="0">INCOMPLETA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="EditarMotivosRechazo" style="display:none;">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class='mb-3'>
                                        <label class="form-label">Motivo Rechazo</label>
                                        <select class='form-control' name='EditarMotivo' id='EditarMotivo'>
                                            <option value="0">Seleccione</option>
                                            <?php 
                                                $campo = "*";
                                                $tabla = "gi_motivos_rechazo";

                                                $datos = ModeloIncapacidades::mdlMostrarGroupAndOrder($campo,$tabla,null, null, " ORDER BY mot_desc_v ASC");
                                                $option = "<option value='0'>Selecione</option>";
                                                foreach ($datos as $key => $value) {
                                                    echo "<option value='".$value['mot_id_i']."'>".$value['mot_desc_v']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class='mb-3'>
                                        <label class="form-label">Observaciones Incapacidad Incompleta</label>
                                        <input class="form-control" id="EditarObservacion_segunda" name="EditarObservacion_segunda" placeholder="Observaciones Incapacidad Incompleta" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover" id="tablaComentarios_r">
                                        
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="rowIncapacidadesE" style="display: none;">
                            <div class="col-lg-12 col-xs-12 table-responsive">
                                <div style="height: 200px; overflow-y: auto;">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <th>#</th>
                                            <th>Fecha Inicial</th>
                                            <th>Fecha Final</th>
                                            <th>Dias</th>
                                            <th>Origen</th>
                                            <th>DX</th>
                                            <th>Desc. Diagnóstico</th>
                                        </thead>
                                        <tbody id="tableIncapAnteriorE">
                                            
                                        </tbody>                                    
                                    </table>
                                </div>
                            </div>  
                        </div>

                        

                        <div class="row">
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Fecha de Pago Nómina</label>
                                    <input class="form-control flatpickr-input " type="text" name="EditartxtFechaPagoNomina"  required id="EditartxtFechaPagoNomina" placeholder="Fecha de Pago Nómina" >
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Valor Empresa</label> 
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" title="Liquidar Incapacidad" id="LiquidarIncapaE" class="btn btn-outline-danger waves-effect waves-light"><i class='fa fa-money-bill-wave'></i></button>
                                        </span>  
                                        <input class="form-control  Valores" type="text" name="EditarValorPagadoEmpresa" id="EditarValorPagadoEmpresa" placeholder="Valor Empresa" >
                                    </div>
                                        <!--<span class="input-group-btn">
                                            <button type="button" title="Liquidar Incapacidad" id="LiquidarIncapa" class="btn btn-info btn-flat"><i class='fa fa-usd'></i></button>
                                        </span>-->
                                </div>
                            </div>
                            
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Valor Administradora</label>
                                    <input class="form-control  Valores" type="text" name="EditarValorPagoEPS" id="EditarValorPagoEPS" >    
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Valor Ajuste</label>
                                    <input class="form-control  Valores" type="text" name="EditarValorPagoAjuste" id="EditarValorPagoNomina" placeholder="Valor Ajuste" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Diagnóstico</label>
                                    <select class="form-control " style="width: 100%;" required="true" name="EditarDiagnostico" id="EditarDiagnostico">
                                        <option value="0">Selecionar Diagnóstico</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Profesional Responsable</label>
                                    <select class="form-control" name="EditarProfesional" id="EditarProfesional" required>
                                        <option value="0">Seleccionar Médico</option>
                                    </select>
                                    <!--<input class="form-control  " type="text" name="EditarProfesional" id="EditarProfesional" placeholder="Profesional responsable" >-->
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Registro Médico</label>
                                    <input class="form-control  " type="text" name="EditarRegistroMedico" id="EditarRegistroMedico" placeholder="Registro Medico" readonly>   
                                </div>
                            </div>
                            <div class="col-lg-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Estado Rethus</label>
                                    <input class="form-control validRethus" type="text" name="EditarEstadoRethus" id="EditarEstadoRethus" placeholder="Estado Rethus" readonly>   
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Institución que genera</label> 
                                    <select class="form-control" name="EditarInstitucionGenera" id="EditarInstitucionGenera" required>
                                        <option value="0">Seleccionar Entidad</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-2 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">NIT - Entidad</label>
                                    <input class="form-control " type="text" name="EditarNitEntidad" id="EditarNitEntidad" placeholder="NIT" readonly>   
                                </div>
                            </div>
                            <div class="col-lg-2 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">¿Pertenece a la Red?</label>
                                    <input class="form-control validEntidad" type="text" name="EditarPertRed" id="EditarPertRed" placeholder="¿Pertenece a la Red?" readonly>   
                                </div>
                            </div>
                        </div>

                        <div id="EditarOcultarRechazado"> 
                            <div class="row">
                                <div class="col-lg-3 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Registrado en Entidad</label>
                                            <select class="form-control" name="EditarRegistradoEntidad" id ="EditarRegistradoEntidad" >
                                                <option value="NO">NO</option>  
                                                <option value="SI">SI</option>     
                                            </select> 
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Estado Tramite</label>
                                        <select class="form-control  "  name="EditarEstadoTramite" id="EditarEstadoTramite">
                                            <?php 
                                                $camppos ="est_tra_id_i, est_tra_desc_i";
                                                $tabla__ = "gi_estado_tramite";
                                                $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                                foreach ($atencion as $key => $value) {
                                                    echo '<option value="'.$value['est_tra_id_i'].'">'.$value['est_tra_desc_i'].'</option>';
                                                }
                                            ?>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12" id="divSubEstadoTramite_E" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Sub Estado</label>
                                        <select class="form-control"  name="EditarSubEstadoTramite" id="EditarSubEstadoTramite">
                                            <?php
                                                $camppos ="set_id_i, set_descripcion_v";
                                                $tabla__ = "gi_sub_estado_tramite";
                                                $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                                foreach ($atencion as $key => $value) {
                                                    echo '<option value="'.$value['set_id_i'].'">'.$value['set_descripcion_v'].'</option>';
                                                }
                                            ?>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12"  id="divNumeroRadicacion" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Numero de Radicado</label>
                                        <input class="form-control " type="text" name="EditarNumeroRadicado" id="EditarNumeroRadicado" placeholder="Ingresar Numero de Radicado">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12" id="divValorIncapacidad" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Valor de la incapacidad</label>
                                        <input class="form-control " type="text" name="EditarValorIncapacidad" id="EditarValorIncapacidad" placeholder="Ingresar Valor de la incapacidad">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12" id="divValorSolicitado" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Valor Solicitado</label>
                                        <input class="form-control " type="text" name="EditarValorSolicitud" id="EditarValorSolicitud" placeholder="Ingresar Valor Solicitado">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12"  id="divFechaPago" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Fecha de Pago</label>
                                        <input class="form-control flatpickr-input" type="text" name="EditarFechaPagoIncapacidad" id="EditarFechaPagoIncapacidad" placeholder="Ingresar Fecha de Pago">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12"  id="divFechaRadicacion" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Fecha de Radicado</label>
                                        <input class="form-control flatpickr-input" type="text" name="EditarFechaRadicacionIncapacidad" id="EditarFechaRadicacionIncapacidad" placeholder="Ingresar Fecha de Radicado">
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-xs-12" id="divFechaSolicitud" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Fecha de Solicitud de Pago</label>
                                        <input class="form-control flatpickr-input" type="text" name="EditarFechaSolicitudIncapacidad" id="EditarFechaSolicitudIncapacidad" placeholder="Ingresar Fecha de Solicitud de Pago">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xs-12" id="divFechaNoreconocida" style="display: none;">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Fecha de Negación</label>
                                        <input class="form-control flatpickr-input" type="text" name="EditarFechaSinReconocimiento" id="EditarFechaSinReconocimiento" placeholder="Ingresar Fecha de Negación">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-xs-12" id="divEnvioDatoCorreo" style="display: none;">
                                    <div class="mb-3">
                                        <div class="checkbox">
                                            <label style="visibility:hidden;">
                                                Notificar al correo de la empresa
                                            </label>
                                            <label class="form-label">
                                                <input type="checkbox" name="EditarEnvioCorreo" id="EditarEnvioCorreo" value="1"> Notificar pago, al correo de la empresa
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divEditarInformacion" style="display: none;">
                                <div class="col-lg-12 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Observación</label>
                                        <textarea class="form-control  " id="EditarObservacion_2" name="EditarObservacion" placeholder="Observación"></textarea> 
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divEditarInformacionSinReconocer" style="display: none;">
                                <div class="col-lg-12 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Observación Incapacidad Sin Reconocimiento</label>
                                        <textarea class="form-control  " id="EditarObservacion_3" name="EditarObservacion_3" placeholder="Observación"></textarea>    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-xs-12">
                                    <div class="mb-3">
                                        <label class="form-label">Observación</label>
                                        <textarea class="form-control  " id="EditarObservacion" name="EditarObservacion" placeholder="Observación"></textarea>  
                                        <input type="hidden" name="idObservacionEditar" id="idObservacionEditar" value="0"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-xs-12">
                                <table class="table table-bordered table-hover" id="tablaComentarios">
                                    
                                </table>
                            </div>
                        </div>
                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Adjuntar Incapacidad</label>
                                    <input type="file" class="NuevaFoto" valor='0' name="EditarFotoIncapacidad">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar pull-right">
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Adjuntar transcripción</label>
                                    <input type="file" class="NuevaFoto" valor='_2' name="EditarOtroIncapacidad">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2 pull-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                    <input type="hidden" name="id_incapacidad" id="id_incapacidad">
                    <input type="hidden" name="EditarNombreEmpresa" id="EditarEmpresa">
                    <input type="hidden" name="EditarIpsEmpleado" id="EditarIpsEmpleado">
                    <input type="hidden" name="EditarAfpAfiliado" id="EditarAfpAfiliado">
                    <!--<input type="hidden" name="EditarArlAfiliado" id="EditarArlAfiliado">-->
                    <input type="hidden" name="EditarEmpleado" id="EditarEmpleado">
                    <input type="hidden" name="ruta_incapacidad" id="ruta_incapacidad">
                    <input type="hidden" name="ruta_otroincapacidad" id="ruta_otroincapacidad">
                    <input type="hidden" name="EditarPerteneceRed" id="EditarPerteneceRed">

                    <?php if($_SESSION['edita'] == 1) { ?>
                    <button type="button" id="btnEditarIncapacidad" class="btn btn-primary">Guardar Incapacidad</button>
                    <?php } ?>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->


<!-- Modal agregar usuario -->
<div id="modalAgregarIncapacidadesMasivamente" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" id="foermularioMaxivo" enctype="multipart/form-data">
                <div class="modal-header">

                    <h5 class="modal-title" id="staticBackdropLabel">Cargar Incapacidades Masivamente</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                   <div class="row panel">
                       <div class="col-md-12 col-xs-12">
                            <div class="mb-3">
                                <label class="form-label">Formato de carga</label>
                                <input type="file" class="NuevoExell" required valor='0' name="NuevoIncapacidad"  id="NuevoIncapacidad">
                                <p class="help-block">
                                    Peso maximo del archivo 3 MB
                                </p>
                                <p>
                                    <div class="mb-3">
                                        <label class="form-label">
                                            <input type="checkbox" name="chekUpdateDatos" id="chekUpdateDatos" value="-1"> Editar Incapacidades
                                            <p><small class="text-warning">Nota: solo usar ésta opción para editar incapacidades masivamente.</small></p>
                                        </label>
                                    </div>
                                </p>
                                <p>
                                    Descarga el Formato de carga para incapacidades <a href="vistas/formatos/FC_002.xlsx" download="FC_002.xlsx">aqui</a>
                                </p>
                                <input type="hidden" name="empresa" value="<?php echo $_SESSION['cliente_id'];?>">
                            </div>
                        </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" id="cargarDatos" class="btn btn-primary">Guardar Incapacidades</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Modal agregar usuario -->
<div id="modalAgregarComentariosMasivamente" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" id="foermularioMaxivoComents" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar Comentarios Masivamente</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                   <div class="row panel">
                       <div class="col-md-12 col-xs-12">
                            <div class="mb-3">
                                <label class="form-label">Formato de carga</label>
                                <input type="file" class="NuevoExell" required valor='0' name="NuevoIncapacidadComent"  id="NuevoIncapacidadComent">
                                <p class="help-block">
                                    Peso maximo del archivo 3 MB
                                </p>
                                <p>
                                    Descarga el Formato de carga para comentarios masivos <a href="vistas/formatos/FC_005.xls" download="FC_005.xls">aqui</a>
                                </p>
                            </div>
                        </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" id="cargarDatosComents" class="btn btn-primary">Guardar Comentarios</button>
                </div>
               
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Modal agregar usuario -->
<div id="modalDocumentosIncapacidad" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Documentos de la Incapacidad</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                   <div class="row" id="documentosEmpleados">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th style="width:10px;">#</th>
                                        <th style="width: 20%; text-align:center;">Tipo</th>
                                        <th style="text-align:center;">Nombre Archivo</th>
                                    </tr>
                                </thead>
                                <tbody id="CuerpoDocumentos">
                                    
                                </tbody>
                            </table>
                        </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Modal agregar usuario -->
<div id="modalAgregarDocumentosIncapacidadesMasivamente" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" id="foermularioMaxivoDocu" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar Documentos a Incapacidades Masivamente</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                   <div class="row panel">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <label style="text-align: center;">Identificación</label>
                                </div>
                                <div class="col-md-2">
                                    <label style="text-align: center;">Fecha inicio</label>
                                </div>
                                <div class="col-md-3">
                                    <label style="text-align: center;">Incapacidad imagen</label>
                                </div>
                                <div class="col-md-3">
                                    <label style="text-align: center;">Incapacidad transcrita</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12" id="filas">
                             
                                 
                        </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" id="AddcargaInformacion" class="btn btn-danger">Agregar fila</button>
 
                    <button type="button" id="cargarInformacionDocumentosMax" class="btn btn-danger">Guardar Incapacidades</button>
                </div>
                <?php
                    /*$crearUsuario = new ControladorIncapacidades();
                    $crearUsuario->ctrCargaMaxivaIncapacidades();*/
                ?>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Modal agregar usuario -->
<div id="modalEditarrIncapacidadver" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">DETALLE DE LA INCAPACIDAD</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" id="cuerpoDetalleIncapacidad"> 

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_incapacidad" id="idExportar">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                    <a hre="" id="exportarInca" class="btn btn-primary" target="_blank">Exportar Incapacidad</a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<div id="modalExportarFechasIncapacidadesTotal" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Exportar Incapacidades</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="mb-3">
                            <label class="form-label">Forma de descarga</label>
                            <select class="form-control" id="totalChechedCompleta">
                                <option value="1">Descargar Total Incapacidades</option>
                                <option value="2">Descargar Incapacidades por Fechas</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="mb-3">
                            <label class="form-label">Seleccione estado a descargar</label>
                            <select class="form-control"id="selestadotramite">
                            <option value="0">TODOS</option>
                            <?php
                                $camppos ="est_tra_id_i, est_tra_desc_i";
                                $tabla__ = "gi_estado_tramite";
                                $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                foreach ($atencion as $key => $value) {
                                    echo '<option value="'.$value['est_tra_id_i'].'">'.$value['est_tra_desc_i'].'</option>';
                                }
                            ?>
                            </select>
                        </div>
                    </div><!--new-->
                </div>

                <div class="row" id="fecha1X" style="display:none;">
                    <div class="col-xs-12">
                        <div class="mb-3">
                            <label class="form-label">Fecha Inicial</label>
                            <input type="text" id="FechaInicioTotal" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row"  id="fecha2X" style="display:none;">
                    <div class="col-xs-12">
                        <div class="mb-3">
                            <label class="form-label">Fecha Final</label>
                            <input type="text" id="FechaInicioFinal" class="form-control">
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                <button id="incapacidadesExportacion" type="button" class="btn btn-danger">Exportar</button>
            </div>
        </div>
    </div>
</div>


<div id="modalExportarFechasIncapacidadesIncompletas" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Exportar Incapacidades</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="mb-3">
                            <label class="form-label">Forma de descarga</label>
                            <select class="form-control" id="totalChechedIncompleta">
                                <option value="1">Descargar Total Incapacidades</option>
                                <option value="2">Descargar Incapacidades por Fechas</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row" id="fecha3X" style="display:none;">
                    <div class="col-xs-12">
                        <div class="mb-3">
                            <label class="form-label">Fecha Inicial</label>
                            <input type="text" id="FechaInicioIncompleta" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row"  id="fecha4X" style="display:none;">
                    <div class="col-xs-12">
                        <div class="mb-3">
                            <label class="form-label">Fecha Final</label>
                            <input type="text" id="FechaInicioFinalIncompleta" class="form-control">
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                <button id="incapacidadesExportacionIncompletas" type="button" class="btn btn-danger">Exportar</button>
            </div>
        </div>
    </div>
</div>

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>
<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/incapacidades.js?v=<?php echo rand();?>"></script>
<script type="text/javascript">
     $(function(){
        $("#ExportarIncapacidadesLink").on('click', function(){
            $.ajax({
                url    : "https://incapacidades.app/services/exports/exportar/<?php echo $_SESSION['cliente_id']; ?>",
                headers: {
                    'Authorization':'<?php echo $_SESSION['apiKey'];?>'},
                type: 'GET',
                dataType : 'json',
                success : function(data){
                    var currentdate = new Date();
                    var fecha_hora = currentdate.getFullYear() + rellenarIncapacidades((currentdate.getMonth()+1), 2) +  rellenarIncapacidades(currentdate.getDate(), 2) + "_" + rellenarIncapacidades(currentdate.getHours(), 2) +  rellenarIncapacidades(currentdate.getMinutes(), 2) + rellenarIncapacidades(currentdate.getSeconds(), 2);
                    var $a = $("<a>");
                    $a.attr("href",data.file);
                    $("body").append($a);
                    $a.attr("download","consolidado_incapacidades"+fecha_hora+".xlsx");
                    $a[0].click();
                    $a.remove();
                },
                beforeSend:function(){
                    $.blockUI({
                        message : '<h3>Un momento por favor....</h3>', 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            });
        });
        
        $("#ExportarComentariosLink").on('click', function(){
            $.ajax({
                url    : "https://incapacidades.app/services/exports/incapacidades/comentarios/<?php echo $_SESSION['cliente_id']; ?>",
                headers: {
                    'Authorization':'<?php echo $_SESSION['apiKey'];?>'},
                type: 'GET',
                dataType : 'json',
                success : function(data){
                    var currentdate = new Date();
                    var fecha_hora = currentdate.getFullYear() + rellenarIncapacidades((currentdate.getMonth()+1), 2) +  rellenarIncapacidades(currentdate.getDate(), 2) + "_" + rellenarIncapacidades(currentdate.getHours(), 2) +  rellenarIncapacidades(currentdate.getMinutes(), 2) + rellenarIncapacidades(currentdate.getSeconds(), 2);
                    var $a = $("<a>");
                    $a.attr("href",data.file);
                    $("body").append($a);
                    $a.attr("download","comentarios_"+fecha_hora+".xlsx");
                    $a[0].click();
                    $a.remove();
                },
                beforeSend:function(){
                    $.blockUI({
                        message : '<h3>Un momento por favor....</h3>', 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            });
        });
        
        function rellenarIncapacidades (str, max) {
            str = str.toString();
            return str.length < max ? rellenarIncapacidades("0" + str, max) : str;
        }
    });
    
</script>
