<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Administrar Incapacidades  
            <small>Estado de las incapacidades por nominas</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><i class="fa fa-th"></i>&nbsp;Incapacidades</li>
            <li class="active">Estado Incapacidades por nomina</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">

                <h3 class="box-title">
                    
                </h3>
                <div class="box-tools pull-right">
                    <?php if($_SESSION['adiciona'] == 1){ ?>
                    <button class="btn btn-primary btn-sm" title="Agregar Incapacidad" data-toggle="modal" data-target="#modalAgregarEstadoIncapacidadesMasivamente">
                        <i class="fa fa-plus"></i>
                    </button>
                    <?php } ?>
                    <a class="btn btn-info btn-sm" data-target="#exportarNominaModal" data-toggle="modal" title="Exportar Registros a excel" href="#">
                        <i class="fa fa-file-excel-o"></i>
                    </a>

                    <?php if($_SESSION['edita'] == 1){ ?>
                    <button class="btn btn-success btn-sm" title="Cargar Documentos Incapacidades Masivamente" data-toggle="modal" data-target="#modalAgregarDocumentosIncapacidadesMasivamente">
                        <i class="fa fa-file-o"></i>
                    </button>
                    <?php } ?>
                    <!--<a class="btn btn-success btn-sm" data-target="#exportarNominaPdfModal" data-toggle="modal"  title="Exportar Registros a pdf" href="#">
                        <i class="fa fa-file-pdf-o"></i>
                    </a>-->
                    
                    
                </div>
            </div>
            <div class="box-body" >
                <input type="hidden" name="session" id="session" value="<?php if(isset($_SESSION['cliente_id'])){ echo $_SESSION['cliente_id']; } else { echo 0; } ?>">
                <input type="hidden" id="editar" value="<?php echo $_SESSION['edita'];?>">
                <input type="hidden" id="elimina" value="<?php echo $_SESSION['elimina'];?>">

                <table id="tbl_IncapacidadesNomina" style="width:100%;" class="table table-striped table-bordered dt-responsive tblGeneralPagos">
                    <thead>
                        <tr>
                            <th style="width: 1%;">#</th>
                            <?php if($_SESSION['cliente_id'] == 0){ ?>
                            <th style="width: 10%;">Empresa</th>
                            <?php }else{ ?>
                             <th style="width: 10%;">Cédula</th>
                            <?php }?>
                            <th style="width: 15%;">Nombre</th>
                            <th style="width: 10%;">Cargo</th>
                            <th style="width: 7%;">F. Inicio</th>
                            <th style="width: 5%;">F. Final</th>
                            <th style="width: 5%;">Días</th>
                            <th style="width: 4%;">Estado</th>
                            <th style="width: 10%;">Motivo</th>
                            <th style="width: 8%;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $where = "";
                            if($_SESSION['cliente_id'] != 0){
                                $where = "incn_emp_id = ".$_SESSION['cliente_id']." AND emd_emp_id = ".$_SESSION['cliente_id']." AND ";
                            }   

                            $campo_ = "incn_id_i, incn_cc_v, incn_fecha_inicio_d, incn_fecha_fin_d, incn_estado_v, incn_nomina_i, incn_anho_i, incn_fecha_creacion_t, incn_emp_id, emd_nombre, emd_cargo, emp_nombre, mot_desc_v, DATE_FORMAT(incn_fecha_inicio_d, '%d/%m/%Y') as fecha_inicio_d, DATE_FORMAT(incn_fecha_fin_d, '%d/%m/%Y') as fecha_final_d ";
                            $tabla_ = " gi_incapacidades_nomina JOIN gi_empleados ON emd_cedula = incn_cc_v JOIN gi_empresa ON emp_id = incn_emp_id LEFT JOIN gi_motivos_rechazo ON incn_razon_v = mot_id_i";
                            $condi_ = $where." 1 = 1";
                            // echo "SELECT ".$campo_." FROM ".$tabla_." WHERE ".$condi_;
                            $resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($campo_,$tabla_,$condi_, null, " ORDER BY incn_fecha_creacion_t DESC");
                            // var_dump($resultado);
                            foreach ($resultado as $key => $value) {
                                echo "<tr>";
                                    echo '<td style="width: 1%;">'.($key+1).'</td>';
                                    if($_SESSION['cliente_id'] == 0){ 
                                        echo '<td style="width: 8%;">'.$value['emp_nombre'].'</td>';
                                    }else{
                                        echo '<td style="width: 8%;"><a style="cursor:pointer;" title="Ver informacion de la Incapacidad" class="btnVerIncapacidad" idIncapacidad="'.$value["incn_id_i"].'" data-toggle="modal" data-target="#modalverIncapacidadesNomina"><i class="fa fa-info-circle"></i> &nbsp;'.$value['incn_cc_v'].'</a></td>';
                                    }
                                    echo '<td style="width: 13%;">'.$value['emd_nombre'].'</td>';
                                    echo '<td style="width: 10%;">'.$value['emd_cargo'].'</td>';
                                    echo '<td style="width: 7%;">'.$value['fecha_inicio_d'].'</td>';
                                    echo '<td style="width: 7%;">'.$value['fecha_final_d'].'</td>';
                                    echo '<td style="width: 4%;">'.ControladorIncapacidades::dias_transcurridos($value['incn_fecha_inicio_d'], $value['incn_fecha_fin_d']).'</td>';
                                    echo '<td style="width: 4%;">'.$value['incn_estado_v'].'</td>';
                                    echo '<td style="width: 10%;">'.$value['mot_desc_v'].'</td>';
                                    echo '<td style="width: 15%;">';

                                      

                                        if($_SESSION['edita'] == 1){
                                            echo '&nbsp;&nbsp;<button title="Editar Incapacidad" class="btn btn-warning btnEditarIncapacidad" idIncapacidad="'.$value["incn_id_i"].'" data-toggle="modal" data-target="#modalEdicionDeIncapacidadesNomina"><i class="fa fa-edit"></i></button>';
                                        }
                                        if($_SESSION['elimina'] == 1){
                                            echo '&nbsp;&nbsp;<button title="Eliminar Incapacidad" class="btn btn-danger btnEliminarIncapacidad" idIncapacidad="'.$value["incn_id_i"].'"><i class="fa fa-times"></i></button>';
                                        }

                                        if($value['incn_estado_v'] != 'ACEPTADA'){
                                           $totalNoti =  ModeloTesoreria::mdlMostrarUnitario('count(*) as total','incapacidad_notificaciones','inn_inc_id_i = '.$value["incn_id_i"]);
                                            echo '&nbsp;&nbsp;<button title="Notificar, Total Envios '.$totalNoti['total'].'" class="btn btn-info btnReportarIncapacidad" idIncapacidad="'.$value["incn_id_i"].'"><i class="fa fa-envelope"></i></button>';    
                                        }

                                    echo '</td>';
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th style="width: 1%;">#</th>
                            <?php if($_SESSION['cliente_id'] == 0){ ?>
                            <th style="width: 10%;">Empresa</th>
                            <?php }else{ ?>
                             <th style="width: 10%;">Cédula</th>
                            <?php }?>
                            <th style="width: 10%;">Nombre</th>
                            <th style="width: 10%;">Cargo</th>
                            <th style="width: 7%;">F. Inicio</th>
                            <th style="width: 5%;">F. Final</th>
                            <th style="width: 5%;"># Días</th>
                            <th style="width: 4%;">Estado</th>
                            <th style="width: 15%;">Motivo</th>
                            <th style="width: 8%; text-align: center;"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
</div>

<!-- Modal agregar estado incapacidades -->
<div id="modalAgregarEstadoIncapacidadesMasivamente" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" id="foermularioMaxivoDocu" enctype="multipart/form-data">
                <div class="modal-header" style="background: #dd4b39;color: white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Incapacidades Masivamente</h4>
                </div>
                <div class="modal-body">
                     <div class="row panel">
                       <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Nomina</label>
                                    <select class="form-control" name="txtNomina" id="txtNomina" placeholder="Nomina">
                                        <?php
                                            $campo = "nom_id_i, nom_desc_v";
                                            $tabla = "gi_nominas";
                                            $condi = "nom_tipo_v = 'Mensual'";
                                            $resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($campo, $tabla, $condi, null, " ORDER BY nom_id_i ASC");
                                            // var_dump($resultado);
                                            foreach ($resultado as $key => $value) {
                                                echo "<option value='".$value['nom_id_i']."'>".$value['nom_desc_v']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Año de Nomina</label>
                                    <input type="text" class="form-control" name="txtAnhoNomina" id="txtAnhoNomina" placeholder="Año de Nomina" value="<?php echo date('Y');?>">
                                </div>
                                <div class="col-md-4">
                                    <?php if($_SESSION['cliente_id'] == 0){ ?>
                                    <div class="form-group">
                                        <label>Empresa</label>
                                        <select class="form-control" id="NuevoIdEmpresa" name="NuevoIdEmpresa" style="width: 100%;">
                                            <option value="0">Seleccione la empresa</option>
                                            <?php 
                                                $clientes = ControladorClientes::ctrMostrarClientes(NULL, NULL);
                                                foreach ($clientes as $key => $value) {
                                                    echo "<option value='".$value['emp_id']."'>".$value['emp_nombre']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                <?php } else { ?>
                                    <input type="hidden" name="NuevoIdEmpresa" id="NuevoIdEmpresa" value="<?php if($_SESSION['cliente_id'] != 0) { echo $_SESSION['cliente_id']; }?>">
                                <?php } ?>
                                </div>
                            </div>
                        </div>    
                   </div>
                    <div class="row panel">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <label style="text-align: center;">Identificación</label>
                                </div>
                                <div class="col-md-2">
                                    <label style="text-align: center;">Fecha inicio</label>
                                </div>
                                <div class="col-md-2">
                                    <label style="text-align: center;">Fecha final</label>
                                </div>
                                <div class="col-md-2">
                                    <label style="text-align: center;">Estado</label>
                                </div>
                                <div class="col-md-4">
                                    <label style="text-align: center;">Motivo</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12" id="filas">
                                  
                        </div>                
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="button" id="AddcargaInformacionNomina" class="btn btn-danger">Agregar fila</button>
 
                    <button type="button" id="cargarInformacionDocumentosMaxNomina" class="btn btn-danger">Guardar Incapacidades</button>
                </div>
                <?php
                    /*$crearUsuario = new ControladorIncapacidades();
                    $crearUsuario->ctrCargaMaxivaIncapacidades();*/
                ?>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Modal exportar excel estado incapacidades -->
<div id="exportarNominaModal" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" action="index.php?exportar=incNominaExc" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background: #dd4b39;color: white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Exp. Incapacidades por Nominas</h4>
                </div>
                <div class="modal-body">
                    <div class="row panel">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Nomina</label>
                                    <select class="form-control" name="txtNomina" id="txtNomina" placeholder="Nomina">
                                        <option value="0">Todas</option>
                                        <?php
                                            $campo = "nom_id_i, nom_desc_v";
                                            $tabla = "gi_nominas";
                                            $condi = "nom_tipo_v = 'Mensual'";
                                            $resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($campo, $tabla, $condi, null, " ORDER BY nom_id_i ASC");
                                            // var_dump($resultado);
                                            foreach ($resultado as $key => $value) {
                                                echo "<option value='".$value['nom_id_i']."'>".$value['nom_desc_v']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label>Año de Nomina</label>
                                    <input type="text" class="form-control" name="txtAnhoNomina" id="txtAnhoNomina" placeholder="Año de Nomina" value="<?php echo date('Y');?>">
                                </div>
                                <div class="col-md-12">
                                    <?php if($_SESSION['cliente_id'] == 0){ ?>
                                    <div class="form-group">
                                        <label>Empresa</label>
                                        <select class="form-control" id="NuevoIdEmpresa" name="NuevoIdEmpresa" style="width: 100%;">
                                            <option value="0">Seleccione la empresa</option>
                                            <?php 
                                                $clientes = ControladorClientes::ctrMostrarClientes(NULL, NULL);
                                                foreach ($clientes as $key => $value) {
                                                    echo "<option value='".$value['emp_id']."'>".$value['emp_nombre']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                <?php } else { ?>
                                    <input type="hidden" name="NuevoIdEmpresa" id="NuevoIdEmpresa" value="<?php if($_SESSION['cliente_id'] != 0) { echo $_SESSION['cliente_id']; }?>">
                                <?php } ?>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                
                    <button type="submit" id="exportarNomina" class="btn btn-danger">Exportar Nomina</button>
                </div>
                <?php
                    /*$crearUsuario = new ControladorIncapacidades();
                    $crearUsuario->ctrCargaMaxivaIncapacidades();*/
                ?>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<div id="exportarNominaPdfModal" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" action="index.php?exportar=incNominaPdf" autocomplete="off" method="post"  enctype="multipart/form-data">
                <div class="modal-header" style="background: #dd4b39;color: white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Exp. Incapacidades por Nominas</h4>
                </div>
                <div class="modal-body">
                    <div class="row panel">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Nomina</label>
                                    <select class="form-control" name="txtNomina" id="txtNomina" placeholder="Nomina">
                                        <option value="0">Todas</option>
                                        <?php
                                            $campo = "nom_id_i, nom_desc_v";
                                            $tabla = "gi_nominas";
                                            $condi = "nom_tipo_v = 'Mensual'";
                                            $resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($campo, $tabla, $condi, null, " ORDER BY nom_id_i ASC");
                                            // var_dump($resultado);
                                            foreach ($resultado as $key => $value) {
                                                echo "<option value='".$value['nom_id_i']."'>".$value['nom_desc_v']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <label>Año de Nomina</label>
                                    <input type="text" class="form-control" name="txtAnhoNomina" id="txtAnhoNomina" placeholder="Año de Nomina" value="<?php echo date('Y');?>">
                                </div>
                                <div class="col-md-12">
                                    <?php if($_SESSION['cliente_id'] == 0){ ?>
                                    <div class="form-group">
                                        <label>Empresa</label>
                                        <select class="form-control" id="NuevoIdEmpresa" name="NuevoIdEmpresa" style="width: 100%;">
                                            <option value="0">Seleccione la empresa</option>
                                            <?php 
                                                $clientes = ControladorClientes::ctrMostrarClientes(NULL, NULL);
                                                foreach ($clientes as $key => $value) {
                                                    echo "<option value='".$value['emp_id']."'>".$value['emp_nombre']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                <?php } else { ?>
                                    <input type="hidden" name="NuevoIdEmpresa" id="NuevoIdEmpresa" value="<?php if($_SESSION['cliente_id'] != 0) { echo $_SESSION['cliente_id']; }?>">
                                <?php } ?>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                
                    <button type="submit" id="exportarNomina" class="btn btn-danger">Exportar Nomina</button>
                </div>
                <?php
                    /*$crearUsuario = new ControladorIncapacidades();
                    $crearUsuario->ctrCargaMaxivaIncapacidades();*/
                ?>
            </form>
        </div>
    </div>
</div>

<div id="modalEdicionDeIncapacidadesNomina" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background: #dd4b39;color: white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edición de Incapacidades por Nomina</h4>
                </div>
                <div class="modal-body">
                    <div class="row panel">
                        <div class="col-md-6">
                         
                            <div class="form-group">
                                <label>Identificación</label>
                                <input type="text" name="Editarcedula" id="Editarcedula" class="form-control" placeholder="Identificación" readonly="true">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nombres</label>
                                <input type="text" name="EditarNombres" id="EditarNombres" class="form-control" placeholder="Nombres" readonly="true">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Administradora</label>
                                <input type="text" name="EditarAdministradora" id="EditarAdministradora" class="form-control" placeholder="Administradora" readonly="true">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fecha Inicio</label>
                                <input type="text" name="EditarFechaInicio" id="EditarFechaInicio" class="form-control" placeholder="Fecha Inicio" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fecha Final</label>
                                <input type="text" name="EditarFechaFinal" id="EditarFechaFinal" class="form-control" placeholder="Fecha Final" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class='form-group'>
                                <label>Estado</label>
                                <select class='form-control' name='EditarEstado' id='EditarEstado'>
                                    <option value='ACEPTADA'>ACEPTADA</option>
                                    <option value='RECHAZADA'>RECHAZADA</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class='form-group'>
                                <label>Motivo Rechazo</label>
                                <select class='form-control' name='EditarMotivo' disabled="true" id='EditarMotivo'>
                                    <option value="0">Seleccione</option>
                                    <?php 
                                        $campo = "*";
                                        $tabla = "gi_motivos_rechazo";

                                        $datos = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,null, null, " ORDER BY mot_desc_v ASC");
                                        $option = "<option value='0'>Selecione</option>";
                                        foreach ($datos as $key => $value) {
                                            echo "<option value='".$value['mot_id_i']."'>".$value['mot_desc_v']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class='form-group'>
                                <label>Observaciones</label>
                                <textarea class="form-control" id="txtObservaciones" name="txtObservaciones" placeholder="Observaciones" disabled></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nomina</label>
                            <select class="form-control" name="EditarNomina" id="EditarNomina" placeholder="Nomina">
                                <?php
                                    $campo = "nom_id_i, nom_desc_v";
                                    $tabla = "gi_nominas";
                                    $condi = "nom_tipo_v = 'Mensual'";
                                    $resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($campo, $tabla, $condi, null, " ORDER BY nom_id_i ASC");
                                    // var_dump($resultado);
                                    foreach ($resultado as $key => $value) {
                                        echo "<option value='".$value['nom_id_i']."'>".$value['nom_desc_v']."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Año de Nomina</label>
                            <input type="text" class="form-control" name="EditarAnhoNomina" id="EditarAnhoNomina" placeholder="Año de Nomina" value="<?php echo date('Y');?>">
                        </div>
                    </div>    
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="idIncapacidadEdicion" id="idIncapacidadEdicion" value="0">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" id="guardarIncapacidad" class="btn btn-danger">Guardar</button>
                </div>
                <?php
                    $crearUsuario = new ControladorNomina();
                    $crearUsuario->ctrEditarNomina();
                ?>
            </form>
        </div>
    </div>
</div>

<!-- Modal agregar usuario -->
<div id="modalAgregarDocumentosIncapacidadesMasivamente" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" id="foermularioMaxivoDocux" enctype="multipart/form-data">
                <div class="modal-header" style="background: #dd4b39;color: white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Documentos a Incapacidades Masivamente</h4>
                </div>
                <div class="modal-body">
                   <div class="row panel">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <label style="text-align: center;">Identificación</label>
                                </div>
                                <div class="col-md-2">
                                    <label style="text-align: center;">Fecha inicio</label>
                                </div>
                                <div class="col-md-3">
                                    <label style="text-align: center;">Incapacidad imagen</label>
                                </div>
                                <div class="col-md-3">
                                    <label style="text-align: center;">Observacion</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12" id="filasx">
                             
                                 
                        </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="button" id="AddcargaInformacionx" class="btn btn-danger">Agregar fila</button>
 
                    <button type="button" id="cargarInformacionDocumentosMaxx" class="btn btn-danger">Guardar Incapacidades</button>
                </div>
                <?php
                    /*$crearUsuario = new ControladorIncapacidades();
                    $crearUsuario->ctrCargaMaxivaIncapacidades();*/
                ?>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->


<!-- Modal agregar usuario -->
<div id="modalverIncapacidadesNomina" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" id="foermularioMaxivoDocux" enctype="multipart/form-data">
                <div class="modal-header" style="background: #dd4b39;color: white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detalle de la incapacidad</h4>
                </div>
                <div class="modal-body">
                   <table class="table table-bordered">
                        <tr>
                           <th width="10%">Identificación</th>
                           <td id="tdIdentificacion"></td>
                           <th width="10%">Empleado</th>
                           <td id="tdEmpleado"></td>
                        </tr>
                        <tr>
                           <th width="10%">Administradora</th>
                           <td id="tdEps"></td>
                           <th width="10%">Estado</th>
                           <td id="tdEstadoEmpleado"></td>
                        </tr>
                        <tr>
                           <th width="10%">Cargo</th>
                           <td id="tdCargo"></td>
                           <th width="10%">Sede</th>
                           <td id="tdSede"></td>
                        </tr>
                   </table>
                    <hr>
                    <table class="table table-bordered">
                        <tr>
                           <th width="15%">Fecha Inicio</th>
                           <td id="tdFechaInicio"></td>
                           <th width="15%">Fecha Final</th>
                           <td id="tdFechaFinal"></td>
                           <th width="15%">Días</th>
                           <td id="tdDias"></td>
                        </tr>
                    </table>
                    <hr>
                    <table class="table table-bordered">
                        <tr>
                           <th width="15%">Estado</th>
                           <td width="10%" id="tdEstado"></td>
                           <th width="20%">Motivo Rechazo</th>
                           <td id="tdMotivo"></td>
                        </tr>
                        <tr>
                           <th width="10%">Observación</th>
                           <td id="tdObservacion" colspan="3"></td>
                        </tr>
                    </table>

                    <hr>
                    <table class="table table-bordered">
                        <tr>
                           <th>Historial de Notificaciones</th>
                        </tr>
                    </table>
                    <table class="table table-bordered">
                        <tr>
                           <th width="15%"># Notificaciones</th>
                           <td id="tdNoNotificaciones"></td>
                           <td id="tdArchivoVisualizar" style="text-align: center;"></td>
                        </tr>
                    </table>
                    <hr>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                               <th width="20%">#</th>
                               <th>Fecha y hora Notificación</th>
                            </tr>    
                        </thead>
                        <tbody id="tablaReportes">
                            
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
 
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Para esta pagina me pase por el forro el MVC y lo hice lo mas rapido posible el javascript tambien lo meti aqui -->

<script type="text/javascript">
    var options = "<option value='ACEPTADA'>ACEPTADA</option>";
    options += "<option value='RECHAZADA'>RECHAZADA</option>";
    contadorFilas = 0;

    <?php 
        $campo = "*";
        $tabla = "gi_motivos_rechazo";

        $datos = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,null, null, " ORDER BY mot_desc_v ASC");
        $option = "<option value='0'>Selecione</option>";
        foreach ($datos as $key => $value) {
            $option .="<option value='".$value['mot_id_i']."'>".$value['mot_desc_v']."</option>";
        }
    ?>

    $(function(){
        $("#cargarInformacionDocumentosMaxx").click(function(){
            var form = $("#foermularioMaxivoDocux");
            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#foermularioMaxivoDocux")[0]);
            formData.append('contados', contadorFilas);
            $.ajax({
                url: 'ajax/solo_cargar_imagenes_nominax.php',
                type  : 'post',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                
                    alertify.success('Proceso terminado, total registros '+ data.total +', se cargaron con exito '+ data.exito +', Datos Errados '+ data.fallas );
                    
                    if(data.fallas == '0'){
                        $("#foermularioMaxivoDocu")[0].reset();  
                        $("#modalAgregarIncapacidadesMasivamente").modal('hide'); 
                    }
                    
                },
                //si ha ocurrido un error
                error: function(e){
                    console.log(e)
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            }); 
        });

        $("#AddcargaInformacionx").click(function(){
            var campos = "<div class='row' id='row_"+contadorFilas+"'>";
            campos += "<div class='col-md-3'>";
            campos += "<div class='form-group' id='divInputCedula_"+contadorFilas+"'><input type='text' class='form-control cedulasI' name='Editarcedula_"+contadorFilas+"' numero='"+contadorFilas+"' id='Editarcedula_"+contadorFilas+"' placeholder='Identificación'><span class='help-block' id='spanHelblok_"+contadorFilas+"'></span></div>";
            campos += "</div>";
            campos += "<div class='col-md-2'>";
            campos += "<div class='form-group'><input type='text' class='form-control fechasinicio' name='EditarFechaInicio_"+contadorFilas+"' id='EditarFechaInicio_"+contadorFilas+"' placeholder='Fecha Inicio'></div>";
            campos += "</div>";
            campos += "<div class='col-md-3'>";
            campos += "<div class='form-group'><input type='file' class='form-control cedulasI' name='EditarFilePrimerImagen_"+contadorFilas+"' id='EditarFilePrimerImagen_"+contadorFilas+"' placeholder='Identificación'></div>";
            campos += "</div>";
            campos += "<div class='col-md-3'>";
            campos += "<div class='form-group'><input type='text' class='form-control cedulasI' name='EditarObservacion_"+contadorFilas+"' id='EditarObservacion_"+contadorFilas+"' placeholder='Observación'></div>";
            campos += "</div>";
            campos += "<div class='col-md-1'>";
            campos += "<button type='button' class='btn btn-danger eliminarImagenes' id = 'borrar_"+contadorFilas+"' idfila='"+contadorFilas+"' title='Borrar fila'><i class='fa fa-trash-o'></i></button>";
            campos += "</div>";

            $("#filasx").append(campos);

            $("#borrar_"+contadorFilas).click(function(){
                var id = $(this).attr('idfila');
                $("#row_"+id).remove();
            });

            $("#Editarcedula_"+contadorFilas).change(function(){
                $(".alert").remove();
                var usuario = $(this).val();
                var numero = $(this).attr("numero");
                var datos = new FormData();
                datos.append('validarCedula', usuario);
                datos.append('empresa', $("#session").val());
                $.ajax({
                    url   : 'ajax/incapacidades.ajax.php',
                    method: 'post',
                    data  : datos,
                    cache : false,
                    contentType : false,
                    processData : false,
                    dataType    : 'json',
                    success     : function(respuesta){
                        if(respuesta == false){
                            alertify.error('Esta cedula no existe');
                            $("#Editarcedula_"+numero).focus();
                            $("#spanHelblok_"+numero).html('Identificación no registrada');
                            $("#divInputCedula_"+numero).removeClass( "has-warning has-success" ).addClass("has-error");
                        }else{
                            if(respuesta.emd_estado == '1'){
                                console.log("Soy Batman ::> seguimiento x ::> "+ respuesta.emd_estado)
                            $("#spanHelblok_"+numero).html('Empleado valido, EPS '+respuesta.ips_nombre);
                                $("#divInputCedula_"+numero).removeClass( "has-error has-warning" ).addClass("has-success");
                            }else{
                            
                                $("#spanHelblok_"+numero).html('Empleado valido pero no activo, Fecha de retiro '+ respuesta.emd_fecha_retiro+" EPS "+respuesta.ips_nombre);
                                $("#divInputCedula_"+numero).removeClass( "has-error has-success" ).addClass("has-warning");
                            }
                        }
                    }

                })
            });
            
            $('#EditarFechaInicio_'+contadorFilas).datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true    
            });
            contadorFilas++;
        });
        
        //Aqui hago la magia de agregar nuevas incapacidades 
        $("#AddcargaInformacionNomina").click(function(){
            //Aqui se agrega las filas con dos rows
            var campos = "<div class='row' id='row_"+contadorFilas+"'>";
            campos += "<div class='col-md-2'>";
            campos += "<div class='form-group' id='divInputCedula_"+contadorFilas+"'><input type='text' class='form-control cedulasI' name='Editarcedula_"+contadorFilas+"' numero='"+contadorFilas+"' id='Editarcedula_"+contadorFilas+"' placeholder='Identificación'><span class='help-block' id='spanHelblok_"+contadorFilas+"'></span></div>";
            campos += "</div>";
            campos += "<div class='col-md-2'>";
            campos += "<div class='form-group'><input type='text' class='form-control fechasinicio' name='EditarFechaInicio_"+contadorFilas+"' id='EditarFechaInicio_"+contadorFilas+"' placeholder='Fecha Inicio'></div>";
            campos += "</div>";
            campos += "<div class='col-md-2'>";
            campos += "<div class='form-group'><input type='text' class='form-control fechasFinal' name='EditarFechaFinal_"+contadorFilas+"' id='EditarFechaFinal_"+contadorFilas+"' placeholder='Fecha Final'></div>";
            campos += "</div>";
            campos += "<div class='col-md-2'>";
            campos += "<div class='form-group'><select class='form-control' name='EditarEstado_"+contadorFilas+"' id='EditarEstado_"+contadorFilas+"' numero='"+ contadorFilas +"'>"+ options +"</select></div>";
            campos += "</div>";
            campos += "<div class='col-md-4'>";
            campos += "<div class='form-group'><select disabled='true' class='form-control' name='EditarMotivo_"+contadorFilas+"' id='EditarMotivo_"+contadorFilas+"'><?php echo $option;?></select></div>";
            campos += "</div></div>";
            campos += "<div class='row'>";
            campos += "<div class='col-md-4'>";
            campos += "<div class='form-group'><input type='file' disabled='true' class='form-control' name='EditarFile_"+contadorFilas+"' id='EditarFile_"+contadorFilas+"'></div>";
            campos += "</div>";
            campos += "<div class='col-md-7'>";
            campos += "<div class='form-group'><input type='text' disabled='true' class='form-control' name='EditarObservacion_"+contadorFilas+"' id='EditarObservacion_"+contadorFilas+"'></div>";
            campos += "</div>";
            campos += "<div class='col-md-1'>";
            campos += "<button type='button' class='btn btn-danger eliminarImagenes' id = 'borrar_"+contadorFilas+"' idfila='"+contadorFilas+"' title='Borrar fila'><i class='fa fa-trash-o'></i></button>";
            campos += "</div>";
     
            $("#filas").append(campos);
            //Como agregamos un nuevo boton de borrar toca darle funcionalidad
            $("#borrar_"+contadorFilas).click(function(){
                var id = $(this).attr('idfila');
                $("#row_"+id).remove();
            });
        
            //Lo mismo que con el boton de borrar, necesito que el buscadode cedulas funcione por cada nuevo componente haga lo que tenga que hacer
            $("#Editarcedula_"+contadorFilas).change(function(){
                $(".alert").remove();
                var usuario = $(this).val();
                var numero = $(this).attr("numero");
                var datos = new FormData();
                datos.append('validarCedula', usuario);
                datos.append('empresa', <?php echo $_SESSION['cliente_id'];?>);
                $.ajax({
                    url   : 'ajax/incapacidades.ajax.php',
                    method: 'post',
                    data  : datos,
                    cache : false,
                    contentType : false,
                    processData : false,
                    dataType    : 'json',
                    success     : function(respuesta){
                        if(respuesta == false){
                            alertify.error('Esta cedula no existe');
                            $("#Editarcedula_"+numero).focus();
                            $("#spanHelblok_"+numero).html('Identificación no registrada');
                            $("#divInputCedula_"+numero).removeClass( "has-warning has-success" ).addClass("has-error");
                        }else{
                            console.log(respuesta);
                           // console.log("Soy Batman ::> respuesta.emp_estado ::> "+ respuesta.emp_estado);
                            if(respuesta.emd_estado == '1'){
                               
                                $("#spanHelblok_"+numero).html('Empleado valido, EPS '+respuesta.ips_nombre);
                                $("#divInputCedula_"+numero).removeClass( "has-error has-warning" ).addClass("has-success");
                            }else{
                                
                                $("#spanHelblok_"+numero).html('Empleado valido pero no activo, Fecha de retiro '+ respuesta.emd_fecha_retiro+", EPS "+respuesta.ips_nombre);
                                $("#divInputCedula_"+numero).removeClass( "has-error has-success" ).addClass("has-warning");
                            }
                        }
                    }
     
                })
            });
            
            //Igual para las fechas
            $('#EditarFechaInicio_'+contadorFilas).datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true    
            }).on('changeDate', function (selected) {
                $('#EditarFechaFinal_'+contadorFilas).val('');
                console.log(contadorFilas);
                var minDate = new Date(selected.date.valueOf());
               
                $('#EditarFechaFinal_'+contadorFilas).datepicker('setStartDate', minDate);
            }); 



            $("#EditarFechaFinal_"+contadorFilas).datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#EditarEstado_"+contadorFilas).change(function(){
                var numero =$(this).attr("numero");
                console.log("Este ::> "+ numero)
                if($(this).val() == "RECHAZADA"){
                    $("#EditarMotivo_"+numero).attr("disabled", false);
					$("#EditarFile_"+numero).attr("disabled", false);
                    $("#EditarObservacion_"+numero).attr("disabled", false);
                }else{
                    $("#EditarMotivo_"+numero).attr("disabled", true);
					$("#EditarFile_"+numero).attr("disabled", true);
                    $("#EditarObservacion_"+numero).attr("disabled", true);
				}
            })
            //aumento el contador para la siguiente fila
            contadorFilas++;
        });

        //Aqui es donde guardo la info
        $("#cargarInformacionDocumentosMaxNomina").click(function(){
            var form = $("#foermularioMaxivoDocu");
            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#foermularioMaxivoDocu")[0]);
            formData.append('contados', contadorFilas);
            $.ajax({
                url: 'ajax/solo_cargar_incapacidades_estado.php',
                type  : 'post',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                   
                    alertify.success('Proceso terminado, total registros '+ data.total +', se cargaron con exito '+ data.exito +', Datos Errados '+ data.fallas );
                    
                    if(data.fallas == '0'){
                        $("#foermularioMaxivoDocu")[0].reset();  
                        $("#modalAgregarIncapacidadesMasivamente").modal('hide'); 
                    }
                       
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            }); 
        });

        /* Eliminar Clientes */
        $('#tbl_IncapacidadesNomina tbody').on("click", ".btnEliminarIncapacidad", function(){
            var x = $(this).attr('idIncapacidad');
            swal({
                title: '¿Está seguro de borrar la incapacidad?',
                text: "¡Si no lo está puede cancelar la accíón!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, borrar incapacidad!'
            },function(isConfirm) {
                if (isConfirm) {
                    window.location = "index.php?ruta=incapacidadesnomina&idIncapacidad="+x ;
                }
            })
        });


        
        $('#tbl_IncapacidadesNomina tbody').on("click", ".btnVerIncapacidad", function(){
            var x = $(this).attr('idIncapacidad');
            var datas = new FormData();
            datas.append('getDataIncapacidades', x);
            $.ajax({
                url   : 'ajax/solo_cargar_incapacidades_estado.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    $("#tdIdentificacion").html(data.incn_cc_v); 
                    $("#tdEmpleado").html(data.emd_nombre);
                    $("#tdCargo").html(data.emd_cargo);
                    $("#tdEstado").html(data.incn_estado_v);
                    $("#tdMotivo").html(data.mot_desc_v);
                    $("#tdObservacion").html(data.inc_observacion_v);
                    $("#tdSede").html(data.emd_sede);
                    $("#tdEps").html(data.ips_nombre);
                    $("#tdEstadoEmpleado").html(data.emd_estado);

                    if(data.incn_fecha_inicio_d != null && data.incn_fecha_inicio_d != ''){
                        $("#tdFechaInicio").html(data.incn_fecha_inicio_d.split(' ')[0]);
                    }
                    if(data.incn_fecha_fin_d != null && data.incn_fecha_fin_d != ''){
                        $("#tdFechaFinal").html(data.incn_fecha_fin_d.split(' ')[0]);            
                    }

                    if(data.inc_img_ruta_v.length > 1){
                        $("#tdArchivoVisualizar").html('<a target="_blank" href="'+data.inc_img_ruta_v+'">Visualizar Archivo</a>');
                    }

                    $("#tdDias").html(data.dias);

                    var datax = new FormData();
                    datax.append('getDataNotificaciones', x);
                    $.ajax({
                        url   : 'ajax/solo_cargar_incapacidades_estado.php',
                        method: 'post',
                        data  : datax,
                        cache : false,
                        contentType : false,
                        processData : false,
                        dataType    : 'json',
                        success     : function(datex){
                            $("#tablaReportes").html('');
                            var cuantos = 0;
                            $.each(datex, function(i, item){
                                var cuerpo = '';
                                cuerpo += '<tr>';
                                    cuerpo += '<th width="20%">Fecha '+(i + 1)+'° Notificación:</th>';
                                    cuerpo += '<td>'+item.inn_fecha_notificacion+'</td>';
                                cuerpo += '</tr>';
                                $("#tablaReportes").append(cuerpo);
                                cuantos++;
                                $("#tdNoNotificaciones").html(cuantos);
                            });                            
                            
                        }
                    });
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }

            });
        });

        $('#tbl_IncapacidadesNomina tbody').on("click", ".btnEditarIncapacidad", function(){
            var x = $(this).attr('idIncapacidad');
            var datas = new FormData();
            datas.append('getDataIncapacidades', x);
            $.ajax({
                url   : 'ajax/solo_cargar_incapacidades_estado.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    $("#Editarcedula").val(data.incn_cc_v); 
                    $("#EditarNombres").val(data.emd_nombre);
                    $("#EditarAdministradora").val(data.ips_nombre);
                    $("#EditarEstado").val(data.incn_estado_v);
                    $("#EditarEstado").val(data.incn_estado_v).change();
                    $("#EditarMotivo").val(data.incn_razon_v);
                    $("#EditarMotivo").val(data.incn_razon_v).change();
                    $("#EditarAnhoNomina").val(data.incn_anho_i);
                    $("#EditarNomina").val(data.incn_nomina_i);
                    $("#EditarNomina").val(data.incn_nomina_i).change();
                    $("#txtObservaciones").val(data.inc_observacion_v);
                    
                    if(data.incn_fecha_inicio_d != null && data.incn_fecha_inicio_d != ''){
                        $("#EditarFechaInicio").val(data.incn_fecha_inicio_d.split(' ')[0]);
                    }

                    if(data.incn_fecha_fin_d != null && data.incn_fecha_fin_d != ''){
                        $("#EditarFechaFinal").val(data.incn_fecha_fin_d.split(' ')[0]);            
                    }
                    $("#idIncapacidadEdicion").val(data.incn_id_i);
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }

            });
        });

        $("#tbl_IncapacidadesNomina tbody").on("click", ".btnReportarIncapacidad", function(){
            var x = $(this).attr('idIncapacidad');
            var datas = new FormData();
            datas.append('sendMailIncapacidades', x);
            $.ajax({
                url   : 'ajax/enviar_correos.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    if(data.code == '1'){
                        alertify.success("Correo enviado");
                    }else{
                        alertify.error("Correo no enviado");
                    }
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            });
        });

        $("#EditarEstado").change(function(){
            if($(this).val() == "RECHAZADA"){
                $("#EditarMotivo").attr('disabled', false);
                $("#txtObservaciones").attr('disabled', false);
            }else{
                $("#EditarMotivo").attr('disabled', true);
                $("#txtObservaciones").attr('disabled', true);
                $("#txtObservaciones").val("");
                $("#EditarMotivo").val(0);
                $("#EditarMotivo").val(0).change();
            }
        })

        $('#EditarFechaInicio').datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true    
        }).on('changeDate', function (selected) {
            $('#EditarFechaFinal').val('');
            var minDate = new Date(selected.date.valueOf());
            $('#EditarFechaFinal').datepicker('setStartDate', minDate);
        }); 

        $("#EditarFechaFinal").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

    })
</script>

<?php 
    $nomina = new ControladorNomina();
    $nomina->ctrEliminarNomina();
?>