<?php 
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);

	require_once __DIR__.'/../../../extenciones/spout/src/Spout/Autoloader/autoload.php';
	require_once __DIR__.'/../../../extenciones/Carbon/autoload.php';
	use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
	use Box\Spout\Common\Entity\Row;
	use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
	use Box\Spout\Common\Entity\Style\CellAlignment;
	use Box\Spout\Common\Entity\Style\Color;

	$writer = WriterEntityFactory::createXLSXWriter();
	$writer->openToBrowser("Informe_consolidado.xlsx");
	

	$style = (new StyleBuilder())
           ->setFontBold()
           ->setFontColor(Color::WHITE)
           ->setCellAlignment(CellAlignment::CENTER)
           ->setBackgroundColor(Color::RED)
           ->build();

    $styleDate1 = (new StyleBuilder())->setFormat('mm/dd/yy')->build();
    $styleDate2 = (new StyleBuilder())->setFormat('mm/dd/yyyy')->build();

	$cells = [
	    WriterEntityFactory::createCell("#"),
	    WriterEntityFactory::createCell("Empresa"),
	    WriterEntityFactory::createCell("Cod. Nomina"), 
	    WriterEntityFactory::createCell("Identificación"),    
	    WriterEntityFactory::createCell("Apellidos y Nombres"), 
	    WriterEntityFactory::createCell("Cargo"), 
	    WriterEntityFactory::createCell("Salario"), 
	    WriterEntityFactory::createCell("Fecha Inicio"), 
	    WriterEntityFactory::createCell("Fecha Final"), 
	    WriterEntityFactory::createCell("Días"), 
	    WriterEntityFactory::createCell("Clasificación"), 
	    WriterEntityFactory::createCell("Diagnostico"), 
	    WriterEntityFactory::createCell("Origen"), 
	    WriterEntityFactory::createCell("Valor Empresa"), 
	    WriterEntityFactory::createCell("Valor Administradora"), 
	    WriterEntityFactory::createCell("Valor Ajuste"), 
	    WriterEntityFactory::createCell("Fecha Pago En Nomina"),  
	    WriterEntityFactory::createCell("Fecha Generación"), 
	    WriterEntityFactory::createCell("Estado Tramite"),
     	WriterEntityFactory::createCell("Fecha Estado"),
	    WriterEntityFactory::createCell("Valor Pagado"), 
	    WriterEntityFactory::createCell("Fecha Pago"),
	    WriterEntityFactory::createCell("Estado Empleado"),  
	    WriterEntityFactory::createCell("EPS"), 
	    WriterEntityFactory::createCell("AFP"), 
	    WriterEntityFactory::createCell("Sede"), 
	    WriterEntityFactory::createCell("Centro de Costos"), 
	    WriterEntityFactory::createCell("SubCentro de Costos"), 
	    WriterEntityFactory::createCell("Ciudad"),
	    WriterEntityFactory::createCell("Profesional Responsable"), 
	    WriterEntityFactory::createCell("Registro Medico"), 
	    WriterEntityFactory::createCell("Entidad-IPS"), 
	    WriterEntityFactory::createCell("Documentos"), 
	    WriterEntityFactory::createCell("Atención"), 
	    WriterEntityFactory::createCell("Creado Por"), 
	    WriterEntityFactory::createCell("Editado Por"), 
	    WriterEntityFactory::createCell("N. Radicado"), 
	    WriterEntityFactory::createCell("Motivo Rechazo"), 
	    WriterEntityFactory::createCell("Observacion 1"), 
	    WriterEntityFactory::createCell("Observacion 2"), 
	    WriterEntityFactory::createCell("Observacion 3"), 
	    WriterEntityFactory::createCell("Observacion 4"), 
	    WriterEntityFactory::createCell("Observacion 5"), 
	];

	$singleRow = WriterEntityFactory::createRow($cells);
	$singleRow->setStyle($style);
	$writer->addRow($singleRow);

	$item = null;
    $valor = null;
    if($_SESSION['cliente_id'] != 0){
        $item = 'inc_empresa';
        $valor = $_SESSION['cliente_id'];
    }
    //$incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);
    if(isset($_GET['fechaInicio']) && isset($_GET['fechaFinal']) && $_GET['fechaInicio'] != null && $_GET['fechaFinal'] != null){
        $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor, $_GET['fechaInicio'], $_GET['fechaFinal']);
    }else{
        $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);     
    }

    //print_r($incapacidades);
    foreach ($incapacidades as $key => $value) {
	    $rowFromValues = WriterEntityFactory::createRowFromArray($value);
		$writer->addRow($rowFromValues);
    }
    $writer->close();
