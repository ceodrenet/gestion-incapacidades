<?php
	session_start();
	require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
	require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../controladores/tesoreria.controlador.php';
    require_once '../../../modelos/dao.modelo.php';
    require_once '../../../modelos/incapacidades.modelo.php';
    require_once '../../../modelos/tesoreria.modelo.php';

	if(isset($_POST['yearFilter'])){
		$anho = null;
		if($_POST['yearFilter'] != 0){
			$anho = $_POST['yearFilter'];
		}		
?>
 <div class="row">
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">POR RADICAR</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo ControladorIncapacidades::getCountIncapacidades($anho, "2"); ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger"><?php echo number_format(ControladorIncapacidades::getSumIncapacidades($anho, "2"), 0, ',', '.'); ?></span>
                    <span class="ms-1 text-muted font-size-13">VALOR TOTAL ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">INCOMPLETAS</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo ControladorIncapacidades::getCountIncapacidadesIncompletas(); ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger"><?php echo number_format(ControladorIncapacidades::getSumIncapacidadesIncompletas(), 0, ',', '.'); ?></span>
                    <span class="ms-1 text-muted font-size-13">VALOR TOTAL ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col-->

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">RADICADAS</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo ControladorIncapacidades::getCountIncapacidades($anho, "3"); ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-success text-success"><?php echo number_format(ControladorIncapacidades::getSumIncapacidades($anho, "3"), 0, ',', '.'); ?></span>
                    <span class="ms-1 text-muted font-size-13">VALOR TOTAL ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-12">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">FILTRAR POR AÑOS</span>
                        <h4 class="mb-3">
                           <select id="filtrosIncapacidadAnual" class="form-control">
                               <?php 
                                    $i = 0;
                                    while ($i < 7) {
                                    	$_anho = (date('Y') - $i);
                                    	if($anho == $_anho){
                                        	echo "<option selected value='".$_anho."'>".$_anho."</option>";
                                    	}else{
                                    		echo "<option value='".$_anho."'>".$_anho."</option>";
                                    	}
                                        $i++;
                                    }
                               ?>
                           </select>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-primary text-primary"><?php if($_POST['yearFilter'] == 0) { echo "TOTAL"; }else{ echo $_POST['yearFilter'];}?></span>
                    <span class="ms-1 text-muted font-size-13">ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->
</div><!-- end row-->
</div><!-- end row-->

<script type="text/javascript">
	$(function(){
		$("#filtrosIncapacidadAnual").on('change', function(){
	        var _miValor = $(this).val();
	        $.ajax({
	            url : 'vistas/modulos/incapacidades/incapacidades_filtroAnual.php',
	            type: 'post',
	            data: {
	                yearFilter : _miValor
	            },
	            success : function(data){
	                $("#FieldsFilters").html(data);
	            }
	        });
	    });

		initCounterNumber();

	    function initCounterNumber() {
	        var counter = document.querySelectorAll('.counter-value');
	        var speed = 250; // The lower the slower
	        counter.forEach(function (counter_value) {
	            function updateCount() {
	                var target = +counter_value.getAttribute('data-target');
	                var count = +counter_value.innerText;
	                var inc = target / speed;
	                if (inc < 1) {
	                    inc = 1;
	                }
	                // Check if target is reached
	                if (count < target) {
	                    // Add inc to count and output in counter_value
	                    counter_value.innerText = (count + inc).toFixed(0);
	                    // Call function every ms
	                    setTimeout(updateCount, 1);
	                } else {
	                    counter_value.innerText = target;
	                }
	            };
	            updateCount();
	        });
	    }
	});
</script>
<?php		
	}
?>