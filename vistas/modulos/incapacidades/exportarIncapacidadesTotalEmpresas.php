<?php
    
    //session_start();
    ini_set('memory_limit', '-1');
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    ini_set('max_execution_time', '500');

    require_once __DIR__.'/../../../extenciones/spout/src/Spout/Autoloader/autoload.php';
    require_once __DIR__.'/../../../extenciones/Carbon/autoload.php';
    use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
    use Box\Spout\Common\Entity\Row;
    use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
    use Box\Spout\Common\Entity\Style\CellAlignment;
    use Box\Spout\Common\Entity\Style\Color;


    
    $empresas = ControladorIncapacidades::getDataFromLsql('emp_id, emp_nit, emp_nombre', 'gi_empresa','emp_estado = 1', null,  null, null);
    
    $zip = new ZipArchive();
    $nombreArchivoZip = __DIR__ . "/excell/EmpresasIncapacidades-".date('Y_m_d').".zip";

    if (!$zip->open($nombreArchivoZip, ZipArchive::CREATE | ZipArchive::OVERWRITE)) {
        echo "Error abriendo ZIP en $nombreArchivoZip";
    }

    foreach($empresas as $empresa => $company){

        $item = 'inc_empresa';
        $valor = $company['emp_id'];
        $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor, null, null, null,'%Y-%m-%d');     
        
        $writer = WriterEntityFactory::createXLSXWriter();
        $writer->openToFile(__DIR__ . "/excell/".$company['emp_nit'].".xlsx");
        

        $style = (new StyleBuilder())
               ->setFontBold()
               ->setFontColor(Color::WHITE)
               ->setCellAlignment(CellAlignment::CENTER)
               ->setBackgroundColor(Color::RED)
                ->build();

        $styleDate1 = (new StyleBuilder())->setFormat('mm/dd/yy')->build();
        $styleDate2 = (new StyleBuilder())->setFormat('mm/dd/yyyy')->build();

        $cells = [
            WriterEntityFactory::createCell("#"),
            WriterEntityFactory::createCell("Empresa"),
            WriterEntityFactory::createCell("Cod. Nomina"), 
            WriterEntityFactory::createCell("Identificación"),    
            WriterEntityFactory::createCell("Apellidos y Nombres"), 
            WriterEntityFactory::createCell("Cargo"), 
            WriterEntityFactory::createCell("Salario"), 
            WriterEntityFactory::createCell("Fecha Inicio"), 
            WriterEntityFactory::createCell("Fecha Final"), 
            WriterEntityFactory::createCell("Días"), 
            WriterEntityFactory::createCell("Clasificación"), 
            WriterEntityFactory::createCell("Diagnostico"), 
            WriterEntityFactory::createCell("Origen"), 
            WriterEntityFactory::createCell("Valor Empresa"), 
            WriterEntityFactory::createCell("Valor Administradora"), 
            WriterEntityFactory::createCell("Valor Ajuste"), 
            WriterEntityFactory::createCell("Fecha Pago En Nomina"),  
            WriterEntityFactory::createCell("Fecha Generación"), 
            WriterEntityFactory::createCell("Estado Tramite"),
            WriterEntityFactory::createCell("Fecha Estado"),
            WriterEntityFactory::createCell("Valor Pagado"), 
            WriterEntityFactory::createCell("Fecha Pago"),
            WriterEntityFactory::createCell("Estado Empleado"),  
            WriterEntityFactory::createCell("EPS"), 
            WriterEntityFactory::createCell("AFP"), 
            WriterEntityFactory::createCell("Sede"), 
            WriterEntityFactory::createCell("Centro de Costos"), 
            WriterEntityFactory::createCell("SubCentro de Costos"), 
            WriterEntityFactory::createCell("Ciudad"),
            WriterEntityFactory::createCell("Profesional Responsable"), 
            WriterEntityFactory::createCell("Registro Medico"), 
            WriterEntityFactory::createCell("Entidad-IPS"), 
            WriterEntityFactory::createCell("Documentos"), 
            WriterEntityFactory::createCell("Atención"), 
            WriterEntityFactory::createCell("Creado Por"), 
            WriterEntityFactory::createCell("Editado Por"), 
            WriterEntityFactory::createCell("N. Radicado"), 
            WriterEntityFactory::createCell("Motivo Rechazo"), 
            WriterEntityFactory::createCell("Ruta Imagen 1"), 
            WriterEntityFactory::createCell("Ruta Imagen 2"), 
            WriterEntityFactory::createCell("Observacion 1"), 
            WriterEntityFactory::createCell("Observacion 2"), 
            WriterEntityFactory::createCell("Observacion 3"), 
        ];

        $singleRow = WriterEntityFactory::createRow($cells);
        $singleRow->setStyle($style);
        $writer->addRow($singleRow);

        foreach ($incapacidades as $key => $value) {
            $rowFromValues = WriterEntityFactory::createRowFromArray($value);
            $writer->addRow($rowFromValues);
        }
        $writer->close();
        $writer = null;
        $rutaAbsoluta = __DIR__ . "/excell/".$company['emp_nit'].".xlsx";
        $nombre = basename($rutaAbsoluta);
        $zip->addFile($rutaAbsoluta, $nombre);

    }

    $resultado = $zip->close();

    foreach($empresas as $empresa => $company){
        $rutaAbsoluta = __DIR__ . "/excell/".$company['emp_nit'].".xlsx";
        unlink($rutaAbsoluta);
    }

    
