<div id="modalAgregarEntidades" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content border border-danger">
            <form id="frmGuardarNuevaEntidad" role="form" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                <h5 class="modal-title"id="staticBackdropLabel">Agregar Entidades</h5>
                <?php if($moduloInc == false){ ?>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <?php } ?>
                    
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Nombre</label>
                                <input class="form-control input-lg" type="text" name="IngresarNombre" placeholder="Ingresar Nombre" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Nit</label>
                                <input class="form-control input-lg" type="text" name="IngresarNit" id="IngresarNit" placeholder="Ingresar Nit" required="true">
                            </div>
                        </div>
                        <div class="form-group mb-3 ">
                            <div class="input-group">
                                <label class="form-label">Red a la que Pertenece</label>
                                <select style="width: 100%;" multiple="multiple" class="form-control input-lg" id="RedToPertenece" name="RedToPertenece[]">
                                    <?php
                                        $listEps = ControladorIps::ctrMostrarIps('ips_tipo', 'EPS');
                                        foreach ($listEps as $key => $value) {
                                            echo "<option value='".$value['ips_id']."'>".$value['ips_nombre']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php if($moduloInc == true){ ?>
                        <button type="button" id="btnGuardarEntidad" class="btn btn-danger">Guardar Entidad</button>
                    <?php }else{ ?>
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                        <button type="button" id="btnGuardarEntidad" class="btn btn-danger">Guardar Entidad</button>
                    <?php } ?>
                </div>
              
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*Nueva entidad*/
    /*Implementar la funcion AJax la de aqui es NuevoEntidad*/
    //Se crean un array con los datos a enviar, apartir del formulario
    $("#btnGuardarEntidad").click(function(){
        var formData = new FormData($("#frmGuardarNuevaEntidad")[0]);
        $.ajax({
            url   : 'ajax/registroEntidad.ajax.php',
            method: 'post',
            data  : formData,
            cache : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            beforeSend:function(){
                $.blockUI({ 
                    baseZ: 2000,
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff'
                    } 
                }); 
            },
           complete:function(){
                <?php if($moduloInc != true){ ?>
                    tablaRegistroEntidad.ajax.reload();
                <?php }?>
                $("#frmGuardarNuevaEntidad")[0].reset();
                $("#modalAgregarEntidades").modal('hide');  
                $.unblockUI();
            },
            //una vez finalizado correctamente
            success: function(data){
                if(data.code == '1'){
                    alertify.success( data.desc );    
                }else{
                    alertify.error( data.desc );
                } 
            },
            //si ha ocurrido un error
            error: function(){
                //after_save_error();
                alertify.error('Error al realizar el proceso');
            }
        });
    });
</script>