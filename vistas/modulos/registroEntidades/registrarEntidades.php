<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REGISTRAR ENTIDADES</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Entidades</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                   
                    <div class="row">
                        <div class="col-lg-11">
                            ADMINISTRAR ENTIDADES
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">
                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <a class="dropdown-item" title="Agregar Nuevas Entidades" data-bs-toggle="modal" data-bs-target="#modalAgregarEntidades" href="#">AGREGAR</a>
                                    <?php } ?>
                                </div>
                                
                            </div> 
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
            <table style="width: 100%;" id="tablaRegistroEntidad" class="table table-bordered table-striped dt-responsive tablas">
                    <thead>
                        <tr>
                            <th width="30%">Nombre Entidad</th>
                            <th width="17%">Nit </th>
                            <th width="17%">Fecha Creacion</th>
                            <th width="18%">Estado</th>
                            <th width="18%">Acciones</th>
                        </tr>
                    </thead>
                    
                </table>    
            </div>
        </div>
    </div>
</div>
<!-- <div class="row">
    <div class="col-12" id="resultados">
        
    </div>
</div> -->

<?php 
    $moduloInc = false;
    include 'modalAddEntidadIps.php';
?>

<div id="modalEditarEntidades" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form id="frmEditarEntidad" role="form" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                <h5 class="modal-title"id="staticBackdropLabel">Editar Entidades</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Nombre</label>
                                <input class="form-control input-lg" type="text" name="EditarNombre" id="EditarNombre" placeholder="Ingresar Nombre" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Nit</label>
                                <input class="form-control input-lg" type="text" name="EditarNit" id="EditarNit" placeholder="Ingresar Nit" required="true">
                            </div>
                        </div>

                        <div class="form-group mb-3 ">
                            <div class="input-group">
                                <label class="form-label">Red a la que Pertenece</label>
                                <select style="width: 100%;" multiple="multiple" class="form-control input-lg" id="EditarRedToPertenece" name="EditarRedToPertenece[]">
                                    <?php
                                        $listEps = ControladorIps::ctrMostrarIps('ips_tipo', 'EPS');
                                        foreach ($listEps as $key => $value) {
                                            echo "<option value='".$value['ips_id']."'>".$value['ips_nombre']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    
                       <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Seleccione el Estado</label>
                                <select class="form-control input-lg" name="EditarEstado" id="EditarEstado">
                                  <option value="0">INACTIVA</option>
                                  <option value="1">ACTIVA</option>
                                </select>
                            </div>
                        </div> 
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="EditarEntidadID" id="EditarEntidadID"/>
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                    <button type="button" id="btnEditarEntidad" class="btn btn-danger">Editar Entidad</button>
                </div>
              
            </form>
        </div>
    </div>
</div>

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>
<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/registroEntidad.js?v=<?php echo rand();?>">
</script>


<!-- <script >
function numeros(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " 0123456789";
    especiales = [8,37,39,46];
 
    tecla_especial = false
    for(var i in especiales){
 if(key == especiales[i]){
     tecla_especial = true;
     break;
        } 
    }
 
    if(letras.indexOf(tecla)==-1 && !tecla_especial)
        return false;
}
</script> -->