<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">ENTIDADES</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Configurar Notificaciones </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                   
                    <div class="row">
                        <div class="col-lg-11">
                     NOTIFICACIONES
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">
                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <a class="dropdown-item" title="Agregar Usuario EPS" data-bs-toggle="modal" data-bs-target="#modalAgregarEntidad" href="#">AGREGAR</a>
                                    <div class="dropdown-divider"></div>
                                    <?php } ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
            
        <div class="box">
           <!-- <div class="box-header with-border">
                <?php if($_SESSION['adiciona'] == 1){ ?>
                <button class="btn btn-primary pull-right" data-toggle="modal" title="Agregar Entidad" data-target="#modalAgregarEntidad">
                   <i class="fa fa-plus"></i>
                </button>
                <?php } ?>
            </div>-->
            <div class="box-body">
                <table style="width:100%;" id="tablaEntidadesEps" class="table table-bordered table-striped dt-responsive tablas">
                    <thead>
                        <tr>
                            <th style="width: 20%;">Nombre</th>
                            <th>NIT</th>
                            <th>C. Cartera</th>
                            <th>C. Radicación</th>
                            <th>C. PQRS</th>
                            <th>C. Notificación</th>
                            <th style="width: 10%;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>      
            </div>
        </div>
   
    <!-- /.content -->
            </div>
        </div>
    </div>
</div>
<!-- Modal agregar entidad-->
<div id="modalAgregarEntidad"  data-backdrop="static" data-keyboard="false"  class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" >
            <form role="form" id="nuevaEntidadIps" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                <h5 class="modal-title"id="staticBackdropLabel">Agregar Entidad</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                    
                                    <label class="form-label">Codigo Entidad</label>
                                    <input class="form-control fecha" type="text" name="NuevoCodigoEps" id="NuevoCodigoEps" placeholder="Ingresar Codigo Entidad" >
                                    
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                          <label class="form-label">Nombre Entidad</label>
                                          <input class="form-control" type="text" name="NuevoNombreEps" id="NuevoNombreEps" placeholder="Ingresar Nombre Entidad" >
                                    
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                 <label class="form-label">Nit Entidad</label>
                                    <div class="input-group">
                                    <input class="form-control fecha" type="text" name="NuevoNitEps" id="NuevoNitEps" placeholder="Ingresar NIT" >
                                    </div>
                                            </div>   
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <label class="form-label">Tipo Entidad</label>
                                    <div class="input-group">
                                    <select class="form-control" style="width: 100%;" required="true" id="NuevoTipoEps" name="NuevoTipoEps">
                                            <option value="">Selecionar Tipo</option>
                                            <option value="EPS">EPS</option>
                                            <option value="ARL">ARL</option>
                                            <option value="AFP">AFP</option>
                                            <option value="ADMINISTRADORA">ADMINISTRADORA</option>
                                        </select>
                                    </div>
                                     
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="mb-3">
                                    <label>Correo Cartera</label>
                                    <div class="input-group">
                                    <input class="form-control fecha" type="text" name="NuevoCorreoCartera" id="NuevoCorreoCartera" placeholder="Ingresar Correo Cartera" >
                                     </div>
                            </div>        
                        </div>      
                            
                            <div class="col-md-6 col-xs-12">
                                    <label>Correo Radicación</label>
                                    <div class="input-group">
                                    <input class="form-control" type="text" name="NuevoCorreoRadicar" id="NuevoCorreoRadicar" placeholder="Ingresar Correo Radicación" >
                                    </div>
                                      
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                    
                                    <label class="form-label">Correo PQR</label>
                                    <input class="form-control" type="text" name="NuevoCorreoPQRS" id="NuevoCorreoPQRS" placeholder="Ingresar Correo PQRS" >
                                    
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                          <label class="form-label">Correo Notificacin</label>
                                          <input class="form-control" type="text" name="NuevoCorreoNOtificacion" id="NuevoCorreoNOtificacion" placeholder="Ingresar Correo Notificación" >
                                    
                                </div>        
                            </div>
                        </div>
                </div>   
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                        <button type="button" id="guardarEntidadNueva"  class="btn btn-danger">Guardar Entidad</button>
                    </div>
                <?php
                    /*$crearArchivo = new ControladorCartera();
                    $crearArchivo->ctrCrearCartera();*/
                ?>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal editar entidad-->
<div id="modalEditarEntidad"  data-backdrop="static" data-keyboard="false"  class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg"  role="document">
        <div class="modal-content" >
            <form role="form" autocomplete="off" method="post" id="EdicionEntidad" enctype="multipart/form-data">
                <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Editar Entidad</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> 
                  
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                    
                                    <label class="form-label">Codigo Entidad</label>
                                    <input class="form-control fecha" type="text" required name="EditarCodigoEps" id="EditarCodigoEps" placeholder="Ingresar Codigo Entidad" >
                                    
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                          <label class="form-label">Nombre Entidad</label>
                                          <input class="form-control" type="text" required name="EditarNombreEps" id="EditarNombreEps" placeholder="Ingresar Nombre Entidad" >
                                    
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                 <label class="form-label">Nit Entidad</label>
                                    <div class="input-group">
                                    <input class="form-control fecha" type="text" required name="EditarNitEps" id="EditarNitEps" placeholder="Ingresar NIT" >
                                    </div>
                                            </div>   
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <label class="form-label">Tipo Entidad</label>
                                    <div class="input-group">
                                    <select class="form-control" style="width: 100%;" required="true" id="EditarTipoEps" name="EditarTipoEps">
                                            <option value="">Selecionar Tipo</option>
                                            <option value="EPS">EPS</option>
                                            <option value="ARL">ARL</option>
                                            <option value="AFP">AFP</option>
                                            <option value="ADMINISTRADORA">ADMINISTRADORA</option>
                                        </select>
                                    </div>
                                     
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="mb-3">
                                    <label>Correo Cartera</label>
                                    <div class="input-group">
                                        <input class="form-control fecha" type="text" name="EditarCorreoCartera" id="EditarCorreoCartera" placeholder="Ingresar Correo Cartera" >
                                     </div>
                            </div>        
                        </div>      
                            
                            <div class="col-md-6 col-xs-12">
                                    <label>Correo Radicación</label>
                                    <div class="input-group">
                                        <input class="form-control fecha" type="text" name="EditarCorreoRadicar" id="EditarCorreoRadicar" placeholder="Ingresar Correo Radicación" >
                                    </div>
                                      
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                    
                                    <label class="form-label">Correo PQR</label>
                                    <input class="form-control fecha" type="text" name="EditarCorreoPQRS" id="EditarCorreoPQRS" placeholder="Ingresar Correo PQRS" >
                                    
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                          <label class="form-label">Correo Notificacin</label>
                                          <input class="form-control" type="text" name="EditarCorreoNOtificacion" id="EditarCorreoNOtificacion" placeholder="Ingresar Correo Notificación" >
                                    
                                </div>        
                            </div>
                        </div>
                </div>   
                    <div class="modal-footer">
                        <input type="hidden" name="EditarId" id="EditarId" value="0">
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                        <button type="button" id="enviarEdicion" class="btn btn-danger">Guardar Entidad</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php 
   /* $crearPaciente = new ControladorEntidades();
    $crearPaciente->ctrBorrarEntidad();*/
?>
<script type="text/javascript">
    $(function(){

        /*var edicion = '<button  title="Editar Entidad" class="btn btn-warning btnEditarEps" idEntidades data-toggle="modal" data-target="#"><i class="fa fa-edit"></i></button>&nbsp;';
        edicion += '<button title="Eliminar Entidad" class="btn btn-danger btnEliminarEps" idEntidades><i class="fa fa-times"></i></button>';
*/

let edicion = '<div class="btn-group dropstart" role="group">';
edicion += '<button type="button" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
edicion += '<i class="fa fa-info-circle"></i>';
edicion += '</button>';
edicion += '<div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">';
edicion += '<a class="dropdown-item btnEditarEps" title="Editar Entidad" idEntidades data-bs-toggle="modal" data-bs-target="#modalEditarEntidad">EDITAR</a>';
edicion += '<div class="dropdown-divider"></div>';
edicion += '<a class="dropdown-item btnEliminarEps" title="Eliminar Entidad" idEntidades codigo imagen>ELIMINAR</a>';



        var tablaIncapa = $('#tablaEntidadesEps').DataTable({
            "ajax": "ajax/administradoras.ajax.php?dameEpsPorFavor=owi",
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "defaultContent": edicion
                }
            ],
            "language" : {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            } 
        });

        $('#tablaEntidadesEps tbody').on( 'click', 'button', function () {
            var data = tablaIncapa.row( $(this).parents('tr') ).data();
            $(this).attr("idEntidades", data[6]);
        });
        $('#tablaEntidadesEps tbody').on( 'click', 'a', function () {
            var data = tablaIncapa.row( $(this).parents('tr') ).data();
            console.log(data);
            $(this).attr("idEntidades", data[6]);
        });
        $('#tablaEntidadesEps tbody').on("click", ".btnEditarEps", function(){
                var x = $(this).attr('idEntidades');
                var datas = new FormData();
                console.log($(this));
                datas.append('idEntidades', x);
                $.ajax({
                url   : 'ajax/administradoras.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    $("#EditarNombreEps").val(data[0].ips_nombre); 
                    $("#EditarCodigoEps").val(data[0].ips_codigo);        
                    $("#EditarNitEps").val(data[0].ips_nit);
                    $("#EditarTipoEps").val(data[0].ips_tipo);
                    $("#EditarCorreoCartera").val(data[0].ips_correo_cartera);
                    $("#EditarCorreoPQRS").val(data[0].ips_correo_pqr_v);
                    $("#EditarCorreoRadicar").val(data[0].ips_correo_radicacion);
                    $("#EditarCorreoNOtificacion").val(data[0].ips_correo_not_v);
                    $("#EditarId").val(data[0].ips_id);
                }

            });
        });

        /* Eliminar administradoras */
    $('#tablaEntidadesEps tbody').on("click", ".btnEliminarEps", function(){
        var x = $(this).attr('idEntidades');
        swal.fire({
            title: '¿Está seguro de borrar el usuario?',
            text: "¡Si no lo está puede cancelar la accíón!",
        /* type: 'warning',*/
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, borralo!'
        }).then(function (result) {
            if (result.value) {
                //window.location = "index.php?ruta=usuariosEps&id_usuario_eps="+x ;
                var datas = new FormData();
                datas.append('ips_id', x);
                $.ajax({
                url   : 'ajax/administradoras.ajax.php',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                   // tabla_usuariosEps.ajax.reload();
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    if(data.code == '1'){
                        alertify.success( data.desc );    
                    }else{
                        alertify.error( data.desc );
                    } 
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        }
    })
});

  

var table = $('#tablaUsuariosEps').DataTable({
    "language" : {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});

        $("#enviarEdicion").click(function(){
            var form = $("#EdicionEntidad");
            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#EdicionEntidad")[0]);
            $.ajax({
                url: 'ajax/administradoras.ajax.php',
                type  : 'post',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    tablaIncapa.ajax.reload();
                    $("#modalEditarEntidad").modal('hide');
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    if(data.code == '1'){
                        alertify.success( data.message );    
                        $("#EdicionEntidad")[0].reset(); 
                    }else{
                        alertify.error( data.message );
                    }
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        });

        $("#guardarEntidadNueva").click(function(){
            var form = $("#nuevaEntidadIps");
            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#nuevaEntidadIps")[0]);
            $.ajax({
                url: 'ajax/administradoras.ajax.php',
                type  : 'post',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    tablaIncapa.ajax.reload();
                    $("#modalAgregarEntidad").modal('hide');
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    if(data.code == '1'){
                        alertify.success( data.message );  
                        $("#nuevaEntidadIps")[0].reset();  
                    }else{
                        alertify.error( data.message );
                    }


                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        });

      
    });
    
</script>

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
