<?php
    $fecha_para_buscar = "inc_fecha_pago_nomina";
    if(isset($tipoReporte)){
        $fecha_para_buscar = "inc_fecha_pago";
    }
?>
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="info-box bg-green">
            <span class="info-box-icon"><i class="ion ion-cash"></i></span>
            <?php
               
                $campos = "sum(inc_valor_pagado_eps) as total ";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones =" inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'PAGADA' AND inc_estado_tramite != 'SIN RECONOCIMIENTO' AND inc_clasificacion != 4 AND inc_origen != 'ACCIDENTE TRANSITO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = "inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'PAGADA' AND inc_estado_tramite != 'SIN RECONOCIMIENTO' AND inc_clasificacion != 4 AND inc_origen != 'ACCIDENTE TRANSITO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }
                $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                $totaValor = $respuesta['total'];
            


                $campos = "sum(inc_valor_pagado_eps) as total";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'PAGADA' AND inc_estado_tramite != 'SIN RECONOCIMIENTO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = " inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'PAGADA' AND inc_estado_tramite != 'SIN RECONOCIMIENTO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }
                $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                $totaValor1 = $respuesta2['total'];

                /*Obtenemos los porcentajes*/
                $porcentaje = 0;
                if($totaValor1 > 0){
                    $porcentaje =  ($totaValor * 100) / $totaValor1;
                }
            ?>
                
            <div class="info-box-content">
                <span class="">EPS</span>
                <span class="info-box-number" style="font-size:15px;"><?php echo number_format($totaValor, 0, ',', '.'); ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="info-box bg-green" >
            <span class="info-box-icon"><i class="ion ion-cash"></i></span>
            <?php
                $campos = "sum(inc_valor_pagado_eps) as total ";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones =" inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'PAGADA' AND inc_clasificacion != 4 AND inc_origen = 'ACCIDENTE TRANSITO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = "inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'PAGADA' AND inc_clasificacion != 4 AND inc_origen = 'ACCIDENTE TRANSITO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }
                $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                $totaValor = $respuesta['total'];
                
                

                /*Obtenemos los porcentajes*/
                $porcentaje = 0;
                if($totaValor1 > 0){
                    $porcentaje =  ($totaValor * 100) / $totaValor1;
                }
            ?>
            <div class="info-box-content">
                <span class="info-box-text">ARL</span>
                <span class="info-box-number" style="font-size:15px;"><?php echo number_format($totaValor, 0, ',', '.'); ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>

    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <div class="info-box bg-green" >
            <span class="info-box-icon"><i class="ion ion-cash"></i></span>
            <?php
                $campos = "sum(inc_valor_pagado_eps) as total ";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones =" inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'PAGADA' AND inc_clasificacion = 4 AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = "inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'PAGADA' AND inc_clasificacion = 4 AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }
                $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                $totaValor = $respuesta['total'];
                
                if($totaValor1 > 0){
                    $porcentaje =  ($totaValor * 100) / $totaValor1;
                }
            ?>
            <div class="info-box-content">
                <span class="info-box-text">AFP</span>
                <span class="info-box-number" style="font-size:15px;"><?php echo number_format($totaValor, 0, ',', '.'); ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
<!-- ./col -->
</div>