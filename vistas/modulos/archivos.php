<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">ARCHIVOS</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Archivos</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                   
                    <div class="row">
                        <div class="col-lg-11">
                        REGISTROS DEL SISTEMA
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">
                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <a class="dropdown-item" title="Agregar Archivos" data-bs-toggle="modal" data-bs-target="#modalAgregarArchivos" href="#">AGREGAR</a>
                                    <?php } ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body"> 
            <table style="width:100%;" id="tablaArchivos" class="table table-bordered table-striped dt-responsive tablas">
                    <thead>
                        <tr>
                            <th>Entidad</th>
                            <th>Origen</th>
                            <th style="width: 15%;">Fecha</th>
                            <th>Descripción</th>
                            <th>Adjunto #1</th>
                            <th>Adjunto #2</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Entidad</th>
                            <th>Origen</th>
                            <th style="width: 15%;">Fecha</th>
                            <th>Descripción</th>
                            <th>Adjunto #1</th>
                            <th>Adjunto #2</th>
                            <th>Acciones</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>      
            </div>
        </div>
    </div>
</div>

<div id="modalAgregarArchivos" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" id="AgragrNuevoArchivo" enctype="multipart/form-data">
                <div class="modal-header">
                     <h5 class="modal-title"id="staticBackdropLabel">Agregar Archivo</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                 <div class="mb-3">
                                        <label class="form-label">Ingresar Nombre</label>
                                        <input class="form-control" type="text" name="NuevoEntidad"  id="NuevoEntidad" placeholder="Ingresar Nombre Entidad" required="true">
                                  
                                     </div> 
                            </div>
                        </div>

                        <div class="row">
                                 <div class="col-md-6 col-xs-12">
                                    <div class="mb-3">
                                         <label class="form-label">Ingresar Origen</label>
                                            <select class="form-control" name="NuevoOrigen" id="NuevoOrigen" >
                                                        <option value="Enviado">Seleccione</option>
                                                        <option value="Enviado">Enviado</option>
                                                         <option value="Recibido">Recibido</option>
                                             </select>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12">
                                    <div class="mb-3">
                                         <label class="form-label">Ingresar Fecha</label>
                                         <input class="form-control fech" type="text" name="NuevoFecha" id="NuevoFecha" placeholder="Ingresar Fecha" >
                                    </div>
                                </div>
                        </div>
                                
                               
                            <?php if($_SESSION['cliente_id'] != 0){ ?>
                                <input type="hidden" name="Nuevoarc_empresa_id_i" value="<?php echo $_SESSION['cliente_id']; ?>">
                            <?php }else{ ?>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                         
                                            <select name="Nuevoarc_empresa_id_i" class="form-control input-sm"> 
                                                <option value="0">Seleccione</option>
                                                <?php 
                                                    $item = null;
                                                    $valor = null;
                                                    $clientes = ControladorClientes::ctrMostrarClientes($item, $valor);
                                                    foreach ($clientes as $key => $value) {
                                                        echo "<option value='".$value['emp_id']."'>".$value['emp_nombre']."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                            <?php }?>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="mb-3">
                                    <div class="input-group">
                                        <textarea class="form-control" type="text" name="NuevoDescripcion" id="NuevoDescripcion" placeholder="Ingresar Descripción" ></textarea>
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Archivo adjunto numero 1</label>
                                    <input type="file" class="NuevaFoto" valor='0' name="NuevoFormato1" placeholder="Elegir rut del cliente">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar pull-right">
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Archivo adjunto numero 2</label>
                                    <input type="file" class="NuevaFoto" valor='2' name="NuevoFormato2" placeholder="Elegir rut del cliente">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2 pull-right">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-bs-dismiss="modal">Salir</button>
                        <button type="button" id="btnNuevoArchivo" class="btn btn-danger">Guardar Archivo</button>
                    </div>
                    <?php
                        /*$crearArchivo = new ControladorArchivos();
                        $crearArchivo->ctrCrearArchivo();*/
                    ?>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="modalEditarArchivos" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off"  id="EditarArchivoExistente"  method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title"id="staticBackdropLabel">Editar Archivo</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Nombre</label>
                                        <input class="form-control" type="text" name="EditarEntidad"  id="EditarEntidad" placeholder="Ingresar Nombre Entidad" required="true">
                                    </div>
                                </div>        
                            </div>

                            
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Fecha</label>
                                        <input class="form-control fech" type="text" name="EditarFecha" id="EditarFecha" placeholder="Ingresar Fecha" >
                                    </div>
                                </div>        
                            </div>
                            <?php if($_SESSION['cliente_id'] != 0){ ?>
                                <input type="hidden" name="Editararc_empresa_id_i" id="Editararc_empresa_id_i" value="<?php echo $_SESSION['cliente_id']; ?>">
                            <?php }else{ ?>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                          
                                            <select name="Editararc_empresa_id_i" id="Editararc_empresa_id_i" class="form-control input-sm"> 
                                                <option value="0">Seleccione</option>
                                                <?php 
                                                    $item = null;
                                                    $valor = null;
                                                    $clientes = ControladorClientes::ctrMostrarClientes($item, $valor);
                                                    foreach ($clientes as $key => $value) {
                                                        echo "<option value='".$value['emp_id']."'>".$value['emp_nombre']."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                            <?php }?>

                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                   
                                         <label class="form-label">Ingresar Origen</label>
                                        <select class="form-control" name="EditarOrigen" id="EditarOrigen" >
                                            <option value="Enviado">Enviado</option>
                                            <option value="Recibido">Recibido</option>
                                        </select>
                                   
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Observacion</label>
                                        <textarea class="form-control" type="text" name="EditarDescripcion" id="EditarDescripcion" placeholder="Ingresar Descripción" ></textarea>
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Adjunto Numero 1</label>
                                    <input type="file" class="NuevaFoto" valor='2' name="EditarFormato1">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2 pull-right">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Adjunto Numero 2</label>
                                    <input type="file" class="NuevaFoto" valor='3' name="EditarFormato2">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_3 pull-right">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="EditarId" id="EditarId">
                        <input type="hidden" name="formato1" id="formato1">
                        <input type="hidden" name="formato2" id="formato2">

                        <button type="button" class="btn btn-default pull-left" data-bs-dismiss="modal">Salir</button>
                        <button type="button" id="btnEditarArchivo" class="btn btn-danger">Guardar Archivos</button>
                    </div>
                <?php
                   /* $crearArchivo = new ControladorArchivos();
                    $crearArchivo->ctrEditarArchivos();*/
                ?>
                </div>
            </form>
        </div>
    </div>
</div>

<?php 
   /* $crearArchivo = new ControladorArchivos();
    $crearArchivo->ctrBorrarArchivo();*/
?>

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/archivos.js?v=<?php echo rand();?>"></script>