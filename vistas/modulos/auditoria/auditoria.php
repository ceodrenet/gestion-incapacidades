<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">AUDITORIA - EMPRESA <?php echo mb_strtoupper(ModeloIncapacidades::getDatos('gi_empresa', 'emp_id', $_SESSION['cliente_id'])['emp_nombre']); ?></h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Auditoria</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    FILTRAR INFORMACION DE AUDITORIA
                </h5>
            </div>
            <div class="card-body">
                <form autocomplete="off">
                    <div class="row">
                        <div class="col-md-2">
                            <input class="form-control flatpickr-input" type="text" name="NuevoFechaInicio" id="NuevoFechaInicio2" placeholder="" required="true">
                        </div>

                        <div class="col-md-2">
                            <input class="form-control flatpickr-input" type="text" name="NuevoFechaFinal" id="NuevoFechaFinal2" placeholder="" required="true">
                        </div>

                        <div class="col-md-3">
                            <select name="session" id="buscarUsuario"  >
                            <option value="0">Busqueda por Usuarios</option>
                            <?php
                                                $camppos ="user_id, user_nombre as nombres";
                                                $tabla__ = "gi_usuario";
                                                $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                                foreach ($atencion as $key => $value) {
                                                    echo '<option value="'.$value['user_id'].'">'.$value['nombres'].'</option>';
                                                }
                                            ?>  
                            </select>
                        </div>

                        <div class="col-md-3">
                            <select name="session" id="buscarEmpresa" class= "form-control" value="<?php echo $_SESSION['cliente_id'];?>">
                            <option value="0">Busqueda por Empresas</option>
                            <?php
                                                $camppos ="emp_id, emp_nombre as empresas";
                                                $tabla__ = "gi_empresa";
                                                $atencion = ModeloTesoreria::mdlMostrarGroupAndOrder($camppos,$tabla__,null, null, null);
                                                foreach ($atencion as $key => $value) {
                                                    echo '<option value="'.$value['emp_id'].'">'.$value['empresas'].'</option>';
                                                }
                                            ?>  
                            </select>
                        </div>
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscarAuditoria"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>
                    </div>    
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12" id="resultados">
        
    </div>
</div>

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>
<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/auditoria.js?v=<?php echo rand();?>">
</script>
  