<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Radicaciones 
            <small>Soporte radicaciones</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Radicaciones</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-7 col-xs-12">
                <div class="box">
                    <div class="box-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Cedula</label>
                                <input type="text" class="form-control" name="txtAnhoNomina" id="txtAnhoNomina" placeholder="Cedula">
                                <span class="help-block" id="spanHelblok"></span>
                            </div>
                            <div class="col-md-4">
                                <?php //$menuspermisos = ControladorClientes::ctrMostrarClientes("emp_id", $_SESSION['cliente_id']); ?>
                                <label>Eps</label>
                                <select id="eps" class="form-control">
                                    <option value="nuevaeps">Nueva eps</option>
                                    <option value="otra">Otra</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>&nbsp;</label>
                                <input type="button" value="Imprimir" class="form-control" id="btnPrint" />
                            </div>
                            <div class="col-md-2">
                                <label>&nbsp;</label>
                                <div id="editor"></div>
                                <button id="cmd" class="form-control">Descargar</button>
                                <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
                                <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
                                <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js" integrity="sha512-s/XK4vYVXTGeUSv4bRPOuxSDmDlTedEpMEcAQk0t/FMd9V6ft8iXdwSBxV0eD60c6w/tjotSlKu9J2AAW1ckTA==" crossorigin="anonymous"></script>-->
                                <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
                                <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.js"></script>
                                <script src="https://cdn.bootcss.com/html2pdf.js/0.9.1/html2pdf.js"></script>-->    
                            </div>
                        </div>
                    </div>    
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <label style="text-align: center;">Identificación</label>
                            </div>
                            <div class="col-md-2">
                                <label style="text-align: center;">Fecha inicio</label>
                            </div>
                            <div class="col-md-2">
                                <label style="text-align: center;">Fecha final</label>
                            </div>
                            <div class="col-md-4">
                                <label style="text-align: center;">Estado</label>
                            </div>
                            <div class="col-md-2">
                                <label style="text-align: center;">Motivo</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12" id="filas">
                            
                                
                    </div>            
                    </div>            
                </div>
            </div>
            <div class="col-md-5 col-xs-12"> 
                <div class="box formato">
                    <div class="box-body">
                        <div class="col-md-12 col-sm-12">
                            Seleccionar formato
                        </div>
                    </div>
                </div>
            </div><!--col-->
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    $("#txtAnhoNomina").change(function(){
        $(".alert").remove();
        var usuario = $(this).val();
        var numero = $(this).attr("numero");
        var datos = new FormData();
        datos.append('getIncapaciades', usuario);//validarCedula
        //datos.append('empresa', $("#session").val()); 25284837
        $.ajax({
            url   : 'ajax/incapacidades.ajax.php',
            method: 'post',
            data  : datos,
            cache : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            success     : function(respuesta){
                console.log(respuesta)
                if(respuesta == false){
                    alertify.error('Esta cedula no existe');
                    $("#Editarcedula_"+numero).focus();
                    $("#spanHelblok").html('Identificación no registrada');
                    $("#divInputCedula_"+numero).removeClass( "has-warning has-success" ).addClass("has-error");
                }else{
                    /*console.log("Soy Batman ::> respuesta.emd_estado ::> "+ respuesta.emd_estado)
                    if(respuesta.emd_estado == '1'){
                        console.log("Soy Batman ::> seguimiento ::> "+ respuesta.emd_estado)
                    $("#spanHelblok").html('Empleado valido');
                        $("#divInputCedula").removeClass( "has-error has-warning" ).addClass("has-success");
                    }else{
                        console.log("Soy Batman ::> seguimiento ::> "+ respuesta.emd_estado)
                        $("#spanHelblok").html('Empleado valido pero no activo');
                        $("#divInputCedula").removeClass( "has-error has-success" ).addClass("has-warning");
                    }*/
                    var text = '';
                    $.each(respuesta, function( index, value ) {
  text += '<div class="row"><div class="col-md-2 col-xs-2 col-sm-2">'+value['emd_cedula'] + ' ' +value['emd_nombre']+'</div><div class="col-md-2 col-xs-2 col-sm-2">'+value['inc_fecha_inicio']+'</div><div class="col-md-2 col-xs-2 col-sm-2">'+value['inc_fecha_final']+'</div><div class="col-md-4 col-xs-4 col-sm-4"><button class="btn">'+value['inc_estado_tramite']+'</button></div><div class="col-md-2 col-xs-2 col-sm-2">'+value['inc_origen']+'</div></div>';
});
                    $("#filas").html(text)
                }
            }

        })
    });
    
    $("#eps").change(function(){
        $(".alert").remove();
        var usuario = $(this).val();
        var numero = <?php echo $_SESSION['cliente_id']; ?>;//$(this).attr("numero");
        var datos = new FormData();
        datos.append('EditarClienteId', numero);//validarCedula usuario
        //datos.append('empresa', $("#session").val()); 25284837
        $.ajax({
            url   : 'ajax/clientes.ajax.php',
            method: 'post',
            data  : datos,
            cache : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            success     : function(respuesta){
                console.log(respuesta)
                if(respuesta == false){
                    alertify.error('Esta cedula no existe');
                    $("#Editarcedula_"+numero).focus();
                    $("#spanHelblok").html('Identificación no registrada');
                    $("#divInputCedula_"+numero).removeClass( "has-warning has-success" ).addClass("has-error");
                }else{
                    var formato = [];
                    formato['nuevaeps'] = '<div class="box-body"> \
                        <div class="col-md-12 col-sm-12"> \
                            <div class="row h"> \
                                <div class="col-md-8 col-xs-8 col-sm-8"><h3>Formato de Solicitud y <br />Notificación deTranscripción <br />para Incapacidad o Licencia</h3> <i class="small">(Favor diligenciar los siguientes datos con letra clara y legible)</i></div> \
                                <div class="col-md-4 col-xs-4 col-sm-4"><img src="vistas/img/plantilla/nuevaeps.png" /></div> \
                            </div> \
                            <div class="row"> \
                                <div class="col-md-12 col-xs-12 col-sm-12"><div class="box-light p-6 mb-2">Espacio para radicado.</div></div> \
                            </div> \
                            <div class="row"> \
                                <div class="col-md-12 col-xs-12 col-sm-12"> \
                                    <div class="panel panel-default"> \
                                        <div class="panel-heading">Datos Remitente</div> \
                                        <div class="panel-body"> \
                                            <div class="col-md-6 col-xs-6 col-sm-6"><div class="box-light p-3 mb-2 mr-2"><b>No. Identificación:</b> '+respuesta['emp_nit']+'</div></div> \
                                            <div class="col-md-6 col-xs-6 col-sm-6"><div class="box-light p-3 mb-2"><b>Tipo Identificación:</b> nit</div></div> \
                                            <div class="col-md-12 col-xs-12 col-sm-12"><div class="box-light p-3 mb-2"><b>Nombre:</b> '+respuesta['emp_nombre']+'</div></div> \
                                            <div class="col-md-12 col-xs-12 col-sm-12"><div class="box-light p-3 mb-2"><b>Direccion:</b> '+respuesta['emp_direccion']+'</div></div> \
                                            <div class="col-md-6 col-xs-6 col-sm-6"><div class="box-light p-3 mb-2 mr-2"><b>Teléfono Fijo:</b> '+respuesta['emp_telefono']+'</div></div> \
                                            <div class="col-md-6 col-xs-6 col-sm-6"><div class="box-light p-3 mb-2"><b>Teléfono Celular:</b> '+respuesta['emp_celular']+'</div></div> \
                                            <div class="col-md-6 col-xs-12"><div class="box-light p-3 mb-2"><b>Teléfono trabajo:</b> '+respuesta['emp_celular_tesoreria']+'</div></div> \
                                            <div class="col-md-6 col-xs-12"><div class="box-light p-3 mb-2"><b>Extensión:</b> '+respuesta['emp_telefono_gestion']+'</div></div> \
                                            <div class="col-md-12 col-xs-12"><div class="box-light p-3 mb-2"><b>Correo Electrónico:</b> '+respuesta['emp_email']+'</div></div> \
                                            <div class="col-md-12 col-xs-12"><div class="box-light p-3 mb-2"><b>Observaciones Asesor OAA:</b> </div></div> \
                                            <div class="col-md-12 col-xs-12 center"><i class="small">Autorizo emitir respuesta de acuerdo a los datos consignados en el presente formato. Ley 1581/2012</i></div> \
                                            <div class="col-md-12 col-xs-12 col-sm-12"> \
                                                <div class="p-3 mb-2"><p><b>Respetado(a) señor(a):</b> Reciba un cordial saludo en nombre de NUEVA EPS S.A. Agradecemos \
                                                su confianza al exponernos sus inquietudes.</p> \
                                                <p>Su solicitud de transcripción de incapacidad o licencia se encuentra en trámite y tiene una \
                                                duración de tres días hábiles de acuerdo a la “RESOLUCION 2266 DE 1998 Art. 23”; por lo cual \
                                                realizamos entrega de sus documentos originales, lo cuales deberá custodiar en su poder.</p> \
                                                <p>Recibirá información de su trámite vía mensaje de texto o correo electrónico y podra descargar e \
                                                imprimir su incapacidad a través de NUEVA EPS en Linea en nuestra página web \
                                                www.nuevaeps.com.co, a traves de la ruta:</p> \
                                                <p>* Transacciones NUEVA EPS en linea: Servicios en linea/empleador/ certificado de incapacidad \
                                                * Transacciones NUEVA EPS en linea: Servicios en linea/afiliado POS/ certificado de incapacidad</p> \
                                                <p><i class="small">NUEVA EPS S.A., se reserva el derecho de solicitar ampliación de información en caso de requerirlo, así como de \
                                                transcribir correctamente la incapacidad, con base en la historia clínica y normatividad vigente.</i><p> \
                                                </div> \
                                            </div> \
                                        </div> \
                                    </div> \
                                </div> \
                            </div> \
                            <div class="row"> \
                                <div class="col-md-12 col-xs-12 col-sm-12"></div> \
                            </div> \
                        </div> \
                    </div>';
                    formato['otra'] = '<div class="box-body"> \
                        <div class="col-md-12 col-sm-12"> \
                            Formato no existe \
                        </div> \
                    </div>';
                    $(".formato").html(formato[usuario])
                }
            }

        })
    });

    $("#filas").on("click", function () {   
        console.log(event.target.closest(".row")) 
        event.target != this && $(".formato .row:nth-child(4n) .col-md-12").append(event.target.closest(".row"))
    }),$("#btnPrint").on("click", function () {
        $('.formato').pdfExport({
            importStyle: !0,
            removeInline: !0,
            canvas: !0,
            removeScripts: !0,
            copyTagClasses: !0
        });
    }),$('#cmd').click(function () {
        var e = document.getElementsByClassName('formato')[0];
        html2pdf(e, {
                        margin: 0,
                        filename: 'archivo.pdf',
                        image: { type: 'jpeg', quality: 1 },
                        html2canvas: { scale: 2, logging: !0 },
                        jsPDF: { unit: 'pt', format: [450, 752] }
        });
    });
</script>
