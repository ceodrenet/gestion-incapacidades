<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<?php 

    $empleadosActivos= ControladorEmpleados::getTotalEmpleadosByEmpresayTipo($_SESSION['cliente_id'], 'AND emd_estado = 1');
    /*Empleados*/
    $totalEmpleados = ControladorEmpleados::getTotalEmpleadosByEmpresayTipo($_SESSION['cliente_id'], "AND emd_tipo_empleado = 'EMPLEADO' AND emd_estado = 1 ");

    /*Practicantes*/
    $totalAprendices = ControladorEmpleados::getTotalEmpleadosByEmpresayTipo($_SESSION['cliente_id'], "AND emd_tipo_empleado = 'APRENDIZ' AND emd_estado = 1");
    /*Practicantes*/
    $totalPensionados = ControladorEmpleados::getTotalEmpleadosByEmpresayTipo($_SESSION['cliente_id'], "AND emd_tipo_empleado = 'PENSIONADO' AND emd_estado = 1 ");

    $porcentajeEmple = 0;
    $porcentajeApren = 0;
    $porcentajePensi = 0;

    if($empleadosActivos > 0){
        $porcentajeEmple = ($totalEmpleados * 100 )/ $empleadosActivos;
        $porcentajeApren = ($totalAprendices * 100 )/ $empleadosActivos;
        $porcentajePensi = ($totalPensionados * 100 )/ $empleadosActivos;
    }
    
?>
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">COLABORADORES - EMPRESA <?php echo mb_strtoupper(ModeloIncapacidades::getDatos('gi_empresa', 'emp_id', $_SESSION['cliente_id'])['emp_nombre']); ?></h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Colaboradores</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row" id="FieldsFilters">
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">ACTIVOS</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php 
                            echo $empleadosActivos; ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger">100 %</span>
                    <span class="ms-1 text-muted font-size-13">TOTAL EMPLEADOS</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">COLABORADORES</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo $totalEmpleados; ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger"><?php echo number_format($porcentajeEmple);?> %</span>
                    <span class="ms-1 text-muted font-size-13">PORCENTAJE ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col-->

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">APRENDICES</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo $totalAprendices; ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-success text-success"><?php echo number_format($porcentajeApren);?> %</span>
                    <span class="ms-1 text-muted font-size-13">PORCENTAJE ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">PENSIONADOS</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo number_format($totalPensionados); ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-success text-success"><?php echo $porcentajePensi;?> %</span>
                    <span class="ms-1 text-muted font-size-13">PORCENTAJE ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->
</div><!-- end row-->
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    <div class="row">
                        <div class="col-lg-11">
                            
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">

                    
                                    <!--<li class="divider"></li>
                                    <li><a href="#" title="Exportar datos a Excel" id="ExportarEmpleadosInc">EXP. INCAPACITADOS</a></li>-->

                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <a class="dropdown-item" title="Agregar Colaborador" data-bs-toggle="modal" data-bs-target="#modalAgregarEmpleado" href="#">AGREGAR</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" title="Agregar Colaboradores Masivamente"  data-bs-toggle="modal" data-bs-target="#modalAgregarEmpleados" href="#">AGREGAR MASIVO</a>
                                    <?php } ?>
                                                                
                                    <?php if($_SESSION['edita'] == 1){ ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#" title="Editar IBC Colaboradores"  data-bs-toggle="modal" data-bs-target="#modalEditarIblEmpleados">EDITAR MASIVO</a>
                                    <?php } ?>

                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="index.php?exportar=empleados" target="_blank" title="Exportar datos a Excel">EXPORTAR</a></li>
                                    <div class="dropdown-divider"></div>

                                </div>
                            </div> 
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
                <input type="hidden" name="session" id="session" value="<?php if(isset($_SESSION['cliente_id'])){ echo $_SESSION['cliente_id']; } else { echo 0; } ?>">
                <input type="hidden" id="editar" value="<?php echo $_SESSION['edita'];?>">
                <input type="hidden" id="elimina" value="<?php echo $_SESSION['elimina'];?>">

                <table id="tablaEmpleados" style="width:100%;" class="table table-bordered dt-responsive nowrap w-100">  
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Cédula</th>
                            <th style="width: 15%;">Administradora</th>
                            <th>F. Ingreso</th>
                            <th>Estado</th>
                            <th>Tipo Empleado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nombre</th>
                            <th>Cédula</th>
                            <th style="width: 15%;">Administradora</th>
                            <th>F. Ingreso</th>
                            <th>Estado</th>
                            <th>Tipo Empleado</th>
                            <th>Acciones</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Modal agregar usuario -->
<div id="modalAgregarEmpleado" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" id="formGuardarEmpleados" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar Colaborador</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Tipo Identificación</label>
                                    <select name="NuevoTipoIdentificacion" id="NuevoTipoIdentificacion" class="form-control">
                                        <option value="CC">Cédula de Ciudadania</option>
                                        <option value="CE">Cédula de Extranjeria</option>
                                        <option value="TI">Tarjeta de Identidad</option>
                                        <option value="PA">Pasaporte</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Identificación</label>
                                    <input class="form-control" type="text" name="NuevoCedula" id="NuevoCedula" placeholder="Ingresar Identificación" required="true">
                                </div>
                            </div>      
                        
                            <div class="col-md-6 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Nombre</label>
                                    <input class="form-control" type="text" name="NuevoNombre" placeholder="Ingresar Nombre" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                <label class="form-label">Ingresar Fecha Nacimiento</label>
                                 <div class="mb-3">
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaNacimiento" id="NuevoFechaNacimiento" placeholder="Ingresar Fecha Nacimiento" required="true">
                                </div>
                            </div>

                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Fecha Ingreso</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaIngreso" id="NuevoFechaIngreso" placeholder="Ingresar Fecha Ingreso" required="true">
                                </div>
                            </div>
                        
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Fecha Retiro</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaRetiro" id="NuevoFechaRetiro" placeholder="Ingresar Fecha Retiro">
                                </div>
                            </div>

                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Telefono</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoNumeroTelefonico" id="NuevoNumeroTelefonico" placeholder="Ingresar Numero Telefono">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Salario</label>
                                    <input class="form-control numerico" type="text" name="NuevoSalario" id="NuevoSalario" placeholder="Ingresar Salario" required="true">
                                </div>
                            </div>
                        
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Salario Promedio</label>
                                    <input class="form-control numerico" type="text" name="NuevoSalarioPromedio" id="NuevoSalarioPromedio" placeholder="Ingresar Salario Promedio" >
                                </div>
                            </div>

                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Cargo</label>
                                    <input class="form-control" type="text" name="NuevoCargo" id="NuevoCargo" placeholder="Ingresar Cargo">
                                </div>
                            </div>

                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Correo Electronico</label>
                                    <input class="form-control" type="text" name="NuevoCorreo" id="NuevoCorreo" placeholder="Ingresar Correo Electronico">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Sede</label>
                                    <input class="form-control" type="text" name="NuevoSede" id="NuevoSede" placeholder="Ingresar Sede">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Codigo de Nomina</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoCodigoNomina" id="NuevoCodigoNomina" placeholder="Ingresar Codigo de nomina">
                                </div> 
                            </div>
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Centro de Costos</label>
                                    <input class="form-control" type="text" name="NuevoCentroDecostos" id="NuevoCentroDecostos" placeholder="Centro de costos">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Sub Centro de Costos</label>
                                    <input class="form-control" type="text" name="NuevoSubcentroCostos" id="NuevoSubcentroCostos" placeholder="Sub Centro de costos">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Ciudad Nomina</label>
                                    <input class="form-control" type="text" name="NuevoCiudadNomina" id="NuevoCiudadNomina" placeholder="Ingresar Ciudad">
                                </div>
                            </div>

                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Estado Empleado</label>
                                    <select name="NuevoEstado" id="NuevoEstado" class="form-control">
                                        <option value="1">Activo</option>
                                        <option value="2">Retirado</option>
                                    </select>
                                </div>
                            </div>
                        
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Genero</label>
                                    <select name="NuevoGenero" id="NuevoGenero" class="form-control">
                                        <option value="FEMENINO">Femenino</option>
                                        <option value="MASCULINO">Masculino</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Tipo de Trabajador</label>
                                    <select name="NuevoTipoEmpleado" id="NuevoTipoEmpleado" class="form-control">
                                        <option value="APRENDIZ">Aprendiz</option>
                                        <option value="EMPLEADO">Empleado</option>
                                        <option value="PENSIONADO">Pensionado</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Seleccionar Administradora EPS</label>
                                    <select class="form-control" required="true" id="NuevoEps" name="NuevoEps">
                                        <option value="0">Selecionar Administradora</option>
                                        <?php
                                            $item = 'ips_tipo';
                                            $valor = 'EPS';
                                            $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                            foreach ($IpsAfiliada as $key => $value) {
                                                echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                
                            </div>
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Fecha Ingreso EPS</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaAfiliacionEps" id="NuevoFechaAfiliacionEps" placeholder="Ingresar Fecha Ingreso EPS" >
                                </div>
                            </div>

                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Seleccionar Administradora AFP</label>
                                    <select class="form-control" required="true" id="NuevoAfp" name="NuevoAfp">
                                        <option value="0">Selecionar AFP</option>
                                        <?php
                                            $item = 'ips_tipo';
                                            $valor = 'AFP';
                                            $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                            foreach ($IpsAfiliada as $key => $value) {
                                                echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                            }
                                        ?>
                                    </select>   
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Seleccionar Administradora ARL</label>
                                    <select class="form-control" required="true" id="NuevoArl" name="NuevoArl">
                                        <option value="0">Selecionar ARL</option>
                                        <?php
                                            $item = 'ips_tipo';
                                            $valor = 'ARL';
                                            $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                            foreach ($IpsAfiliada as $key => $value) {
                                                echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Fecha Calificación PCL</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaCalificacionPCL" id="NuevoFechaCalificacionPCL" placeholder="Ingresar Fecha Calificación PCL">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Entidad Calificadora PCL</label>
                                    <input class="form-control" type="text" name="NuevoEntidadCalificadora" id="NuevoEntidadCalificadora" placeholder="Ingresar Entidad Calificadora PCL">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Diagnostico</label>
                                    <input class="form-control" type="text" name="NuevoDiagnostico" id="NuevoDiagnosticoX" placeholder="Ingresar Diagnostico">
                                </div>
                            </div>
                        </div>

                        <!-- Casos especiales -->

                        <div class="row">
                            <div class="col-md-3">
                                <div class="mb-3">
                                    <label class="form-label">
                                        <input type="checkbox" name="NuevoCasosEspeciales" id="NuevoCasosEspeciales" value="1"> Casos especiales
                                    </label>    
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="mb-3">
                                    <label class="form-label">
                                        <input type="checkbox" name="NuevoSuspSalario" id="NuevoSuspSalario" value="1"> Suspencion de Salario
                                    </label>  
                                </div>  
                            </div>

                            <div class="col-md-3" id="txtfechaSusp" style="display: none;">
                                <div class="mb-3">
                                    <label class="form-label"> Fecha de Suspencion de Salario</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaSusp" id="NuevoFechaSusp">
                                </div>
                            </div>
                            
                        </div>

                        <div class="row casosEspeciales" style="display: none;" >
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Concepto Rehabilitación</label>
                                    <select name="NuevoConceptoRehabilitacion" id="NuevoConceptoRehabilitacion" class="form-control" placeholder="Concepto Rehabilitación">
                                        <option value="DESFAVORABLE">Desfavorable</option>
                                        <option value="FAVORABLE">Favorable</option>
                                        <option value="NINGUNO">Ninguno</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Fecha Concepto</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaConcepto" id="NuevoFechaConcepto" placeholder="Fecha Concepto">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Diagnostico</label>
                                    <input class="form-control" type="text" name="NuevoDiagnosticoConcepto" id="NuevoDiagnosticoConcepto" placeholder="Ingresar Diagnostico">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Entidad</label>
                                    <select name="NuevoEntidadConcepto" id="NuevoEntidadConcepto" class="form-control select2" placeholder="Ingresar Entidad">
                                        <option  value="0">Entidad</option>
                                        <?php
                                            $item = 'ips_tipo';
                                            $valor = 'EPS';
                                            $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                            foreach ($IpsAfiliada as $key => $value) {
                                                echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--casos especiales-->
                        <div class="row casosEspeciales" style="display: none;">
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Calificación PCL</label>
                                    <input class="form-control" type="text" name="NuevoCalificacionPCLCS" id="NuevoCalificacionPCLCS" placeholder="Ingresar Calificación">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Fecha Calificación</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaCaliciacion" id="NuevoFechaCaliciacion" placeholder="Fecha Calificación">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Diagnostico</label>
                                    <input class="form-control" type="text" name="NuevoDiagnosticoPCL" id="NuevoDiagnosticoPCL" placeholder="Ingresar Diagnostico">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Entidad</label>
                                    <select name="NuevoEntidadPCL" id="NuevoEntidadPCL" class="form-control select2" placeholder="Ingresar Entidad">
                                        <option  value="0">Entidad</option>
                                        <?php
                                            $item = 'ips_tipo';
                                            $valor = 'EPS';
                                            $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                            foreach ($IpsAfiliada as $key => $value) {
                                                echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row casosEspeciales" style="display: none;">
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Calificación Junta Regional</label>
                                    <input class="form-control" type="text" name="NuevoJuntaRegional" id="NuevoJuntaRegional" placeholder="Ingresar Calificación Junta Regional">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Fecha Calificación</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaCalificacionJR" id="NuevoFechaCalificacionJR" placeholder="Fecha Calificación">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Diagnostico</label>
                                    <input class="form-control" type="text" name="NuevoDiagnosticoJR" id="NuevoDiagnosticoJR" placeholder="Ingresar Diagnostico">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Entidad</label>
                                    <input type="text" name="NuevoEntidadJR" id="NuevoEntidadJR" class="form-control select2" placeholder="Ingresar Entidad">
                                </div>
                            </div>
                        </div>

                        <div class="row casosEspeciales" style="display: none;">
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Calificación Junta Nacional</label>
                                    <input class="form-control" type="text" name="NuevoCalificacionJN" id="NuevoCalificacionJN" placeholder="Ingresar Calificación Junta Nacional">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Fecha Calificación</label>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaCalificacionJN" id="NuevoFechaCalificacionJN" placeholder="Fecha Calificación">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Diagnostico</label>
                                    <input class="form-control" type="text" name="NuevoDiagnosticoJN" id="NuevoDiagnosticoJN" placeholder="Ingresar Diagnostico">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Entidad</label>
                                    <input type="text" name="NuevoEntidadJN" id="NuevoEntidadJN" class="form-control" placeholder="Ingresar Entidad">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row casosEspeciales" style="display: none;">
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Tipo de Caso</label>
                                    <select name="NuevoTipoDecaso" id="NuevoTipoDecaso" class="form-control" placeholder="Tipo de Caso">
                                    <!--<option value="1">Seleccione</option>-->
                                        <option value="1">Caso Especial</option>
                                        <option value="2">Incapacitado Permanente</option>
                                        <option value="3">Ambos Casos</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- Casos especiales -->

                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Cédula empleado</label>
                                    <input type="file" class="form-control NuevaFoto" valor='0' name="NuevoCedulaEmpleado">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar pull-right">
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Otro Documento</label>
                                    <input type="file" class="form-control NuevaFoto" valor='_2' name="NuevoOtroDocumento">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2 pull-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                    <input type="hidden" name="Nuevo_idEmpresa" id="Nuevo_idEmpresa" value="<?php echo $_SESSION['cliente_id'];?>">
                    <button type="button" id="btnGuardarEmpleados" class="btn btn-primary">Guardar Empleado</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Modal agregar usuario -->
<div id="modalEditarEmpleado" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
         <!--Desde aqui-->
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" id="formEditarEmpleados" autocomplete="off" method="post" enctype="multipart/form-data">          
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Editar Colaborador</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>  
                </div>
                <div class="modal-body">
                    <div class="box-body">
                    <div class="row">
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Tipo de identificación</label>
                                    <select name="EditarTipoIdentificacion" id="EditarTipoIdentificacion" class="form-control">
                                        <option >Seleccione tipo Identificacion</option>
                                        <option value="CC">Cédula de Ciudadania</option>
                                        <option value="CE">Cédula de Extranjeria</option>
                                        <option value="TI">Tarjeta de Identidad</option>
                                        <option value="PA">Pasaporte</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Identificación</label>
                                    <input class="form-control" type="text" name="EditarCedula" id="EditarCedula" placeholder="Ingresar Identificación" required="true">
                                </div>
                            </div>     
                        
                            <div class="col-md-6 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Nombre</label>
                                    <input class="form-control" type="text" name="EditarNombre" id="EditarNombre" placeholder="Ingresar Nombre" required="true">
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Fecha Nacimiento</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaNacimiento" id="EditarFechaNacimiento" placeholder="Ingresar Fecha Nacimiento" required="true"> 
                                </div>
                            </div>
                
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Fecha Ingreso</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaIngreso" id="EditarFechaIngreso" placeholder="Ingresar Fecha Ingreso" required="true">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Ingresar Fecha Retiro</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaRetiro" id="EditarFechaRetiro" placeholder="Ingresar Fecha Retiro">
                                </div>
                            </div>
                             <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Telefono</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarTelefono" id="EditarTelefono" placeholder="Ingresar Numero Telefonico"> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Ingresar Salario</label>
                                    <input class="form-control numerico" type="text" name="EditarSalario" id="EditarSalario" placeholder="Ingresar Salario" required="true">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Ingresar Salario Promedio</label>
                                    <input class="form-control numerico" type="text" name="EditarSalarioPromedio" id="EditarSalarioPromedio" placeholder="Ingresar Salario Promedio" >
                                </div>
                            </div>
                     
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Cargo</label>
                                    <input class="form-control" type="text" name="EditarCargo" id="EditarCargo" placeholder="Editar Cargo">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="mb-3">
                                   <label class="form-label">Ingresar correo</label>
                                   <input class="form-control" type="text" name="EditarCorreo" id="EditarCorreo" placeholder="Ingresar Correo Electronico">
                               </div>
                           </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Sede</label>
                                    <input class="form-control" type="text" name="EditarSede" id="EditarSede" placeholder="Editar Sede">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Código de nomina</label>
                                    <input class="form-control" type="text" name="EditarCodigoNomina" id="EditarCodigoNomina" placeholder="Ingresar Código de nomina">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="mb-3">
                                   <label class="form-label">Ingresar Centro de costos</label>
                                   <input class="form-control" type="text" name="EditarCentroDecostos" id="EditarCentroDecostos" placeholder="Centro de costos">
                               </div>
                           </div>
                           <div class="col-md-3 col-xs-12">
                                <div class="mb-3">
                                   <label class="form-label">Ingresar Sub Centro de costos</label>
                                   <input class="form-control" type="text" name="EditarSubcentroCostos" id="EditarSubcentroCostos" placeholder="Sub Centro de costos">
                               </div>
                           </div>
                        </div>
                          
                        
                        <div class="row">
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Ciudad Nomina</label>
                                    <input class="form-control" type="text" name="EditarCiudadNomina" id="EditarCiudadNomina" placeholder="Ingresar Ciudad">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Estado Empleado</label>
                                    <select name="EditarEstado" id="EditarEstado" class="form-control">
                                        <option value="1">Activo</option>
                                        <option value="2">Retirado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="mb-3">
                                   <label class="form-label">Ingresar Genero</label>
                                   <select name="EditarGenero" id="EditarGenero" class="form-control">
                                       <option value="FEMENINO">Femenino</option>
                                       <option value="MASCULINO">Masculino</option>
                                   </select>
                               </div>
                           </div>
                           <div class="col-md-3 col-xs-12">
                            <div class="mb-3">
                               <label class="form-label">Ingresar Tipo de empleado</label>
                               <select name="EditarTipoEmpleado" id="EditarTipoEmpleado" class="form-control">
                                   <option value="APRENDIZ">Aprendiz</option>
                                   <option value="EMPLEADO">Empleado</option>
                                   <option value="PENSIONADO">Pensionado</option>
                               </select>
                           </div>
                       </div>
                    </div>
                    <div class="row">
                     <div class="col-md-3 col-xs-12">
                                <div class="mb-3">
                                   <label class="form-label">Ingresar Administradora EPS</label>
                                   <select class="form-control" id="EditarEps" required="true" name="EditarEps">
                                       <option  value="0">Selecionar Administradora</option>
                                       <?php
                                           $item = 'ips_tipo';
                                           $valor = 'EPS';
                                           $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                           foreach ($IpsAfiliada as $key => $value) {
                                               echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                           }
                                       ?>
                                   </select>
                               </div>
                           </div>       
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Fecha Ingreso EPS</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaAfiliacionEps" id="EditarFechaAfiliacionEps" placeholder="Ingresar Fecha Ingreso EPS" > 
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="mb-3">
                                   <label class="form-label">Ingresar Administradora AFP</label>
                                   <select class="form-control" id="EditarAfp" required="true" name="EditarAfp">
                                       <option  value="0">Selecionar AFP</option>
                                       <?php
                                           $item = 'ips_tipo';
                                           $valor = 'AFP';
                                           $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                           foreach ($IpsAfiliada as $key => $value) {
                                               echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                           }
                                       ?>
                                   </select> 
                               </div>
                           </div>
                           <div class="col-md-3 col-xs-12">
                            <div class="mb-3">
                               <label class="form-label">Ingresar Administradora ARL</label>
                               <select class="form-control" id="EditarArl" required="true"  name="EditarArl">
                                   <option  value="0">Selecionar ARL</option>
                                   <?php
                                       $item = 'ips_tipo';
                                       $valor = 'ARL';
                                       $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                       foreach ($IpsAfiliada as $key => $value) {
                                           echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                       }
                                   ?>
                               </select>
                           </div>
                       </div>
                    </div>
                         
                        <div class="row panel">
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Fecha Calificacion PCL</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaCalificacionPCL" id="EditarFechaCalificacionPCL" placeholder="Ingresar Fecha Calificacion PCL"> 
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Entidad Calificadora PCL</label>
                                    <input class="form-control" type="text" name="EditarEntidadCalificadora" id="EditarEntidadCalificadora" placeholder="Ingresar Entidad Calificadora PCL">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Ingresar Diagnostico</label>
                                    <input class="form-control" type="text" name="EditarDiagnostico" id="EditarDiagnostico" placeholder="Ingresar Diagnostico">
                                </div>
                            </div>
                        </div>

                        <!-- Casos especiales -->

                        <div class="row panel">
                            <div class="col-md-3">
                                <label class="form-label">
                                    <input type="checkbox" name="EditarCasosEspeciales" id="EditarCasosEspeciales" value="1"> Casos especiales
                                </label>    
                            </div>

                            <div class="col-md-3">
                                <div class="mb-3">
                                    <label class="form-label">
                                        <input type="checkbox" name="EditarSuspSalario" id="EditarSuspSalario" value="1"> Suspencion de Salario
                                    </label>  
                                </div>  
                            </div>

                            <div class="col-md-3" id="txtfechaSusp_editar" style="display: none;">
                                <div class="mb-3">
                                    <label class="form-label"> Fecha de Suspencion de Salario</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaSusp" id="EditarFechaSusp">
                                </div>
                            </div>
                        </div>

                        <div class="row casosEspeciales" style="display: none;" >
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Concepto Rehabilitación</label>
                                    <select name="EditarConceptoRehabilitacion" id="EditarConceptoRehabilitacion" class="form-control" placeholder="Concepto Rehabilitación">
                                        <option value="DESFAVORABLE">Desfavorable</option>
                                        <option value="FAVORABLE">Favorable</option>
                                        <option value="NINGUNO">Ninguno</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Fecha Concepto</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaConcepto" id="EditarFechaConcepto" placeholder="Fecha Concepto">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Diagnostico</label>
                                    <input class="form-control" type="text" name="EditarDiagnosticoConcepto" id="EditarDiagnosticoConcepto" placeholder="Ingresar Diagnostico">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="mb-3">
                                    <label class="form-label">Entidad</label>
                                    <select name="EditarEntidadConcepto" id="EditarEntidadConcepto" class="form-control select2" placeholder="Ingresar Entidad">
                                        <option  value="0">Entidad</option>
                                        <?php
                                            $item = 'ips_tipo';
                                            $valor = 'EPS';
                                            $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                            foreach ($IpsAfiliada as $key => $value) {
                                                echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row casosEspeciales" style="display: none;">
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Calificación PCL</label>
                                    <input class="form-control" type="text" name="EditarCalificacionPCLCS" id="EditarCalificacionPCLCS" placeholder="Ingresar Calificación">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Fecha Calificación</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaCaliciacion" id="EditarFechaCaliciacion" placeholder="Fecha Calificación">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Diagnostico</label>
                                    <input class="form-control" type="text" name="EditarDiagnosticoPCL" id="EditarDiagnosticoPCL" placeholder="Ingresar Diagnostico">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="mb-3">
                                    <label class="form-label">Entidad</label>
                                    <select name="EditarEntidadPCL" id="EditarEntidadPCL" class="form-control select2" placeholder="Ingresar Entidad">
                                        <option  value="0">Entidad</option>
                                        <?php
                                            $item = 'ips_tipo';
                                            $valor = 'EPS';
                                            $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                            foreach ($IpsAfiliada as $key => $value) {
                                                echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row casosEspeciales" style="display: none;">
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Calificación Junta Regional</label>
                                    <input class="form-control" type="text" name="EditarJuntaRegional" id="EditarJuntaRegional" placeholder="Ingresar Calificación Junta Regional">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Fecha Calificación</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaCalificacionJR" id="EditarFechaCalificacionJR" placeholder="Fecha Calificación">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="mb-3">
                                    <label class="form-label">Diagnostico</label>
                                    <input class="form-control" type="text" name="EditarDiagnosticoJR" id="EditarDiagnosticoJR" placeholder="Ingresar Diagnostico">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="mb-3">
                                    <label class="form-label">Entidad</label>
                                    <input type="text" name="EditarEntidadJR" id="EditarEntidadJR" class="form-control select2" placeholder="Ingresar Entidad">
                                </div>
                            </div>
                        </div>

                        <div class="row casosEspeciales" style="display: none;">
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Calificación Junta Nacional</label>
                                    <input class="form-control" type="text" name="EditarCalificacionJN" id="EditarCalificacionJN" placeholder="Ingresar Calificación Junta Nacional">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Fecha Calificación</label>
                                    <input class="form-control flatpickr-input" type="text" name="EditarFechaCalificacionJN" id="EditarFechaCalificacionJN" placeholder="Fecha Calificación">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Diagnostico</label>
                                    <input class="form-control" type="text" name="EditarDiagnosticoJN" id="EditarDiagnosticoJN" placeholder="Ingresar Diagnostico">
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Entidad</label>
                                    <input type="text" name="EditarEntidadJN" id="EditarEntidadJN" class="form-control" placeholder="Ingresar Entidad">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row casosEspeciales" style="display: none;">
                            <div class="col-md-3 col-xs-6">
                                 <div class="mb-3">
                                    <label class="form-label">Tipo de Caso</label>
                                    <select name="NuevoTipoDecasoEdicion" id="NuevoTipoDecasoEdicion" class="form-control" placeholder="Tipo de Caso">
                                          <!--<option value="1">Seleccione</option>-->
                                          <option value="1">Caso Especial</option>
                                        <option value="2">Incapacitado Permanente</option>
                                        <option value="3">Ambos Casos</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- Casos especiales -->

                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Cédula empleado</label>
                                    <input type="file" class="form-control NuevaFoto" valor='0' name="EditarCedulaEmpleado">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar pull-right">
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                 <div class="mb-3">
                                    <label class="form-label">Otro Documento</label>
                                    <input type="file" class="form-control NuevaFoto" valor='_2' name="EditarOtroDocumento">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2 pull-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                 <!-- Hasta aqui-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                    <input type="hidden" name="Editar_idEmpleado" id="Editar_idEmpleado" >
                    <input type="hidden" name="Editar_idEmpresa" id="Editar_idEmpresa" value="<?php echo $empresa;?>" >
                    <input type="hidden" name="Editar_NombreEmpresa" id="Editar_NombreEmpresa" value="<?php echo $respuesta['emp_nombre'];?>"  >
                    <input type="hidden" name="ruta_cedula" id="ruta_cedula" >
                    <input type="hidden" name="ruta_otroDocumento" id="ruta_otroDocumento" >
        
                    <?php if($_SESSION['edita'] == 1){ ?>
                    <button type="button" id="btnEditarEmpleados" class="btn btn-primary">Guardar Empleado</button>
                    <?php } ?>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->
                  


<!-- Modal agregar usuario -->
<div id="modalDocumentosEmpleado" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered"  role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Documentos del Colaborador</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>  
                </div>
                <div class="modal-body">
                   <div class="row panel" id="documentosEmpleados">
                        <div class="col-md-6 col-xs-12">
                             <div class="mb-3">
                                <label class="form-label">Cédula Colaborador</label>
                                <a id="cedula" target='_blank'>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_M">
                                </a>
                            </div>
                        </div>

                        <div class="col-md-6 col-xs-12">
                             <div class="mb-3">
                             <label class="form-label">Otro Documento</label>
                                <a id="otro">
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2_M">
                                </a>
                            </div>
                        </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->


<!-- Modal agregar usuario -->
<div id="modalEditarIblEmpleados" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form  method="post" enctype="multipart/form-data" id="formularioIblassivaEmpleados">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Editar Colaboradores Masivamente</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row panel">
                       <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Formato de carga</label>
                                <input type="file" class="NuevoExell" required valor='0' name="IblEmpleados">
                                <p class="help-block">
                                    Peso maximo del archivo 3 MB
                                </p>
                                <p>
                                    Descarga el Formato de edición para colaboradores <a href="vistas/formatos/FC_004.xlsx" download="FC_004.xlsx">aqui</a>
                                </p>
                                <input type="hidden" name="empresa" value="<?php echo $_SESSION['cliente_id'];?>">
                            </div>
                        </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                    <button type="button" id="cargarIblEmpleados" class="btn btn-primary">Editar Colaboradores</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal agregar usuario -->
<div id="modalAgregarEmpleados" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        
            <form role="form" autocomplete="off" method="post" enctype="multipart/form-data" id="formularioCargaMassivaEmpleados">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar Colaboradores Masivamente</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                   <div class="row panel">
                       <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Formato de carga</label>
                                <input type="file" class="NuevoExell" required valor='0' name="NuevoEmpleados">
                                <p class="help-block">
                                    Peso maximo del archivo 3 MB
                                </p>
                                <p>
                                    <div class="mb-3">
                                        <label class="form-label">
                                            <input type="checkbox" name="chekUpdateDatos" id="chekUpdateDatos" value="-1"> Crear Empleados que no esten en el sistema
                                        </label>
                                    </div>
                                </p>
                                <p>
                                    Descarga el Formato de carga para colaboradores <a href="vistas/formatos/FC_001.xlsx" download="FC_001.xlsx">aqui</a>
                                </p>
                                <input type="hidden" name="empresa" value="<?php echo $_SESSION['cliente_id'];?>">
                            </div>
                        </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                    <button type="button" id="cargarDatosEmpleados" class="btn btn-primary">Guardar Colaboradores</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>
<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/empleados.js?v=<?php echo rand();?>"></script>