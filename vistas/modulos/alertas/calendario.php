<link rel="stylesheet" href="vistas/bower_components/fullcalendar/dist/fullcalendar.min.css">
<link rel="stylesheet" href="vistas/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
<link rel="stylesheet" href="vistas/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Calendario  
            <small>Eventos registrados en el sistema</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Calendario</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger box-solid">
                    <div class="box-body no-padding">
                        <!-- THE CALENDAR -->
                        <div id="calendar"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
        </div>
    </section>
</div>
<div id="ModalAgregarEvento" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header" id="title_opciones_avanzadas" style="background: #d73925;color: white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="nombre_campo_h4">Nuevo evento GEIN</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Asunto</label>
                                <input type="text" name="txtNombreEvento" id="txtNombreEvento" required="true" class="form-control" placeholder="Asunto del evento">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fecha Inicio</label>
                                <input type="text" name="txtFechaInicioEventoT" required="true" id="txtFechaInicioEventoT" class="form-control" placeholder="Fecha Inicio">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label>Hora Inicio Evento</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control timepicker" required="true" placeholder="Hora Inicio Evento" name="txtHoraInicioEvento" id="txtHoraInicioEvento" >
                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="checkboxTodoElDia" id="checkboxTodoElDia" value="-1">&nbsp;Evento de todo el día
                                </label>     
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fecha Final</label>
                                <input type="text" name="txtFechaFinalEventoT" id="txtFechaFinalEventoT" class="form-control" placeholder="Fecha Final">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label>Hora Fin Evento</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control timepicker" placeholder="Hora Fin Evento" name="txtHoraFinalEvento" id="txtHoraFinalEvento" >
                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Descripción del evento</label>
                                <textarea name="txtDecripcionEvento" id="txtDecripcionEvento" class="form-control" placeholder="Descripción del evento"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Invitar personas al evento</label>
                                <input name="txtPersonasventos" id="txtPersonasventos" class="form-control" placeholder="Añadir correos, separados por comas">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="txtFechaInicioEvento" id="txtFechaInicioEvento" value="0">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-danger" id="saveEvento">Guardar</button>
                    <?php
                        $crearEvento = new ControladorCalendario();
                        $crearEvento->ctrCrearEvento();
                    ?>
                </div>
            </div>  
        </form>
        
        <!-- /.modal-content -->
    </div>
</div>

<div id="ModalEditarEvento" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form method="post">
            <div class="modal-content">
                <div class="modal-header" id="title_opciones_avanzadas" style="background: #d73925;color: white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="nombre_campo_h4">Editar evento GEIN</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Asunto</label>
                                <input type="text" name="txtEditarNombreEvento" id="txtEditarNombreEvento" required="true" class="form-control" placeholder="Asunto del evento">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fecha Inicio</label>
                                <input type="text" name="txtEditarFechaInicioEventoT" required="true" id="txtEditarFechaInicioEventoT" class="form-control" placeholder="Fecha Inicio">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label>Hora Inicio Evento</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control timepicker" required="true" placeholder="Hora Inicio Evento" name="txtEditarHoraInicioEvento" id="txtEditarHoraInicioEvento" >
                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="checkboxEditarTodoElDia" id="checkboxEditarTodoElDia" value="-1">&nbsp;Evento de todo el día
                                </label>     
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fecha Final</label>
                                <input type="text" name="txtEditarFechaFinalEventoT" id="txtEditarFechaFinalEventoT" class="form-control" placeholder="Fecha Final">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label>Hora Fin Evento</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control timepicker" placeholder="Hora Fin Evento" name="txtEditarHoraFinalEvento" id="txtEditarHoraFinalEvento" >
                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Descripción del evento</label>
                                <textarea name="txtEditarDecripcionEvento" id="txtEditarDecripcionEvento" class="form-control" placeholder="Descripción del evento"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Invitar personas al evento</label>
                                <input name="txtEditarPersonasventos" id="txtEditarPersonasventos" class="form-control" placeholder="Añadir correos, separados por comas">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" name="txtValorDelEvento" id="txtValorDelEvento" value="0">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="button" class="btn btn-danger" idEvento="" id="eliminarEventoEdicion">Eliminar Evento</button>
                    <button type="submit" class="btn btn-primary" id="saveEventoEdicion">Guardar Evento</button>
                    <?php
                        $crearEvento = new ControladorCalendario();
                        $crearEvento->ctrEditarEvento();
                    ?>
                </div>
            </div>  
        </form>
        
        <!-- /.modal-content -->
    </div>
</div>

<script src="vistas/bower_components/moment/moment.js"></script>
<script src="vistas/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src='vistas/bower_components/fullcalendar/dist/locale-all.js'></script>
<script src="vistas/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script type="text/javascript">
    $(function(){
        var date    = new Date()
        var d       = date.getDate(),
        m           = date.getMonth(),
        y           = date.getFullYear();


        $("#checkboxTodoElDia").on('ifChanged', function(event){
            if($("#checkboxTodoElDia").is(':checked')){
                $('#txtFechaFinalEventoT').attr('readonly', true);
                $("#txtHoraFinalEvento").attr('readonly', true);
            }else{
                $('#txtFechaFinalEventoT').attr('readonly', false);
                $("#txtHoraFinalEvento").attr('readonly', false);
            }
        });

        $("#checkboxTodoElDia").on('unChecked', function(event){
            alert(event.type + ' callback');
        });

        $("#txtFechaInicioEventoT").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (selected) {
            $('#txtFechaFinalEventoT').val('');
            var minDate = new Date(selected.date.valueOf());
            $('#txtFechaFinalEventoT').datepicker('setStartDate', minDate);
        });

        $('#txtFechaFinalEventoT').datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });


        $('.timepicker').timepicker({
            showInputs: false,
            minuteStep: 5,
            showMeridian : false
        });

        $("#eliminarEventoEdicion").click(function(){
            var x = $(this).attr("idEvento");
            swal({
                title: '¿Está seguro de borrar este evento?',
                text: "¡Si no lo está, puede cancelar la accíón!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, borrala!'
            },function(isConfirm) {
                if (isConfirm) {
                    window.location = "index.php?ruta=calendario&idEvento="+x ;
                }
            })
        });


        $('#calendar').fullCalendar({
            header    : {
                left  : 'prev,next today',
                center: 'title',
                right : 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                today: 'Hoy',
                month: 'Mes',
                week : 'Semana',
                day  : 'Día'
            },
            //Random default events
            events    : [
                <?php
                    $item = NULL;
                    $value = NULL;
                    $respuesta = ControladorCalendario::ctrMostrarEventos($item, $value);
                    foreach ($respuesta as $key => $value) {
                        echo '{
                            title          : \''.$value['evn_asunto'].'\',
                            start          : \''.$value['evn_fecha_inicio'].' '.$value['evn_hora_inicio'].'\',';
                        if($value['evn_fecha_final'] != NULL && $value['evn_fecha_final'] != '' && $value['evn_fecha_final'] != '0000-00-00'){
                            echo '
                            end             :  \''.$value['evn_fecha_final'].' '.$value['evn_hora_final'].'\',';
                        }

                        if($value['evn_tododia'] != NULL && $value['evn_tododia'] != '0'){
                            echo '
                            allDay          : true,';
                        }else{
                            echo '
                            allDay          : false,';
                        }

                        echo '
                            backgroundColor : \'#f56954\', //red
                            borderColor     : \'#f56954\',
                            id              : '.$value['evn_id'].'
                        },';
                    }
                ?>
            ],
            editable  : false,
            locale    : 'es',
            allDaySlot : true,
            dayRender: function(date, element, view){
                element.bind('dblclick', function() {
                    $("#ModalAgregarEvento").modal();

                    var m = $.fullCalendar.moment(date);
                    var minDate = m.format();
                    $('#txtFechaInicioEventoT').val(minDate);
                    $('#txtFechaInicioEventoT').datepicker('setStartDate', minDate);
                    $('#txtFechaFinalEventoT').datepicker('setStartDate', minDate)
                });
            },
            eventRender: function(event, element) {
                element.bind('dblclick', function() {
                    //console.log('Jose sisas '+event.id);
                    $("#eliminarEventoEdicion").attr("idEvento", 0);
                    var x = event.id;
                    var datas = new FormData();
                    datas.append('EditarCalendarioId', x);
                    $.ajax({
                        url   : 'ajax/calendario.ajax.php',
                        method: 'post',
                        data  : datas,
                        cache : false,
                        contentType : false,
                        processData : false,
                        dataType    : 'json',
                        success     : function(data){

                            $("#txtEditarNombreEvento").val(data.evn_asunto);
                            $("#txtEditarFechaInicioEventoT").val(data.evn_fecha_inicio);
                            $("#txtEditarHoraInicioEvento").val(data.evn_hora_inicio);

                            if(data.evn_tododia == 1){
                                $("#checkboxEditarTodoElDia").attr('checked', true);
                            }else{
                                $("#checkboxEditarTodoElDia").attr('checked', false);
                            }
                            
                            $("#txtEditarFechaFinalEventoT").val(data.evn_fecha_final);
                            $("#txtEditarHoraFinalEvento").val(data.evn_hora_final);
                            $("#txtEditarDecripcionEvento").val(data.evn_observacion);
                            $("#txtValorDelEvento").val(data.evn_id);
                            $("#eliminarEventoEdicion").attr("idEvento", data.evn_id);
                            $("#ModalEditarEvento").modal();
                        }
                    });
                    
                    /*$('#ModalEdit #id').val(event.id);
                    $('#ModalEdit #title').val(event.title);
                    $('#ModalEdit #color').val(event.color);
                    $('#ModalEdit').modal('show');*/
                });
            }
        });
    })
</script>
<?php 
    $crearEvento = new ControladorCalendario();
    $crearEvento->deleteEvento();
    
?>