<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Colaboradores  
            <small>Con mas de 540 dias de incapacidad</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Incapacidades de mas de 540 dias</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Alerta Incapacidades con mas de 540 Días
                </h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-info btn-sm" title="Exportar incapacidades a excel" href="index.php?exportarAlertas=540">
                        <i class="fa fa-file-excel-o"></i>
                    </a>
                </div>
            </div>
            <div class="box-body">
                <table style="width:100%;" id="tablaClientes" class="table table-bordered table-striped dt-responsive tablas">
                    <thead>
                        <tr>
                            <th style="width: 10px;">#</th>
                            <?php
                                if($_SESSION['cliente_id'] == 0){
                                    echo '<th>Empresa</th>';
                                }
                            ?>
                            <th>Nombre</th>
                            <th>Estado</th>
                            <th>Cedula</th>
                            <th>EPS</th>
                            <th>ARL</th>
                            <th>AFP</th>
                            <th style="width: 20%;">Días de incapacidad</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $campos = 'emp_nombre, emd_cedula, emd_nombre, emd_fecha_ingreso, CASE emd_estado WHEN 1 THEN \'ACTIVO\' WHEN 0 THEN \'RETIRADO\' END AS emd_estado , a.ips_nombre as ips_nombre, b.ips_nombre as emd_arl_id, c.ips_nombre as emd_afp_id, Tiempo_dias';
                            $tablas = 'reporte_alarmas JOIN gi_empleados ON emd_cedula = inc_cc_afiliado LEFT JOIN gi_ips a ON a.ips_id = emd_eps_id LEFT JOIN gi_ips b ON b.ips_id = emd_arl_id LEFT JOIN gi_ips c ON c.ips_id = emd_afp_id JOIN gi_empresa ON emp_id = emd_emp_id';
                            $condiciones  = 'Tiempo_dias > 540';

                            if($_SESSION['cliente_id'] != 0){
                                $condiciones .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
                            }
                            $alarma1 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones, '', '');                          
                            
                            foreach ($alarma1 as $key => $value) {
                                $camposX = "inc_fecha_inicio, inc_fecha_final";
                                $tablasX = "gi_incapacidad";
                                $whereXX = "inc_cc_afiliado = ".$value['emd_cedula']." AND inc_empresa = ".$_SESSION['cliente_id'];
                                $verificar = ModeloTesoreria::mdlMostrarGroupAndOrder($camposX, $tablasX, $whereXX, '', 'ORDER BY inc_fecha_inicio ASC');
                                $valida = false;
                                $dias_transcurridos = 0;
                                $inc_fecha_inicio_Final = ''; 
                                foreach ($verificar as $incapacidades => $incapacidad) {
                                    if($inc_fecha_inicio_Final != ''){
                                        //tebemos que validar que la fecha inicio sea mayor a 30 dias de la final pasada
                                        $dias = 0;
                                        $dias = ControladorIncapacidades::dias_transcurridos( $inc_fecha_inicio_Final, $incapacidad['inc_fecha_inicio']);
                                        if($dias <= 30){
                                            $valida = true;
                                            $dias_transcurridos += $dias;
                                        }
                                    }
                                    $inc_fecha_inicio_Final = $incapacidad['inc_fecha_final'];
                                }
                                if($dias_transcurridos >= 540){
 
                                    echo '<tr>';
                                        echo '<td>'.($key+1).'</td>';
                                        
                                    if($_SESSION['cliente_id'] == 0){
                                        echo '<td>'.$value['emp_nombre'].'</td>';
                                    }
                                
                                        echo '<td>'.$value['emd_nombre'].'</td>';
                                        echo '<td>'.$value['emd_estado'].'</td>';
                                        echo '<td>'.$value['emd_cedula'].'</td>';
                                        echo '<td>'.$value['ips_nombre'].'</td>';
                                        echo '<td>'.$value['emd_arl_id'].'</td>';
                                        echo '<td>'.$value['emd_afp_id'].'</td>';
                                        echo '<td>'.$dias_transcurridos.'</td>';
                                    echo '</tr>';
                                }
                            }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th style="width: 10px;">#</th>
                            <?php
                                if($_SESSION['cliente_id'] == 0){
                                    echo '<th>Empresa</th>';
                                }
                            ?>
                            <th>Nombre</th>
                            <th>Estado</th>
                            <th>Cedula</th>
                            <th>EPS</th>
                            <th>ARL</th>
                            <th>AFP</th>
                            <th style="width: 20%;">Días de incapacidad</th>
                        </tr>
                    </tfoot>
                </table>      
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

