<link rel="stylesheet" href="vistas/bower_components/fullcalendar/dist/fullcalendar.min.css">
<link rel="stylesheet" href="vistas/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
<link rel="stylesheet" href="vistas/plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Actividades Diarias  
            <small>Mi registro de Actividades en el sistema</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Actividades</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">
                            Actividades registradas fecha <?php echo date("d/m/Y");?>
                        </h3>
                        <div class="box-tools">
                            <a href="#" data-toggle="modal" data-target="#modalExportarActividads" class="btn btn-danger">Exportar</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12 table-resposive">
                                <form method="post">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Rango</th>
                                                <th>Empresa</th>
                                                <th>Actividad</th>
                                                <th>Observación</th>
                                            </tr>
                                        </thead>                  
                                        <tbody>
                                            <?php
                                                $rangos = array('07:01 a 08:00', 
                                                    '08:01 a 09:00',
                                                    '09:01 a 10:00',
                                                    '10:01 a 11:00',
                                                    '11:01 a 12:00',
                                                    '12:01 a 13:00',
                                                    '13:01 a 14:00',
                                                    '14:01 a 15:00',
                                                    '15:01 a 16:00',
                                                    '16:01 a 17:00',
                                                    '17:01 a 18:00',
                                                    '18:01 a 19:00',
                                                    '19:01 a 20:00');

                                                $campos = 'emp_id, emp_nombre';
                                                $tablas = 'gi_empresa';
                                                $camposAPintar = '';
                                                if($_SESSION['perfil'] == 7){
                                                    $condiciones = '';
                                                    $camposAPintar .= '<option class="proyectos" idproyecto="0" id_session="'.session_id().'" value=0>Seleccione</option>';
                                                }else{
                                                    $condiciones = 'emp_id IN (SELECT emu_id_empresa FROM gi_empresa_usuarios WHERE emu_id_usuario = '.$_SESSION['id_Usuario'].')';
                                                }
                                                $proyectos = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones, '', 'ORDER BY emp_nombre ASC');
                                                $activo = 1;
                                                
                                                foreach ($proyectos as $key => $value) {    
                                                    $camposAPintar .= '<option class="proyectos" value="'.$value["emp_id"].'">'.$value["emp_nombre"].'</option>';
                                                }

                                                $newCamposPrint = '<option value="0">Seleccione</option>';
                                                $campos = "act_id_i, act_descripcion_v";
                                                $tabals = "gi_actividades_d";
                                                $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabals, '', '', 'ORDER BY act_id_i ASC');
                                               
                                                
                                                foreach ($respuesta as $key => $value) {    
                                                    $newCamposPrint .= '<option value="'.$value["act_id_i"].'">'.$value["act_descripcion_v"].'</option>';
                                                }

                                                for($i = 0; $i < 13; $i++){
                                                    $campos =  "act_hora_rango_v, act_actividad_v, act_emp_id_i, act_observacion_v";
                                                    $tablas = "gi_actividades";
                                                    $condic = "act_fecha_d = '".date('Y-m-d')."' AND act_user_id_i = ".$_SESSION['id_Usuario']." AND act_hora_rango_v = '".$rangos[$i]."'";
                                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos,$tablas, $condic);
                                                    $actividad = null;
                                                    $Observaci = null;
                                                    $empresaxx = 0;
                                                    if($respuesta != false){
                                                        $actividad  = $respuesta['act_actividad_v'];
                                                        $Observaci  = $respuesta['act_observacion_v'];
                                                        $empresaxx  = $respuesta['act_emp_id_i'];
                                                    }

                                                    echo "<tr>";
                                                        echo "<td style='width:10%;'>".$rangos[$i]."<input type=\"hidden\" name=\"rango_".$i."\" value='".$rangos[$i]."'></td>";
                                                        echo "<td style='width:30%;'><select class='form-control' id=\"empresa_".$i."\" name=\"empresa_".$i."\">".$camposAPintar."</select></td>";
                                                        echo "<td style='width:30%;'><select class='form-control' id=\"txtActividadRealizada_".$i."\" name=\"txtActividadRealizada_".$i."\">".$newCamposPrint."</select></td>";
                                                        echo "<td style='width:30%;'><input type=\"text\" name=\"txtActividadRealizadaObservacion_".$i."\"  class=\"form-control\" placeholder=\"Observación\" value=\"".$Observaci."\"></td>";
                                                    echo "</tr>";
                                                    echo "<script>$('#empresa_".$i."').val(".$empresaxx.");$('#empresa_".$i."').val(".$empresaxx.").change();
                                                        $('#txtActividadRealizada_".$i."').val(".$actividad.");$('#txtActividadRealizada_".$i."').val(".$actividad.").change();</script>";
                                                }
                                            ?>

                                        </tbody>             
                                    </table>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-danger pull-right">
                                                <i class="fa fa-save"></i>&nbsp;Guardar
                                            </button>
                                        </div>
                                    </div> 
                                    <?php
                                        $actividades = new ControladorActividades();
                                        $actividades->ctrGuardarActividades();
                                    ?>
                                </form>        
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
        </div>
    </section>
</div>


<div id="modalExportarActividads" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <form action="index.php?exportar=horasLaboradasPdf" method="post">
            <div class="modal-content">
                <div class="modal-header" id="title_opciones_avanzadas" style="background: #d73925;color: white;">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" id="nombre_campo_h4">Descargar Actividades Diarias</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" checked name="chkSoloFechaToday" id="chekToday" value="1"> Solo actividades de hoy <?php echo date('Y-m-d');?>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Fecha Inicio</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control" type="text" placeholder="YYY-MM-DD" disabled id="fechaInicio" name="fechaInicio">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Fecha Final</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control" type="text" placeholder="YYY-MM-DD"  disabled id="fechaFinal" name="fechaFinal">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="button" class="btn btn-danger" id="Descargar">Descargar</button>
                </div>
            </div>
        </form>
        
      <!-- /.modal-content -->
    </div>
</div>

<script type="text/javascript">
    $(function(){
        $("#chekToday").on('ifChecked', function (event){
            $("#fechaInicio").attr("disabled", true);
            $("#fechaFinal").attr("disabled", true);          
        });
        $('#chekToday').on('ifUnchecked', function (event) {
            $("#fechaInicio").attr("disabled", false);
            $("#fechaFinal").attr("disabled", false);  
        });

        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
            daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Today",
            clear: "Clear",
            format: "yyyy-mm-dd", 
            weekStart: 0
        };

        $("#fechaInicio").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#fechaFinal').datepicker('setStartDate', minDate);
        });


        $("#fechaFinal").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#Descargar").click(function(){
            if($('#chekToday').is(':checked')){
                window.open('index.php?exportar=horasLaboradasPdf');
            }else{
                window.open('index.php?exportar=horasLaboradasPdf&fechaInicio='+$("#fechaInicio").val()+"&fechaFinal="+$("#fechaFinal").val());    
            }
            
        });
    })
</script>