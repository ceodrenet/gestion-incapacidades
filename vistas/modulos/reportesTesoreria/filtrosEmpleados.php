<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REPORTES DE TESORERIA</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Pagos por Colaborador</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    PAGOS POR EMPLEADOS
                </h5>
            </div>
            <div class="card-body">
                <form autocomplete="off">
                    <div class="row">
                        <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-3">
                            <div class="mb-3">
                                <input class="form-control flatpickr-input" type="text" name="NuevoFechaInicio" id="NuevoFechaInicio2" placeholder="Ingresar Fecha Inicial" required="true">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3">
                                <input class="form-control flatpickr-input" type="text" name="NuevoFechaFinal" id="NuevoFechaFinal2" placeholder="Ingresar Fecha Final" required="true">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscarRangos_empleados"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="d-grid gap-2 ">
                                <a target="_blank" class="btn btn-outline-danger waves-effect waves-light btn-flat" role="button" id="btnExportargeneral" style="display: none;"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <a target="_blank" class="btn btn-outline-danger waves-effect waves-light btn-flat" role="button" id="btnExportarPDF" style="display: none;"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</a>
                            </div>
                        </div>
                    </div>    
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-body" id="resultados">
            </div>
        </div>
    </div>
</div>
<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>
<script type="text/javascript">

$(function(){

   /**Editar**/
    /*Esto es lo que reemlaza al datepicker*/ 
    /*Lo que tiene en #ya sabes que es un ID */
    var NuevoFechaFinal2 = flatpickr('#NuevoFechaFinal2', {
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        locale: "es",
        onChange: function(selectedDates, dateStr, instance){
            var minDate = dateStr;
            var fecha1 = moment($("#NuevoFechaInicio2").val());
            var fecha2 = moment(minDate);
            $("#EditarNumeroDias").val( (fecha2.diff(fecha1, 'days') + 1) + " Días");
        }
    });

    var NuevoFechaInicio2 = flatpickr('#NuevoFechaInicio2', {
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        locale: "es",
        onChange: function(selectedDates, dateStr, instance) {
            NuevoFechaFinal2.set("minDate", dateStr);
            NuevoFechaFinal2.setDate(dateStr);
        },
    });

    $("#btnBuscarRangos_empleados").click(function(){
        var fechaInicio = $("#NuevoFechaInicio2").val();
        var fechaFinal  = $("#NuevoFechaFinal2").val();
        var paso = 0;
        if(fechaFinal.length < 0){
            paso = 1;
            Swal.fire({
                title: 'Error al buscar',
                text: "La fecha Final es necesaria",
                icon: "error",
                confirmButtonText: 'Cerrar'
            });

        }   

        if(fechaInicio.length < 1){
            paso = 1;
            Swal.fire({
                title: 'Error al buscar',
                text: "La fecha Inicial es necesaria",
                icon: "error",
                confirmButtonText: 'Cerrar'
            });
        }

        if(paso == 0){
            $.ajax({
                url    : 'ajax/pagosempleados.php',
                type   : 'post',
                data   :{
                    fechaInicial  : fechaInicio,
                    fechaFinal    : fechaFinal,
                    cliente_id    : $("#session").val()
                },
                dataType : 'html',
                success : function(data){
                    $("#resultados").html(data);
                    $("#btnExportargeneral").attr('href', 'index.php?exportar=pdfReportepagosEmpleados&fi='+fechaInicio+"&ff="+fechaFinal+"&clid="+$("#session").val());
                    $("#btnExportargeneral").show();

                    $("#btnExportarPDF").attr('href', 'index.php?exportar=excelReportepagosEmpleados&fi='+fechaInicio+"&ff="+fechaFinal+"&clid="+$("#session").val());
                    $("#btnExportarPDF").show();
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            })
        }
    });

});
</script>
