<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REPORTES-GERENCIAL</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Informe de Saldos</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    INFORME DE SALDOS
                </h5>
            </div>
            <div class="card-body">
                
                <form autocomplete="off" action="ajax/informeSaldo.ajax.php" method="post" autocomplete="off">
                    <div class="row">
                        <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-2">
                            <div class="mb-3">
                                <select class="form-control" id="yearFilter" name="yearFilter">
                                    <?php
                                    $years = ModeloDAO::extractYearOfDate("gi_incapacidad", "inc_fecha_recepcion_d", $_SESSION['cliente_id']);
                                    foreach ($years as $value) {
                                        echo "<option value='".$value['anho']."'>".$value['anho']."</option>";
                                    }
                                    ?>
                                    <option value="0">TODOS</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3">
                                <select class="form-control" id="filtroEstadoTramite" name="filtroEstadoTramite">
                                    <option value="1">PAGADAS</option>
                                    <option value="2">PENDIENTES</option>
                                    <option value="3">SIN RECONOCIMIENTO</option>
                                    <option value="0" selected>TODAS</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3">
                                <select class="form-control" id="filtroFecha" name="filtroFecha">
                                    <option value="inc_fecha_recepcion_d">FECHA DE RECEPCIÓN</option>
                                    <option value="inc_fecha_inicio">FECHA DE INICIO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscarSaldos"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>   
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="submit" id="btnExportar" style="display: none;"><i class="fa fa-file-excel"></i>&nbsp;Exportar</button>
                            </div>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div id="resultados">
        </div>
    </div>
</div>
<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>

<script type="text/javascript">
    
        $("#btnBuscarSaldos").click(function(){
            var year = $("#yearFilter").val();
            var estadoTramite = $("#filtroEstadoTramite").val();
            var tipoFecha = $("#filtroFecha").val();
        
            $.ajax({
                url    : 'ajax/visualizarInformeSaldo.ajax.php',
                type   : 'post',
                data   :{
                    year : year,
                    estadoTramite : estadoTramite,
                    tipoFecha : tipoFecha
                },
                dataType : 'html',
                success : function(data){
                    $("#resultados").html(data);
                    $("#btnExportar").show()
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            })
        });
    
</script>