<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REPORTES-GERENCIAL</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Detalle pagos por mes  </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                   DETALLE DE PAGOS POR MES 
                </h5>
            </div>
            <div class="card-body">
                 <form id="envioFormulario" autocomplete="off" method="post">
                    <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                    <div class="row">
                   <!-- <input type="hidden" name="anhoBusqueda" required id="txtYearOfInit" value="<?php// echo $_SESSION['cliente_id'];?>">-->
                        <div class="col-md-3">
                            <div class="mb-3">
                            <input class="form-control fecha" type="text" name="anhoBusqueda" required id="txtYearOfInit" placeholder="Ingresar Fecha Inicio" required="true" value="<?php echo date('Y');?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            
                                    <select class="form-control" id="cmbSemestres" name="cmbSemestres">
                                            <option value="1">Primer Semestre</option>
                                            <option value="2">Segundo Semestre</option>
                                        </select>
                        </div>

                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscarRangos_general"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="hidden" name="imagen" id="nombreImagen">
                                <input type="hidden" name="fila" id="imagenBase64">
                                <button class="btn btn-primary btn-block" id="btnExportarReporteGerencial" title="Descargar Reporte en PDF"><i class="fa fa-file-pdf-o"></i>&nbsp;Descargar</button>
                            </div>
                        </div>
                    </div>    
                </form>
            </div>
        </div>
    </div>
        <div class="row">
                <div class="col-md-12" id="resultados">
                    
                </div>
            </div> 

<script src="vistas/assets/assets/libs/chart.js/chart.min.js"></script>
<script type="text/javascript">
    $(function(){

       // getDatosGerencial();
        
        $("#btnExportarReporteGerencial").click(function(e){
            e.preventDefault();
            var d = new Date();
            var newNombre = "img"+d.getDate()+""+d.getHours()+""+d.getMinutes()+""+d.getSeconds()+""+d.getMilliseconds();
            $("#nombreImagen").val(newNombre);
            $("#imagenBase64").val(btoa($("#base64").val()));
            var paso = 0;
            var anhoBusqueda = $("#txtYearOfInit").val();
            var semestrebusq = $("#cmbSemestres").val();
            if(anhoBusqueda.length < 0){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "El a���o es necesario",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });

            }  


            if(paso == 0){
                $("#envioFormulario").attr("action", 'vistas/modulos/exportar/exportarPdfPagosxmesGenerals.php');
                $("#envioFormulario").submit();          
            }
        });  


        $("#btnBuscarRangos_general").click(function(){
            getDatosGerencial();
        });     

    });


function getDatosGerencial(){
    var anhoBusqueda = $("#txtYearOfInit").val();
    var semestrebusq = $("#cmbSemestres").val();
    var paso = 0;
    if(anhoBusqueda.length < 0){
        paso = 1;
        swal({
            title : "Error al buscar",
            text  : "El anño es necesario",
            type  : "error",
            confirmButtonText : "Cerrar"
        });

    } 


    if(paso == 0){
        $.ajax({
            url    : 'ajax/detallepagosxmes.php',
            type   : 'post',
            data   :{
                anhoBusqueda  : anhoBusqueda,
                semestrebusq  : semestrebusq
            },
            dataType : 'html',
            beforeSend:function(){
                $.blockUI({ 
                    baseZ: 2000,
                    message : '<h3>Un momento por favor....</h3>',
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff'
                    } 
                }); 
                
            },
            complete:function(){
                //$.unblockUI();
            },
            success : function(data){
                $("#resultados").html(data);
                
                $("#btnExportarReporteGerencial").attr('href', 'index.php?exportar=pdfPagosxmesGeneral&anhoBusqueda='+anhoBusqueda+'&semestrebusq='+semestrebusq);
            }
        })
    } 
}
</script>
</div>