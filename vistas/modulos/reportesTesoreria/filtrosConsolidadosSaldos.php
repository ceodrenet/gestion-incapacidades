<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">INFORME DE TESORERIA</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Reporte consolidado de saldos</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    CONSOLIDADO DE SALDOS
                </h5>
            </div>
            <div class="card-body">
                
                <form autocomplete="off" action="ajax/consolidadoSaldos.php" method="post" autocomplete="off">
                    <div class="row">
                        <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-3">
                            <div class="mb-3">
                                <select class="form-control" name="txtYearOfInitSaldos">
                                    <?php
                                        $strCampo = "min_vigencia_d";
                                        $strTabla = "gi_minimo_vigente";
                                        $strWhere = "";
                                        $resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampo, $strTabla, $strWhere, null, 'order by min_vigencia_d DESC', null);
                                        foreach ($resultado as $key => $value) {   
                                            echo "<option value='".$value['min_vigencia_d']."'>".$value['min_vigencia_d']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3">
                                <select class="form-control" name="selecTNominas">
                                    <option value="0">Todas</option>
                                    <?php
                                        $strCampo = "nom_id_i, nom_desc_v";
                                        $strTabla = "gi_nominas";
                                        $strWhere = "";
                                        $resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampo, $strTabla, $strWhere, null, 'order by nom_id_i ASC', null);
                                        foreach ($resultado as $key => $value) {   
                                            echo "<option value='".$value['nom_id_i']."'>".$value['nom_desc_v']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="submit" id="btnBuscarRangos_general"><i class="fa fa-file-excel"></i>&nbsp;&nbsp;Generar</button>
                            </div>
                        </div>   
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript">

$(function(){

/**Editar**/
 /*Esto es lo que reemlaza al datepicker*/ 
 /*Lo que tiene en #ya sabes que es un ID */

    /*$("#btnBuscarRangos_general").on("click", function(){
        var datas = new FormData();
        datas.append('selecTNominas', $("#selecTNominas").val());
        datas.append('txtYearOfInitSaldos', $("#NuevoFechaInicio2").val());
        $.ajax({
            url    : "ajax/consolidadoSaldos.php",
            type   : 'post',
            cache : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            data : datas,
            success : function(data){
                var currentdate = new Date();
                var fecha_hora = currentdate.getFullYear() + rellenar((currentdate.getMonth()+1), 2) +  rellenar(currentdate.getDate(), 2) + "_" + rellenar(currentdate.getHours(), 2) +  rellenar(currentdate.getMinutes(), 2) + rellenar(currentdate.getSeconds(), 2);
                var $a = $("<a>");
                $a.attr("href",data.file);
                $("body").append($a);
                $a.attr("download","consolidado_incapacidades_"+fecha_hora+".xlsx");
                $a[0].click();
                $a.remove();
            },
            beforeSend:function(){
                $.blockUI({ 
                    message : '<h3>Un momento por favor....</h3>',
                    baseZ: 2000,
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff' 
                    } 
                }); 
            },
            complete:function(){
                $.unblockUI();
            }
        });
    });*/

});
</script>
