<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">INFORME DE TESORERIA</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Cartera Antigua</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
               
                <h5 class="my-0 text-danger">
                    <div class="row">
                        <div class="col-lg-11">
                        <h5 class="my-0 text-danger">INFORME DE CARTERA ANTIGUA</h5>
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">

                    
                                    <!--<li class="divider"></li>
                                    <li><a href="#" title="Exportar datos a Excel" id="ExportarEmpleadosInc">EXP. INCAPACITADOS</a></li>-->

                                    
                                    <a class="dropdown-item" href="index.php?exportar=carteraAntigua" target="_blank" title="Exportar cartera antigua a excel">EXPORTAR A EXCEL</a></li>
                                    <div class="dropdown-divider"></div> 

                                
                                    <a class="dropdown-item" target="_blank" title="Exportar cartera antigua a pdf" data-toggle="modal" data-target="#modalExportarFechasIncapacidadesIncompletas">EXPORTAR A PDF</a></li>
                                                  
                                  
                                  <!--  <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="index.php?exportar=empleados" target="_blank" title="Exportar datos a Excel">EXPORTAR</a></li>
                                    <div class="dropdown-divider"></div>-->

                                </div>
                            </div> 
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
               <!-- <input type="hidden" name="session" id="session" value="<?php //if(isset($_SESSION['cliente_id'])){ echo $_SESSION['cliente_id']; } else { echo 0; } ?>">
                <input type="hidden" id="editar" value="<?php //echo $_SESSION['edita'];?>">
                <input type="hidden" id="elimina" value="<?php //echo $_SESSION['elimina'];?>">-->

                <table id="" style="width:100%;" class="table table-bordered dt-responsive nowrap w-100">  
                <thead>
                            <tr>
                                <th style="width: 10%;text-align:center;"></th>
                                <th style="width: 25%;text-align:center;">2018</th>
                                <th style="width: 25%;text-align:center;">2019</th>
                                <th style="width: 25%;text-align:center;">2020</th>
                                <th style="width: 15%;text-align:center;">TOTAL</th>
                            </tr>
                            <tr>
                                <th style="width: 10%;text-align:center;"></th>
                                <th style="width: 25%;text-align:center;">
                                    <table style="width:100%;">
                                        <tr>
                                            <th style="width: 50%;text-align:center;">VALOR</th>
                                            <th style="width: 25%;text-align:center;">#</th>
                                            <th style="width: 25%;text-align:center;">%</th>
                                        </tr>
                                    </table>
                                </th>
                                <th style="width: 25%;text-align:center;">
                                    <table style="width:100%;">
                                        <tr>
                                            <th style="width: 50%;text-align:center;">VALOR</th>
                                            <th style="width: 25%;text-align:center;">#</th>
                                            <th style="width: 25%;text-align:center;">%</th>
                                        </tr>
                                    </table>
                                </th>
                                <th style="width: 25%;text-align:center;">
                                    <table style="width:100%;">
                                        <tr>
                                            <th style="width: 50%;text-align:center;">VALOR</th>
                                            <th style="width: 25%;text-align:center;">#</th>
                                            <th style="width: 25%;text-align:center;">%</th>
                                        </tr>
                                    </table>
                                </th>
                                <th style="width: 15%;text-align:center;">
                                    <table style="width:100%;">
                                        <tr>
                                            <th style="width: 70%;text-align:center;">VALOR</th>
                                            <th style="width: 30%;text-align:center;">#</th>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                        </thead>
                    <tfoot>
                        <tr>
                                <th style="width: 10%;text-align:center;"></th>
                                <th style="width: 25%;text-align:center;">2018</th>
                                <th style="width: 25%;text-align:center;">2019</th>
                                <th style="width: 25%;text-align:center;">2020</th>
                                <th style="width: 15%;text-align:center;">TOTAL</th>
                            </tr>
                            <tr>
                                <th style="width: 10%;text-align:center;"></th>
                                <th style="width: 25%;text-align:center;">
                                    <table style="width:100%;">
                                        <tr>
                                            <th style="width: 50%;text-align:center;">VALOR</th>
                                            <th style="width: 25%;text-align:center;">#</th>
                                            <th style="width: 25%;text-align:center;">%</th>
                                        </tr>
                                    </table>
                                </th>
                                <th style="width: 25%;text-align:center;">
                                    <table style="width:100%;">
                                        <tr>
                                            <th style="width: 50%;text-align:center;">VALOR</th>
                                            <th style="width: 25%;text-align:center;">#</th>
                                            <th style="width: 25%;text-align:center;">%</th>
                                        </tr>
                                    </table>
                                </th>
                                <th style="width: 25%;text-align:center;">
                                    <table style="width:100%;">
                                        <tr>
                                            <th style="width: 50%;text-align:center;">VALOR</th>
                                            <th style="width: 25%;text-align:center;">#</th>
                                            <th style="width: 25%;text-align:center;">%</th>
                                        </tr>
                                    </table>
                                </th>
                                <th style="width: 15%;text-align:center;">
                                    <table style="width:100%;">
                                        <tr>
                                            <th style="width: 70%;text-align:center;">VALOR</th>
                                            <th style="width: 30%;text-align:center;">#</th>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                    </tfoot>
                    <tbody  style="text-align:center;">
                                <?php 
                                    /*Area de valores principales*/
                                    $carteraTotal18 = ControladorIncapacidades::getSumIncapacidades(2018, null);
                                    $carteraTotal19 = ControladorIncapacidades::getSumIncapacidades(2019, null);
                                    $carteraTotal20 = ControladorIncapacidades::getSumIncapacidades(2020, null);

                                    $countTotal18 = ControladorIncapacidades::getCountIncapacidades(2018, null);
                                    $countTotal19 = ControladorIncapacidades::getCountIncapacidades(2019, null);
                                    $countTotal20 = ControladorIncapacidades::getCountIncapacidades(2020, null);

                                    $carteraPagadas18 = ControladorIncapacidades::getSumIncapacidades(2018, "PAGADA");
                                    $carteraPagadas19 = ControladorIncapacidades::getSumIncapacidades(2019, "PAGADA");
                                    $carteraPagadas20 = ControladorIncapacidades::getSumIncapacidades(2020, "PAGADA");

                                    $countPagadas18 = ControladorIncapacidades::getCountIncapacidades(2018, "PAGADA");
                                    $countPagadas19 = ControladorIncapacidades::getCountIncapacidades(2019, "PAGADA");
                                    $countPagadas20 = ControladorIncapacidades::getCountIncapacidades(2020, "PAGADA");

                                    

                                    $carteraNegadas18 = ControladorIncapacidades::getSumIncapacidades(2018, 'SIN RECONOCIMIENTO');
                                    $carteraNegadas19 = ControladorIncapacidades::getSumIncapacidades(2019, 'SIN RECONOCIMIENTO');
                                    $carteraNegadas20 = ControladorIncapacidades::getSumIncapacidades(2020, 'SIN RECONOCIMIENTO');

                                    $countNegadas18 = ControladorIncapacidades::getCountIncapacidades(2018, "SIN RECONOCIMIENTO");
                                    $countNegadas19 = ControladorIncapacidades::getCountIncapacidades(2019, "SIN RECONOCIMIENTO");
                                    $countNegadas20 = ControladorIncapacidades::getCountIncapacidades(2020, "SIN RECONOCIMIENTO");

                                    $carteraPorRadicar18 = ControladorIncapacidades::getSumIncapacidades(2018, 'POR RADICAR');
                                    $carteraPorRadicar19 = ControladorIncapacidades::getSumIncapacidades(2019, 'POR RADICAR');
                                    $carteraPorRadicar20 = ControladorIncapacidades::getSumIncapacidades(2020, 'POR RADICAR');

                                    $countPorRadicar18 = ControladorIncapacidades::getCountIncapacidades(2018, "POR RADICAR");
                                    $countPorRadicar19 = ControladorIncapacidades::getCountIncapacidades(2019, "POR RADICAR");
                                    $countPorRadicar20 = ControladorIncapacidades::getCountIncapacidades(2020, "POR RADICAR");

                                    $carteraRadicadas18 = ControladorIncapacidades::getSumIncapacidades(2018, 'RADICADA');
                                    $carteraRadicadas19 = ControladorIncapacidades::getSumIncapacidades(2019, 'RADICADA');
                                    $carteraRadicadas20 = ControladorIncapacidades::getSumIncapacidades(2020, 'RADICADA');

                                    $countReadicadas18 = ControladorIncapacidades::getCountIncapacidades(2018, "RADICADA");
                                    $countReadicadas19 = ControladorIncapacidades::getCountIncapacidades(2019, "RADICADA");
                                    $countReadicadas20 = ControladorIncapacidades::getCountIncapacidades(2020, "RADICADA");
                                    ?>
                            <tr>
                               <td>CARTERA</td>
                               <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraTotal18, 2, ',', '.'); ?>   
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countTotal18; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">100%</td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraTotal19, 2, ',', '.'); ?>   
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countTotal19; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">100%</td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraTotal20, 2, ',', '.'); ?>   
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countTotal20; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">100%</td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 70%;text-align:center;">
                                                $<?php echo number_format(($carteraTotal20 + $carteraTotal19 + $carteraTotal18), 2, ',', '.'); ?>   
                                            </td>
                                            <td style="width: 35%;text-align:center;">
                                                <?php echo number_format($countTotal20+$countTotal19+$countTotal18, 0, ',', '.'); ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>PAGADAS</td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraPagadas18, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countPagadas18; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraPagadas18 * 100) / $carteraTotal18), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraPagadas19, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countPagadas19; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraPagadas19 * 100) / $carteraTotal19), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td> 
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraPagadas20, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countPagadas20; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraPagadas20 * 100) / $carteraTotal20), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 70%;text-align:center;">
                                                $<?php echo number_format(($carteraPagadas20 + $carteraPagadas18 + $carteraPagadas19), 2, ',', '.'); ?>   
                                            </td>
                                            <td style="width: 30%;text-align:center;">
                                                <?php echo number_format($countPagadas20+$countPagadas19+$countPagadas18, 0, ',', '.'); ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                               <td>NEGADAS</td>
                               <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraNegadas18, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countNegadas18; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraNegadas18 * 100) / $carteraTotal18), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraNegadas19, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countNegadas19; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraNegadas19 * 100) / $carteraTotal19), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td> 
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraNegadas20, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countNegadas20; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraNegadas20 * 100) / $carteraTotal20), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 70%;text-align:center;">
                                                $<?php echo number_format(($carteraNegadas20 + $carteraNegadas19 + $carteraNegadas18), 2, ',', '.'); ?>   
                                            </td>
                                            <td style="width: 30%;text-align:center;">
                                                <?php echo number_format($countNegadas20+$countNegadas19+$countNegadas18, 0, ',', '.'); ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>POR RADICAR</td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraPorRadicar18, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countPorRadicar18; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraPorRadicar18 * 100) / $carteraTotal18), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                               <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraPorRadicar19, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countPorRadicar19; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraPorRadicar19 * 100) / $carteraTotal19), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td> 
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraPorRadicar20, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countPorRadicar20; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraPorRadicar20 * 100) / $carteraTotal20), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 70%;text-align:center;">
                                                $<?php echo number_format(($carteraPorRadicar20 + $carteraPorRadicar18 + $carteraPorRadicar19), 2, ',', '.'); ?>   
                                            </td>
                                            <td style="width: 30%;text-align:center;">
                                                <?php echo number_format($countPorRadicar20+$countPorRadicar19+$countPorRadicar18, 0, ',', '.'); ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                               <td>RADICADAS</td>
                               <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraRadicadas18, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countReadicadas18; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraRadicadas18 * 100) / $carteraTotal18), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraRadicadas19, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countReadicadas19; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraRadicadas19 * 100) / $carteraTotal19), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td> 
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 50%;text-align:center;">
                                                $<?php echo number_format($carteraRadicadas20, 2, ',', '.'); ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo $countReadicadas20; ?>
                                            </td>
                                            <td style="width: 25%;text-align:center;">
                                                <?php echo number_format((($carteraRadicadas20 * 100) / $carteraTotal20), 2);?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width: 70%;text-align:center;">
                                                $<?php echo number_format(($carteraRadicadas20 + $carteraRadicadas19 + $carteraRadicadas18), 2, ',', '.'); ?>   
                                            </td>
                                            <td style="width: 30%;text-align:center;">
                                                <?php echo number_format($countReadicadas20+$countReadicadas19+$countReadicadas18, 0, ',', '.'); ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>   
                            </tr>
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>