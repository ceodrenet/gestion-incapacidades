<div class="row">
    <div class="col-md-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    INFORMACIÓN POR ADMINISTRADORAS
                </h5>
                     <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle"title="Exportar incapacidades a excel" href="index.php?exportarAdministradoraValores=true&year=<?php echo $year;?>&otherYear=<?php echo $otherYear;?>" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">                                    EXPORTAR 
                                </button>
                   
               
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-hover tblOther" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ADMIISTRADORA</th>
                                    <th>EMPRESA</th>
                                    <th>POR COBRAR</th>
                                    <th>PAGADAS</th>
                                    <th>SALDOS</th>
                                    <th>%</th>
                                    <th>AFILIADOS</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <?php 
/*Primero toca obtener las EPS que estan en esta empresa */
$intTotalEmpresPagado = 0;
$intTotalIpsPagado___ = 0;
$intTotalEmpleados___ = 0;
$intTotalPorcentaje__ = 0;
$intTotalRecuperado__ = 0;
$totalSaldo = 0;

$fecha_para_buscar = "inc_fecha_pago_nomina";
if(isset($tipoReporte)){
    $fecha_para_buscar = "inc_fecha_pago";
}

$strCampo = "ips_nombre, ips_id";
$strTabla = " gi_ips JOIN gi_empleados ON emd_eps_id = ips_id";
$strWhere = '1 = 1';
if($_SESSION['cliente_id'] != 0){
    $strWhere .= " AND emd_emp_id = ".$_SESSION['cliente_id'];
}
$arrResul = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampo,$strTabla,$strWhere, 'GROUP BY ips_id', ' ORDER BY ips_nombre ASC');
foreach ($arrResul as $key => $eps) {
    /*tenemos las EPS*/
   

        /*ahora vamos a obtener las Incapacidades pagadas por la empresa*/
        $strCampo = "sum(inc_valor_pagado_empresa) as total";
        $strTabla = "gi_incapacidad JOIN gi_empleados ON emd_cedula = inc_cc_afiliado";
        $strWhere = " inc_valor_pagado_empresa IS NOT NULL AND inc_valor_pagado_empresa != '' AND  ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' AND emd_eps_id = ".$eps['ips_id'];
        if($_SESSION['cliente_id'] != 0){
            $strWhere .= " AND inc_empresa = ".$_SESSION['cliente_id'];
        }
        $arrRespuestaPE = ModeloTesoreria::mdlMostrarUnitario($strCampo, $strTabla, $strWhere);
        $intTotalEmpresPagado += $arrRespuestaPE['total'];


        /*ahora vamos a obtener las Incapacidades por que se deben pagar por las EPS*/
        $strCampo = "sum(inc_valor_pagado_eps) as total";
        $strTabla = "gi_incapacidad JOIN gi_empleados ON emd_cedula = inc_cc_afiliado";
        $strWhere = " inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'SIN RECONOCIMIENTO'  AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' AND emd_eps_id = ".$eps['ips_id'];
        if($_SESSION['cliente_id'] != 0){
            $strWhere .= " AND inc_empresa = ".$_SESSION['cliente_id'];
        }
        $arrRespuestaIP = ModeloTesoreria::mdlMostrarUnitario($strCampo, $strTabla, $strWhere);
        $intTotalIpsPagado___ += $arrRespuestaIP['total'];
        
        /*ahora vamos a obtener las Incapacidades Que se pagaron*/
        $strCampo = "sum(inc_valor) as total";
        $strTabla = "gi_incapacidad JOIN gi_empleados ON emd_cedula = inc_cc_afiliado";
        $strWhere = " inc_estado_tramite =  'PAGADA' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' AND emd_eps_id = ".$eps['ips_id'];
        if($_SESSION['cliente_id'] != 0){
            $strWhere .= " AND inc_empresa = ".$_SESSION['cliente_id'];
        }
        $arrRespuestaPS = ModeloTesoreria::mdlMostrarUnitario($strCampo, $strTabla, $strWhere);
        $intTotalRecuperado__ += $arrRespuestaPS['total'];

        $douPorcentage = 0;
        if($arrRespuestaIP['total'] != 0)    
            $douPorcentage = ($arrRespuestaPS['total'] * 100) / $arrRespuestaIP['total'];
        
        //$intTotalPorcentaje__ += $douPorcentage;
        /*Afiliados*/
        $strCampo = "COUNT(emd_cedula) as total";
        $strTabla = "gi_empleados";
        $strWhere = " emd_eps_id = ".$eps['ips_id']."  AND emd_estado = 1 ";
        if($_SESSION['cliente_id'] != 0){
            $strWhere .= " AND emd_emp_id = ".$_SESSION['cliente_id'];
        }
        $arrRespuestaEM = ModeloTesoreria::mdlMostrarUnitario($strCampo, $strTabla, $strWhere);
        $intTotalEmpleados___ += $arrRespuestaEM['total'];
        /*Pintamos*/
        echo '<tr>';
            echo '<td>'.($key+1).'</td>';
            echo "<td>".$eps['ips_nombre']."</td>";
            echo "<td>$ ".number_format($arrRespuestaPE['total'], 0, ',', '.')."</td>";
            echo "<td>$ ".number_format($arrRespuestaIP['total'], 0, ',', '.')."</td>";
            echo "<td>$ ".number_format($arrRespuestaPS['total'], 0, ',', '.')."</td>";
            $totaliszado = $arrRespuestaIP['total'] - $arrRespuestaPS['total'];
            $totalSaldo += $totaliszado; 
            echo "<td>$ ".number_format($totaliszado, 0, ',', '.')."</td>";
            echo "<td>".number_format($douPorcentage, 2)." %</td>";
            echo "<td>".number_format($arrRespuestaEM['total'], 0)."</td>";
        echo '</tr>';        
    
    
}   
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>TOTAL</th>
                                        <th><?php echo number_format($intTotalEmpresPagado, 0, ',', '.'); ?></th>
                                        <th><?php echo number_format($intTotalIpsPagado___, 0, ',', '.'); ?></th>
                                        <th><?php echo number_format($intTotalRecuperado__, 0, ',', '.')?></th>
                                        <th><?php echo number_format($totalSaldo, 0, ',', '.');?></th>
                                        <th></th>
                                        <th><?php echo number_format($intTotalEmpleados___, 0, ',', '.'); ?></th>
                                    </tr>
                                </tfoot>
                            </table>   
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
