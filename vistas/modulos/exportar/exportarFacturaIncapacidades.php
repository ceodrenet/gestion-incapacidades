<?php
	require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';
	require_once __DIR__.'/../../../extenciones/SVGGraph/SVGGraph.php';

	function sanear_string($string) { 
        $string = str_replace( array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string );
        $string = str_replace( array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string ); 
        $string = str_replace( array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string ); 
        $string = str_replace( array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string ); 
        $string = str_replace( array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string ); 
        $string = str_replace( array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C',), $string ); 
        //Esta parte se encarga de eliminar cualquier caracter extraño 
        //$string = str_replace( array("\\", "¨", "º", "-", "~", "#", "@", "|", "!", "\"", "·", "$", "%", "&", "/", "(", ")", "?", "'", "¡", "¿", "[", "^", "`", "]", "+", "}", "{", "¨", "´", ">“, “< ", ";", ",", ":", "."), '', $string ); 
        return $string; 
    }

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }


    class MYPDF extends TCPDF {

        protected $processId = 0;
        protected $header = '';
        protected $footer = '';
        static $errorMsg = '';
        //Page header
        public function Header() {
            $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
           $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

           $this->Line(5, 5, $this->getPageWidth()-5, 5); 

           $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
           $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
           $this->Line(5, 5, 5, $this->getPageHeight()-5);

        }

        // Page footer
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            // Set font
            $this->SetFont('helvetica', 'N', 8);
            // Page number
            $this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
    }


    /*empezar el proceso del PDF*/
    $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("Reporte Incapacidades por Empleado");
    //$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    // set default header data

    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(true);
    $obj_pdf->setPrintFooter(true);
    $obj_pdf->SetAutoPageBreak(TRUE, 20);
    $obj_pdf->SetFont('times', '', 15);
    // set image scale factor

    $obj_pdf->AddPage('L', 'A4'); 
    $title = '';
    $title2 = '';
    $empres = '';
    $nit = '';
    if(isset($_GET['clid']) && $_GET['clid'] == 0){
        $title = '<th style="text-align:center;">Empresa</th>';
        $title2 = '<th></th>';
        $empres = '<td style="width:9%;text-align:justify;"> </td>
            <td style="width:50%;text-align:justify;"></td>';
        $nit = '<td style="width:9%;text-align:justify;"></td>
            <td style="width:50%;text-align:justify;"></td>';
    }else{
        $item  = 'emp_id';
        $valor = $_GET['clid']; 
        $empresa = ControladorClientes::ctrMostrarClientes($item, $valor);

        $empres = '<td style="width:9%;text-align:justify;">Empresa </td>
            <td style="width:50%;text-align:justify;">:&nbsp; <b>'.$empresa['emp_nombre']."</b></td>";
        $nit = '<td style="width:9%;text-align:justify;">NIT </td>
            <td style="width:50%;text-align:justify;">:&nbsp; <b>'.$empresa['emp_nit']."</b></td>";
    }

    $content = '';
    $content = '
    <br/>
    <br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:10%"></td>
                        <td style="width:70%; text-align:center; font-size:12px;"><b>REPORTE GENERAL DE PAGOS</b></td>
                        <td style="width:20%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:20%"></td>
                        <td style="width:50%;text-align:center;">INFORME DE PAGOS POR ADMINISTRADORA</td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <br/>
    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            '.$empres.'
            <td style="width:14%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;">Fecha:&nbsp;&nbsp;&nbsp; '.date('d/m/Y H:i:s').'</td>
        </tr>
        <tr>
            '.$nit.'
            <td style="width:14%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
        </tr>
        
    </table>
    <br/>
    <br/>
    <table  border="1" style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <thead>
            <tr style="background-color:#bdbdbd;border:solid 1px;">
                '.$title.'
                <th style="text-align:center;">Administradora</th>
                <th style="text-align:center;">Valor Pagado</th>
                <th style="text-align:center;">Fecha de Pago</th>
                <th style="text-align:center;">No. Factura</th>
                <th style="text-align:center;">Fecha Radicaci&oacute;n</th>
                <th style="text-align:center;">F. Pago Factura</th>
            </tr>
        </thead>
        <tbody>';
    $item = null;
    $valor = null;
    $sitienenDatos = 0;
    $numberFactura = '';
    $fechaFactura = '';
    $fechaPagoFactu = '';
    $item = null;
    $valor = null;

    if($_GET['clid'] != 0){
        $item = 'inc_empresa';
        $valor = $_GET['clid'];
        $pagadas = ControladorTesoreria::ctrMostrarGeneralPagos_byCliente($item, $valor, $_GET['fi'], $_GET['ff']);
    }else{
        $pagadas = ControladorTesoreria::ctrMostrarGeneralPagos($item, $valor, $_GET['fi'], $_GET['ff']);
    }

    $valorPagadoF = 0;
    foreach ($pagadas as $key => $value) {
        $content .= '<tr>';
            if($_GET['clid'] == 0){ 
                $content .= '<td style="text-align:center">'.$value['emp_nombre'].'</td>';
            }
            
            if($value['inc_clasificacion'] != 4){
                if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                    $content .= '<td style="text-align:center">'.($value["arl_nombre"]).'</td>'; 
                }else{
                    $content .= '<td style="text-align:center">'.($value["ips_nombre"]).'</td>'; 
                }
            }else{
                $content .= '<td style="text-align:center">'.($value["afp_nombre"]).'</td>';    
            }

            if(!empty($value['inc_valor']) && $value['inc_valor'] != ''){
                $content .= '<td style="text-align:center">$ '.number_format($value['inc_valor'], 0, ',', '.').'</td>';
                $valorPagadoF += $value['inc_valor'];
            }else{
                $content .= '<td></td>';
            }
            
            $content .= '<td style="text-align:center;">'.$value['inc_fecha_pago2'].'</td>';
            $content .=  '<td style="text-align:center;">'.$value['inc_numero_factura'].'</td>';

            $numberFactura = $value['inc_numero_factura'];
   
   

            if($value['inc_fecha_emision_factura'] != null && $value['inc_fecha_emision_factura'] != '0000-00-00'){
                $content .=  '<td style="text-align:center;">'.$value['inc_fecha_emision_factura'].'</td>';
                $fechaFactura = $value['inc_fecha_emision_factura'];
            }else{
                $content .=  '<td></td>';
            }

            if($value['inc_fecha_pago_factura'] != null && $value['inc_fecha_pago_factura'] != '0000-00-00'){
                $content .=  '<td style="text-align:center;">'.$value['inc_fecha_pago_factura'].'</td>';
                $fechaPagoFactu = $value['inc_fecha_pago_factura'];
            }else{
                $content .=  '<td></td>';
            }
        $content .= '</tr>';
    }

    $content .= '
            <tr style="background-color:#bdbdbd;border:solid 1px;">
                '.$title2.'
                <th style="text-align:center;"></th>
                <th style="text-align:center;background-color:#bdbdbd;">$ '.number_format($valorPagadoF,  0, ',', '.').'</th>
                <th style="text-align:center;"></th>
                <th style="text-align:center;"></th>
                <th style="text-align:center;"></th>
                <th style="text-align:center;"></th>
            </tr>
        </tbody>
    </table>';
    $obj_pdf->writeHTML($content, true, false, false, false, '');

    $nombre='Reporte_incapacidades_facturacion_'.date("d-m-Y H-i-s").'.pdf';
    //header('Content-type: application/pdf');
    $obj_pdf->Output($nombre, 'I');
            

