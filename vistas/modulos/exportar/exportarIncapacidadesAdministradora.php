<?php
	require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';
	require_once __DIR__.'/../../../extenciones/SVGGraph/SVGGraph.php';

	function sanear_string($string) { 
        $string = str_replace( array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string );
        $string = str_replace( array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string ); 
        $string = str_replace( array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string ); 
        $string = str_replace( array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string ); 
        $string = str_replace( array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string ); 
        $string = str_replace( array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C',), $string ); 
        //Esta parte se encarga de eliminar cualquier caracter extraño 
        //$string = str_replace( array("\\", "¨", "º", "-", "~", "#", "@", "|", "!", "\"", "·", "$", "%", "&", "/", "(", ")", "?", "'", "¡", "¿", "[", "^", "`", "]", "+", "}", "{", "¨", "´", ">“, “< ", ";", ",", ":", "."), '', $string ); 
        return $string; 
    }

    class MYPDF extends TCPDF {

        protected $processId = 0;
        protected $header = '';
        protected $footer = '';
        static $errorMsg = '';
        //Page header
        public function Header() {
            $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
           $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

           $this->Line(5, 5, $this->getPageWidth()-5, 5); 

           $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
           $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
           $this->Line(5, 5, 5, $this->getPageHeight()-5);

        }

        // Page footer
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            // Set font
            $this->SetFont('helvetica', 'N', 8);
            // Page number
            $this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
    }


    /*empezar el proceso del PDF*/
    $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("Reporte Incapacidades por Administradora");
    //$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    // set default header data

    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(true);
    $obj_pdf->setPrintFooter(true);
    $obj_pdf->SetAutoPageBreak(TRUE, 30);
    $obj_pdf->SetFont('times', '', 15);
    // set image scale factor

    $obj_pdf->AddPage('L', 'A4');    

    $item  = 'ips_id';
    $valor = $_GET['adm']; 
    $ips = ControladorIps::ctrMostrarIpsById($item, $valor);

    $title = '';
    if($_GET['clid'] == 0){ 
        $title = '<th style="width: 15%;text-align:center;">Empresa</th>';
    }else{ 
        $title = '<th style="width: 15%;text-align:center;">Cédula</th>';
    }

    $content = '';
    $content = '
    <br/>
    <br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:90%; text-align:center; font-size:12px;"><b>REPORTE INCAPACIDADES POR ADMINISTRADORA</b></td>
                        <td style="width:10%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:90%;text-align:center;">INFORME TOTAL</td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    <br/>
    <br/>

    <br/>
     <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <td style="width:9%;text-align:justify;">Nombre </td>
            <td style="width:50%;text-align:justify;">:&nbsp;<b>'.mb_strtoupper($ips['ips_nombre']).'</b></td>
            <td style="width:15%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;">Fecha:&nbsp;&nbsp;&nbsp; '.date('d/m/Y H:i:s').'</td>
        </tr>
        <tr>
            <td style="width:9%;text-align:justify;">NIT </td>
            <td style="width:50%;text-align:justify;">:&nbsp;'.$ips['ips_nit'].' </td>
            <td style="width:15%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;">Tipo Administradora: '.$ips['ips_tipo'].' </td>
        </tr>
        <tr>
            <td style="width:9%;text-align:justify;">Codigo  </td>
            <td style="width:50%;text-align:justify;">:&nbsp;<b>'.$ips['ips_codigo'].'</b></td>
            <td style="width:15%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
        </tr>
        <tr>
            <td style="width:9%;text-align:justify;"></td>
            <td style="width:50%;text-align:justify;"></td>
            <td style="width:15%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
        </tr>
    </table>
    <br/>
     <table  border="1" style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <thead>
            <tr style="background-color:#bdbdbd;border:solid 1px;">
                '.$title.'
                <th style="width: 20%;text-align:center;">Nombre</th>
                <th style="width: 15%;text-align:center;">Origen</th>
                <th style="width: 10%;text-align:center;">F. Inicial</th>
                <th style="width: 10%;text-align:center;">F. Final</th>
                <th style="width: 10%;text-align:center;">Estado</th>
                <th style="width: 10%;text-align:center;">Valor</th>
                <th style="width: 10%;text-align:center;">F. Pago</th>
            </tr>
        </thead>
        <tbody>';

        $whereCliente = '';
        if($_GET['clid'] != 0){
            $whereCliente =  "inc_empresa = ".$_GET['clid']." AND ";
        }

        $whereAdministradora ='';
        if($_GET['tp'] == 'ARL'){
            $whereAdministradora =' AND inc_arl_afiliado = '.$_GET['adm'];
        }else if($_GET['tp'] == 'AFP'){
            $whereAdministradora =' AND inc_afp_afiliado = '.$_GET['adm'];
        }if($_GET['tp'] == 'EPS'){
            $whereAdministradora =' AND inc_ips_afiliado = '.$_GET['adm'];
        }


        $valor = $_GET['fi'];
        $valor2 = $_GET['ff'];
        $campos = 'emp_nombre, emd_cedula, emd_nombre, inc_diagnostico, inc_origen, inc_clasificacion, inc_fecha_inicio, inc_fecha_final, inc_tipo_generacion, inc_profesional_responsable, inc_prof_registro_medico, inc_donde_se_genera, inc_fecha_generada, inc_observacion, inc_estado_tramite, inc_fecha_radicacion, inc_fecha_solicitud, DATE_FORMAT(inc_fecha_pago, "%Y-%m-%d") as inc_fecha_pago, inc_valor, inc_fecha_pago_nomina, inc_valor_pagado_nomina';
        $tabla  = 'gi_incapacidad LEFT JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id';
        $condicion = $whereCliente." inc_fecha_inicio BETWEEN '".$valor."' AND '".$valor2."' ".$whereAdministradora;
        $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, '', 'ORDER BY inc_id ASC LIMIT 0, 25');
        
        $valorPagado = 0;
        foreach ($incapacidades as $key => $value) {
            $content .= ' 
            <tr>';
            if($_GET['clid'] == 0){ 
               $content .= '<td style="width: 15%;text-align:center;">'.$value["emp_nombre"].'</td>';
            }else{
                $content .= '<td style="width: 15%;text-align:center;">'.$value["emd_cedula"].'</td>';
            }
            $valorEstado = 0;
            if(!empty($value["inc_valor"]) && !is_null($value["inc_valor"])){
                $valorEstado = "$ ".number_format($value["inc_valor"], 0, ',', '.');
                $valorPagado += $value["inc_valor"];
            }
        
            $fecha = $value['inc_fecha_pago'];
            
            $content .= '
                <td style="width: 20%;text-align:center;">'.$value["emd_nombre"].'</td>';
            $content .= '<td style="width: 15%;text-align:center;">'.($value["inc_origen"]).'</td>'; 

            $content .=' <td style="width: 10%;text-align:center;">'.explode(' ',$value["inc_fecha_inicio"])[0].'</td>
                <td style="width: 10%;text-align:center;">'.explode(' ',$value["inc_fecha_final"])[0].'</td>
                <td style="width: 10%;text-align:center;">'.$value["inc_estado_tramite"].'</td>
                <td style="width: 10%;text-align:center;">'.$valorEstado.'</td>
                <td style="width: 10%;text-align:center;">'.$fecha.'</td>
                
            </tr>'; 
        }

    $content .='
            <tr style="background-color:#bdbdbd;border:solid 1px;">
                <th style="text-align:rigth;" colspan="6" >TOTAL VALOR INCAPACIDADES</th>
                <th style="width: 10%;text-align:center;">$ '.number_format($valorPagado, 0, ',', '.').'</th>
                <th style="width: 10%;text-align:center;"></th>
            </tr>
        </tbody>
    </table>';

    $obj_pdf->writeHTML($content, true, false, false, false, '');

    $nombre='Reporte_incapacidades_por_colaborador_'.date("d-m-Y H-i-s").'.pdf';
    //header('Content-type: application/pdf');
    $obj_pdf->Output($nombre, 'I');
            