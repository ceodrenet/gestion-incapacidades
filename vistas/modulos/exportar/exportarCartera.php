<?php

	$campos = "emp_nombre, emp_id";
	$tablas = "incapa_gein.gi_cartera JOIN gi_empresa ON emp_id = car_emp_id";
	$condic = "";
	$repsue = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condic, 'GROUP BY emp_id', 'ORDER BY emp_nombre');

	$objPHPExcel = new PHPExcel();

	$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

	$objPHPExcel->setActiveSheetIndex(0);

	$objPHPExcel->getProperties()->setCreator("GEIN")
	                     ->setLastModifiedBy("".$_SESSION['nombres'])
	                     ->setTitle("GEIN - Cartera")
	                     ->setSubject("Cartera")
	                     ->setDescription("Descarga la cartera, registrada en el sistema")
	                     ->setKeywords("office 2007 openxml php")
	                     ->setCategory("Cartera");

	$objPHPExcel->getActiveSheet();

	$borders = array(
			'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);


		$campos = "emp_nombre, emp_direccion, emp_nit, emp_telefono";
		$tablas = "gi_empresa";
		$condicion = "emp_id = ".$_SESSION['cliente_id'];

		$newEmpresa = ModeloTesoreria::mdlMostrarUnitario($campos, $tablas, $condicion);

		$objPHPExcel->getActiveSheet()->setTitle($newEmpresa['emp_nombre']);
		$objPHPExcel->getActiveSheet()->getStyle('A1:G1')
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$objPHPExcel->getActiveSheet()->getStyle('B1:G1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('B2:G2')->applyFromArray($borders);

    $objPHPExcel->getActiveSheet()->setCellValue("B1", "EMPRESA"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "NIT"); 
    $objPHPExcel->getActiveSheet()->mergeCells('D1:F1');
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "DIRECCION"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "TELEFONO"); 

    $objPHPExcel->getActiveSheet()->getStyle('B1:G1')->getFont()->setBold( true );

    $objPHPExcel->getActiveSheet()->setCellValue("B2", mb_strtoupper($newEmpresa['emp_nombre'])); 
    $objPHPExcel->getActiveSheet()->setCellValue("C2", mb_strtoupper($newEmpresa['emp_nit'])); 
    $objPHPExcel->getActiveSheet()->mergeCells('D2:F2');
    $objPHPExcel->getActiveSheet()->setCellValue("D2", mb_strtoupper($newEmpresa['emp_direccion'])); 
    $objPHPExcel->getActiveSheet()->setCellValue("G2", mb_strtoupper($newEmpresa['emp_telefono'])); 


    $objPHPExcel->getActiveSheet()->setCellValue("B5", "ADMINISTRADORA"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C5", "USUARIO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D5", "CLAVE"); 
    $objPHPExcel->getActiveSheet()->getStyle('B5:D5')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('A5:G5')
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$objPHPExcel->getActiveSheet()->getStyle('B5:D5')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('B6:D6')->applyFromArray($borders);


    $item = 'car_emp_id';
    $valo = $_SESSION['cliente_id'];
    $cartera = ControladorCartera::ctrMostrarCarteraDeglosada($item, $valo);



    $i = 9;
    $objPHPExcel->getActiveSheet()->setCellValue("A8", 'ITEM');
    $objPHPExcel->getActiveSheet()->setCellValue("B8", "EPS (ADMINISTRADORAS)"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C8", "CODIGO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D8", "NIT ENTIDAD"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E8", 'COLABORADORES');
    $objPHPExcel->getActiveSheet()->setCellValue("F8", "SOLICITUD"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G8", "RADICACION"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H8", "COLABORADORES ACTIVOS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I8", 'PAZ Y SALVO');
    $objPHPExcel->getActiveSheet()->setCellValue("J8", "RESPUESTA"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K8", "VALOR CARTERA"); 
    $objPHPExcel->getActiveSheet()->setCellValue("L8", "OBSERVACIONES"); 

    $objPHPExcel->getActiveSheet()->getStyle('A8:L8')
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('A8:L8')->applyFromArray($borders);
    $objPHPExcel->getActiveSheet()->getStyle('A8:L8')->getFont()->setBold( true );

    foreach ($cartera as $key => $value) {

    	$valor = 0;
        if(!empty($value['car_valor'])){
            $valor = number_format(trim($value['car_valor']), 0);
        }


        $campo = "COUNT(*) as total";
	    $tabla = "gi_empleados";
	    $condi = "emd_emp_id = ".$_SESSION['cliente_id']." AND (emd_eps_id = ".$value['ips_id']." OR emd_afp_id = ".$value['ips_id']." OR emd_arl_id = ".$value['ips_id'].")";
	    $TotalEmprelados = ModeloTesoreria::mdlMostrarUnitario($campo, $tabla, $condi);

    	$objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1));
	    $objPHPExcel->getActiveSheet()->setCellValue("B".$i,  mb_strtoupper($value['ips_nombre'])); 
	    $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value['ips_codigo']); 
	    $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value['ips_nit']); 
	    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $TotalEmprelados['total']);
	    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value['car_fecha_cre']); 
	    $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value['car_fecha_rad']); 
	    $objPHPExcel->getActiveSheet()->setCellValue("H".$i, "COLABORADORES ACTIVOS"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value['car_paz_salvo']);
	    $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value['car_respuesta']); 
	    $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $valor); 
	    $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $value['car_observacion']); 

	    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':L'.$i)->applyFromArray($borders);

	    foreach(range('A','L') as $columnID) {
		    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
		        ->setAutoSize(true);
		}
	    $i++;
    }

    

     // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="cartera'.date('Y_m_d_H_i_s').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');


