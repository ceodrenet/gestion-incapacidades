<?php
	require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';
	require_once __DIR__.'/../../../extenciones/SVGGraph/SVGGraph.php';

	
    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }

    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    
    require_once dirname(__FILE__) . '/../../../extenciones/Excel.php';
    require_once dirname(__FILE__) . '/../../../controladores/incapacidades.controlador.php';
    require_once dirname(__FILE__) . '/../../../modelos/tesoreria.modelo.php';

    $item  = 'emd_cedula';
    $valor = $_GET['cc']; 
    $empleado = ControladorEmpleados::ctrMostrarEmpleados($item, $valor);

    $item  = 'emp_id';
    $valor = $empleado['emd_emp_id']; 
    $empresa = ControladorClientes::ctrMostrarClientes($item, $valor);


    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    
    
    $objPHPExcel = new Spreadsheet();


    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Reporte Incapacidades Empleados")
                             ->setSubject("Reporte Incapacidades Empleados")
                             ->setDescription("Descarga el Reporte de Incapacidades Empleados registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Reporte Incapacidades Empleados");


   
    $objPHPExcel->getActiveSheet()->setTitle('INCAPACIDADES POR EMPLEADO');

    $objPHPExcel->getActiveSheet()->getStyle('B4:B8')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('B4:B8')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);


    $objPHPExcel->getActiveSheet()->getStyle('B4:B10')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('B4:B10')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
    $objPHPExcel->getActiveSheet()->getStyle('I4:I6')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('I4:I6')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);


    $objPHPExcel->getActiveSheet()->mergeCells('B2:L2');
    $objPHPExcel->getActiveSheet()->setCellValue("B2", "HISTORICO DE INCAPACIDADES POR EMPLEADO"); 
    $objPHPExcel->getActiveSheet()->getStyle('B2:L2')->getFont()
        ->setName('Arial')
        ->setSize(24)
        ->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('B2:L2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


    /*CENTRO LAS LETRAS*/
    $objPHPExcel->getActiveSheet()->getStyle('B4:B10')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('E4:E10')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('I4:I10')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('K4:K10')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);





    $objPHPExcel->getActiveSheet()->mergeCells('B4:D4');
    $objPHPExcel->getActiveSheet()->mergeCells('E4:H4');
    $objPHPExcel->getActiveSheet()->mergeCells('I4:J4');
    $objPHPExcel->getActiveSheet()->mergeCells('K4:L4');
    $objPHPExcel->getActiveSheet()->setCellValue("B4", "EMPRESA"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E4", $empresa['emp_nombre']); 
    $objPHPExcel->getActiveSheet()->setCellValue("I4", "NIT"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K4", $empresa['emp_nit']); 
    $objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('I4')->getFont()->setBold( true );

    $objPHPExcel->getActiveSheet()->mergeCells('B5:D5');
    $objPHPExcel->getActiveSheet()->mergeCells('E5:H5');
    $objPHPExcel->getActiveSheet()->mergeCells('I5:J5');
    $objPHPExcel->getActiveSheet()->mergeCells('K5:L5');
    $objPHPExcel->getActiveSheet()->setCellValue("B5", "COLABORADOR"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E5", $empleado['emd_nombre']); 
    $objPHPExcel->getActiveSheet()->setCellValue("I5", "IDENTIFICACION"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K5", $empleado['emd_cedula']); 
    $objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('I5')->getFont()->setBold( true );

    $objPHPExcel->getActiveSheet()->mergeCells('B6:D6');
    $objPHPExcel->getActiveSheet()->mergeCells('E6:H6');
    $objPHPExcel->getActiveSheet()->mergeCells('I6:J6');
    $objPHPExcel->getActiveSheet()->mergeCells('K6:L6');
    $objPHPExcel->getActiveSheet()->setCellValue("B6", "CARGO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E6", $empleado['emd_cargo']); 
    $objPHPExcel->getActiveSheet()->setCellValue("I6", "FECHA DE INGRESO"); 
    $date3 = new DateTime($empleado["emd_fecha_ingreso"]);
    $objPHPExcel->getActiveSheet()->setCellValue("K6", PHPExcel_Shared_Date::PHPToExcel($date3));
    $objPHPExcel->getActiveSheet()->getStyle("K6")->getNumberFormat()->setFormatCode("dd/mm/yyyy");

    $objPHPExcel->getActiveSheet()->getStyle('B6')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('I6')->getFont()->setBold( true );


     /*CENTRO LAS LETRAS*/
    $objPHPExcel->getActiveSheet()->getStyle('C7:C10')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('G7:G10')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('H7:H10')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('J7:J10')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('K7:K10')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('G7:G10')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('G7:G10')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);


    $objPHPExcel->getActiveSheet()->getStyle('J7:J10')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('J7:J10')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);


    $objPHPExcel->getActiveSheet()->mergeCells('C7:F7');
    $objPHPExcel->getActiveSheet()->mergeCells('H7:I7');
    $objPHPExcel->getActiveSheet()->mergeCells('K7:L7');
    $objPHPExcel->getActiveSheet()->setCellValue("B7", "SEDE"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C7", $empleado['emd_sede']); 
    $objPHPExcel->getActiveSheet()->setCellValue("G7", "CENTRO DE COSTOS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H7", $empleado['emd_centro_costos']); 
    $objPHPExcel->getActiveSheet()->setCellValue("J7", "CIUDAD"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K7", $empleado['emd_ciudad']); 
    $objPHPExcel->getActiveSheet()->getStyle('B7')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('G7')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('J7')->getFont()->setBold( true );

    $objPHPExcel->getActiveSheet()->mergeCells('C8:F8');
    $objPHPExcel->getActiveSheet()->mergeCells('H8:I8');
    $objPHPExcel->getActiveSheet()->mergeCells('K8:L8');
    $objPHPExcel->getActiveSheet()->setCellValue("B8", "EPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C8", $empleado['ips_nombre']); 
    $objPHPExcel->getActiveSheet()->setCellValue("G8", "FECHA AFILIACIÓN EPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H8", $empleado['emd_fecha_afiliacion_eps']); 
    $objPHPExcel->getActiveSheet()->setCellValue("J8", "AFP"); 

    $item  = 'ips_id';
    $valor = $empleado['emd_afp_id']; 
    $afpX = ControladorEmpleados::getData('gi_ips', $item, $valor);
    $objPHPExcel->getActiveSheet()->setCellValue("K8", $afpX['ips_nombre']); 

    $objPHPExcel->getActiveSheet()->getStyle('B8')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('G8')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('J8')->getFont()->setBold( true );



    $item  = 'cae_emd_id_i';
    $valor = $empleado['emd_id']; 
    $concepto = ControladorEmpleados::getData('gi_casos_especiales', $item, $valor);
    

    $objPHPExcel->getActiveSheet()->mergeCells('C9:F9');
    $objPHPExcel->getActiveSheet()->mergeCells('H9:I9');
    $objPHPExcel->getActiveSheet()->mergeCells('K9:L9');
    $objPHPExcel->getActiveSheet()->setCellValue("B9", "CONCEPTO REHABILITACIÓN"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C9", $concepto['cae_con_rea_v']); 
    $objPHPExcel->getActiveSheet()->setCellValue("G9", "FECHA CONCEPTO"); 

    $dateX = new DateTime($concepto['cae_xon_fecha_d']);
    $objPHPExcel->getActiveSheet()->setCellValue("H9", PHPExcel_Shared_Date::PHPToExcel($dateX));
    $objPHPExcel->getActiveSheet()->getStyle("H9")->getNumberFormat()->setFormatCode("dd/mm/yyyy");

    $objPHPExcel->getActiveSheet()->setCellValue("J9", "DIAGNOSTICO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K9", $concepto['cae_condia_id_i']);
    $objPHPExcel->getActiveSheet()->getStyle('B9')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('G9')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('J9')->getFont()->setBold( true );


    $objPHPExcel->getActiveSheet()->mergeCells('C10:F10');
    $objPHPExcel->getActiveSheet()->mergeCells('H10:I10');
    $objPHPExcel->getActiveSheet()->mergeCells('K10:L10');
    $objPHPExcel->getActiveSheet()->setCellValue("B10", "CALIFICACIÓN PCL"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C10", $concepto['cae_cal_v']); 
    $objPHPExcel->getActiveSheet()->setCellValue("G10", "FECHA CALIFICACIÓN"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H10", $concepto['cae_cal_fecha_d']); 
    $objPHPExcel->getActiveSheet()->setCellValue("J10", "ENTIDAD"); 
    $itemXD  = 'ips_id';
    $valorXD = $concepto['cae_cal_entidad_i']; 
    $afpXD = ControladorEmpleados::getData('gi_ips', $itemXD, $valorXD);
    $objPHPExcel->getActiveSheet()->setCellValue("K10", $afpXD['ips_nombre']); 
    $objPHPExcel->getActiveSheet()->getStyle('B10')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('G10')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('J10')->getFont()->setBold( true );


    /*Incapacidades*/
    $objPHPExcel->getActiveSheet()->setCellValue("A12", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B12", "FECHA INICIAL"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C12", "FECHA FINAL"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D12", "DIAS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E12", "CLASIFICACIÓN"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F12", "DX");
    $objPHPExcel->getActiveSheet()->setCellValue("G12", "ORIGEN"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H12", "ESTADO TRAMITE"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I12", "FECHA ESTADO");
    $objPHPExcel->getActiveSheet()->setCellValue("J12", "No. RADICADO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K12", "VALOR PAGADO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("L12", "FECHA PAGO"); 
    $objPHPExcel->getActiveSheet()->getStyle('A12:L12')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('A12:L12')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('A12:L12')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
    $objPHPExcel->getActiveSheet()->getStyle('A12:L12')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    
    $item   = 'inc_emd_id';
    $valor  =  $empleado['emd_id'];
    $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);
    $totalDIas = 0;
    $valoPagado = 0;
    $i = 13;
    foreach ($incapacidades as $key => $value) {
        //$totalDIas += dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]);
        $valorP = 0;
        if($value["inc_valor"] != null && $value["inc_valor"] != ''){
            $valoPagado += $value["inc_valor"];
            $valorP = trim($value["inc_valor"]);
        } 

        $strFecha = null;
        if($value["inc_estado_tramite"] == 'POR RADICAR'){
            $strFecha = $value["inc_fecha_generada"];
        }else if($value["inc_estado_tramite"] == 'RADICADA'){
            $strFecha = $value["inc_fecha_radicacion"];
        }else if($value["inc_estado_tramite"] == 'SOLICITUD DE PAGO'){
            $strFecha = $value["inc_fecha_solicitud"];
        }else if($value["inc_estado_tramite"] == 'PAGADA'){
            $strFecha = $value["inc_fecha_pago"];
        }else if($value["inc_estado_tramite"] == 'SIN RECONOCIMIENTO'){
            $strFecha = $value["inc_fecha_negacion_"];
        }else{
            $strFecha = $value["inc_fecha_generada"];
        }

        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $date3 = new DateTime(explode(' ', $value["inc_fecha_inicio"])[0]);
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, PHPExcel_Shared_Date::PHPToExcel($date3));
        $objPHPExcel->getActiveSheet()->getStyle("B".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $date4 = new DateTime(explode(' ', $value["inc_fecha_final"])[0]);
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, PHPExcel_Shared_Date::PHPToExcel($date4));
        $objPHPExcel->getActiveSheet()->getStyle("C".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]));
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["inc_clasificacion"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["inc_diagnostico"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["inc_origen"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value["inc_estado_tramite"]); 
        $date5 = new DateTime($strFecha);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, PHPExcel_Shared_Date::PHPToExcel($date5));
        $objPHPExcel->getActiveSheet()->getStyle("I".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["inc_numero_radicado_v"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $value["inc_valor"]); 
        $objPHPExcel->getActiveSheet()->getStyle("K".$i)
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        if($value["inc_fecha_pago"] != null && $value["inc_fecha_pago"] != ''){
            $date6 = new DateTime($value["inc_fecha_pago"]);
            $objPHPExcel->getActiveSheet()->setCellValue("L".$i, PHPExcel_Shared_Date::PHPToExcel($date6));
            $objPHPExcel->getActiveSheet()->getStyle("L".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }
               
        $i++;
    }

    foreach(range('A','L') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }
    $i = $i+2;
    $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':C'.$i);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, "COMENTARIOS"); 
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i)
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    /*Espacio para comentarios*/
    $comentarios = ControladorIncapacidades::getDataFromLsql('incm_comentario_t, inc_fecha_inicio ', 'gi_incapacidades_comentarios JOIN gi_incapacidad ON inc_id = inm_inc_id_i ', "inc_cc_afiliado = '".$_GET['cc']."'", null, 'ORDER BY incm_fecha_comentario ASC', null );
    $i = $i+1;
    foreach($comentarios as $key => $val){
        $date3 = new DateTime(explode(' ', $val["inc_fecha_inicio"])[0]);
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, PHPExcel_Shared_Date::PHPToExcel($date3));
        $objPHPExcel->getActiveSheet()->getStyle("B".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $val["incm_comentario_t"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $i++;
    }


    $i = $i+3;
    $objPHPExcel->getActiveSheet()->mergeCells('J'.$i.':K'.$i);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, "FECHA DE REPORTE"); 
    $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->getStyle('J'.$i)
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $date7 = new DateTime(date('Y-m-d'));
    $objPHPExcel->getActiveSheet()->setCellValue("L".$i, PHPExcel_Shared_Date::PHPToExcel($date7));
    $objPHPExcel->getActiveSheet()->getStyle("L".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");


    $nombre='Reporte_incapacidades_por_colaborador_'.date("d-m-Y H-i-s").'.xlsx';
     // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$nombre.'"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $writer = new Xlsx($objPHPExcel);
    $writer->save('php://output');
        

    

