<?php
    require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';


    class MYPDF extends TCPDF {

        protected $processId = 0;
        protected $header = '';
        protected $footer = '';
        static $errorMsg = '';
        //Page header
        public function Header() {
            $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
           $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

           $this->Line(5, 5, $this->getPageWidth()-5, 5); 

           $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
           $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
           $this->Line(5, 5, 5, $this->getPageHeight()-5);

        }

        // Page footer
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            // Set font
            $this->SetFont('helvetica', 'N', 8);
            // Page number
            $this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
    }

    /*empezar el proceso del PDF*/
    $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("REPORTE INCAPACIDAD INDIVIDUAL");
    //$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    // set default header data

    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(true);
    $obj_pdf->setPrintFooter(true);
    $obj_pdf->SetAutoPageBreak(TRUE, 30);
    $obj_pdf->SetFont('times', '', 15);
    // set image scale factor

    $obj_pdf->AddPage();

 	$item  = 'emp_id';
    $valor = $_SESSION['cliente_id'];
    $empresa = ControladorClientes::ctrMostrarClientes($item, $valor);

    $item = 'inc_id';
	$valor = $_GET['id'];
	$respuesta = ControladorIncapacidades::ctrMostrarIncapacidades_edicion($item, $valor);
	$estado = "Original";
	$clasificacion = "Inicial";
    $atencion = $respuesta['atn_descripcion'];
	if($respuesta['inc_tipo_generacion']==1){ $estado= "Original";}else{$estado= "Transcrita";}
	if($respuesta['inc_clasificacion'] != 1){ $clasificacion = "Prorroga";}
	$valorEmpresa = 0;
	$valorAdminis = 0;
	$valorAjustes = 0;
	if($respuesta['inc_valor_pagado_empresa'] != null && $respuesta['inc_valor_pagado_empresa'] != ''){
	    $valorEmpresa = number_format($respuesta['inc_valor_pagado_empresa'], 0, '.', '.');
	}
	if($respuesta['inc_valor_pagado_eps'] != null && $respuesta['inc_valor_pagado_eps'] != ''){
	    $valorAdminis = number_format($respuesta['inc_valor_pagado_eps'],0, '.', '.');
	}
	if($respuesta['inc_valor_pagado_ajuste'] != null && $respuesta['inc_valor_pagado_ajuste'] != ''){
	    $valorAjustes = number_format($respuesta['inc_valor_pagado_ajuste'],0, '.', '.');
	}
	$valorPagado = '';
	$fechaPago__ = '';
	if($respuesta['inc_estado_tramite'] == 'SOLICITUD DE PAGO'){
    	$valorPagado = '';
	    $fechaPago__ = $respuesta['inc_fecha_solicitud'];
	}else if($respuesta['inc_estado_tramite'] == 'PAGADA'){
    	$valorPagado = '$ '.number_format($respuesta['inc_valor'], 0, '.','.');
	    $fechaPago__ = $respuesta['inc_fecha_pago'];
	}else if($respuesta['inc_estado_tramite'] == 'SIN RECONOCIMIENTO'){
    	$valorPagado = '';
	    $fechaPago__ = 'inc_fecha_negacion_';
	}else if($respuesta['inc_estado_tramite'] == 'RADICADA'){
    	$valorPagado = '';
	    $fechaPago__ = $respuesta['inc_fecha_radicacion'];
	}
	
	$fecha1 = explode(' ', $respuesta['inc_fecha_inicio'])[0];
	$fecha1 = explode('-', $fecha1);
	$fecha2 = explode(' ', $respuesta['inc_fecha_final'])[0];
	$fecha2 = explode('-', $fecha2);
	$fecha3 = explode(' ', $respuesta['inc_fecha_pago_nomina'])[0];
	$fecha3 = explode('-', $fecha3);
    
    //print_r($fechaPago__);
	if($fechaPago__ != '' && $fechaPago__ != null){
		$fechaPago__ = explode(' ', $fechaPago__)[0];
		$fechaPago__ = explode('-', $fechaPago__);	
		$fechaPago__ = $fechaPago__[2].'/'.$fechaPago__[1]."/".$fechaPago__[0];
	}
	
	$ivl = 0;

	if($respuesta['inc_ivl_v'] != null && $respuesta['inc_ivl_v'] != ''){
	    $ivl = str_replace(',', '', $respuesta['inc_ivl_v']);
	    $ivl = number_format( $ivl, 0, '.', '.');
	}

	$content = '<br/>
<br/>
<br/>
<table width="100%">
    <tr>
        <td width="80%">
            <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                <tr>
                    <td style="width:100%; text-align:center; font-size:12px;"><b>REPORTE INDIVIDUAL DE INCAPACIDAD</b></td>
                </tr>
            </table>
        </td>
        <td width="20%">
            <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
        </td>
    </tr>
</table>
<br/>
<br/>
<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
    <tr>
        <td style="width:50%;text-align:justify;"><b>'.$empresa['emp_nombre'].'</b></td>
        <td style="width:25%;text-align:justify;"></td>
        <td style="width:25%;text-align:justify;"></td>
    </tr>
    <tr>
        <td style="width:50%;text-align:justify;"></td>
        <td style="width:25%;text-align:justify;"></td>
        <td style="width:25%;text-align:justify;">Fecha: '.date('d/m/Y H:i:s').'</td>
    </tr>
</table>
<br>
<br>
<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="3px" cellspacing="0px" >
    <tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
        <td style="width:33%;text-align:justify;"><b>Identificación</b></td>
        <td style="text-align:justify;" colspan="2"><b>Nombre Empleado</b></td>
    </tr>
    <tr>
        <td style="width:33%;text-align:justify;">'.$respuesta['inc_cc_afiliado'].'</td>
        <td style="text-align:justify;" colspan="2">'.$respuesta['emd_nombre'].'</td>
    </tr>
    <tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
        <td style="width:33%;text-align:justify;"><b>Cargo del empleado</b></td>
        <td style="width:34%;text-align:justify;"><b>Administradora</b></td>
        <td style="width:33%;text-align:justify;"><b>IBL</b></td>
    </tr>
    <tr>
        <td style="width:33%;text-align:justify;">'.$respuesta['emd_cargo'].'</td>
        <td style="width:34%;text-align:justify;">'.$respuesta['ips_nombre'].'</td>
        <td style="width:33%;text-align:justify;">$ '.$ivl.'</td>
    </tr>
</table>
<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="3px" cellspacing="0px" >

    <tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
        
        <td style="width:25%;text-align:justify;"><b>Origen</b></td>
        <td style="width:25%;text-align:justify;"><b>Clasificación</b></td>
        <td style="width:25%;text-align:justify;"><b>Atención</b></td>
        <td style="width:25%;text-align:justify;"><b>Estado Incapacidad</b></td>
    </tr>
    <tr>
        
        <td style="width:25%;text-align:justify;">'.$respuesta['inc_origen'].'</td>
        <td style="width:25%;text-align:justify;">'.$clasificacion.'</td>
        <td style="width:25%;text-align:justify;">'.$atencion.'</td>
        <td style="width:25%;text-align:justify;">'.$estado.'</td>
    </tr>
</table>
<br/>
<hr>
<br/>
<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="3px" cellspacing="0px" width="100%">
    <tr>
        <td style="width:33%;text-align:justify;"><b>Fecha Inicio</b></td>
        <td style="width:33%;text-align:justify;"><b>Fecha Final</b></td>
        <td style="width:33%;text-align:justify;"><b>Días</b></td>
    </tr>
    <tr>
        <td style="width:33%;text-align:justify;">'.$fecha1[2].'/'.$fecha1[1].'/'.$fecha1[0].'</td>
        <td style="width:33%;text-align:justify;">'.$fecha2[2].'/'.$fecha2[1].'/'.$fecha2[0].'</td>
        <td style="width:33%;text-align:justify;">'.ControladorIncapacidades::dias_transcurridos($respuesta['inc_fecha_inicio'], $respuesta['inc_fecha_final']).'</td>
    </tr>
</table>
<br/>
<hr>
<br/>
<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="3px" cellspacing="0px" width="100%">
    <tr>
        <td style="width:25%;text-align:justify;"><b>Fecha de Pago Nómina</b></td>
        <td style="width:25%;text-align:justify;"><b>Valor Empresa</b></td>
        <td style="width:25%;text-align:justify;"><b>Valor Administradora</b></td>
        <td style="width:25%;text-align:justify;"><b>Valor Ajuste</b></td>
    </tr>
    <tr>
        <td style="width:25%;text-align:justify;">'.$fecha3[2].'/'.$fecha3[1].'/'.$fecha3[0].'</td>
        <td style="width:25%;text-align:justify;">$ '.$valorEmpresa.'</td>
        <td style="width:25%;text-align:justify;">$ '.$valorAdminis.'</td>
        <td style="width:25%;text-align:justify;">$ '.$valorAjustes.'</td>
    </tr>
</table>
<br/>
<hr>
<br/>
<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="3px" cellspacing="0px" width="100%">
    <tr>
        <td style="width:100%;text-align:justify;"><b>Diagnóstico</b></td>
    </tr>
    <tr>
        <td style="width:100%;text-align:justify;">'.$respuesta['dia_descripcion'].'</td>
    </tr>
</table>
<br/>
<hr>
<br/>
<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="3px" cellspacing="0px" width="100%">
    <tr>
        <td style="width:50%;text-align:justify;"><b>Profesional responsable</b></td>
        <td style="width:50%;text-align:justify;"><b>Institución que genera</b></td>
    </tr>
    <tr>
        
        <td style="width:50%;text-align:justify;">'.$respuesta['inc_profesional_responsable'].'</td>
        <td style="width:50%;text-align:justify;">'.$respuesta['inc_donde_se_genera'].'</td>
    </tr>
</table>
<br/>
<hr>
<br/>
<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="3px" cellspacing="0px" width="100%">
    <tr>
        <td style="width:100%;text-align:justify;"><b>Observación</b></td>
    </tr>
    <tr>
        <td style="width:100%;text-align:justify;">'.$respuesta['inc_observacion'].'</td>
    </tr>
</table>
<br/>
<hr>
<br/>
<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="3px" cellspacing="0px" width="100%">
    <tr>
        <td style="width:33%;text-align:justify;"><b>Estado Trámite</b></td>
        <td style="width:33%;text-align:justify;"><b>Fecha Estado Trámite</b></td>
        <td style="width:33%;text-align:justify;"><b>Valor</b></td>
    </tr>
    <tr>
        <td style="width:33%;text-align:justify;">'.$respuesta['inc_estado_tramite'].'</td>
        <td style="width:33%;text-align:justify;">'.$fechaPago__.'</td>
        <td style="width:33%;text-align:justify;">'.$valorPagado.'</td>
    </tr>
</table>';

$obj_pdf->writeHTML($content, true, false, false, false, '');
$nombre='Informe_incapacidad_inidividual'.date('H:i:s').'.pdf';
$obj_pdf->Output($nombre, 'I');