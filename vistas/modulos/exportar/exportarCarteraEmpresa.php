<?php

	$campos = "emp_nombre, emp_id";
	$tablas = "incapa_gein.gi_cartera JOIN gi_empresa ON emp_id = car_emp_id";
	$condic = "";
	$repsue = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condic, 'GROUP BY emp_id', 'ORDER BY emp_nombre');

	$objPHPExcel = new PHPExcel();

	$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

	$objPHPExcel->setActiveSheetIndex(0);

	$objPHPExcel->getProperties()->setCreator("GEIN")
	                     ->setLastModifiedBy("".$_SESSION['nombres'])
	                     ->setTitle("GEIN - Cartera")
	                     ->setSubject("Cartera")
	                     ->setDescription("Descarga la cartera, registrada en el sistema")
	                     ->setKeywords("office 2007 openxml php")
	                     ->setCategory("Cartera");

	$objPHPExcel->getActiveSheet();

	$borders = array(
			'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);

    foreach ($repsue as $empresas => $empresa) {


   		$campos = "emp_nombre, emp_direccion, emp_nit, emp_telefono";
   		$tablas = "gi_empresa";
   		$condicion = "emp_id = ".$empresa['emp_id'];

   		$newEmpresa = ModeloTesoreria::mdlMostrarUnitario($campos, $tablas, $condicion);

   		$newHoja = $objPHPExcel->createSheet($empresas);

   		$newHoja->setTitle($empresa['emp_nombre']);
   		$newHoja->getStyle('A1:G1')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$newHoja->getStyle('B1:G1')->applyFromArray($borders);
		$newHoja->getStyle('B2:G2')->applyFromArray($borders);

	    $newHoja->setCellValue("B1", "EMPRESA"); 
	    $newHoja->setCellValue("C1", "NIT"); 
	    $newHoja->mergeCells('D1:F1');
	    $newHoja->setCellValue("D1", "DIRECCION"); 
	    $newHoja->setCellValue("G1", "TELEFONO"); 

	    $newHoja->getStyle('B1:G1')->getFont()->setBold( true );

	    $newHoja->setCellValue("B2", mb_strtoupper($empresa['emp_nombre'])); 
	    $newHoja->setCellValue("C2", mb_strtoupper($newEmpresa['emp_nit'])); 
	    $newHoja->mergeCells('D2:F2');
	    $newHoja->setCellValue("D2", mb_strtoupper($newEmpresa['emp_direccion'])); 
	    $newHoja->setCellValue("G2", mb_strtoupper($newEmpresa['emp_telefono'])); 


	    $newHoja->setCellValue("B5", "ADMINISTRADORA"); 
	    $newHoja->setCellValue("C5", "USUARIO"); 
	    $newHoja->setCellValue("D5", "CLAVE"); 
	    $newHoja->getStyle('B5:D5')->getFont()->setBold( true );
	    $newHoja->getStyle('A5:G5')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$newHoja->getStyle('B5:D5')->applyFromArray($borders);
		$newHoja->getStyle('B6:D6')->applyFromArray($borders);


	    $item = 'car_emp_id';
	    $valo = $empresa['emp_id'];
	    $cartera = ControladorCartera::ctrMostrarCarteraDeglosada($item, $valo);



	    $i = 9;
	    $newHoja->setCellValue("A8", 'ITEM');
	    $newHoja->setCellValue("B8", "EPS (ADMINISTRADORAS)"); 
	    $newHoja->setCellValue("C8", "CODIGO"); 
	    $newHoja->setCellValue("D8", "NIT ENTIDAD"); 
	    $newHoja->setCellValue("E8", 'COLABORADORES');
	    $newHoja->setCellValue("F8", "SOLICITUD"); 
	    $newHoja->setCellValue("G8", "RADICACION"); 
	    $newHoja->setCellValue("H8", "COLABORADORES ACTIVOS"); 
	    $newHoja->setCellValue("I8", 'PAZ Y SALVO');
	    $newHoja->setCellValue("J8", "RESPUESTA"); 
	    $newHoja->setCellValue("K8", "VALOR CARTERA"); 
	    $newHoja->setCellValue("L8", "OBSERVACIONES"); 

	    $newHoja->getStyle('A8:L8')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$newHoja->getStyle('A8:L8')->applyFromArray($borders);
	    $newHoja->getStyle('A8:L8')->getFont()->setBold( true );

	    foreach ($cartera as $key => $value) {

	    	$valor = 0;
            if(!empty($value['car_valor'])){
                $valor = number_format(trim($value['car_valor']), 0);
            }


            $campo = "COUNT(*) as total";
		    $tabla = "gi_empleados";
		    $condi = "emd_emp_id = ".$empresa['emp_id']." AND (emd_eps_id = ".$value['ips_id']." OR emd_afp_id = ".$value['ips_id']." OR emd_arl_id = ".$value['ips_id'].")";
		    $TotalEmprelados = ModeloTesoreria::mdlMostrarUnitario($campo, $tabla, $condi);

	    	$newHoja->setCellValue("A".$i, ($key+1));
		    $newHoja->setCellValue("B".$i,  mb_strtoupper($value['ips_nombre'])); 
		    $newHoja->setCellValue("C".$i, $value['ips_codigo']); 
		    $newHoja->setCellValue("D".$i, $value['ips_nit']); 
		    $newHoja->setCellValue("E".$i, $TotalEmprelados['total']);
		    $newHoja->setCellValue("F".$i, $value['car_fecha_cre']); 
		    $newHoja->setCellValue("G".$i, $value['car_fecha_rad']); 
		    $newHoja->setCellValue("H".$i, "COLABORADORES ACTIVOS"); 
		    $newHoja->setCellValue("I".$i, $value['car_paz_salvo']);
		    $newHoja->setCellValue("J".$i, $value['car_respuesta']); 
		    $newHoja->setCellValue("K".$i, $valor); 
		    $newHoja->setCellValue("L".$i, $value['car_observacion']); 

		    $newHoja->getStyle('A'.$i.':L'.$i)->applyFromArray($borders);

		    foreach(range('A','L') as $columnID) {
			    $newHoja->getColumnDimension($columnID)
			        ->setAutoSize(true);
			}
		    $i++;
	    }

    }

     // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="cartera'.date('Y_m_d_H_i_s').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');


