<?php
    session_start();
    require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../controladores/tesoreria.controlador.php';
    require_once '../../../modelos/incapacidades.modelo.php';
    require_once '../../../modelos/tesoreria.modelo.php';

?>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <div class="col-lg-6 col-xs-12">
            <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            <?php
                                $item = null;
                                $valor = null;
                                if($_SESSION['cliente_id'] != 0){
                                    $item = 'inc_empresa';
                                    $valor = $_SESSION['cliente_id'];
                                    $pagadas = ControladorTesoreria::ctrMostrarGeneralPagos_byCliente($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                                }else{
                                    $pagadas = ControladorTesoreria::ctrMostrarGeneralPagos($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                                }

                                echo count($pagadas);
                            ?>
                        </h3>
                        <p>PAGADAS</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-done-all"></i>
                    </div>
                    <a href="incapacidades" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            <?php
                                $item = null;
                                $valor = null;
                                if($_SESSION['cliente_id'] != 0){
                                    $item = 'inc_empresa';
                                    $valor = $_SESSION['cliente_id']; 
                                }

                                $pagadas = ControladorTesoreria::ctrTotalValorIncapacidades($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                                echo number_format($pagadas['total'], 0, ',', '.');
                            ?>
                        </h3>
                        <p>VALOR PAGADO</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-cash"></i>
                    </div>
                    <a href="incapacidades" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>  
    </div>
    <div class="col-md-7">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Grafico de pagos por Administradora</h3>
            </div>
            <div class="box-body">
                <canvas id="myChart" style="height:250px"></canvas>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Registro de pagos</h3>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-hover table-condensed tblGeneralPagos">
                    <thead>
                        <tr>
                            <th style="width: 10px;">#</th>
                            <?php 
                                if($_SESSION['cliente_id'] == 0){ 
                                    echo '<th>EMPRESA</th>';
                                }
                            ?>
                            <th>EMPLEADO</th>
                            <th>CÉDULA</th>
                            <th>ADMINISTRADORA</th>
                            <th>VALOR PAGADO</th>
                            <th>FECHA DE PAGO</th>
                            <th>FECHA INICIO</th>
                            <th>DIAS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $item = null;
                            $valor = null;
                            if($_SESSION['cliente_id'] != 0){
                                $item = 'inc_empresa';
                                $valor = $_SESSION['cliente_id'];
                                $pagadas = ControladorTesoreria::ctrMostrarGeneralPagos_byCliente($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                            }else{
                                $pagadas = ControladorTesoreria::ctrMostrarGeneralPagos($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                            }

                            foreach ($pagadas as $key => $value) {
                                echo '<tr>';
                                    echo '<td>'.($key+1).'</td>';
                                    if($_SESSION['cliente_id'] == 0){ 
                                        echo '<td>'.$value['emp_nombre'].'</td>';
                                    }
                                    echo '<td>'.$value['emd_nombre'].'</td>';
                                    echo '<td>'.$value['inc_cc_afiliado'].'</td>';

                                    if($value['inc_clasificacion'] != 4){
                                        if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                                            echo '<td class="text-uppercase">'.($value["arl_nombre"]).'</td>'; 
                                        }else{
                                            echo '<td class="text-uppercase">'.($value["ips_nombre"]).'</td>'; 
                                        }
                                    }else{
                                        echo '<td class="text-uppercase">'.($value["afp_nombre"]).'</td>';    
                                    }
                                    
                                    if(!empty($value['inc_valor']) && $value['inc_valor'] != ''){
                                        echo '<td>$ '.number_format($value['inc_valor'], 0, ',', '.').'</td>';
                                    }else{
                                        echo '<td></td>';
                                    }
                                    echo '<td>'.$value['inc_fecha_pago'].'</td>';
                                    echo '<td>'.explode(' ', $value['inc_fecha_inicio'])[0].'</td>';
                                    echo '<td>'.dias_transcurridos($value['inc_fecha_inicio'], $value['inc_fecha_final']) .'</td>';
                                echo '</tr>';

                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function(){
        crearBarrasporPagar();
        $('.tblGeneralPagos').DataTable({
            "language" : {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
            ,"lengthMenu": [
                [10, 25, 50, 100, 200, -1], 
                [10, 25, 50, 100, 200, "Todos"]
            ]
        });
    });

function crearBarrasporPagar(){
    <?php

       /*Obtenemos las afp porque son por clasificacion +180 */        
        $campo = 'SUM(inc_valor) as total, afp_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = 'inc_clasificacion = 4 ';
        if($_SESSION['cliente_id'] != 0){
            $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }else{
            $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY afp_nombre', '');
        
        $label = '';
        $Qdias = '';
        $colors = '';
        foreach ($respuesta as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */

            $label .= "\"".$value['afp_nombre']."\" , ";
            $Qdias .= $value['total']." , ";
            $colors .= "getRandomColor(),";
        }

        /*Obtenemos las ARL porque son por accidente de transito*/
        $campo = 'SUM(inc_valor) as total, arl_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = "inc_clasificacion != 4 AND inc_origen = 'ACCIDENTE LABORAL' ";
        if($_SESSION['cliente_id'] != 0){
            $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }else{
            $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $respuesta2 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY arl_nombre', '');
        foreach ($respuesta2 as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */

            $label .= "\"".$value['arl_nombre']."\" , ";
            $Qdias .= $value['total']." , ";
            $colors .= "getRandomColor(),";
        }

        /*Obtenemos las EPS porque son por accidente de transito*/
        $campo = 'SUM(inc_valor) as total, ips_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = "inc_clasificacion != 4 AND inc_origen != 'ACCIDENTE LABORAL' ";
        if($_SESSION['cliente_id'] != 0){
            $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }else{
            $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $respuesta3 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY ips_nombre', '');
        foreach ($respuesta3 as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */

            $label .= "\"".$value['ips_nombre']."\" , ";
            $Qdias .= $value['total']." , ";
            $colors .= "getRandomColor(),";
        }
    ?>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php echo $label; ?>],
            datasets: [
                {
                    label: 'Valor pagado',
                    data: [<?php echo $Qdias; ?>],
                    backgroundColor: [<?php echo $colors; ?>]
                }
            ]
        },
        options: {
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        return "$" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                        });
                    }
                }
            }
        }
    });
}

function scaleLabel (valuePayload) {
    return Number(valuePayload.value).toFixed(2).replace('.',',') + '$';
}

function getRandomColor() {
   return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
}
</script>

<?php
    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }

?>
