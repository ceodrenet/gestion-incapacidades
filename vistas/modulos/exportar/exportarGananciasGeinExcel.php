<?php
    
    $objPHPExcel = new PHPExcel();

    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Ganancias")
                             ->setSubject("Colaboradores historico")
                             ->setDescription("Descarga del historico de Colaboradores, registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Colaboradores");


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:G1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold( true );
    

    $objPHPExcel->getActiveSheet()->setTitle('Ganancias x Empresas Mensual');


    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "NIT"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Ingreso Mes"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Porcentaje"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Comisión"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Facturado"); 



    $i = 2;

    $fechaFinal = $_GET['fechaFinal'];
    $fechaInicial = $_GET['fechaInicial'];
    $cliente_id = $_GET['cliente_id'];

    $valorTotal = 0;
    $strCampos = "emp_nombre , emp_id, emp_nit, emp_porcentaje_i ";
    $strTablas = "gi_empresa";
    $condicion = 'emp_estado  = 1';
    $resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos, $strTablas, $condicion, null, " ORDER BY emp_nombre ASC"); 

    foreach ($resultado as $key => $value) {

        /*Vamos a obtener las ganancias de esa empresa en ese intervalo de tiempo */
        $strNewCampos = "SUM(inc_valor) as total";
        $strNewTablas = "gi_incapacidad";
        $strCondicion = "inc_fecha_pago BETWEEN '".$fechaInicial."' AND '".$fechaFinal."' AND   inc_empresa = ".$value['emp_id'];
        $respuestaEmp = ModeloTesoreria::mdlMostrarUnitario($strNewCampos, $strNewTablas, $strCondicion);

        $valorInc = $respuestaEmp['total'] * ($value['emp_porcentaje_i']/100);
        $valorTotal += $valorInc;

        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nit"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, mb_strtoupper($value['emp_nombre']));
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $respuestaEmp['total']); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value['emp_porcentaje_i']); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $valorInc); 
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, ''); 
        $i++;         
    }
    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, "Total Ganancia Mensual"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $valorTotal);

    foreach(range('A','G') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    
    }


    $objPHPExcel->getActiveSheet()->getStyle('D2:D'.$i)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

    $objPHPExcel->getActiveSheet()->getStyle('F2:F'.$i)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);


    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="GananciasPeriodo'.$fechaInicial.'_hasta_'.$fechaFinal."_".date('H:i:s').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');
    

