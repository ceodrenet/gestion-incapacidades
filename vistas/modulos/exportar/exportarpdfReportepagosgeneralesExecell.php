<?php
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }

    
    
    require_once dirname(__FILE__) . '/../../../extenciones/Excel.php';
    require_once dirname(__FILE__) . '/../../../controladores/mail.controlador.php';
    require_once dirname(__FILE__) . '/../../../controladores/plantilla.controlador.php'; 
    require_once dirname(__FILE__) . '/../../../controladores/incapacidades.controlador.php';
    
    require_once dirname(__FILE__) . '/../../../modelos/dao.modelo.php';
    require_once dirname(__FILE__) . '/../../../modelos/tesoreria.modelo.php';



    $objPHPExcel = new PHPExcel();

    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Reporte General de Pagos")
                             ->setSubject("Reporte General de Pagos")
                             ->setDescription("Descarga el Reporte General de Pagos registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Reporte General de Pagos");


   
    $objPHPExcel->getActiveSheet()->setTitle('Pagos Registrados');

    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "EMPRESA"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "ADMINISTRADORA"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "VALOR PAGADO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "FECHA DE PAGO");  

    $where = '';
    if($_SESSION['cliente_id'] != 0){
        $where = 'inc_empresa = '.$_SESSION['cliente_id']." AND ";
    }

    $campos = "SUM(inc_valor) as inc_valor, ips_nombre, inc_fecha_pago, emp_nombre";
    $tablas = "reporte_tesoreria";
    $where_ = $where." inc_fecha_pago between '".$_GET['fi']."' AND '".$_GET['ff']."' AND inc_clasificacion != '4' AND inc_origen != 'ACCIDENTE LABORAL' ";
    $pagadasIps = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tablas,$where_, 'group by ips_nombre, inc_fecha_pago', 'ORDER By inc_fecha_pago DESC');
    $valorPagadoF = 0;
    $i = 2;
    foreach ($pagadasIps  as $key => $value) {
        $administrador = '';
      
        $administrador = $value["ips_nombre"];
            
        $valor = 0;
        if(!empty($value['inc_valor']) && $value['inc_valor'] != ''){
            $valor = $value['inc_valor'];
            $valorPagadoF += $value['inc_valor'];
        }
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value['emp_nombre']); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $administrador); 
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $valor); 
        $date3 = new DateTime($value["inc_fecha_pago"]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, PHPExcel_Shared_Date::PHPToExcel($date3));
        $objPHPExcel->getActiveSheet()->getStyle("E".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $i++;
    }

	 /*ARL*/
    $campos = "SUM(inc_valor) as inc_valor, arl_nombre, inc_fecha_pago, emp_nombre";
    $tablas = "reporte_tesoreria";
    $where_ = $where." inc_fecha_pago between '".$_GET['fi']."' AND '".$_GET['ff']."'  AND inc_clasificacion != '4' AND inc_origen = 'ACCIDENTE LABORAL' ";
    $pagadasArl = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tablas,$where_, 'group by arl_nombre, inc_fecha_pago', 'ORDER By inc_fecha_pago DESC');
    
    foreach ($pagadasArl   as $key => $value) {
        $administrador = '';
      
        $administrador = $value["arl_nombre"];
            
        $valor = 0;
        if(!empty($value['inc_valor']) && $value['inc_valor'] != ''){
            $valor = $value['inc_valor'];
            $valorPagadoF += $value['inc_valor'];
        }
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value['emp_nombre']); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $administrador); 
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $valor); 
        $date3 = new DateTime($value["inc_fecha_pago"]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, PHPExcel_Shared_Date::PHPToExcel($date3));
        $objPHPExcel->getActiveSheet()->getStyle("E".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $i++;
    }

    $campos = "SUM(inc_valor) as inc_valor, afp_nombre, inc_fecha_pago, emp_nombre";
    $tablas = "reporte_tesoreria";
    $where_ = $where." inc_fecha_pago between '".$_GET['fi']."' AND '".$_GET['ff']."' AND inc_clasificacion = '4' ";
    $pagadasAfp = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tablas,$where_, 'group by afp_nombre, inc_fecha_pago', 'ORDER By inc_fecha_pago DESC');
    
    foreach ($pagadasAfp  as $key => $value) {
        $administrador = '';
      
        $administrador = $value["afp_nombre"];
            
        $valor = 0;
        if(!empty($value['inc_valor']) && $value['inc_valor'] != ''){
            $valor = $value['inc_valor'];
            $valorPagadoF += $value['inc_valor'];
        }
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value['emp_nombre']); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $administrador); 
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $valor); 
        $date3 = new DateTime($value["inc_fecha_pago"]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, PHPExcel_Shared_Date::PHPToExcel($date3));
        $objPHPExcel->getActiveSheet()->getStyle("E".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $i++;
    }

    $objPHPExcel->getActiveSheet()->setCellValue("C".$i, "TOTAL PAGADO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $valorPagadoF);

     $objPHPExcel->getActiveSheet()->getStyle('D1:D'.$i)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

    foreach(range('A','D') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    
    }

    $nombre='Reporte_Pagos_general_'.date("d-m-Y H-i-s").'.xlsx';
     // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$nombre.'"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');
            

    

