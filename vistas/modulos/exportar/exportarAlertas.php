<?php
    
    $objPHPExcel = new PHPExcel();

    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - COLABORADORES")
                             ->setSubject("Colaboradores historico")
                             ->setDescription("Descarga del historico de Colaboradores, registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Colaboradores");


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:X1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:X1')->getFont()->setBold( true );
    

    $objPHPExcel->getActiveSheet()->setTitle('COLABORADORES');


    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Tipo de identificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Identificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Nombre"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Administradora EPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Administradora ARL"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Administradora AFP"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Dias de Incapacidad"); 
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Estado Empleado"); 


    $campos = 'emp_nombre, emd_tipo_identificacion, emd_cedula, emd_nombre, emd_fecha_ingreso, CASE emd_estado WHEN 1 THEN \'ACTIVO\' WHEN 0 THEN \'NO ACTIVO\' END AS emd_estado , a.ips_nombre as ips_nombre, b.ips_nombre as emd_arl_id, c.ips_nombre as emd_afp_id, Tiempo_dias';
    $tablas = 'reporte_alarmas JOIN gi_empleados ON emd_cedula = inc_cc_afiliado LEFT JOIN gi_ips a ON a.ips_id = emd_eps_id LEFT JOIN gi_ips b ON b.ips_id = emd_arl_id LEFT JOIN gi_ips c ON c.ips_id = emd_afp_id JOIN gi_empresa ON emp_id = emd_emp_id';
    
    if($_GET['exportarAlertas'] == 90){
        $condiciones  = 'Tiempo_dias > 90 AND Tiempo_dias < 120';
    }else if($_GET['exportarAlertas'] == 120){
        $condiciones  = 'Tiempo_dias > 120 AND Tiempo_dias < 180';
    }else if($_GET['exportarAlertas'] == 180){
        $condiciones  = 'Tiempo_dias > 180 AND Tiempo_dias < 540';
    }else if($_GET['exportarAlertas'] == 540){
        $condiciones  = 'Tiempo_dias > 540';
    }

    if($_SESSION['cliente_id'] != 0){
        $condiciones .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
    }
    //echo "SELECT ".$campos." FROM ".$tablas." WHERE ".$condiciones." ORDER BY emp_nombre ASC;";
    $alarma1 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones, '', 'ORDER BY emp_nombre ASC');         
    
    //print_r($alarma1);
    
    $i = 2;
    foreach ($alarma1 as $key => $value) {


        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["emd_tipo_identificacion"]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_cedula"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["emd_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["ips_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["emd_arl_id"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value["emd_afp_id"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value["Tiempo_dias"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["emd_estado"]); 
        $i++;         
    }


    foreach(range('A','J') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    
    }


    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="AlertasIncMasde'.$_GET['exportarAlertas'].'dias'.date('H:i:s').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');
    

