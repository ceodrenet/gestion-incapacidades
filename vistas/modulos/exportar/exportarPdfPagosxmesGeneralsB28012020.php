<?php
    session_start();
    require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';

    require_once __DIR__.'/../../../controladores/archivos.controlador.php';
    require_once __DIR__.'/../../../controladores/calendario.controlador.php';
    require_once __DIR__.'/../../../controladores/clientes.controlador.php';
    require_once __DIR__.'/../../../controladores/diagnostico.controlador.php';
    require_once __DIR__.'/../../../controladores/empleados.controlador.php';
    require_once __DIR__.'/../../../controladores/entidades.controlador.php';
    require_once __DIR__.'/../../../controladores/incapacidades.controlador.php';
    require_once __DIR__.'/../../../controladores/ips.controlador.php';
    require_once __DIR__.'/../../../controladores/perfiles.controlador.php';
    require_once __DIR__.'/../../../controladores/seguridad.controlador.php';
    require_once __DIR__.'/../../../controladores/tesoreria.controlador.php';
    require_once __DIR__.'/../../../controladores/usuarios.controlador.php';
    require_once __DIR__.'/../../../controladores/mail.controlador.php';
    require_once __DIR__.'/../../../controladores/cartera.controlador.php';

    require_once __DIR__.'/../../../modelos/archivos.modelo.php';
    require_once __DIR__.'/../../../modelos/calendario.modelo.php'; 
    require_once __DIR__.'/../../../modelos/clientes.modelo.php';
    require_once __DIR__.'/../../../modelos/diagnostico.modelo.php';
    require_once __DIR__.'/../../../modelos/empleados.modelo.php';
    require_once __DIR__.'/../../../modelos/entidades.modelo.php';
    require_once __DIR__.'/../../../modelos/incapacidades.modelo.php';
    require_once __DIR__.'/../../../modelos/ips.modelo.php';
    require_once __DIR__.'/../../../modelos/perfiles.modelo.php';
    require_once __DIR__.'/../../../modelos/seguridad.modelo.php';
    require_once __DIR__.'/../../../modelos/tesoreria.modelo.php';
    require_once __DIR__.'/../../../modelos/usuarios.modelo.php';
    require_once __DIR__.'/../../../modelos/cartera.modelo.php';


    
    function sanear_string($string) { 
        $string = str_replace( array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string );
        $string = str_replace( array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string ); 
        $string = str_replace( array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string ); 
        $string = str_replace( array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string ); 
        $string = str_replace( array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string ); 
        $string = str_replace( array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C',), $string ); 
        //Esta parte se encarga de eliminar cualquier caracter extraño 
        //$string = str_replace( array("\\", "¨", "º", "-", "~", "#", "@", "|", "!", "\"", "·", "$", "%", "&", "/", "(", ")", "?", "'", "¡", "¿", "[", "^", "`", "]", "+", "}", "{", "¨", "´", ">“, “< ", ";", ",", ":", "."), '', $string ); 
        return $string; 
    }

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }


    class MYPDF extends TCPDF {

        protected $processId = 0;
        protected $header = '';
        protected $footer = '';
        static $errorMsg = '';
        //Page header
        public function Header() {
            $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
           $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

           $this->Line(5, 5, $this->getPageWidth()-5, 5); 

           $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
           $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
           $this->Line(5, 5, 5, $this->getPageHeight()-5);

        }

        // Page footer
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            // Set font
            $this->SetFont('helvetica', 'N', 8);
            // Page number
            $this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
    }


    /*empezar el proceso del PDF*/
    $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("Reporte Detalle de Pagos x Mes");
    //$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    // set default header data

    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(true);
    $obj_pdf->setPrintFooter(true);
    $obj_pdf->SetAutoPageBreak(TRUE, 20);
    $obj_pdf->SetFont('times', '', 15);
    // set image scale factor

    $obj_pdf->AddPage('L', 'A4'); 


   

    $empres = '';
    $nit = '';
    if($_SESSION['cliente_id'] == 0){
        $title = '<th style="text-align:center;">Empresa</th>';
        $title2 = '<th></th>';
        $empres = '<td style="width:9%;text-align:justify;"> </td>
            <td style="width:50%;text-align:justify;"></td>';
        $nit = '<td style="width:9%;text-align:justify;"></td>
            <td style="width:50%;text-align:justify;"></td>';
    }else{
        $item  = 'emp_id';
        $valor = $_SESSION['cliente_id']; 
        $empresa = ControladorClientes::ctrMostrarClientes($item, $valor);

        $empres = '<td style="width:9%;text-align:justify;">Empresa </td>
            <td style="width:50%;text-align:justify;">:&nbsp; <b>'.$empresa['emp_nombre']."</b></td>";
        $nit = '<td style="width:9%;text-align:justify;">NIT </td>
            <td style="width:50%;text-align:justify;">:&nbsp; <b>'.$empresa['emp_nit']."</b></td>";
    }

    $semes1 = array('01', '02', '03', '04', '05', '06');
    $semes2 = array('07', '08', '09', '10', '11', '12');
    $seme = $_POST['cmbSemestres'];
    $tablaThead = '';
    if($seme == 1){
        $year = $_POST['anhoBusqueda'].'-01-01';
        $otherYear = $_POST['anhoBusqueda'].'-06-30';
        $tablaThead = '<th colspan="3" style="text-align: center;width:15.6%">ENERO</th><th colspan="3" style="text-align: center;width:15.6%">FEBRERO</th><th colspan="3" style="text-align: center;width:15.6%">MARZO</th><th colspan="3" style="text-align: center;width:15.6%">ABRIL</th><th colspan="3" style="text-align: center;width:15.6%">MAYO</th><th colspan="3" style="text-align: center;width:15.6%">JUNIO</th>';
    }else{
        $year = $_POST['anhoBusqueda'].'-07-01';
        $otherYear = $_POST['anhoBusqueda'].'-12-31';
        $tablaThead = '<th colspan="3" style="text-align: center;width:15.6%">JULIO</th><th colspan="3" style="text-align: center;width:15.6%">AGOSTO</th><th colspan="3" style="text-align: center;width:15.6%">SEPTIEMBRE</th><th colspan="3" style="text-align: center;width:15.6%">OCTUBRE</th><th colspan="3" style="text-align: center;width:15.6%">NOVIEMBRE</th><th colspan="3" style="text-align: center;width:15.6%">DICIEMBRE</th>';
    }


    $filepath = '';
    if(isset($_POST['imagen']) && $_POST['imagen'] != ''){
        $baseFromJavascript = base64_decode($_POST['fila']);


        $directorio = __DIR__."/estadisticas/";
        if (!file_exists($directorio)) {
            mkdir($directorio, 0755);
        }
        // Nuestro base64 contiene un esquema Data URI (data:image/png;base64,)
        // que necesitamos remover para poder guardar nuestra imagen
        // Usa explode para dividir la cadena de texto en la , (coma)
        // El segundo item del array base_to_php contiene la información que necesitamos (base64 plano)
        // y usar base64_decode para obtener la información binaria de la imagen
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $baseFromJavascript));

        // Proporciona una locación a la nueva imagen (con el nombre y formato especifico)
        $filepath = $directorio.$_POST['imagen'].".png"; // or image.jpg

        // Finalmente guarda la imágen en el directorio especificado y con la informacion dada
        file_put_contents($filepath, $data);
    }

  
    $content = '';
    $content = '
    <br/>
    <br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:8%" style="text-align:center;"></td>
                        <td style="width:70%; text-align:center; font-size:12px;"><b>REPORTE DE PAGOS POR MES</b></td>
                        <td style="width:20%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:20%"></td>
                        <td style="width:50%;text-align:center;">DETALLE MENSUAL DE PAGOS</td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            '.$empres.'
            <td style="width:14%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;">Fecha:&nbsp;&nbsp;&nbsp; '.date('d/m/Y H:i:s').'</td>
        </tr>
        <tr>
            '.$nit.'
            <td style="width:14%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
        </tr>
        
    </table>
    <br/>
    <br/>
    <img src="'.$filepath.'" alt="imagen del grafico" width="800px" height="400px">
    <br/>
    <br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <thead>
            <tr style="text-align: center;background-color:#bdbdbd;border:solid 1px;">
                <th width="8%" style="text-align:center;">
                    
                </th>
                '.$tablaThead.'
            </tr>
            <tr>
                <th width="8%" style="text-align:center;"></th>
                <th width="8%" style="text-align:center;">VALOR</th>
                <th width="3.8%" style="text-align:center;">#</th>
                <th width="3.8%" style="text-align:center;">%</th>
                <th width="8%" style="text-align:center;">VALOR</th>
                <th width="3.8%" style="text-align:center;">#</th>
                <th width="3.8%" style="text-align:center;">%</th>
                <th width="8%" style="text-align:center;">VALOR</th>
                <th width="3.8%" style="text-align:center;">#</th>
                <th width="3.8%" style="text-align:center;">%</th>
                <th width="8%" style="text-align:center;">VALOR</th>
                <th width="3.8%" style="text-align:center;">#</th>
                <th width="3.8%" style="text-align:center;">%</th>
                <th width="8%" style="text-align:center;">VALOR</th>
                <th width="3.8%" style="text-align:center;">#</th>
                <th width="3.8%" style="text-align:center;">%</th>
                <th width="8%" style="text-align:center;">VALOR</th>
                <th width="3.8%" style="text-align:center;">#</th>
                <th width="3.8%" style="text-align:center;">%</th>
            </tr>
        </thead>';

        $empres = '';
        $admini = '';
        $adminP = '';
        $ajuste = '';
        $totalTH = '';
        
        $porcob = '';
        $pagada = '';
        $negada = '';
        $busque = 0;
        if($seme == 1){
            $busque  = $semes1;
        }else{
            $busque  = $semes2;
        }
         $whereCliente = '';
        if($_SESSION['cliente_id'] != 0){
            $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
        }

        for ($i=0; $i < 6 ; $i++) { 
            $totalLC = 0;
            $totalMC = 0;
            $totalNC = 0;
            $incapacidadesTotal = 0;

            /*Total incapacidades de ese mes*/
            $campos = 'COUNT(*) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda'];
            $incapacidadesTotal = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $incapacidadesTotal = $incapacidadesTotal['total'];
            $totalNC = $incapacidadesTotal;



            /*Incapaciodades Pagadas Empresa*/
            $campos = 'SUM(inc_valor_pagado_empresa) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda'];
            
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $empres .= '<td style="text-align:center;width:8%;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
            $totalLC += $incapacidades['total'];
            $totalInEmp = $incapacidades['total'];


            /*Incapacidades de ese Año*/
            $campos = 'COUNT(*) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_valor_pagado_empresa IS NOT NULL';
            //echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $totalInc = 0;
            if($incapacidades['total'] > 0){
                $totalInc = $incapacidades['total'];
            }
            $empres .= '<td style="text-align:center;width:3.8%;">'.$totalInc.'</td>';
            //$totalMC += $totalInc;



            


            /*Administradora*/
            /*Incapaciodades Pagadas*/
            $campos = 'SUM(inc_valor_pagado_eps) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda']."";
        
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $admini .= '<td style="text-align:center;width:8%;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
            $adminP .= '<td style="text-align:center;width:8%;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
            $totalLC += $incapacidades['total'];
            $totalInAdmi = $incapacidades['total'];



            /*Incapacidades de ese Año*/
            $campos = 'COUNT(*) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_valor_pagado_eps IS NOT NULL';
            //echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $totalInc = 0;
            if($incapacidades['total'] > 0){
                $totalInc = $incapacidades['total'];
            }
            $admini .= '<td style="text-align:center;width:3.8%;">'.$totalInc.'</td>';
            $adminP .= '<td style="text-align:center;width:3.8%;">'.$totalInc.'</td>';
            //$totalMC += $totalInc;

        


            /*Ajuste*/
            /*Incapaciodades Pagadas*/
            $campos = 'SUM(inc_valor_pagado_ajuste) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda'];
            
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $ajuste .= '<td style="text-align:center;width:8%;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
            $totalLC += $incapacidades['total'];
            $totalInAju = $incapacidades['total'];



            /*Incapacidades de ese Año*/
            $campos = 'COUNT(*) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_valor_pagado_ajuste IS NOT NULL';
            //echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $totalInc = 0;
            if($incapacidades['total'] > 0){
                $totalInc = $incapacidades['total'];
            }
            $ajuste .= '<td style="text-align:center;width:3.8%;">'.$totalInc.'</td>';
            //$totalMC += $totalInc;

            
            if($incapacidadesTotal > 0){
                $total = $totalNC * 100 / $incapacidadesTotal;
            }

            $totalTH .= '<td style="text-align:center;width:8%;">$'.number_format($totalLC, 1).'</td><td style="text-align:center;" width="3.8%">'.$incapacidadesTotal.'</td><td style="text-align:center;" width="3.8%">'.number_format($total, 1).'</td>';




            /*Incapacidades pocentaje de pagos*/
            if($totalLC != 0){
                $total = $totalInAju * 100 / $totalLC;
            }else{
                $total = 0;
            }
            
            $ajuste .= '<td style="text-align:center;width:3.8%;">'.number_format($total, 1).'</td>';


            /*Incapacidades pocentaje de pagos*/
            if($totalLC != 0){
                $total = $totalInAdmi * 100 / $totalLC;
            }else{
                $total = 0;
            }
            
            $admini .= '<td style="text-align:center;width:3.8%;">'.number_format($total, 1).'</td>';
            $adminP .= '<td style="text-align:center;width:3.8%;">100.0</td>';

            /*Incapacidades pocentaje de pagos*/
            if($totalLC != 0){
                $total = $totalInEmp * 100 / $totalLC;
            }else{
                $total = 0;
            }
            
            $empres .= '<td style="text-align:center;width:3.8%;">'.number_format($total, 1).'</td>';
            


            /*POR PAGAR*/
            /*Incapaciodades SOLICITADAS */
            $campos = 'SUM(inc_valor_solicitado) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda']." AND inc_estado_tramite = 'SOLICITUD DE PAGO'" ;
            
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $porcob .= '<td style="text-align:center;width:8%;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
            



            /*Incapacidades de ese Año*/
            $campos = 'COUNT(*) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_estado_tramite = \'SOLICITUD DE PAGO\'';
            //echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $totalInc = 0;
            if($incapacidades['total'] > 0){
                $totalInc = $incapacidades['total'];
            }
            $porcob .= '<td style="text-align:center;width:3.8%;">'.$totalInc.'</td>';
            



            /*Incapacidades pocentaje de pagos*/
            if($incapacidadesTotal != 0){
                $total = $totalInc * 100 / $incapacidadesTotal;
            }else{
                $total = 0;
            }
            
            $porcob .= '<td style="text-align:center;width:3.8%;">'.number_format($total, 1).'</td>';



            /*PAGADAS*/
            /*Incapaciodades SOLICITADAS */
            $campos = 'SUM(inc_valor) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda']." AND inc_estado_tramite = 'PAGADA'" ;
            
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $pagada .= '<td style="text-align:center;width:8%;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
            



            /*Incapacidades de ese Año*/
            $campos = 'COUNT(*) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_estado_tramite = \'PAGADA\'';
            //echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $totalInc = 0;
            if($incapacidades['total'] > 0){
                $totalInc = $incapacidades['total'];
            }
            $pagada .= '<td style="text-align:center;width:3.8%;">'.$totalInc.'</td>';
            

            /*Incapacidades pocentaje de pagos*/
            if($incapacidadesTotal != 0){
                $total = $totalInc * 100 / $incapacidadesTotal;
            }else{
                $total = 0;
            }
            
            $pagada .= '<td style="text-align:center;width:3.8%;">'.number_format($total, 1).'</td>';




            /*NEGADAS*/
            /*Incapaciodades SOLICITADAS */
            $campos = 'SUM(inc_valor_pagado_eps) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda']." AND inc_estado_tramite = 'SIN RECONOCIMIENTO'" ;
            
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $negada .= '<td style="text-align:center;width:8%;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
            



            /*Incapacidades de ese Año*/
            $campos = 'COUNT(*) as total';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_estado_tramite = \'SIN RECONOCIMIENTO\'';
            //echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
            $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
            $totalInc = 0;
            if($incapacidades['total'] > 0){
                $totalInc = $incapacidades['total'];
            }
            $negada .= '<td style="text-align:center;width:3.8%;">'.$totalInc.'</td>';
            



            /*Incapacidades pocentaje de pagos*/
            if($incapacidadesTotal != 0){
                $total = $totalInc * 100 / $incapacidadesTotal;
            }else{
                $total = 0;
            }
            
            $negada .= '<td style="text-align:center;width:3.8%;">'.number_format($total, 1).'</td>';



        }
        $content .= '
            <tbody>
                <tr>
                    <td width="8%" style="text-align:center;">EMPRESA</td>
                    '.$empres.'
                </tr>
                <tr>
                    <td width="8%" style="text-align:center;">EPS/ARL</td>
                    '.$admini.'
                </tr>
                <tr>
                    <td width="8%" style="text-align:center;">AJUSTE</td>
                    '.$ajuste.'
                </tr>
                <tr>
                    <td width="8%" style="text-align:center;">TOTAL</td>
                    '.$totalTH.'
                </tr>
                <tr>
                    <td colspan="19" style="background: #f5f5f5;">&nbsp;</td>
                </tr>
                <tr>
                    <td width="8%" style="text-align:center;">POR COBRAR</td>
                    '.$adminP.'
                </tr>
                <tr>
                    <td width="8%" style="text-align:center;">PAGADAS</td>
                    '.$pagada.'
                </tr>
                <tr>
                    <td width="8%" style="text-align:center;">NEGADAS</td>
                    '.$negada.'
                </tr>
            </tbody>
        </table>';

    $obj_pdf->writeHTML($content, true, false, false, false, '');

    $nombre='Reporte_pagos_x_mes_'.date("d-m-Y H-i-s").'.pdf';
    //header('Content-type: application/pdf');
    $obj_pdf->Output($nombre, 'D');
            