<?php
	require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';
	require_once __DIR__.'/../../../extenciones/SVGGraph/SVGGraph.php';

	function sanear_string($string) { 
        $string = str_replace( array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string );
        $string = str_replace( array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string ); 
        $string = str_replace( array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string ); 
        $string = str_replace( array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string ); 
        $string = str_replace( array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string ); 
        $string = str_replace( array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C',), $string ); 
        //Esta parte se encarga de eliminar cualquier caracter extraño 
        //$string = str_replace( array("\\", "¨", "º", "-", "~", "#", "@", "|", "!", "\"", "·", "$", "%", "&", "/", "(", ")", "?", "'", "¡", "¿", "[", "^", "`", "]", "+", "}", "{", "¨", "´", ">“, “< ", ";", ",", ":", "."), '', $string ); 
        return $string; 
    }

	class MYPDF extends TCPDF {

		protected $processId = 0;
	    protected $header = '';
	    protected $footer = '';
	    static $errorMsg = '';
		//Page header
		public function Header() {
			$this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
	       $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

	       $this->Line(5, 5, $this->getPageWidth()-5, 5); 

	       $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
	       $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
	       $this->Line(5, 5, 5, $this->getPageHeight()-5);

		}

		// Page footer
		public function Footer() {
			// Position at 15 mm from bottom
			$this->SetY(-15);
			// Set font
			$this->SetFont('helvetica', 'N', 8);
			// Page number
			$this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}


	/*empezar el proceso del PDF*/
	$obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$obj_pdf->SetCreator(PDF_CREATOR);
	$obj_pdf->SetTitle("REPORTE ESTADOS DE CARTERA");
	//$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
	// set default header data

	$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
	$obj_pdf->setPrintHeader(true);
	$obj_pdf->setPrintFooter(true);
	$obj_pdf->SetAutoPageBreak(TRUE, 30);
	$obj_pdf->SetFont('times', '', 15);
	// set image scale factor

	$obj_pdf->AddPage();

	$item = 'emp_id';
	$valor = $_GET['idEmpresa'];
	$respuesta = ControladorClientes::ctrMostrarClientes($item, $valor);




	$content = '';
	$content = '
	<br/>
	<br/>
	<table>
		<tr>
			<td width="80%">
				<table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
					<tr>
						<td style="width:20%"></td>
						<td style="width:60%; text-align:center; font-size:12px;"><b>REPORTE ESTADOS DE CARTERA</b></td>
						<td style="width:20%"></td>
					</tr>
				</table>
				<table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
					<tr>
						<td style="width:30%"></td>
						<td style="width:40%;text-align:center;">INFORME RESUMIDO</td>
						<td style="width:30%"></td>
					</tr>
				</table>
			</td>
			<td width="20%">
				<img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
			</td>
		</tr>
	</table>
	
	<br/>
	<br/>
	<br/>
	<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
		<tr>
			<td style="width:50%;text-align:justify;">Empresa: <b>'.$respuesta['emp_nombre'].'</b></td>
			<td style="width:25%;text-align:justify;"></td>
			<td style="width:25%;text-align:justify;"></td>
		</tr>
		<tr>
			<td style="width:50%;text-align:justify;">Nit: '.$respuesta['emp_nit'].'</td>
			<td style="width:25%;text-align:justify;"></td>
			<td style="width:25%;text-align:justify;"></td>
		</tr>
		<tr>
			<td style="width:50%;text-align:justify;"></td>
			<td style="width:25%;text-align:justify;"></td>
			<td style="width:25%;text-align:justify;">Fecha: '.date('d/m/Y H:i:s').'</td>
		</tr>
	</table>
	<br/>
	<br/>
	<br/>
	<table  border="1" style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
		<thead>
			<tr style="background-color:#bdbdbd;border:solid 1px;">
				<th style="width:20px;text-align:center;">#</th>
				<th style="width:48%;text-align:center;">ENTIDAD</th>
				<th style="width:14%;text-align:center;"># COLABORADORES</th>
				<th style="width:18%;text-align:center;">ESTADO</th>
				<th style="width:20%;text-align:center;">VALOR CARTERA</th>
			</tr>
		</thead>
		<tbody>';

	$item = 'car_emp_id';
    $valo = $_GET['idEmpresa'];
    $cartera = ControladorCartera::ctrMostrarCarteraDeglosada($item, $valo);
    $totalCartera = 0;

     foreach ($cartera as $key => $value) {

    	$valor = 0;
        if(!empty($value['car_valor'])){
            $valor = number_format(trim($value['car_valor']), 0, '.', '.');
            $totalCartera += trim($value['car_valor']);
        }

        $estado = 'EN MORA';
        if($value['car_paz_salvo'] != 'NO'){
        	$estado = 'PAZ Y SALVO';
        }


        $campo = "COUNT(*) as total";
	    $tabla = "gi_empleados";
	    $condi = "emd_emp_id = ".$_GET['idEmpresa']." AND (emd_eps_id = ".$value['ips_id']." OR emd_afp_id = ".$value['ips_id']." OR emd_arl_id = ".$value['ips_id'].")";
	    $TotalEmprelados = ModeloTesoreria::mdlMostrarUnitario($campo, $tabla, $condi);

	    $content .= '<tr>';
	    $content .=	'<td style="width:20px;text-align:center;">'.($key+1).'</td>';
	    $content .=	'<td style="width:48%;text-align:center;">'.mb_strtoupper(sanear_string($value['ips_nombre'])).'</td>';
	    $content .=	'<td style="width:14%;text-align:center;">'.$TotalEmprelados['total'].'</td>';
	    $content .=	'<td style="width:18%;text-align:center;">'.$estado.'</td>';
	    $content .=	'<td style="width:20%;text-align:center;">'.$valor.'</td>';
	    $content .=	'</tr>';

	}


	    $content .= '<tr>
	    		<td colspan="3"></td>
	    		<td style="text-align:right;background-color:#bdbdbd;" >TOTAL CARTERA</td>
	    		<td style="width:20%;text-align:center;">'.number_format(trim($totalCartera), 0, '.', '.').'</td>
	    	</tr>';

	$content .= '
		</tbody>
	</table>';

	$obj_pdf->writeHTML($content, true, false, false, false, '');

	$nombre='Reporte_Estados_cartera_'.date("d-m-Y H-i-s").'.pdf';
	//header('Content-type: application/pdf');
	$obj_pdf->Output($nombre, 'I');
			