<?php
    
    //session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    ini_set('memory_limit','1024M');

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    
    
    $objPHPExcel = new Spreadsheet();


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:Z1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

    $objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->getFont()->setBold( true );


    $objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->getFont()->setBold( true );
    

    $objPHPExcel->getActiveSheet()->setTitle('COLABORADORES INCAPACITADOS');


    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Tipo de identificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Identificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Nombre"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Fecha de ingreso"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Salario"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Salario Promedio"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Estado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Fecha de retiro"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Administradora EPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("L1", "Fecha afilicación a EPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("M1", "Administradora ARL"); 
    $objPHPExcel->getActiveSheet()->setCellValue("N1", "Administradora AFP"); 
    $objPHPExcel->getActiveSheet()->setCellValue("O1", "Fecha de calificacion PCD"); 
    $objPHPExcel->getActiveSheet()->setCellValue("P1", "Entidad calificadora"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Q1", "Diagnostico"); 
    $objPHPExcel->getActiveSheet()->setCellValue("R1", "Cargo");
    $objPHPExcel->getActiveSheet()->setCellValue("S1", "Sede"); 
    $objPHPExcel->getActiveSheet()->setCellValue("T1", "Genero"); 
    $objPHPExcel->getActiveSheet()->setCellValue("U1", "Tipo de colaborador"); 
    $objPHPExcel->getActiveSheet()->setCellValue("V1", "Centro de Costo"); 
    $objPHPExcel->getActiveSheet()->setCellValue("W1", "Sub Centro de Costo"); 
    $objPHPExcel->getActiveSheet()->setCellValue("X1", "Codigo Nomina"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Y1", "Correo Contacto"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Z1", "Telefono Contacto"); 


    $where = null;
    $tabla = "gi_empleados JOIN gi_empresa ON emp_id = emd_emp_id JOIN gi_ips ON emd_eps_id = ips_id";
    $whereSub = null;
    if($_SESSION['cliente_id'] != 0){
        $where = 'emd_emp_id='.$_SESSION['cliente_id']." AND ";
        $whereSub = "WHERE inc_empresa = ".$_SESSION['cliente_id'];
    }

    $respuesta = ModeloEmpleados::mdlMostrarGroupAndOrder('*', $tabla, $where." emd_id IN (SELECT inc_emd_id FROM gi_incapacidad ".$whereSub.")" , null, 'ORDER BY emd_nombre ASC', null );
    
    $i = 2;
    foreach ($respuesta as $key => $value) {
        $estado = "ACTIVO";
        if($value["emd_fecha_retiro"] != NULL && $value['emd_fecha_retiro'] != ''){
            $estado = "RETIRADO";
        }

        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["emd_tipo_identificacion"]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_cedula"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["emd_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["emd_fecha_ingreso"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["emd_salario"]); 
        if($value["emd_salario_promedio"] != null && $value["emd_salario_promedio"] != ''){
            $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value["emd_salario_promedio"]); 
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("H".$i, "0");
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $estado); 
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["emd_fecha_retiro"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $value["ips_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $value["emd_fecha_afiliacion_eps"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("M".$i, $value["emd_arl_id"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $value["emd_afp_id"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $value["emd_fecha_calificacion_PCL"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("P".$i, $value["emd_entidad_calificadora"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$i, $value["emd_diagnostico"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("R".$i, $value["emd_cargo"]);
        $objPHPExcel->getActiveSheet()->setCellValue("S".$i, $value["emd_sede"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("T".$i, $value["emd_genero"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("U".$i, $value["emd_tipo_empleado"]);
        $objPHPExcel->getActiveSheet()->setCellValue("V".$i, $value["emd_centro_costos"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("W".$i, $value["emd_subcentro_costos"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("X".$i, $value["emd_codigo_nomina"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("Y".$i, $value["emd_correo_v"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("Z".$i, $value["emd_telefono_v"]); 
        $i++;         
    }


    foreach(range('A','Z') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    
    }

    $objPHPExcel->getActiveSheet()->getStyle('G1:G'.$i)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

    $objPHPExcel->getActiveSheet()->getStyle('H1:H'.$i)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);


   // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Informe_consolidado.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    ob_start();
    //$writer->save("php://output");
    $writer = new Xlsx($objPHPExcel);
    $writer->save("php://output");
    $xlsData = ob_get_contents();
    ob_end_clean(); 

    $response =  array(
        'op' => 'ok',
        'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
    );

    echo json_encode($response);

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }