<?php
    
    $objPHPExcel = new PHPExcel();

    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Valores Administradoras")
                             ->setSubject("Valores Administradoras")
                             ->setDescription("Descarga Valores Administradoras, registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Valores Administradoras");


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:H1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold( true );
    

    $objPHPExcel->getActiveSheet()->setTitle('Valores x Administradora');


    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "ADMINISTRADORA"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "EMPRESA"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "POR COBRAR"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "PAGADAS");
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "SALDOS");   
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "%"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "AFILIADOS"); 



    $i = 2;

    $intTotalEmpresPagado = 0;
    $intTotalIpsPagado___ = 0;
    $intTotalEmpleados___ = 0;
    $intTotalPorcentaje__ = 0;
    $intTotalRecuperado__ = 0;
    $saldosTotales = 0;
    $year = $_GET['year'];
    $otherYear = $_GET['otherYear'];

    $strCampo = "ips_nombre, ips_id";
    $strTabla = " gi_ips JOIN gi_empleados ON emd_eps_id = ips_id";
    $strWhere = '1 = 1';
    if($_SESSION['cliente_id'] != 0){
        $strWhere .= " AND emd_emp_id = ".$_SESSION['cliente_id'];
    }
    $arrResul = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampo,$strTabla,$strWhere, 'GROUP BY ips_id', ' ORDER BY ips_nombre ASC');
    
    foreach ($arrResul as $key => $eps) {

        /*Vamos a obtener las ganancias de esa empresa en ese intervalo de tiempo */
        /*ahora vamos a obtener las Incapacidades pagadas por la empresa*/
        $strCampo = "sum(inc_valor_pagado_empresa) as total";
        $strTabla = "gi_incapacidad JOIN gi_empleados ON emd_cedula = inc_cc_afiliado";
        $strWhere = " inc_valor_pagado_empresa IS NOT NULL AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND emd_eps_id = ".$eps['ips_id'];
        if($_SESSION['cliente_id'] != 0){
            $strWhere .= " AND inc_empresa = ".$_SESSION['cliente_id'];
        }
        $arrRespuestaPE = ModeloTesoreria::mdlMostrarUnitario($strCampo, $strTabla, $strWhere);
        $intTotalEmpresPagado += $arrRespuestaPE['total'];


        /*ahora vamos a obtener las Incapacidades por que se deben pagar por las EPS*/
        $strCampo = "sum(inc_valor_pagado_eps) as total";
        $strTabla = "gi_incapacidad JOIN gi_empleados ON emd_cedula = inc_cc_afiliado";
        $strWhere = " inc_estado_tramite !=  'EMPRESA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND emd_eps_id = ".$eps['ips_id'];
        if($_SESSION['cliente_id'] != 0){
            $strWhere .= " AND inc_empresa = ".$_SESSION['cliente_id'];
        }
        $arrRespuestaIP = ModeloTesoreria::mdlMostrarUnitario($strCampo, $strTabla, $strWhere);
        $intTotalIpsPagado___ += $arrRespuestaIP['total'];
        
        /*ahora vamos a obtener las Incapacidades Que se pagaron*/
        $strCampo = "sum(inc_valor) as total";
        $strTabla = "gi_incapacidad JOIN gi_empleados ON emd_cedula = inc_cc_afiliado";
        $strWhere = " inc_estado_tramite !=  'EMPRESA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND emd_eps_id = ".$eps['ips_id'];
        if($_SESSION['cliente_id'] != 0){
            $strWhere .= " AND inc_empresa = ".$_SESSION['cliente_id'];
        }
        $arrRespuestaPS = ModeloTesoreria::mdlMostrarUnitario($strCampo, $strTabla, $strWhere);
        $intTotalRecuperado__ += $arrRespuestaPS['total'];

        $douPorcentage = 0;
        if($arrRespuestaIP['total'] != 0)    
            $douPorcentage = ($arrRespuestaPS['total'] * 100) / $arrRespuestaIP['total'];
        
        //$intTotalPorcentaje__ += $douPorcentage;
        /*Afiliados*/
        $strCampo = "COUNT(emd_cedula) as total";
        $strTabla = "gi_empleados";
        $strWhere = " emd_eps_id = ".$eps['ips_id']."  AND emd_estado = 1 ";
        if($_SESSION['cliente_id'] != 0){
            $strWhere .= " AND emd_emp_id = ".$_SESSION['cliente_id'];
        }
        $arrRespuestaEM = ModeloTesoreria::mdlMostrarUnitario($strCampo, $strTabla, $strWhere);
        $intTotalEmpleados___ += $arrRespuestaEM['total'];

        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $eps['ips_nombre']); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $arrRespuestaPE['total']);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $arrRespuestaIP['total']); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $arrRespuestaPS['total']);
        $totalX = $arrRespuestaIP['total'] - $arrRespuestaPS['total'];
        $saldosTotales += $totalX;  
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $totalX);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $douPorcentage); 
        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $arrRespuestaEM['total']); 
        $i++;         
    }

    $objPHPExcel->getActiveSheet()->setCellValue("C".($i+1), $intTotalEmpresPagado);
    $objPHPExcel->getActiveSheet()->setCellValue("D".($i+1), $intTotalIpsPagado___); 
    $objPHPExcel->getActiveSheet()->setCellValue("E".($i+1), $intTotalRecuperado__); 
    $objPHPExcel->getActiveSheet()->setCellValue("F".($i+1), $saldosTotales);  
    $objPHPExcel->getActiveSheet()->setCellValue("H".($i+1), $intTotalEmpleados___); 

    foreach(range('A','H') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);    
    }

    $objPHPExcel->getActiveSheet()->getStyle('C2:C'.($i+1))
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

    $objPHPExcel->getActiveSheet()->getStyle('D2:D'.($i+1))
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

    $objPHPExcel->getActiveSheet()->getStyle('E2:E'.($i+1))
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

    $objPHPExcel->getActiveSheet()->getStyle('F2:F'.($i+1))
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Administradoras_Periodo'.$year.'_hasta_'.$otherYear."_".date('H:i:s').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');
    

