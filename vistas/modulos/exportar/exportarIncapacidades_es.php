<?php
    
    $objPHPExcel = new PHPExcel();

    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Incapacidades")
                             ->setSubject("Incapacidades Actuales")
                             ->setDescription("Descarga del historico de incapacidades registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Incapacidades");


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:Y1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:Y1')->getFont()->setBold( true );


    $objPHPExcel->getActiveSheet()->setTitle('INCAPACIDADES');

    if($_SESSION['cliente_id'] == 0){
        $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
        $objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa"); 
        $objPHPExcel->getActiveSheet()->setCellValue("C1", "Administradora"); 
        $objPHPExcel->getActiveSheet()->setCellValue("D1", "Identificación"); 
        $objPHPExcel->getActiveSheet()->setCellValue("E1", "Nombre"); 
        $objPHPExcel->getActiveSheet()->setCellValue("F1", "Diagnostico"); 
        $objPHPExcel->getActiveSheet()->setCellValue("G1", "Origen"); 
        $objPHPExcel->getActiveSheet()->setCellValue("H1", "Clasificación"); 
        $objPHPExcel->getActiveSheet()->setCellValue("I1", "Fecha Inicio"); 
        $objPHPExcel->getActiveSheet()->setCellValue("J1", "Fecha Final"); 
        $objPHPExcel->getActiveSheet()->setCellValue("K1", "Días"); 
        $objPHPExcel->getActiveSheet()->setCellValue("L1", "Tipo Generación"); 
        $objPHPExcel->getActiveSheet()->setCellValue("M1", "Profesional Responsable"); 
        $objPHPExcel->getActiveSheet()->setCellValue("N1", "Registro del Profesional"); 
        $objPHPExcel->getActiveSheet()->setCellValue("O1", "Donde se genero"); 
        $objPHPExcel->getActiveSheet()->setCellValue("P1", "Fecha Generación"); 
        $objPHPExcel->getActiveSheet()->setCellValue("Q1", "Observación"); 
        $objPHPExcel->getActiveSheet()->setCellValue("R1", "Estado Tramite");
        $objPHPExcel->getActiveSheet()->setCellValue("S1", "Fecha Radicación"); 
        $objPHPExcel->getActiveSheet()->setCellValue("T1", "Fecha Solicitud"); 
        $objPHPExcel->getActiveSheet()->setCellValue("U1", "Valor Solicitado"); 
        $objPHPExcel->getActiveSheet()->setCellValue("V1", "Fecha Pago"); 
        $objPHPExcel->getActiveSheet()->setCellValue("W1", "Valor Pagado"); 
        $objPHPExcel->getActiveSheet()->setCellValue("X1", "Fecha Pago Nomina");  
        $objPHPExcel->getActiveSheet()->setCellValue("Y1", "Valor Empresa"); 
        $objPHPExcel->getActiveSheet()->setCellValue("Z1", "Valor Administradora"); 
        $objPHPExcel->getActiveSheet()->setCellValue("AA1", "Valor Ajuste");
    }else{
        $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
        $objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa"); 
        $objPHPExcel->getActiveSheet()->setCellValue("C1", "Administradora"); 
        $objPHPExcel->getActiveSheet()->setCellValue("D1", "Identificación"); 
        $objPHPExcel->getActiveSheet()->setCellValue("E1", "Nombre"); 
        $objPHPExcel->getActiveSheet()->setCellValue("F1", "Diagnostico"); 
        $objPHPExcel->getActiveSheet()->setCellValue("G1", "Origen"); 
        $objPHPExcel->getActiveSheet()->setCellValue("H1", "Clasificación"); 
        $objPHPExcel->getActiveSheet()->setCellValue("I1", "Fecha Inicio"); 
        $objPHPExcel->getActiveSheet()->setCellValue("J1", "Fecha Final"); 
        $objPHPExcel->getActiveSheet()->setCellValue("K1", "Días"); 
        $objPHPExcel->getActiveSheet()->setCellValue("L1", "Tipo Generación"); 
        $objPHPExcel->getActiveSheet()->setCellValue("M1", "Profesional Responsable"); 
        $objPHPExcel->getActiveSheet()->setCellValue("N1", "Registro del Profesional"); 
        $objPHPExcel->getActiveSheet()->setCellValue("O1", "Donde se genero"); 
        $objPHPExcel->getActiveSheet()->setCellValue("P1", "Observación"); 
        $objPHPExcel->getActiveSheet()->setCellValue("Q1", "Estado Tramite"); 
        $objPHPExcel->getActiveSheet()->setCellValue("R1", "Valor Solicitado"); 
        $objPHPExcel->getActiveSheet()->setCellValue("S1", "Fecha Pago"); 
        $objPHPExcel->getActiveSheet()->setCellValue("T1", "Valor Pagado"); 
        $objPHPExcel->getActiveSheet()->setCellValue("U1", "Fecha Pago Nomina");  
        $objPHPExcel->getActiveSheet()->setCellValue("V1", "Valor Empresa"); 
        $objPHPExcel->getActiveSheet()->setCellValue("W1", "Valor Administradora"); 
        $objPHPExcel->getActiveSheet()->setCellValue("X1", "Valor Ajuste");
    }
      

    $item = null;
    $valor = null;
    if($_SESSION['cliente_id'] != 0){
        $item = 'inc_empresa';
        $valor = $_SESSION['cliente_id'];
    }
    $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);
    $i = 2;
    foreach ($incapacidades as $key => $value) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nombre"]); 

        if($value['inc_clasificacion'] != 4){
            if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["inc_arl_afiliado"]);  
            }else{
                $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["inc_ips_afiliado"]);  
            }
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["inc_afp_afiliado"]);    
        }

        $strClasificacion = "PRORROGA";
        if($value["inc_clasificacion"] == 1){
            $strClasificacion = "INICIAL";
        }
        
        if($_SESSION['cliente_id'] == 0){
            $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_cedula"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["emd_nombre"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["inc_diagnostico"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["inc_origen"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $strClasificacion); 
            $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value["inc_fecha_inicio"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["inc_fecha_final"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("K".$i, dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"])); 
            $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $value["inc_tipo_generacion"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("M".$i, $value["inc_profesional_responsable"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $value["inc_prof_registro_medico"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $value["inc_donde_se_genera"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("P".$i, $value["inc_fecha_generada"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("Q".$i, $value["inc_observacion"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("R".$i, $value["inc_estado_tramite"]);
            $objPHPExcel->getActiveSheet()->setCellValue("S".$i, $value["inc_fecha_radicacion"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("T".$i, $value["inc_fecha_solicitud"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("U".$i, $value["inc_valor_solicitado"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("V".$i, $value["inc_fecha_pago"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("W".$i, $value["inc_valor"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("X".$i, $value["inc_fecha_pago_nomina"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("Y".$i, $value["inc_valor_pagado_empresa"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("Z".$i, $value["inc_valor_pagado_eps"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("AA".$i, $value["inc_valor_pagado_ajuste"]); 
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_cedula"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["emd_nombre"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["inc_diagnostico"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["inc_origen"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $strClasificacion); 
            $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value["inc_fecha_inicio"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["inc_fecha_final"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("K".$i, dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"])); 
            $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $value["inc_tipo_generacion"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("M".$i, $value["inc_profesional_responsable"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $value["inc_prof_registro_medico"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $value["inc_donde_se_genera"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("P".$i, $value["inc_observacion"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("Q".$i, $value["inc_estado_tramite"]);
            $objPHPExcel->getActiveSheet()->setCellValue("R".$i, $value["inc_valor_solicitado"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("S".$i, $value["inc_fecha_pago"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("T".$i, $value["inc_valor"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("U".$i, $value["inc_fecha_pago_nomina"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("V".$i, $value["inc_valor_pagado_empresa"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("W".$i, $value["inc_valor_pagado_eps"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("X".$i, $value["inc_valor_pagado_ajuste"]); 
        }
        $i++;         
    }

    foreach(range('A','AA') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }

    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="incapacidades.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');
    

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }