<?php
    
    $objPHPExcel = new PHPExcel();

    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Sessiones")
                             ->setSubject("Historico Sessiones")
                             ->setDescription("Descarga del historico de sessiones, registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Sessiones");


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:X1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold( true );
    

    $objPHPExcel->getActiveSheet()->setTitle('Sessiones');


    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Usuario"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Fecha Inicio"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Fecha Final");


    $Select = "user_nombre, session_fecha_inicio_d, session_fecha_final_d, session_idSession_v ";
    $From = " gi_sessiones JOIN gi_usuario ON gi_usuario.user_id = session_user_id_i";
    $Where = "";
    if(isset($_POST['fechaInicio']) && isset($_POST['fechaFinal'])){
        $Where = " session_fecha_inicio_d BETWEEN '".$_POST['fechaInicio']."' AND '".$_POST['fechaFinal']."' ";
    }else{
        $Where = " session_fecha_inicio_d BETWEEN '".date('Y-m-d')." 00:00:00' AND  '".date('Y-m-d')." 23:59:59'";
    }


    $result = ModeloTesoreria::mdlMostrarGroupAndOrder($Select, $From, $Where, null, "ORDER BY user_nombre ASC");
    
    $i = 2;
    foreach ($result as $key => $value) {
    
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["user_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["session_fecha_inicio_d"]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["session_fecha_final_d"]);
        $i++;         
    
    }


    foreach(range('A','D') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    
    }
    

    $newHoja = $objPHPExcel->createSheet(1);
    $newHoja->setTitle("ACTIVIDADES");
    $newHoja
    ->getStyle('A1:O1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $newHoja->getStyle('A1:O1')->getFont()->setBold( true );
    

    $newHoja->setCellValue("A1", "#"); 
    $newHoja->setCellValue("B1", "Rango Horas"); 
    $newHoja->setCellValue("C1", "Empresa"); 
    $newHoja->setCellValue("D1", "Actividad"); 
    $newHoja->setCellValue("E1", "Observacion"); 
    $newHoja->setCellValue("F1", "Usuario"); 
    $newHoja->setCellValue("G1", "Fecha Actividad"); 
    

    $Select2 = "act_hora_rango_v, emp_nombre, act_descripcion_v, act_observacion_v, user_nombre, act_fecha_d ";
    $From2 = " gi_actividades JOIN gi_empresa ON emp_id = act_emp_id_i JOIN gi_actividades_d ON act_actividad_v = gi_actividades_d.act_id_i JOIN gi_usuario ON gi_usuario.user_id = act_user_id_i";
    $Where2 = "";
    if(isset($_POST['fechaInicio']) && isset($_POST['fechaFinal'])){
        $Where2 = " act_fecha_d BETWEEN '".$_POST['fechaInicio']."' AND '".$_POST['fechaFinal']."' ";
    }else{
        $Where2 = " act_fecha_d BETWEEN '".date('Y-m-d')." 00:00:00' AND  '".date('Y-m-d')." 23:59:59'";
    }


    $result2 = ModeloTesoreria::mdlMostrarGroupAndOrder($Select2, $From2, $Where2, null, "ORDER BY act_hora_rango_v ASC");
    

    $i = 2;
    foreach ($result2 as $key => $value) {
    
        $newHoja->setCellValue("A".$i, ($key+1)); 
        $newHoja->setCellValue("B".$i, $value["act_hora_rango_v"]); 
        $newHoja->setCellValue("C".$i, $value["emp_nombre"]);
        $newHoja->setCellValue("D".$i, $value["act_descripcion_v"]); 
        $newHoja->setCellValue("E".$i, $value["act_observacion_v"]); 
        $newHoja->setCellValue("F".$i, $value["user_nombre"]); 
        $newHoja->setCellValue("G".$i, $value["act_fecha_d"]); 
    
        $i++;         
    }

    $Select3 = "count(*) as Horas, emp_nombre , act_fecha_d ";
    $From3 = " gi_actividades JOIN gi_empresa ON emp_id = act_emp_id_i ";
    $Where3 = "";
    $dias = 1;
    if(isset($_POST['fechaInicio']) && isset($_POST['fechaFinal'])){
        $Where3 = " act_fecha_d BETWEEN '".$_POST['fechaInicio']."' AND '".$_POST['fechaFinal']."' ";
        $dias = ControladorIncapacidades::dias_transcurridos($_POST['fechaInicio'], $_POST['fechaFinal']);
    }else{
        $Where3 = " act_fecha_d BETWEEN '".date('Y-m-d')." 00:00:00' AND  '".date('Y-m-d')." 23:59:59'";
    }

    $result3 = ModeloTesoreria::mdlMostrarGroupAndOrder($Select3, $From3, $Where3, "GROUP BY act_emp_id_i, act_fecha_d ", "ORDER BY act_fecha_d ASC");

    /*Aqui vamos a poner un cuadro para ver cuantas horas fueron dedicadas al dia*/

    $i = $i + 5;

    $newHoja->setCellValue("A".$i, "Cliente"); 
    $newHoja->setCellValue("B".$i, "Jornada Horas"); 
    $newHoja->setCellValue("C".$i, "Dedicacion Horas");
    $newHoja->setCellValue("D".$i, "Cumplimiento %");
    $newHoja->setCellValue("E".$i, "Fecha");

    $newHoja
    ->getStyle('A'.$i.':E'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $newHoja->getStyle('A'.$i.':E'.$i)->getFont()->setBold( true );
     
    $i = $i + 1;
    $dedicacion = 0;
    $porcentage = 0;
    foreach ($result3 as $key => $value) {
        $dedicacion = $dedicacion+8;
        $porcentage += $value["Horas"];
        $newHoja->setCellValue("A".$i, $value["emp_nombre"]); 
        $newHoja->setCellValue("B".$i, 8); 
        $newHoja->setCellValue("C".$i, $value["Horas"]);
        $newHoja->setCellValue("D".$i, ($value['Horas']*100/8)); 
        $newHoja->setCellValue("E".$i, $value["act_fecha_d"]); 
        
        $i++;         
    }    

    $i = $i + 1;
    if($dias == 1){
        $dedicacion = 8;
    }else{
        $dedicacion = $dias * 8;
    }
    $newHoja->setCellValue("A".$i, "Totales");     
    $newHoja->setCellValue("B".$i, $dedicacion); 
    $newHoja->setCellValue("C".$i, $porcentage);
    $newHoja->setCellValue("D".$i, ($porcentage * 100 / $dedicacion)); 

    $newHoja
    ->getStyle('A'.$i.':E'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $newHoja->getStyle('A'.$i.':E'.$i)->getFont()->setBold( true );

    foreach(range('A','G') as $columnID) {
        $newHoja->getColumnDimension($columnID)
            ->setAutoSize(true);
    }

    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="actividades'.date('Ymdhis').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');
    

