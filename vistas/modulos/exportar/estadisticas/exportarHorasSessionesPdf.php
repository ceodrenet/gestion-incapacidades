<?php
	require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';

	if($_SESSION['perfil'] != 7){
		include 'exportarHorasSessionesPdfA.php';
	}else{

		$content = '
		<br/>
		<br/>
		<table width="100%">
			<tr>
				<td width="80%">
					<table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
						<tr>
							<td style="width:20%"></td>
							<td style="width:60%; text-align:center; font-size:12px;"><b>REPORTE SESSIONES TRABAJADAS</b></td>
							<td style="width:20%"></td>
						</tr>
					</table>
					<table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
						<tr>
							<td style="width:30%"></td>
							<td style="width:40%;text-align:center;">HORAS LABORADAS</td>
							<td style="width:30%"></td>
						</tr>
					</table>
				</td>
				<td width="20%">
					<img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
				</td>
			</tr>
		</table>
		<br/>
		<br/>
		<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
			<tr>
				<td style="width:50%;text-align:justify;">Administardor : <b>'.$_SESSION['nombres'].'</b></td>
				<td style="width:25%;text-align:justify;"></td>
				<td style="width:25%;text-align:justify;">Fecha: '.date('d/m/Y H:i:s').'</td>
			</tr>
		</table>
		<br/>
		<br/>';

		$Select = "user_nombre, act_user_id_i, user_ruta_imagen_v, user_perfil_id";
		$From = " gi_actividades JOIN gi_usuario ON gi_usuario.user_id = act_user_id_i";
		$Where = "";
		if(isset($_GET['fechaInicio']) && isset($_GET['fechaFinal'])){
			$Where .= " act_fecha_d BETWEEN '".$_GET['fechaInicio']."' AND '".$_GET['fechaFinal']."' ";
		}else{
			$Where .= " act_fecha_d BETWEEN '".date('Y-m-d')." 00:00:00' AND  '".date('Y-m-d')." 23:59:59'";
		}

		$resulta2 = ModeloTesoreria::mdlMostrarGroupAndOrder($Select, $From, $Where, 'GROUP BY act_user_id_i', "ORDER BY user_nombre ASC");
		foreach ($resulta2 as $jey => $usuarios) {
			$respuestaDias = 0;
			$horasEs = 0;
			if(isset($_GET['fechaInicio']) && isset($_GET['fechaFinal'])){
				// = ControladorIncapacidades::dias_transcurridos($_GET['fechaInicio'], $_GET['fechaFinal']);
				$starDate = new DateTime($_GET['fechaInicio']);
				$endDate = new DateTime($_GET['fechaFinal']);
				
				while( $starDate <= $endDate){
				    if($starDate->format('l')== 'Saturday'){
				       	$horasEs += 4;
				       	$respuestaDias++;
				    }else if($starDate->format('l')== 'Sunday'){
				    	$horasEs +=  0;
				    }else{
				    	if($usuarios['user_perfil_id'] == 12){
				    		$horasEs += 4;
				    	}else{
				    		$horasEs += 8;	
				    	}
				    	$respuestaDias++;
				    }	
				    $starDate->modify("+1 days");
				}
			}
			
			$diasLaborados = 0;
			$Select = "COUNT(*) as total";
		    $From = " gi_sessiones JOIN gi_usuario ON gi_usuario.user_id = session_user_id_i";
		    $Where = "session_user_id_i = ".$usuarios['act_user_id_i']." AND ";
		    if(isset($_GET['fechaInicio']) && isset($_GET['fechaFinal'])){
		        $Where .= " session_fecha_inicio_d BETWEEN '".$_GET['fechaInicio']."' AND '".$_GET['fechaFinal']."' ";
		    }else{
		        $Where .= " session_fecha_inicio_d BETWEEN '".date('Y-m-d')." 00:00:00' AND  '".date('Y-m-d')." 23:59:59'";
		    }
		    $result = ModeloTesoreria::mdlMostrarUnitario($Select, $From, $Where);
			$diasLaborados = $result['total'];

			$horasReportadas= 0;

		    $Select2 = " count(*) as total, act_fecha_d ";
		    $From2 = " gi_actividades";
		    $Where2 = "";
		    $Where2 .= " act_user_id_i = ".$usuarios['act_user_id_i']." AND ";
		    
		    
		    if(isset($_GET['fechaInicio']) && isset($_GET['fechaFinal'])){
		        $Where2 .= " act_fecha_d BETWEEN '".$_GET['fechaInicio']."' AND '".$_GET['fechaFinal']."' ";
		    }else{
		        $Where2 .= " act_fecha_d BETWEEN '".date('Y-m-d')." 00:00:00' AND  '".date('Y-m-d')." 23:59:59'";
		    }
		    $result2 = ModeloTesoreria::mdlMostrarGroupAndOrder($Select2,$From2,$Where2, 'GROUP BY act_fecha_d', 'ORDER BY act_fecha_d ASC');
		    $cuerpoDeglosado = '';
		    foreach ($result2 as $key => $value) {
		    	$horasReportadas += $value['total'];

		    	$cuerpoDeglosado .= '<tr style="background-color:#bdbdbd;border:solid 1px;text-align:justify;">
					<td colspan="3">Fecha : '.$value['act_fecha_d'].'</td>
				</tr>';

				$Select3 = "count(*) as Horas, emp_nombre";
			    $From3 = " gi_actividades JOIN gi_empresa ON emp_id = act_emp_id_i ";
			    $Where3 = " act_fecha_d = '".$value['act_fecha_d']."'";
			    $Where3 .= " AND act_user_id_i = ".$usuarios['act_user_id_i']." ";
			    

			    $result3 = ModeloTesoreria::mdlMostrarGroupAndOrder($Select3, $From3, $Where3, 'GROUP BY emp_nombre', "ORDER BY emp_nombre ASC");
			    foreach ($result3 as $key2 => $value3) {
			    	$cuerpoDeglosado .= '<tr><td style="width:60%;">'.$value3['emp_nombre'].'</td>
				  			<td style="width:20%;">'.$value3['Horas'].'</td>
				  			<td style="width:20%;">'.number_format(($value3['Horas'] * 100)/8, 1).'%</td></tr>';
			    }	
		    }

		    /*Promedio*/
		    $horasPromedio = 0;
		    if($horasEs > 0){
		    	$horasPromedio = number_format(($horasReportadas * 100 / $horasEs), 2);	
		    }
		    
			$color="color:green;";
			if($horasPromedio < 70){
				$color = "color:red;";
			}    

			$cuerpoActividades= '';
			$Select2 = "act_hora_rango_v, emp_nombre, act_descripcion_v, act_observacion_v, user_nombre, act_fecha_d ";
		    $From2 = " gi_actividades JOIN gi_empresa ON emp_id = act_emp_id_i JOIN gi_actividades_d ON act_actividad_v = gi_actividades_d.act_id_i JOIN gi_usuario ON gi_usuario.user_id = act_user_id_i";
		    $Where2 = "";

		    $Where2 .= " act_user_id_i = ".$usuarios['act_user_id_i']." AND ";
		    
		    
		    if(isset($_GET['fechaInicio']) && isset($_GET['fechaFinal'])){
		        $Where2 .= " act_fecha_d BETWEEN '".$_GET['fechaInicio']."' AND '".$_GET['fechaFinal']."' ";
		    }else{
		        $Where2 .= " act_fecha_d BETWEEN '".date('Y-m-d')." 00:00:00' AND  '".date('Y-m-d')." 23:59:59'";
		    }


		    $result2 = ModeloTesoreria::mdlMostrarGroupAndOrder($Select2, $From2, $Where2, null, "ORDER BY act_fecha_d asc, act_hora_rango_v ASC");
		    

		    $i = 2;
		    foreach ($result2 as $key => $value) {
		    	
		    	$cuerpoActividades .= '<tr>
		    		<td style="width:2%;">'.($key+1).'</td>
		  			<td style="width:10%;">'.$value["act_hora_rango_v"].'</td>
		  			<td style="width:20%;">'.$value["emp_nombre"].'</td>
		  			<td style="width:20%;">'.$value["act_descripcion_v"].'</td>
		  			<td style="width:30%;">'.$value["act_observacion_v"].'</td>
		  			<td style="width:10%;">'.$value["user_nombre"].'</td>
		  			<td style="width:8%;">'.$value["act_fecha_d"].'</td>
		    	</tr>';
		    }

			$content .= '<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
				<tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
					<td colspan="2" style="text-align:center;"><b>'.$usuarios['user_nombre'].'</b></td>
				</tr>
				<tr>
					<td style="width:30%;text-align:center;">
						<img src="'.$usuarios['user_ruta_imagen_v'].'" width="100px" height="100px">
					</td>
					<td style="width:70%;">
						<table style="font-family:tahoma;font-size:10;" width="100%" border="1" cellpadding="1px" cellspacing="0px" width="100%">
							<tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;"> 
								<td style="width:20%;text-align:center;"><b>Dias Consultados</b></td>
								<td style="width:20%;text-align:center;"><b>Horas Esperadas</b></td>
								<td style="width:20%;text-align:center;"><b>Dias Con información</b></td>	
								<td style="width:20%;text-align:center;"><b>Horas Laboradas</b></td>
								<td style="width:20%;text-align:center;"><b>% Cumplimiento</b></td>
							</tr>
							<tr>
								<td style="width:20%;text-align:center;">'.$respuestaDias.'</td>
								<td style="width:20%;text-align:center;">'.$horasEs.'</td>
								<td style="width:20%;text-align:center;"><b>'.$diasLaborados.'</b></td>	
								<td style="width:20%;text-align:center;"><b>'.$horasReportadas.'</b></td>
								<td style="width:20%;text-align:center;'.$color.'"><b>'.$horasPromedio.' %</b></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br/>
			<br/>
			<table style="font-family:tahoma;font-size:10;" width="100%" border="1" cellpadding="1px" cellspacing="0px" width="100%">
				<tr> 
					<td colspan="3" style="text-align:center;"><b>Horas Laboradas por Fecha</b></td>
				</tr>
				<tr>
		  			<td style="width:60%;"><b>Empresa</b></td>
		  			<td style="width:20%;"><b>Horas Dedicadas</b></td>
		  			<td style="width:20%;"><b>Porcentaje  Horas</b></td>
		  		</tr>
				'.$cuerpoDeglosado.'
			</table>
			<br/>
			<br/>
			<br/>
			<br/>
			<table style="font-family:tahoma;font-size:10;" width="100%" border="1" cellpadding="1px" cellspacing="0px" width="100%">
				<tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;"> 
					<td colspan="7"><b>Actividades Realizadas</b></td>
				</tr>
				<tr>
		  			<td style="width:2%;"><b>#</b></td>
		  			<td style="width:10%;"><b>Rango Horas</b></td>
		  			<td style="width:20%;"><b>Empresa</b></td>
		  			<td style="width:20%;"><b>Actividad</b></td>
		  			<td style="width:30%;"><b>Observación</b></td>
		  			<td style="width:10%;"><b>Usuario</b></td>
		  			<td style="width:8%;"><b>Fecha Actividad</b></td>
		  		</tr>
		  		'.$cuerpoActividades.'
			</table>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>';

		}

		class MYPDF extends TCPDF {

			protected $processId = 0;
		    protected $header = '';
		    protected $footer = '';
		    static $errorMsg = '';
			//Page header
			public function Header() {
				$this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
		       $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

		       $this->Line(5, 5, $this->getPageWidth()-5, 5); 

		       $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
		       $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
		       $this->Line(5, 5, 5, $this->getPageHeight()-5);
			}

			// Page footer
			public function Footer() {
				// Position at 15 mm from bottom
				$this->SetY(-15);
				// Set font
				$this->SetFont('helvetica', 'N', 8);
				// Page number
				$this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			}
		}


		$obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$obj_pdf->SetTitle("REPORTE ACTIVIDADES");
		$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
		$obj_pdf->setPrintHeader(false);
		$obj_pdf->setPrintFooter(true);
		$obj_pdf->SetAutoPageBreak(TRUE, 30);
		$obj_pdf->SetFont('times', '', 15);
		// set image scale factor
		$obj_pdf->AddPage('L', 'A4');


		$obj_pdf->writeHTML($content, true, false, false, false, '');

	    $nombre='reporte_Actividades_'.date("d-m-Y H-i-s").'.pdf';
	    //header('Content-type: application/pdf');
	    $obj_pdf->Output($nombre, 'I');
	}