<?php
    
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    

    $objPHPExcel = new Spreadsheet();

    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold( true );

    $objPHPExcel->getActiveSheet()
        ->getStyle('A1:L1')
        ->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold( true );
    
    $objPHPExcel->getActiveSheet()->setTitle('PAGOS COLABORADORES');



    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Identificación");    
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Apellidos y Nombres"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Cargo"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Fecha Inicio"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Fecha Final"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Días"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Fecha de Radicacion"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Fecha de Pago"); 
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Valor Pagado");
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Administradora"); 
    $objPHPExcel->getActiveSheet()->setCellValue("L1", "Centro de Costos"); 


    $campos = "emp_nombre, emd_nombre, emd_cedula, ip2.ips_nombre as eps, ip3.ips_nombre as arl,  ip4.ips_nombre as afp, inc_valor ,  inc_fecha_pago  , inc_fecha_inicio, 
   inc_fecha_final,
    (DATEDIFF(inc_fecha_final,inc_fecha_inicio) +1) as dias, inc_clasificacion, emd_cargo ,  inc_fecha_radicacion, emd_centro_costos , inc_origen";
    $tabla_ = " gi_incapacidad ";
    $tabla_ .= " JOIN gi_empresa ON emp_id = inc_empresa 
    JOIN gi_empleados ON emd_id = inc_emd_id 
    JOIN gi_ips ip2 ON ip2.ips_id = emd_eps_id 
    LEFT JOIN gi_ips ip3 ON ip3.ips_id = emd_arl_id 
    LEFT JOIN gi_ips ip4 ON ip4.ips_id = emd_afp_id ";
    $where_ = "inc_fecha_pago BETWEEN '".$_GET['fi']."' AND '".$_GET['ff']."' and inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite= 5 ";
    $pagadas = ModeloDAO::mdlMostrarGroupAndOrder($campos,$tabla_,$where_, null, 'ORDER BY inc_fecha_pago ASC', null);
    $i = 2;
    foreach ($pagadas as $key => $value) {

        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($i-1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emd_cedula"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["emd_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_cargo"]); 
        $date = new DateTime($value["inc_fecha_inicio"]);
        $date2 = new DateTime($value["inc_fecha_final"]);
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, PHPExcel_Shared_Date::PHPToExcel($date));
        $objPHPExcel->getActiveSheet()->getStyle("E".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");  
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, PHPExcel_Shared_Date::PHPToExcel($date2)); 
        $objPHPExcel->getActiveSheet()->getStyle("F".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value['dias']);

        if($value["inc_fecha_radicacion"] != null && $value["inc_fecha_radicacion"] != ''){
            $date6 = new DateTime($value["inc_fecha_radicacion"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("H".$i, PHPExcel_Shared_Date::PHPToExcel($date6)); 
            $objPHPExcel->getActiveSheet()->getStyle("H".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value["inc_fecha_radicacion"] ); 
        }
       
        if($value["inc_fecha_pago"] != null && $value["inc_fecha_pago"] != ''){
            $date7 = new DateTime($value["inc_fecha_pago"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("I".$i, PHPExcel_Shared_Date::PHPToExcel($date7)); 
            $objPHPExcel->getActiveSheet()->getStyle("I".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value["inc_fecha_pago"] ); 
        }
        if($value['inc_valor'] != null && $value['inc_valor']  != ''){
            $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["inc_valor"]); 
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("J".$i, '0'); 
        }
        if($value["inc_origen"] == '4'){
            $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $value["arl"]);  
        }else{
           $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $value["eps"]);     
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $value["emd_centro_costos"]);
        $i++;         
    }


    foreach(range('A','L') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }




    $newHoja = $objPHPExcel->createSheet(1);
    $newHoja->getStyle('A1:D1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $newHoja->getStyle('A1:D1')->getFont()->setBold( true );
    $newHoja->getStyle('A1:D1')
        ->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $newHoja->getStyle('A1:D1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $newHoja->getStyle('A1:D1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

    $newHoja->getStyle('A1:D1')->getFont()->setBold( true );

    $newHoja->setTitle('PAGOS POR ENTIDADES');
    
    $newHoja->setCellValue("A1", "NIT");
    $newHoja->setCellValue("B1", "ENTIDAD");    
    $newHoja->setCellValue("C1", "VALOR PAGADO"); 
    $newHoja->setCellValue("D1", "FECHA PAGO"); 
     

    $where = '';
    if(isset($_GET['clid']) && $_GET['clid'] != 0){
        $where .= 'inc_empresa = '.$_GET['clid']." AND ";
    }

    $camposFiltro = "inc_fecha_pago";
    $selectFiltro = "reporte_tesoreria";
    $condicionFil = $where."inc_fecha_pago BETWEEN '".$_GET['fi']."' AND '".$_GET['ff']."' AND estado_tramite_id = 5";
    $respuestaFil = ModeloTesoreria::mdlMostrarGroupAndOrder($camposFiltro, $selectFiltro, $condicionFil, 'GROUP BY inc_fecha_pago', 'ORDER BY inc_fecha_pago ASC');
    $total = 0;
    $j = 2;
    foreach ($respuestaFil as $key => $value) {

        $campo = 'SUM(inc_valor) as total, afp_nombre, afp_nit, inc_fecha_pago ';
        $tabla = 'reporte_tesoreria';
        $condicion = $where."inc_fecha_pago = '".$value['inc_fecha_pago']."' AND inc_clasificacion_id = 4 ";

       // echo "SELECT ".$campo." FROM ".$tabla." WHERE ".$condicion;   

        $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY afp_nombre, afp_nit, inc_fecha_pago', 'ORDER BY afp_nombre');

        /*Obtenemos las ARL porque son por accidente de transito*/
        $campo = 'SUM(inc_valor) as total, arl_nombre, arl_nit, inc_fecha_pago ';
        $tabla = 'reporte_tesoreria';
        $condicion = $where."inc_fecha_pago = '".$value['inc_fecha_pago']."' AND inc_clasificacion_id != 4 AND inc_origen_id = '4' ";
        $respuestaArl = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY arl_nombre, arl_nit, inc_fecha_pago', 'ORDER BY arl_nombre');

        /*Obtenemos las EPS*/
        $campo = 'SUM(inc_valor) as total, ips_nombre, ips_nit, inc_fecha_pago ';
        $tabla = 'reporte_tesoreria';
        $condicion = $where."inc_fecha_pago = '".$value['inc_fecha_pago']."' AND inc_clasificacion_id != 4 AND inc_origen_id != '4' ";
        $respuestaEps = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY ips_nombre, ips_nit, inc_fecha_pago', 'ORDER BY ips_nombre');
        
        foreach ($respuesta as $key => $valueAfp) {
            if($valueAfp['afp_nombre'] != null){
                $total += $valueAfp['total'];
                $dateAfp = new DateTime($valueAfp["inc_fecha_pago"]);
                $newHoja->setCellValue("D".$j, PHPExcel_Shared_Date::PHPToExcel($dateAfp));
                $newHoja->getStyle("D".$j)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
                $newHoja->setCellValue("B".$j, $valueAfp['afp_nombre']);    
                $newHoja->setCellValue("C".$j, $valueAfp['total']);
                $newHoja->setCellValue("A".$j, $valueAfp['afp_nit']);
                $j++;
            }
        }

        foreach ($respuestaArl as $key => $valueArl) {
            if($valueArl['arl_nombre'] != null){
                $total += $valueArl['total'];
                $dateArl = new DateTime($valueArl["inc_fecha_pago"]);
                $newHoja->setCellValue("D".$j, PHPExcel_Shared_Date::PHPToExcel($dateArl));
                $newHoja->getStyle("D".$j)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
                $newHoja->setCellValue("B".$j, $valueArl['arl_nombre']);    
                $newHoja->setCellValue("C".$j, $valueArl['total']); 
                $newHoja->setCellValue("A".$j, $valueArl['arl_nit']);
                $j++;
            }
        }

        foreach ($respuestaEps as $key => $valueIps) {
            if($valueIps['ips_nombre'] != null){
                $total += $valueIps['total'];
                $dateIps = new DateTime($valueIps["inc_fecha_pago"]);
                $newHoja->setCellValue("D".$j, PHPExcel_Shared_Date::PHPToExcel($dateIps));
                $newHoja->getStyle("D".$j)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
                $newHoja->setCellValue("B".$j, $valueIps['ips_nombre']);    
                $newHoja->setCellValue("C".$j, $valueIps['total']); 
                $newHoja->setCellValue("A".$j, $valueIps['ips_nit']);
                $j++;
            }
        }
    }

    foreach(range('A','D') as $columnID) {
        $newHoja->getColumnDimension($columnID)
            ->setAutoSize(true);
    }



    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="ReportePagosEmpleados.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer = new Xlsx($objPHPExcel);
    $writer->save("php://output");  
