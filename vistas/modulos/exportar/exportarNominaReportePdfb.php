<?php
	require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';

	$month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $nomina = $_GET['nomina'];

    if($nomina < 10){
        $nomina = '0'.$nomina;
    }

	class MYPDF extends TCPDF {

		protected $processId = 0;
	    protected $header = '';
	    protected $footer = '';
	    static $errorMsg = '';
		//Page header
		public function Header() {
			$this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
	       $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

	       $this->Line(5, 5, $this->getPageWidth()-5, 5); 

	       $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
	       $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
	       $this->Line(5, 5, 5, $this->getPageHeight()-5);
		}

		// Page footer
		public function Footer() {
			// Position at 15 mm from bottom
			$this->SetY(-15);
			// Set font
			$this->SetFont('helvetica', 'N', 8);
			// Page number
			$this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}


	$obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$obj_pdf->SetCreator(PDF_CREATOR);
	$obj_pdf->SetTitle("REPORTE NOMINA ".mb_strtoupper($month[$_GET['nomina']-1]));
	$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
	$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
	$obj_pdf->setPrintHeader(false);
	$obj_pdf->setPrintFooter(true);
	$obj_pdf->SetAutoPageBreak(TRUE, 30);
	$obj_pdf->SetFont('times', '', 15);
	// set image scale factor
	$obj_pdf->AddPage();


	function getIncapacidadesNumber($tipoOrigen, $fechaIni, $fechaFin, $empresa){
		$campos = "count(*) as estado";
        $tabla = "gi_incapacidad";
        $condiciones = "inc_origen = '".$tipoOrigen."' AND inc_fecha_pago_nomina BETWEEN '".$fechaIni."' AND '".$fechaFin."' ";
        if($_SESSION['cliente_id'] != 0){
            $condiciones .= "AND inc_empresa = ".$empresa;
        }
        $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
        return $respuesta['estado'];
	}

	function getIncapacidadesTipoNumber($fechaIni, $fechaFin, $empresa){
		$campos = "count(*) as estado";
        $tabla = "gi_incapacidad";
        $condiciones = "inc_clasificacion = '1' AND inc_fecha_pago_nomina BETWEEN '".$fechaIni."' AND '".$fechaFin."' ";
        if($_SESSION['cliente_id'] != 0){
            $condiciones .= "AND inc_empresa = ".$empresa;
        }
        $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
        return $respuesta['estado'];
	}

	function getIncapacidadesTipoNumber_2($fechaIni, $fechaFin, $empresa){
		$campos = "count(*) as estado";
        $tabla = "gi_incapacidad";
        $condiciones = "inc_clasificacion != '1' AND inc_fecha_pago_nomina BETWEEN '".$fechaIni."' AND '".$fechaFin."' ";
        if($_SESSION['cliente_id'] != 0){
            $condiciones .= "AND inc_empresa = ".$empresa;
        }
        $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
        return $respuesta['estado'];
	}

	

    $year = $_GET['anho'].'-'.$nomina.'-01';
    $otherYear =  date("Y-m-d",strtotime($year."+ 1 month"));// $_GET['anho'].'-'.$nomina.'-31';
    $otherYear =  date("Y-m-d",strtotime($otherYear."- 1 days"));

	$item = 'emp_id';
	$valor = $_GET['empresa'];
	$respuesta = ControladorClientes::ctrMostrarClientes($item, $valor);

	$eG = getIncapacidadesNumber('ENFERMEDAD GENERAL', $year, $otherYear, $_GET['empresa']);
	$lM = getIncapacidadesNumber('LICENCIA DE MATERNIDAD', $year, $otherYear, $_GET['empresa']);
	$lP = getIncapacidadesNumber('LICENCIA DE PATERNIDAD', $year, $otherYear, $_GET['empresa']);
	$aL = getIncapacidadesNumber('ACCIDENTE LABORAL', $year, $otherYear, $_GET['empresa']);
	$aT = getIncapacidadesNumber('ACCIDENTE TRANSITO', $year, $otherYear, $_GET['empresa']);

	$d1=0;
	$d3=0;
	$d6=0;
	$d16=0;
	$d31=0;

	$campos = "inc_fecha_inicio, inc_fecha_final";
    $tabla = "gi_incapacidad";
    $condiciones = "inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' ";
    if($_SESSION['cliente_id'] != 0){
        $condiciones .= "AND inc_empresa = ".$_GET['empresa'];
    }
    $incapacidades =  ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$condiciones, $groupBy = null, $orderBy = null);
    foreach ($incapacidades as $key => $value) {
    	$dias = ControladorIncapacidades::dias_transcurridos($value['inc_fecha_inicio'], $value['inc_fecha_final']);

    	if($dias < 3){
    		$d1++;
    	}else if($dias>2 && $dias < 6){
    		$d3++;
    	}else if($dias>5 && $dias < 16){
    		$d6++;
    	}else if($dias>15 && $dias < 31){
    		$d16++;
    	}else{
    		$d31++;
    	}
    }

	$campos = "COUNT(inc_diagnostico) as total, inc_diagnostico, dia_descripcion";
    $tabla = "gi_incapacidad JOIN gi_diagnostico ON inc_diagnostico = dia_codigo ";
    $condiciones = "inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' ";
    if($_SESSION['cliente_id'] != 0){
        $condiciones .= "AND inc_empresa = ".$_GET['empresa'];
    }
    $diagnisticos =  ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$condiciones, 'GROUP BY inc_diagnostico', 'ORDER BY total DESC');
    $cuerpoDiagnostico = '';
    $megaTotal=0;
    foreach ($diagnisticos as $key => $value) {
    	$cuerpoDiagnostico .= '<tr> 
			<td style="width:10%;text-align:center;">'.$value['inc_diagnostico'].'</td>
			<td style="width:80%;text-align:center;">'.$value['dia_descripcion'].'</td>
			<td style="width:10%;text-align:center;">'.$value['total'].'</td>
		</tr>';
		$megaTotal += $value['total'];
    }

    /*Centro de Costos*/
    $campos = "COUNT(*) as total, emd_centro_costos";
    $tabla = "gi_incapacidad JOIN gi_empleados ON gi_incapacidad.inc_emd_id = gi_empleados.emd_id ";
    $condiciones = "inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' ";
    if($_SESSION['cliente_id'] != 0){
        $condiciones .= "AND inc_empresa = ".$_GET['empresa'];
    }
    $centroCosInc =  ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$condiciones, 'GROUP BY emd_centro_costos', 'ORDER BY total DESC');
    $centro_costos = '';
    $megaTotalCostos=0;
    foreach ($centroCosInc as $key => $value) {
    	$centro_costos .= '<tr> 
			<td style="width:10%;text-align:center;">'.$value['total'].'</td>
			<td style="width:90%;text-align:justify;">'.$value['emd_centro_costos'].'</td>
		</tr>';
		$megaTotalCostos += $value['total'];
    }

    /*Sede*/
    $campos = "COUNT(*) as total, emd_sede";
    $tabla = "gi_incapacidad JOIN gi_empleados ON gi_incapacidad.inc_emd_id = gi_empleados.emd_id ";
    $condiciones = "inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' ";
    if($_SESSION['cliente_id'] != 0){
        $condiciones .= "AND inc_empresa = ".$_GET['empresa'];
    }
    $sedeInca =  ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$condiciones, 'GROUP BY emd_sede', 'ORDER BY total DESC');
    $sede_s = '';
    $megaTotalSedes=0;
    foreach ($sedeInca as $key => $value) {
    	$sede_s .= '<tr> 
			<td style="width:10%;text-align:center;">'.$value['total'].'</td>
			<td style="width:80%;text-align:justify;">'.$value['emd_sede'].'</td>
		</tr>';
		$megaTotalSedes += $value['total'];
    }

    /*Ciudad*/
    $campos = "COUNT(*) as total, emd_ciudad";
    $tabla = "gi_incapacidad JOIN gi_empleados ON gi_incapacidad.inc_emd_id = gi_empleados.emd_id  ";
    $condiciones = "inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' ";
    if($_SESSION['cliente_id'] != 0){
        $condiciones .= "AND inc_empresa = ".$_GET['empresa'];
    }
    $ciudadInca =  ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$condiciones, 'GROUP BY emd_ciudad', 'ORDER BY total DESC');
    $ciudad_s = '';
    $megaTotalCiudad=0;
    foreach ($ciudadInca as $key => $value) {
    	$ciudad_s .= '<tr> 
			<td style="width:10%;text-align:center;">'.$value['total'].'</td>
			<td style="width:80%;text-align:justify;">'.$value['emd_ciudad'].'</td>
		</tr>';
		$megaTotalCiudad += $value['total'];
    }



	$content = '';
	$content = '
	<br/>
	<br/>
	<table width="100%">
		<tr>
			<td width="80%">
				<table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
					<tr>
						<td style="width:20%"></td>
						<td style="width:60%; text-align:center; font-size:12px;"><b>REPORTE ESTADO DE NOMINA</b></td>
						<td style="width:20%"></td>
					</tr>
				</table>
				<table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
					<tr>
						<td style="width:30%"></td>
						<td style="width:40%;text-align:center;">INFORME NOMINA '.mb_strtoupper($month[$_GET['nomina']-1]).'</td>
						<td style="width:30%"></td>
					</tr>
				</table>
			</td>
			<td width="20%">
				<img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
			</td>
		</tr>
	</table>
	
	<br/>
	<br/>
	<br/>
	<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
		<tr>
			<td style="width:50%;text-align:justify;">Empresa: <b>'.$respuesta['emp_nombre'].'</b></td>
			<td style="width:25%;text-align:justify;"></td>
			<td style="width:25%;text-align:justify;"></td>
		</tr>
		<tr>
			<td style="width:50%;text-align:justify;">Nit: '.$respuesta['emp_nit'].'</td>
			<td style="width:25%;text-align:justify;"></td>
			<td style="width:25%;text-align:justify;">Fecha: '.date('d/m/Y H:i:s').'</td></td>
		</tr>
	</table>
	<br/>
	<br/>
	<br/>
	<table style="font-family:tahoma;font-size:10;" width="100%" border="1" cellpadding="1px" cellspacing="0px" width="100%">
		<tr>
			<td style="width:15%;text-align:center;background:#00a65a;color:white;">ENFERMEDAD GENERAL</td>
			<td style="width:15%;text-align:center;background-color:#dd4b39 !important; color: #FFF;">ACCIDENTE LABORAL</td>
			<td style="width:15%;text-align:center;background-color:#e65100 !important; color: #FFF;">ACCIDENTE TRANSITO</td>
			<td style="width:15%;text-align:center;background-color:#ff6384 !important; color: #FFF;">LICENCIA MATERNIDAD</td>
			<td style="width:15%;text-align:center;background-color:#00c0ef !important; color: #FFF;">LICENCIA PATERNIDAD</td>
			<td style="width:15%;text-align:center;">TOTAL</td>
		</tr>
		<tr>
			<td style="width:15%;text-align:center;">'.$eG.'</td>
			<td style="width:15%;text-align:center;">'.$aL.'</td>
			<td style="width:15%;text-align:center;">'.$aT.'</td>
			<td style="width:15%;text-align:center;">'.$lM.'</td>
			<td style="width:15%;text-align:center;">'.$lP.'</td>
			<td style="width:15%;text-align:center;">'.($eG+$aL+$aT+$lM+$lP).'</td>
		</tr>
	</table>
	<br/>
	<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
		<tr>
			<td style="width:50%;text-align:center;"></td>
			<td style="width:50%;">
				<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
					<tr>
						<td style="width:25%;text-align:right;"><b>INICIAL: </b></td>
						<td style="width:25%;">'.getIncapacidadesTipoNumber($year, $otherYear, $_GET['empresa']).'</td>
						<td style="width:25%;text-align:right;"><b>PRORROGA: </b></td>
						<td style="width:25%;">'.getIncapacidadesTipoNumber_2($year, $otherYear, $_GET['empresa']).'</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
		<tr>
			<td style="width:30%;text-align:center;">
				<img src="vistas/modulos/exportar/reloj.png" width="100px" height="100px">
			</td>
			<td style="width:70%;">
				<table style="font-family:tahoma;font-size:10;" width="100%" border="1" cellpadding="1px" cellspacing="0px" width="100%">
					<tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;"> 
						<td style="width:20%;text-align:center;"><b>1-2 Días</b></td>
						<td style="width:20%;text-align:center;"><b>3-5 Días</b></td>
						<td style="width:20%;text-align:center;"><b>6-15 Días</b></td>
						<td style="width:20%;text-align:center;"><b>16-30 Días</b></td>
						<td style="width:20%;text-align:center;"><b>+30 Días</b></td>
					</tr>
					<tr>
						<td style="width:20%;text-align:center;">'.$d1.'</td>
						<td style="width:20%;text-align:center;">'.$d3.'</td>
						<td style="width:20%;text-align:center;">'.$d6.'</td>
						<td style="width:20%;text-align:center;">'.$d16.'</td>
						<td style="width:20%;text-align:center;">'.$d31.'</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br/>
	<table style="font-family:tahoma;font-size:10;" width="100%" border="1" cellpadding="1px" cellspacing="0px" width="100%">
		<tr> 
			<td style="width:10%;text-align:center;"><b>CODIGO</b></td>
			<td style="width:80%;text-align:center;"><b>DESCRIPCIÓN</b></td>
			<td style="width:10%;text-align:center;"><b>#</b></td>
		</tr>
		'.$cuerpoDiagnostico.'
		<tr> 
			<td style="width:10%;text-align:center;"></td>
			<td style="width:80%;text-align:center;"></td>
			<td style="width:10%;text-align:center;">'.$megaTotal.'</td>
		</tr>
	</table>

	<br/>
	<table style="font-family:tahoma;font-size:10;" width="100%" border="1" cellpadding="1px" cellspacing="0px" width="100%">
		<tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
			<td style="text-align:center;"><b>INCAPACIDADES POR CENTRO DE COSTO</b></td>
		</tr>
		<tr> 
			<td style="width:10%;text-align:center;"><b>#</b></td>
			<td style="width:90%;text-align:justify;"><b>CENTRO DE COSTOS</b></td>
		</tr>
		'.$centro_costos.'
		<tr> 
			<td style="width:10%;text-align:center;">'.$megaTotalCostos.'</td>
			<td style="width:80%;text-align:center;"></td>
		</tr>
	</table>

	<br/>
	<table style="font-family:tahoma;font-size:10;" width="100%" border="1" cellpadding="1px" cellspacing="0px" width="100%">
		<tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
			<td style="text-align:center;"><b>INCAPACIDADES POR SEDE</b></td>
		</tr>
		<tr> 
			<td style="width:10%;text-align:center;"><b>#</b></td>
			<td style="width:90%;text-align:justify;"><b>SEDE</b></td>
		</tr>
		'.$sede_s.'
		<tr> 
			<td style="width:10%;text-align:center;">'.$megaTotalSedes.'</td>
			<td style="width:80%;text-align:center;"></td>
		</tr>
	</table>

	<br/>
	<table style="font-family:tahoma;font-size:10;" width="100%" border="1" cellpadding="1px" cellspacing="0px" width="100%">
		<tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
			<td style="text-align:center;"><b>INCAPACIDADES POR CIUDAD</b></td>
		</tr>
		<tr> 
			<td style="width:10%;text-align:center;"><b>#</b></td>
			<td style="width:90%;text-align:justify;"><b>CIUDAD</b></td>
		</tr>
		'.$ciudad_s.'
		<tr> 
			<td style="width:10%;text-align:center;">'.$megaTotalCiudad.'</td>
			<td style="width:80%;text-align:center;"></td>
		</tr>
	</table>';


	$obj_pdf->writeHTML($content, true, false, false, false, '');

    $nombre='reporte_nomina_'.$month[$_GET['nomina']-1].'_'.date("d-m-Y H-i-s").'.pdf';
    //header('Content-type: application/pdf');
    $obj_pdf->Output($nombre, 'D');