<?php
	require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';
	require_once __DIR__.'/../../../extenciones/SVGGraph/SVGGraph.php';

	function sanear_string($string) { 
        $string = str_replace( array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string );
        $string = str_replace( array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string ); 
        $string = str_replace( array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string ); 
        $string = str_replace( array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string ); 
        $string = str_replace( array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string ); 
        $string = str_replace( array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C',), $string ); 
        //Esta parte se encarga de eliminar cualquier caracter extraño 
        //$string = str_replace( array("\\", "¨", "º", "-", "~", "#", "@", "|", "!", "\"", "·", "$", "%", "&", "/", "(", ")", "?", "'", "¡", "¿", "[", "^", "`", "]", "+", "}", "{", "¨", "´", ">“, “< ", ";", ",", ":", "."), '', $string ); 
        return $string; 
    }

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }


    class MYPDF extends TCPDF {

        protected $processId = 0;
        protected $header = '';
        protected $footer = '';
        static $errorMsg = '';
        //Page header
        public function Header() {
            $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
           $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

           $this->Line(5, 5, $this->getPageWidth()-5, 5); 

           $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
           $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
           $this->Line(5, 5, 5, $this->getPageHeight()-5);

        }

        // Page footer
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            // Set font
            $this->SetFont('helvetica', 'N', 8);
            // Page number
            $this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
    }


    /*empezar el proceso del PDF*/
    $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("Reporte Incapacidades por Empleado");
    //$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    // set default header data

    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(true);
    $obj_pdf->setPrintFooter(true);
    $obj_pdf->SetAutoPageBreak(TRUE, 30);
    $obj_pdf->SetFont('times', '', 15);
    // set image scale factor
    $obj_pdf->AddPage('L', 'A4'); 

    
    $item  = 'emd_cedula';
    $valor = $_GET['cc']; 
    $empleados = ControladorEmpleados::ctrMostrarNombres($item, $valor);

    $item  = 'emp_id';
    $valor = $_SESSION['cliente_id']; 
    $empresa = ControladorClientes::ctrMostrarClientes($item, $valor);


    $content = '';
    $content = '
    <br/>
    <br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:10%"></td>
                        <td style="width:70%; text-align:center; font-size:12px;"><b>REPORTE INCAPACIDADES POR EMPLEADO</b></td>
                        <td style="width:20%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:20%"></td>
                        <td style="width:50%;text-align:center;">INFORME TOTAL</td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    
    <br/>
    <br/>
    <br/>';
    $totalDIas = 0;
    $valoPagado = 0;
    foreach ($empleados as $key => $empleado) {
    
    
    $content.= '<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <td style="width:50%;text-align:justify;">Empleado: <b>'.$empleado['emd_nombre'].'</b></td>
            <td style="width:25%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;">Fecha:&nbsp;&nbsp;&nbsp; '.date('d/m/Y H:i:s').'</td>
        </tr>
        <tr>
            <td style="width:50%;text-align:justify;">Identificaci&oacute;n: '.$empleado['emd_cedula'].' </td>
            <td style="width:25%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;">Fecha de Ingreso: '.$empleado['emd_fecha_ingreso'].' </td>
        </tr>
        <tr>
            <td style="width:50%;text-align:justify;">Empresa : <b>'.$empresa['emp_nombre'].'</b></td>
            <td style="width:25%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
        </tr>
        <tr>
            <td style="width:50%;text-align:justify;">Cargo: '.$empleado['emd_cargo'].' </td>
            <td style="width:25%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
        </tr>
        <tr>
            <td style="width:50%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
        </tr>
    </table>
    <br/>
    <table  border="1" style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <thead>
            <tr style="background-color:#bdbdbd;border:solid 1px;">
                <th style="width:20px;text-align:center;">#</th>
                <th style="width:7%;text-align:center;">Fecha Inicial</th>
                <th style="width:7%;text-align:center;">Fecha Final</th>
                <th style="width:5%;text-align:center;">Dias</th>
                <th style="width:10%;text-align:center;">Diagnostico</th>
                <th style="width:15%;text-align:center;">Administradora</th>
                <th style="width:10%;text-align:center;">Estado</th>
                <th style="width:13%;text-align:center;">Valor Pagado</th>
                <th style="width:13%;text-align:center;">Sucursal</th>
                <th style="width:10%;text-align:center;">Centro de Costo</th>
                <th style="width:10%;text-align:center;">Ciudad</th>   
            </tr>
        </thead>
        <tbody>';

    $item   = 'inc_emd_id';
    $valor  =  $empleado['emd_id'];
    $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);
    $totalDIas = 0;
    $valoPagado = 0;
    foreach ($incapacidades as $key => $value) {

        $totalDIas += dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]);

        $content .= '<tr>';
        $content .= '<td style="width:20px;text-align:center;">'.($key+1).'</td>';
        $content .= '<td style="width:7%;text-align:center;">'.explode(' ', $value["inc_fecha_inicio"])[0].'</td>
            <td style="width:7%;text-align:center;">'.explode(' ', $value["inc_fecha_final"])[0].'</td>
            <td style="width:5%;text-align:center;">'.dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]).'</td>
            <td style="width:10%;text-align:center;">'.$value["inc_diagnostico"].'</td>';

        if($value['inc_clasificacion'] != 4){
            if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                $content .= '<td style="width:15%;text-align:center;">'.$value["inc_arl_afiliado"].'</td>'; 
            }else{
                $content .= '<td style="width:15%;text-align:center;">'.$value["inc_ips_afiliado"].'</td>'; 
            }
        }else{
            $content .= '<td style="width:15%;text-align:center;">'.$value["inc_afp_afiliado"].'</td>';    
        }

        $content .='<td style="width:10%;text-align:center;">'.$value["inc_estado_tramite"].'</td>';       

        if($value["inc_valor"] != null && $value["inc_valor"] != ''){
            $valoPagado += $value["inc_valor"];
            $content .= '<td style="width:13%;text-align:center;">$'.number_format(trim($value["inc_valor"]), 0, '.', '.').'</td>';
        }else{
            $content .= '<td style="width:13%;text-align:center;"> </td>';
        }

        $content .= '<td style="width:13%;text-align:center;">'.$value["emd_sede"].'</td>';
        $content .= '<td style="width:10%;text-align:center;">'.$value["emd_centro_costos"].'</td>';
        $content .= '<td style="width:10%;text-align:center;">'.$value["emd_ciudad"].'</td>'; 

        $content .= '</tr>';
    }
}

    $content .= '
            <tr>
                <th style="width:20px;text-align:center;"></th>
                <th style="width:7%;text-align:center;"></th>
                <th style="background-color:#bdbdbd;width:7%;text-align:center;">Total D&iacute;as </th>
                <th style="background-color:#bdbdbd;width:5%;text-align:center;">'.$totalDIas.'</th>
                <th style="width:15%;text-align:center;"></th>
                <th style="width:10%;text-align:center;"></th>
                <th style="background-color:#bdbdbd;width:10%;text-align:center;">Total Pagado</th>
                <th style="background-color:#bdbdbd;width:13%;text-align:center;">$ '.number_format($valoPagado, 0, '.', '.').'</th>
                <th style="width:13%;text-align:center;"></th>
                <th style="width:10%;text-align:center;"> </th>
                
                <th style="width:10%;text-align:center;"></th>
            </tr>
        ';
    $content .= '
        </tbody>
    </table>';

    $obj_pdf->writeHTML($content, true, false, false, false, '');

    $nombre='Reporte_incapacidades_por_colaborador_'.date("d-m-Y H-i-s").'.pdf';
    //header('Content-type: application/pdf');
    $obj_pdf->Output($nombre, 'I');
            

    

