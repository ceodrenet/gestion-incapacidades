<?php
    session_start();

    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    
    require_once dirname(__FILE__) . '/../../../extenciones/Excel.php';
    require_once dirname(__FILE__) . '/../../../controladores/mail.controlador.php';
    require_once dirname(__FILE__) . '/../../../controladores/plantilla.controlador.php'; 
    require_once dirname(__FILE__) . '/../../../controladores/incapacidades.controlador.php';

    require_once dirname(__FILE__) . '/../../../modelos/dao.modelo.php';
    require_once dirname(__FILE__) . '/../../../modelos/tesoreria.modelo.php';

    require_once dirname(__FILE__) . '/../../../vendor/autoload.php';

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    
    $objPHPExcel = new Spreadsheet();

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Reporte Cargue de Nomina")
                             ->setSubject("Reporte Cargue de Nomina")
                             ->setDescription("Descarga el Reporte de Cargue de Nomina registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Reporte Cargue de Nomina");


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:K1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold( true );


    $objPHPExcel->getActiveSheet()->setTitle('CARGUE DE NOMINA');

    $objPHPExcel->getActiveSheet()->setCellValue("A1", "CLIENTE"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "IDENTIFICACIÓN"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "CONTRATO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "DESCRIPCIÓN TIPO AUSENTISMO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "NUMERO DE DÍAS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "DÍAS COMPENSADOS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "FECHA INICIO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "FECHA FIN"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "DIAGNOSTICO"); 
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "TIPO INCAPACIDAD");
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "No. PRORROGA");

    $where = '';
    if($_POST['session'] != 0){
        $where = "emp_id = ".$_POST['session']." AND ";
    }

    $campos_ = "emp_nit, emd_cedula, emd_id, emd_codigo_nomina, ori_descripcion_v as inc_origen, DATE_FORMAT(inc_fecha_inicio, '%d/%m/%Y') as inc_fecha_inicio, inc_fecha_inicio as fecha1, DATE_FORMAT(inc_fecha_final, '%d/%m/%Y') as inc_fecha_final, inc_fecha_final as fecha2, inc_diagnostico, inc_clasificacion AS inc_estado" ;
    $tablas_ = "gi_incapacidad JOIN gi_empresa ON emp_id = inc_empresa JOIN gi_empleados ON inc_emd_id = emd_id LEFT JOIN gi_origen on ori_id_i= inc_origen ";
    $condic_ = $where." inc_fecha_pago_nomina  BETWEEN '".$_POST['NuevoFechaInicio']."' AND '".$_POST['NuevoFechaFinal']."' AND emd_estado = 1 AND inc_estado = 1";
    
    $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos_, $tablas_, $condic_, null, " ORDER BY inc_fecha_inicio DESC");

    $i = 2;
    foreach ($incapacidades as $key => $value) { 
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, explode( '-', $value["emp_nit"])[0]); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emd_cedula"]);
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["emd_codigo_nomina"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["inc_origen"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, ControladorIncapacidades::dias_transcurridos($value["fecha1"] , $value["fecha2"])); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, "" ); 
        $date = new DateTime($value["fecha1"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, PHPExcel_Shared_Date::PHPToExcel( $date ));
        $objPHPExcel->getActiveSheet()->getStyle("G".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $newDate = new DateTime($value["fecha2"]);
        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, PHPExcel_Shared_Date::PHPToExcel( $newDate ));
        $objPHPExcel->getActiveSheet()->getStyle("H".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value['inc_diagnostico']); 
        $estado = "Inicial";
        $numero = "";
        if($value['inc_estado'] != '1' && $value['inc_estado'] != '6'){
            $estado = "Prorroga";
            $numero = Time().rand();
        }
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $estado); 
        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $numero); 
        $i++;         
    }

    $objPHPExcel->getActiveSheet()->getStyle('L2:L'.$i)
        ->getNumberFormat()
        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);


    foreach(range('A','L') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }


    


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Informe_cargue_nomina.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $writer = new Xlsx($objPHPExcel);
    $writer->save("php://output");