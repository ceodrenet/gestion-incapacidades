<?php
    
    //session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    ini_set('memory_limit','1024M');

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    
    
    $objPHPExcel = new Spreadsheet();


    $objPHPExcel->getActiveSheet()
        ->getStyle('A1:S1')
        ->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

    $objPHPExcel->getActiveSheet()->getStyle('A1:S1')->getFont()->setBold(true);


    $objPHPExcel->getActiveSheet()->setTitle('FACTURACIÓN');

    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Identificación");    
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Apellidos y Nombres"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Fecha Inicio"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Fecha Final"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Días"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Origen"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Fecha Generación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Fecha Radicación");
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Valor Administradora"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Administradora"); 
    $objPHPExcel->getActiveSheet()->setCellValue("L1", "Estado Tramite");
    $objPHPExcel->getActiveSheet()->setCellValue("M1", "Valor Pagado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("N1", "Fecha Pago"); 
    $objPHPExcel->getActiveSheet()->setCellValue("O1", "Fecha Conciliación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("P1", "Comisión"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Q1", "Fecha Facturación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("R1", "Numero Factura"); 
    $objPHPExcel->getActiveSheet()->setCellValue("S1", "Fecha Pago Factura");
    $item = null;
    $valor = null;
    if($_SESSION['cliente_id'] != 0){
        $item = 'inc_empresa';
        $valor = $_SESSION['cliente_id'];
    }

    //$incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);
    if(isset($_GET['fechaInicio']) && isset($_GET['fechaFinal']) && $_GET['fechaInicio'] != null && $_GET['fechaFinal'] != null){
        $incapacidades = ControladorTesoreria::ctrVerReportesTesoreria($item, $valor, $_GET['fechaInicio'], $_GET['fechaFinal'], 'mostrarGeneralPagos_byCliente');
    }else{
        $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);     
    }


   
    $i = 2;
    foreach ($incapacidades as $key => $value) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["inc_cc_afiliado"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["emd_nombre"]); 
        $date = new DateTime($value["inc_fecha_inicio"]);
        $date2 = new DateTime($value["inc_fecha_final"]);
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, PHPExcel_Shared_Date::PHPToExcel($date));
        $objPHPExcel->getActiveSheet()->getStyle("D".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");  
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, PHPExcel_Shared_Date::PHPToExcel($date2)); 
        $objPHPExcel->getActiveSheet()->getStyle("E".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]));
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["inc_origen"]); 
        $date4 = new DateTime($value["inc_fecha_generada"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, PHPExcel_Shared_Date::PHPToExcel($date4));
        $objPHPExcel->getActiveSheet()->getStyle("H".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");

        $date5 = new DateTime($value["inc_fecha_radicacion"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, PHPExcel_Shared_Date::PHPToExcel($date5));
        $objPHPExcel->getActiveSheet()->getStyle("I".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["inc_valor_pagado_eps"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $value["ips_nombre"]);
        $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $value["inc_estado_tramite"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("M".$i, $value["inc_valor"]); 
        if($value["inc_fecha_pago"] != null && $value["inc_fecha_pago"] != ''){
            $date6 = new DateTime($value["inc_fecha_pago"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("N".$i, PHPExcel_Shared_Date::PHPToExcel($date6)); 
            $objPHPExcel->getActiveSheet()->getStyle("N".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $value["inc_fecha_pago"] ); 
        }
        if($value["inc_fecha_consiliacion_d"] != null && $value["inc_fecha_consiliacion_d"] != ''){
            $date6 = new DateTime($value["inc_fecha_consiliacion_d"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("O".$i, PHPExcel_Shared_Date::PHPToExcel($date6)); 
            $objPHPExcel->getActiveSheet()->getStyle("O".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }
        $objPHPExcel->getActiveSheet()->setCellValue("P".$i, $value['inc_condicion_id_i']);
        if($value["inc_fecha_fecturacion_d"] != null && $value["inc_fecha_fecturacion_d"] != ''){
            $date6 = new DateTime($value["inc_fecha_fecturacion_d"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("Q".$i, PHPExcel_Shared_Date::PHPToExcel($date6)); 
            $objPHPExcel->getActiveSheet()->getStyle("Q".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }
        $objPHPExcel->getActiveSheet()->setCellValue("R".$i, $value["inc_numero_factura"]);

        if($value["inc_fecha_pago_factura"] != null && $value["inc_fecha_pago_factura"] != '' && $value["inc_fecha_pago_factura"] != '0000-00-00'){
            $date6 = new DateTime($value["inc_fecha_pago_factura"]); 
            $objPHPExcel->getActiveSheet()->setCellValue("S".$i, PHPExcel_Shared_Date::PHPToExcel($date6)); 
            $objPHPExcel->getActiveSheet()->getStyle("S".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        }

        $i++;         
    }

    foreach(range('A','S') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }

    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Informe_consolidado.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    ob_start();
    //$writer->save("php://output");
    $writer = new Xlsx($objPHPExcel);
    $writer->save("php://output");
    $xlsData = ob_get_contents();
    ob_end_clean(); 

    $response =  array(
        'op' => 'ok',
        'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
    );

    echo json_encode($response);

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }