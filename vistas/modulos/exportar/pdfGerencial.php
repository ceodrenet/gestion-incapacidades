<?php
	require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';
	require_once __DIR__.'/../../../extenciones/SVGGraph/SVGGraph.php';

	class MYPDF extends TCPDF {

		protected $processId = 0;
	    protected $header = '';
	    protected $footer = '';
	    static $errorMsg = '';
		//Page header
		public function Header() {
			$this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
	       $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

	       $this->Line(5, 5, $this->getPageWidth()-5, 5); 

	       $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
	       $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
	       $this->Line(5, 5, 5, $this->getPageHeight()-5);
		}

		// Page footer
		public function Footer() {
			// Position at 15 mm from bottom
			$this->SetY(-15);
			// Set font
			$this->SetFont('helvetica', 'N', 8);
			// Page number
			$this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

	//$anho = explode('-',$_POST['NuevoFechaInicio'])[0];
	$anho = '2019';
	$fechaInical = null;
	$fechaFinal_ = null;
	if(isset($_GET['NuevoFechaInicial'])){
		$anho = explode('-',$_GET['NuevoFechaInicial'])[0];
		$fechaInical  = $_GET['NuevoFechaInicial'];
		$fechaFinal_  = $_GET['NuevoFechaFinal'];
	}

	function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }

    if($_SESSION['cliente_id'] != 0){
	    $cliente = new ControladorClientes();
       	$respuesta = $cliente->ctrMostrarClientes('emp_id', $_SESSION['cliente_id']);
    }


	/*empezar el proceso del PDF*/
	$obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$obj_pdf->SetCreator(PDF_CREATOR);
	$obj_pdf->SetTitle("RELACIÓN DE INCAPACIDADES PAGADAS ENTRE ".$fechaInical." - ".$fechaFinal_);
	$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
	$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
	$obj_pdf->setPrintHeader(false);
	$obj_pdf->setPrintFooter(true);
	$obj_pdf->SetAutoPageBreak(TRUE, 30);
	$obj_pdf->SetFont('times', '', 15);
	// set image scale factor

	$obj_pdf->AddPage();

	$content = '';
	
	if($_SESSION['cliente_id'] != 0){

		$content .= '
		<br/><br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/>
		<img src="vistas/modulos/exportar/LOGO_2_GRANDE.jpg" alt="Logo de GEIN" />
		<br/><br/><br/><br/><br/><br/><br/>
		<table>
            <tr>
                <td width="80%">
                    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                        <tr>
                            <td style="width:90%; text-align:Justify; font-size:12px;"><b>RELACIÓN DE INCAPACIDADES PAGADAS</b></td>
                            <td style="width:10%"></td>
                        </tr>
                    </table>
                    
                    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                        <tr>
                            <td style="width:90%;text-align:Justify;"><b>'.$respuesta['emp_nombre'].'</b></td>
                            <td style="width:30%"></td>
                        </tr>
                    </table>
                    
                    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                        <tr>
                            <td style="width:90%;text-align:Justify;"><b>Periodo consultado entre '.$fechaInical.' y '.$fechaFinal_.'</b></td>
                            <td style="width:30%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';
	}else{
		$content .= '
		<br/><br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/>
		<img src="vistas/modulos/exportar/LOGO_2_GRANDE.jpg" alt="Logo de GEIN" />
		<br/><br/><br/><br/><br/><br/><br/>
		<table>
            <tr>
                <td width="80%">
                    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                        <tr>
                            <td style="width:90%; text-align:Justify; font-size:12px;"><b>RELACIÓN DE INCAPACIDADES PAGADAS</b></td>
                            <td style="width:10%"></td>
                        </tr>
                    </table>
                    
                    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                        <tr>
                            <td style="width:90%;text-align:Justify;"><b>Periodo consultado entre '.$fechaInical.' y '.$fechaFinal_.'</b></td>
                            <td style="width:30%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';
	}
	$obj_pdf->writeHTML($content);

	$obj_pdf->SetFont('times', '', 13);
	
	$obj_pdf->AddPage();

	/*$bMargin = $obj_pdf->getBreakMargin();
	// get current auto-page-break mode
	$auto_page_break = $obj_pdf->getAutoPageBreak();

	$img_file =  __DIR__.'/LOGO_2_GRANDE.jpg';
	$obj_pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
	// restore auto-page-break status
	$obj_pdf->SetAutoPageBreak($auto_page_break, $bMargin);
	// set the starting point for the page content
	$obj_pdf->setPageMark();
	/*Aqui debemos obtener las Incapacidades pagadas por la Administradora y por fecha*/
	
	$where = '';
    if($_SESSION['cliente_id'] != 0){
        $where .= 'inc_empresa = '.$_SESSION['cliente_id']." AND ";
    }

    $camposFiltro = "inc_fecha_pago";
    $selectFiltro = "reporte_tesoreria";
    $condicionFil = $where."inc_fecha_pago BETWEEN '".$fechaInical."' AND '".$fechaFinal_."' AND inc_estado_tramite =  'PAGADA'";

   //cho "SELECT ".$camposFiltro." FROM ".$selectFiltro." WHERE ".$condicionFil;	

    $respuestaFil = ModeloTesoreria::mdlMostrarGroupAndOrder($camposFiltro, $selectFiltro, $condicionFil, 'GROUP BY inc_fecha_pago', 'ORDER BY inc_fecha_pago DESC');


    $content = '';
	$content .= '
	<br/>
    <br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:90%; text-align:center; font-size:12px;"><b>REPORTE DE PAGOS RECIBIDOS POR FECHA</b></td>
                        <td style="width:10%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:90%;text-align:center;">INFORME TOTAL</td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <br/>
    <br/>';

	$content .= '
	<table width="100%">
		<tr>
			<td style="width:5%"></td>
			<td style="width:90%">
				<table border="1" style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px">
					<thead>
						<tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
							<th><b>Administradora</b></th>
							<th><b>Valor Pagado</b></th>
							<th><b>Fecha de Pago</b></th>
						</tr>
					</thead>
					<tbody>';
				$total = 0;
			    foreach ($respuestaFil as $key => $value) {
			    	
			    	$campo = 'SUM(inc_valor) as total, afp_nombre, DATE_FORMAT(inc_fecha_pago, \'%d/%m/%Y\') as inc_fecha_pago ';
				    $tabla = 'reporte_tesoreria';
				    $condicion = $where."inc_fecha_pago = '".$value['inc_fecha_pago']."' AND inc_clasificacion = 4 ";

				   // echo "SELECT ".$campo." FROM ".$tabla." WHERE ".$condicion;	

				    $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY afp_nombre, inc_fecha_pago', 'ORDER BY afp_nombre');

				    /*Obtenemos las ARL porque son por accidente de transito*/
				    $campo = 'SUM(inc_valor) as total, arl_nombre, DATE_FORMAT(inc_fecha_pago, \'%d/%m/%Y\') as inc_fecha_pago ';
				    $tabla = 'reporte_tesoreria';
				    $condicion = $where."inc_fecha_pago = '".$value['inc_fecha_pago']."' AND inc_clasificacion != 4 AND inc_origen = 'ACCIDENTE LABORAL' ";
				    $respuestaArl = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY arl_nombre, inc_fecha_pago', 'ORDER BY arl_nombre');

				    /*Obtenemos las EPS*/
				    $campo = 'SUM(inc_valor) as total, ips_nombre, DATE_FORMAT(inc_fecha_pago, \'%d/%m/%Y\') as inc_fecha_pago ';
				    $tabla = 'reporte_tesoreria';
				    $condicion = $where."inc_fecha_pago = '".$value['inc_fecha_pago']."' AND inc_clasificacion != 4 AND inc_origen != 'ACCIDENTE LABORAL' ";
				    $respuestaEps = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY ips_nombre, inc_fecha_pago', 'ORDER BY ips_nombre');
			    	
			    	foreach ($respuesta as $key => $value) {
				        if($value['afp_nombre'] != null){
				            $content .= '<tr>';
				            $content .= "<td>".$value['afp_nombre']."</td>";
				            $content .= "<td style=\"text-align:center;\">".number_format($value['total'], 0, '.', '.')."</td>";
				            $content .= "<td style=\"text-align:rigth;margin-rigth:1px;\">".$value['inc_fecha_pago']."</td>";
				            $content .= '</tr>';
				            $total += $value['total'];
				        }
				    }

				    foreach ($respuestaArl as $key => $value) {
				        if($value['arl_nombre'] != null){
				            $content .= '<tr>';
				            $content .= "<td>".$value['arl_nombre']."</td>";
				            $content .= "<td style=\"text-align:center;\">".number_format($value['total'], 0, '.', '.')."</td>";
				            $content .= "<td style=\"text-align:rigth;\">".$value['inc_fecha_pago']."</td>";
				            $content .= '</tr>';
				     		$total += $value['total'];
				        }
				    }

				    foreach ($respuestaEps as $key => $value) {
				        if($value['ips_nombre'] != null){
				            $content .= '<tr>';
				            $content .= "<td>".$value['ips_nombre']."</td>";
				            $content .= "<td style=\"text-align:center;\">".number_format($value['total'], 0, '.', '.')."</td>";
				            $content .= "<td style=\"text-align:rigth;\">".$value['inc_fecha_pago']."</td>";
				            $content .= '</tr>';
				            $total += $value['total'];
				        }
				    }

			    }
			

    $content .='
					</tbody>
				</table>
				<br/>
				<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" border="1">
					<tr>
    					<td></td>
    					<td style="text-align:center; background-color:#bdbdbd;text-align:center;">'.number_format($total, 0, '.', '.').'</td>
    					<td></td>
    				</tr>
				</table>
			</td>
			<td style="width:5%"></td>
		</tr>
	</table>
	
</html>
	';
	$obj_pdf->writeHTML($content, true, false, false, false, '');

	$obj_pdf->SetFont('times', '', 13);
	$obj_pdf->AddPage();

	$content = '	
	<br/>
    <br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:90%; text-align:center; font-size:12px;"><b>REPORTE DE PAGOS RECIBIDOS POR ENTIDAD</b></td>
                        <td style="width:10%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:90%;text-align:center;">INFORME TOTAL</td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <br/>';


	$where = '';
    if($_SESSION['cliente_id'] != 0){
        $where .= 'inc_empresa = '.$_SESSION['cliente_id']." AND ";
    }

	$campoAfp____ = 'SUM(inc_valor) as total, afp_nombre';
    $tablaAfp____ = 'reporte_tesoreria';
    $condicionAfp = $where."inc_clasificacion = 4 AND inc_fecha_pago BETWEEN '".$fechaInical."' AND '".$fechaFinal_."' ";
    $respuestaAfp = ModeloTesoreria::mdlMostrarGroupAndOrder($campoAfp____,$tablaAfp____,$condicionAfp, 'GROUP BY afp_nombre', '');
   	
   	$arrayEPS = array();

    foreach ($respuestaAfp as $key => $value) {
        if($value['afp_nombre'] != null){
            $arrayEPS[$value['afp_nombre']] = $value['total'];
        }
    }

    /*Obtenemos las ARL porque son por accidente de transito*/
    $campo = 'SUM(inc_valor) as total, arl_nombre';
    $tabla = 'reporte_tesoreria';
    $condicion =  $where."inc_clasificacion != 4 AND inc_origen = 'ACCIDENTE LABORAL' AND inc_fecha_pago BETWEEN '".$fechaInical."' AND '".$fechaFinal_."' ";
    $respuesta2 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY arl_nombre', '');
    foreach ($respuesta2 as $key => $value) {
        if($value['arl_nombre'] != null){
            $arrayEPS[$value['arl_nombre']] = $value['total'];
        }
    }

    /*Obtenemos las EPS porque son por accidente de transito*/
    $campo = 'SUM(inc_valor) as total, ips_nombre';
    $tabla = 'reporte_tesoreria';
    $condicion =  $where."inc_clasificacion != 4 AND inc_origen != 'ACCIDENTE LABORAL' AND inc_fecha_pago BETWEEN '".$fechaInical."' AND '".$fechaFinal_."' ";
    $respuesta3 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY ips_nombre', 'ORDER BY SUM(inc_valor)');
    foreach ($respuesta3 as $key => $value) {
        if($value['ips_nombre'] != null){
            $arrayEPS[$value['ips_nombre']] = $value['total'];
        }
    }

    //var_dump($arrayEPS);
    $settings = array(
		'back_colour' => '#eee',  
		'stroke_colour' => '#000',
		'back_stroke_width' => 0, 
		'back_stroke_colour' => '#eee',
		'axis_colour' => '#333',  
		'axis_overlap' => 2,
		'axis_font' => 'Georgia', 
		'axis_font_size' => 6,
		'grid_colour' => '#666',  
		'label_colour' => '#000',
		'pad_right' => 5,         
		'pad_left' => 5,
		'link_base' => '/',       
		'link_target' => '_top',
		'minimum_grid_spacing' => 10,
		'show_legend' => true,
		'legend_position' => 'top',
		'show_grid' => false,
		'show_axis_v' => false,
		'show_axis_text_v' => false,
		'show_data_labels' => true,
		'data_label_type' => "bubble",
		'data_label_font' => 'Georgia',
		'data_label_font_size' => 7,
		'data_label_colour' => 'black',
		'data_label_font_adjust' => 0.6,
		'data_label_tail_length' =>3
	);

	$graph = new SVGGraph(500, 300, $settings);
	$values = array(
	 	$arrayEPS
	);
	$graph->Values($values);
	$colours = array(array('#1e88e5','#1e88e5'), array('#1e88e5','#1e88e5'));
	$graph->colours = $colours;
	$output = $graph->fetch('BarGraph');

	$obj_pdf->writeHTML($content, true, false, false, false, '');

	$obj_pdf->ImageSVG('@' . $output, $x=15, $y=35, $w='', $h='', $link='', $align='', $palign='', $border=1, $fitonpage=false);

	$content = '<br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/>
	<br/><br/><br/><br/>
	<br/><br/>
	<br/>
    <br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:80%; text-align:center; font-size:12px;"><b>REPORTE DE PAGOS RECIBIDOS POR MES</b></td>
                        <td style="width:20%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:80%;text-align:center;">INFORME TOTAL</td>
                        <td style="width:20%"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>';

	$obj_pdf->writeHTML($content, true, false, false, false, '');
	/* Pagina de las graficas */

	$where = '';
    if($_SESSION['cliente_id'] != 0){
        $where .= 'inc_empresa = '.$_SESSION['cliente_id']." AND ";
    }

    $campo = 'sum(inc_valor) as valor, MONTH(inc_fecha_pago) AS MES , YEAR(inc_fecha_pago) AS ANO';
    $tabla = 'gi_incapacidad';
    $condicion =  $where."inc_fecha_pago BETWEEN '".$fechaInical."' AND '".$fechaFinal_."' AND inc_estado_tramite = 'PAGADA'";
    $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY MONTH(inc_fecha_pago), YEAR(inc_fecha_pago)', ' ORDER BY YEAR(inc_fecha_pago) , MONTH(inc_fecha_pago) ASC');
   
    $arrayMeses = array();
    $arrayMont = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
    foreach ($respuesta as $key => $value) {
        if( $value['valor'] != '' &&  $value['valor'] != null){
        	$arrayMeses[$arrayMont[$value['MES']-1]] = $value['valor'];
        }
    }
   // var_dump($arrayMeses);
	$graph2 = new SVGGraph(500, 300, $settings);
	$values2 = array(
	 	$arrayMeses
	);
	$graph2->Values($values2);
	$colours = array(array('#1e88e5','#1e88e5'), array('#1e88e5','#1e88e5'));
	$graph2->colours = $colours;
	$output2 = $graph2->fetch('BarGraph');
	$obj_pdf->ImageSVG('@' . $output2, $x=15, $y=160, $w='', $h='', $link='', $align='', $palign='', $border=1, $fitonpage=false);

	$obj_pdf->SetFont('times', '', 13);
	$obj_pdf->AddPage();
	$content = '
	<br/>
    <br/>
	<table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:90%; text-align:center; font-size:12px;"><b>REPORTE DE PAGOS RECIBIDOS POR EMPLEADO</b></td>
                        <td style="width:10%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:90%;text-align:center;">INFORME TOTAL</td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <br/>';

	
	$where = '';
    if($_SESSION['cliente_id'] != 0){
        $where .= 'inc_empresa = '.$_SESSION['cliente_id']." AND ";
    }

    $campo = '*';
    $tabla = 'reporte_tesoreria';
    $condicion =  $where." inc_fecha_pago BETWEEN '".$fechaInical."' AND '".$fechaFinal_."' ";
    $pagadas = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, '', ' ORDER BY inc_fecha_pago DESC');



    $content .= '<table width="100%">
		<tr>
			<td style="width:5%"></td>
			<td style="width:90%">
				<table  border="1" style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px">
	                <thead>
	                    <tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
	                    	<th style="width:13%;text-align:center;"><b>C&eacute;dula</b></th>
	                        <th style="width:45%;text-align:center;"><b>Nombres</b></th>
	                        <th style="width:12%;text-align:center;"><b>F. Inicio</b></th>
	              			<th style="width:7%;text-align:center;"><b>D&iacute;as</b></th>
	                        <th style="width:12%;text-align:center;"><b>Valor</b></th>
	                        <th style="width:12%;text-align:center;"><b>F. Pago</b></th>
	                    </tr>
	                </thead>
	                <tbody>';
	$totalEstoAqui = 0;
	foreach ($pagadas as $key => $value) {
        $content .= '<tr>';
           	$content .= '<td style="width:13%;text-align:rigth;">'.$value['inc_cc_afiliado'].'</td>';
            $content .= '<td style="width:45%;text-align:left;">'.$value['emd_nombre'].'</td>';
            $content .= '<td style="width:12%;text-align:center;">'.explode(' ', $value['inc_fecha_inicio'])[0].'</td>';
            $content .= '<td style="width:7%;text-align:center;">'.dias_transcurridos($value['inc_fecha_inicio'], $value['inc_fecha_final']) .'</td>';
            if(!empty($value['inc_valor']) && $value['inc_valor'] != ''){
                $content .= '<td style="width:12%;text-align:center;">'.number_format($value['inc_valor'], 0, ',', '.').'</td>';
            }else{
                $content .= '<td></td>';
            }
            $content .= '<td style="width:12%;text-align:rigth;">'.$value['inc_fecha_pago2'].'</td>';
            $totalEstoAqui += $value['inc_valor'];
            
        $content .= '</tr>';

    }
    $content .= '</tbody>
            </table>
            <br/>
			<table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" border="1">
				<tr >
                	<th style="width:13%;text-align:center;"></th>
                    <th style="width:45%;text-align:center;"></th>
                    <th style="width:12%;text-align:center;"></th>
          			<th style="width:7%;text-align:center;"></th>
                    <th style="width:12%;text-align:center; background-color:#bdbdbd;text-align:center;">'.number_format($totalEstoAqui, 0, '.', '.').'</th>
                    <th style="width:12%;text-align:center;"></th>
                </tr>
			</table>
        </td>
        <td style="width:5%"></td>
		</tr>
	</table>';

    $obj_pdf->writeHTML($content, true, false, false, false, '');


	$nombre='Archivo_Consolidado_'.date("d-m-Y H-i-s").'.pdf';
	//header('Content-type: application/pdf');
	$obj_pdf->Output($nombre, 'I');
			

	