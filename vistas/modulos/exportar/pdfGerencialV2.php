<?php
	require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';
	require_once __DIR__.'/../../../extenciones/SVGGraph/SVGGraph.php';

	class MYPDF extends TCPDF {

		protected $processId = 0;
	    protected $header = '';
	    protected $footer = '';
	    static $errorMsg = '';
		//Page header
		public function Header() {
			$this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
	       $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

	       $this->Line(5, 5, $this->getPageWidth()-5, 5); 

	       $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
	       $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
	       $this->Line(5, 5, 5, $this->getPageHeight()-5);
		}

		// Page footer
		public function Footer() {
			// Position at 15 mm from bottom
			$this->SetY(-15);
			// Set font
			$this->SetFont('helvetica', 'N', 8);
			// Page number
			$this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

	//$anho = explode('-',$_POST['NuevoFechaInicio'])[0];
	$anho = '2019';
	$fechaInical = null;
	$fechaFinal_ = null;
	if(isset($_GET['NuevoFechaInicial'])){
		$anho = explode('-',$_GET['NuevoFechaInicial'])[0];
		$fechaInical  = $_GET['NuevoFechaInicial'];
		$fechaFinal_  = $_GET['NuevoFechaFinal'];
	}

	function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }

    if($_SESSION['cliente_id'] != 0){
	    $cliente = new ControladorClientes();
       	$respuesta = $cliente->ctrMostrarClientes('emp_id', $_SESSION['cliente_id']);
    }


	/*empezar el proceso del PDF*/
	$obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$obj_pdf->SetCreator(PDF_CREATOR);
	$obj_pdf->SetTitle("RELACIÓN DE INCAPACIDADES PAGADAS ENTRE ".$fechaInical." - ".$fechaFinal_);
	$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
	$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
	$obj_pdf->setPrintHeader(false);
	$obj_pdf->setPrintFooter(true);
	$obj_pdf->SetAutoPageBreak(TRUE, 30);
	$obj_pdf->SetFont('times', '', 15);
	// set image scale factor

	$obj_pdf->AddPage();

	$content = '';
	
	if($_SESSION['cliente_id'] != 0){

		$content .= '
		<br/><br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/>
		<img src="vistas/modulos/exportar/LOGO_2_GRANDE.jpg" alt="Logo de GEIN" />
		<br/><br/><br/><br/><br/><br/><br/>
		<table>
            <tr>
                <td width="80%">
                    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                        <tr>
                            <td style="width:90%; text-align:Justify; font-size:12px;"><b>RELACIÓN DE INCAPACIDADES PAGADAS</b></td>
                            <td style="width:10%"></td>
                        </tr>
                    </table>
                    
                    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                        <tr>
                            <td style="width:90%;text-align:Justify;"><b>'.$respuesta['emp_nombre'].'</b></td>
                            <td style="width:30%"></td>
                        </tr>
                    </table>
                    
                    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                        <tr>
                            <td style="width:90%;text-align:Justify;"><b>Periodo consultado entre '.$fechaInical.' y '.$fechaFinal_.'</b></td>
                            <td style="width:30%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';
	}else{
		$content .= '
		<br/><br/><br/><br/><br/><br/><br/>
		<br/><br/><br/><br/><br/>
		<img src="vistas/modulos/exportar/LOGO_2_GRANDE.jpg" alt="Logo de GEIN" />
		<br/><br/><br/><br/><br/><br/><br/>
		<table>
            <tr>
                <td width="80%">
                    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                        <tr>
                            <td style="width:90%; text-align:Justify; font-size:12px;"><b>RELACIÓN DE INCAPACIDADES PAGADAS</b></td>
                            <td style="width:10%"></td>
                        </tr>
                    </table>
                    
                    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                        <tr>
                            <td style="width:90%;text-align:Justify;"><b>Periodo consultado entre '.$fechaInical.' y '.$fechaFinal_.'</b></td>
                            <td style="width:30%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';
	}
	$obj_pdf->writeHTML($content);

	$obj_pdf->SetFont('times', '', 13);
	
	$obj_pdf->AddPage();

	$month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

	/*$bMargin = $obj_pdf->getBreakMargin();
	// get current auto-page-break mode
	$auto_page_break = $obj_pdf->getAutoPageBreak();

	$img_file =  __DIR__.'/LOGO_2_GRANDE.jpg';
	$obj_pdf->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
	// restore auto-page-break status
	$obj_pdf->SetAutoPageBreak($auto_page_break, $bMargin);
	// set the starting point for the page content
	$obj_pdf->setPageMark();
	/*Aqui debemos obtener las Incapacidades pagadas por la Administradora y por fecha*/
	
	$where = '';
    if($_SESSION['cliente_id'] != 0){
        $where .= 'inc_empresa = '.$_SESSION['cliente_id']." AND ";
    }

    $camposFiltro = "inc_fecha_pago";
    $selectFiltro = "reporte_tesoreria";
    $condicionFil = $where."inc_fecha_pago BETWEEN '".$fechaInical."' AND '".$fechaFinal_."' AND inc_estado_tramite !=  'EMPRESA'";
    $respuestaFil = ModeloTesoreria::mdlMostrarGroupAndOrder($camposFiltro, $selectFiltro, $condicionFil, 'GROUP BY inc_fecha_pago', 'ORDER BY inc_fecha_pago DESC');


    $content = '';
	$content .= '
	<br/>
    <br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:90%; text-align:center; font-size:12px;"><b>REPORTE DE INCAPACIDADES PAGADAS POR MES</b></td>
                        <td style="width:10%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:90%;text-align:center;">EMPRESA VS ADMINISTRADORA</td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    <br/>';
    
    $arrayEmpresa = array();
    $arrayAdminis = array();

	$strCampos___ = "count(*) as total , MONTH(inc_fecha_pago_nomina) as mes";
	$strTablas___ = "gi_incapacidad";
	$strWhere____ = '';
	if($_SESSION['cliente_id'] != 0){
	    $strWhere____ =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
	}
	$strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$fechaInical."' AND '".$fechaFinal_."' AND inc_estado_tramite =  'EMPRESA' ";
	$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');

	$nombres = '';
	$valores = '';
	$i = 0;

	foreach ($incapacidades as $key => $value) {

	    $numero = $value['mes'];
	    $arrayEmpresa[$month[$numero -1]] = $value['total'];
	}


	$strCampos___ = "count(*) as total , MONTH(inc_fecha_pago_nomina) as mes";
	$strTablas___ = "gi_incapacidad";
	$strWhere____ = '';
	if($_SESSION['cliente_id'] != 0){
	    $strWhere____ =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
	}
	$strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$fechaInical."' AND '".$fechaFinal_."' AND inc_estado_tramite !=  'EMPRESA' ";
	$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');

	$valoresP = '';
	$i = 0;

	foreach ($incapacidades as $key => $value) {
		$numero = $value['mes'];
	    $arrayAdminis[$month[$numero -1]] = $value['total'];
	}

    //var_dump($arrayEPS);
    $settings = array(
		'back_colour' => '#eee',  
		'stroke_colour' => '#000',
		'back_stroke_width' => 0, 
		'back_stroke_colour' => '#eee',
		'axis_colour' => '#333',  
		'axis_overlap' => 2,
		'axis_font' => 'Georgia', 
		'axis_font_size' => 6,
		'grid_colour' => '#666',  
		'label_colour' => '#000',
		'pad_right' => 5,         
		'pad_left' => 5,
		'link_base' => '/',       
		'link_target' => '_top',
		'minimum_grid_spacing' => 10,
		'show_legend' => true,
		'legend_position' => 'top',
		'show_grid' => false,
		'show_axis_v' => true,
		'show_axis_text_v' => true,
		'show_data_labels' => true,
		'data_label_type' => "bubble",
		'data_label_font' => 'Georgia',
		'data_label_font_size' => 7,
		'data_label_colour' => 'black',
		'data_label_font_adjust' => 0.6,
		'data_label_tail_length' =>3,
		'legend_entries' => array('Empresa', 'Administradora'),
		'legend_columns' => 2
	);



	$graph = new SVGGraph(500, 300, $settings);
	$values = array(
	 	$arrayEmpresa, $arrayAdminis
	);

	$graph->Values($values);
	$colours = array(array('#1e88e5','#1e88e5'), array('#FF0000','#FF0000'));
	$graph->colours = $colours;
	$output = $graph->fetch('GroupedBarGraph');

	$obj_pdf->ImageSVG('@' . $output, $x=15, $y=30, $w='', $h='', $link='', $align='', $palign='', $border=1, $fitonpage=false);


	$content .= '<br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/>
	<br/><br/><br/><br/>
	<br/><br/>
	<br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
    	<thead>
    		<tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
    			<th>MES</th>
    			<th>ADMINISTRADORA</th>
    			<th>EMPRESA</th>
    		</tr>
    	</thead>
    	<tbody>';

    	foreach ($arrayEmpresa as $key => $value) {
    		$content .= "<tr>";
    			$content .= "<th>".$key."</th>";
    			foreach ($arrayAdminis as $key2 => $value2) {
    				if($key2 == $key){
    					$content .= "<th style=\"text-align:center;\">".$value2." Incapacidades</th>";
    				}
    			}
    			
    			$content .= "<th style=\"text-align:center;\">".$value." Incapacidades</th>";
    		$content .= "</tr>";

    	}
    $content .='</tbody>	
    </table>';

    
    //print_r($arrayAdminis);

    $obj_pdf->writeHTML($content, true, false, false, false, '');
    $obj_pdf->AddPage();


    $content ='
	<br/><br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:80%; text-align:center; font-size:12px;"><b>REPORTE DE PAGOS POR MES</b></td>
                        <td style="width:20%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:80%;text-align:center;">EMPRESA VS ADMINISTRADORA</td>
                        <td style="width:20%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    <br/>';

	$obj_pdf->writeHTML($content, true, false, false, false, '');

	/*AQUI PONEMOS LA SIGUIENTE GRAFICA*/
	$ArrayPagoEmpresa = array();
	$ArrayPagoAdminis = array();

	$strCampos___ = "sum(inc_valor_pagado_eps) as total , MONTH(inc_fecha_pago_nomina) as mes";
	$strTablas___ = "gi_incapacidad";
	$strWhere____ = '';
	if($_SESSION['cliente_id'] != 0){
	    $strWhere____ =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
	}
	$strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$fechaInical."' AND '".$fechaFinal_."' AND inc_estado_tramite !=  'EMPRESA' ";
	$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');

	$nombres = '';
    $valores = '';
    $i = 0;

    foreach ($incapacidades as $key => $value) {
        $numero = $value['mes'];
	    $ArrayPagoEmpresa[$month[$numero -1]] = $value['total'];
    }


    $strCampos___ = "sum(inc_valor) as total , MONTH(inc_fecha_pago_nomina) as mes";
	$strTablas___ = "gi_incapacidad";
	$strWhere____ = '';
	if($_SESSION['cliente_id'] != 0){
	    $strWhere____ =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
	}
	$strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$fechaInical."' AND '".$fechaFinal_."' AND inc_estado_tramite =  'PAGADA' ";
	$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');

    $valoresP = '';
    $i = 0;

    foreach ($incapacidades as $key => $value) {
    	$numero = $value['mes'];
	    $ArrayPagoAdminis[$month[$numero -1]] = $value['total'];
    }

    $settingsX = array(
		'back_colour' => '#eee',  
		'stroke_colour' => '#000',
		'back_stroke_width' => 0, 
		'back_stroke_colour' => '#eee',
		'axis_colour' => '#333',  
		'axis_overlap' => 2,
		'axis_font' => 'Georgia', 
		'axis_font_size' => 6,
		'grid_colour' => '#666',  
		'label_colour' => '#000',
		'pad_right' => 5,         
		'pad_left' => 5,
		'link_base' => '/',       
		'link_target' => '_top',
		'minimum_grid_spacing' => 10,
		'show_legend' => true,
		'legend_position' => 'inner',
		'show_grid' => false,
		'show_axis_v' => true,
		'show_axis_text_v' => true,
		'show_data_labels' => true,
		'data_label_position' => 'above',
		'data_label_type' => "bubble",
		'data_label_font' => 'Georgia',
		'data_label_font_size' => 7,
		'data_label_colour' => 'black',
		'data_label_font_adjust' => 0.9,
		'data_label_tail_length' =>4,
		'legend_entries' => array('Administradora', 'Empresa'),
		'legend_columns' => 2
	);

    $graphX = new SVGGraph(500, 300, $settingsX);
	$valuesX = array(
	 	$ArrayPagoAdminis, $ArrayPagoEmpresa
	);

	$graphX->Values($valuesX);
	$coloursX = array(array('#FF0000','#FF0000'), array('#1e88e5','#1e88e5'));
	$graphX->colours = $coloursX;
	$outputX = $graphX->fetch('GroupedBarGraph');

	$obj_pdf->ImageSVG('@' . $outputX, $x=15, $y=30, $w='', $h='', $link='', $align='', $palign='', $border=1, $fitonpage=false);

	$content = '<br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/>
	<br/><br/><br/><br/>
	<br/><br/>
	<br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
    	<thead>
    		<tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
    			<th>MES</th>
    			<th>ADMINISTRADORA</th>
    			<th>EMPRESA</th>
    		</tr>
    	</thead>
    	<tbody>';

    	foreach ($ArrayPagoEmpresa as $key => $value) {
    		$content .= "<tr>";
    			$content .= "<th>".$key."</th>";
    			foreach ($ArrayPagoAdminis as $key2 => $value2) {
    				if($key2 == $key){
    					$content .= "<th style=\"text-align:center;\">$ ".number_format($value2, 2, ',', '.')." </th>";
    				}
    			}
    			
    			$content .= "<th style=\"text-align:center;\">$ ".number_format($value, 2, ',', '.')." </th>";
    		$content .= "</tr>";

    	}
    $content .='</tbody>	
    </table>';
    $obj_pdf->writeHTML($content, true, false, false, false, '');

	$obj_pdf->SetFont('times', '', 14);
	$obj_pdf->AddPage();
	$content = '<html>
	<body style="text-align:justify;">
	<br/>
	<table>
        <tr>
            <td width="70%">
                &nbsp;
            </td>
            <td width="30%" align="rigth">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="100px" height="60px">
            </td>
        </tr>
    </table>
	<br/>
	<br/>
	<br/>
	
	<p>
		INCAPACIDADES.CO, nace en el 2014 como una empresa especializada en el Reconocimiento de Prestaciones Económicas ante el sistema general de seguridad social en Colombia, dando respuesta a las necesidades de las compañías de incrementar su productividad, a través de la optimización de procesos al ser entregados a terceros especializados. 
	</p>
	<br/>
	<br/>
	<br/>
	<br/>
	<p>
		Trabajamos en la integración de modelos de mejora enfocados en: 
	</p>
	<br/>
	<br/>
	<br/>
	<br/>
	<ul>
		<li>Consolidación de una Compañía con mayor capacidad operativa para dar respuesta a los requerimientos de los clientes, con mayor participación en el mercado nacional.</li>
		<br/>
		<li>Ofrecer un portafolio integral de soluciones que sea atractivo para las empresas interesadas en el outsourcing como herramienta de gestión. </li>
		<br/>
		<li>Convertirnos en el aliado estratégico de nuestros clientes, acompañándolos en sus iniciativas de crecimiento y expansión a otras plazas. </li>
		<br/>
	</ul>
	<br/>
	<br/>
	<br/>
	<br/>
	<br/>
	<p>
		Comprometidos a liderar en forma permanente una política empresarial seria, guiada por los principios de respeto, equidad, responsabilidad y transparencia, participes del desarrollo económico de la región bajo un estricto respeto por la ley. 
	</p>

	<br/>
	<br/>
	<p>&nbsp;</p>
	<br/>
	<br/>
	<br/>
	<p>&nbsp;</p>
	<br/>
  
	Atte.  
	<br/>
	<br/>
	<br/>
	Luis Carlos Garcia Urquijo
	<br/>           
	CEO 
	
	</body>
	</html>
	';
	$obj_pdf->writeHTML($content, true, false, false, false, '');

	$nombre='Archivo_Consolidado_'.date("d-m-Y H-i-s").'.pdf';
	//header('Content-type: application/pdf');
	$obj_pdf->Output($nombre, 'I');
			

	