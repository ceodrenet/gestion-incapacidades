<?php 
	session_start();
    require_once dirname(__FILE__) . '/../../../modelos/dao.modelo.php';
    require_once '../../../modelos/incapacidades.modelo.php'; 
    require_once '../../../modelos/empleados.modelo.php';
    require_once '../../../modelos/diagnostico.modelo.php';  
    
    require_once dirname(__FILE__) . '/../../../controladores/mail.controlador.php';
    require_once dirname(__FILE__) . '/../../../controladores/plantilla.controlador.php'; 
	require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../controladores/empleados.controlador.php';
    require_once '../../../controladores/diagnostico.controlador.php';
    


    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }

?>
<html>
	<head>
		<style type="text/css">
			
		</style>
	</head>
	<body onload="javascript: window.print();">
		<table width="90%" style="margin: 0 auto;">
			<tr>
				<td style="text-align: left; width: 20%;">
					<img src="../../img/plantilla/logo_blanco.jpg" style="width: 100%;">
				</td>
				<td style="text-align: center; font-size: 18px;">
					COLABORADOR CON MAS INCAPACIDADES Y DIAGNOSTICOS MAS FRECUENTES.
				</td>
			</tr>
		</table>
		<br/>
		<br/>
		<table style="width:90%;margin: 0 auto;border-collapse: collapse;" border="1">
			<thead>
				<tr>
					<th colspan="4">TOP 10 COLABORADOR MAS INCAPACITADOS</th>
				</tr>
				<tr>
                	<th style="width: 10px;">#</th>
                    <th>COLABORADOR</th>
                    <th>INCAPACIDADES</th>
                    <th>TOTAL DÍAS INC.</th>
                </tr>
			</thead>
			<tbody>
                <?php
                	$item = null;
				    $valor = null;
				    if($_SESSION['cliente_id'] != 0){

				        $item = 'inc_empresa';
				        $valor = $_SESSION['cliente_id'];
				        $ctrGetTopTenIncapacidades = ControladorIncapacidades::ctrGetTopTenIncapacidades($item, $valor, $_GET['fechaInicial'], $_GET['fechaFinal']);
				    }else{

				        $ctrGetTopTenIncapacidades = ControladorIncapacidades::ctrGetTopTenIncapacidades($item, $valor, $_GET['fechaInicial'], $_GET['fechaFinal']);
				    }


                    foreach ($ctrGetTopTenIncapacidades as $key => $value) {
                        $dato = ControladorEmpleados::ctrMostrarEmpleados('emd_cedula', $value['inc_cc_afiliado'] );
                        $item = 'inc_cc_afiliado';
                        $valor = $value['inc_cc_afiliado'];
                        $datoDias = ControladorIncapacidades::ctrGetTopTenFechasSinLimite($item, $valor, $_GET['fechaInicial'], $_GET['fechaFinal']);
                        $totalDIas = 0;
                        foreach ($datoDias as $keyX => $valueres) {
                            $totalDIas += dias_transcurridos($valueres['inc_fecha_inicio'] , $valueres['inc_fecha_final']);
                        }
                        echo '<tr>';
                            echo '<td>'.($key+1).'</td>';
                            echo '<td>'.$dato['emd_nombre'].'</td>';
                            echo '<td style="text-align:center;">'.$value['total'].'</td>';
                            echo '<td style="text-align:center;">'.$totalDIas.'</td>';
                        echo '</tr>';

                    }
                ?>
            </tbody>
		</table>
		<br/>
		<br/>
		<table style="width:90%;margin: 0 auto;border-collapse: collapse;" border="1">
			<thead>
				<tr>
					<th colspan="3">TOP 10 DIAGNOSTICOS MAS FRECUENTES</th>
				</tr>
				<tr>
                    <th style="width: 10px;">#</th>
                    <th>DIAGNOSTICO</th>
                    <th style="width: 30%;"># REPETICIONES</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $item = null;
                    $valor = null;
                    if($_SESSION['cliente_id'] != 0){

                        $item = 'inc_empresa';
                        $valor = $_SESSION['cliente_id'];
                        $ctrGetTopTenDiagnosticos = ControladorIncapacidades::ctrGetTopTenDiagnosticos($item, $valor, $_GET['fechaInicial'], $_GET['fechaFinal']);
                    }else{

                        $ctrGetTopTenDiagnosticos = ControladorIncapacidades::ctrGetTopTenDiagnosticos($item, $valor, $_GET['fechaInicial'], $_GET['fechaFinal']);
                    }


                    foreach ($ctrGetTopTenDiagnosticos as $key => $value) {
                        $dato = ControladorDiagnostico::ctrMostrarDiagnostico('dia_codigo', $value['inc_diagnostico'] );
                        echo '<tr>';
                            echo '<td>'.($key+1).'</td>';
                            if($dato['name'] == null){
                                echo '<td>SIN DIAGNOSTICO</td>';    
                            }else{
                                echo '<td>'.$dato['name'].'</td>';
                            }
                            
                            echo '<td style="text-align:center;">'.$value['total'].'</td>';
                      
                        echo '</tr>';

                    }
                ?>
            </tbody>
		</table>
		<br/>
		<br/>
	</body>
</html>