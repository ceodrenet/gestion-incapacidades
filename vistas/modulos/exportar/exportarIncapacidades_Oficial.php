<?php
    
    //session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    ini_set('memory_limit','1024M');



    
    $objPHPExcel = new PHPExcel();

    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Incapacidades")
                             ->setSubject("Incapacidades Actuales")
                             ->setDescription("Descarga del historico de incapacidades registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Incapacidades");


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:AR1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:AR1')->getFont()->setBold( true );


    $objPHPExcel->getActiveSheet()->setTitle('INCAPACIDADES');

    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa");
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Cod. Nomina"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Identificación");    
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Apellidos y Nombres"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Cargo"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Salario"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Sede"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Centro de Costos"); 
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "SubCentro de Costos"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Ciudad");
    $objPHPExcel->getActiveSheet()->setCellValue("L1", "Fecha Inicio"); 
    $objPHPExcel->getActiveSheet()->setCellValue("M1", "Fecha Final"); 
    $objPHPExcel->getActiveSheet()->setCellValue("N1", "Días"); 
    $objPHPExcel->getActiveSheet()->setCellValue("O1", "Clasificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("P1", "Diagnostico"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Q1", "Descripcion"); 
    $objPHPExcel->getActiveSheet()->setCellValue("R1", "Origen"); 
    $objPHPExcel->getActiveSheet()->setCellValue("S1", "Valor Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("T1", "Valor Administradora"); 
    $objPHPExcel->getActiveSheet()->setCellValue("U1", "Valor Ajuste"); 
    $objPHPExcel->getActiveSheet()->setCellValue("V1", "Fecha Pago En Nomina");  
    $objPHPExcel->getActiveSheet()->setCellValue("W1", "Fecha Generación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("X1", "Estado Tramite");
    $objPHPExcel->getActiveSheet()->setCellValue("Y1", "Fecha de Estado");
    $objPHPExcel->getActiveSheet()->setCellValue("Z1", "Valor Pagado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AA1", "Fecha Pago"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AB1", "EPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AC1", "AFP"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AD1", "Profesional Responsable"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AE1", "Registro Medico"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AF1", "Entidad-IPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AG1", "Documentos"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AH1", "Atención"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AI1", "Estado Empleado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AJ1", "Creado Por"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AK1", "Editado Por"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AL1", "N. Radicado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AM1", "Motivo Rechazo"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AN1", "Observacion 1"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AO1", "Observacion 2"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AP1", "Observacion 3"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AQ1", "Observacion 4"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AR1", "Observacion 5"); 
    $item = null;
    $valor = null;
    if($_SESSION['cliente_id'] != 0){
        $item = 'inc_empresa';
        $valor = $_SESSION['cliente_id'];
    }
    //$incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);
    if(isset($_GET['fechaInicio']) && isset($_GET['fechaFinal']) && $_GET['fechaInicio'] != null && $_GET['fechaFinal'] != null){
        $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor, $_GET['fechaInicio'], $_GET['fechaFinal']);
    }else{
        $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);     
    }
   
    $i = 2;
    foreach ($incapacidades as $key => $value) {

        $strClasificacion = "PRORROGA";
        if($value["inc_clasificacion"] == 1){
            $strClasificacion = "INICIAL";
        }
        
        $strFecha = null;
        if($value["inc_estado_tramite"] == 'POR RADICAR'){
            $strFecha = $value["inc_fecha_generada"];
        }else if($value["inc_estado_tramite"] == 'RADICADA'){
            $strFecha = $value["inc_fecha_radicacion"];
        }else if($value["inc_estado_tramite"] == 'SOLICITUD DE PAGO'){
            $strFecha = $value["inc_fecha_solicitud"];
        }else if($value["inc_estado_tramite"] == 'PAGADA'){
            $strFecha = $value["inc_fecha_pago"];
        }else if($value["inc_estado_tramite"] == 'SIN RECONOCIMIENTO'){
            $strFecha = $value["inc_fecha_negacion_"];
        }else{
            $strFecha = $value["inc_fecha_generada"];
        }

       
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["emd_codigo_nomina"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_cedula"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["emd_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["emd_cargo"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["emd_salario"]);  
        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value["emd_sede"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value["emd_centro_costos"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["emd_subcentro_costos"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $value["emd_ciudad"]);
        $date = new DateTime($value["inc_fecha_inicio"]);
        $date2 = new DateTime($value["inc_fecha_final"]);
        $objPHPExcel->getActiveSheet()->setCellValue("L".$i, PHPExcel_Shared_Date::PHPToExcel($date));
        $objPHPExcel->getActiveSheet()->getStyle("L".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");  
        $objPHPExcel->getActiveSheet()->setCellValue("M".$i, PHPExcel_Shared_Date::PHPToExcel($date2)); 
        $objPHPExcel->getActiveSheet()->getStyle("M".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $objPHPExcel->getActiveSheet()->setCellValue("N".$i, dias_transcurridos($value["fecha1"] , $value["fecha2"]));
        $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $strClasificacion);
        $objPHPExcel->getActiveSheet()->setCellValue("P".$i, $value["inc_diagnostico"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$i, $value["dia_descripcion"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("R".$i, $value["inc_origen"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("S".$i, $value["inc_valor_pagado_empresa"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("T".$i, $value["inc_valor_pagado_eps"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("U".$i, $value["inc_valor_pagado_ajuste"]); 
        $date3 = new DateTime($value["inc_fecha_pago_nomina"]);
        $objPHPExcel->getActiveSheet()->setCellValue("V".$i, PHPExcel_Shared_Date::PHPToExcel($date3));
        $objPHPExcel->getActiveSheet()->getStyle("V".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $date4 = new DateTime($value["inc_fecha_generada"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("W".$i, PHPExcel_Shared_Date::PHPToExcel($date4));
        $objPHPExcel->getActiveSheet()->getStyle("W".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $estadoIncapacidad = '';
        if($value['inc_estado'] == '1'){
            $estadoIncapacidad = $value["inc_estado_tramite"];
        }else{
            $estadoIncapacidad = 'INCOMPLETA';
        }
        $objPHPExcel->getActiveSheet()->setCellValue("X".$i, $estadoIncapacidad);


        $date5 = new DateTime($strFecha);
        $objPHPExcel->getActiveSheet()->setCellValue("Y".$i, PHPExcel_Shared_Date::PHPToExcel($date5));
        $objPHPExcel->getActiveSheet()->getStyle("Y".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
        $objPHPExcel->getActiveSheet()->setCellValue("Z".$i, $value["inc_valor"]); 
       
        if($value["inc_fecha_pago"] != null && $value["inc_fecha_pago"] != ''){
            $date6 = new DateTime($value["inc_fecha_pago"]); 
            
            $objPHPExcel->getActiveSheet()->setCellValue("AA".$i, PHPExcel_Shared_Date::PHPToExcel($date6)); 
            
            $objPHPExcel->getActiveSheet()->getStyle("AA".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
      
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("AA".$i, $value["inc_fecha_pago"] ); 
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue("AB".$i, $value["inc_ips_afiliado"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AC".$i, $value["inc_afp_afiliado"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AD".$i, $value["inc_profesional_responsable"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AE".$i, $value["inc_prof_registro_medico"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AF".$i, $value["inc_donde_se_genera"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AG".$i, $value["inc_observacion"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AH".$i, $value["atn_descripcion"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AI".$i, $value["emd_estado"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AJ".$i, $value["creador"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AK".$i, $value["editor"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AL".$i, $value["inc_numero_radicado_v"]);
        $objPHPExcel->getActiveSheet()->setCellValue("AM".$i, $value["mot_desc_v"]);

        $campos = "incm_comentario_t" ;
        $tabla = "gi_incapacidades_comentarios";
        $where = "inm_inc_id_i = ".$value['inc_id'];
        
        $respuesta = ModeloIncapacidades::mdlMostrarGroupAndOrder($campos,$tabla,$where, null, 'ORDER BY incm_fecha_comentario ASC');
        $contComentario = 0;
        foreach ($respuesta as $key => $valuex) {
            if($contComentario == 0){
                $objPHPExcel->getActiveSheet()->setCellValue("AN".$i, $valuex["incm_comentario_t"]);
            }else if($contComentario == 1){
                $objPHPExcel->getActiveSheet()->setCellValue("AO".$i, $valuex["incm_comentario_t"]);
            }else if($contComentario == 2){
                $objPHPExcel->getActiveSheet()->setCellValue("AP".$i, $valuex["incm_comentario_t"]);    
            }else if($contComentario == 3){
                $objPHPExcel->getActiveSheet()->setCellValue("AQ".$i, $valuex["incm_comentario_t"]);
            }
            $contComentario++;
        }
        $objPHPExcel->getActiveSheet()->setCellValue("AR".$i, $value["mot_observacion_v"]);
        $i++;         
    }

    foreach(range('A','AR') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }

    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Informe_consolidado.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    ob_start();
    $writer->save("php://output");
    $xlsData = ob_get_contents();
    ob_end_clean(); 

    $response =  array(
        'op' => 'ok',
        'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
    );

    echo json_encode($response);

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }