<?php
	require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';
	require_once __DIR__.'/../../../extenciones/SVGGraph/SVGGraph.php';

	class MYPDF extends TCPDF {

		protected $processId = 0;
	    protected $header = '';
	    protected $footer = '';
	    static $errorMsg = '';
		//Page header
		public function Header() {
			$this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
	       $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

	       $this->Line(5, 5, $this->getPageWidth()-5, 5); 

	       $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
	       $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
	       $this->Line(5, 5, 5, $this->getPageHeight()-5);
		}

		// Page footer
		public function Footer() {
			// Position at 15 mm from bottom
			$this->SetY(-15);
			// Set font
			$this->SetFont('helvetica', 'N', 8);
			// Page number
			$this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}


	$anho = '2019';
	$fechaInical = null;
	$fechaFinal_ = null;
	if(isset($_GET['NuevoFechaInicial'])){
		$anho = explode('-',$_GET['NuevoFechaInicial'])[0];
		$fechaInical  = $_GET['NuevoFechaInicial'];
		$fechaFinal_  = $_GET['NuevoFechaFinal'];
	}

	if($_SESSION['cliente_id'] != 0){
	    $cliente = new ControladorClientes();
       	$respuesta = $cliente->ctrMostrarClientes('emp_id', $_SESSION['cliente_id']);
    }

    $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$obj_pdf->SetCreator(PDF_CREATOR);
	$obj_pdf->SetTitle("RELACIÓN DE INCAPACIDADES PAGADAS ENTRE ".$fechaInical." - ".$fechaFinal_);
	$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
	$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
	$obj_pdf->setPrintHeader(false);
	$obj_pdf->setPrintFooter(true);
	$obj_pdf->SetAutoPageBreak(TRUE, 30);
	$obj_pdf->SetFont('times', '', 15);
	// set image scale factor

	$obj_pdf->AddPage();


	