<?php
    
    $objPHPExcel = new PHPExcel();

    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Incapacidades Estado")
                             ->setSubject("Incapacidades Estado Por Nominas")
                             ->setDescription("Descarga del historico de incapacidades por nominas registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Incapacidades");


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:L1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold( true );


    $objPHPExcel->getActiveSheet()->setTitle('INCAPACIDADES ACEPTADAS');


    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Identificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Nombre"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Cargo"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Centro de costo"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Sede"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Fecha Inicial");
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Fecha Final"); 
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Días"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Estado");
    $objPHPExcel->getActiveSheet()->setCellValue("L1", "Nomina");

    $where = "";
    if($_SESSION['cliente_id'] != 0){
        $where .= "incn_emp_id = ".$_SESSION['cliente_id']." AND ";
    }   

    if(isset($_POST['txtNomina']) && isset($_POST['txtAnhoNomina'])){
        $where .= " incn_nomina_i = ".$_POST['txtNomina']." AND incn_anho_i = ".$_POST['txtAnhoNomina']." AND ";
    }

    $campo_ = "incn_id_i, incn_cc_v, incn_fecha_inicio_d, incn_fecha_fin_d, incn_estado_v, incn_nomina_i, incn_anho_i, incn_fecha_creacion_t, incn_emp_id, emd_nombre, emd_cargo, emp_nombre, emd_sede, emd_centro_costos, nom_desc_v";
    $tabla_ = " gi_incapacidades_nomina JOIN gi_empleados ON emd_cedula = incn_cc_v JOIN gi_empresa ON emp_id = incn_emp_id JOIN gi_nominas ON incn_nomina_i = nom_id_i";
    $condi_ = $where." incn_estado_v = 'ACEPTADA'";

    //echo "SELECT ".$campo_." FROM ".$tabla_." WHERE ".$condi_;
    $i = 2;
    $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campo_,$tabla_,$condi_, null, " ORDER BY incn_id_i DESC");
    foreach ($incapacidades as $key => $value) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nombre"]);     
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["incn_cc_v"]);    
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["emd_cargo"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["emd_centro_costos"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["emd_sede"]); 
        $date = new DateTime($value["incn_fecha_inicio_d"]);
        $newDate = new DateTime($value["incn_fecha_fin_d"]);
        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, PHPExcel_Shared_Date::PHPToExcel( $date ));
        $objPHPExcel->getActiveSheet()->getStyle("H".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, PHPExcel_Shared_Date::PHPToExcel( $newDate ));
        $objPHPExcel->getActiveSheet()->getStyle("I".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, dias_transcurridos($value["incn_fecha_inicio_d"] , $value["incn_fecha_fin_d"])); 
        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $value["incn_estado_v"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $value["nom_desc_v"]."-".$value["incn_anho_i"]);  
        $i++;         
    }

    foreach(range('A','L') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }

    $newHoja = $objPHPExcel->createSheet(1);
    $newHoja->setTitle("INCAPACIDADES RECHAZADAS");
    $newHoja
    ->getStyle('A1:M1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $newHoja->getStyle('A1:M1')->getFont()->setBold( true );


    $newHoja->setCellValue("A1", "#"); 
    $newHoja->setCellValue("B1", "Empresa"); 
    $newHoja->setCellValue("C1", "Identificación"); 
    $newHoja->setCellValue("D1", "Nombre"); 
    $newHoja->setCellValue("E1", "Cargo"); 
    $newHoja->setCellValue("F1", "Centro de costo"); 
    $newHoja->setCellValue("G1", "Sede"); 
    $newHoja->setCellValue("H1", "Fecha Inicial");
    $newHoja->setCellValue("I1", "Fecha Final"); 
    $newHoja->setCellValue("J1", "Días"); 
    $newHoja->setCellValue("K1", "Estado");
    $newHoja->setCellValue("L1", "Motivo Rechazo");
    $newHoja->setCellValue("M1", "Observación Rechazo");
    $newHoja->setCellValue("N1", "Nomina");

    $where = "";
    if($_SESSION['cliente_id'] != 0){
        $where = "incn_emp_id = ".$_SESSION['cliente_id']." AND ";
    }   

    if(isset($_POST['txtNomina']) && isset($_POST['txtAnhoNomina'])){
        $where .= " incn_nomina_i = ".$_POST['txtNomina']." AND incn_anho_i = ".$_POST['txtAnhoNomina']." AND ";
    }

    $campo_ = "incn_id_i, incn_cc_v, incn_fecha_inicio_d, incn_fecha_fin_d, incn_estado_v, incn_nomina_i, incn_anho_i, incn_fecha_creacion_t, incn_emp_id, emd_nombre, emd_cargo, emp_nombre, mot_desc_v, emd_sede, emd_centro_costos, nom_desc_v, mot_observacion_v ";
    $tabla_ = " gi_incapacidades_nomina JOIN gi_empleados ON emd_cedula = incn_cc_v JOIN gi_empresa ON emp_id = incn_emp_id JOIN gi_nominas ON incn_nomina_i = nom_id_i JOIN gi_motivos_rechazo ON mot_id_i = incn_razon_v";
    $condi_ = $where." incn_estado_v = 'RECHAZADA'";
    $i = 2;

    //echo "SELECT ".$campo_." FROM ".$tabla_." WHERE ".$condi_;
    $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campo_,$tabla_,$condi_, null, " ORDER BY incn_id_i DESC");
    foreach ($incapacidades as $key => $value) {
        $newHoja->setCellValue("A".$i, ($key+1)); 
        $newHoja->setCellValue("B".$i, $value["emp_nombre"]);     
        $newHoja->setCellValue("C".$i, $value["incn_cc_v"]);    
        $newHoja->setCellValue("D".$i, $value["emd_nombre"]); 
        $newHoja->setCellValue("E".$i, $value["emd_cargo"]); 
        $newHoja->setCellValue("F".$i, $value["emd_centro_costos"]); 
        $newHoja->setCellValue("G".$i, $value["emd_sede"]); 
        $date = new DateTime($value["incn_fecha_inicio_d"]);
        $newDate = new DateTime($value["incn_fecha_fin_d"]);
        $newHoja->setCellValue("H".$i, PHPExcel_Shared_Date::PHPToExcel( $date ));
        $newHoja->getStyle("H".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $newHoja->setCellValue("I".$i, PHPExcel_Shared_Date::PHPToExcel( $newDate ));
        $newHoja->getStyle("I".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $newHoja->setCellValue("J".$i, dias_transcurridos($value["incn_fecha_inicio_d"] , $value["incn_fecha_fin_d"])); 
        $newHoja->setCellValue("K".$i, $value["incn_estado_v"]); 
        $newHoja->setCellValue("L".$i, $value["mot_desc_v"]); 
        $newHoja->setCellValue("M".$i, $value["mot_observacion_v"]); 
        $newHoja->setCellValue("N".$i, $value["nom_desc_v"]."-".$value["incn_anho_i"]); 
        $i++;         
    }

    foreach(range('A','M') as $columnID) {
        $newHoja->getColumnDimension($columnID)
            ->setAutoSize(true);
    }

    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="IncapacidadesEstadoPorNomina.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }