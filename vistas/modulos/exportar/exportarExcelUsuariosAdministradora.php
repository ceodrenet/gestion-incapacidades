<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);

    $objPHPExcel = new PHPExcel();

    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Usuarios Administradoras")
                             ->setSubject("Usuarios Administradoras Actuales")
                             ->setDescription("Descarga del historico de Usuarios Administradoras registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Usuarios Administradoras");


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:G1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold( true );


    $objPHPExcel->getActiveSheet()->setTitle('Usuarios Administradoras');


    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Entidad"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Usuario"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Password"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Url"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Observacion");

    $where = '';
    if($_SESSION['cliente_id'] != 0){
        $where = 'use_emp_id_i = '.$_SESSION['cliente_id'];
    }

    $campos = "use_id_i, ips_nombre, use_usuario_v, use_password_v, use_url_v, emp_nombre, use_observacion_v";
    $tablas = "gi_usuarios_eps JOIN gi_ips ON use_ips_id_i = ips_id JOIN gi_empresa ON use_emp_id_i = emp_id";
    $condic = $where;
    $clientes = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tablas,$condic, null, null);

    $i = 2;
    foreach ($clientes as $key => $value) {
        
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["ips_nombre"]);    
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["use_usuario_v"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["use_password_v"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["use_url_v"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["use_observacion_v"]);  
        $i++;         
    }


    foreach(range('A','G') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }


    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="usuarios_administradoras_'.date('Y-m-d-H-i-s').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');