<?php

	header('Content-Type: application/octet-stream');
	header("Content-Transfer-Encoding: Binary"); 
	header("Content-disposition: attachment; filename=\"Fallaron.csv\"");

	$campos = "filas_fallo_fila, fila_fallo_mensaje , fila_fallo_session_id, fila_fallo_texto_error_v, fila_fallo_cedula";
	$tabla  = 'gi_filas_fallo';
	$condicion = "fila_fallo_session_id = '".$_SESSION['idSession']."'";

	$respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, '', '');

	$outputBuffer = fopen("php://output", 'w');
	$outputData = array( 
						'Fila',
						'Cedula',
						'Comentario',
						'Error'
					);
	fputcsv($outputBuffer, $outputData, ';');
	//var_dump($respuesta);
	foreach ($respuesta as $key => $value) {
		$outputData = array( 
						$value['filas_fallo_fila'],
						$value['fila_fallo_cedula'],
						$value['fila_fallo_mensaje'],
						$value['fila_fallo_texto_error_v']
					);
		
		fputcsv($outputBuffer, $outputData, ';');	
	}


	//cerramos el wrapper
	fclose($outputBuffer);

	$respuesta = ModeloTesoreria::mdlBorrar('gi_filas_fallo', 'fila_fallo_session_id = '.$_SESSION['idSession']);

	exit;

