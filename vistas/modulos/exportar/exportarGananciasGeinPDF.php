<?php
    require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';


    class MYPDF extends TCPDF {

        protected $processId = 0;
        protected $header = '';
        protected $footer = '';
        static $errorMsg = '';
        //Page header
        public function Header() {
            $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
           $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

           $this->Line(5, 5, $this->getPageWidth()-5, 5); 

           $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
           $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
           $this->Line(5, 5, 5, $this->getPageHeight()-5);

        }

        // Page footer
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            // Set font
            $this->SetFont('helvetica', 'N', 8);
            // Page number
            $this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
    }


    /*empezar el proceso del PDF*/
    $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("REPORTE GANANCIAS MES A MES");
    //$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    // set default header data

    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(true);
    $obj_pdf->setPrintFooter(true);
    $obj_pdf->SetAutoPageBreak(TRUE, 30);
    $obj_pdf->SetFont('times', '', 15);
    // set image scale factor

    $obj_pdf->AddPage();

    $fechaFinal = $_GET['fechaFinal'];
    $fechaInicial = $_GET['fechaInicial'];
    $cliente_id = $_GET['cliente_id'];

    $content = '
    <br/>
    <br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:20%"></td>
                        <td style="width:60%; text-align:center; font-size:12px;"><b>REPORTE GANANCIA MENSUAL</b></td>
                        <td style="width:20%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:30%"></td>
                        <td style="width:40%;text-align:center;">INFORME RESUMIDO</td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    
    <br/>
    <br/>
    <br/>
    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <td style="width:50%;text-align:justify;">Incapacidades.co</b></td>
            <td style="width:25%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
        </tr>
        <tr>
            <td style="width:50%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;">Fecha: '.date('d/m/Y H:i:s').'</td>
        </tr>
    </table>
    <br/>
    <br/>
    <br/>
    <table  border="1" style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <thead>
        <tr style="background-color:#bdbdbd;border:solid 1px;">
            <th>#</th>
            <th>NIT</th>
            <th>Empresa</th>
            <th>Ingreso Mes</th>
            <th>Porcentaje</th>
            <th>Comisión</th>
            <th>Facturado</th>
        </tr>
        </thead>
        <tbody>';

            $valorTotal = 0;
            $strCampos = "emp_nombre , emp_id, emp_nit, emp_porcentaje_i ";
            $strTablas = "gi_empresa";
            $condicion = 'emp_estado  = 1';
            $resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos, $strTablas, $condicion, null, " ORDER BY emp_nombre ASC"); 

            foreach ($resultado as $key => $value) {

                /*Vamos a obtener las ganancias de esa empresa en ese intervalo de tiempo */
                $strNewCampos = "SUM(inc_valor) as total";
                $strNewTablas = "gi_incapacidad";
                $strCondicion = "inc_fecha_pago BETWEEN '".$fechaInicial."' AND '".$fechaFinal."' AND   inc_empresa = ".$value['emp_id'];
                $respuestaEmp = ModeloTesoreria::mdlMostrarUnitario($strNewCampos, $strNewTablas, $strCondicion);

                $valorInc = $respuestaEmp['total'] * ($value['emp_porcentaje_i']/100);
                $valorTotal += $valorInc;
                $content .= '<tr>';
                    $content .= '<td>'.($key+1).'</td>';
                    $content .= '<td>'.$value['emp_nit'].'</td>';
                    $content .= '<td>'.mb_strtoupper($value['emp_nombre']).'</td>';
                    $content .= '<td>'.number_format($respuestaEmp['total'], 2, ',', '.').'</td>';
                    $content .= '<td>'.$value['emp_porcentaje_i'].' %</td>';
                    $content .= '<td>'.number_format($valorInc, 2, ',', '.').'</td>';
                    $content .= '<td></td>';
                $content .= '</tr>';
            }

        $content .= '<tr>
                <td colspan="4"></td>
                <td style="text-align:right;background-color:#bdbdbd;" >TOTAL</td>
                <td style="text-align:center;">'.number_format($valorTotal, 0, '.', '.').'</td>
                <td></td>
            </tr>';

    $content .= '</tbody></table>';


    $obj_pdf->writeHTML($content, true, false, false, false, '');

    $nombre='Reporte_Ganancias_Periodo_'.$fechaInicial.'_hasta_'.$fechaFinal."_".date('H:i:s').'.pdf';
    //header('Content-type: application/pdf');
    $obj_pdf->Output($nombre, 'I');
            