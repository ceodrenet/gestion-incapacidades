<?php
    

    //session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    ini_set('memory_limit','1024M');

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    
    
    $objPHPExcel = new Spreadsheet();


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:AA1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);


    $objPHPExcel->setActiveSheetIndex(0);

    

    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:X1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:X1')->getFont()->setBold( true );
    

    $objPHPExcel->getActiveSheet()->setTitle('COLABORADORES ACTIVOS');


    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Tipo de identificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Identificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Nombre"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Fecha de ingreso"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Salario"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Salario Promedio"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Estado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Fecha de retiro"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Administradora EPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("L1", "Fecha afilicación a EPS"); 
    $objPHPExcel->getActiveSheet()->setCellValue("M1", "Administradora ARL"); 
    $objPHPExcel->getActiveSheet()->setCellValue("N1", "Administradora AFP"); 
    $objPHPExcel->getActiveSheet()->setCellValue("O1", "Fecha de calificacion PCD"); 
    $objPHPExcel->getActiveSheet()->setCellValue("P1", "Entidad calificadora"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Q1", "Diagnostico"); 
    $objPHPExcel->getActiveSheet()->setCellValue("R1", "Cargo");
    $objPHPExcel->getActiveSheet()->setCellValue("S1", "Sede"); 
    $objPHPExcel->getActiveSheet()->setCellValue("T1", "Genero"); 
    $objPHPExcel->getActiveSheet()->setCellValue("U1", "Tipo de colaborador"); 
    $objPHPExcel->getActiveSheet()->setCellValue("V1", "Centro de Costo"); 
    $objPHPExcel->getActiveSheet()->setCellValue("W1", "Sub Centro de Costo");
    $objPHPExcel->getActiveSheet()->setCellValue("X1", "Ciudad");
    $objPHPExcel->getActiveSheet()->setCellValue("Y1", "Codigo Nomina"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Z1", "Correo Contacto"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AA1", "Telefono Contacto"); 


    $item = null;
    $valor = null;
    $tabla = "gi_empleados";
    if($_SESSION['cliente_id'] != 0){
        $item = 'emd_emp_id';
        $valor = $_SESSION['cliente_id'];
        $respuesta = ModeloEmpleados::mdlMostrarEmpleados_byEmpresax($tabla, $item, $valor);
    }else{
        $respuesta = ModeloEmpleados::mdlMostrarEmpleadosTotales($tabla);
    }
    
    
    $i = 2;
    foreach ($respuesta as $key => $value) {
        $estado = "ACTIVO";
        $fechaRetiro = $value["emd_fecha_retiro"];
        if($value["emd_fecha_retiro"] != NULL && $value['emd_fecha_retiro'] != ''){
            $fechaRetiro = "";
        }

        $fechaCalificacionPCL = $value["emd_fecha_calificacion_PCL"];
        if ($fechaCalificacionPCL == NULL || $fechaCalificacionPCL == "NULL") {
            $fechaCalificacionPCL = "";
        }

        $entidadCalificadora = $value["emd_entidad_calificadora"];
        if ($entidadCalificadora == NULL || $entidadCalificadora == "NULL") {
            $entidadCalificadora = "";
        }

        $diagnostico = $value["emd_diagnostico"];
        if ($diagnostico == NULL || $diagnostico == "NULL") {
            $diagnostico = "";
        }

        $centroCosto = $value["emd_centro_costos"];
        if ($centroCosto == NULL || $centroCosto == "NULL") {
            $centroCosto = "";
        }

        $subCentroCosto = $value["emd_subcentro_costos"];
        if ($subCentroCosto == NULL || $subCentroCosto == "NULL") {
            $subCentroCosto = "";
        }

        $codigoNomina = $value["emd_codigo_nomina"];
        if ($codigoNomina == NULL || $codigoNomina == "NULL") {
            $codigoNomina = "";
        }

        $telefono = $value["emd_telefono_v"];
        if ($telefono == NULL || $telefono == "NULL") {
            $telefono = "";
        }

        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, trim($value["emd_tipo_identificacion"]));
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_cedula"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["emd_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["emd_fecha_ingreso"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["emd_salario"]); 
        if($value["emd_salario_promedio"] != null && $value["emd_salario_promedio"] != ''){
            $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value["emd_salario_promedio"]); 
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("H".$i, "0");
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $estado); 
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $fechaRetiro); 
        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $value["ips_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $value["emd_fecha_afiliacion_eps"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("M".$i, $value["emd_arl_id"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $value["emd_afp_id"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $fechaCalificacionPCL); 
        $objPHPExcel->getActiveSheet()->setCellValue("P".$i, $entidadCalificadora); 
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$i, $diagnostico); 
        $objPHPExcel->getActiveSheet()->setCellValue("R".$i, $value["emd_cargo"]);
        $objPHPExcel->getActiveSheet()->setCellValue("S".$i, $value["emd_sede"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("T".$i, $value["emd_genero"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("U".$i, $value["emd_tipo_empleado"]);
        $objPHPExcel->getActiveSheet()->setCellValue("V".$i, $centroCosto); 
        $objPHPExcel->getActiveSheet()->setCellValue("W".$i, $subCentroCosto); 
        $objPHPExcel->getActiveSheet()->setCellValue("X".$i, $value["emd_ciudad"]);
        $objPHPExcel->getActiveSheet()->setCellValue("Y".$i, $codigoNomina); 
        $objPHPExcel->getActiveSheet()->setCellValue("Z".$i, $value["emd_correo_v"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AA".$i, $telefono); 
        $i++;         
    }


    foreach(range('A','AA') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    
    }

    $objPHPExcel->getActiveSheet()->getStyle('G1:G'.$i)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

    $objPHPExcel->getActiveSheet()->getStyle('H1:H'.$i)
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);


    

    $newHoja = $objPHPExcel->createSheet(1);
    $newHoja->setTitle("COLABORADORES RETIRADOS");
    $newHoja
    ->getStyle('A1:O1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:AA1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $newHoja->getStyle('A1:AA1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $newHoja->getStyle('A1:AA1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
    

    $newHoja->getStyle('A1:O1')->getFont()->setBold( true );
    

    $newHoja->setCellValue("A1", "#"); 
    $newHoja->setCellValue("B1", "Empresa"); 
    $newHoja->setCellValue("C1", "Tipo de identificación"); 
    $newHoja->setCellValue("D1", "Identificación"); 
    $newHoja->setCellValue("E1", "Nombre"); 
    $newHoja->setCellValue("F1", "Fecha de ingreso"); 
    $newHoja->setCellValue("G1", "Salario"); 
    $newHoja->setCellValue("H1", "Salario Promedio"); 
    $newHoja->setCellValue("I1", "Estado"); 
    $newHoja->setCellValue("J1", "Fecha de retiro"); 
    $newHoja->setCellValue("K1", "Administradora EPS"); 
    $newHoja->setCellValue("L1", "Fecha afilicación a EPS"); 
    $newHoja->setCellValue("M1", "Administradora ARL"); 
    $newHoja->setCellValue("N1", "Administradora AFP"); 
    $newHoja->setCellValue("O1", "Fecha de calificacion PCD"); 
    $newHoja->setCellValue("P1", "Entidad calificadora"); 
    $newHoja->setCellValue("Q1", "Diagnostico"); 
    $newHoja->setCellValue("R1", "Cargo");
    $newHoja->setCellValue("S1", "Sede"); 
    $newHoja->setCellValue("T1", "Genero"); 
    $newHoja->setCellValue("U1", "Tipo de colaborador"); 
    $newHoja->setCellValue("V1", "Centro de Costo"); 
    $newHoja->setCellValue("W1", "Sub Centro de Costo"); 
    $newHoja->setCellValue("X1", "Ciudad");
    $newHoja->setCellValue("Y1", "Codigo Nomina"); 
    $newHoja->setCellValue("Z1", "Correo Contacto"); 
    $newHoja->setCellValue("AA1", "Telefono Contacto"); 

    $item = null;
    $valor = null;
    $tabla = "gi_empleados";
    if($_SESSION['cliente_id'] != 0){
        $item = 'emd_emp_id';
        $valor = $_SESSION['cliente_id'];
        $respuesta = ModeloEmpleados::mdlMostrarEmpleados_byEmpresaY($tabla, $item, $valor);
    }else{
        $respuesta = ModeloEmpleados::mdlMostrarEmpleadosTotales($tabla);
    }

    $i = 2;
    foreach ($respuesta as $key => $value) {
        $estado = "RETIRADO";

        $fechaCalificacionPCL = $value["emd_fecha_calificacion_PCL"];
        if ($fechaCalificacionPCL == NULL || $fechaCalificacionPCL == "NULL") {
            $fechaCalificacionPCL = "";
        }

        $entidadCalificadora = $value["emd_entidad_calificadora"];
        if ($entidadCalificadora == NULL || $entidadCalificadora == "NULL") {
            $entidadCalificadora = "";
        }

        $diagnostico = $value["emd_diagnostico"];
        if ($diagnostico == NULL || $diagnostico == "NULL") {
            $diagnostico = "";
        }

        $centroCosto = $value["emd_centro_costos"];
        if ($centroCosto == NULL || $centroCosto == "NULL") {
            $centroCosto = "";
        }

        $subCentroCosto = $value["emd_subcentro_costos"];
        if ($subCentroCosto == NULL || $subCentroCosto == "NULL") {
            $subCentroCosto = "";
        }

        $codigoNomina = $value["emd_codigo_nomina"];
        if ($codigoNomina == NULL || $codigoNomina == "NULL") {
            $codigoNomina = "";
        }

        $telefono = $value["emd_telefono_v"];
        if ($telefono == NULL || $telefono == "NULL") {
            $telefono = "";
        }

        $newHoja->setCellValue("A".$i, ($key+1)); 
        $newHoja->setCellValue("B".$i, $value["emp_nombre"]); 
        $newHoja->setCellValue("C".$i, trim($value["emd_tipo_identificacion"]));
        $newHoja->setCellValue("D".$i, $value["emd_cedula"]); 
        $newHoja->setCellValue("E".$i, $value["emd_nombre"]); 
        $newHoja->setCellValue("F".$i, $value["emd_fecha_ingreso"]); 
        $newHoja->setCellValue("G".$i, $value["emd_salario"]); 
        if($value["emd_salario_promedio"] != null && $value["emd_salario_promedio"] != ''){
            $newHoja->setCellValue("H".$i, $value["emd_salario_promedio"]); 
        }else{
            $newHoja->setCellValue("H".$i, "0");
        }
        
        $newHoja->setCellValue("I".$i, $estado); 
        $newHoja->setCellValue("J".$i, $value["emd_fecha_retiro"]); 
        $newHoja->setCellValue("K".$i, $value["ips_nombre"]); 
        $newHoja->setCellValue("L".$i, $value["emd_fecha_afiliacion_eps"]); 
        $newHoja->setCellValue("M".$i, $value["emd_arl_id"]); 
        $newHoja->setCellValue("N".$i, $value["emd_afp_id"]); 
        $newHoja->setCellValue("O".$i, $fechaCalificacionPCL); 
        $newHoja->setCellValue("P".$i, $entidadCalificadora); 
        $newHoja->setCellValue("Q".$i, $diagnostico); 
        $newHoja->setCellValue("R".$i, $value["emd_cargo"]);
        $newHoja->setCellValue("S".$i, $value["emd_sede"]); 
        $newHoja->setCellValue("T".$i, $value["emd_genero"]); 
        $newHoja->setCellValue("U".$i, $value["emd_tipo_empleado"]);
        $newHoja->setCellValue("V".$i, $centroCosto); 
        $newHoja->setCellValue("W".$i, $subCentroCosto); 
        $newHoja->setCellValue("X".$i, $value["emd_ciudad"]);
        $newHoja->setCellValue("Y".$i, $codigoNomina); 
        $newHoja->setCellValue("Z".$i, $value["emd_correo_v"]); 
        $newHoja->setCellValue("AA".$i, $telefono); 
        $i++;         
    }


    foreach(range('A','AA') as $columnID) {
        $newHoja->getColumnDimension($columnID)
            ->setAutoSize(true);
    
    }

    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="empleados.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $writer = new Xlsx($objPHPExcel);
    $writer->save('php://output');
    

