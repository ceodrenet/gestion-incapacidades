<?php
    require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';
	function sanear_string($string) { 
        $string = str_replace( array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string );
        $string = str_replace( array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string ); 
        $string = str_replace( array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string ); 
        $string = str_replace( array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string ); 
        $string = str_replace( array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string ); 
        $string = str_replace( array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C',), $string ); 
        //Esta parte se encarga de eliminar cualquier caracter extraño 
        //$string = str_replace( array("\\", "¨", "º", "-", "~", "#", "@", "|", "!", "\"", "·", "$", "%", "&", "/", "(", ")", "?", "'", "¡", "¿", "[", "^", "`", "]", "+", "}", "{", "¨", "´", ">“, “< ", ";", ",", ":", "."), '', $string ); 
        return $string; 
    }

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }


    class MYPDF extends TCPDF {

        protected $processId = 0;
        protected $header = '';
        protected $footer = '';
        static $errorMsg = '';
        //Page header
        public function Header() {
            $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
           $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

           $this->Line(5, 5, $this->getPageWidth()-5, 5); 

           $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
           $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
           $this->Line(5, 5, 5, $this->getPageHeight()-5);

        }

        // Page footer
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            // Set font
            $this->SetFont('helvetica', 'N', 8);
            // Page number
            $this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
    }


    /*empezar el proceso del PDF*/
    $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("Reporte Incapacidades por Empleado");
    //$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    // set default header data

    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(true);
    $obj_pdf->setPrintFooter(true);
    $obj_pdf->SetAutoPageBreak(TRUE, 20);
    $obj_pdf->SetFont('times', '', 15);
    // set image scale factor

    $obj_pdf->AddPage();
    $title = '';
    $title2 = '';
    $empres = '';
    $nit = '';
    if($_SESSION['cliente_id'] == 0){
        $title = '<th style="text-align:center;">Empresa</th>';
        $title2 = '<th></th>';
        $empres = '<td style="width:9%;text-align:justify;"> </td>
            <td style="width:50%;text-align:justify;"></td>';
        $nit = '<td style="width:9%;text-align:justify;"></td>
            <td style="width:50%;text-align:justify;"></td>';
    }else{
        $item  = 'emp_id';
        $valor = $_SESSION['cliente_id']; 
        $empresa = ControladorClientes::ctrMostrarClientes($item, $valor);

        $empres = '<td style="width:9%;text-align:justify;">Empresa </td>
            <td style="width:50%;text-align:justify;">:&nbsp; <b>'.$empresa['emp_nombre']."</b></td>";
        $nit = '<td style="width:9%;text-align:justify;">NIT </td>
            <td style="width:50%;text-align:justify;">:&nbsp; <b>'.$empresa['emp_nit']."</b></td>";
    }

    $content = '';
    $content = '
    <br/>
    <br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:10%"></td>
                        <td style="width:70%; text-align:center; font-size:12px;"><b>DESCARGA DE USUARIOS DE ADMINISTRADORAS</b></td>
                        <td style="width:20%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:20%"></td>
                        <td style="width:50%;text-align:center;">INFORME</td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <br/>
    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            '.$empres.'
            <td style="width:14%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;">Fecha:&nbsp;&nbsp;&nbsp; '.date('d/m/Y H:i:s').'</td>
        </tr>
        <tr>
            '.$nit.'
            <td style="width:14%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
        </tr>
        
    </table>
    <br/>
    <br/>
    <table  border="1" style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px">
        <thead>
            <tr style="background-color:#bdbdbd;border:solid 1px;">
                <th style="width:4%;text-align:center;">#</th>
                <th style="width:20%;text-align:center;">Empresa</th>
                <th style="width:20%;text-align:center;">Entidad</th>
                <th style="width:10%;text-align:center;">Usuario</th>
                <th style="width:10%;text-align:center;">Password</th>
                <th style="width:10%;text-align:center;">Url</th>
                <th style="width:26%;text-align:center;">Observacion</th>
            </tr>
        </thead>
        <tbody>';
    $where = '';
    if($_SESSION['cliente_id'] != 0){
        $where = 'use_emp_id_i = '.$_SESSION['cliente_id'];
    }

    $campos = "use_id_i, ips_nombre, use_usuario_v, use_password_v, use_url_v, emp_nombre, use_observacion_v";
    $tablas = "gi_usuarios_eps JOIN gi_ips ON use_ips_id_i = ips_id JOIN gi_empresa ON use_emp_id_i = emp_id";
    $condic = $where;
    $clientes = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tablas,$condic, null, null);

    $i = 2;
    foreach ($clientes as $key => $value) {
        $content .= "<tr>";
        $content .= "<td style=\"width:4%;text-align:center;\">".($key+1)."</td>";
        
           $content .= "<td style=\"width:20%;text-align:center;\">".$value['emp_nombre']."</td>"; 
        
        $content .= "<td style=\"width:20%;text-align:center;\">".$value['ips_nombre']."</td>";
        $content .= "<td style=\"width:10%;text-align:center;\">".$value['use_usuario_v']."</td>";
        $content .= "<td style=\"width:10%;text-align:center;\">".$value['use_password_v']."</td>";
        $content .= "<td style=\"width:10%;text-align:center;\">".$value['use_url_v']."</td>";
        $content .= '<td style="width:26%;text-align:center;">'.$value['use_observacion_v'].'</td>'; 
        $content .= '</tr>';
    }

    $content .= '
        </tbody>
    </table>';
    $obj_pdf->writeHTML($content, true, false, false, false, '');

    $nombre='Informe_usuarios_administradoras_'.date("d-m-Y H-i-s").'.pdf';
    //header('Content-type: application/pdf');
    $obj_pdf->Output($nombre, 'I');
            