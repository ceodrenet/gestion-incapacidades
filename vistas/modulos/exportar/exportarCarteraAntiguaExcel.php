<?php 
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

    if(isset($_GET['exportar'])){
		
		

    	require_once dirname(__FILE__) . '/../../../controladores/incapacidades.controlador.php';
		require_once dirname(__FILE__) . '/../../../modelos/tesoreria.modelo.php';

    	$carteraTotal18 = ControladorIncapacidades::getSumIncapacidades(2018, null);
	    $carteraTotal19 = ControladorIncapacidades::getSumIncapacidades(2019, null);
	    $carteraTotal20 = ControladorIncapacidades::getSumIncapacidades(2020, null);

	    $countTotal18 = ControladorIncapacidades::getCountIncapacidades(2018, null);
	    $countTotal19 = ControladorIncapacidades::getCountIncapacidades(2019, null);
	    $countTotal20 = ControladorIncapacidades::getCountIncapacidades(2020, null);

	    $carteraPagadas18 = ControladorIncapacidades::getSumIncapacidades(2018, "PAGADA");
	    $carteraPagadas19 = ControladorIncapacidades::getSumIncapacidades(2019, "PAGADA");
	    $carteraPagadas20 = ControladorIncapacidades::getSumIncapacidades(2020, "PAGADA");

	    $countPagadas18 = ControladorIncapacidades::getCountIncapacidades(2018, "PAGADA");
	    $countPagadas19 = ControladorIncapacidades::getCountIncapacidades(2019, "PAGADA");
	    $countPagadas20 = ControladorIncapacidades::getCountIncapacidades(2020, "PAGADA");

	    

	    $carteraNegadas18 = ControladorIncapacidades::getSumIncapacidades(2018, 'SIN RECONOCIMIENTO');
	    $carteraNegadas19 = ControladorIncapacidades::getSumIncapacidades(2019, 'SIN RECONOCIMIENTO');
	    $carteraNegadas20 = ControladorIncapacidades::getSumIncapacidades(2020, 'SIN RECONOCIMIENTO');

	    $countNegadas18 = ControladorIncapacidades::getCountIncapacidades(2018, "SIN RECONOCIMIENTO");
	    $countNegadas19 = ControladorIncapacidades::getCountIncapacidades(2019, "SIN RECONOCIMIENTO");
	    $countNegadas20 = ControladorIncapacidades::getCountIncapacidades(2020, "SIN RECONOCIMIENTO");

	    $carteraPorRadicar18 = ControladorIncapacidades::getSumIncapacidades(2018, 'POR RADICAR');
	    $carteraPorRadicar19 = ControladorIncapacidades::getSumIncapacidades(2019, 'POR RADICAR');
	    $carteraPorRadicar20 = ControladorIncapacidades::getSumIncapacidades(2020, 'POR RADICAR');

	    $countPorRadicar18 = ControladorIncapacidades::getCountIncapacidades(2018, "POR RADICAR");
	    $countPorRadicar19 = ControladorIncapacidades::getCountIncapacidades(2019, "POR RADICAR");
	    $countPorRadicar20 = ControladorIncapacidades::getCountIncapacidades(2020, "POR RADICAR");

	    $carteraRadicadas18 = ControladorIncapacidades::getSumIncapacidades(2018, 'RADICADA');
	    $carteraRadicadas19 = ControladorIncapacidades::getSumIncapacidades(2019, 'RADICADA');
	    $carteraRadicadas20 = ControladorIncapacidades::getSumIncapacidades(2020, 'RADICADA');

	    $countReadicadas18 = ControladorIncapacidades::getCountIncapacidades(2018, "RADICADA");
	    $countReadicadas19 = ControladorIncapacidades::getCountIncapacidades(2019, "RADICADA");
	    $countReadicadas20 = ControladorIncapacidades::getCountIncapacidades(2020, "RADICADA");

	    $totalCartera = 0;
	    $totalCarteraPagada = 0;
	    $totalCarteraNegada = 0;
	    $totalCarteraPorRadicar = 0;
	    $totalCarteraRadicada = 0;

    	$objPHPExcel = new Spreadsheet();
	    $objPHPExcel->setActiveSheetIndex(0);
	    $objPHPExcel->getProperties()->setCreator("GEIN")
	                             ->setLastModifiedBy("".$_SESSION['nombres'])
	                             ->setTitle("GEIN - Reporte Cartera Antigua")
	                             ->setSubject("Reporte Cartera Antigua")
	                             ->setDescription("Descarga el Reporte de Cartera Antigua")
	                             ->setKeywords("Office 2007 openxml php")
	                             ->setCategory("Reporte Cartera Antigua");
	   
	    $objPHPExcel->getActiveSheet()->setTitle('CARTERA ANTIGUA');

	    $objPHPExcel->getActiveSheet()->getStyle('A2:M2')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
	    $objPHPExcel->getActiveSheet()->getStyle('A2:M2')->getFill()
		    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
		    ->getStartColor()
		    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

		$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getFont()
	        ->setBold(true);

       	$objPHPExcel->getActiveSheet()->getStyle('A2:M2')->getFont()
	        ->setBold(true);	

	    $objPHPExcel->getActiveSheet()->getStyle('A1:M1')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle('A2:M2')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->mergeCells('B1:D1');
        $objPHPExcel->getActiveSheet()->mergeCells('E1:G1');
        $objPHPExcel->getActiveSheet()->mergeCells('H1:J1');
     	$objPHPExcel->getActiveSheet()->mergeCells('K1:M1');

        /*Encabezado Parte de Arriba*/
        $objPHPExcel->getActiveSheet()->setCellValue("B1", "2018"); 
        $objPHPExcel->getActiveSheet()->setCellValue("E1", "2019"); 
        $objPHPExcel->getActiveSheet()->setCellValue("H1", "2020"); 
        $objPHPExcel->getActiveSheet()->setCellValue("K1", "TOTAL"); 

        $objPHPExcel->getActiveSheet()->setCellValue("B2", "VALOR"); 
        $objPHPExcel->getActiveSheet()->setCellValue("C2", "PORCENTAJE"); 
        $objPHPExcel->getActiveSheet()->setCellValue("D2", "CANTIDAD"); 

        $objPHPExcel->getActiveSheet()->setCellValue("E2", "VALOR"); 
        $objPHPExcel->getActiveSheet()->setCellValue("F2", "PORCENTAJE"); 
        $objPHPExcel->getActiveSheet()->setCellValue("G2", "CANTIDAD"); 

        $objPHPExcel->getActiveSheet()->setCellValue("H2", "VALOR"); 
        $objPHPExcel->getActiveSheet()->setCellValue("I2", "PORCENTAJE"); 
        $objPHPExcel->getActiveSheet()->setCellValue("J2", "CANTIDAD"); 

        $objPHPExcel->getActiveSheet()->setCellValue("K2", "VALOR"); 
        $objPHPExcel->getActiveSheet()->setCellValue("L2", "PORCENTAJE"); 
        $objPHPExcel->getActiveSheet()->setCellValue("M2", "CANTIDAD"); 
        /*Hasta aqui*/

        /*Encabezado lado izquierdo*/
        $objPHPExcel->getActiveSheet()->setCellValue("A3", "CARTERA"); 
        $objPHPExcel->getActiveSheet()->setCellValue("A4", "PAGADAS"); 
        $objPHPExcel->getActiveSheet()->setCellValue("A5", "NEGADAS"); 
        $objPHPExcel->getActiveSheet()->setCellValue("A6", "POR RADICAR"); 
        $objPHPExcel->getActiveSheet()->setCellValue("A7", "RADICADAS"); 
        /*Hasta aqui*/

        /*Cuerpo del Reporte*/
        /*Cartera Total*/
        $objPHPExcel->getActiveSheet()->setCellValue("B3", $carteraTotal18); 
        $objPHPExcel->getActiveSheet()->getStyle("B3")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("C3", 100); 
        $objPHPExcel->getActiveSheet()->setCellValue("D3", $countTotal18); 

        $objPHPExcel->getActiveSheet()->setCellValue("E3", $carteraTotal19); 
        $objPHPExcel->getActiveSheet()->getStyle("E3")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

            
        $objPHPExcel->getActiveSheet()->setCellValue("F3", 100); 
        $objPHPExcel->getActiveSheet()->setCellValue("G3", $countTotal19); 

        $objPHPExcel->getActiveSheet()->setCellValue("H3", $carteraTotal20); 
        $objPHPExcel->getActiveSheet()->getStyle("H3")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("I3", 100); 
        $objPHPExcel->getActiveSheet()->setCellValue("J3", $countTotal20); 

        $totalCartera = $carteraTotal20 + $carteraTotal19 + $carteraTotal18;
        $objPHPExcel->getActiveSheet()->setCellValue("K3", $totalCartera); 
     	$objPHPExcel->getActiveSheet()->getStyle("K3")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("L3", 100); 
        $objPHPExcel->getActiveSheet()->setCellValue("M3", $countTotal20+$countTotal19+$countTotal18); 


        /*Cartera Pagada*/
        $objPHPExcel->getActiveSheet()->setCellValue("B4", $carteraPagadas18); 
        $objPHPExcel->getActiveSheet()->getStyle("B4")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("C4", number_format((($carteraPagadas18 * 100) / $carteraTotal18), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("D4", $countPagadas18); 

        $objPHPExcel->getActiveSheet()->setCellValue("E4", $carteraPagadas19); 
        $objPHPExcel->getActiveSheet()->getStyle("E4")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("F4", number_format((($carteraPagadas19 * 100) / $carteraTotal19), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("G4", $countPagadas19); 

        $objPHPExcel->getActiveSheet()->setCellValue("H4", $carteraPagadas20); 
        $objPHPExcel->getActiveSheet()->getStyle("H4")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("I4", number_format((($carteraPagadas20 * 100) / $carteraTotal20), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("J4", $countPagadas20); 

        $totalCarteraPagada = $carteraPagadas20 + $carteraPagadas18 + $carteraPagadas19;
        $objPHPExcel->getActiveSheet()->setCellValue("K4", $totalCarteraPagada); 
     	$objPHPExcel->getActiveSheet()->getStyle("K4")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

        $objPHPExcel->getActiveSheet()->setCellValue("L4", number_format((($totalCarteraPagada * 100) / $totalCartera), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("M4", $countPagadas20+$countPagadas19+$countPagadas18); 

        /*Cartera Negadas*/
        $objPHPExcel->getActiveSheet()->setCellValue("B5", $carteraNegadas18); 
        $objPHPExcel->getActiveSheet()->getStyle("B5")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("C5", number_format((($carteraNegadas18 * 100) / $carteraTotal18), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("D5", $countNegadas18); 

        $objPHPExcel->getActiveSheet()->setCellValue("E5", $carteraNegadas19); 
        $objPHPExcel->getActiveSheet()->getStyle("E5")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("F5", number_format((($carteraNegadas19 * 100) / $carteraTotal19), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("G5", $countNegadas19); 

        $objPHPExcel->getActiveSheet()->setCellValue("H5", $carteraNegadas20); 
        $objPHPExcel->getActiveSheet()->getStyle("H5")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("I5", number_format((($carteraNegadas20 * 100) / $carteraTotal20), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("J5", $countNegadas20); 

        $totalCarteraNegada = $carteraNegadas20 + $carteraNegadas19 + $carteraNegadas18;
        $objPHPExcel->getActiveSheet()->setCellValue("K5", $totalCarteraNegada); 
     	$objPHPExcel->getActiveSheet()->getStyle("K5")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

        $objPHPExcel->getActiveSheet()->setCellValue("L5", number_format((($totalCarteraNegada * 100) / $totalCartera), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("M5", $countNegadas20+$countNegadas19+$countNegadas18); 


        /*Cartera Por Radicar*/
        $objPHPExcel->getActiveSheet()->setCellValue("B6", $carteraPorRadicar18); 
        $objPHPExcel->getActiveSheet()->getStyle("B6")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("C6", number_format((($carteraPorRadicar18 * 100) / $carteraTotal18), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("D6", $countPorRadicar18); 

        $objPHPExcel->getActiveSheet()->setCellValue("E6", $carteraPorRadicar19); 
        $objPHPExcel->getActiveSheet()->getStyle("E6")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("F6", number_format((($carteraPorRadicar19 * 100) / $carteraTotal19), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("G6", $countPorRadicar19); 

        $objPHPExcel->getActiveSheet()->setCellValue("H6", $carteraPorRadicar20);
        $objPHPExcel->getActiveSheet()->getStyle("H6")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("I6", number_format((($carteraPorRadicar20 * 100) / $carteraTotal20), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("J6", $countPorRadicar20); 

        $totalCarteraPorRadicar = $carteraPorRadicar20 + $carteraPorRadicar18 + $carteraPorRadicar19;
        $objPHPExcel->getActiveSheet()->setCellValue("K6", $totalCarteraPorRadicar); 
     	$objPHPExcel->getActiveSheet()->getStyle("K6")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

        $objPHPExcel->getActiveSheet()->setCellValue("L6", number_format((($totalCarteraPorRadicar * 100) / $totalCartera), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("M6", $countPorRadicar20+$countPorRadicar19+$countPorRadicar18); 


        /*Cartera Radicadas*/
        $objPHPExcel->getActiveSheet()->setCellValue("B7", $carteraRadicadas18); 
        $objPHPExcel->getActiveSheet()->getStyle("B7")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("C7", number_format((($carteraRadicadas18 * 100) / $carteraTotal18), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("D7", $countReadicadas18); 

        $objPHPExcel->getActiveSheet()->setCellValue("E7", $carteraRadicadas19); 
        $objPHPExcel->getActiveSheet()->getStyle("E7")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("F7", number_format((($carteraRadicadas19 * 100) / $carteraTotal19), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("G7", $countReadicadas19); 

        $objPHPExcel->getActiveSheet()->setCellValue("H7", $carteraRadicadas20); 
        $objPHPExcel->getActiveSheet()->getStyle("H7")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("I7", number_format((($carteraRadicadas20 * 100) / $carteraTotal20), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("J7", $countReadicadas19); 

        $totalCarteraRadicada = $carteraRadicadas20 + $carteraRadicadas19 + $carteraRadicadas18;
        $objPHPExcel->getActiveSheet()->setCellValue("K7", $totalCarteraRadicada); 
     	$objPHPExcel->getActiveSheet()->getStyle("K7")
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
        $objPHPExcel->getActiveSheet()->setCellValue("L7", number_format((($totalCarteraRadicada * 100) / $totalCartera), 2)); 
        $objPHPExcel->getActiveSheet()->setCellValue("M7", $countReadicadas20+$countReadicadas19+$countReadicadas18); 

        foreach(range('A','L') as $columnID) {
	        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
	            ->setAutoSize(true);
	    }

	    /*Parte de las EPS*/
	    /**/
	    $objPHPExcel->getActiveSheet()->getStyle('A9:M9')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
	    $objPHPExcel->getActiveSheet()->getStyle('A9:M9')->getFill()
		    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
		    ->getStartColor()
		    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
		$objPHPExcel->getActiveSheet()->getStyle('A9:M9')->getFont()
	        ->setBold(true);
	    $objPHPExcel->getActiveSheet()->getStyle('A9:M9')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle('A10:M10')->getFill()
		    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
		    ->getStartColor()
		    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
	 	$objPHPExcel->getActiveSheet()->getStyle('A10:M10')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$objPHPExcel->getActiveSheet()->getStyle('A10:M10')->getFont()
	        ->setBold(true);
	    $objPHPExcel->getActiveSheet()->getStyle('A10:M10')
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        

        $objPHPExcel->getActiveSheet()->mergeCells('A9:M9');
   		$objPHPExcel->getActiveSheet()->setCellValue("A9", "DETALLE VALORES POR ENTIDAD"); 
   		/*Encabezado*/
   		$objPHPExcel->getActiveSheet()->mergeCells('A10:E10');
   		$objPHPExcel->getActiveSheet()->mergeCells('F10:G10');
   		$objPHPExcel->getActiveSheet()->mergeCells('I10:J10');
   		$objPHPExcel->getActiveSheet()->mergeCells('L10:M10');
   		$objPHPExcel->getActiveSheet()->setCellValue("A10", "ENTIDAD");
   		$objPHPExcel->getActiveSheet()->setCellValue("F10", "TOTAL");
   		$objPHPExcel->getActiveSheet()->setCellValue("H10", "PAGADAS");
   		$objPHPExcel->getActiveSheet()->setCellValue("I10", "NEGADAS");
   		$objPHPExcel->getActiveSheet()->setCellValue("K10", "POR RADICAR");
   		$objPHPExcel->getActiveSheet()->setCellValue("L10", "RADICADA");

   		$eps = ControladorIncapacidades::getTotalEps(null);
   		$i = 11;
	 	$totalCartera = 0;
	 	$CarteraPagada = 0;
	    $CarteraNegada = 0;
	    $CarteraPorRadicar = 0;
	    $CarteraRadicada = 0;
	    $totalCarteraPagada = 0;
	    $totalCarteraNegada = 0;
	    $totalCarteraPorRadicar = 0;
	    $totalCarteraRadicada = 0;

   		foreach ($eps as $key => $value) {

   			$totalCartera += $value['total'];
   			$CarteraPagada = ControladorIncapacidades::getTotalEpsIndividual("PAGADA", $value['ips_id']);
		    $CarteraNegada = ControladorIncapacidades::getTotalEpsIndividual("SIN RECONOCIMIENTO", $value['ips_id']);
		    $CarteraPorRadicar =  ControladorIncapacidades::getTotalEpsIndividual("POR RADICAR", $value['ips_id']);
		    $CarteraRadicada = ControladorIncapacidades::getTotalEpsIndividual("RADICADA", $value['ips_id']);

		    $totalCarteraPagada += $CarteraPagada;
		    $totalCarteraNegada += $CarteraNegada;
		    $totalCarteraPorRadicar += $CarteraPorRadicar;
		    $totalCarteraRadicada += $CarteraRadicada;

   			$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':E'.$i.'');
	   		$objPHPExcel->getActiveSheet()->mergeCells('F'.$i.':G'.$i.'');
	   		$objPHPExcel->getActiveSheet()->mergeCells('I'.$i.':J'.$i.'');
	   		$objPHPExcel->getActiveSheet()->mergeCells('L'.$i.':M'.$i.'');
	   		$objPHPExcel->getActiveSheet()->setCellValue("A".$i, $value['ips_nombre']);
	   		$objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value['total']);
	   		$objPHPExcel->getActiveSheet()->setCellValue("H".$i, $CarteraPagada);
	   		$objPHPExcel->getActiveSheet()->setCellValue("I".$i, $CarteraNegada);
	   		$objPHPExcel->getActiveSheet()->setCellValue("K".$i, $CarteraPorRadicar);
	   		$objPHPExcel->getActiveSheet()->setCellValue("L".$i, $CarteraRadicada);
   		 	$objPHPExcel->getActiveSheet()->getStyle("B".$i.":M".$i)
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
	   		$i++;
   		}

		$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':E'.$i.'');
   		$objPHPExcel->getActiveSheet()->mergeCells('F'.$i.':G'.$i.'');
   		$objPHPExcel->getActiveSheet()->mergeCells('I'.$i.':J'.$i.'');
   		$objPHPExcel->getActiveSheet()->mergeCells('L'.$i.':M'.$i.'');
   		$objPHPExcel->getActiveSheet()->setCellValue("A".$i, "TOTAL");
   		$objPHPExcel->getActiveSheet()->setCellValue("F".$i, $totalCartera);
   		$objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalCarteraPagada);
   		$objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalCarteraNegada);
   		$objPHPExcel->getActiveSheet()->setCellValue("K".$i, $totalCarteraPorRadicar);
   		$objPHPExcel->getActiveSheet()->setCellValue("L".$i, $totalCarteraRadicada);
 		$objPHPExcel->getActiveSheet()->getStyle("B".$i.":M".$i)
            ->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':M'.$i.'')->getFill()
		    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
		    ->getStartColor()
		    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
	 	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':M'.$i.'')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':M'.$i.'')->getFont()
	        ->setBold(true);


        /*Siguiente Hoja*/
     	$newHoja = $objPHPExcel->createSheet(1);
    	$newHoja->setTitle("CONSOLIDADO INCAPACIDADES");
	    $newHoja
	    ->getStyle('A1:AR1')
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    $newHoja->getStyle('A1:AR1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
	    $newHoja->getStyle('A1:AR1')->getFill()
	    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	    ->getStartColor()
	    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

	    $newHoja->getStyle('A1:AR1')->getFont()->setBold( true );


	    $newHoja->setTitle('INCAPACIDADES');

	    $newHoja->setCellValue("A1", "#"); 
	    $newHoja->setCellValue("B1", "Empresa");
	    $newHoja->setCellValue("C1", "Cod. Nomina"); 
	    $newHoja->setCellValue("D1", "Identificación");    
	    $newHoja->setCellValue("E1", "Apellidos y Nombres"); 
	    $newHoja->setCellValue("F1", "Cargo"); 
	    $newHoja->setCellValue("G1", "Salario"); 
	    $newHoja->setCellValue("H1", "Sede"); 
	    $newHoja->setCellValue("I1", "Centro de Costos"); 
	    $newHoja->setCellValue("J1", "SubCentro de Costos"); 
	    $newHoja->setCellValue("K1", "Ciudad");
	    $newHoja->setCellValue("L1", "Fecha Inicio"); 
	    $newHoja->setCellValue("M1", "Fecha Final"); 
	    $newHoja->setCellValue("N1", "Días"); 
	    $newHoja->setCellValue("O1", "Clasificación"); 
	    $newHoja->setCellValue("P1", "Diagnostico"); 
	    $newHoja->setCellValue("Q1", "Descripcion"); 
	    $newHoja->setCellValue("R1", "Origen"); 
	    $newHoja->setCellValue("S1", "Valor Empresa"); 
	    $newHoja->setCellValue("T1", "Valor Administradora"); 
	    $newHoja->setCellValue("U1", "Valor Ajuste"); 
	    $newHoja->setCellValue("V1", "Fecha Pago En Nomina");  
	    $newHoja->setCellValue("W1", "Fecha Generación"); 
	    $newHoja->setCellValue("X1", "Estado Tramite");
	    $newHoja->setCellValue("Y1", "Fecha de Estado");
	    $newHoja->setCellValue("Z1", "Valor Pagado"); 
	    $newHoja->setCellValue("AA1", "Fecha Pago"); 
	    $newHoja->setCellValue("AB1", "EPS"); 
	    $newHoja->setCellValue("AC1", "AFP"); 
	    $newHoja->setCellValue("AD1", "Profesional Responsable"); 
	    $newHoja->setCellValue("AE1", "Registro Medico"); 
	    $newHoja->setCellValue("AF1", "Entidad-IPS"); 
	    $newHoja->setCellValue("AG1", "Documentos"); 
	    $newHoja->setCellValue("AH1", "Atención"); 
	    $newHoja->setCellValue("AI1", "Estado Empleado"); 
	    $newHoja->setCellValue("AJ1", "Creado Por"); 
	    $newHoja->setCellValue("AK1", "Editado Por"); 
	    $newHoja->setCellValue("AL1", "N. Radicado"); 
	    $newHoja->setCellValue("AM1", "Motivo Rechazo"); 
	    $newHoja->setCellValue("AN1", "Observacion 1"); 
	    $newHoja->setCellValue("AO1", "Observacion 2"); 
	    $newHoja->setCellValue("AP1", "Observacion 3"); 
	    $newHoja->setCellValue("AQ1", "Observacion 4"); 
	    $newHoja->setCellValue("AR1", "Observacion 5"); 
	    $item = null;
	    $valor = null;
	    if($_SESSION['cliente_id'] != 0){
	        $item = 'inc_empresa';
	        $valor = $_SESSION['cliente_id'];
	    }

	    $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor, null, null, null, '%Y-%m-%d');     
   	 	$i = 2;
	    foreach ($incapacidades as $key => $value) {

	        $strClasificacion = "PRORROGA";
	        if($value["inc_clasificacion"] == 1){
	            $strClasificacion = "INICIAL";
	        }
	        
	        $strFecha = $value['inc_Fecha_estado'];
	        /*if($value["inc_estado_tramite"] == 'POR RADICAR'){
	            $strFecha = $value["inc_fecha_generada"];
	        }else if($value["inc_estado_tramite"] == 'RADICADA'){
	            $strFecha = $value["inc_fecha_radicacion"];
	        }else if($value["inc_estado_tramite"] == 'SOLICITUD DE PAGO'){
	            $strFecha = $value["inc_fecha_solicitud"];
	        }else if($value["inc_estado_tramite"] == 'PAGADA'){
	            $strFecha = $value["inc_fecha_pago"];
	        }else if($value["inc_estado_tramite"] == 'SIN RECONOCIMIENTO'){
	            $strFecha = $value["inc_fecha_negacion_"];
	        }else{
	            $strFecha = $value["inc_fecha_generada"];
	        }*/

	       
	        $newHoja->setCellValue("A".$i, ($key+1)); 
	        $newHoja->setCellValue("B".$i, $value["emp_nombre"]); 
	        $newHoja->setCellValue("C".$i, $value["emd_codigo_nomina"]); 
	        $newHoja->setCellValue("D".$i, $value["emd_cedula"]); 
	        $newHoja->setCellValue("E".$i, $value["emd_nombre"]); 
	        $newHoja->setCellValue("F".$i, $value["emd_cargo"]); 
	        $newHoja->setCellValue("G".$i, $value["emd_salario"]);  
	        $newHoja->setCellValue("H".$i, $value["emd_sede"]); 
	        $newHoja->setCellValue("I".$i, $value["emd_centro_costos"]); 
	        $newHoja->setCellValue("J".$i, $value["emd_subcentro_costos"]); 
	        $newHoja->setCellValue("K".$i, $value["emd_ciudad"]);
	        $date = new DateTime($value["inc_fecha_inicio"]);
	        $date2 = new DateTime($value["inc_fecha_final"]);
	        $newHoja->setCellValue("L".$i, PHPExcel_Shared_Date::PHPToExcel($date));
	        $newHoja->getStyle("L".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");  
	        $newHoja->setCellValue("M".$i, PHPExcel_Shared_Date::PHPToExcel($date2)); 
	        $newHoja->getStyle("M".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
	        $newHoja->setCellValue("N".$i, ControladorIncapacidades::dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]));
	        $newHoja->setCellValue("O".$i, $strClasificacion);
	        $newHoja->setCellValue("P".$i, $value["inc_diagnostico"]); 
	       // $newHoja->setCellValue("Q".$i, $value["dia_descripcion"]); 
	        $newHoja->setCellValue("R".$i, $value["inc_origen"]); 
	        $newHoja->setCellValue("S".$i, $value["inc_valor_pagado_empresa"]); 
	        $newHoja->setCellValue("T".$i, $value["inc_valor_pagado_eps"]); 
	        $newHoja->setCellValue("U".$i, $value["inc_valor_pagado_ajuste"]); 
	        $date3 = new DateTime($value["inc_fecha_pago_nomina"]);
	        $newHoja->setCellValue("V".$i, PHPExcel_Shared_Date::PHPToExcel($date3));
	        $newHoja->getStyle("V".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
	        $date4 = new DateTime($value["inc_fecha_generada"]); 
	        $newHoja->setCellValue("W".$i, PHPExcel_Shared_Date::PHPToExcel($date4));
	        $newHoja->getStyle("W".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
	        $estadoIncapacidad = '';
	       
	        $estadoIncapacidad = $value["inc_estado_tramite"];
	       
	        $newHoja->setCellValue("X".$i, $estadoIncapacidad);


	        $date5 = new DateTime($strFecha);
	        $newHoja->setCellValue("Y".$i, PHPExcel_Shared_Date::PHPToExcel($date5));
	        $newHoja->getStyle("Y".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
	        $newHoja->setCellValue("Z".$i, $value["inc_valor"]); 
	       
	        if($value["inc_fecha_pago"] != null && $value["inc_fecha_pago"] != ''){
	            $date6 = new DateTime($value["inc_fecha_pago"]); 
	            
	            $newHoja->setCellValue("AA".$i, PHPExcel_Shared_Date::PHPToExcel($date6)); 
	            
	            $newHoja->getStyle("AA".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
	      
	        }else{
	            $newHoja->setCellValue("AA".$i, $value["inc_fecha_pago"] ); 
	        }
	        
	        $newHoja->setCellValue("AB".$i, $value["inc_ips_afiliado"]);
	        $newHoja->setCellValue("AC".$i, $value["inc_afp_afiliado"]); 
	        $newHoja->setCellValue("AD".$i, $value["inc_profesional_responsable"]); 
	        $newHoja->setCellValue("AE".$i, $value["inc_prof_registro_medico"]); 
	        $newHoja->setCellValue("AF".$i, $value["inc_donde_se_genera"]); 
	        $newHoja->setCellValue("AG".$i, "NA"); 
	        $newHoja->setCellValue("AH".$i, $value["atn_descripcion"]);
	        $newHoja->setCellValue("AI".$i, $value["est_emp_desc_v"]);
	        $newHoja->setCellValue("AJ".$i, $value["creador"]);
	        $newHoja->setCellValue("AK".$i, $value["editor"]);
	        $newHoja->setCellValue("AL".$i, $value["inc_numero_radicado_v"]);
	        $newHoja->setCellValue("AM".$i, $value["mot_desc_v"]);

	        $campos = "incm_comentario_t" ;
	        $tabla = "gi_incapacidades_comentarios";
	        $where = "inm_inc_id_i = ".$value['inc_id'];
	        
	        $respuesta = ModeloIncapacidades::mdlMostrarGroupAndOrder($campos,$tabla,$where, null, 'ORDER BY incm_fecha_comentario ASC');
	        $contComentario = 0;
	        foreach ($respuesta as $key => $valuex) {
	            if($contComentario == 0){
	                $newHoja->setCellValue("AN".$i, $valuex["incm_comentario_t"]);
	            }else if($contComentario == 1){
	                $newHoja->setCellValue("AO".$i, $valuex["incm_comentario_t"]);
	            }else if($contComentario == 2){
	                $newHoja->setCellValue("AP".$i, $valuex["incm_comentario_t"]);    
	            }else if($contComentario == 3){
	                $newHoja->setCellValue("AQ".$i, $valuex["incm_comentario_t"]);
	            }
	            $contComentario++;
	        }
	        $newHoja->setCellValue("AR".$i, $value["mot_observacion_v"]);
	        $i++;         
	    }

	    foreach(range('A','AR') as $columnID) {
	        $newHoja->getColumnDimension($columnID)
	            ->setAutoSize(true);
	    }

	    $objPHPExcel->setActiveSheetIndex(0);
	    $nombre='Reporte_Cartera_Antigua'.date("d-m-Y H-i-s").'.xlsx';
	     // Write the Excel file to filename some_excel_file.xlsx in the current directory
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header('Content-Disposition: attachment;filename="'.$nombre.'"');
	    header('Cache-Control: max-age=0');
	    // If you're serving to IE 9, then the following may be needed
	    header('Cache-Control: max-age=1');

	    // If you're serving to IE over SSL, then the following may be needed
	    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	    header ('Pragma: public'); // HTTP/1.0
	    $writer = new Xlsx($objPHPExcel);
	    $writer->save('php://output');
    }