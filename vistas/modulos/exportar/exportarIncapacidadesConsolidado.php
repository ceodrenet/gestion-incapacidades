<?php
    session_start();

    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    
    require_once dirname(__FILE__) . '/../../../extenciones/Excel.php';
    require_once dirname(__FILE__) . '/../../../controladores/mail.controlador.php';
    require_once dirname(__FILE__) . '/../../../controladores/plantilla.controlador.php';    
    require_once dirname(__FILE__) . '/../../../controladores/incapacidades.controlador.php';
    require_once dirname(__FILE__) . '/../../../controladores/tesoreria.controlador.php';
    require_once dirname(__FILE__) . '/../../../modelos/dao.modelo.php';
    require_once dirname(__FILE__) . '/../../../modelos/incapacidades.modelo.php';
    require_once dirname(__FILE__) . '/../../../modelos/tesoreria.modelo.php';

    $objPHPExcel = new PHPExcel();

    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getProperties()->setCreator("GEIN")
                             ->setLastModifiedBy("".$_SESSION['nombres'])
                             ->setTitle("GEIN - Incapacidades")
                             ->setSubject("Incapacidades Actuales")
                             ->setDescription("Descarga del historico de incapacidades registrado en el sistema")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Incapacidades");


    $objPHPExcel->getActiveSheet()
    ->getStyle('A1:AA1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->getFont()->setBold( true );


    $objPHPExcel->getActiveSheet()->setTitle('INCAPACIDADES');


    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
    $objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("C1", "Administradora"); 
    $objPHPExcel->getActiveSheet()->setCellValue("D1", "Identificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Nombre"); 
    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Diagnostico"); 
    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Origen"); 
    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Clasificación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Fecha Inicio"); 
    $objPHPExcel->getActiveSheet()->setCellValue("J1", "Fecha Final"); 
    $objPHPExcel->getActiveSheet()->setCellValue("K1", "Días"); 
    $objPHPExcel->getActiveSheet()->setCellValue("L1", "Tipo Generación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("M1", "Profesional Responsable"); 
    $objPHPExcel->getActiveSheet()->setCellValue("N1", "Registro del Profesional"); 
    $objPHPExcel->getActiveSheet()->setCellValue("O1", "Donde se genero"); 
    $objPHPExcel->getActiveSheet()->setCellValue("P1", "Fecha Generación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Q1", "Observación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("R1", "Estado Tramite");
    $objPHPExcel->getActiveSheet()->setCellValue("S1", "Fecha Radicación"); 
    $objPHPExcel->getActiveSheet()->setCellValue("T1", "Fecha Solicitud"); 
    $objPHPExcel->getActiveSheet()->setCellValue("U1", "Valor Solicitado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("V1", "Fecha Pago"); 
    $objPHPExcel->getActiveSheet()->setCellValue("W1", "Valor Pagado"); 
    $objPHPExcel->getActiveSheet()->setCellValue("X1", "Fecha Pago Nomina");  
    $objPHPExcel->getActiveSheet()->setCellValue("Y1", "Valor Empresa"); 
    $objPHPExcel->getActiveSheet()->setCellValue("Z1", "Valor Administradora"); 
    $objPHPExcel->getActiveSheet()->setCellValue("AA1", "Valor Ajuste");  

    $where = null;
    if($_SESSION['cliente_id'] != 0){
        $where = 'inc_empresa = '.$_SESSION['cliente_id']." AND ";
    }

    $campos = "emp_nombre, emd_cedula, emd_nombre, inc_diagnostico, inc_origen, inc_clasificacion, DATE_FORMAT(inc_fecha_inicio, '%d/%m/%Y') as inc_fecha_inicio, inc_fecha_inicio as fecha1, DATE_FORMAT(inc_fecha_final, '%d/%m/%Y') as inc_fecha_final, inc_fecha_final as fecha2, inc_tipo_generacion, inc_profesional_responsable, inc_prof_registro_medico, inc_donde_se_genera, DATE_FORMAT(inc_fecha_generada, '%d/%m/%Y') as inc_fecha_generada, inc_observacion, inc_estado_tramite,  DATE_FORMAT(inc_fecha_radicacion, '%d/%m/%Y') as inc_fecha_radicacion,  DATE_FORMAT(inc_fecha_solicitud, '%d/%m/%Y') as inc_fecha_solicitud, DATE_FORMAT(inc_fecha_pago, '%d/%m/%Y') as inc_fecha_pago, inc_valor, DATE_FORMAT(inc_fecha_pago_nomina, '%d/%m/%Y') as inc_fecha_pago_nomina, inc_valor_pagado_nomina, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado ,  ip4.ips_nombre as inc_afp_afiliado , DATE_FORMAT(inc_fecha_negacion_, '%d/%m/%Y') as inc_fecha_negacion_, inc_valor_solicitado, inc_valor_pagado_empresa, inc_valor_pagado_eps, inc_valor_pagado_ajuste";
    $tablas = "gi_incapacidad LEFT JOIN gi_ips as ip2 ON ip2.ips_id = inc_ips_afiliado LEFT JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id ";
    $clasi = '';
    if($_POST['clasificacion'] == '0'){
        /*Procedemos por el filtro*/
        $clasi = 'inc_estado_tramite = \''.$_POST['filtro'].'\'';

    }else{

        if($_POST['clasificacion'] == 'X'){
            /*Procedemos por el origen*/
            $clasi = 'inc_origen = \''.$_POST['filtro'].'\'';

        }else{
            /*Procedemos por clasificacion*/
            $clasi = 'inc_clasificacion = \''.$_POST['clasificacion'].'\'';
        }   
    }
    $condic = $where." ".$clasi."  AND inc_fecha_pago_nomina BETWEEN '".$_POST['inc_fecha_inicio']."' AND '".$_POST['inc_fecha_final']."'";

    $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condic, '', ' ORDER BY emd_nombre');

    $i = 2;
    foreach ($incapacidades as $key => $value) {
        $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nombre"]); 

        if($value['inc_clasificacion'] != 4){
            if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["inc_arl_afiliado"]);  
            }else{
                $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["inc_ips_afiliado"]);  
            }
        }else{
            $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["inc_afp_afiliado"]);    
        }

        
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_cedula"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["emd_nombre"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["inc_diagnostico"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["inc_origen"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value["inc_clasificacion"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value["inc_fecha_inicio"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["inc_fecha_final"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, dias_transcurridos($value["fecha1"] , $value["fecha2"])); 
        $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $value["inc_tipo_generacion"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("M".$i, $value["inc_profesional_responsable"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $value["inc_prof_registro_medico"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $value["inc_donde_se_genera"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("P".$i, $value["inc_fecha_generada"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("Q".$i, $value["inc_observacion"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("R".$i, $value["inc_estado_tramite"]);
        $objPHPExcel->getActiveSheet()->setCellValue("S".$i, $value["inc_fecha_radicacion"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("T".$i, $value["inc_fecha_solicitud"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("U".$i, $value["inc_valor_solicitado"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("V".$i, $value["inc_fecha_pago"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("W".$i, $value["inc_valor"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("X".$i, $value["inc_fecha_pago_nomina"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("Y".$i, $value["inc_valor_pagado_empresa"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("Z".$i, $value["inc_valor_pagado_eps"]); 
        $objPHPExcel->getActiveSheet()->setCellValue("AA".$i, $value["inc_valor_pagado_ajuste"]); 
        $i++;         
    }


    foreach(range('A','Z') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    }


    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="incapacidadesFiltradas_'.date('Y-m-d-H-i-s').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer->save('php://output');

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }