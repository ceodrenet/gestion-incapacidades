<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Ganancias 
            <small>Mis ganancias mes a mes</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Ganancias</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Filtros</h3>
            </div>
            <div class="box-body">
                <form autocomplete="off"> 
                    <div class="row">
                        <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar-o"></i>
                                    </span>
                                    <input class="form-control fecha" type="text" name="NuevoFechaInicio" id="NuevoFechaInicio2" placeholder="Ingresar Fecha Inicial" required="true">
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar-o"></i>
                                    </span>
                                    <input class="form-control fecha" type="text" name="NuevoFechaFinal" id="NuevoFechaFinal2" placeholder="Ingresar Fecha Final" required="true">
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <button class="btn btn-primary btn-block" type="button" id="btnBuscarRangos_general"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>
                        <div class="col-md-2 Imprimir" id="" style="display: none;">
                            <div class="form-group">
                                <a role="button" class="btn btn-primary btn-block" type="button" id="btnBuscarRangos_general_2" target="_bank"><i class="fa fa-file-excel-o"></i>&nbsp;Exportar</a>
                            </div>
                        </div>
                        <!--<div class="col-md-2 Imprimir" id="" style="display: none;">
                            <div class="form-group">
                                <a role="button" class="btn btn-primary btn-block" type="button" id="btnBuscarRangos_general_3" target="_bank"><i class="fa fa-file-pdf-o"></i>&nbsp;Exportar</a>
                            </div>
                        </div>-->
                    </div>
                </form>
                
            </div>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Reporte de ganancias mensual Incapacidades.co</h3>
            </div>
            <div class="box-body" id="resultadosGananciasIncap">
                <table style="width:100%;" id="tablaClientes" class="table table-bordered table-striped dt-responsive tablas">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 10%;">NIT</th>
                            <th>Empresa</th>
                            <th>Ingreso Mes</th>
                            <th>Porcentaje</th>
                            <th style="width: 20%;">Comisión</th>
                            <th style="width: 20%;">Facturado</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th style="width: 10%;">Nit</th>
                            <th>Empresa</th>
                            <th>Ingreso Mes</th>
                            <th>Porcentaje</th>
                            <th style="width: 20%;">Comisión</th>
                            <th style="width: 20%;">Facturado</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">

    $.fn.datepicker.dates['es'] = {
        days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
        daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        today: "Today",
        clear: "Clear",
        format: "yyyy-mm-dd", 
        weekStart: 0
    };

    $("#NuevoFechaInicio2").datepicker({
        language: "es",
        autoclose: true,
        todayHighlight: true
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#NuevoFechaFinal2').datepicker('setStartDate', minDate);
    });


    $("#NuevoFechaFinal2").datepicker({
        language: "es",
        autoclose: true,
        todayHighlight: true
    });


    $("#btnBuscarRangos_general").click(function(){
 
        var fechaInicio = $("#NuevoFechaInicio2").val();
        var fechaFinal  = $("#NuevoFechaFinal2").val();
        var paso = 0;
        if(fechaFinal.length < 0){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha final es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });

        }   

        if(fechaInicio.length < 1){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha Inicial es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
        }

        if(paso == 0){
            $.ajax({
                url    : 'vistas/modulos/reporteGanancias.php',
                type   : 'post',
                data   :{
                    fechaInicial  : fechaInicio,
                    fechaFinal    : fechaFinal,
                    cliente_id    : $("#session").val()
                },
                dataType : 'html',
                success : function(data){
                    $("#resultadosGananciasIncap").html(data);
                    $("#btnBuscarRangos_general_2").attr('href', 'index.php?exportar=gananExcel&fechaInicial='+ fechaInicio +'&fechaFinal='+ fechaFinal+'&cliente_id='+$("#session").val());
                    $("#btnBuscarRangos_general_3").attr('href', 'index.php?exportar=gananPdf&fechaInicial='+ fechaInicio +'&fechaFinal='+ fechaFinal+'&cliente_id='+$("#session").val());
                    $(".Imprimir").show();
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            })
        }
    });
</script>