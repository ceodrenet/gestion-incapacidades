<footer class="main-footer">
	<div class="pull-right hidden-xs">
      	<b>Version</b> 1.9.0
    </div>
	<strong>Copyright &copy; 2018 - 2021 <a href="http://groupge.co" target="_blank">Incapacidades.co</a>.</strong> Todos los derechos reservados.
</footer>