<?php session_start(); ?>
<?php $month = array(
	"Enero" 	=> '01', 
	"Febrero" 	=> '02', 
	"Marzo" 	=> '03', 
	"Abril" 	=> '04', 
	"Mayo" 		=> '05', 
	"Junio" 	=> '06', 
	"Julio" 	=> '07', 
	"Agosto" 	=> '08', 
	"Septiembre" => '09', 
	"Octubre" 	=> '10', 
	"Noviembre" => '11', 
	"Diciembre" => '12'); ?>
<?php 
require_once '../../../modelos/dao.modelo.php';
require_once '../../../modelos/tesoreria.modelo.php'; 
?>
<!-- Empresa, admnistradora, pagado, pago real del mes (Valor Pagado )-->
<div class="row">
    <div class="col-md-6">
        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">
                    INCAPACIDADES EMPRESA x ADMINISTRADORA
                </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="pieChart_1" style="height:300px"></canvas>        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="box box-danger box-solid">
            <div class="box-header">
                <h3 class="box-title">
                    INFORME GRAFICO - PAGOS MENSUALES
                </h3>
            </div>
            <div class="box-body">
                <canvas id="pieChart_2" style="height:300px"></canvas>
            </div>
        </div>
    </div>

</div>
<div class="row">
	<div class="col-md-12">
		<div class="box box-danger box-solid">
            <div class="box-header">
                <h3 class="box-title">
                    ESTADO DE INCAPACIDADES
                </h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-hover">
                	<thead>
                		<tr>
                			<th>MES</th>
                			<th>EMPRESA</th>
                			<th>#</th>
                			<th>%</th>
                			<th>POR COBRAR</th>
                			<th>#</th>
                			<th>%</th>
                			<th>PAGADAS</th>
                			<th>#</th>
                			<th>%</th>
                			<th>INGRESO MES</th>
                			<th>#</th>
                			<th>%</th>
                		</tr>
                	</thead>
                	<tbody>
                		<?php 
                        $totalEmpresa = 0;
                        $totalAdminis = 0;
                        $totalRecupea = 0;
                        $totalPagado_ = 0;

                        $totalEmpresaN = 0;
                        $totalAdminisN = 0;
                        $totalRecupeaN = 0;
                        $totalPagado_N = 0;

                		foreach ($month as $key => $value) {

                			$empresa = getDataMonthEmpresa($_POST['anho'], $value);
                			$admnistradora = getDataMonthAdministradora($_POST['anho'], $value);
                			$pagasdas = getDataMonthPagadasAdmin($_POST['anho'], $value);
                			$pagadasMes = getDataMonthPagadas($_POST['anho'], $value);
                			$total = $empresa + $admnistradora;
                			$porcEmpresa = 0;
                			$porcAdminis = 0;
                			$porcPagadas = 0;
                			$porcPagadasReal = 0;
                            $totalEmpresa += $empresa;
                            $totalAdminis += $admnistradora;
                            $totalRecupea += $pagasdas;
                            $totalPagado_ += $pagadasMes;
                			if($total > 0){
                				$porcEmpresa = $empresa * 100 / $total;	
                				$porcAdminis = $admnistradora * 100 / $total;
                				$porcPagadas = $pagasdas * 100 / $total;
                				$porcPagadasReal = $pagadasMes * 100 / $total;
                			}
                			
                            $totalEmpresaN += getDataMonthEmpresaCount($_POST['anho'], $value);
                            $totalAdminisN += getDataMonthAdministradoraCount($_POST['anho'], $value);
                            $totalRecupeaN += getDataMonthPagadasAdminCount($_POST['anho'], $value);
                            $totalPagado_N += getDataMonthPagadasCount($_POST['anho'], $value);

                			echo "<tr>";
                				echo "<td>".$key."</td>";
                				echo "<td>".number_format($empresa, 0, '.', '.')."</td>";
                				echo "<td>".getDataMonthEmpresaCount($_POST['anho'], $value)."</td>";
                				echo "<td>".number_format($porcEmpresa, 2)."</td>";
                				echo "<td>".number_format($admnistradora, 0, '.', '.')."</td>";
                				echo "<td>".getDataMonthAdministradoraCount($_POST['anho'], $value)."</td>";
                				echo "<td>".number_format($porcAdminis, 2)."</td>";
                				echo "<td>".number_format($pagasdas, 0, '.', '.')."</td>";
                				echo "<td>".getDataMonthPagadasAdminCount($_POST['anho'], $value)."</td>";
                				echo "<td>".number_format($porcPagadas , 2)."</td>";
                				echo "<td>".number_format($pagadasMes, 0, '.', '.')."</td>";
                				echo "<td>".getDataMonthPagadasCount($_POST['anho'], $value)."</td>";
                				echo "<td>".number_format($porcPagadasReal, 2)."</td>";
                			echo "</tr>";
                		}

                        echo "<tr>";
                                echo "<td>Totales</td>";
                                echo "<td>".number_format($totalEmpresa, 0, '.', '.')."</td>";
                                echo "<td>".number_format($totalEmpresaN, 0, '.', '.')."</td>";
                                echo "<td></td>";
                                echo "<td>".number_format($totalAdminis, 0, '.', '.')."</td>";
                                echo "<td>".number_format($totalAdminisN, 0, '.', '.')."</td>";
                                echo "<td></td>";
                                echo "<td>".number_format($totalRecupea, 0, '.', '.')."</td>";
                                echo "<td>".number_format($totalRecupeaN, 0, '.', '.')."</td>";
                                echo "<td></td>";
                                echo "<td>".number_format($totalPagado_ , 0, '.', '.')."</td>";
                                echo "<td>".number_format($totalPagado_N , 0, '.', '.')."</td>";
                                echo "<td></td>";
                            echo "</tr>";

                		?>
                	</tbody>
                </table>
            </div>
        </div>
	</div>
</div>
<input type="hidden" name="base64Imagen1" id="base64Imagen1"/>
<input type="hidden" name="base64Imagen2" id="base64Imagen2"/>

<?php 
	function  getDataMonthEmpresa($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'SUM(inc_valor_pagado_empresa) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_valor_pagado_empresa IS NOT NULL  ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthAdministradora($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'SUM(inc_valor_pagado_eps) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != 'EMPRESA' ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthPagadasAdmin($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'SUM(inc_valor) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthPagadas($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'SUM(inc_valor) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA'  ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	/*Contar las incapacidades*/
	function  getDataMonthEmpresaCount($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'COUNT(inc_valor_pagado_empresa) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_valor_pagado_empresa IS NOT NULL  ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthAdministradoraCount($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'COUNT(inc_valor_pagado_eps) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != 'EMPRESA' ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthPagadasAdminCount($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'COUNT(inc_valor) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthPagadasCount($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'COUNT(inc_valor) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA'  ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}
?>

<?php 
	$year = $_POST['anho'].'-01-01';
    $otherYear = $_POST['anho'].'-12-31';
?>
<script type="text/javascript">
	var barChart1, barChart2;
	$(function(){
		getDatosPrimeraGrafica();
		getDatosSegundaGrafica();
	});

	function done(){
		var url=barChart1.toBase64Image();
		document.getElementById("base64Imagen1").value = url;
	}

	function dona(){
		var url2=barChart2.toBase64Image();
		document.getElementById("base64Imagen2").value = url2;
	}

	function getDatosPrimeraGrafica(){
 		<?php 
            $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

            $whereCliente = '';
            if($_SESSION['cliente_id'] != 0){
                $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
            }

            $campos = 'SUM(inc_valor_pagado_eps) as total , MONTH(inc_fecha_pago_nomina) as mes';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != 'EMPRESA'  ";
            $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');

            $nombres = '';
            $valores = '';
            $valoreP = '';
            $valoreE = '';
            $i = 0;

            foreach ($incapacidades as $key => $value) {
                $numero = $value['mes'];
                if($i == 0){
                    $nombres .= "'".$month[$numero -1]."'";
                    $valores .= "'".$value['total']."'"; 
                }else{
                    $nombres .= " , '".$month[$numero -1]."'";
                    $valores .= " , '".$value['total']."'"; 
                }
                $i++;
            }

            $campos = 'SUM(inc_valor) as total , MONTH(inc_fecha_pago_nomina) as mes';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
            $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');
            $i = 0;
            foreach ($incapacidades as $key => $value) {
                
                if($i == 0){
                    $valoreP .= "'".$value['total']."'"; 
                }else{
                    $valoreP .= ", '".$value['total']."'"; 
                }
                $i++;
            }

            $campos = 'SUM(inc_valor_pagado_empresa) as total , MONTH(inc_fecha_pago_nomina) as mes';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_valor_pagado_empresa IS NOT NULL  ";
            $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');
            $i = 0;

            foreach ($incapacidades as $key => $value) {
                $numero = $value['mes'];
                if($i == 0){
                    $valoreE .= "'".$value['total']."'"; 
                }else{
                    $valoreE .= " , '".$value['total']."'"; 
                }
                $i++;
            }
        ?>

        var densityCanvas = document.getElementById("pieChart_1").getContext('2d');
       
        var densityData_empresa = {
            label: 'Empresa 1 y 2 Días',
            data: [<?php echo $valoreE;?>],
            borderColor:'#545454',
            backgroundColor : '#545454',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var densityData = {
            label: 'Por Cobrar',
            data: [<?php echo $valores;?>],
            borderColor:'#006191',
            backgroundColor : '#006191',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var densityData_pagadas = {
            label: 'Pagadas',
            data: [<?php echo $valoreP; ?>],
            borderColor:'#ff5757',
            backgroundColor : '#ff5757',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        barChart1 = new Chart(densityCanvas, {
            type: 'line',
            data: {
                labels: [<?php echo $nombres; ?>],
                datasets: [densityData_empresa, densityData, densityData_pagadas],
            },
            options: {
            	bezierCurve : false,
    			animation: {
				    onComplete: done
			  	},
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor : $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (value) {
                                return addCommas(value)
                            }
                        }
                    }]
                }
            }
        });
	}

	function getDatosSegundaGrafica(){
 		<?php 
            $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

            $whereCliente = '';
            if($_SESSION['cliente_id'] != 0){
                $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
            }

            $campos = 'SUM(inc_valor_pagado_eps) as total , MONTH(inc_fecha_pago_nomina) as mes';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != 'EMPRESA'  ";
            $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');

            $nombres = '';
            $valores = '';
            $valoreP = '';
            $valoreE = '';
            $i = 0;

            foreach ($incapacidades as $key => $value) {
                $numero = $value['mes'];
                if($i == 0){
                    $nombres .= "'".$month[$numero -1]."'";
                    $valores .= "'".$value['total']."'"; 
                }else{
                    $nombres .= " , '".$month[$numero -1]."'";
                    $valores .= " , '".$value['total']."'"; 
                }
                $i++;
            }

            $campos = 'SUM(inc_valor) as total , MONTH(inc_fecha_pago) as mes';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
            $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');
            $i = 0;
            foreach ($incapacidades as $key => $value) {
                
                if($i == 0){
                    $valoreP .= "'".$value['total']."'"; 
                }else{
                    $valoreP .= ", '".$value['total']."'"; 
                }
                $i++;
            }
        ?>

        var densityCanvas = document.getElementById("pieChart_2").getContext('2d');

        var densityData = {
            label: 'Por Cobrar',
            data: [<?php echo $valores;?>],
            borderColor:'#006191',
            backgroundColor : '#006191',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var densityData_pagadas = {
            label: 'Ingresos Mes',
            data: [<?php echo $valoreP; ?>],
            borderColor:'#e40808',
            backgroundColor : '#e40808',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        barChart2 = new Chart(densityCanvas, {
            type: 'line',
            data: {
                labels: [<?php echo $nombres; ?>],
                datasets: [densityData, densityData_pagadas],
            },
            options: {
            	bezierCurve : false,
    			animation: {
				    onComplete: dona
			  	},
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor : $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (value) {
                                return addCommas(value)
                            }
                        }
                    }]
                }
            }
        });
	}

	function addCommas(nStr){
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
</script>