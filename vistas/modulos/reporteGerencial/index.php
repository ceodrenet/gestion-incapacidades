
<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REPORTES-GERENCIAL</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Gestión Anual </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                  INFORME DE GESTION ANUAL GERENCIAL
                </h5>
            </div>
            <div class="card-body">
                 <form id="formEnvioReporteGestionAnual" autocomplete="off" method="post" action="vistas/modulos/reporteGerencial/exportar.php">
                        <input type="hidden" name="anho" id="anho">
                                <input type="hidden" name="imagen1" id="nombreImagen1">
                                <input type="hidden" name="fila2" id="imagenBase642">
                                <input type="hidden" name="imagen3" id="nombreImagen3">
                                <input type="hidden" name="fila4" id="imagenBase644">
                    <div class="row">
                    <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                   
                        <div class="col-md-6">
                            <div class="mb-6">
                            <input type="text" name="txtYearOfInitR" id="txtYearOfInitR" placeholder="Año a revisar" value="<?php echo date('Y');?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnSacarReporte"><i class="fa fa-search"></i>&nbsp;Generar</button>
                            </div>
                        </div>
                    </div>    
                </form>
            </div>
        </div>
    </div>
        <!--<div class="row">
                <div class="col-md-12" id="contenidoInicio">
                    
                </div>
            </div> -->
            <section class="content" id="contenidoInicio">
        
             </section>
</div>

    <!-- Main content -->
  
</div>
<!-- Required charts js -->
<script src="vistas/assets/assets/libs/chart.js/chart.min.js"></script>

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>

<script type="text/javascript">
    $(function(){
        //getdata();

        //$("#txtYearOfInitR").flatpickr({
          //  autoclose: true,
            //todayHighlight: true,
            //viewMode: "years", 
            //minViewMode: "years",
            //format: "yyyy"
       // }).on('changeDate', function (selected) {
         //   var minDate = $("#txtYearOfInitR").val();
           // getdata();
       // });

        $("#btnSacarReporte").click(function(e){
            e.preventDefault();
            var d = new Date();
            var newNombre = "img"+d.getDate()+""+d.getHours()+""+d.getMinutes()+""+d.getSeconds()+""+d.getMilliseconds();
            var newNombre2 = "img_"+d.getDate()+""+d.getHours()+""+d.getMinutes()+""+d.getSeconds()+""+d.getMilliseconds();
            $("#nombreImagen1").val(newNombre);
            $("#imagenBase642").val(btoa($("#base64Imagen1").val()));
            $("#nombreImagen3").val(newNombre2);
            $("#imagenBase644").val(btoa($("#base64Imagen2").val()));
            $("#anho").val($("#txtYearOfInitR").val());
            $("#exportar").submit();
        });

        function getdata(){
            $.ajax({
                url: "vistas/modulos/reporteGerencial/proceso.php",
                method: 'post',
                data  : { anho : $("#txtYearOfInitR").val()},
                dataType    : 'html',
                success     : function(data){
                    $("#contenidoInicio").html(data);
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            });
        }
    });
</script>