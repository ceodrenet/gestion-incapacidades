<?php
	session_start();

	require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';
	require_once __DIR__.'../../../controladores/mail.controlador.php';
    require_once __DIR__.'../../../controladores/plantilla.controlador.php';
	require_once __DIR__.'/../../../controladores/clientes.controlador.php';

	require_once __DIR__.'../../../modelos/dao.modelo.php';
	require_once __DIR__.'/../../../modelos/tesoreria.modelo.php';
	require_once __DIR__.'/../../../modelos/clientes.modelo.php';
	$empres = '';
    $nit = '';
    if($_SESSION['cliente_id'] == 0){
        $title = '<th style="text-align:center;">Empresa</th>';
        $title2 = '<th></th>';
        $empres = '<td style="width:9%;text-align:justify;"> </td>
            <td style="width:50%;text-align:justify;"></td>';
        $nit = '<td style="width:9%;text-align:justify;"></td>
            <td style="width:50%;text-align:justify;"></td>';
    }else{
        $item  = 'emp_id';
        $valor = $_SESSION['cliente_id']; 
        $empresa = ControladorClientes::ctrMostrarClientes($item, $valor);

        $empres = '<td style="width:9%;text-align:justify;">Empresa </td>
            <td style="width:50%;text-align:justify;">:&nbsp; <b>'.$empresa['emp_nombre']."</b></td>";
        $nit = '<td style="width:9%;text-align:justify;">NIT </td>
            <td style="width:50%;text-align:justify;">:&nbsp; <b>'.$empresa['emp_nit']."</b></td>";
    }

    $filepath = '';
    $filepath2 = '';
    if(isset($_POST['imagen1']) && $_POST['imagen1'] != ''){
        $baseFromJavascript = base64_decode($_POST['fila2']);
        $directorio = __DIR__."/estadisticas/";
        if (!file_exists($directorio)) {
            mkdir($directorio, 0755);
        }
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $baseFromJavascript));
        $filepath = $directorio.$_POST['imagen1'].".png"; // or image.jpg
        file_put_contents($filepath, $data);
    }


    if(isset($_POST['imagen3']) && $_POST['imagen3'] != ''){
        $baseFromJavascript = base64_decode($_POST['fila4']);
        $directorio = __DIR__."/estadisticas/";
        if (!file_exists($directorio)) {
            mkdir($directorio, 0755);
        }
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $baseFromJavascript));
        $filepath2 = $directorio.$_POST['imagen3'].".png"; // or image.jpg
        file_put_contents($filepath2, $data);
    }


     $month = array(
		"Enero" 	=> '01', 
		"Febrero" 	=> '02', 
		"Marzo" 	=> '03', 
		"Abril" 	=> '04', 
		"Mayo" 		=> '05', 
		"Junio" 	=> '06', 
		"Julio" 	=> '07', 
		"Agosto" 	=> '08', 
		"Septiembre" => '09', 
		"Octubre" 	=> '10', 
		"Noviembre" => '11', 
		"Diciembre" => '12');

    $content = '
    <br/>
    <br/>
    <table width="100%">
        <tr>
            <td width="80%">
                <table  width="100%">
                    <tr>
                        <td style="width:100%; text-align:center; font-size:16px;color:red;"><b>INFORME GERENCIAL</b></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="../exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            '.$empres.'
            <td style="width:14%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;">Fecha:&nbsp;&nbsp;&nbsp; '.date('d/m/Y H:i:s').'</td>
        </tr>
        <tr>
            '.$nit.'
            <td style="width:14%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;">Periodo :&nbsp;&nbsp;<b>'.$_POST['anho'].'</b> </td>
        </tr>
    </table>
    <br/>
    <hr/>
    <table width="100%">
    	<tr>
    		<td width="50%">
    			<img src="estadisticas/'.$_POST['imagen1'].'.png" alt="imagen del grafico" width="400px" height="300px">
    		</td>
    		<td width="50%">
    			<img src="estadisticas/'.$_POST['imagen3'].'.png" alt="imagen del grafico" width="400px" height="300px">
    		</td>
    	</tr>
    </table>
    <br/>
    <br/>
    <table width="100%" border="1" style="font-family:tahoma;font-size:9;text-align:center;" width="100%" cellpadding="1px" cellspacing="0px">
		<tr style="background-color:#bdbdbd;border:solid 1px;text-align:center;">
			<th width="10%">MES</th>
			<th width="15%">EMPRESA</th>
			<th width="5%">#</th>
			<th width="5%">%</th>
			<th width="15%">POR COBRAR</th>
			<th width="5%">#</th>
			<th width="5%">%</th>
			<th width="15%">PAGADAS</th>
			<th width="5%">#</th>
			<th width="5%">%</th>
			<th width="15%">INGRESO MES</th>
		</tr>
    	';	
    	
    	$valorPorRecuperar = 0;
    	$valorRecuperado__ = 0;
        $valorEmpresa_____ = 0;
        $valorPagado______ = 0;
        
        $TotalRecuperado__ = 0;
        $TotalEmpresa_____ = 0;
        $TotalPagado______ = 0;
        
    	foreach ($month as $key => $value) {
			$empresa = getDataMonthEmpresa( $_POST['anho'], $value);
			$admnistradora = getDataMonthAdministradora( $_POST['anho'], $value);
			$valorPorRecuperar += $admnistradora; 
			$pagasdas = getDataMonthPagadasAdmin( $_POST['anho'], $value);
			$valorRecuperado__ += $pagasdas;
			$pagadasMes = getDataMonthPagadas( $_POST['anho'], $value);
			$total = $empresa + $admnistradora;
			$valorEmpresa_____ += $empresa;
            $valorPagado______ += $pagadasMes;
			$porcEmpresa = 0;
			$porcAdminis = 0;
			$porcPagadas = 0;
			$porcPagadasReal = 0;
			if($total > 0){
				$porcEmpresa = $empresa * 100 / $total;	
				$porcAdminis = $admnistradora * 100 / $total;
				$porcPagadas = $pagasdas * 100 / $admnistradora;
				$porcPagadasReal = $pagadasMes * 100 / $total;
			}
			
			$content .= "<tr>";
				$content .= "<td>".$key."</td>";
				$content .= "<td>$ ".number_format($empresa, 0, '.', '.')."</td>";
				$content .= "<td>".getDataMonthEmpresaCount( $_POST['anho'], $value)."</td>";
				$content .= "<td>".number_format($porcEmpresa, 1)."</td>";
				$content .= "<td>$ ".number_format($admnistradora, 0, '.', '.')."</td>";
				$content .= "<td>".getDataMonthAdministradoraCount( $_POST['anho'], $value)."</td>";
				$content .= "<td>".number_format($porcAdminis, 1)."</td>";
				$content .= "<td>$".number_format($pagasdas, 0, '.', '.')."</td>";
				$content .= "<td>".getDataMonthPagadasAdminCount( $_POST['anho'], $value)."</td>";
				$content .= "<td>".number_format($porcPagadas , 1)."</td>";
				$content .= "<td>$".number_format($pagadasMes, 0, '.', '.')."</td>";
				/*$content .= "<td>".getDataMonthPagadasCount( $_POST['anho'], $value)."</td>";
				$content .= "<td>".number_format($porcPagadasReal, 2)."</td>";*/
				
				$TotalRecuperado__ += getDataMonthAdministradoraCount( $_POST['anho'], $value);
                $TotalEmpresa_____ += getDataMonthEmpresaCount( $_POST['anho'], $value);
                $TotalPagado______ += getDataMonthPagadasAdminCount( $_POST['anho'], $value);
        
			$content .= "</tr>";
		}
		$content .= "<tr>";
				$content .= "<th>Total</th>";
				$content .= "<th>$ ".number_format($valorEmpresa_____, 0, '.', '.')."</th>";
				$content .= "<td>".$TotalEmpresa_____."</td>";
				$content .= "<td></td>";
				$content .= "<th>$ ".number_format($valorPorRecuperar, 0, '.', '.')."</th>";
				$content .= "<td>".$TotalRecuperado__."</td>";
				$content .= "<td></td>";
				$content .= "<th>$ ".number_format($valorRecuperado__ , 0, '.', '.')."</th>";
				$content .= "<td>".$TotalPagado______."</td>";
				$content .= "<td></td>";
				$content .= "<th>$ ".number_format($valorPagado______, 0, '.', '.')."</th>";
			$content .= "</tr>";
	$content .= '
            </table>
            <br/>
            <br/>
            <table width="100%"  border="1" style="font-family:tahoma;font-size:9;text-align:center;" width="100%" cellpadding="2px" cellspacing="0px">
            <tr>
            <td>
			<table width="100%" style="font-family:tahoma;font-size:9;text-align:center;" width="100%" cellpadding="1px" cellspacing="5px">
				<tr>
					<td width="50%" style="text-align:center;font-size:30px; color: #006191; text-decoration: underline;">
					  	$ '.number_format(($valorPorRecuperar - $valorRecuperado__), 0, '.', '.').'
					</td>
					<td width="50%" style="text-align:center;font-size:30px; color: #ff5757; text-decoration: underline;">
					   $ '.number_format($valorPagado______, 0, '.', '.').'
					</td>
				</tr>
				<tr >
			    	<td width="50%" style="text-align:center;font-size:15px; color: #006191;">
					  	VALOR POR COBRAR
					</td>
					<td width="50%" style="text-align:center;font-size:15px; color: #ff5757;">
						VALOR PAGADO
					</td>
				</tr>   
			</table>
			</td>
			</tr>
			</table>
            <br/>
            <br/>
            <table width="100%"  border="1" style="font-family:tahoma;font-size:9;text-align:center;" width="100%" cellpadding="2px" cellspacing="0px">';
			$strCampos = "SUM(inc_valor_pagado_eps) as total, ips_nombre, ips_id";
			$strTablas = "gi_incapacidad JOIN gi_ips ON ips_id = inc_ips_afiliado";
		 	$condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$_POST['anho'].'-01-01'."' AND '".$_POST['anho'].'-12-31'."' AND inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'SIN RECONOCIMIENTO' ";
			$resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos, $strTablas, $condicion, 'GROUP BY gi_incapacidad.inc_ips_afiliado', " ORDER BY total DESC"); 

			$i = 1;
			$OtroTR = '';
			$suma = 0;
			foreach ($resultado as $key => $value) {
	            if($i == 1){
	               $content .='<tr>'; 
	               $OtroTR = '<tr>';
	            }

				$strCampo = "sum(inc_valor) as total";
				$strTabla = "gi_incapacidad";
				$strWhere = " inc_estado_tramite = 'PAGADA' AND inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$_POST['anho'].'-01-01'."' AND '".$_POST['anho'].'-12-31'."' AND inc_ips_afiliado = ".$value['ips_id'];
				$arrRespuestaPS = ModeloTesoreria::mdlMostrarUnitario($strCampo, $strTabla, $strWhere);
				$intTotalRecuperado__ = $arrRespuestaPS['total'];

				$suma += ($value['total'] - $intTotalRecuperado__ );
				$content .= '<td style="text-align:center;font-size:7px;">$ '.number_format(($value['total'] - $intTotalRecuperado__), 0, '.','.').'</td>';
			    $OtroTR .= '<td style="text-align:center;font-size:6px; color: #006191;"><b>'.mb_strtoupper($value['ips_nombre']).'</b></td>';
				 if($i == 6){
	               $content .='</tr>'; 
	               $OtroTR .= '</tr>';
	               $content .= $OtroTR;
	               $i=1;
	             }else{
	                $i++;
	             }
			}
			
	
			if($i != 1 && $i <= 6){
			    $respuesta = 7 -$i;
			    for($in = 0; $in < $respuesta; $in++){
			        $content .= '<td style="text-align:center;font-size:7px;"></td>';
			        $OtroTR .= '<td style="text-align:center;font-size:6px; color: #006191;"></td>';
			    }
			    $content .='</tr>'; 
                $OtroTR .= '</tr>';
                $content .= $OtroTR;
			}
			$content .= '</table>';

	function  getDataMonthEmpresa($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'SUM(inc_valor_pagado_empresa) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_valor_pagado_empresa IS NOT NULL  ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthAdministradora($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'SUM(inc_valor_pagado_eps) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'SIN RECONOCIMIENTO'  ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthPagadasAdmin($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'SUM(inc_valor) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthPagadas($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'SUM(inc_valor) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA'  ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	/*Contar las incapacidades*/
	function  getDataMonthEmpresaCount($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'COUNT(*) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_valor_pagado_empresa IS NOT NULL AND inc_valor_pagado_empresa != '' ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthAdministradoraCount($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'COUNT(inc_valor_pagado_eps) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != 'EMPRESA' AND inc_estado_tramite != 'SIN RECONOCIMIENTO' ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthPagadasAdminCount($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'COUNT(inc_valor) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}

	function  getDataMonthPagadasCount($year2, $mes){
		require_once '../../../modelos/tesoreria.modelo.php';
		$year = $year2.'-'.$mes.'-01';
   	 	$otherYear = $year2.'-'.$mes.'-31';

	 	$campos = 'COUNT(inc_valor) as total';
        $tabla  = 'gi_incapacidad';
        $condicion = " inc_empresa = ".$_SESSION['cliente_id']." AND  inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA'  ";
        $incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        $valor = 0;
        if($incapacidades['total'] != null && $incapacidades['total'] != ''){
        	$valor = $incapacidades['total'];
        }
        return $valor;
	}


	 class MYPDF extends TCPDF {

        protected $processId = 0;
        protected $header = '';
        protected $footer = '';
        static $errorMsg = '';
        //Page header
        public function Header() {
            $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
           $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

           $this->Line(5, 5, $this->getPageWidth()-5, 5); 

           $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
           $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
           $this->Line(5, 5, 5, $this->getPageHeight()-5);

        }

        // Page footer
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            // Set font
            $this->SetFont('helvetica', 'N', 8);
            // Page number
            $this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
    }

    //echo $content;
   
    /*empezar el proceso del PDF*/
    $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("Reporte Detalle de Pagos x Mes");
    //$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    // set default header data

    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(true);
    $obj_pdf->setPrintFooter(true);
    $obj_pdf->SetAutoPageBreak(TRUE, 20);
    $obj_pdf->SetFont('times', '', 15);
    // set image scale factor

    $obj_pdf->AddPage('A4');

    $obj_pdf->writeHTML($content, true, false, false, false, '');

    $nombre='Reporte_Estado_Empresa_'.date("d-m-Y H-i-s").'.pdf';
    //header('Content-type: application/pdf');
    $obj_pdf->Output($nombre, 'I'); 

