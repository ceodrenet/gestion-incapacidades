<?php
    session_start();
    require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php'; 
    require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../controladores/tesoreria.controlador.php';
    
    require_once '../../../modelos/dao.modelo.php';
    require_once '../../../modelos/incapacidades.modelo.php';
    require_once '../../../modelos/tesoreria.modelo.php';

?>
<script src="vistas/assets/assets/libs/chart.js/Chart.bundle.min.js"></script>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <div class="col-lg-6 col-xs-12">
            <!-- small box -->
                <div class="card card -h-100">
                    <div class="inner">
                        <h3>
                            <?php
                                $item = null;
                                $valor = null;
                                if($_SESSION['cliente_id'] != 0){
                                    $item = 'inc_empresa';
                                    $valor = $_SESSION['cliente_id'];
                                    $pagadas = ControladorTesoreria::ctrVerReportesTesoreria($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal'], 'mostrarGeneralPagos_byCliente');
                                }else{
                                    $pagadas = ControladorTesoreria::ctrVerReportesTesoreria($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal'], 'mostrarGeneralPagos');
                                }

                                echo count($pagadas);
                            ?>
                        </h3>
                        <span class="text-muted mb-3 1h-1 d-block text-truncate">PAGADAS</span>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-done-all"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-12">
                <!-- small box -->
                <div class="card card -h-100">
                    <div class="inner">
                        <h3>
                            <?php
                                $item = null;
                                $valor = null;
                                if($_SESSION['cliente_id'] != 0){
                                    $item = 'inc_empresa';
                                    $valor = $_SESSION['cliente_id']; 
                                }

                                $pagadas = ControladorTesoreria::ctrVerReportesTesoreria($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal'], 'totalValorIncapacidades');
                                echo number_format($pagadas['total'], 0, ',', '.');
                            ?>
                        </h3>
                        <p>VALOR PAGADO</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-cash"></i>
                    </div>
                </div>
            </div>
        </div>  
    </div>
    <div class="col-md-7">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Grafico de pagos por EPS</h3>
            </div>
            <div class="box-body">
                <canvas id="myChart" style="height:250px"></canvas>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    REGISTRO DE PAGOS
                </h5>
            </div>
            <div class="card-body">
            <table class="table table-hover table-condensed display nowrap" id="tblGeneralPagos" style="width: 100%;">
                    <thead>
                        <tr>
                            </th>
                            <?php 
                                if($_SESSION['cliente_id'] == 0){ 
                                    echo '<th style="width:15%;">EMPRESA</th>';
                                }
                            ?>
                            <th>CÉDULA</th>
                            <th>FECHA DE INICIO</th>
                            <th>VALOR PAGADO</th>
                            <th>FECHA DE PAGO</th>
                            <th>ADMINISTRADORA</th>
                            <th>F. CONCILIACIÓN</th>
                            <th>F. FACTURACIÓN</th>
                            <th>COMISIÓN</th>
                            <th># FACTURA</th>
                            <th>F. PAGO FACTURA</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sitienenDatos = 0;
                            $numberFactura = '';
                            $fechaFactura = '';
                            $fechaPagoFactu = '';
                            $inc_fecha_fecturacion_d = 0;
                            $inc_fecha_consiliacion_d = 0;
                            $condicion = 0;
                            $item = null;
                            $valor = null;
                            if($_SESSION['cliente_id'] != 0){
                                $item = 'inc_empresa';
                                $valor = $_SESSION['cliente_id'];
                                $pagadas = ControladorTesoreria::ctrVerReportesTesoreria($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal'], 'mostrarGeneralPagos_byCliente');
                            }else{
                                $pagadas = ControladorTesoreria::ctrVerReportesTesoreria($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal'], 'mostrarGeneralPagos');
                            }

                            foreach ($pagadas as $key => $value) {
                                echo '<tr>';
                                    echo '<td>'.$value['inc_cc_afiliado'].'</td>';
                                    $newDate = date("d/m/Y", strtotime($value['inc_fecha_inicio']));
                                    echo '<td>'.$newDate.'</td>';
                                    if(!empty($value['inc_valor']) && $value['inc_valor'] != ''){
                                        echo '<td>$ '.number_format($value['inc_valor'], 0, ',', '.').'</td>';
                                    }else{
                                        echo '<td></td>';
                                    }
                                    $newDate = date("d/m/Y", strtotime($value['inc_fecha_pago']));
                                    echo '<td>'.$newDate.'</td>';

                                    if($_SESSION['cliente_id'] == 0){ 
                                        echo '<td>'.$value['emp_nombre'].'</td>';
                                    }
                                    if($value['inc_clasificacion'] != 4){
                                        if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                                            echo '<td class="text-uppercase">'.($value["arl_nombre"]).'</td>'; 
                                        }else{
                                            echo '<td class="text-uppercase">'.($value["ips_nombre"]).'</td>'; 
                                        }
                                    }else{
                                        echo '<td class="text-uppercase">'.($value["afp_nombre"]).'</td>';    
                                    }
                                    if($value['inc_fecha_consiliacion_d'] != null && $value['inc_fecha_consiliacion_d'] != '0000-00-00'){
                                        $newDate = date("d/m/Y", strtotime($value['inc_fecha_consiliacion_d']));
                                        echo '<td>'.$newDate.'</td>';
                                        $fechaPagoFactu = $value['inc_fecha_consiliacion_d'];
                                    }else{
                                        echo '<td></td>';
                                    }
                                    if($value['inc_fecha_fecturacion_d'] != null && $value['inc_fecha_fecturacion_d'] != '0000-00-00'){
                                        $newDate = date("d/m/Y", strtotime($value['inc_fecha_fecturacion_d']));
                                        echo '<td>'.$newDate.'</td>';
                                        $fechaPagoFactu = $value['inc_fecha_fecturacion_d'];
                                    }else{
                                        echo '<td></td>';
                                    }

                                    echo '<td>'.$value['inc_condicion_id_i'] .'</td>';
                               
                                    echo '<td>'.$value['inc_numero_factura'].'</td>';
                                    if($value['inc_fecha_pago_factura'] != null && $value['inc_fecha_pago_factura'] != '0000-00-00'){
                                        $newDate = date("d/m/Y", strtotime($value['inc_fecha_pago_factura']));
                                        echo '<td>'.$newDate.'</td>';
                                        $fechaPagoFactu = $value['inc_fecha_pago_factura'];
                                    }else{
                                        echo '<td></td>';
                                    }
                                    
                                echo '</tr>';
                                $sitienenDatos = 1;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
   


<script src="vistas/assets/assets/libs/chart.js/chart.min.js"></script>

<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript">
    $(function(){
        crearBarrasporPagar();

        var table = $('#tblGeneralPagos').DataTable({
            columnDefs: [{
                orderable: false,
                targets: 0
            }],
            order: [
                [1, 'asc']
            ],
            "lengthMenu": [
                [10, 25, 50, 100, 200, -1], 
                [10, 25, 50, 100, 200, "All"]
            ],
            "language" : {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "scrollX": true
        });


    });

function crearBarrasporPagar(){
    <?php
        /*Obtenemos las afp porque son por clasificacion +180 */        
        $campo = 'SUM(inc_valor) as total, afp_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = 'inc_clasificacion = 4 ';
        if($_SESSION['cliente_id'] != 0){
            $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }else{
            $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY arl_nombre', '');
        
        $label = '';
        $Qdias = '';
        $colors = '';
        foreach ($respuesta as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */
            if($value['afp_nombre'] != null){
                $label .= "\"".$value['afp_nombre']."\" , ";
                $Qdias .= $value['total']." , ";
                $colors .= "getRandomColor(),";
            }
            
        }

        /*Obtenemos las ARL porque son por accidente de transito*/
        $campo = 'SUM(inc_valor) as total, arl_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = "inc_clasificacion != 4 AND inc_origen = 'ACCIDENTE LABORAL' ";
        if($_SESSION['cliente_id'] != 0){
            $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }else{
            $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $respuesta2 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY arl_nombre', '');
        foreach ($respuesta2 as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */
            if($value['arl_nombre'] != null){
                $label .= "\"".$value['arl_nombre']."\" , ";
                $Qdias .= $value['total']." , ";
                $colors .= "getRandomColor(),";
            }
            
        }

        /*Obtenemos las EPS porque son por accidente de transito*/
        $campo = 'SUM(inc_valor) as total, ips_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = "inc_clasificacion != 4 AND inc_origen != 'ACCIDENTE LABORAL' ";
        if($_SESSION['cliente_id'] != 0){
            $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' ";
        }else{
            $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $respuesta3 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY ips_nombre', '');
        foreach ($respuesta3 as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */
            if($value['ips_nombre'] != null){
                $label .= "\"".$value['ips_nombre']."\" , ";
                $Qdias .= $value['total']." , ";
                $colors .= "getRandomColor(),";
            }
            
        }


    ?>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php echo $label; ?>],
            datasets: [
                {
                    label: '',
                    data: [<?php echo $Qdias; ?>],
                    backgroundColor: [<?php echo $colors; ?>]
                }
            ]
    },
        options: {
            tooltips: {
                callbacks: {
                   label: function(tooltipItem, data) {
                       return "$" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                       });
                    }
                }
            }
        }
    });
}

function scaleLabel (valuePayload) {
    return Number(valuePayload.value).toFixed(2).replace('.',',') + '$';
}

function getRandomColor() {
   return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
}
</script>


