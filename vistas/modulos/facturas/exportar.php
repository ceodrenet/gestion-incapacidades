<?php
session_start();
/** Error reporting */
ini_set('display_errors', 'On');
ini_set('display_errors', 1);
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** PHPExcel */
require_once dirname(__FILE__) . '/../../../extenciones/Excel.php';
require_once dirname(__FILE__) . '/../../../controladores/mail.controlador.php';
require_once dirname(__FILE__) . '/../../../controladores/plantilla.controlador.php';
require_once dirname(__FILE__) . '/../../../controladores/incapacidades.controlador.php';
require_once dirname(__FILE__) . '/../../../controladores/tesoreria.controlador.php';

require_once dirname(__FILE__) . '/../../../modelos/dao.modelo.php';
require_once dirname(__FILE__) . '/../../../modelos/incapacidades.modelo.php';
require_once dirname(__FILE__) . '/../../../modelos/tesoreria.modelo.php';


$objPHPExcel = new PHPExcel();

$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
$writer->setIncludeCharts(TRUE);

$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getProperties()->setCreator("GEIN")
                     ->setLastModifiedBy("".$_SESSION['nombres'])
                     ->setTitle("GEIN - Facturas")
                     ->setSubject("Facturacion")
                     ->setDescription("Descarga la facturacion, registrada en el sistema")
                     ->setKeywords("office 2007 openxml php")
                     ->setCategory("Facturas");

$objPHPExcel->getActiveSheet()
    ->getStyle('A1:U1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold( true );


$objPHPExcel->getActiveSheet()->setTitle('Facturacion');


$objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
$objPHPExcel->getActiveSheet()->setCellValue("B1", "Empresa"); 
$objPHPExcel->getActiveSheet()->setCellValue("C1", "Administradora"); 
$objPHPExcel->getActiveSheet()->setCellValue("D1", "Valor Pagado"); 
$objPHPExcel->getActiveSheet()->setCellValue("E1", "Fecha de Pago"); 
$objPHPExcel->getActiveSheet()->setCellValue("F1", "# Factura"); 
$objPHPExcel->getActiveSheet()->setCellValue("G1", "Fecha de radicacion"); 
$objPHPExcel->getActiveSheet()->setCellValue("H1", "Fecha de pago factura");

$sitienenDatos = 0;
$numberFactura = '';
$fechaFactura = '';
$fechaPagoFactu = '';
$item = null;
$valor = null;
if($_SESSION['cliente_id'] != 0){
    $item = 'inc_empresa';
    $valor = $_SESSION['cliente_id'];
    $pagadas = ControladorTesoreria::ctrMostrarGeneralPagos_byCliente($item, $valor, $_POST['NuevoFechaInicio'], $_POST['NuevoFechaFinal']);
}else{
    $pagadas = ControladorTesoreria::ctrMostrarGeneralPagos($item, $valor, $_POST['NuevoFechaInicio'], $_POST['NuevoFechaFinal']);
} 


$i = 2;
foreach ($pagadas as $key => $value) {
    

    $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1)); 
    $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value['emp_nombre']); 
    $admin ='';
    if($value['inc_clasificacion'] != 4){
        if($value['inc_origen'] == 'ACCIDENTE LABORAL'){

           $admin = $value["arl_nombre"]; 
        
        }else{
           
           $admin = $value["ips_nombre"]; 
        
        }
    
    }else{
       	
       	$admin = $value["afp_nombre"];    
    
    }
    
    $valor = 0;

    if(!empty($value['inc_valor']) && $value['inc_valor'] != ''){
        $valor = $value['inc_valor'];
    }

    $fecha = '';
    if($value['inc_fecha_emision_factura'] != null && $value['inc_fecha_emision_factura'] != '0000-00-00'){
       $fecha = $value['inc_fecha_emision_factura'];
    }

    $fechaPagoFactu = '';
	if($value['inc_fecha_pago_factura'] != null && $value['inc_fecha_pago_factura'] != '0000-00-00'){
        $fechaPagoFactu = $value['inc_fecha_pago_factura'];
	}

    $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $admin);
    $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $valor); 
    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["inc_fecha_pago"]); 
    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["inc_numero_factura"]); 
    $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $fecha); 
    $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $fechaPagoFactu); 
    $i++;         
}

foreach(range('A','H') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}


$newHoja = $objPHPExcel->createSheet(1);
$newHoja->setTitle("Estadisticas");
$newHoja->getStyle('A1:B1')
        ->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$newHoja->setCellValue("A1", "ADMINISTRADORA"); 
$newHoja->setCellValue("B1", "VALOR PAGADO"); 

$i = 2;

/*Obtenemos las afp porque son por clasificacion +180 */        
$campo = 'SUM(inc_valor) as total, afp_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
$tabla = 'reporte_tesoreria';
$condicion = 'inc_clasificacion = 4 ';
if($_SESSION['cliente_id'] != 0){
    $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['NuevoFechaInicio']."' AND '".$_POST['NuevoFechaFinal']."'";
}else{
    $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['NuevoFechaInicio']."' AND '".$_POST['NuevoFechaFinal']."'";
}

$respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campo, $tabla, $condicion, 'GROUP BY arl_nombre', '');

$total = 0;
foreach ($respuesta as $key => $value) {
    /* primero traemos las empresas que vamos a mostrar osea todas */
    /* traemos las de 15 dias */
    if($value['afp_nombre'] != null){
        $newHoja->setCellValue("A".$i, $value['afp_nombre']);
    	$newHoja->setCellValue("B".$i, $value['total']); 
    	$total += $value['total'];
    	$i++;
    }
    
}

/*Obtenemos las ARL porque son por accidente de transito*/
$campo = 'SUM(inc_valor) as total, arl_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
$tabla = 'reporte_tesoreria';
$condicion = "inc_clasificacion != 4 AND inc_origen = 'ACCIDENTE LABORAL' ";
if($_SESSION['cliente_id'] != 0){
    $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['NuevoFechaInicio']."' AND '".$_POST['NuevoFechaFinal']."'";
}else{
    $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['NuevoFechaInicio']."' AND '".$_POST['NuevoFechaFinal']."'";
}

$respuesta2 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY arl_nombre', '');
foreach ($respuesta2 as $key => $value) {
    /* primero traemos las empresas que vamos a mostrar osea todas */
    /* traemos las de 15 dias */
    if($value['arl_nombre'] != null){
        $newHoja->setCellValue("A".$i, $value['arl_nombre']);
    	$newHoja->setCellValue("B".$i, $value['total']); 
    	$total += $value['total'];
    	$i++;
    }
    
}

/*Obtenemos las EPS porque son por accidente de transito*/
$campo = 'SUM(inc_valor) as total, ips_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
$tabla = 'reporte_tesoreria';
$condicion = "inc_clasificacion != 4 AND inc_origen != 'ACCIDENTE LABORAL' ";
if($_SESSION['cliente_id'] != 0){
    $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['NuevoFechaInicio']."' AND '".$_POST['NuevoFechaFinal']."' ";
}else{
    $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['NuevoFechaInicio']."' AND '".$_POST['NuevoFechaFinal']."'";
}

$respuesta3 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY ips_nombre', '');
foreach ($respuesta3 as $key => $value) {
    /* primero traemos las empresas que vamos a mostrar osea todas */
    /* traemos las de 15 dias */
    if($value['ips_nombre'] != null){
        $newHoja->setCellValue("A".$i, $value['ips_nombre']);
    	$newHoja->setCellValue("B".$i, $value['total']); 
    	$total += $value['total'];
    	$i++;
    }
}

$newHoja->setCellValue("A".$i, 'TOTAL PAGADO');
$newHoja->setCellValue("B".$i, $total); 

foreach(range('A','B') as $columnID) {
    $newHoja->getColumnDimension($columnID)
        ->setAutoSize(true);
}

$newHoja->getStyle('B2:B'.$i)->getNumberFormat()->setFormatCode('#,##0.00');


$dataSeriesLabels = array(
	new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$1', NULL, 1)	//	Administradora
);
$xAxisTickValues = array(
	new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$2:$A$'.($i-1), NULL, ($i -1 ))	//	A1 to AX
);

$dataSeriesValues = array(
	new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$B$2:$B$'.($i-1), NULL, ($i - 1)) //Valores Pagados
);


$series = new PHPExcel_Chart_DataSeries(
	PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
	PHPExcel_Chart_DataSeries::GROUPING_STANDARD,	// plotGrouping
	range(0, count($dataSeriesValues)-1),									// plotOrder
	$dataSeriesLabels,								// plotLabel
	$xAxisTickValues,								// plotCategory
	$dataSeriesValues								// plotValues
);

$series->setPlotDirection(PHPExcel_Chart_DataSeries::DIRECTION_COL);
//	Set the series in the plot area
$plotArea = new PHPExcel_Chart_PlotArea(NULL, array($series));
//	Set the chart legend
$legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, true);
$title = new PHPExcel_Chart_Title('Grafica Pagos por Administradora');
$yAxisLabel = new PHPExcel_Chart_Title('Value ($k)');
//	Create the chart
$chart = new PHPExcel_Chart(
	'chart1',		// name
	$title,			// title
	$legend,		// legend
	$plotArea,		// plotArea
	true,			// plotVisibleOnly
	0,				// displayBlanksAs
	NULL,			// xAxisLabel
	$yAxisLabel		// yAxisLabel
);
//	Set the position where the chart should appear in the worksheet
$chart->setTopLeftPosition('A'.($i+2));
$chart->setBottomRightPosition('H'.($i+25));
//	Add the chart to the worksheet
$newHoja->addChart($chart);


//
$newHoja2 = $objPHPExcel->createSheet(2);
$newHoja2->setTitle("Facturas");
$newHoja2->getStyle('A1:E1')
        ->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$newHoja2->setCellValue("A1", "#"); 
$newHoja2->setCellValue("B1", "No. FACTURA"); 
$newHoja2->setCellValue("C1", "VALOR"); 
$newHoja2->setCellValue("D1", "FECHA RADICACION"); 
$newHoja2->setCellValue("E1", "FECHA PAGO"); 


$i = 2;
if($_SESSION['cliente_id'] != 0){
    
    $campo = "SUM(inc_valor) as total, inc_fecha_emision_factura, inc_fecha_pago_factura, inc_numero_factura";
    $tabla = 'reporte_tesoreria';
    $condicion = 'inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_emision_factura BETWEEN '".$_POST['NuevoFechaInicio']."' AND '".$_POST['NuevoFechaFinal']."' ";

    $pagadasx = ModeloTesoreria::mdlMostrarGroupAndOrder($campo, $tabla, $condicion, 'GROUP BY inc_numero_factura', 'ORDER BY inc_numero_factura ASC');
    //= ControladorTesoreria::ctrMostrarGeneralPagos_byClienteFactura($item, $valor, $_POST['NuevoFechaInicio'], $_POST['NuevoFechaFinal']);
}else{
    $campo = "SUM(inc_valor) as total, inc_fecha_emision_factura, inc_fecha_pago_factura, inc_numero_factura";
    $tabla = 'reporte_tesoreria';
    $condicion = "inc_fecha_emision_factura BETWEEN '".$_POST['NuevoFechaInicio']."' AND '".$_POST['NuevoFechaFinal']."' ";
    $pagadasx = ModeloTesoreria::mdlMostrarGroupAndOrder($campo, $tabla, $condicion, 'GROUP BY inc_numero_factura', 'ORDER BY inc_numero_factura ASC');
} 

$i = 2;
foreach ($pagadasx as $key => $value) {

    $newHoja2->setCellValue("A".$i, ($key+1)); 
    $newHoja2->setCellValue("B".$i, $value['inc_numero_factura']); 
    $newHoja2->setCellValue("C".$i, $value['total']); 
    $newHoja2->setCellValue("D".$i, $value['inc_fecha_emision_factura']); 
    $newHoja2->setCellValue("E".$i, $value['inc_fecha_pago_factura']); 
    $i++;
}

foreach(range('A','E') as $columnID) {
    $newHoja2->getColumnDimension($columnID)
        ->setAutoSize(true);
}

$newHoja2->getStyle('C2:C'.$i)->getNumberFormat()->setFormatCode('#,##0.00');

$objPHPExcel->setActiveSheetIndex(2);




// Write the Excel file to filename some_excel_file.xlsx in the current directory
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="facturacion'.date('Y-m-d-H-i-s').'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$writer->save('php://output');

?>