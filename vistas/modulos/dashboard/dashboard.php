<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- start page title -->
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Bienvenido/a <?php echo mb_strtoupper($_SESSION['nombres']); ?></h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active"><?php echo ModeloIncapacidades::getDatos('gi_empresa', 'emp_id', $_SESSION['cliente_id'])['emp_nombre']; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<?php 
    /*--JGM--- ZOna de datos para comparacion*/
    $total_entrante = ControladorIncapacidades::getTotalValorEntranteAnual($_SESSION['anho']);
    $num_Pagadas = ControladorIncapacidades::getSumIncapacidades($_SESSION['anho'], 5);
    $num_Negadas = ControladorIncapacidades::getSumIncapacidades($_SESSION['anho'], 6);
    $num_Negadas += ControladorIncapacidades::getSumIncapacidades($_SESSION['anho'], 7);
    $num_Pendientes = ControladorIncapacidades::getSumIncapacidades($_SESSION['anho'], 2);
    $num_Pendientes += ControladorIncapacidades::getSumIncapacidades($_SESSION['anho'], 3);
    $num_Pendientes += ControladorIncapacidades::getSumIncapacidades($_SESSION['anho'], 4);
    $num_Total = ControladorIncapacidades::getSumIncapacidades($_SESSION['anho'], null);
    $porcentajeRecuperado = 0;
    $porcentajeRecuperadoGeneradas = 0;
    $porcentajeNegadas = 0;
    $porcentajePendientes = 0;

    if($num_Total > 0){
        $porcentajeRecuperado = number_format((($num_Pagadas * 100 )/$num_Total), 0);
        $porcentajeRecuperadoGeneradas = number_format((($total_entrante* 100)/$num_Total ), 0);
        $porcentajeNegadas = number_format((($num_Negadas * 100 )/$num_Total), 0);
        $porcentajePendientes = number_format((($num_Pendientes * 100 )/$num_Total), 0);
    }
    
    /*Datos Comparativos*/
    $datosCom = ControladorIncapacidades::getData('gi_indicadores_semanales', 'ind_emp_id_i', $_SESSION['cliente_id']);
    $rest_Pagadas = 0;
    $rest_Negadas = 0;
    $rest_Pendientes = 0;
    $rest_Total = 0;

    $porcentajePagadas = 0;
    $restaPagadas = 0;
    if($datosCom != null && $datosCom != false && $datosCom['ind_pag_i'] != 0){
        //Porque si esto es null, se daña todo
        if($num_Pagadas > $datosCom['ind_pag_i']){
            //El recaudo Subio
            $restaPagadas = $num_Pagadas - $datosCom['ind_pag_i'];
            $porcentajePagadas = ($restaPagadas * 100) / $datosCom['ind_pag_i'];

        }elseif ($num_Pagadas < $datosCom['ind_pag_i']) {
            //El recaudo bajo
            $restaPagadas = $datosCom['ind_pag_i'] - $num_Pagadas ;
            if($num_Pagadas > 0){
               $porcentajePagadas = ($restaPagadas * 100) / $num_Pagadas; 
            }
            
        }
    }

    /*ZONA DE GRAFICOS*/
    /*GRAFICO UNO*/
    $year = $_SESSION['anho'].'-01-01';
    $otherYear = $_SESSION['anho'].'-12-31';

    $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $monthNumber = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
    $options = "<option value='0'>Total</option>";

    $whereCliente = '';
    if($_SESSION['cliente_id'] != 0){
        $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
    }

    $campos = 'SUM(inc_valor_pagado_eps) as total , MONTH(inc_fecha_inicio) as mes';
    $tabla  = 'gi_incapacidad';
    $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != '1'  ";
    $incapacidades = ControladorPlantilla::getDataFromLsql($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');

    $nombres = '';
    $valores = '';
    $valoreP = '';
    $i = 0;

    foreach ($incapacidades as $key => $value) {
        $numero = $value['mes'];
        if($i == 0){
            $nombres .= "'".$month[$numero -1]."'";
            $valores .= "'".$value['total']."'"; 
        }else{
            $nombres .= " , '".$month[$numero -1]."'";
            $valores .= " , '".$value['total']."'"; 
        }
        $options .= "<option value='".$monthNumber[$numero -1]."'>".$month[$numero -1]."</option>";
        $i++;
    }

    $campos = 'SUM(inc_valor) as total , MONTH(inc_fecha_inicio) as mes';
    $tabla  = 'gi_incapacidad';
    $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = '5' ";
    $incapacidades = ControladorPlantilla::getDataFromLsql($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');
    $i = 0;
    foreach ($incapacidades as $key => $value) {
        
        if($i == 0){
            $valoreP .= "'".$value['total']."'"; 
        }else{
            $valoreP .= ", '".$value['total']."'"; 
        }
        $i++;
    }

    /*GRAFICO DOS*/
    $item = null;
    $valor = null;

    if($_SESSION['cliente_id'] != 0){
        $item = 'inc_empresa';
        $valor = $_SESSION['cliente_id'];
    }

    $respuesta = ControladorIncapacidades::ctrMostrarLastSixMonth($item, $valor, $_SESSION['anho']);
    $arrayMont = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

    $labels = '';
    $datos = '';
    $colors = '';

    foreach ($respuesta as $key => $value) {

        if( $value['valor'] != '' &&  $value['valor'] != null){
            if( $labels == ''){
                $labels = '"'.$arrayMont[$value['MES']-1].'"';
                $datos   = $value['valor'];
                $colors .= "getRandomColor()";
            }else{
                $labels .= ' , "'.$arrayMont[$value['MES']-1].'"';
                $datos   .= " , ".$value['valor'];
                $colors .= ",getRandomColor()";
            }
        }
        
    }

    /*GRAFICO CUATRO*/
    $year = $_SESSION['anho'].'-01-01';
    $otherYear = $_SESSION['anho'].'-12-31';

    $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

    $whereCliente = '';
    if($_SESSION['cliente_id'] != 0){
        $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
    }

    $campos = 'SUM(inc_valor_pagado_eps) as total , MONTH(inc_fecha_inicio) as mes';
    $tabla  = 'gi_incapacidad';
    $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != '1'  ";
    $incapacidades = ControladorPlantilla::getDataFromLsql($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');

    $nombres = '';
    $valores = '';
    $valoreP = '';
    $i = 0;
    $valorMaximoX = 0;

    foreach ($incapacidades as $key => $value) {
        $numero = $value['mes'];
        if($i == 0){
            $nombres .= "'".$month[$numero -1]."'";
            $valores .= "'".$value['total']."'"; 
        }else{
            $nombres .= " , '".$month[$numero -1]."'";
            $valores .= " , '".$value['total']."'"; 
        }
        if($valorMaximoX < $value['total']){
            $valorMaximoX = $value['total'];
        }
        $i++;
    }

    $campos = 'SUM(inc_valor) as total , MONTH(inc_fecha_inicio) as mes';
    $tabla  = 'gi_incapacidad';
    $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = '5' ";
    $incapacidades = ControladorPlantilla::getDataFromLsql($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');
    $i = 0;
    foreach ($incapacidades as $key => $value) {
        
        if($i == 0){
            $valoreP .= "'".$value['total']."'"; 
        }else{
            $valoreP .= ", '".$value['total']."'"; 
        }
        $i++;
    }


    $item = null;
    $valor = null;

    if($_SESSION['cliente_id'] != 0){
        $item = 'inc_empresa';
        $valor = $_SESSION['cliente_id'];
    }

    $respuesta = ControladorIncapacidades::ctrMostrarLastSixMonth($item, $valor, $_SESSION['anho']);
    $arrayMont = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

    $labels = '';
    $datos = '';
    $colors = '';
    $valorMaximoY = 0;
    foreach ($respuesta as $key => $value) {

        if( $value['valor'] != '' &&  $value['valor'] != null){
            if( $labels == ''){
                $labels = '"'.$arrayMont[$value['MES']-1].'"';
                $datos   = $value['valor'];
                $colors .= "getRandomColor()";
            }else{
                $labels .= ' , "'.$arrayMont[$value['MES']-1].'"';
                $datos   .= " , ".$value['valor'];
                $colors .= ",getRandomColor()";
            }

            if($valorMaximoY < $value['valor']){
                $valorMaximoY = $value['valor'];
            }
        }
        
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger d-flex flex-wrap align-items-center mb-4">
                <h5 class="my-0 text-danger card-title me-2">
                    INCAPACIDADES NOMINA VS PAGOS RECIBIDOS - AÑO <?php echo $_SESSION['anho'];?>
                </h5>
                <div class="ms-auto">
                    <select class="form-control" style="width: 100%;" id="selAnhos">
                        <?php
                            $i = 0;
                            while ($i < 7) {
                                $anho = (date('Y') - $i);
                                if($_SESSION['anho'] == $anho){
                                    echo "<option selected value='".(date('Y') - $i)."'>".(date('Y') - $i)."</option>";
                                }else{
                                    echo "<option value='".(date('Y') - $i)."'>".(date('Y') - $i)."</option>";
                                }    
                                $i++;

                            }
                        ?>
                    </select>      
                </div>
            
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="pieChart_4" data-colors='["#ff6384", "#2ab57d", "#36a2eb"]' class="e-charts"></div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th colspan="3" style="text-align:center;">ESTADISTICAS AÑO ACTUAL <?php echo $_SESSION['anho'];?></th>
                </tr>
                <tr>
                    <th>INCAPACIDADES</th>
                    <th>VALOR</th>
                    <th>%</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>PAGADAS</td>
                    <td>$ <?php echo number_format($num_Pagadas, 0, ',', '.');?></td>
                    <th><?php echo number_format($porcentajeRecuperado, 0, ',', '.');?>%</th>
                </tr>
                <tr>
                    <td>NEGADAS</td>
                    <td>$ <?php echo number_format($num_Negadas, 0, ',', '.');?></td>
                    <th><?php echo number_format($porcentajeNegadas, 0, ',', '.');?>%</th>
                </tr>
                <tr>
                    <td>PENDIENTES</td>
                    <td>$ <?php echo number_format($num_Pendientes, 0, ',', '.');?></td>
                    <th><?php echo number_format($porcentajePendientes, 0, ',', '.');?>%</th>
                </tr>
                <tr>
                    <td>TOTAL GENERADAS</td>
                    <td>$ <?php echo number_format($num_Total, 0, ',', '.');?></td>
                    <th>100%</th>
                </tr>
            </tbody>
        </table>
        
        <div class="row align-items-center">
            <div class="col-sm">
                <div id="invested-overview" data-colors='["#36a2eb", "#ff6384"]' class="apex-charts"></div>
            </div>
            <div class="col-sm align-self-center">
                <div class="mt-4 mt-sm-0">
                    <p class="mb-1 text-muted text-uppercase font-size-11">TOTAL GENERADAS</p>
                    <h4>$ <?php echo number_format($num_Total, 0, ',', '.');?></h4>

                    <!--<p class="text-muted mb-4"> + <?php //echo number_format($restaPagadas, 0, ',', '.');?> ( <?php //echo number_format($porcentajePagadas, 2);?> % ) <i class="mdi mdi-arrow-up ms-1 text-success"></i></p>-->

                    <div class="row g-0">
                        <div class="col-12">
                            <div>
                                <p class="mb-2 text-muted text-uppercase font-size-11">TOTAL INGRESOS</p>
                                <h5 class="fw-medium">$ <?php echo number_format($total_entrante, 0, ',', '.');?></h5>
                            </div>
                        </div>
                    </div>

                    <div class="mt-2">
                        <a href="incapacidades" class="btn btn-primary btn-sm">Ver Mas <i class="mdi mdi-arrow-right ms-1"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card card-h-100 border border-danger">
            <!-- card body -->
            <div class="card-body">
                <div class="d-flex flex-wrap align-items-center mb-4">
                    <h5 class="card-title me-2 text-danger">PAGOS POR ENTIDADES</h5>
                    <div class="ms-auto">
                        <select class="form-select form-select-sm" id="selectChangeMonths">
                           <?php echo $options;?>
                        </select>
                    </div>
                </div>

                <div class="row align-items-center" id="entidadesGrafico">
                    
                </div>
            </div>
        </div>
    </div>
</div><!-- end row-->
<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>
<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="vistas/assets/assets/libs/chart.js/Chart.bundle.min.js"></script>
<script src="vistas/assets/assets/libs/apexcharts/apexcharts.min.js"></script>
<script src="vistas/assets/assets/libs/echarts/echarts.min.js"></script>
<script type="text/javascript">
    $(function(){
        generateGraficoCuatro();
        generateGraficoDos();
        generateGraficoCinco();
        $("#selectChangeMonths").on('change', function(){
            generateGraficoCinco();
        });
    });

    // get colors array from the string
    function getChartColorsArray(chartId) {
        var colors = $(chartId).attr('data-colors');
        var colors = JSON.parse(colors);
        return colors.map(function(value){
            var newValue = value.replace(' ', '');
            if(newValue.indexOf('--') != -1) {
                var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
                if(color) return color;
            } else {
                return newValue;
            }
        })
    }

    function generateGraficoUno(){

        var densityCanvas = document.getElementById("pieChart_4").getContext('2d');

        var densityData = {
            label: 'Pagadas por Nomina',
            data: [<?php echo $valores;?>],
            borderColor:'#ff6384',
            backgroundColor : '#ff6384',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var densityData_pagadas = {
            label: 'Pagadas por Administradora',
            data: [<?php echo $valoreP; ?>],
            borderColor:'#36a2eb',
            backgroundColor : '#36a2eb',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var barChart = new Chart(densityCanvas, {
            type: 'bar',
            data: {
                labels: [<?php echo $nombres; ?>],
                datasets: [densityData, densityData_pagadas],
            },
            options: {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor : $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (value) {
                                return addCommas(value)
                            }
                        }
                    }]
                }
            }
        });
    }

    function generateGraficoDos(){
        var radialchartColors = getChartColorsArray("#invested-overview");
        var options = {
            chart: {
                height: 270,
                type: 'radialBar',
                offsetY: -10
            },
            plotOptions: {
                radialBar: {
                    startAngle: -130,
                    endAngle: 130,
                    dataLabels: {
                        name: {
                            show: false
                        },
                        value: {
                            offsetY: 10,
                            fontSize: '18px',
                            color: undefined,
                            formatter: function (val) {
                                return val + "%";
                            }
                        }
                    }
                }
            },
            colors: [radialchartColors[0]],
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'dark',
                    type: 'horizontal',
                    gradientToColors: [radialchartColors[1]],
                    shadeIntensity: 0.15,
                    inverseColors: false,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [<?php echo $porcentajeRecuperadoGeneradas;?>, 100]
                },
            },
            stroke: {
                dashArray: 4,
            },
            legend: {
                show: false
            },
            series: [<?php echo $porcentajeRecuperadoGeneradas;?>],
            labels: ['Series A'],
        }

        var chart = new ApexCharts(
            document.querySelector("#invested-overview"),
            options
        );

        chart.render();
    }

    function generateGraficoTres(){ 

        var densityCanvas = document.getElementById("pieChart_3").getContext('2d');

        var densityData = {
            label: 'Valor Pagado',
            data: [<?php echo $datos; ?>],
            borderColor:'#36a2eb',
            backgroundColor : '#36a2eb',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var barChart = new Chart(densityCanvas, {
            type: 'line',
            data: {
                labels: [<?php echo $labels; ?>],
                datasets: [densityData],
            },
            options: {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor Pagado : $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (value) {
                                return addCommas(value)
                            }
                        }
                    }]
                }
            }
        });
    }

    function generateGraficoCuatro(){
        // mix line & bar
        var mixlinebarColors = getChartColorsArray("#pieChart_4");
        var dom = document.getElementById("pieChart_4");
        var myChart = echarts.init(dom);
        var app = {};
        option = null;
        app.title = 'Data view';

        option = {
            // Setup grid
            grid: {
                zlevel: 1,
                x: 100,
                x2: 70,
                y: 30,
                y2: 30,
                borderWidth: 0,
                backgroundColor: 'rgba(0,0,0,0)',
                borderColor: 'rgba(0,0,0,0)',
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },
            toolbox: {
                orient: 'center',
                left: 0,
                top: 10,
                feature: {
                  //dataView: { readOnly: true, title: "Data View" },
                  magicType: { type: ['line', 'bar'], title: {line: "For line chart", bar: "For bar chart"}},
                  restore: { title: "restore" },
                  saveAsImage: { title: "Download Image" }
                }
              },
            color: mixlinebarColors, //['#2ab57d', '#5156be', '#fd625e'],
            legend: {
                data:['Pagadas por Nómina','Pagadas por Administradora','Ingresos Mes'],
                textStyle: {color: '#858d98'}
            },
            xAxis: [
                {
                    type: 'category',
                    data: [<?php echo $nombres; ?>],
                    axisPointer: {
                        type: 'shadow'
                    },
                    axisLine: {
                        lineStyle: {
                          color: '#858d98'
                        },
                    },
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: 'Valor Nomina',
                    min: 0,
                    max: <?php echo $valorMaximoY;?>,
                    interval: <?php if($valorMaximoY > 200000000){ echo "50000000"; }else{ echo "20000000"; };?>,
                    axisLine: {
                        lineStyle: {
                          color: '#858d98'
                        },
                    },
                    splitLine: {
                        lineStyle: {
                            color:"rgba(133, 141, 152, 0.1)"
                        }
                    }
                },
                {
                    type: 'value',
                    name: 'Ingresos Mes',
                    min: 0,
                    max: <?php echo $valorMaximoY;?>,
                    interval: <?php if($valorMaximoY > 200000000){ echo "50000000"; }else{ echo "20000000"; };?>,
                    axisLine: {
                        lineStyle: {
                          color: '#858d98'
                        },
                    },
                    splitLine: {
                        lineStyle: {
                            color:"rgba(133, 141, 152, 0.1)"
                        }
                    }
                }
            ],
            series: [
                {
                    name:'Pagadas por Nómina',
                    type:'bar',
                    data:[<?php echo $valores;?>]
                },
                {
                    name:'Pagadas por Administradora',
                    type:'bar',
                    data:[<?php echo $valoreP; ?>]
                },
                {
                    name:'Ingresos Mes',
                    type:'line',
                    yAxisIndex: 1,
                    data:[<?php echo $datos; ?>]
                }
            ]
        };
        ;
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
    }

    function generateGraficoCinco() {
        var datas = new FormData();
        datas.append('month', $("#selectChangeMonths").val());
        $.ajax({
            url   : 'ajax/graficoCinco.php',
            method: 'post',
            data  : datas,
            cache : false,
            contentType : false,
            processData : false,
            dataType    : 'html',
            success     : function(data){
                $("#entidadesGrafico").html(data);
            },
            beforeSend:function(){
                $.blockUI({ 
                    message : '<h3>Un momento por favor....</h3>',
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff' 
                    } 
                }); 
            },
            complete:function(){
                $.unblockUI();
            }

        });
    }

    function addCommas(nStr){
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function scaleLabel (valuePayload) {
        return Number(valuePayload.value).toFixed(2).replace('.',',') + '$';
    }

    function getRandomColor() {
       return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
    }

    function getChartColorsArray(chartId) {
        var colors = $(chartId).attr('data-colors');
        var colors = JSON.parse(colors);
        return colors.map(function(value){
            var newValue = value.replace(' ', '');
            if(newValue.indexOf('--') != -1) {
                var color = getComputedStyle(document.documentElement).getPropertyValue(newValue);
                if(color) return color;
            } else {
                return newValue;
            }
        })
    }
</script>