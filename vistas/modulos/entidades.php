<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">FORMATOS</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Entidades </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                   
                    <div class="row">
                        <div class="col-lg-11">
                        ENTIDADES REGISTRADAS
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">
                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <a class="dropdown-item" title="Agregar Usuario EPS" data-bs-toggle="modal" data-bs-target="#modalAgregarEntidades" href="#">AGREGAR</a>
                                    <?php } ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
            <table style="width:100%;" id="tablaEntidades" class="table table-bordered table-striped dt-responsive tablas">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th style="width: 10%;">Codigo</th>
                            <th>Ciudad</th>
                            <th>Dirección</th>
                            <th>Teléfono</th>
                            <th>Horario de atención</th>
                            <th style="width: 10%;">Acciones</th>
                        </tr>
                    </thead>
                </table>    
            </div>
        </div>
    </div>
</div>
<!-- Modal agregar usuario -->
<div id="modalAgregarEntidades" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg"  role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" id="AgragrNuevaEntidad" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                <h5 class="modal-title"id="staticBackdropLabel">Agregar Entidad</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                <div class="mb-3">
                                    <label class="form-label">Ingresar Nombre Entidad</label>
                                        <input class="form-control" type="text" name="NuevoNombre" placeholder="Ingresar Nombre Entidad" required="true">
                                    </div>
                                </div>        
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                   
                                    <label class="form-label">Ingresar Codigo</label>
                                        <input class="form-control " type="text" name="NuevoCodigo" placeholder="Ingresar Codigo" >
                                    
                                </div>        
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                <div class="mb-3">
                                    <label class="form-label">Ingresar Ciudad</label>
                                        <input class="form-control" type="text" name="NuevoCiudad" placeholder="Ingresar Ciudad" >
                                    </div>
                                </div>        
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                  
                                    <label class="form-label">Ingresar Direccion</label>
                                        <input class="form-control" type="text" name="NuevoDireccion" placeholder="Ingresar Dirección" >
                                   
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                <div class="mb-3">
                                    <label class="form-label">Ingresar Telefono</label>
                                        <input class="form-control numeric" type="text" name="NuevoTelefono" placeholder="Ingresar Teléfono" >
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    
                                    <label class="form-label">Ingresar Horario Atencion</label>
                                        <input class="form-control" type="text" name="NuevoHorario" placeholder="Ingresar Horario de atención" >
                                   
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <div class="mb-3">
                                       
                                        <textarea class="form-control"  name="NuevoObservacion" placeholder="Observación"></textarea>
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="mb-3">
                                    <label>Formato Para Radicación De Incapacidades</label>
                                    <input type="file" class="form-control NuevaFoto" valor='0' name="NuevoFormato1" placeholder="Elegir rut del cliente">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar pull-right">
                                </div>
                            </div>

                            <div class="col-md-4 col-xs-12">
                                <div class="mb-3">
                                    <label>Formato Solicitud De Pago Incapcacidades</label>
                                    <input type="file" class="form-control  NuevaFoto" valor='2' name="NuevoFormato2" placeholder="Elegir rut del cliente">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2 pull-right">
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="mb-3">
                                    <label>Otros Formatos <span style="visibility:hidden;">Formato Solicitud De Pago </span></label>
                                    <input type="file" class="form-control  NuevaFoto" valor='3' name="NuevoFormato3" placeholder="Elegir rut del cliente">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_3 pull-right">
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="row panel">
                           
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Salir</button>
                    <!--<button type="submit" class="btn btn-primary">Guardar Entidad</button>-->
                        <button type="button" id="btnNuevaEntidad"class="btn btn-danger">Guardar Entidad</button>
                    </div>
                <?php
                    /*$crearcliente = new ControladorEntidades();
                    $crearcliente->ctrCrearEntidad();*/
                ?>
                </div>
            </form>
        </div>
    </div>
</div>
 
<div id="modalEditarEntidades" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" id="EditarEntidadExistente" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                <h5 class="modal-title"id="staticBackdropLabel">Editar Entidad</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                   
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="mb-3">
                                        <label class="form-label">Ingresar Nombre Entidad</label>
                                        <input class="form-control" type="text" name="EditarNombre"  id="EditarNombre" placeholder="Ingresar Nombre Entidad" required="true">
                                    </div>
                                </div>        
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    
                                        <label class="form-label">Ingresar Codigo</label>
                                        <input class="form-control " type="text" name="EditarCodigo" id="EditarCodigo" placeholder="Ingresar Codigo" >
                                    
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="mb-3">
                                     <label class="form-label">Ingresar Ciudad</label>
                                        <input class="form-control" type="text" name="EditarCiudad" id="EditarCiudad" placeholder="Ingresar Ciudad" >
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    
                                        <label class="form-label">Ingresar Direccion</label>
                                        <input class="form-control" type="text" name="EditarDireccion" id="EditarDireccion" placeholder="Ingresar Dirección" >
                                   
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="mb-3">
                                    <label class="form-label">Ingresar Telefono</label>
                                        <input class="form-control numeric" type="text" name="EditarTelefono" id="EditarTelefono" placeholder="Ingresar Teléfono" >
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                   
                                    <label class="form-label">Ingresar Horario Atencion</label>
                                        <input class="form-control" type="text" name="EditarHorario" id="EditarHorario" placeholder="Ingresar Horario de atención" >
                                  
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <div class="mb-3">
                                        
                                        <textarea class="form-control"  name="EditarObservacion" id="EditarObservacion" placeholder="Observación"></textarea>
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Formato Para Radicación De Incapacidades</label>
                                    <input type="file" class="NuevaFoto" valor='0' name="EditarFormato1" placeholder="Elegir rut del cliente">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar pull-right">
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Formato Solicitud De Pago Incapcacidades</label>
                                    <input type="file" class="NuevaFoto" valor='2' name="EditarFormato2" placeholder="Elegir rut del cliente">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2 pull-right">
                                </div>
                            </div>
                        </div>
                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Otros Formatos</label>
                                    <input type="file" class="NuevaFoto" valor='3' name="EditarFormato3" placeholder="Elegir rut del cliente">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_3 pull-right">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="EditarId" id="EditarId">
                        <input type="hidden" name="formato1" id="formato1">
                        <input type="hidden" name="formato2" id="formato2">
                        <input type="hidden" name="formato3" id="formato3">

                        <button type="button" class="btn btn-secondary waves-effect"data-bs-dismiss="modal">Salir</button>
                    <!--<button type="submit" class="btn btn-primary">Guardar Entidad</button>-->
                        <button type="button" id="btnEditarEntidade" class="btn btn-danger">Guardar Entidad</button>   
                    </div>
                <?php
                    /*$crearcliente = new ControladorEntidades();
                    $crearcliente->ctrEditarEntidad();*/
                ?>
                </div>
            </form>
        </div>
    </div>
</div>

<?php 
    /*$crearPaciente = new ControladorEntidades();
    $crearPaciente->ctrBorrarEntidad();*/
?>

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/entidades.js?v=<?php echo rand();?>"></script>
