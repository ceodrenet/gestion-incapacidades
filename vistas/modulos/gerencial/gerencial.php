
<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REPORTES-GERENCIAL</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Gerencial </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                   DETALLE DE PAGOS CONSOLIDADO
                </h5>
            </div>
            <div class="card-body">
                 <form id="formEnvioReporteFacturas" autocomplete="off" method="post" action="vistas/modulos/facturas/exportar.php">
                    <div class="row">
                    <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-3">
                            <div class="mb-3">
                                <input class="form-control flatpickr-input" type="text" name="NuevoFechaInicio" id="NuevoFechaInicio2" placeholder="Ingresar Fecha Inicial" required="true">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3">
                                 <input class="form-control flatpickr-input" type="text" name="NuevoFechaFinal" id="NuevoFechaFinal2" placeholder="Ingresar Fecha Final" required="true">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscarRangos_general"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="exportarFacturacion"><i class="fa fa-search"></i>&nbsp;Exportar</button>
                            </div>
                        </div>
                    </div>    
                </form>
            </div>
        </div>
    </div>
        <div class="row">
                <div class="col-md-12" id="resultados">
                    
                </div>
            </div> 
</div>

    <!-- Main content -->
  
</div>
<!-- Required charts js -->


<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>

<script type="text/javascript">
    $(function(){

        //getDatosGerencial();
        var NuevoFechaFinal2 = flatpickr('#NuevoFechaFinal2', {
            altInput: true,
            locale: "es",
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            onChange: function(selectedDates, dateStr, instance){
                var minDate = dateStr;
                var fecha1 = moment($("#NuevoFechaInicio2").val());
                var fecha2 = moment(minDate);
                $("#EditarNumeroDias").val( (fecha2.diff(fecha1, 'days') + 1) + " Días");
            }
        });

        var NuevoFechaInicio2 = flatpickr('#NuevoFechaInicio2', {
                altInput: true,
                locale: "es",
                altFormat: "d/m/Y",
                dateFormat: "Y-m-d",
                onChange: function(selectedDates, dateStr, instance) {
                    NuevoFechaFinal2.set("minDate", dateStr);
                    NuevoFechaFinal2.setDate(dateStr);
                },
        });
     
        $("#btnBuscarRangos_general").click(function(){
            getDatosGerencial();
        });
        
        $("#btnExportarReporteGerencial").click(function(){
            var fechaInicio = $("#NuevoFechaInicio2").val();
            var fechaFinal  = $("#NuevoFechaFinal2").val();
            var paso = 0;
            if(fechaInicio.length < 0){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "La fecha inicial es necesaria",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });

            } 

            if(fechaFinal.length < 0){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "La fecha final es necesaria",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });

            }
        });     

    });


function getDatosGerencial(){
    var fechaInicio = $("#NuevoFechaInicio2").val();
    var fechaFinal = $("#NuevoFechaFinal2").val();
    var paso = 0;
    if(fechaInicio.length < 0){
        paso = 1;
        swal({
            title : "Error al buscar",
            text  : "La fecha inicial es necesaria",
            type  : "error",
            confirmButtonText : "Cerrar"
        });

    } 

    if(paso == 0){
        $.ajax({
            url    : 'ajax/gerencial_filtrado.php',
            type   : 'post',
            data   :{
                anho  : fechaInicio,
                anhoFinal  : fechaFinal,
                cliente_id    : $("#session").val()
            },
            dataType : 'html',
            beforeSend:function(){
                $.blockUI({ 
                    baseZ: 2000,
                    message : '<h3>Un momento por favor....</h3>',
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff'
                    } 
                }); 
                $("#btnExportarReporteGerencial").attr('href', 'index.php?exportar=pdfGerencial&NuevoFechaInicial='+$("#NuevoFechaInicio2").val()+'&NuevoFechaFinal='+$("#NuevoFechaInicio3").val());
            },
            complete:function(){
                $.unblockUI();
            },
            success : function(data){
                $("#resultados").html(data);
                
            }
        })
    } 
}
</script>
