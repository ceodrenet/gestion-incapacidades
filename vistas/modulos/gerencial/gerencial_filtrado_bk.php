<?php
    session_start();
    require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../controladores/tesoreria.controlador.php';
    require_once '../../../modelos/incapacidades.modelo.php';
    require_once '../../../modelos/tesoreria.modelo.php';

    $campos = "count(*) as estado";
    $tabla = "gi_incapacidad";
    if($_SESSION['cliente_id'] != 0){
        $condiciones = "inc_estado_tramite !=  'EMPRESA' AND inc_empresa = ".$_SESSION['cliente_id']." AND inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."'";
    }else{
        $condiciones = "inc_estado_tramite !=  'EMPRESA' AND  inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."'";
    }

    $respuestaX = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

    $year = $_POST['anho'];

    $otherYear =  $_POST['anhoFinal'];
?>
<?php include __DIR__.'/../moduloInicio.php'; ?>

<div class="row">
    <div class="col-md-6">
        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">
                    DATOS DE INCAPACIDADES
                </h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>DETALLE</th>
                            <th style="width: 30%;">CANTIDAD</th>
                            <th>VALOR</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                EMPRESA (1 Y 2 Días)
                            </td>
                            <td>
                                <?php
                                    
                                    $campos = "count(*) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_valor_pagado_empresa IS NOT NULL AND inc_valor_pagado_empresa != ''";
                                    }else{
                                        $condiciones = " inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_valor_pagado_empresa IS NOT NULL AND inc_valor_pagado_empresa != '' ";
                                    }
                            
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $solicitudes = $respuesta['estado']; 
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    $campos = "sum(inc_valor_pagado_empresa) as total";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_valor_pagado_empresa IS NOT NULL AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = " inc_valor_pagado_empresa IS NOT NULL AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $totaValor1 = $respuesta2['total'];
                                    echo "$ ".number_format($totaValor1, 0, ',', '.');
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                TOTAL ADMINISTRADORAS
                            </td>
                            <td>
                                <?php
                                    $campos = "count(*) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){ 
                                        $condiciones = "inc_estado_tramite !=  'EMPRESA' AND inc_empresa = ".$_SESSION['cliente_id']." AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite !=  'EMPRESA' AND inc_estado_tramite !=  'PAGADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                                    $totales = $respuesta['estado'];
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    $campos = "sum(inc_valor_pagado_eps) as total ";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_estado_tramite !=  'EMPRESA' AND inc_empresa = ".$_SESSION['cliente_id']." AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite !=  'EMPRESA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                                    $totaValor = $respuesta['total'];
                                    $intVariablePorcentage = $totaValor;
                                    echo "$ ".number_format($respuesta['total'], 0, ',', '.');
                                ?>
                                
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 40%;">INCAPACIDADES POR ESTADO</th>
                            <th>TOTAL</th>
                            <th>VALOR</th>
                            <th>%</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                PAGADAS
                            </td>
                            <td>
                                <?php
                                    $total = 0;
                                    $campos = "count(inc_estado_tramite) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'PAGADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'PAGADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                                    $pagados = $respuesta['estado'];
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    $campos = "sum(inc_valor) as total";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'PAGADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'PAGADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $totaValor1 = $respuesta2['total'];
                                    $total += $totaValor1;
                                    echo "$ ".number_format($totaValor1, 0, ',', '.');
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($totaValor > 0){
                                        echo number_format(($totaValor1 * 100) / $totaValor, 2)." %";
                                    }else{
                                        echo "0";
                                    }
                                    
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                POR RADICAR
                            </td>
                            <td>
                                <?php
                                    
                                    $campos = "count(inc_estado_tramite) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'POR RADICAR' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'POR RADICAR' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $porRadicar = $respuesta['estado'];
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                
                                    $campos = "sum(inc_valor_pagado_eps) as total";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'POR RADICAR' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'POR RADICAR' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);


                                    $totaValor1 = $respuesta2['total'];
                                    $total += $totaValor1;
                                    echo "$ ".number_format($totaValor1, 0, ',', '.');
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($totaValor > 0){
                                        echo number_format(($totaValor1 * 100) / $totaValor, 2)." %";
                                    }else{
                                        echo "0";
                                    }
                                    
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                RADICADAS
                            </td>
                            <td>
                                <?php
                                    
                                    $campos = "count(inc_estado_tramite) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'RADICADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'RADICADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $radicadas = $respuesta['estado'];
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    
                                    $campos = "sum(inc_valor_pagado_eps) as total";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'RADICADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'RADICADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                                    $totaValor1 = $respuesta2['total'];
                                    $total += $totaValor1;
                                    echo "$ ".number_format($totaValor1, 0, ',', '.');
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($totaValor > 0){
                                        echo number_format(($totaValor1 * 100) / $totaValor, 2)." %";
                                    }else{
                                        echo "0";
                                    }
                                    
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                SIN RECONOCIMIENTO
                            </td>
                            <td>
                                <?php 

                                    $campos = "count(inc_estado_tramite) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'SIN RECONOCIMIENTO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'SIN RECONOCIMIENTO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);


                                    $sinReconocimiento = $respuesta['estado']; 
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    $campos = "sum(inc_valor_pagado_eps) as total";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'SIN RECONOCIMIENTO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'SIN RECONOCIMIENTO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                    
                                    $totaValor1 = $respuesta2['total'];
                                    $total += $totaValor1;
                                    echo "$ ".number_format($totaValor1, 0, ',', '.');
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($totaValor > 0){
                                        echo number_format(($totaValor1 * 100) / $totaValor, 2)." %";
                                    }else{
                                        echo "0";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                SOLICITUDES DE PAGO
                            </td>
                            <td>
                                <?php
                                    
                                    $campos = "count(inc_estado_tramite) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'SOLICITUD DE PAGO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'SOLICITUD DE PAGO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $solicitudes = $respuesta['estado']; 
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    $campos = "sum(inc_valor_pagado_eps) as total";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'SOLICITUD DE PAGO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'SOLICITUD DE PAGO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $totaValor1 = $respuesta2['total'];
                                    $total += $totaValor1;
                                    echo "$ ".number_format($totaValor1, 0, ',', '.');
                                ?>
                            </td>
                            <td>
                                <?php
                                    
                                    if($totaValor > 0){
                                        echo number_format(($totaValor1 * 100) / $totaValor, 2)." %";
                                    }else{
                                        echo "0";
                                    }
                                    
                                    
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>  
    </div>
    <div class="col-md-6">
        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">
                    DATOS DE INCAPACIDADES GRAFICO 
                </h3>
            </div>
            <div class="box-body">
                <canvas id="pieChart_2" style="height:334px; width: 100%;"></canvas>
            </div>
        </div>
    </div> 
</div>
<div class="row">
    <div class="col-md-6">
        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">
                    INCAPACIDADES NOMINA VS PAGADAS
                </h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="pieChart_4" style="height:454px"></canvas>
                  
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-danger box-solid">
            <div class="box-header">
                <h3 class="box-title">
                    INFORME GRAFICO - PAGOS MENSUALES
                </h3>
            </div>
            <div class="box-body">
                
                <canvas id="pieChart_3" style="height:454px"></canvas>
            </div>

        </div>
    </div>
</div>

<?php include __DIR__.'/../moduloInicioAlertas.php'; ?>

<?php include __DIR__.'/../moduloAdministradora.php'; ?>


<script type="text/javascript">
    $(function(){
        crearDonutChartTotal_2();
        get_barras_3();
        crearDonaGenero();

        $('.tblOther').DataTable({
            "language" : {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando _START_ al _END_ de  _TOTAL_",
                "sInfoEmpty":      "No hay registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "pageLength": 10
        });
    })
    
    function crearDonutChartTotal_2(){
        <?php
            $whereCliente = '';
            if($_SESSION['cliente_id'] != 0){
                $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
            }

            $campos = 'sum(inc_valor) as total, inc_origen ';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
            $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY inc_origen', 'ORDER BY inc_origen ASC');
            $i = 0;

            $labels = '';
            $datos = '';
            $colors = '';

            foreach ($respuesta as $key => $value) {

                if( $value['total'] != '' &&  $value['total'] != null){
                    if( $labels == ''){
                        $labels = '"'.$value['inc_origen'].'"';
                        $datos   = $value['total'];
                    }else{
                        $labels .= ' , "'.$value['inc_origen'].'"';
                        $datos   .= " , ".$value['total'];
                    }
                }
                
            }
        ?>

        var densityCanvas = document.getElementById("pieChart_2").getContext('2d');

        var densityData = {
            label: 'Valor Pagado',
            data: [<?php echo $datos; ?>],
            borderColor:['#dd4b39','#e65100','#00a65a','#ff6384','#00c0ef'],
            backgroundColor : ['#dd4b39', '#e65100' , '#00a65a', '#ff6384' , '#00c0ef'],
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };
        <?php
            $campos = 'sum(inc_valor) as total ';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
            $respuestaPagos = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
        ?>
        var barChart = new Chart(densityCanvas, {
            type: 'pie',
            data: {
                labels: [<?php echo $labels; ?>],
                datasets:[densityData]
            },
            options: {
                title: {
                    display: true,
                    text: '$<?php echo number_format($respuestaPagos['total'], 0, ',', '.'); ?>',
                    fontStyle: 'bold',
                    fontSize: 30
                },
                legend: false,
                tooltips: {
                    callbacks: {
                        // this callback is used to create the tooltip label
                        label: function(tooltipItem, data) {
                            // get the data label and data value to display
                            // convert the data value to local string so it uses a comma seperated number
                            var dataLabel = data.labels[tooltipItem.index];
                            var value = ': $ ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString();
                            if (Chart.helpers.isArray(dataLabel)) {
                                // show value on first line of multiline label
                                // need to clone because we are changing the value
                                dataLabel = dataLabel.slice();
                                dataLabel[0] += value;
                            } else {
                                dataLabel += value;
                            }

                            // return the text to display on the tooltip
                            return dataLabel;
                        }
                    }
                }
            }
        });

    }

    function get_barras_3(){ 
        <?php              
        if($_SESSION['cliente_id'] != 0){
                $item = 'inc_empresa';
                $valor = $_SESSION['cliente_id'];
                $respuesta = ControladorIncapacidades::ctrMostrarLastSixMonth($item, $valor, $year);
            }else{
                $item = null;
                $value = null;
                $respuesta = ControladorIncapacidades::ctrMostrarLastSixMonth($item, $value, $year);
            }

            $arrayMont = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

            $labels = '';
            $datos = '';
            $colors = '';

            foreach ($respuesta as $key => $value) {

                if( $value['valor'] != '' &&  $value['valor'] != null){
                    if( $labels == ''){
                        $labels = '"'.$arrayMont[$value['MES']-1].'"';
                        $datos   = $value['valor'];
                        $colors .= "getRandomColor()";
                    }else{
                        $labels .= ' , "'.$arrayMont[$value['MES']-1].'"';
                        $datos   .= " , ".$value['valor'];
                        $colors .= ",getRandomColor()";
                    }
                }
                
            }
        ?>

        var densityCanvas = document.getElementById("pieChart_3").getContext('2d');

        var densityData = {
            label: 'Valor Pagado',
            data: [<?php echo $datos; ?>],
            borderColor:'#36a2eb',
            backgroundColor : '#36a2eb',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var barChart = new Chart(densityCanvas, {
            type: 'line',
            data: {
                labels: [<?php echo $labels; ?>],
                datasets: [densityData],
            },
            options: {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor Pagado : $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (value) {
                                return addCommas(value)
                            }
                        }
                    }]
                }
            }
        });
    }

    function crearDonaGenero(){
        <?php 
            $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

            $whereCliente = '';
            if($_SESSION['cliente_id'] != 0){
                $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
            }

            $campos = 'SUM(inc_valor_pagado_eps) as total , MONTH(inc_fecha_pago_nomina) as mes';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != 'EMPRESA'  ";
            $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');

            $nombres = '';
            $valores = '';
            $valoreP = '';
            $i = 0;

            foreach ($incapacidades as $key => $value) {
                $numero = $value['mes'];
                if($i == 0){
                    $nombres .= "'".$month[$numero -1]."'";
                    $valores .= "'".$value['total']."'"; 
                }else{
                    $nombres .= " , '".$month[$numero -1]."'";
                    $valores .= " , '".$value['total']."'"; 
                }
                $i++;
            }

            $campos = 'SUM(inc_valor) as total , MONTH(inc_fecha_pago_nomina) as mes';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
            $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');
            $i = 0;
            foreach ($incapacidades as $key => $value) {
                
                if($i == 0){
                    $valoreP .= "'".$value['total']."'"; 
                }else{
                    $valoreP .= ", '".$value['total']."'"; 
                }
                $i++;
            }
        ?>

        var densityCanvas = document.getElementById("pieChart_4").getContext('2d');

        var densityData = {
            label: 'Incapacidades',
            data: [<?php echo $valores;?>],
            borderColor:'#ff6384',
            backgroundColor : '#ff6384',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var densityData_pagadas = {
            label: 'Pagadas',
            data: [<?php echo $valoreP; ?>],
            borderColor:'#36a2eb',
            backgroundColor : '#36a2eb',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var barChart = new Chart(densityCanvas, {
            type: 'bar',
            data: {
                labels: [<?php echo $nombres; ?>],
                datasets: [densityData, densityData_pagadas],
            },
            options: {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor Pagado : $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (value) {
                                return addCommas(value)
                            }
                        }
                    }]
                }
            }
        });
    }

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function scaleLabel (valuePayload) {
        return Number(valuePayload.value).toFixed(2).replace('.',',') + '$';
    }

    function getRandomColor() {
       return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
    }


</script>