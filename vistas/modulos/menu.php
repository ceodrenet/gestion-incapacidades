<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<?php 
			   	$menuspermisos = ControladorPerfiles::ctrMostrarMenusPermisos('perfiles_permisos_perfil_id_i',$_SESSION['perfil']);
			   	//echo $_SESSION['cliente_id'] ;
                foreach($menuspermisos as $item){
	                $menus = ControladorPerfiles::ctrMostrarMenus('menus_id_i',$item['perfiles_permisos_menu_id_i']);
	                if($menus['menus_reporte_i'] == 0){
	                	if($menus['menus_es_treview'] == 1){
				 			echo '
				 				<li class="treeview" id="'.$menus['menus_id_v'].'">
									<a href="#">
										<i class="'.$menus['menus_html_icon_v'].'"></i> <span>'.mb_strtoupper($menus['menus_nombre_v']).'</span>
										<span class="pull-right-container">
											<i class="fa fa-angle-left pull-right"></i>
										</span>
									</a>
									<ul class="treeview-menu">';
									
									$submenus = ControladorPerfiles::ctrMostrarOpciones('submenu_menu_id', $menus['menus_id_i']);
									//var_dump($submenus);
									foreach ($submenus as $key) {
										echo '<li id="'.$key['submenu_id_v'].'">
												<a href="'.$key['submenu_href'].'">
													<i class="fa fa-circle-o"></i> '.$key['submenu_nombre'].'
												</a>
											</li>';
									}
							echo '	</ul>
								</li>';
				 		}else{
				 			echo '
								<li id="'.$menus['menus_id_v'].'">
				          			<a href="'.$menus['menus_html_href_v'].'">
					            		<i class="'.$menus['menus_html_icon_v'].'"></i>
					            		<span>'.mb_strtoupper($menus['menus_nombre_v']).'</span>
				          			</a>
		        			</li>';
				 		}	
	                }
	        	} 
	        ?>
		</ul>
	</section>
</aside>