<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REPORTES-GERENCIAL</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Gestión </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                  INFORME DE GESTIÓN
                </h5>
            </div>
            <div class="card-body">
                 <form id="formEnvioReporteGestion" autocomplete="off" method="post" action="">
                    <div class="row">
                    <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-3">
                            <div class="mb-3">
                                <label>AÑO DE CONSULTA</label>
                                <select id="NuevoFechaInicioYear" id="NuevoFechaInicioYear" class="form-control">
                                   <?php 
                                        $i = 0;
                                        while ($i < 6) {
                                            echo "<option value='".(date('Y') - $i)."'>".(date('Y') - $i)."</option>";
                                            $i++;
                                        }
                                   ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="mb-3">
                                <label>FECHA INICIAL</label>
                                <select class="form-control" name="NuevoFechaInicio" id="NuevoFechaInicio2" required="true">
                                    <option value="0">Enero</option>
                                    <option value="1">Febrero</option>
                                    <option value="2">Marzo</option>
                                    <option value="3">Abril</option>
                                    <option value="4">Mayo</option>
                                    <option value="5">Junio</option>
                                    <option value="6">Julio</option>
                                    <option value="7">Agosto</option>
                                    <option value="8">Septiembre</option>
                                    <option value="9">Octubre</option>
                                    <option value="10">Noviembre</option>
                                    <option value="11">Diciembre</option>
                                    </select>
                                        
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3">
                                <label>FECHA FINAL</label>
                                <select class="form-control" name="NuevoFechaFinal" id="NuevoFechaFinal2" required="true">
                                    <option value="0">Enero</option>
                                    <option value="1">Febrero</option>
                                    <option value="2">Marzo</option>
                                    <option value="3">Abril</option>
                                    <option value="4">Mayo</option>
                                    <option value="5">Junio</option>
                                    <option value="6">Julio</option>
                                    <option value="7">Agosto</option>
                                    <option value="8">Septiembre</option>
                                    <option value="9">Octubre</option>
                                    <option value="10">Noviembre</option>
                                    <option value="11">Diciembre</option>
                                    </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="mb-3">
                                <label for="btnBuscarRangos_general" style="visibility: hidden;">BTN</label>
                                <div class="d-grid gap-2">
                                    <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscarRangos_general"><i class="fa fa-search"></i>&nbsp;Generar</button>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>
            </div>
        </div>
    </div>
        <div class="row">
                <div class="col-md-12" id="resultados">
                    
                </div>
            </div> 
</div>

    <!-- Main content -->
  
</div>
<!-- Required charts js -->


<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>

<script type="text/javascript">

   /* var NuevoFechaFinal2 = flatpickr('#NuevoFechaFinal2', {
                altInput: true,
                altFormat: "d/m/Y",
                dateFormat: "Y-m-d",
                onChange: function(selectedDates, dateStr, instance){
                    var minDate = dateStr;
                    var fecha1 = moment($("#NuevoFechaInicio2").val());
                    var fecha2 = moment(minDate);
                    $("#EditarNumeroDias").val( (fecha2.diff(fecha1, 'days') + 1) + " Días");
                }
            });
            

    $("#NuevoFechaInicioYear").flatpickr({
        language: "es",
        autoclose: true,
        todayHighlight: true,
        viewMode: "years", 
        minViewMode: "years",
        format: "yyyy"
    });

    $("#NuevoFechaInicio2").flatpickr({
        language: "es",
        autoclose: true,
        todayHighlight: true,
        format: "MM",
        viewMode: "months", 
        minViewMode: "months"
    
    });


    $("#NuevoFechaFinal2").flatpickr({
        language: "es",
        autoclose: true,
        todayHighlight: true,
        format: "MM",
        viewMode: "months", 
        minViewMode: "months"
    });*/


    $("#btnBuscarRangos_general").click(function(){
 
        var fechaInicio = $("#NuevoFechaInicio2").val();
        var fechaFinal  = $("#NuevoFechaFinal2").val();
        var nuevoFechaInicioYear = $("#NuevoFechaInicioYear").val();
        
        var paso = 0;
        /*if(fechaFinal.length < 0){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha final es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });

        }   

        if(fechaInicio.length < 1){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha Inicial es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
        }*/

        if(paso == 0){
            $.ajax({
                url    : 'ajax/procesoReporteEspecial.ajax.php',
                type   : 'post',
                data   :{
                    fechaInicial  : fechaInicio,
                    fechaFinal    : fechaFinal,
                    NuevoFechaInicioYear : nuevoFechaInicioYear,
                    fechaBusqueda : 'inc_fecha_inicio',
                    cliente_id    : $("#session").val()
                },
                dataType : 'json',
                success : function(data){
                    /*var currentdate = new Date();
                    var fecha_hora = currentdate.getFullYear() + rellenar((currentdate.getMonth()+1), 2) +  rellenar(currentdate.getDate(), 2) + "_" + rellenar(currentdate.getHours(), 2) +  rellenar(currentdate.getMinutes(), 2) + rellenar(currentdate.getSeconds(), 2);
                    var $a = $("<a>");
                    $a.attr("href",data.file);
                    $("body").append($a);
                    $a.attr("download","Informe_Gestion_"+fecha_hora+".pdf");
                   // $a.attr('target', '_blank');
                    $a[0].click();
                    $a.remove();*/
                    var pdf=data.file; 
                    let pdfWindow = window.open("")
                    pdfWindow.document.write("<iframe width='100%' height='100%' src='" + encodeURI(pdf) + "'></iframe>")
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            })
        }
    });

    function rellenar (str, max) {
        str = str.toString();
        return str.length < max ? rellenar("0" + str, max) : str;
    }
</script>