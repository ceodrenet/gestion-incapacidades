<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
    require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../controladores/clientes.controlador.php';
    
    require_once '../../../modelos/dao.modelo.php';
    require_once '../../../modelos/incapacidades.modelo.php';
    require_once '../../../modelos/clientes.modelo.php';


    if(isset($_POST['fechaInicial']) && isset($_POST['fechaFinal'])){
        require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';

        $array = array(
            'Enero'     => '01',
            'Febrero'   => '02',
            'Marzo'     => '03',
            'Abril'     => '04',
            'Mayo'      => '05',
            'Junio'     => '06',
            'Julio'     => '07',
            'Agosto'    => '08',
            'Septiembre' => '09',
            'Octubre'   => '10',
            'Noviembre' => '11',
            'Diciembre' => '12'
        );

        $arrayMonths = array(
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'  
        );


        class MYPDF extends TCPDF {

            protected $processId = 0;
            protected $header = '';
            protected $footer = '';
            static $errorMsg = '';
            //Page header
            public function Header() {
                $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
               $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

               $this->Line(5, 5, $this->getPageWidth()-5, 5); 

               $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
               $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
               $this->Line(5, 5, 5, $this->getPageHeight()-5);

            }

            // Page footer
            public function Footer() {
                // Position at 15 mm from bottom
                $this->SetY(-15);
                // Set font
                $this->SetFont('helvetica', 'N', 8);
                // Page number
                $this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            }
        }


        /*empezar el proceso del PDF*/
        $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $obj_pdf->SetCreator(PDF_CREATOR);
        $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
        $obj_pdf->setPrintHeader(true);
        $obj_pdf->setPrintFooter(true);
        $obj_pdf->SetAutoPageBreak(TRUE, 20);
        $obj_pdf->SetFont('times', '', 15);
        $empres = '';
        $nit = '';
        
        $item  = 'emp_id';
        $valor = $_SESSION['cliente_id']; 
        $empresa = ControladorClientes::ctrMostrarClientes($item, $valor);
       
        for($res = $_POST['fechaInicial']; $res < $_POST['fechaFinal']+1; $res++){
            $obj_pdf->AddPage();
            $num = 0;
            $arrayMesesCompleto = array();
            $arrayMesesPagos = array();
            $arrayMesesPagos['_MoneyPagadas0_90'] = 0;
            $arrayMesesPagos['_MoneyPagadas91_120'] = 0;
            $arrayMesesPagos['_MoneyPagadas121_150'] = 0;
            $arrayMesesPagos['_MoneyPagadas151_180'] = 0;
            $arrayMesesPagos['_MoneyPagadas181_210'] = 0;
            $arrayMesesPagos['_MoneyPagadas210'] = 0;
            $arrayMesesCompleto['recibi'] = 0;
            $arrayMesesCompleto['pagada'] = 0 ;
            $arrayMesesCompleto['negada'] = 0;
            $arrayMesesCompleto['restan'] = 0;

            $arrayMesesCompleto['Rrecibi'] = 0;
            $arrayMesesCompleto['Rpagada'] = 0;
            $arrayMesesCompleto['Rnegada'] = 0;
            $arrayMesesCompleto['Rrestan'] = 0; 

            /*
            *Array para tener los valores de las IPS
            */
            $arrayIPS = array();
            $ips = ModeloIncapacidades::mdlMostrarGroupAndOrder('ips_id, ips_nombre','gi_ips JOIN gi_incapacidad ON inc_ips_afiliado = ips_id', 'inc_fecha_pago_nomina BETWEEN \''.$res.'-01-01'.'\' AND \''.$res.'-12-31'.'\' AND inc_empresa = '.$_SESSION['cliente_id'], 'GROUP BY ips_id', 'ORDER BY ips_nombre ASC', null);

            for($i = 1; $i <= 12; $i++){
                $j = $i;
                if($i < 10 && strlen($i) < 2){
                    $j = '0'.$j;
                }
                $Inicio = $res."-".$j.'-01';
                $final =  $res."-".$j.'-31';
                // $arrayMesesCompleto[$num]['mes'] = $arrayMonths[$j];
                //$obj_pdf->AddPage(); 
                $condicionX = 'inc_fecha_pago_nomina BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'].' AND inc_estado != 0 ';

                $recibidas = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionX.' AND inc_estado_tramite != \'EMPRESA\'');
                $pagadas = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'PAGADA\'');
                $negadas = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'SIN RECONOCIMIENTO\'');
                $restante = $recibidas - $pagadas - $negadas;
                

                /*VALORES*/
                $_MoneyRecibidas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite != \'EMPRESA\'');
                $_MoneyPagadas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'PAGADA\'');
                $_MoneyNegadas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'SIN RECONOCIMIENTO\'');
                $_MoneyRestante = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite NOT IN (\'SIN RECONOCIMIENTO\',\'EMPRESA\',\'PAGADA\') ');
                
               

                $arrayMesesCompleto['recibi'] += $_MoneyRecibidas['total'];
                $arrayMesesCompleto['pagada'] += $_MoneyPagadas['total'] ;
                $arrayMesesCompleto['negada'] += $_MoneyNegadas['total'];
                $arrayMesesCompleto['restan'] += $_MoneyRestante['total'];

                $arrayMesesCompleto['Rrecibi'] += $recibidas;
                $arrayMesesCompleto['Rpagada'] += $pagadas;
                $arrayMesesCompleto['Rnegada'] += $negadas;
                $arrayMesesCompleto['Rrestan'] += $restante;

                $_MoneyPagadas0_90 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'PAGADA\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > -100 AND datediff(inc_fecha_pago, inc_fecha_generada ) <= 90');
                $_MoneyPagadas91_120 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'PAGADA\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > 90 AND datediff(inc_fecha_pago, inc_fecha_generada ) <= 120');
                $_MoneyPagadas121_150 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'PAGADA\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > 120 AND datediff(inc_fecha_pago, inc_fecha_generada ) <= 150');

                $_MoneyPagadas151_180 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'PAGADA\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > 151 AND datediff(inc_fecha_pago, inc_fecha_generada ) <= 180');
                $_MoneyPagadas181_210 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'PAGADA\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > 181 AND datediff(inc_fecha_pago, inc_fecha_generada ) <= 210');
                $_MoneyPagadas210 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'PAGADA\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > 210');
                

                $arrayMesesPagos['_MoneyPagadas0_90'] += $_MoneyPagadas0_90['total'];
                $arrayMesesPagos['_MoneyPagadas91_120'] += $_MoneyPagadas91_120['total'];
                $arrayMesesPagos['_MoneyPagadas121_150'] += $_MoneyPagadas121_150['total'];
                $arrayMesesPagos['_MoneyPagadas151_180'] += $_MoneyPagadas151_180['total'];
                $arrayMesesPagos['_MoneyPagadas181_210'] += $_MoneyPagadas181_210['total'];
                $arrayMesesPagos['_MoneyPagadas210'] += $_MoneyPagadas210['total'];

                $num++;
            }

            $_PorPagadas = 0;
            $_PorNegadas = 0;
            $_PorRestant = 0;
            if($arrayMesesCompleto['recibi'] > 0){
                $_PorPagadas = ($arrayMesesCompleto['pagada'] * 100) / $arrayMesesCompleto['recibi'];
                $_PorNegadas = ($arrayMesesCompleto['negada'] * 100) / $arrayMesesCompleto['recibi'];
                $_PorRestant = ($arrayMesesCompleto['restan'] * 100) / $arrayMesesCompleto['recibi'];
            }
            $content = '';
            $content = '
            <br/>
            <br/>
            <table>
                <tr>
                    <td width="80%" >
                        <table style="font-family:tahoma;font-size:12;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                            <tr>
                                <td style="width:20%"></td>
                                <td style="width:60%; text-align:center; font-size:14px;"><b>INFORME DE GESTIÓN</b></td>
                                <td style="width:20%"></td>
                            </tr>
                        </table>
                        <table style="font-family:tahoma;font-size:12;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                            <tr>
                                <td style="width:20%"></td>
                                <td style="width:60%;text-align:center;">'.$empresa['emp_nombre'].'</td>
                                <td style="width:20%"></td>
                            </tr>
                        </table>
                    </td>
                    <td width="20%" align="center" valign="bottom">
                        <img src="../exportar/LOGO_2.jpg" width="85px" height="40px">
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <table style="font-family:tahoma;font-size:9;" width="10%" cellpadding="2px" cellspacing="2px" width="100%">
                <tr>
                    <td style="width:8%;text-align:justify;"><b>AÑO:</b></td>
                    <td style="width:50%;text-align:justify;">'.$res.'</td>
                    <td style="width:42%;text-align:right;">Periodo de Enero a Diciembre de '.$res.'</td>
                </tr>
            </table>
            <br/>
            <br/>
            <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                <tr>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>RECIBIDAS</b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>PAGADAS</b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>NEGADAS</b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>PENDIENTES</b></td>
                </tr>
                <tr>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">$ '.number_format($arrayMesesCompleto['recibi'], 0, '.', '.').'</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">$ '.number_format($arrayMesesCompleto['pagada'], 0, '.', '.').'</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">$ '.number_format($arrayMesesCompleto['negada'], 0, '.', '.').'</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">$ '.number_format($arrayMesesCompleto['restan'], 0, '.', '.').'</td>
                </tr>
                <tr>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>CANTIDAD / % </b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>CANTIDAD / % </b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>CANTIDAD / % </b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>CANTIDAD / % </b></td>
                </tr>
                <tr>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">'.number_format($arrayMesesCompleto['Rrecibi'], 0, '.', '.').' / 100 %</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">'. number_format($arrayMesesCompleto['Rpagada'], 0, '.', '.').' / '.number_format($_PorPagadas, 2).' %</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">'.number_format($arrayMesesCompleto['Rnegada'], 0, '.', '.').' / '.number_format($_PorNegadas,2).' %</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">'.number_format($arrayMesesCompleto['Rrestan'], 0, '.', '.').' / '.number_format($_PorRestant,2).' %</td>
                </tr>
            </table>';

             $content .= '
                    <br/>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:100%;text-align:justify;background-color:#dd4b39;color:white;"><b>DETALLE POR ENTIDAD</b></td>
                        </tr>
                    </table>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:35%;text-align:center;"><b>ADMINISTRADORA</b></td>
                            <td style="width:5%;text-align:center;"><b>#</b></td>
                            <td style="width:15%;text-align:center;"><b>RECIBIDAS</b></td>
                            <td style="width:15%;text-align:center;"><b>PAGADAS</b></td>
                            <td style="width:15%;text-align:center;"><b>NEGADAS</b></td>
                            <td style="width:15%;text-align:center;"><b>PENDIENTES</b></td>
                        </tr>';

            $_TotalRecibidas = 0;
            $_TotalPagadas = 0;
            $_TotalNegadas = 0;
            $_TotalRestante = 0;
            $_TotalNum = 0;

        
            $Inicio = $res.'-01-01';
            $final =  $res.'-12-31';

            $condicionX = 'inc_fecha_pago_nomina BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'].' AND inc_estado != 0 ';

            foreach($ips as $key => $ip){
                $_MoneyRecibidas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite != \'EMPRESA\' AND inc_ips_afiliado = '.$ip['ips_id']);
                $_MoneyPagadas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'PAGADA\' AND inc_ips_afiliado = '.$ip['ips_id']);
                $_MoneyNegadas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'SIN RECONOCIMIENTO\' AND inc_ips_afiliado = '.$ip['ips_id']);
                $_MoneyRestante = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.'  AND inc_estado_tramite NOT IN (\'SIN RECONOCIMIENTO\',\'EMPRESA\',\'PAGADA\') AND inc_ips_afiliado = '.$ip['ips_id']);;

                $_TotalRecibidas += $_MoneyRecibidas['total'];
                $_TotalPagadas   += $_MoneyPagadas['total'];
                $_TotalNegadas   += $_MoneyNegadas['total'];
                $_TotalRestante  += $_MoneyRestante['total'];
                
                $recibidas = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionX.' AND inc_estado_tramite != \'EMPRESA\' AND inc_ips_afiliado = '.$ip['ips_id']);
                $_TotalNum += $recibidas;
                $content .= '
                        <tr>
                            <td style="width:35%;text-align:justify;"><b>'.mb_strtoupper($ip['ips_nombre']).'</b></td>
                            <td style="width:5%;text-align:center;">'.$recibidas.'</td>
                            <td style="width:15%;text-align:center;">$ '.number_format($_MoneyRecibidas['total'], 0, '.', '.').'</td>
                            <td style="width:15%;text-align:center;">$ '.number_format($_MoneyPagadas['total'], 0, '.', '.').'</td>
                            <td style="width:15%;text-align:center;">$ '.number_format($_MoneyNegadas['total'], 0, '.', '.').'</td>
                            <td style="width:15%;text-align:center;">$ '.number_format($_MoneyRestante['total'], 0, '.', '.').'</td>
                        </tr>';
            }

            $content .= '
                    <tr>
                        <td style="width:35%;text-align:justify;"><b>TOTALES</b></td>
                        <td style="width:5%;text-align:center;"><b>'.$_TotalNum.'</b></td>
                        <td style="width:15%;text-align:center;"><b>$ '.number_format($_TotalRecibidas, 0, '.', '.').'</b></td>
                        <td style="width:15%;text-align:center;"><b>$ '.number_format($_TotalPagadas, 0, '.', '.').'</b></td>
                        <td style="width:15%;text-align:center;"><b>$ '.number_format($_TotalNegadas, 0, '.', '.').'</b></td>
                        <td style="width:15%;text-align:center;"><b>$ '.number_format($_TotalRestante, 0, '.', '.').'</b></td>
                    </tr>
                </table>';


            $condicionY = 'inc_fecha_pago_nomina BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'].' AND inc_estado = 0 ';

            $recibidasIn = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionY);
            $_MoneyRecibidasIn = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionY);

            $content .= '
                    <br/>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:100%;text-align:justify;background-color:#dd4b39;color:white;"><b>SOPORTES INCOMPLETOS</b></td>
                        </tr>
                    </table>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:20%;text-align:justify;">
                                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%" >
                                    <tr>
                                        <th align="center"><b>RECIBIDOS / #</b></th>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            $ '.number_format($_MoneyRecibidasIn['total'], 0, '.', '.').' / '.$recibidasIn.'
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:80%;text-align:justify;">
                                <table style="font-family:tahoma;font-size:9;  border:solid 1px #FFF;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                                    <tr>
                                        <td align="justify" width="30%"><b>ADMINISTRADORA</b></td>
                                        <td align="justify" width="20%"><b>VALOR</b></td>
                                        <td align="justify" width="50%"><b>MOTIVO</b></td>
                                    </tr>';
            $motivos = ModeloIncapacidades::mdlMostrarGroupAndOrder('mot_desc_v, count(inc_mot_rech_i) as total, SUM(inc_valor_pagado_eps) as sumX, ips_nombre, ips_id','gi_incapacidad JOIN gi_motivos_rechazo ON inc_mot_rech_i = mot_id_i JOIN gi_ips ON ips_id = inc_ips_afiliado ', 'inc_fecha_pago_nomina BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'].' AND inc_estado = 0  ', 'GROUP BY inc_mot_rech_i, ips_id ', 'ORDER BY mot_desc_v ASC', null);
            foreach($motivos as $motivo => $mot){
                $content .= '<tr>
                                <td align="justify">'.$mot['ips_nombre'].'</td>
                                <td align="justify">$ '.number_format($mot['sumX'], 0, '.', '.').'</td>
                                <td align="justify">'.$mot['mot_desc_v'].'</td>
                            </tr>';
            }
                                    
                    $content .= '</table>
                            </td>
                        </tr>
                    </table>';

            $condicionZ = 'inc_fecha_pago_nomina BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'].' AND inc_estado_tramite = \'SIN RECONOCIMIENTO\'';

            $recibidasNe = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionZ);
            $_MoneyRecibidasNe = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionZ);

            $content .= '
                    <br/>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:100%;text-align:justify;background-color:#dd4b39;color:white;"><b>INCAPACIDADES NEGADAS</b></td>
                        </tr>
                    </table>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:20%;text-align:justify;">
                                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%" >
                                    <tr>
                                        <th align="center"><b>RECIBIDOS / # </b></th>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            $ '.number_format($_MoneyRecibidasNe['total'], 0, '.', '.').' / '.$recibidasNe.'
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:80%;text-align:justify;">
                                <table style="font-family:tahoma;font-size:9;  border:solid 1px #FFF;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                                    <tr>
                                        <td align="justify" width="30%"><b>ADMINISTRADORA</b></td>
                                        <td align="justify" width="20%"><b>VALOR</b></td>
                                        <td align="justify" width="50%"><b>MOTIVO</b></td>
                                    </tr>';  

            $negadas = ModeloIncapacidades::mdlMostrarGroupAndOrder('incm_comentario_t, ips_nombre, inc_valor_pagado_eps','gi_incapacidades_comentarios JOIN gi_incapacidad ON inc_id = inm_inc_id_i JOIN gi_ips ON ips_id = inc_ips_afiliado', 'inc_fecha_pago_nomina BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'].' AND inc_estado_tramite = \'SIN RECONOCIMIENTO\' ', null, 'ORDER BY ips_nombre ASC', null);
            foreach($negadas as $nega => $neg){
                $valorN = 0;
                if($neg['inc_valor_pagado_eps'] != null){
                    $valorN = number_format($neg['inc_valor_pagado_eps'], 0, '.', '.');
                }
                $content .= '<tr>
                                <td align="justify">'.mb_strtoupper($neg['ips_nombre']).'</td>
                                <td align="justify">$ '.$valorN.'</td>
                                <td align="justify">'.$neg['incm_comentario_t'].'</td>
                            </tr>';
            }
                                    
                    $content .= '            
                                </table>
                            </td>
                        </tr>
                    </table>
                ';        

            $obj_pdf->writeHTML($content, true, false, false, false, '');
        }
      
        ob_start();
        $nombre='Reporte_pagos_x_mes_'.date("d-m-Y H-i-s").'.pdf';
        $obj_pdf->Output($nombre, 'I');
        $xlsData = ob_get_contents();
        ob_end_clean(); 

        $response =  array(
            'op' => 'ok',
            'file' => "data:application/pdf;base64,".base64_encode($xlsData)
        );

        echo json_encode($response);
    }

