<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- start page title -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
        <h1>
            Informe de Nomina     
        </h1>
       <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="reportes">Reportes</a></li>
            <li class="active">Reporte Cargue Nomina</li>
        </ol>
    </section>-->

 
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">INFORME DE FACTURAS</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Facturacion </li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    MIS FACTURAS
                </h5>
            </div>
            <div class="card-body">
                 <form id="formEnvioReporteFacturas" autocomplete="off" method="post" action="vistas/modulos/facturas/exportar.php">
                    <div class="row">
                    <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-3">
                            <div class="mb-3">
                                <input class="form-control flatpickr-input" type="text" name="NuevoFechaInicio" id="NuevoFechaInicio2" placeholder="Ingresar Fecha Inicial" required="true">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3">
                                 <input class="form-control flatpickr-input" type="text" name="NuevoFechaFinal" id="NuevoFechaFinal2" placeholder="Ingresar Fecha Final" required="true">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscarRangos_general"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="exportarFacturacion"><i class="fa fa-file-excel"></i>&nbsp;Exportar</button>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button"  data-bs-toggle="modal" data-bs-target="#modalAgregarFacturacion"  ><i class="fa fa-edit"></i>&nbsp;Editar datos</button>
                            </div>
                        </div>
                        
                    </div>    
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-body" id="resultados">
            </div>
        </div>
    </div>
</div>

<!-- Modal agregar Perfil -->
<div id="modalAgregarFacturacion" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" method="post" id="formularioFacturacion" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar Datos de Facturación</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row panel">
                            <div class="col-md-12 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Formato de carga</label>
                                    <input type="file" class="NuevoExell" required valor='0' name="NuevoIncapacidad"  id="NuevoIncapacidad">
                                    <p class="help-block">
                                        Peso maximo del archivo 4 MB
                                    </p>
                        
                                    <p>
                                        Descarga el Formato de carga para facturación <a href="vistas/formatos/FC_003.xlsx" download="FC_003.xlsx">aqui</a>
                                    </p>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>
                <div class="modal-footer">
                
                    <input type="hidden" name="EmpresaFacturada" value="<?php echo $_SESSION['cliente_id'];?>">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="button" id="btnEnvioFacuracion" class="btn btn-danger">Guardar Datos Factura</button>
                </div>
                
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>


<script type="text/javascript">


    /**Editar**/
    /*Esto es lo que reemlaza al datepicker*/ 
    /*Lo que tiene en #ya sabes que es un ID */
    var NuevoFechaFinal2 = flatpickr('#NuevoFechaFinal2', {
       altInput: true,
       altFormat: "d/m/Y",
       locale: "es",
       dateFormat: "Y-m-d",
       onChange: function(selectedDates, dateStr, instance){
         var minDate = dateStr;
         var fecha1 = moment($("#NuevoFechaInicio2").val());
         var fecha2 = moment(minDate);
         $("#EditarNumeroDias").val( (fecha2.diff(fecha1, 'days') + 1) + " Días");
       }
     });

    var NuevoFechaInicio2 = flatpickr('#NuevoFechaInicio2', {
         altInput: true,
         altFormat: "d/m/Y",
         locale: "es",
         dateFormat: "Y-m-d",
         onChange: function(selectedDates, dateStr, instance) {
             NuevoFechaFinal2.set("minDate", dateStr);
             NuevoFechaFinal2.setDate(dateStr);
         },
    });

    $("#btnExportarRangos_general").click(function(){

        var fechaInicio = $("#NuevoFechaInicio2").val();
        var fechaFinal  = $("#NuevoFechaFinal2").val();
        var paso = 0;
        if(fechaFinal.length < 0){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha final es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });

        }   

        if(fechaInicio.length < 1){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha Inicial es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
        }

        if(paso == 0){
            $("#formEnvioReporteFacturas").submit();
        }
    });

    $("#btnBuscarRangos_general").click(function(){
 
        var fechaInicio = $("#NuevoFechaInicio2").val();
        var fechaFinal  = $("#NuevoFechaFinal2").val();
        var paso = 0;
        if(fechaFinal.length < 0){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha final es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });

        }   

        if(fechaInicio.length < 1){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha Inicial es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
        }

        if(paso == 0){
            $.ajax({
                url    : 'vistas/modulos/facturas/generalpagosFacturas.php',
                type   : 'post',
                data   :{
                    fechaInicial  : fechaInicio,
                    fechaFinal    : fechaFinal,
                    cliente_id    : $("#session").val()
                },
                dataType : 'html',
                success : function(data){
                    $("#resultados").html(data);
                    
                }
            })
        }
    });

    /* validar los tipos de imagenes y/o archivos */
    $(".NuevoExell").change(function(){
        var imax = $(this).attr('valor');
        var imagen = this.files[0];
        console.log(imagen);
        /* Validar el tipo de imagen */
        if(imagen['type'] != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ){
            $(".NuevoExell").val('');
            alertify.error('El formato del archivo es xlsx');
        }else if(imagen['size'] > 4000000 ) {
            $(".NuevoExell").val('');
            alertify.error('El archivo debe pesar maximo 3 MB');
        }
    });

    $("#btnEnvioFacuracion").click(function(){
        if($("#NuevoIncapacidad").val() == ''){
            alertify.error("¡No se ha seleccionado un archivo!");
        }else{
            var form = $("#formularioFacturacion");
            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#formularioFacturacion")[0]);
            $.ajax({
                url: 'https://incapacidades.app/services/exports/incapacidades/importar',
                type  : 'post',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    alertify.success('Proceso terminado, total registros '+ data.total +', se cargaron con exito '+ data.exito +', fallaron '+ data.fallaron +', incapacidades no existentes '+ data.totalesAciertosNo );
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        }
    });

    $("#exportarFacturacion").click(function(){
        var url = 'index.php?exportar=exportarFacturacion';
        var fechaInicio = $("#NuevoFechaInicio2").val();
        var fechaFinal  = $("#NuevoFechaFinal2").val();
        var paso = 0;
        if(fechaFinal.length < 0){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha final es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });

        }   

        if(fechaInicio.length < 1){
            paso = 1;
            swal({
                title : "Error al buscar",
                text  : "La fecha Inicial es necesaria",
                type  : "error",
                confirmButtonText : "Cerrar"
            });
        }

        if(paso == 0){

            url += '&fechaInicio='+fechaInicio+'&fechaFinal='+fechaFinal;
            $.ajax({
                url    : url,
                type   : 'post',
                dataType : 'json',
                success : function(data){
                    var currentdate = new Date();
                    var fecha_hora = currentdate.getFullYear() + rellenar((currentdate.getMonth()+1), 2) +  rellenar(currentdate.getDate(), 2) + "_" + rellenar(currentdate.getHours(), 2) +  rellenar(currentdate.getMinutes(), 2) + rellenar(currentdate.getSeconds(), 2);
                    var $a = $("<a>");
                    $a.attr("href",data.file);
                    $("body").append($a);
                    $a.attr("download","consolidado_incapacidades_Facturadas_"+fecha_hora+".xlsx");
                    $a[0].click();
                    $a.remove();
                },
                beforeSend:function(){
                    $.blockUI({
                        message : '<h3>Un momento por favor....</h3>', 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            });
        }
    });

    function rellenar (str, max) {
        str = str.toString();
        return str.length < max ? rellenar("0" + str, max) : str;
    }
</script>