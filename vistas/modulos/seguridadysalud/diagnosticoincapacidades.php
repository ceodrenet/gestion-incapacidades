<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Informe de Seguridad y Salud 
            <small>Reporte de Diagnosticos Incapacidades</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="reportes">Reportes</a></li>
            <li class="active">Reporte de diagnosticos incapacidades</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-5">
                <div class="row">
                    <div class="col-lg-6 col-xs-12">
                    <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>
                                    <?php
                                        $item = null;
                                        $valor = null;
                                        if($_SESSION['cliente_id'] != 0){
                                            $item = 'inc_empresa';
                                            $valor = $_SESSION['cliente_id'];
                                            $pagadas = ControladorSeguridad::ctrGetMasRepetido($item, $valor);
                                        }else{
                                            $pagadas = ControladorSeguridad::ctrGetMasRepetido($item, $valor);
                                        }

                                        echo $pagadas['inc_diagnostico'];
                                    ?>
                                </h3>
                                <p>MAS FRECUENTE</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-android-done-all"></i>
                            </div>
                            <a href="incapacidades" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-6 col-xs-12">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>
                                    <?php
                                        $item = null;
                                        $valor = null;
                                        if($_SESSION['cliente_id'] != 0){
                                            $item = 'inc_empresa';
                                            $valor = $_SESSION['cliente_id']; 
                                        }

                                        $pagadas = ControladorSeguridad::ctrTotalValorIncapacidades($item, $valor);
                                        echo $pagadas;
                                    ?>
                                </h3>
                                <p>DIAGNOSTICOS</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-cash"></i>
                            </div>
                            <a href="incapacidades" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                     <div class="col-lg-12 col-xs-12">
                    <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>
                                    <?php


                                        $item = null;
                                        $valor = null;
                                        if($_SESSION['cliente_id'] != 0){
                                            $item = 'inc_empresa';
                                            $valor = $_SESSION['cliente_id'];
                                            $pagadas = ControladorSeguridad::ctrGetMasRepetido($item, $valor);
                                        }else{
                                            $pagadas = ControladorSeguridad::ctrGetMasRepetido($item, $valor);
                                        }


                                        $item = 'dia_codigo';
                                        $valor = $pagadas['inc_diagnostico'];
                                        $pagadas = ControladorSeguridad::ctrGetMasRepetidoNombre($item, $valor);

                                    ?>
                                </h3>
                                <p><?php echo $pagadas['id']." - ".$pagadas['name']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="col-md-7">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Grafico de Incapacidades por diagnostico</h3>
                    </div>
                    <div class="box-body">
                        <canvas id="myChart" style="height:250px"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Registro de pagos</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-hover table-condensed tblGeneralPagos">
                            <thead>
                                <tr>
                                    <th style="width: 10px;">#</th>
                                    <?php 
                                        if($_SESSION['cliente_id'] == 0){ 
                                            echo '<th>EMPRESA</th>';
                                        }
                                    ?>
                                    <th>EMPLEADO</th>
                                    <th>CÉDULA</th>
                                    <th>ADMINISTRADORA</th>                                    
                                    <th>FECHA INICIO</th>
                                    <th>FECHA FINAL</th>
                                    <th>DIAS INCAPACIDAD</th>
                                    <th>DIAGNOSTICO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $item = null;
                                    $valor = null;
                                    if($_SESSION['cliente_id'] != 0){
                                        $item = 'inc_empresa';
                                        $valor = $_SESSION['cliente_id'];
                                        $pagadas = ControladorSeguridad::ctrMostrarGeneralPagos_byCliente($item, $valor);
                                    }else{
                                        $pagadas = ControladorSeguridad::ctrMostrarGeneralPagos($item, $valor);
                                    }

                                    foreach ($pagadas as $key => $value) {
                                        echo '<tr>';
                                            echo '<td>'.($key+1).'</td>';
                                            if($_SESSION['cliente_id'] == 0){ 
                                                echo '<td>'.$value['emp_nombre'].'</td>';
                                            }
                                            echo '<td>'.$value['emd_nombre'].'</td>';
                                            echo '<td>'.$value['inc_cc_afiliado'].'</td>';
                                            echo '<td>'.$value['ips_nombre'].'</td>';
                                            echo '<td>'.explode(' ', $value['inc_fecha_inicio'])[0].'</td>';
                                            echo '<td>'.explode(' ', $value['inc_fecha_final'])[0].'</td>';
                                            echo '<td>'.dias_transcurridos($value['inc_fecha_inicio'], $value['inc_fecha_final']) .'</td>';
                                            echo '<td>'.$value['inc_diagnostico'].'</td>';
                                        echo '</tr>';
 
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    $(function(){
        crearBarrasporPagar();
    });

function crearBarrasporPagar(){
    <?php

        $respuestaEmpresa = ControladorSeguridad::ctrContarValorIncapacidades($item, 
            $valor);
        $label = '';
        $Qdias = '';
        $colors = '';
        foreach ($respuestaEmpresa as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */
            $label .= "\"".$value['inc_diagnostico']."\" , ";
            $Qdias .= $value['total']." , ";
            $colors .= "getRandomColor(),";
        }
    ?>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php echo $label; ?>],
            datasets: [
                {
                    label: 'Incapacidades con este Diagnostico',
                    data: [<?php echo $Qdias; ?>],
                    backgroundColor: "#f1c40f"
                }
            ]
        }
    });
}


function getRandomColor() {
   return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
}

</script>

<?php
    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }

?>
