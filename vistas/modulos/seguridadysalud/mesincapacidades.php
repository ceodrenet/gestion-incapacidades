<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">SEGURIDAD Y SALUD ESTADISTICAS</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Incapacidades Por mes</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    REPORTE INCAPACIDADES POR MES 
                </h5>
            </div>
            <div class="card-body">
                <form id="ExportarReporteMensual"autocomplete="off" method="post">
                    <div class="row">
                        <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-3">
                            <div class="mb-3">
                                <input class="form-control flatpickr-input" type="text" name="txtFechaAnual" id="txtFechaAnual" placeholder="Año a verficar" required="true">
                            </div>
                        </div>
                        
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscarIncapacidades"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>
                        <div class="col-md-2" id="divExportarExcel" style="display: none;">
                                <label style="visibility: hidden;">Expotar</label>
                                <button class="btn btn-primary btn-block" type="button" id="btnBuscarExportarInformacion"><i class="fa fa-file-excel-o"></i>&nbsp;Exportar</button>
                          
                        </div>
                     
                    </div>    
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12" id="resultados">
        
    </div>
</div>

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- chart js -->
<script src="vistas/assets/assets/libs/chart.js/Chart.bundle.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
</script>
<script type="text/javascript">
    $(function(){

        $("#txtFechaAnual").flatpickr({
            autoclose: true,
            todayHighlight: true,
            viewMode: "years", 
            minViewMode: "years",
            format: "yyyy"
        });

        $("#btnBuscarIncapacidades").click(function(){
            var fechaFinal = $("#txtFechaAnual").val();
            var paso = 0;
            if(fechaFinal.length < 0){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "La año a revisar es necesario",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });
            }   

            if(paso == 0){
                $.ajax({
                    url    : 'vistas/modulos/seguridadysalud/incapacidadesEstadisticas.php',
                    type   : 'post',
                    data   :{
                        fechaFinal  : fechaFinal,
                        cliente_id    : $("#session").val()
                    },
                    dataType : 'html',
                    success : function(data){
                        $("#resultados").html(data);
                        $("#divExportarExcel").show();
                    },
                    beforeSend:function(){
                        $.blockUI({
                            message : '<h3>Un momento por favor....</h3>', 
                            css: { 
                                border: 'none', 
                                padding: '1px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff' 
                            } 
                        }); 
                    },
                    complete:function(){
                        $.unblockUI();
                    }
                })
            }
        });

        $("#btnBuscarExportarInformacion").click(function(){
            var fechaFinal = $("#txtFechaAnual").val();
            var paso = 0;
            if(fechaFinal.length < 0){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "La año a revisar es necesario",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });
            }   

            if(paso == 0){
                $.ajax({
                    url    : 'ajax/exportar.ajax.php',
                    type   : 'post',
                    data   :{
                        fechaFinal  : fechaFinal,
                        cliente_id    : $("#session").val(),
                        exportarIncapacidadesAnualesAjax : true
                    },
                    dataType : 'json',
                    success : function(data){
                        var currentdate = new Date();
                        var fecha_hora = currentdate.getFullYear() + rellenar((currentdate.getMonth()+1), 2) +  rellenar(currentdate.getDate(), 2) + "_" + rellenar(currentdate.getHours(), 2) +  rellenar(currentdate.getMinutes(), 2) + rellenar(currentdate.getSeconds(), 2);
                        var $a = $("<a>");
                        $a.attr("href",data.file);
                        $("body").append($a);
                        $a.attr("download","incapacidades_anuales_meses_"+fecha_hora+".xlsx");
                        $a[0].click();
                        $a.remove();
                    },
                    beforeSend:function(){
                        $.blockUI({
                            message : '<h3>Un momento por favor....</h3>', 
                            css: { 
                                border: 'none', 
                                padding: '1px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff' 
                            } 
                        }); 
                    },
                    complete:function(){
                        $.unblockUI();
                    }
                })
            }
        })
    });
    function rellenar (str, max) {
        str = str.toString();
        return str.length < max ? rellenar("0" + str, max) : str;
    }
    
</script>