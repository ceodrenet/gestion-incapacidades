<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
    require_once '../../../modelos/dao.modelo.php';

    require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../modelos/incapacidades.modelo.php';  
    require_once '../../../controladores/empleados.controlador.php';
    require_once '../../../modelos/empleados.modelo.php';   
    require_once '../../../controladores/diagnostico.controlador.php';
    require_once '../../../modelos/diagnostico.modelo.php'; 
    require_once '../../../modelos/tesoreria.modelo.php';   
    require_once '../../../controladores/tesoreria.controlador.php';
    require_once '../../../modelos/seguridad.modelo.php';   
    require_once '../../../controladores/seguridad.controlador.php';



    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }

    $campos = "count(*) as estado";
    $tabla = "gi_incapacidad";
    if($_SESSION['cliente_id'] != 0){
        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' ANd  '".$_POST['fechaFinal']."'";
    }else{
        $condiciones = "inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' ANd  '".$_POST['fechaFinal']."'";
    }

    $respuestaX = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

?>

<div class="row">
    <div class="col-md-6">
        <div class="card">
           
            <div class="card border border-danger">
                <div class="card-header bg-transparent border-danger">
                    
                <h5 class="my-0 text-danger">Top 10 Colaboradores con mas d&iacute;as de incapacidad</h5>
            </div>
                    <table class="table table-hover table-condensed tblGeneralPagos">
                        <thead>
                            <tr>
                                <th style="width: 10px;">#</th>
                                <th>COLABORADOR</th>
                                <th># INCAPACIDADES</th>
                                <th>TOTAL DÍAS INC.</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $item = null;
                                $valor = null;
                                if($_SESSION['cliente_id'] != 0){

                                    $item = 'inc_empresa';
                                    $valor = $_SESSION['cliente_id'];
                                    $ctrGetTopTenIncapacidades = ControladorIncapacidades::ctrGetTopTenIncapacidades($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                                }else{

                                    $ctrGetTopTenIncapacidades = ControladorIncapacidades::ctrGetTopTenIncapacidades($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                                }


                                foreach ($ctrGetTopTenIncapacidades as $key => $value) {
                                    $dato = ControladorEmpleados::ctrMostrarEmpleados('emd_cedula', $value['inc_cc_afiliado'] );
                                    $item = 'inc_cc_afiliado';
                                    $valor = $value['inc_cc_afiliado'];
                                // $datoDias = ControladorIncapacidades::ctrGetTopTenFechasSinLimite($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                                    $totalDIas = $value['total_dias'];
                                    //foreach ($datoDias as $keyX => $valueres) {
                                    //   $totalDIas += dias_transcurridos($valueres['inc_fecha_inicio'] , $valueres['inc_fecha_final']);
                                    //}
                                    echo '<tr>';
                                        echo '<td>'.($key+1).'</td>';
                                        echo '<td>'.$dato['emd_nombre'].'</td>';
                                        echo '<td>'.$value['total'].'</td>';
                                        echo '<td>'.$totalDIas.'</td>';
                                    echo '</tr>';

                                }
                            ?>
                        </tbody>
                    </table>
            </div>
           
        </div>
    </div>

    <div class="col-md-6">
        <!--<div class="box">-->
            <div class="card border border-danger">
            
            <div class="card-header bg-transparent border-danger">
            <h5 class="my-0 text-danger">Top 10 Colaboradores con mas incapacidades</h5>
                            </div>
            <div class="card-body table-responsive"> 
                <table class="table table-hover table-condensed tblGeneralPagos">
                    <thead>
                        <tr>
                            <th style="width: 10px;">#</th>
                            <th>COLABORADOR</th>
                            <th># INCAPACIDADES</th>
                            <th>TOTAL DÍAS INC.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $item = null;
                            $valor = null;
                            if($_SESSION['cliente_id'] != 0){

                                $item = 'inc_empresa';
                                $valor = $_SESSION['cliente_id'];
                                $ctrGetTopTenIncapacidades = ControladorIncapacidades::ctrGetTopTenIncapacidadesByday($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                            }else{

                                $ctrGetTopTenIncapacidades = ControladorIncapacidades::ctrGetTopTenIncapacidadesByday($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                            }


                            foreach ($ctrGetTopTenIncapacidades as $key => $value) {
                                $dato = ControladorEmpleados::ctrMostrarEmpleados('emd_cedula', $value['inc_cc_afiliado'] );
                                $item = 'inc_cc_afiliado';
                                $valor = $value['inc_cc_afiliado'];
                               // $datoDias = ControladorIncapacidades::ctrGetTopTenFechasSinLimite($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                                $totalDIas = $value['total_dias'];
                                //foreach ($datoDias as $keyX => $valueres) {
                                 //   $totalDIas += dias_transcurridos($valueres['inc_fecha_inicio'] , $valueres['inc_fecha_final']);
                                //}
                                echo '<tr>';
                                    echo '<td>'.($key+1).'</td>';
                                    echo '<td>'.$dato['emd_nombre'].'</td>';
                                    echo '<td>'.$value['total'].'</td>';
                                     echo '<td>'.$totalDIas.'</td>';
                                echo '</tr>';

                            }
                        ?>
                    </tbody>
                </table>
                </div>
            </div>
       <!-- </div>-->
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Estadistica Origen de Incapacidad</h3>
            </div>
            <div class="box-body">
                <canvas id="pieChart_2" style="height:454px"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Estadistica pagos por mes</h3>
            </div>
            <div class="box-body">
                <canvas id="pieChart_3" style="height:454px"></canvas>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Estadistica, d&iacute;as con mas incapacidades</h3>
            </div>
            <div class="box-body">
                <canvas id="pieChart_4" style="height:454px"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Estado de los Colaboradores</h3>
            </div>
            <div class="box-body">
                <canvas id="pieChart_5" style="height:454px"></canvas>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>REPETICIONES</th>
                            <th>C&Oacute;DIGO</th>
                            <th>DESCRIPCI&Oacute;N</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $item = null;
                            $valor = null;
                            if($_SESSION['cliente_id'] != 0){
                                $item = 'inc_empresa';
                                $valor = $_SESSION['cliente_id'];
                            }
                            $respuestaEmpresa = ControladorSeguridad::ctrVerReportesTesoreria($item, 
                                $valor, $_POST['fechaInicial'], $_POST['fechaFinal'], 'contarValorIncapacidades');
                            $label = '';
                            $Qdias = '';
                            $colors = '';
                            foreach ($respuestaEmpresa as $key => $value) {
                                if($value['inc_diagnostico'] != '0'){
                                    $item = 'dia_codigo';
                                    $valuesx = $value['inc_diagnostico'];
                                    $respuesta = ControladorDiagnostico::ctrMostrarDiagnostico($item, $valuesx);
                                    echo "<tr>";
                                        echo "<td>".$value['total']."</td>";
                                        echo "<td>".$value['inc_diagnostico']."</td>";
                                        echo "<td>".$respuesta['name']."</td>";
                                    echo "</tr>";
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th style="width: 10px;">#</th>
                            <th>COLABORADOR</th>
                            <th># INCAPACIDADES</th>
                            <th>TOTAL DÍAS INC.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $item = null;
                            $valor = null;
                            if($_SESSION['cliente_id'] != 0){
                                $item = 'inc_empresa';
                                $valor = $_SESSION['cliente_id'];
                            }
                            $respuestaEmpresa = ControladorIncapacidades::ctrGetTopTenIncapacidadesByMaternidad($item, 
                                $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                            foreach ($respuestaEmpresa as $key => $value) {
                                $dato = ControladorEmpleados::ctrMostrarEmpleados('emd_cedula', $value['inc_cc_afiliado'] );
                                $item = 'inc_cc_afiliado';
                                $valor = $value['inc_cc_afiliado'];
                               // $datoDias = ControladorIncapacidades::ctrGetTopTenFechasSinLimite($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                                $totalDIas = $value['total_dias'];
                                //foreach ($datoDias as $keyX => $valueres) {
                                 //   $totalDIas += dias_transcurridos($valueres['inc_fecha_inicio'] , $valueres['inc_fecha_final']);
                                //}
                                echo '<tr>';
                                    echo '<td>'.($key+1).'</td>';
                                    echo '<td>'.$dato['emd_nombre'].'</td>';
                                    echo '<td>'.$value['total'].'</td>';
                                     echo '<td>'.$totalDIas.'</td>';
                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Incapacidades por genero</h3>
            </div>
            <div class="box-body">
                <canvas id="pieChart_7" style="height:454px"></canvas>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        
        creatEstadisticaOrigenes_inc();
        creatLineChartValoresPagados();
        creatLineChartDiasConMasInca();
        creatLineChartEmpleadosEstat();
        creatEstadisticaGeneroEmplea();

    })

    function creatEstadisticaOrigenes_inc(){
        <?php 
            $item = null;
            $valor = null;
            if($_SESSION['cliente_id'] != 0){
                $item = 'inc_empresa';
                $valor = $_SESSION['cliente_id'];
                $ctrGetTopTenIncapacidades = ControladorIncapacidades::ctrGetConsolidadoDeEstados($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
            }else{

                $ctrGetTopTenIncapacidades = ControladorIncapacidades::ctrGetConsolidadoDeEstados($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
            }

            $nombres = '';
            $valores = '';
            $valoreP = '';
            $i = 0;

            foreach ($ctrGetTopTenIncapacidades as $key => $value) {
                if($value['inc_origen'] != '0'){
                    if($i == 0){
                        $nombres .= "'".$value['inc_origen']."'";
                        $valores .= "'".$value['total']."'"; 
                    }else{
                        $nombres .= " , '".$value['inc_origen']."'";
                        $valores .= " , '".$value['total']."'"; 
                    }
                    $i++;
                }
                
            }
        ?>

        var densityCanvas = document.getElementById("pieChart_2").getContext('2d');

        var densityData = {
            label: 'Incapacidad Origen',
            data: [<?php echo $valores;?>],
            borderColor:'#ff6384',
            backgroundColor: [
                "#f1c40f",
                "#e74c3c",
                "#34495e",
                "#2ecc71",
                "#3498db",
                "#95a5a6",
                "#9b59b6",
                "#f1c40f",
                "#e74c3c",
                "#34495e"
            ],
            borderWidth: 0,
            hoverBorderWidth: 0,
            fill: false
        };


        var barChart = new Chart(densityCanvas, {
            type: 'horizontalBar',
            data: {
                labels: [<?php echo $nombres; ?>],
                datasets: [densityData],
            }
        });
    }

    function creatLineChartValoresPagados(){
        <?php 
            
            if($_SESSION['cliente_id'] != 0){
                $item = 'inc_empresa';
                $valor = $_SESSION['cliente_id'];
                $respuesta = ControladorIncapacidades::ctrMostrarLastSixMonth_2($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
            }else{
                $item = null;
                $value = null;
                $respuesta = ControladorIncapacidades::ctrMostrarLastSixMonth_2($item, $value, $_POST['fechaInicial'], $_POST['fechaFinal']);
            }

            $arrayMont = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');

            $labels = '';
            $datos = '';
            $colors = '';

            foreach ($respuesta as $key => $value) {

                if( $value['valor'] != '' &&  $value['valor'] != null){
                    if( $labels == ''){
                        $labels = '"'.$arrayMont[$value['MES']-1].'-'.$value['ANO'].'"';
                        $datos   = $value['valor'];
                        $colors .= "getRandomColor()";
                    }else{
                        $labels .= ' , "'.$arrayMont[$value['MES']-1].'-'.$value['ANO'].'"';
                        $datos   .= " , ".$value['valor'];
                        $colors .= ",getRandomColor()";
                    }
                }
                
            }
        ?>

        var densityCanvas = document.getElementById("pieChart_3").getContext('2d');

        var densityData = {
            label: 'Valor Pagado',
            data: [<?php echo $datos; ?>],
            borderColor:'#36a2eb',
            backgroundColor : '#36a2eb',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var barChart = new Chart(densityCanvas, {
            type: 'line',
            data: {
                labels: [<?php echo $labels; ?>],
                datasets: [densityData],
            },
            options: {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor Pagado : $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (value) {
                                return addCommas(value)
                            }
                        }
                    }]
                }
            }
        });
    }

    function creatLineChartDiasConMasInca(){
        <?php 
            $whereCliente = '';
            if($_SESSION['cliente_id'] != 0){
                $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
            }

            $campos = "count(*) as total, (ELT(WEEKDAY(inc_fecha_inicio) + 1, 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo')) AS DIA_SEMANA";
            $tabla_ = "gi_incapacidad";
            $condiciones = $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['NuevoFechaInicio_CE2']."' ANd  '".$_POST['fechaFinal']."'";
            $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla_,$condiciones, 'GROUP BY DIA_SEMANA', 'ORDER BY total DESC');
            $labels = '';
            $data = '';
            $i= 0;
            foreach ($respuesta as $key => $value) {
                if($i == 0){
                    $labels = '"'.$value['DIA_SEMANA'].'"';
                    $data = $value['total'];
                }else{
                    $labels .= ' , "'.$value['DIA_SEMANA'].'"';
                    $data .= ' , '.$value['total'];
                }
                $i++;
            }

        ?>

        var densityCanvas = document.getElementById("pieChart_4").getContext('2d');

        var densityData = {
            label: '# de incapacidades',
            data: [<?php echo $data; ?>],
            borderColor:'#36a2eb',
            backgroundColor : '#36a2eb',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var barChart = new Chart(densityCanvas, {
            type: 'line',
            data: {
                labels: [<?php echo $labels; ?>],
                datasets: [densityData],
            }
        });
    }

    function creatLineChartEmpleadosEstat(){
        <?php 
            $labels = '';
            $data = '';
            $whereCliente = '';
            if($_SESSION['cliente_id'] != 0){
                $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
            }

            $campos = "COUNT(*) as Total";
            $tabla_ = "gi_empleados";
            $condiciones = $whereCliente;
            $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos,$tabla_,$condiciones);
            $labels .= "'EMPLEADOS'";
            $data .= $respuesta['Total'];


            $camposx = "COUNT(*) as Totalx";
            $tablax_ = "gi_empleados";
            $condicionesx = $whereCliente.' emd_estado = 1;';
            $respuestax = ModeloTesoreria::mdlMostrarUnitario($camposx,$tablax_,$condicionesx);
            
            $labels .= " , 'ACTIVOS'";
            $data .= " , ".$respuestax['Totalx'];


            $camposy = "COUNT(*) as Totaly";
            $tablay_ = "gi_empleados";
            $condicionesy = $whereCliente.' emd_estado != 1;';
            $respuestay = ModeloTesoreria::mdlMostrarUnitario($camposy,$tablay_,$condicionesy);
            $labels .= " , 'RETIRADOS'";
            $data .= " , ".$respuestay['Totaly'];


            $camposz = "COUNT(*) as Totalz";
            $tablaz_ = "gi_empleados";
            $condicionesz = $whereCliente.' emd_afp_id = 123;';
            $respuestaz = ModeloTesoreria::mdlMostrarUnitario($camposz,$tablaz_,$condicionesz);
            $labels .= " , 'APRENDICES'";
            $data .= " , ".$respuestaz['Totalz'];


            $campost = "COUNT(*) as Totalt";
            $tablat_ = "gi_empleados";
            $condicionest = $whereCliente.' emd_afp_id = 124;';
            $respuestat = ModeloTesoreria::mdlMostrarUnitario($campost,$tablat_,$condicionest);
            $labels .= " , 'PENSIONADOS'";
            $data .= " , ".$respuestat['Totalt'];

        ?>

        var densityCanvas = document.getElementById("pieChart_5").getContext('2d');

        var densityData = {
            label: '# De Colaboradores',
            data: [<?php echo $data; ?>],
            borderColor:'#36a2eb',
            backgroundColor : '#36a2eb',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var barChart = new Chart(densityCanvas, {
            type: 'bar',
            data: {
                labels: [<?php echo $labels; ?>],
                datasets: [densityData],
            }
        });
    }

    function creatEstadisticaDiagnosticos(){
        <?php

            $item = null;
            $valor = null;
            if($_SESSION['cliente_id'] != 0){
                $item = 'inc_empresa';
                $valor = $_SESSION['cliente_id'];
            }
            $respuestaEmpresa = ControladorSeguridad::ctrVerReportesTesoreria($item, 
                $valor, $_POST['fechaInicial'], $_POST['fechaFinal'], 'contarValorIncapacidades');
            $label = '';
            $Qdias = '';
            $colors = '';
            foreach ($respuestaEmpresa as $key => $value) {
                /* primero traemos las empresas que vamos a mostrar osea todas */
                /* traemos las de 15 dias */
                if($value['inc_diagnostico'] != '0'){
                    $label .= "\"".$value['inc_diagnostico']."\" , ";
                    $Qdias .= $value['total']." , ";
                    $colors .= "getRandomColor(),";
                }
            }
        ?>
        var ctx = document.getElementById("pieChart_6").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [<?php echo $label; ?>],
                datasets: [
                    {
                        label: '# Incapacidades con este Diagnostico',
                        data: [<?php echo $Qdias; ?>],
                        backgroundColor: "#f1c40f"
                    }
                ]
            }
        });
    }


    function creatEstadisticaGeneroEmplea(){
        <?php
            $whereCliente = '';
            if($_SESSION['cliente_id'] != 0){
                $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." ";
            }

            $campos = "count(*) as total, emd_genero ";
            $tabla_ = "gi_incapacidad JOIN gi_empleados ON emd_id = inc_emd_id ";
            $codici = $whereCliente;

            $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla_,$codici, 'GROUP BY emd_genero', 'ORDER BY emd_genero DESC');
            $labels = '';
            $data = '';
            $i= 0;
            foreach ($respuesta as $key => $value) {
                $nombreGenero = 'No registra';
                if($value['emd_genero'] != null){
                    $nombreGenero = $value['emd_genero'];
                }

                if($i == 0){
                    $labels = '"'.$nombreGenero.'"';
                    $data = $value['total'];
                }else{
                    $labels .= ' , "'.$nombreGenero.'"';
                    $data .= ' , '.$value['total'];
                }
                $i++;
            }
        ?>

        var densityCanvas = document.getElementById("pieChart_7").getContext('2d');

        var densityData = {
            label: '# Incapacidades',
            data: [<?php echo $data; ?>],
            borderColor:'#36a2eb',
            backgroundColor : '#36a2eb',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var barChart = new Chart(densityCanvas, {
            type: 'line',
            data: {
                labels: [<?php echo $labels; ?>],
                datasets: [densityData],
            }
        });
    }


    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function scaleLabel (valuePayload) {
        return Number(valuePayload.value).toFixed(2).replace('.',',') + '$';
    }

    function getRandomColor() {
       return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
    }
</script>