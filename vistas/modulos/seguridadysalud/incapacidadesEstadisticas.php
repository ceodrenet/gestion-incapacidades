<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
    require_once '../../../modelos/dao.modelo.php';
    
	require_once '../../../controladores/incapacidades.controlador.php';
	require_once '../../../modelos/incapacidades.modelo.php';
    require_once '../../../modelos/tesoreria.modelo.php';

    $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

    $whereCliente = '';
    if($_POST['cliente_id'] != 0){
        $whereCliente =  "inc_empresa = ".$_POST['cliente_id']." AND ";
    }

    $campos = 'count(*) as total , MONTH(inc_fecha_inicio) as mes';
    $tabla  = 'gi_incapacidad';
    $condicion =  $whereCliente." YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." ";
    $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');
                    
?>

<div class="row">
	<div class="col-md-6">
		<div class="card border border-danger">
		    <div class="card-header bg-transparent border-danger">
		        <h5 class="my-0 text-danger">INCAPACIDADES MENSUALES</h5>
		    </div>
		    <div class="box-body">
		    	<table width="100%" class="table table-hover table-bordered" id="tableMensual">
		    		<thead>
		    			<tr>
		    				<th style="width: 10px;">#</th>
		    				<th>MES</th>
		    				<th># Incapacidades</th>
                            <th># E-G</th>
                            <th># L-M</th>
                            <th># L-P</th>
                            <th># A-L</th>
                            <th># A-T</th>
		    			</tr>
		    		</thead>
		    		<tbody>
		    			<?php 
		    				$nombres = '';
		    				$valores = '';
                            $valoreP = '';
                            $i = 0;
                            $totalIncapacidades = 0;
                            $totalIncaEnferGene = 0;
                            $totalIncaLicenMate = 0;
                            $totalIncaLicenPate = 0;
                            $totalIncaAcciLabor = 0;
                            $totalIncaAccoTrans = 0;


		    				foreach ($incapacidades as $key => $value) {
                                $numero = $value['mes'];
                                
                                
                                $tabla  = 'gi_incapacidad';
                                $campos = 'count(*) as total';
                                $condicion =  $whereCliente." MONTH(inc_fecha_inicio) = ".$value['mes']." AND YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." AND inc_origen = 'ENFERMEDAD GENERAL' ";
                                $eg = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
                                
                                $condicion =  $whereCliente." MONTH(inc_fecha_inicio) = ".$value['mes']." AND YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." AND inc_origen = 'LICENCIA DE MATERNIDAD' ";
                                $lm = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
                                
                                $condicion =  $whereCliente." MONTH(inc_fecha_inicio) = ".$value['mes']." AND YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." AND inc_origen = 'LICENCIA DE PATERNIDAD' ";
                                $lp = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

                                $condicion =  $whereCliente." MONTH(inc_fecha_inicio) = ".$value['mes']." AND YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." AND inc_origen = 'ACCIDENTE LABORAL' ";
                                $al = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

                                $condicion =  $whereCliente." MONTH(inc_fecha_inicio) = ".$value['mes']." AND YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." AND inc_origen = 'ACCIDENTE TRANSITO' ";
                                $at = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

                                $condicion = $whereCliente." MONTH(inc_fecha_inicio) = ".$value['mes']." AND YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." AND inc_estado_tramite = 'PAGADA' ";
                                $val = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

                                $totalIncapacidades += $value['total'];
                                $totalIncaEnferGene += $eg['total'];
                                $totalIncaLicenMate += $lm['total'];
                                $totalIncaLicenPate += $lp['total'];
                                $totalIncaAcciLabor += $al['total'];
                                $totalIncaAccoTrans += $at['total'];


		    					echo "<tr>";
		    						echo "<td>".($key +1)."</td>";
		    						echo "<td>".$month[$numero -1]."</td>";
                                    echo "<td>".$value['total']."</td>";
                                    echo "<td>".$eg['total']."</td>";
                                    echo "<td>".$lm['total']."</td>";
                                    echo "<td>".$lp['total']."</td>";
                                    echo "<td>".$al['total']."</td>";
                                    echo "<td>".$at['total']."</td>";
		    					echo "</tr>";
		    					if($i == 0){
		    						$nombres .= "'".$month[$numero -1]."'";
		    						$valores .=	"'".$value['total']."'"; 
                                    $valoreP .= "'".$val['total']."'"; 
		    					}else{
		    						$nombres .= " , '".$month[$numero -1]."'";
		    						$valores .=	" , '".$value['total']."'"; 
                                    $valoreP .= ", '".$val['total']."'"; 
		    					}
		    					$i++;
                            }
                            echo "<tr>";
                                echo "<td></td>";
                                echo "<td>Total</td>";
                                echo "<td>".$totalIncapacidades."</td>";
                                echo "<td>".$totalIncaEnferGene."</td>";
                                echo "<td>".$totalIncaLicenMate."</td>";
                                echo "<td>".$totalIncaLicenPate."</td>";
                                echo "<td>".$totalIncaAcciLabor."</td>";
                                echo "<td>".$totalIncaAccoTrans."</td>";
                            echo "</tr>";
		    			?>
		    		</tbody>
		    	</table>
		    </div>
		</div>
	</div>
	<div class="col-md-6">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    INCAPACIDADES X MESES GRAFICO 
                </h5>
            </div>
            <div class="box-body">
                <canvas id="pieChart_2" style="height:334px"></canvas>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
	$(function(){
		crearDonutChartTotal_2();
	});

    function crearDonutChartTotal_2(){


        var densityCanvas = document.getElementById("pieChart_2").getContext('2d');

        var densityData = {
            label: 'Incapacidades',
            data: [<?php echo $valores;?>],
            borderColor:'#ff6384',
            backgroundColor : '#ff6384',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var densityData_pagadas = {
            label: 'Pagadas',
            data: [<?php echo $valoreP; ?>],
            borderColor:'#36a2eb',
            backgroundColor : '#36a2eb',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var barChart = new Chart(densityCanvas, {
            type: 'line',
            data: {
                labels: [<?php echo $nombres; ?>],
                datasets: [densityData, densityData_pagadas],
            }
        });
    }
</script>