<?php
    require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
	require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../modelos/dao.modelo.php';
	require_once '../../../modelos/incapacidades.modelo.php';

?>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    CONSOLIDADO DE INCAPACIDADES
                </h5>
            </div>
            <div class="card-body">
                <table id="tbl_Incapacidades" class="table table-bordered table-striped dt-responsive tablas " style="width: 100%;">
                    <thead>
                        <tr>
                            <?php if($_POST['cliente_id'] == 0){ ?>
                            <th style="width: 15%;">Empresa</th>
                            <?php }else{ ?>
                             <th style="width: 15%;">Cédula</th>
                            <?php }?>
                            <th style="width: 20%;">Nombre</th>
                            <th style="width: 20%;">Administradora</th>
                            <th style="width: 5%;">Diagnostico</th>
                            <th style="width: 10%;">Fecha Inicial</th>
                            <th style="width: 10%;">Fecha Final</th>
                            <th style="width: 10%;"># Días</th>
                            <th style="width: 5%;">Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            function dias_transcurridos($fecha_i, $fecha_f)
                            {
                                $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
                                $dias   = abs($dias); $dias = floor($dias);     
                                return $dias + 1;
                            }

                            if($_POST['cliente_id'] != 0){
                                $item = $_POST['cliente_id'];
                                $valor = $_POST['fechaInicial'];
                                $valor2 = $_POST['fechaFinal'];
                                $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_Consolidado($item, $valor, $valor2);
                            }else{

                                $item = null;
                                $valor = $_POST['fechaInicial'];
                                $valor2 = $_POST['fechaFinal'];
                                $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_Consolidado($item, $valor, $valor2);
                            }

                            //var_dump($incapacidades);
                            foreach ($incapacidades as $key => $value) {
                                echo ' 
                                <tr>';
                                if($_POST['cliente_id'] == 0){ 
                                   echo '<td class="text-uppercase">'.$value["emp_nombre"].'</td>';
                                }else{
                                    echo '<td class="text-uppercase">'.$value["emd_cedula"].'</td>';
                                }
                                echo '
                                    <td class="text-uppercase">'.$value["emd_nombre"].'</td>';
                                
                                if($value['inc_clasificacion'] != 4){
                                    if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                                        echo '<td class="text-uppercase">'.($value["inc_arl_afiliado"]).'</td>'; 
                                    }else{
                                        echo '<td class="text-uppercase">'.($value["inc_ips_afiliado"]).'</td>'; 
                                    }
                                }else{
                                    echo '<td class="text-uppercase">'.($value["inc_afp_afiliado"]).'</td>';    
                                }

                                echo '  <td class="text-uppercase">'.$value["inc_diagnostico"].'</td>
                                    <td class="text-uppercase">'.explode(' ',$value["inc_fecha_inicio"])[0].'</td>
                                    <td class="text-uppercase">'.explode(' ',$value["inc_fecha_final"])[0].'</td>
                                    <td class="text-uppercase">'.dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]).'</td>
                                    <td class="text-uppercase">'.$value["inc_estado_tramite"].'</td>
                                </tr>'; 
                            }

                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
	 $('#tbl_Incapacidades').DataTable({
        "lengthMenu": [
            [10, 25, 50, 100, 200, -1], 
            [10, 25, 50, 100, 200, "Todos"]
        ],
        "columnDefs": [
            {
                "targets": 4,
                "render": $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD/MM/YYYY')
            } ,
            {
                "targets": 5,
                "render": $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD/MM/YYYY')
            } 
        ],
        "language" : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        } 
    });
</script>