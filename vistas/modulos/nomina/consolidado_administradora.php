<?php
    require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
	require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../modelos/dao.modelo.php';
	require_once '../../../modelos/incapacidades.modelo.php';
    require_once '../../../modelos/tesoreria.modelo.php';

?>
<div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    CONSOLIDADO INCAPACIDADES POR ADMINISTRADORA
                </h5>
            </div>
            <div class="card-body">
                <table id="tbl_Incapacidades" class="table table-bordered table-striped" style="width: 100%;">
                    <thead>
                        <tr>
                            <?php if($_POST['cliente_id'] == 0){ ?>
                            <th style="width: 10%;">Empresa</th>
                            <?php }else{ ?>
                             <th style="width: 10%;">Cédula</th>
                            <?php }?>
                            <th style="width: 21%;">Nombre</th>
                            <th style="width: 5%;">Origen</th>
                            <th style="width: 7%;">Fecha Inicial</th>
                            <th style="width: 7%;">Fecha Final</th>
                            <th style="width: 5%;">Estado</th>
                            <th style="width: 8%;">Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $whereCliente = '';
                            if($_POST['cliente_id'] != 0){
                                $whereCliente =  "inc_empresa = ".$_POST['cliente_id']." AND ";
                            }

                            $whereAdministradora ='';
                            if($_POST['tipoadminist'] == 'ARL'){
                                $whereAdministradora =' AND inc_arl_afiliado = '.$_POST['administradora'];
                            }else if($_POST['tipoadminist'] == 'AFP'){
                                $whereAdministradora =' AND inc_afp_afiliado = '.$_POST['administradora'];
                            }if($_POST['tipoadminist'] == 'EPS'){
                                $whereAdministradora =' AND inc_ips_afiliado = '.$_POST['administradora'];
                            }


                            $valor = $_POST['fechaInicial'];
                            $valor2 = $_POST['fechaFinal'];
                            $campos = 'emp_nombre, emd_cedula, emd_nombre, inc_diagnostico, ori_descripcion_v as inc_origen, inc_clasificacion, inc_fecha_inicio, inc_fecha_final, inc_tipo_generacion, inc_profesional_responsable, inc_prof_registro_medico, inc_donde_se_genera, inc_fecha_generada, inc_observacion, est_tra_desc_i as inc_estado_tramite, inc_fecha_radicacion, inc_fecha_solicitud, inc_fecha_pago, inc_valor, inc_fecha_pago_nomina, inc_valor_pagado_nomina';
                            $tabla  = 'gi_incapacidad LEFT JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id JOIN gi_origen ON inc_origen = ori_id_i JOIN gi_estado_tramite ON inc_estado_tramite = est_tra_id_i';
                            $condicion = $whereCliente." inc_fecha_inicio BETWEEN '".$valor."' AND '".$valor2."' ".$whereAdministradora;
                            $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, '', 'ORDER BY inc_id ASC');
                            
                            $valorPagado = 0;
                            foreach ($incapacidades as $key => $value) {
                                echo ' 
                                <tr>';
                                if($_POST['cliente_id'] == 0){ 
                                   echo '<td class="text-uppercase">'.$value["emp_nombre"].'</td>';
                                }else{
                                    echo '<td class="text-uppercase">'.$value["emd_cedula"].'</td>';
                                }
                                $valorEstado = 0;
                                if(!empty($value["inc_valor"]) && !is_null($value["inc_valor"])){
                                    $valorEstado = "$ ".number_format($value["inc_valor"], 0, ',', '.');
                                    $valorPagado += $value["inc_valor"];
                                }
                            
                                $fecha = $value['inc_fecha_generada'];
                                
                                echo '
                                    <td class="text-uppercase">'.$value["emd_nombre"].'</td>';
                                echo '<td class="text-uppercase">'.($value["inc_origen"]).'</td>'; 

                                echo' <td class="text-uppercase">'.explode(' ',$value["inc_fecha_inicio"])[0].'</td>
                                    <td class="text-uppercase">'.explode(' ',$value["inc_fecha_final"])[0].'</td>
                                    <td class="text-uppercase">'.$value["inc_estado_tramite"].'</td>
                             
                                    <td class="text-uppercase">'.$valorEstado.'</td>
                                    
                                </tr>'; 

                                
                            }

                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th style="width: 10%;"></th>
                            <th style="width: 21%;"></th>
                            <th style="width: 5%;"></th>
                            <th style="width: 7%;"></th>
                            <th style="width: 7%;"></th>
                            <th style='width: 5%;'>Total</th>
                            <th style="width: 8%;"><?php echo "$ ".number_format($valorPagado, 0, ',', '.'); ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
	 $('#tbl_Incapacidades').DataTable({
        "language" : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "columnDefs": [
            {
                "targets": 3,
                "render": $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD/MM/YYYY')
            } ,
            {
                "targets": 4,
                "render": $.fn.dataTable.render.moment('YYYY-MM-DD', 'DD/MM/YYYY')
            }  
        ]
    });
</script>