<?php
	session_start();
	require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
	require_once '../../../controladores/incapacidades.controlador.php';
	 require_once '../../../modelos/dao.modelo.php';
	require_once '../../../modelos/incapacidades.modelo.php';
    require_once '../../../modelos/tesoreria.modelo.php';
 
	$campos = "count(*) as estado";
    $tabla = "gi_incapacidad";
    if($_SESSION['cliente_id'] != 0){
        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' ";
    }else{
        $condiciones = " inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
    }

    $whereCliente = '';
    if($_SESSION['cliente_id'] != 0){
        $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
    }

    $respuestaX = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
 	$where = "";
    if($_SESSION['cliente_id'] != 0){
        $where = "inc_empresa = ".$_SESSION['cliente_id']." AND ";
    }
    
    $campos = "count(inc_estado_tramite) as estado";
    $tabla = "gi_incapacidad";
    $condiciones = $where." inc_origen =  '1' AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
    
    $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $porcentajeEG = 0;
    $totalEG = $respuesta['estado'];
    if($respuestaX['estado']){
        $porcentajeEG = $totalEG * 100 / $respuestaX['estado'];
    }

	$condiciones = $where." inc_origen =  '2' AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
    $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $porcentajeLM = 0;
    $totalLM = $respuesta['estado'];
    if($respuestaX['estado']){
        $porcentajeLM = $totalLM * 100 / $respuestaX['estado'];
    }
	   
	$condiciones = $where." inc_origen =  '3' AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
    $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $porcentajeLP = 0;
    $totalLP = $respuesta['estado'];
    if($respuestaX['estado']){
        $porcentajeLP = $totalLP * 100 / $respuestaX['estado'];
    }             
	     
	$condiciones = $where." inc_origen =  '4' AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
    $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $porcentajeAL = 0;
    $totalAL = $respuesta['estado'];
    if($respuestaX['estado']){
        $porcentajeAL = $totalAL * 100 / $respuestaX['estado'];
    }             
	      
	$condiciones = $where." inc_origen =  '5' AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
    $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $porcentajeAT = 0;
    $totalAT = $respuesta['estado'];
    if($respuestaX['estado']){
        $porcentajeAT = $totalAT * 100 / $respuestaX['estado'];
    }   
    $condiciones = $where." inc_clasificacion =  4 AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
    $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $porcentaje180 = 0;
    $total180 = $respuesta['estado'];
    if($respuestaX['estado']){
        $porcentaje180 = $total180 * 100 / $respuestaX['estado'];
    }                  
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="card border border-danger">
			    <div class="card-header bg-transparent border-danger">
			        <h5 class="my-0 text-danger">DETALLADO POR ORIGEN</h5>
			    </div>
			    <div class="card-body">
					<table class="table table-bordered table-hover table-striped" width="100%">
						<thead>
							<tr>
								<th style="width:17%;">ENFERMEDAD GENERAL</th>
								<th style="width:17%;">LIC. MATERNIDAD</th>
								<th style="width:17%;">LIC. PATERNIDAD</th>
								<th style="width:17%;">ACCIDENTE LABORAL</th>
								<th style="width:17%;">ACCIDENTE TRANSITO</th>
								<th style="width:15%;">+ 180 DÍAS</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width:17%;"><?php echo $totalEG; ?> / <?php echo number_format($porcentajeEG, 2);?>%</td>
								<td style="width:17%;"><?php echo $totalLM; ?> / <?php echo number_format($porcentajeLM, 2);?>%</td>
								<td style="width:17%;"><?php echo $totalLP; ?> / <?php echo number_format($porcentajeLP, 2);?>%</td>
								<td style="width:17%;"><?php echo $totalAL; ?> / <?php echo number_format($porcentajeAL, 2);?>%</td>
								<td style="width:17%;"><?php echo $totalAT; ?> / <?php echo number_format($porcentajeAT, 2);?>%</td>
								<td style="width:15%;"><?php echo $total180; ?> / <?php echo number_format($porcentaje180, 2);?>%</td>
							</tr>
						</tbody>			
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card border border-danger">
			    <div class="card-header bg-transparent border-danger">
			        <h5 class="my-0 text-danger">Consolidado de Incapacidades por Sede</h5>
			    </div>
			    <div class="card-body">
		    		<table width="100%" class="table table-hover table-bordered" id="tableMensual">
			    		<thead>
			    			<tr>
			    				<th style="width: 10px;">#</th>
			    				<th>SEDE</th>
			    				<th># Incapacidades</th>
	                            <th># E-G</th>
	                            <th># L-M</th>
	                            <th># L-P</th>
	                            <th># A-L</th>
	                            <th># A-T</th>
			    			</tr>
			    		</thead>
			    		<tbody>
			    			<?php 
			    				$labelSede = '';
			    				$valoresSede = '';
			    				$campos = "emd_sede";
			    				$tabla = "gi_empleados";
			    				$condiciones = " emd_sede is NOT NULL AND emd_sede <> ''";
			    				if($_SESSION['cliente_id'] != 0){
			    					$condiciones .=" AND emd_emp_id = ".$_SESSION['cliente_id'];
		    					} 
		    					$resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condiciones, 'GROUP BY emd_sede', 'ORDER BY emd_sede ASC');
		    					$i = 1;
		    					foreach ($resultado as $key => $value) {
		    						if($i == 1){
		    							$labelSede .= "'".$value['emd_sede']."'";
		    						}else{
		    							$labelSede .= ",'".$value['emd_sede']."'";	
		    						}
		    						 
		    						$tabla  = 'gi_incapacidad JOIN gi_empleados ON emd_id = inc_emd_id';

		    						$campos = 'count(*) as total';
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND emd_sede = '".$value['emd_sede']."'";
	                                $to = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                if($i == 1){
		    							$valoresSede .= "".$to['total']."";
		    						}else{
		    							$valoresSede .= ",".$to['total']."";	
		    						}

	                                /*$campos = 'count(*) as total';
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  AND inc_origen = '1' AND emd_sede = '".$value['emd_sede']."'";
	                                $eg = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = '2' AND emd_sede = '".$value['emd_sede']."'";
	                                $lm = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = '3' AND emd_sede = '".$value['emd_sede']."'";
	                                $lp = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = '4' AND emd_sede = '".$value['emd_sede']."' ";
	                                $al = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = '5' AND emd_sede = '".$value['emd_sede']."' ";
	                                $at = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');*/

		    						echo '<tr>';
		    							echo "<td>".$i."</td>";
		    							echo "<td>".$value['emd_sede']."</td>";
		    							echo "<td>".$to['total']."</td>";
		    							echo "<td>"./*$eg['total'].*/"</td>";
	                                    echo "<td>"./*$lm['total'].*/"</td>";
	                                    echo "<td>"./*$lp['total'].*/"</td>";
	                                    echo "<td>"./*$al['total'].*/"</td>";
	                                    echo "<td>"./*$at['total'].*/"</td>";
		    						echo '</tr>';
		    						$i++;
		    					}
			    			?>
			    		</tbody>
			    	</table>
			    </div>
			</div>
		</div>
	</div>