
<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REPORTE INCAPACIDADES POR ADMINISTRADORA </h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Informe de Nomina</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    FILTROS
                </h5>
            </div>
            <div class="card-body">
                <form autocomplete="off">
                    <div class="row">
                        <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-2">
                            <div class="mb-3">
                                <label for="cmbTIpoAdministradora">Tipo de Administradora</label>
                                <select id="tipoAdministradora" name="tipoAdministradora" class="form-control">
                                    <option value="0">Seleccione</option>
                                    <option value="AFP">AFP</option>
                                    <option value="ARL">ARL</option>
                                    <option value="EPS">EPS</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="mb-3">
                                <label for="cmbAdministradora">Administradora</label>
                                <select id="Administradora" name="Administradora"  disabled="true" class="form-control">
                                    <option value="0">Seleccione</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="mb-3">
                                <label for="cmbAdministradora">Fecha Inicial</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar-o"></i>
                                    </span>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaInicio_CE2" id="NuevoFechaInicio_CE2" placeholder="Ingresar Fecha Inicial" required="true">
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="mb-3">
                                <label for="cmbAdministradora">Fecha Final</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar-o"></i>
                                    </span>
                                    <input class="form-control flatpickr-input" type="text" name="NuevoFechaFinal_CE2" id="NuevoFechaFinal_CE2" placeholder="Ingresar Fecha Final" required="true">
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-1">
                            <div class="mb-3">
                                <label style="visibility: hidden;">JoseDavid</label>
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscarRangosEstadosX">Buscar</button>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="mb-3">
                                <label style="visibility: hidden;">JoseDavid</label>
                                <a target="_blank" style="display: none;" class="btn btn-primary btn-block" role="button" href="#" id="btnExportarRangosEstadosX">Exportar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12" id="resultados">
        
    </div>
</div>


<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>
<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>

<script type="text/javascript">
    $(function(){
        var EditarFechaFinal = flatpickr('#NuevoFechaFinal_CE2', {
          altInput: true,
          locale: "es",
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d"
        });
        var EditarFechaInicio = flatpickr('#NuevoFechaInicio_CE2', {
            altInput: true,
            locale: "es",
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            onChange: function(selectedDates, dateStr, instance) {
                EditarFechaFinal.set("minDate", dateStr);
                EditarFechaFinal.setDate(dateStr);
            },
        });

        $("#tipoAdministradora").change(function(){
            if($(this).val() != 0){
                $.ajax({
                    url   : 'ajax/administradoras.ajax.php',
                    data  : { TipoAdministradora : $(this).val() },
                    type  : 'post',
                    dataType : 'json',
                    success  : function(data){
                        var opciones = '<option value="0">Seleccione</option>';

                        $.each(data, function(i, item){
                            opciones += '<option value ="'+ item.ips_id +'">'+ item.ips_nombre +'</option>';
                        });

                        $("#Administradora").html(opciones);
                        $("#Administradora").attr('disabled', false);
                        
                    },
                    beforeSend:function(){
                        $.blockUI({ 
                            css: { 
                                border: 'none', 
                                padding: '1px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff' 
                            } 
                        }); 
                    },
                    complete:function(){
                        $.unblockUI();
                    }
                });
            }else{
                $("#Administradora").html('<option value="0">Seleccione</option>');
                $("#Administradora").attr('disabled', true);
            }
           
        });

        $("#btnBuscarRangosEstadosX").click(function(){
            var fechaInicio     = $("#NuevoFechaInicio_CE2").val();
            var fechaFinal      = $("#NuevoFechaFinal_CE2").val();
            var administradora  = $("#Administradora").val();
            var tipoadministra  = $("#tipoAdministradora").val();
            var paso = 0;
            if(fechaFinal.length < 0){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "La fecha final es necesaria",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });

            }   

            if(fechaInicio.length < 1){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "La fecha Inicial es necesaria",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });
            }

            if(tipoadministra == 0){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "El tipo de administradora necesaria",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });
            }

            if(administradora == 0){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "La administradora es necesaria",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });
            }

            if(paso == 0){
                $.ajax({
                    url    : 'vistas/modulos/nomina/consolidado_administradora.php',
                    type   : 'post',
                    data   :{
                        fechaInicial  : fechaInicio,
                        fechaFinal    : fechaFinal,
                        tipoadminist  : tipoadministra,
                        administradora: administradora,
                        cliente_id    : $("#session").val()
                    },
                    dataType : 'html',
                    success : function(data){
                        $("#resultados").html(data);
                        $("#btnExportarRangosEstadosX").attr('href', 'index.php?exportar=pdfReportAdministradoras&adm='+administradora+"&fi="+fechaInicio+"&ff="+fechaFinal+"&clid="+$("#session").val()+'&tp='+tipoadministra);
                        $("#btnExportarRangosEstadosX").show();
                    },
                    beforeSend:function(){
                        $.blockUI({
                            message : '<h3>Un momento por favor....</h3>', 
                            css: { 
                                border: 'none', 
                                padding: '1px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff' 
                            } 
                        }); 
                    },
                    complete:function(){
                        $.unblockUI();
                    }
                })
            }
        });
    });
</script>