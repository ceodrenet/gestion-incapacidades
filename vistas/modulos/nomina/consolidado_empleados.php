<?php
    require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
	require_once '../../../controladores/empleados.controlador.php';
    require_once '../../../modelos/dao.modelo.php';
	require_once '../../../modelos/empleados.modelo.php';
	require_once '../../../controladores/incapacidades.controlador.php';
	require_once '../../../modelos/incapacidades.modelo.php';
	
	$item  = 'emd_cedula';
	$valor = $_POST['empleado']; 
	$empleados = ControladorEmpleados::ctrMostrarNombres($item, $valor);
    $valoPagado=0;


    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }
    $totalDIas = 0;
?>
<div class="card border border-danger">
    <div class="card-header bg-transparent border-danger">
        <h5 class="my-0 text-danger">CONSOLIDADO DE INCAPACIDADES</h5>
    </div>
    <div class="card-body">
    <table id="tbl_Incapacidades" class="table table-bordered table-striped dt-responsive tablas" style="width: 100%;">
            <thead>
                <tr>
                    <?php if($_POST['cliente_id'] == 0){ ?>
                    <th style="width: 12%;">Empresa</th>
                    <?php }else{ ?>
                     <th style="width: 12%;">Cédula</th>
                    <?php }?>
                    <th style="width: 20%;">Nombre</th>
                    <th style="width: 15%;">Administradora</th>
                    <th style="width: 12%;">Fecha Inicial</th>
                    <th style="width: 12%;">Fecha Final</th>
                    <th style="width: 9%;"># Dias</th>
                    <th style="width: 5%;">Diagnostico</th>
                    <th style="width: 5%;">Estado</th>
                    <th style="width: 10%;">Valor Pagado</th>
                </tr>
            </thead>
            <tbody>
                <?php

                foreach ($empleados as $keys => $empleado) {
                    $item   = 'inc_emd_id';
                    $valor  =  $empleado['emd_id'];
                    $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor);
                    $totalDIas = 0;
                    $valoPagado = 0;
                    foreach ($incapacidades as $key => $value) {
                        echo ' 
                        <tr>';
                        if($_POST['cliente_id'] == 0){ 
                           echo '<td class="text-uppercase">'.$value["emp_nombre"].'</td>';
                        }else{
                            echo '<td class="text-uppercase">'.$value["emd_cedula"].'</td>';
                        }

                        echo '
                            <td class="text-uppercase">'.$value["emd_nombre"].'</td>';

                        if($value['inc_clasificacion'] != 4){
                            if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                                echo '<td class="text-uppercase">'.$value["inc_arl_afiliado"].'</td>'; 
                            }else{
                                echo '<td class="text-uppercase">'.$value["inc_ips_afiliado"].'</td>'; 
                            }
                        }else{
                            echo '<td class="text-uppercase">'.$value["inc_afp_afiliado"].'</td>';    
                        }
                        
                        $totalDIas += dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]);
                        echo '<td class="text-uppercase">'.explode(' ', $value["inc_fecha_inicio"])[0].'</td>
                            <td class="text-uppercase">'.explode(' ', $value["inc_fecha_final"])[0].'</td>
                            <td class="text-uppercase">'.dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]).'</td>
                            <td class="text-uppercase">'.$value["inc_diagnostico"].'</td>
                            <td class="text-uppercase">'.$value["inc_estado_tramite"].'</td>';
                        if($value["inc_valor"] != null && $value["inc_valor"] != ''){
                            $valoPagado += $value["inc_valor"];
                            echo '<td class="text-uppercase">$'.number_format(trim($value["inc_valor"])).'</td>';
                        }else{
                            echo '<td class="text-uppercase"></td>';
                        } 
                            
                        echo '</tr>'; 
                    }
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <?php if($_POST['cliente_id'] == 0){ ?>
                    <th style="width: 12%;"></th>
                    <?php }else{ ?>
                     <th style="width: 12%;"></th>
                    <?php }?>
                    <th style="width: 20%;"></th>
                    <th style="width: 15%;"></th>
                    <th style="width: 12%;"></th>
                    <th style="width: 12%;"></th>
                    <th style="width: 9%;"><?php echo $totalDIas;?></th>
                    <th style="width: 5%;"></th>
                    <th style="width: 5%;"></th>
                    <th style="width: 10%;">$<?php echo number_format($valoPagado);?></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>


<script type="text/javascript">


	 /*$('#tbl_Incapacidades').DataTable({
          "lengthMenu": [
                [10, 25, 50, 100, 200, -1], 
                [10, 25, 50, 100, 200, "Todos"]
            ],
        "language" : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }*/
    });
</script>