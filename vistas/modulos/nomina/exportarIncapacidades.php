<?php
	header("Content-type: application/vnd.ms-excel; charset=utf-8");

    header("Content-Disposition: attachment; filename=Reporte_Consolidado_incapacidades.xls");

    
	echo '<table>
                    <thead>
                        <tr>
                            <th style="width: 10px;">#</th>
                            <th style="width: 15%;">Empresa</th>
                            <th style="width: 20%;">Administradora</th>
                            <th style="width: 25%;">C&eacute;dula</th>
                            <th style="width: 25%;">Nombre</th>
                            <th style="width: 5%;">Diagnostico</th>
                            <th style="width: 5%;">Origen</th>
                            <th style="width: 5%;">Clasificaci&oacute;n</th>
                            <th style="width: 5%;">Fecha Inicio</th>
                            <th style="width: 5%;">Fecha Final</th>
                            <th style="width: 5%;">Dias transcurridos</th>
                            <th style="width: 5%;">Tipo Generaci&oacute;n</th>
                            <th style="width: 5%;">Profesional Responsable</th>
                            <th style="width: 5%;">Registro del Profesional</th>
                            <th style="width: 10%;">Donde se genero</th>
                            <th style="width: 10%;">Fecha Generaci&oacute;n</th>
                            <th style="width: 10%;">Observaci&oacute;n</th>
                            <th style="width: 10%;">Estado Tramite</th>
                            <th style="width: 10%;">Fecha Radicaci&oacute;n</th>
                            <th style="width: 10%;">Fecha Solicitud</th>
                            <th style="width: 10%;">Fecha Pago</th>
                            <th style="width: 10%;">Valor Pagado</th>
                            <th style="width: 10%;">Fecha Pago Nomina</th>
                            <th style="width: 10%;">Valor Pagado Nomina</th>
                        </tr>
                    </thead>
                    <tbody>';
                        
    if($_SESSION['cliente_id'] != 0){
        $item = $_SESSION['cliente_id'];
        $valor = $_GET['fechaInicial'];
        $valor2 = $_GET['fechaFinal'];
        $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_Consolidado($item, $valor, $valor2);
    }else{
        $item = null;
        $valor = $_GET['fechaInicial'];
        $valor2 = $_GET['fechaFinal'];
        $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_Consolidado($item, $valor, $valor2);
    }
    foreach ($incapacidades as $key => $value) {
        echo ' 
        <tr>
            <td>'.($key+1).'</td>
            <td class="text-uppercase">'.sanear_string($value["emp_nombre"]).'</td>';

        if($value['inc_clasificacion'] != 4){
            if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                echo '<td class="text-uppercase">'.($value["inc_arl_afiliado"]).'</td>'; 
            }else{
                echo '<td class="text-uppercase">'.($value["inc_ips_afiliado"]).'</td>'; 
            }
        }else{
            echo '<td class="text-uppercase">'.($value["inc_afp_afiliado"]).'</td>';    
        }
        
        echo '<td class="text-uppercase">'.sanear_string($value["emd_cedula"]).'</td>
            <td class="text-uppercase">'.sanear_string($value["emd_nombre"]).'</td>
            <td class="text-uppercase">'.$value["inc_diagnostico"].'</td>
            
            
            <td class="text-uppercase">'.$value["inc_origen"].'</td>
            <td class="text-uppercase">'.$value["inc_clasificacion"].'</td>  
            <td class="text-uppercase">'.$value["inc_fecha_inicio"].'</td>
            <td class="text-uppercase">'.$value["inc_fecha_final"].'</td>
            <td class="text-uppercase">'.dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]).'</td>

            <td class="text-uppercase">'.$value["inc_tipo_generacion"].'</td>
            <td class="text-uppercase">'.sanear_string($value["inc_profesional_responsable"]).'</td>  
            <td class="text-uppercase">'.$value["inc_prof_registro_medico"].'</td>
            <td class="text-uppercase">'.sanear_string($value["inc_donde_se_genera"]).'</td>
            <td class="text-uppercase">'.$value["inc_fecha_generada"].'</td>
            <td class="text-uppercase">'.sanear_string($value["inc_observacion"]).'</td>
            <td class="text-uppercase">'.$value["inc_estado_tramite"].'</td>
            <td class="text-uppercase">'.$value["inc_fecha_radicacion"].'</td>
            <td class="text-uppercase">'.$value["inc_fecha_solicitud"].'</td>
            <td class="text-uppercase">'.$value["inc_fecha_pago"].'</td>
            <td class="text-uppercase">'.$value["inc_valor"].'</td>  
            <td class="text-uppercase">'.$value["inc_fecha_pago_nomina"].'</td>
            <td class="text-uppercase">'.$value["inc_valor_pagado_nomina"].'</td>              
        </tr>';
    }

   	echo '
                    </tbody>
                </table>';

function dias_transcurridos($fecha_i, $fecha_f)
{
	$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
	$dias 	= abs($dias); $dias = floor($dias);		
	return $dias + 1;
}

function sanear_string($string) { 
    $string = str_replace( array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string );
    $string = str_replace( array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string ); 
    $string = str_replace( array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string ); 
    $string = str_replace( array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string ); 
    $string = str_replace( array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string ); 
    $string = str_replace( array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C',), $string ); 
    //Esta parte se encarga de eliminar cualquier caracter extraño 
    //$string = str_replace( array("\\", "¨", "º", "-", "~", "#", "@", "|", "!", "\"", "·", "$", "%", "&", "/", "(", ")", "?", "'", "¡", "¿", "[", "^", "`", "]", "+", "}", "{", "¨", "´", ">“, “< ", ";", ",", ":", "."), '', $string ); 
    return $string; 
}

