<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REPORTES-NOMINA</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Consolidado PQRS</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    INFORME - CONSOLIDADO PQRS
                </h5>
            </div>
            <div class="card-body">
                
                <form autocomplete="off" action="ajax/informeSaldo.ajax.php" method="post" autocomplete="off">
                    <div class="row">
                        <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-2">
                            <div class="mb-3">
                                <select class="form-control" id="yearFilter" name="yearFilter">
                                    <?php
                                    $years = ModeloDAO::extractYearOfDate("gi_incapacidad", "inc_fecha_recepcion_d", $_SESSION['cliente_id']);
                                    foreach ($years as $value) {
                                        echo "<option value='".$value['anho']."'>".$value['anho']."</option>";
                                    }
                                    ?>
                                    <option value="0">TODOS</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3">
                                <select class="form-control" id="filtroEstadoTramite" name="filtroEstadoTramite">
                                    <option value="1">EMPRESA</option>
                                    <option value="2">PAGADAS</option>
                                    <option value="3">PENDIENTES</option>
                                    <option value="4">SIN RECONOCIMIENTO</option>
                                    <option value="0" selected>TODAS</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3">
                                <select class="form-control" id="filtroFecha" name="filtroFecha">
                                    <option value="inc_fecha_recepcion_d">FECHA DE RECEPCIÓN</option>
                                    <option value="inc_fecha_inicio">FECHA DE INICIO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="ExportarConsolidadoPQRS"><i class="fa fa-file-excel"></i>&nbsp;Exportar</button>
                            </div>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>
<script type="text/javascript">
    $(function(){

        $("#ExportarConsolidadoPQRS").on('click', function(){
            var year = $("#yearFilter").val();
            var estado = $("#filtroEstadoTramite").val();
            var filtroFecha = $("#filtroFecha").val();
            var emp = <?php echo $_SESSION['cliente_id']; ?>
            
            $.ajax({
                url    : `https://incapacidades.app/services/exports/incapacidades/consolidadoPQRS?emp=${emp}&year=${year}&estado=${estado}&filtFecha=${filtroFecha}`,
                headers: {
                    'Authorization':'<?php echo $_SESSION['apiKey'];?>'},
                type: 'GET',
                dataType : 'json',
                success : function(data){
                    var $a = $("<a>");
                    $a.attr("href",data.file);
                    $("body").append($a);
                    $a.attr("download","consolidadoPQRS.xlsx");
                    $a[0].click();
                    $a.remove();
                },
                beforeSend:function(){
                    $.blockUI({
                        message : '<h3>Un momento por favor....</h3>', 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            });
        });
    });
</script>