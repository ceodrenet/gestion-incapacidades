<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<?php
$months = array(
    0 => "Enero", 1 => "Febrero", 2 => "Marzo", 3 => "Abril", 4 => "Mayo", 5 => "Junio", 6 => "Julio",
    7 => "Agosto", 8 => "Septiembre", 9 => "Octubre", 10 => "Noviembre", 11 => "Diciembre"
);
?>


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REPORTES-GERENCIAL</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Informe de Saldos</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    INFORME DE RECEPCIÓN
                </h5>
            </div>
            <div class="card-body">
                
                <form id="frmInformeRecepcion" method="post" autocomplete="off">
                    <div class="row">
                        <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-2">
                            <div class="mb-3">
                                <select class="form-control" id="year" name="year">
                                    <?php
                                    $years = ModeloDAO::extractYearOfDate("gi_incapacidad", "inc_fecha_recepcion_d", $_SESSION['cliente_id']);
                                    foreach ($years as $value) {
                                        echo "<option value='".$value['anho']."'>".$value['anho']."</option>";
                                    }
                                    ?>
                                    <option value="0">TODOS</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3">
                                <select class="form-control" name="month" id="month">
                                    <?php 
                                    foreach ($months as $key => $value) {
                                        echo "<option value='".$key."'>".$value."</option>";
                                    }
                                    ?>
                                    <option value="all">TODOS</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mt-2">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="options" id="inlineRadio2" value="#" checked>
                                    <label class="form-check-label" for="inlineRadio2">valor(#)</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="options" id="inlineRadio1" value="$">
                                    <label class="form-check-label" for="inlineRadio1">valor($)</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="options" id="inlineRadio3" value="%">
                                    <label class="form-check-label" for="inlineRadio3">valor(%)</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscar"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>   
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12" id="resultados">
    </div>
</div>

<script type="text/javascript">
    $("#btnBuscar").click(function(){
            var year = $("#year").val();
            var month = $("#month").val();
            var options = $('input:radio[name=options]:checked').val();

           
            $.ajax({
                url    : 'ajax/visualizarInformeRecepcion.ajax.php',
                type   : 'post',
                data   :{
                    year : year,
                    month : month,
                    options : options,
                },
                dataType : 'html',
                success : function(data){
                    $("#resultados").html(data);
                    $("#btnExportar").show()
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                }
            })
        });
</script>
<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>