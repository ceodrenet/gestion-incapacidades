<?php
    require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
	require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../modelos/dao.modelo.php';
	require_once '../../../modelos/incapacidades.modelo.php';
	require_once '../../../modelos/tesoreria.modelo.php';
?>
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<div class="card border border-danger">
    <div class="card-header bg-transparent border-danger">
        <h5 class="my-0 text-danger">CONSOLIDADO DE INCAPACIDADES</h5>
    </div>
    <div class="card-body">
        <table id="tbl_Incapacidades" class="table table-bordered table-striped dt-responsive tablas " style="width: 100%;">
            <thead>
                <tr>
                    <th style="width: 10%;">Cliente</th>
                    <th style="width: 10%;">Identificación</th>
                    <th style="width: 10%;">Contrato</th>
                    <th style="width: 15%;">Desc. Tipo Ausentismo</th>
                    <th style="width: 5%;"># Días</th>
                    <th style="width: 10%;">Días Compensados</th>
                    <th style="width: 10%;">Fecha Inicial</th>
                    <th style="width: 10%;">Fecha Final</th>
                    <th style="width: 5%;">Díagnostico</th>
                    <th style="width: 10%;">Tipo Inc.</th>
                </tr>
            </thead>
            <tbody>
                <?php
                	$where = '';
                   	if($_POST['cliente_id'] != 0){
                    	$where = "emp_id = ".$_POST['cliente_id']." AND ";
                    }

                    $campos_ = "emp_nit, emd_cedula, emd_id, emd_codigo_nomina, ori_descripcion_v as inc_origen, DATE_FORMAT(inc_fecha_inicio, '%d/%m/%Y') as inc_fecha_inicio, inc_fecha_inicio as fecha1, DATE_FORMAT(inc_fecha_final, '%d/%m/%Y') as inc_fecha_final, inc_fecha_final as fecha2, inc_diagnostico, inc_clasificacion as inc_estado" ;
                    $tablas_ = "gi_incapacidad JOIN gi_empresa ON emp_id = inc_empresa JOIN gi_empleados ON inc_emd_id = emd_id LEFT JOIN gi_origen on ori_id_i= inc_origen";
                    $condic_ = $where." inc_fecha_pago_nomina  BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND emd_estado = 1 AND inc_estado = 1";
    				
    				$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos_, $tablas_, $condic_, null, " ORDER BY inc_fecha_inicio DESC");

                    //echo "SELECT ".$campos_." FROM ".$tablas_." WHERE ".$condic_." ORDER BY inc_fecha_inicio DESC";
                    //var_dump($incapacidades);
                    foreach ($incapacidades as $key => $value) {
                        echo '<tr>';
                       	echo '<td class="text-uppercase">'.$value["emp_nit"].'</td>';
                    	echo '<td class="text-uppercase">'.$value["emd_cedula"].'</td>';
                    	echo '<td class="text-uppercase">'.$value["emd_codigo_nomina"].'</td>';
                   		echo '<td class="text-uppercase">'.$value["inc_origen"].'</td>';
                   		echo '<td class="text-uppercase">'.ControladorIncapacidades::dias_transcurridos($value["fecha1"] , $value["fecha2"]).'</td>';
                   		echo '<td></td>';
                        echo '<td class="text-uppercase">'.$value["inc_fecha_inicio"].'</td>';
                        echo '<td class="text-uppercase">'.$value["inc_fecha_final"].'</td>';
                  		echo '<td class="text-uppercase">'.$value["inc_diagnostico"].'</td>';
                        $estado = "Inicial";
                        if($value['inc_estado'] != '1' && $value['inc_estado'] != '6'){
                            $estado = "Prorroga";
                        }
                        echo '<td class="text-uppercase">'.$estado.'</td>';
                        echo '</tr>'; 
                    }

                ?>
            </tbody>
            <tfoot>
            	<tr>
            		<th style="width: 10%;">Cliente</th>
                    <th style="width: 10%;">Identificación</th>
                    <th style="width: 10%;">Contrato</th>
                    <th style="width: 15%;">Desc. Tipo Ausentismo</th>
                    <th style="width: 5%;"># Días</th>
                    <th style="width: 10%;">Días Compensados</th>
                    <th style="width: 10%;">Fecha Inicial</th>
                    <th style="width: 10%;">Fecha Final</th>
                    <th style="width: 5%;">Díagnostico</th>
                    <th style="width: 10%;">Tipo Inc.</th>
            	</tr>
            </tfoot>
        </table>
    </div>
</div>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
	 $('#tbl_Incapacidades').DataTable({
        "language" : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        } 
    });
</script>