<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">INFORME DE NOMINA</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Incapacidades por centros de costo</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    INFORME POR CENTRO DE COSTO
                </h5>
            </div>
            <div class="card-body">
                <form autocomplete="off">
                    <div class="row">
                        <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-3">
                            <div class="mb-3">
                                <input class="form-control flatpickr-input" type="text" name="NuevoFechaInicio_CE" id="NuevoFechaInicio_CE2" placeholder="Ingresar Fecha Inicial" required="true">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mb-3">
                                <input class="form-control flatpickr-input" type="text" name="NuevoFechaFinal_CE" id="NuevoFechaFinal_CE2" placeholder="Ingresar Fecha Final" required="true">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscarRangosEstadosX"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>
                        <!--<div class="col-md-2">
                            <div class="d-grid gap-2 ">
                                <a target="_blank" class="btn btn-outline-danger waves-effect waves-light btn-flat" role="button" id="btnExportargeneral" style="display: none;"><i class="fa fa-file-pdf-o"></i>&nbsp;PDF</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <a target="_blank" class="btn btn-outline-danger waves-effect waves-light btn-flat" role="button" id="btnExportarPDF" style="display: none;"><i class="fa fa-file-excel-o"></i>&nbsp;Excel</a>
                            </div>
                        </div>-->
                    </div>    
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12" id="resultados">

    </div>
</div>


<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>
<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>

<script type="text/javascript">
    $(function(){
 var NuevoFechaFinal_CE2 = flatpickr('#NuevoFechaFinal_CE2', {
          altInput: true,
          altFormat: "d/m/Y",
          dateFormat: "Y-m-d",
          locale: "es",
          onChange: function(selectedDates, dateStr, instance){
            var minDate = dateStr;
            var fecha1 = moment($("#NuevoFechaInicio_CE2").val());
            var fecha2 = moment(minDate);
            $("#EditarNumeroDias").val( (fecha2.diff(fecha1, 'days') + 1) + " Días");
          }
        });

        var NuevoFechaInicio_CE2 = flatpickr('#NuevoFechaInicio_CE2', {
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            locale: "es",
            onChange: function(selectedDates, dateStr, instance) {
                NuevoFechaFinal_CE2.set("minDate", dateStr);
                NuevoFechaFinal_CE2.setDate(dateStr);
            },
        });
    

        $("#btnBuscarRangosEstadosX").click(function(){
            var fechaInicio     = $("#NuevoFechaInicio_CE2").val();
            var fechaFinal      = $("#NuevoFechaFinal_CE2").val();
            var paso = 0;
            if(fechaFinal.length < 0){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "La fecha final es necesaria",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });

            }   

            if(fechaInicio.length < 1){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "La fecha Inicial es necesaria",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });
            }

            if(paso == 0){
                $.ajax({
                    url    : 'vistas/modulos/nomina/centrocosto_proceso.php',
                    type   : 'post',
                    data   :{
                        fechaInicial  : fechaInicio,
                        fechaFinal    : fechaFinal
                    },
                    dataType : 'html',
                    success : function(data){
                        $("#resultados").html(data);
                    },
                    beforeSend:function(){
                        $.blockUI({ 
                            message : '<h3>Un momento por favor....</h3>',
                            css: { 
                                border: 'none', 
                                padding: '1px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5, 
                                color: '#fff' 
                            } 
                        }); 
                    },
                    complete:function(){
                        $.unblockUI();
                    }
                })
            }
        });
    });
</script>