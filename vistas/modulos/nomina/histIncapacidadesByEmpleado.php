<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">INFORME DE NOMINA</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Historico de Incapacidades por Empleado</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    INCAPACIDADES POR EMPLEADO
                </h5>
            </div>
            <div class="card-body">
                <form autocomplete="off" action="ajax/exportHistIncEmpl.ajax.php" method="post">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="mb-3">
                                <select class="form-control" name="buscarEmpleado" id="buscarEmpleado" required>
                                    <option value="0">Consultar empleado</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                                <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="button" id="btnBuscarEmpleados"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="d-grid gap-2">
                            <button class="btn btn-outline-danger waves-effect waves-light btn-flat" type="submit" id="btnExportar" style="display: none;"><i class="fa fa-file-excel"></i>&nbsp;Exportar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12" id="resultados">
    </div>
</div>
<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>
<script type="text/javascript">
    var config = {
        placeholderValue: 'Inicie escribiendo la palabra',
        shouldSort: false,
        searchFloor: 3, // or whatever value makes sense for your data / API
        searchChoices: false, // see bullet point 2 above
        duplicateItems: false, // this is ignored, see bullet point 3 above
        removeItemButton: true,
    };
    var selEmpleados = new Choices('#buscarEmpleado', config);
    var urlEmpleado = 'ajax/empleados.ajax.php?getEmpleadosSelect=true';
    var lookupDelay = 250;
    var lookupTimeout = null;

    /*choises.js para poder buscar por AJAX - MÉDICOS*/
    var serverLookupEmpleado = function() {
        var query = selEmpleados.input.value;
        $.post(urlEmpleado, {
            query: query
        }, function(event) {
            var results = JSON.parse(event);
            selEmpleados['setChoices'](results, 'id', 'text', true);
        });
    }

    document.getElementById("buscarEmpleado").addEventListener('search', function(event) {
        clearTimeout(lookupTimeout);
        lookupTimeout = setTimeout(serverLookupEmpleado, lookupDelay);
    });
    document.getElementById("buscarEmpleado").addEventListener('choice', function(event) {
        selEmpleados.setChoices([], 'id', 'text', true);
    });

    $("#btnBuscarEmpleados").on('click', function() {
   
        if ($("#buscarEmpleado").val() != 0 && $("#buscarEmpleado").val() != null) {
            $.ajax({
                url: 'ajax/visualizarInformeHistoricoIncEmpl.ajax.php',
                type: 'post',
                data: {
                    cedula: $("#buscarEmpleado").val(),
                },
                dataType: 'html',
                success: function(data) {
                    $("#resultados").html(data)
                    $("#btnExportar").show()
                },
                beforeSend: function() {
                    $.blockUI({
                        message: '<h3>Un momento por favor....</h3>',
                        css: {
                            border: 'none',
                            padding: '1px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                }
            })
        } else {
            $("#btnExportar").hide()
            alertify.error("No ha seleccionando ningún empleado");
        }
    });
</script>
<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>