<?php
    require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
	require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../modelos/dao.modelo.php';
	require_once '../../../modelos/incapacidades.modelo.php';
    require_once '../../../modelos/tesoreria.modelo.php';

?>
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    CONSOLIDADO DE INCAPACIDADES
                </h5>
            </div>
            <div class="card-body">
                <table id="tbl_Incapacidades" class="table table-bordered table-striped dt-responsive tablas" style="width: 100%;">
                    <thead>
                        <tr>
                            <th style="width: 10%;">Cédula</th>
                            <th style="width: 20%;">Nombre</th>
                            <th style="width: 17%;">Administradora</th>
                            <th style="width: 11%;">Fecha Inicial</th>
                            <th style="width: 10%;">Fecha Final</th>
                            
                            <?php 
                                if($_POST['estado'] == '3'){
                                    echo "<th style='width: 10%;'>Fecha Radicación</th>";
                                }else if($_POST['estado'] == '4'){
                                    echo "<th style='width: 10%;'>Fecha Solicitud</th>";
                                }else if($_POST['estado'] == '5'){
                                    echo "<th style='width: 10%;'>Fecha Pago </th>";
                                }else if($_POST['estado'] == '6'){
                                    echo "<th style='width: 10%;'>Fecha Negacion</th>";
                                }else{
                                    echo "<th style='width: 10%;'>Fecha Registro</th>";
                                }
                            ?> 
                            <th style="width: 12%;">Estado</th>
                            <th style="width: 10%;">Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            if($_POST['cliente_id'] != 0){
                                //echo  "Hola";
                                $item = $_POST['cliente_id'];
                                $valor = $_POST['fechaInicial'];
                                $valor2 = $_POST['fechaFinal'];
                                $valor3 = $_POST['estado'];
                                $campos = 'emp_nombre, emd_cedula, emd_nombre, inc_diagnostico, inc_origen, inc_clasificacion, Date_format(inc_fecha_inicio,\'%d/%m/%y\') as inc_fecha_inicio, Date_format(inc_fecha_final,\'%d/%m/%y\') as inc_fecha_final,   inc_tipo_generacion, inc_profesional_responsable, inc_prof_registro_medico, inc_donde_se_genera,  Date_format(inc_fecha_generada,\'%d/%m/%y\') as inc_fecha_generada , inc_observacion, est_tra_desc_i as inc_estado_tramite,  Date_format(inc_fecha_radicacion,\'%d/%m/%y\') as inc_fecha_radicacion, Date_format(inc_fecha_solicitud,\'%d/%m/%y\') as  inc_fecha_solicitud, Date_format(inc_fecha_pago,\'%d/%m/%y\') as inc_fecha_pago, inc_valor, Date_format(inc_fecha_pago_nomina,\'%d/%m/%y\') as inc_fecha_pago_nomina, inc_valor_pagado_nomina, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado ,  ip4.ips_nombre as inc_afp_afiliado, Date_format(inc_fecha_negacion_,\'%d/%m/%y\') as  inc_fecha_negacion_';
                                $tabla  = 'gi_incapacidad LEFT JOIN gi_ips as ip2 ON ip2.ips_id = inc_ips_afiliado LEFT JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id LEFT JOIN gi_estado_tramite on est_tra_id_i= inc_estado_tramite';
                                if($valor3 == '-1'){
                                    $condicion = "inc_empresa = ".$item." AND inc_estado = '".$valor3."' AND inc_fecha_inicio BETWEEN '".$valor."' AND '".$valor2."' ";
                                }else{
                                    $condicion = "inc_empresa = ".$item." AND inc_estado_tramite = '".$valor3."' AND inc_fecha_inicio BETWEEN '".$valor."' AND '".$valor2."' ";  
                                }
                                
                                $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, '', 'ORDER BY inc_id ASC');

                            }else{

                                $item = null;
                                $valor = $_POST['fechaInicial'];
                                $valor2 = $_POST['fechaFinal'];
                                $valor3 = $_POST['estado'];
                                $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_ConsolidadoEstado($item, $valor3, $valor, $valor2);
                            }

                            $valorPagado = 0;
                            foreach ($incapacidades as $key => $value) {
                                echo ' 
                                <tr>';
                                if($_POST['cliente_id'] == 0){ 
                                   echo '<td class="text-uppercase">'.$value["emp_nombre"].'</td>';
                                }else{
                                    echo '<td class="text-uppercase">'.$value["emd_cedula"].'</td>';
                                }
                                $valorEstado = 0;
                                if(!empty($value["inc_valor"]) && !is_null($value["inc_valor"])){
                                    $valorEstado = "$ ".number_format($value["inc_valor"], 0, ',', '.');
                                    $valorPagado += $value["inc_valor"];
                                }
                                $fecha = null;
                                if($_POST['estado'] == 'RADICADA'){
                                    $fecha = $value['inc_fecha_radicacion'];
                                }else if($_POST['estado'] == 'SOLICITUD DE PAGO'){
                                    $fecha = $value['inc_fecha_solicitud'];
                                }else if($_POST['estado'] == 'PAGADA'){
                                    $fecha = $value['inc_fecha_pago'];
                                }else if($_POST['estado'] == 'SIN RECONOCIMIENTO'){
                                    $fecha = $value['inc_fecha_negacion_'];
                                }else{
                                    $fecha = $value['inc_fecha_generada'];
                                }
                                echo '
                                    <td class="text-uppercase">'.$value["emd_nombre"].'</td>';
                                if($value['inc_clasificacion'] != 4){
                                    if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                                        echo '<td class="text-uppercase">'.($value["inc_arl_afiliado"]).'</td>'; 
                                    }else{
                                        echo '<td class="text-uppercase">'.($value["inc_ips_afiliado"]).'</td>'; 
                                    }
                                }else{
                                    echo '<td class="text-uppercase">'.($value["inc_afp_afiliado"]).'</td>';    
                                }
                                echo' <td class="text-uppercase">'.explode(' ',$value["inc_fecha_inicio"])[0].'</td>
                                    <td class="text-uppercase">'.explode(' ',$value["inc_fecha_final"])[0].'</td>
                                    <td class="text-uppercase">'.$fecha.'</td>
                                    <td class="text-uppercase">'.$value["inc_estado_tramite"].'</td>
                                    <td class="text-uppercase">'.$valorEstado.'</td>
                                    
                                </tr>'; 
                            }

                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th style="width: 10%;"></th>
                            <th style="width: 20%;"></th>
                            <th style="width: 17%;"></th>
                            <th style="width: 11%;"></th>
                            <th style="width: 10%;"></th>
                            <th style="width: 12%;"></th>
                            <th style='width: 10%;'>Total</th>
                            <th style="width: 10%;"><?php echo "$ ".number_format($valorPagado, 0, ',', '.'); ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
	 $('#tbl_Incapacidades').DataTable({
        "language" : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
</script>