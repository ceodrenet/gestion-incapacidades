<?php
	session_start();
	require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
	require_once '../../../controladores/incapacidades.controlador.php';
	 require_once '../../../modelos/dao.modelo.php';
	require_once '../../../modelos/incapacidades.modelo.php';
    require_once '../../../modelos/tesoreria.modelo.php';
 
	$campos = "count(*) as estado";
    $tabla = "gi_incapacidad";
    if($_SESSION['cliente_id'] != 0){
        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' ";
    }else{
        $condiciones = " inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
    }

    $whereCliente = '';
    if($_SESSION['cliente_id'] != 0){
        $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
    }

    $respuestaX = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
?>
	<div class="row">

	    <div class="col-lg-2 col-xs-4">
	    	<!-- small box -->
	        <div class="info-box bg-aqua">
	            <span class="info-box-icon"><i class="fa fa-heartbeat"></i></span>
	            <?php
	                $campos = "count(inc_estado_tramite) as estado";
	                $tabla = "gi_incapacidad";
	                $condiciones = "inc_origen =  'ENFERMEDAD GENERAL' AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
	                if($_SESSION['cliente_id'] != 0){
	                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
	                }
	                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
	                if($respuestaX['estado']){
	                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
	                }else{
	                    $porcentaje = 0;
	                }
	                
	            ?>
	            <div class="info-box-content">
	                <span class="info-box-text">E. GENERAL</span>
	                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

	                <div class="progress">
	                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
	                </div>
	                <span class="progress-description">
	                    <?php echo number_format($porcentaje, 2)." %";?>
	                </span>
	            </div>
	            <!-- /.info-box-content -->
	        </div>
	    </div>
	    <!-- ./col -->

	    <div class="col-lg-2 col-xs-4">
	        <div class="info-box bg-green">
	            <span class="info-box-icon"><i class="fa fa-female"></i></span>
	            <?php

	                $campos = "count(inc_estado_tramite) as estado";
	                $tabla = "gi_incapacidad";
	                $condiciones = "inc_origen =  'LICENCIA DE MATERNIDAD' AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
	                if($_SESSION['cliente_id'] != 0){
	                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
	                }
	                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
	                if($respuestaX['estado']){
	                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
	                }else{
	                    $porcentaje = 0;
	                }
	                
	            ?>
	            <div class="info-box-content">
	                <span class="">MATERNIDAD</span>
	                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>
	                <div class="progress">
	                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
	                </div>
	                <span class="progress-description">
	                    <?php echo number_format($porcentaje, 2)." %";?>
	                </span>
	            </div>
	            <!-- /.info-box-content -->
	        </div>
	    </div>

	    <div class="col-lg-2 col-xs-4">
	        <!-- small box --> 
	        <div class="info-box bg-green">
	            <span class="info-box-icon"><i class="fa fa-street-view"></i></span>
	            <?php
	                $campos = "count(inc_estado_tramite) as estado";
	                $tabla = "gi_incapacidad";
	                $condiciones = "inc_origen =  'LICENCIA DE PATERNIDAD' AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
	                if($_SESSION['cliente_id'] != 0){
	                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
	                }
	                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
	                if($respuestaX['estado']){
	                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
	                }else{
	                    $porcentaje = 0;
	                }
	                
	            ?>
	            <div class="info-box-content">
	                <span class="">PATERNIDAD</span>
	                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

	                <div class="progress">
	                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
	                </div>
	                <span class="progress-description">
	                    <?php echo number_format($porcentaje, 2)." %";?>
	                </span>
	            </div>
	            <!-- /.info-box-content -->
	        </div>
	    </div>

	    <!-- ./col -->
	    <div class="col-lg-2 col-xs-4">
	        <div class="info-box bg-red">
	            <span class="info-box-icon"><i class="fa fa-wheelchair"></i></span>
	            <?php
	                $campos = "count(inc_estado_tramite) as estado";
	                $tabla = "gi_incapacidad";
	                $condiciones = "inc_origen =  'ACCIDENTE LABORAL' AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
	                if($_SESSION['cliente_id'] != 0){
	                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
	                }
	                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
	                if($respuestaX['estado']){
	                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
	                }else{
	                    $porcentaje = 0;
	                }
	                
	            ?>
	            <div class="info-box-content">
	                <span class="info-box-text">A. LABORAL</span>
	                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

	                <div class="progress">
	                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
	                </div>
	                <span class="progress-description">
	                    <?php echo number_format($porcentaje, 2)." %";?>
	                </span>
	            </div>
	            <!-- /.info-box-content -->
	        </div>
	    </div>

	    <!-- ./col -->
	    <div class="col-lg-2 col-xs-4">
	        <div class="info-box bg-aqua">
	            <span class="info-box-icon"><i class="fa fa-motorcycle"></i></span>
	            <?php
	                $campos = "count(inc_estado_tramite) as estado";
	                $tabla = "gi_incapacidad";
	                $condiciones = "inc_origen =  'ACCIDENTE TRANSITO' AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
	                if($_SESSION['cliente_id'] != 0){
	                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
	                }
	                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
	                if($respuestaX['estado']){
	                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
	                }else{
	                    $porcentaje = 0;
	                }
	                
	            ?>
	            <div class="info-box-content">
	                <span class="info-box-text">A. TRANSITO</span>
	                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

	                <div class="progress">
	                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
	                </div>
	                <span class="progress-description">
	                    <?php echo number_format($porcentaje, 2)." %";?>
	                </span>
	            </div>
	            <!-- /.info-box-content -->
	        </div>
	    </div>

	    <div class="col-lg-2 col-xs-4">
	        <div class="info-box bg-yellow">
	            <span class="info-box-icon"><i class="fa fa-plus"></i></span>
	            <?php
	                $item = 'inc_clasificacion';
	                $value = '4';
	                $campos = "count(inc_estado_tramite) as estado";
	                $tabla = "gi_incapacidad";
	                $condiciones = "inc_clasificacion =  4 AND inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  ";
	                if($_SESSION['cliente_id'] != 0){
	                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
	                }
	                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
	                if($respuestaX['estado']){
	                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
	                }else{
	                    $porcentaje = 0;
	                }
	                
	            ?>
	            <div class="info-box-content">
	                <span class="info-box-text">+ 180 DÍAS</span>
	                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

	                <div class="progress">
	                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
	                </div>
	                <span class="progress-description">
	                    <?php echo number_format($porcentaje, 2)." %";?>
	                </span>
	            </div>
	            <!-- /.info-box-content -->
	        </div>
	    </div>
		<!-- ./col -->

	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card border border-danger">
			    <div class="card-header bg-transparent border-danger">
			        <h5 class="my-0 text-danger">Consolidado de Incapacidades por Sede</h5>
			    </div>
			    <div class="card-body">
		    		<table width="100%" class="table table-hover table-bordered" id="tableMensual">
			    		<thead>
			    			<tr>
			    				<th style="width: 10px;">#</th>
			    				<th>SEDE</th>
			    				<th># Incapacidades</th>
	                            <th># E-G</th>
	                            <th># L-M</th>
	                            <th># L-P</th>
	                            <th># A-L</th>
	                            <th># A-T</th>
			    			</tr>
			    		</thead>
			    		<tbody>
			    			<?php 
			    				$labelSede = '';
			    				$valoresSede = '';
			    				$campos = "emd_sede";
			    				$tabla = "gi_empleados";
			    				$condiciones = "emd_sede is NOT NULL AND emd_sede <> ''";
			    				if($_SESSION['cliente_id'] != 0){
			    					$condiciones .=" AND emd_emp_id = ".$_SESSION['cliente_id'];
		    					}  
		    					$resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condiciones, 'GROUP BY emd_sede', 'ORDER BY emd_sede ASC');
		    					$i = 1;
		    					foreach ($resultado as $key => $value) {
		    						if($i == 1){
		    							$labelSede .= "'".$value['emd_sede']."'";
		    						}else{
		    							$labelSede .= ",'".$value['emd_sede']."'";	
		    						}
		    						 

		    						$tabla  = 'gi_incapacidad JOIN gi_empleados ON emd_id = inc_emd_id';

		    						$campos = 'count(*) as total';
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND emd_sede = '".$value['emd_sede']."'";
	                                $to = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                if($i == 1){
		    							$valoresSede .= "".$to['total']."";
		    						}else{
		    							$valoresSede .= ",".$to['total']."";	
		    						}

	                                $campos = 'count(*) as total';
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  AND inc_origen = 'ENFERMEDAD GENERAL' AND emd_sede = '".$value['emd_sede']."'";
	                                $eg = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'LICENCIA DE MATERNIDAD' AND emd_sede = '".$value['emd_sede']."'";
	                                $lm = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'LICENCIA DE PATERNIDAD' AND emd_sede = '".$value['emd_sede']."'";
	                                $lp = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'ACCIDENTE LABORAL' AND emd_sede = '".$value['emd_sede']."' ";
	                                $al = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'ACCIDENTE TRANSITO' AND emd_sede = '".$value['emd_sede']."' ";
	                                $at = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

		    						echo '<tr>';
		    							echo "<td>".$i."</td>";
		    							echo "<td>".$value['emd_sede']."</td>";
		    							echo "<td>".$to['total']."</td>";
		    							echo "<td>".$eg['total']."</td>";
	                                    echo "<td>".$lm['total']."</td>";
	                                    echo "<td>".$lp['total']."</td>";
	                                    echo "<td>".$al['total']."</td>";
	                                    echo "<td>".$at['total']."</td>";
		    						echo '</tr>';
		    						$i++;
		    					}
			    			?>
			    		</tbody>
			    	</table>
			    </div>
			</div>

		</div>
		<div class="col-md-12">
			<div class="box box-solid box-danger">
			    <div class="box-header with-border">
			        <h3 class="box-title">Consolidado de Incapacidades por Centro de costo</h3>
			    </div>
			    <div class="box-body">
			    	<table width="100%" class="table table-hover table-bordered" id="tableMensual">
			    		<thead>
			    			<tr>
			    				<th style="width: 10px;">#</th>
			    				<th>CENTRO DE COSTO</th>
			    				<th># Incapacidades</th>
	                            <th># E-G</th>
	                            <th># L-M</th>
	                            <th># L-P</th>
	                            <th># A-L</th>
	                            <th># A-T</th>
			    			</tr>
			    		</thead>
			    		<tbody>
			    			<?php 
			    				$labelcCosto = '';
			    				$valorescCosto = '';
			    				$campos = "emd_centro_costos";
			    				$tabla = "gi_empleados";
			    				$condiciones = "emd_centro_costos is NOT NULL AND emd_centro_costos <> ''";
			    				if($_SESSION['cliente_id'] != 0){
			    					$condiciones .=" AND emd_emp_id = ".$_SESSION['cliente_id'];
		    					}  
		    					$resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condiciones, 'GROUP BY emd_centro_costos', 'ORDER BY emd_centro_costos ASC');
		    					$i = 1;
		    					foreach ($resultado as $key => $value) {

		    						$tabla  = 'gi_incapacidad JOIN gi_empleados ON emd_id = inc_emd_id';

		    						$campos = 'count(*) as total';
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND emd_centro_costos = '".$value['emd_centro_costos']."'";
	                                $to = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                if($i == 1){
		    							$labelcCosto .= "'".$value['emd_centro_costos']."'";
		    						}else{
		    							$labelcCosto .= ",'".$value['emd_centro_costos']."'";	
		    						}

		    						if($i == 1){
		    							$valorescCosto .= "".$to['total']."";
		    						}else{
		    							$valorescCosto .= ",".$to['total']."";	
		    						}


	                                $campos = 'count(*) as total';
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  AND inc_origen = 'ENFERMEDAD GENERAL' AND emd_centro_costos = '".$value['emd_centro_costos']."'";
	                                $eg = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'LICENCIA DE MATERNIDAD' AND emd_centro_costos = '".$value['emd_centro_costos']."'";
	                                $lm = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'LICENCIA DE PATERNIDAD' AND emd_centro_costos = '".$value['emd_centro_costos']."'";
	                                $lp = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'ACCIDENTE LABORAL' AND emd_centro_costos = '".$value['emd_centro_costos']."' ";
	                                $al = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'ACCIDENTE TRANSITO' AND emd_centro_costos = '".$value['emd_centro_costos']."' ";
	                                $at = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

		    						echo '<tr>';
		    							echo "<td>".$i."</td>";
		    							echo "<td>".$value['emd_centro_costos']."</td>";
		    							echo "<td>".$to['total']."</td>";
		    							echo "<td>".$eg['total']."</td>";
	                                    echo "<td>".$lm['total']."</td>";
	                                    echo "<td>".$lp['total']."</td>";
	                                    echo "<td>".$al['total']."</td>";
	                                    echo "<td>".$at['total']."</td>";
		    						echo '</tr>';

		    						$i++;
		    					}
			    			?>
			    		</tbody>
			    	</table>
			    </div>
			</div>			

		</div>
		<div class="col-md-12">
			<div class="box box-solid box-danger">
				<div class="box-header with-border">
				    <h3 class="box-title">Consolidado de Incapacidades por Ciudad</h3>
				</div>
				<div class="box-body">
					<table width="100%" class="table table-hover table-bordered" id="tableMensual">
			    		<thead>
			    			<tr>
			    				<th style="width: 10px;">#</th>
			    				<th>CIUDAD</th>
			    				<th># Incapacidades</th>
	                            <th># E-G</th>
	                            <th># L-M</th>
	                            <th># L-P</th>
	                            <th># A-L</th>
	                            <th># A-T</th>
			    			</tr>
			    		</thead>
			    		<tbody>
			    			<?php
			    				$labelciudades = '';
			    				$valorCiudades = '';
			    				$campos = "emd_ciudad";
			    				$tabla = "gi_empleados";
			    				$condiciones = "emd_ciudad is NOT NULL AND emd_ciudad <> ''";
			    				if($_SESSION['cliente_id'] != 0){
			    					$condiciones .=" AND emd_emp_id = ".$_SESSION['cliente_id'];
		    					}  
		    					$resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condiciones, 'GROUP BY emd_ciudad', 'ORDER BY emd_ciudad ASC');
		    					$i = 1;
		    					foreach ($resultado as $key => $value) {
		    						$tabla  = 'gi_incapacidad JOIN gi_empleados ON emd_id = inc_emd_id';

		    						$campos = 'count(*) as total';
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND emd_ciudad = '".$value['emd_ciudad']."'";
	                                $to = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                if($i == 1){
		    							$labelciudades .= "'".$value['emd_ciudad']."'";
		    						}else{
		    							$labelciudades .= ",'".$value['emd_ciudad']."'";	
		    						}

		    						if($i == 1){
		    							$valorCiudades .= "".$to['total']."";
		    						}else{
		    							$valorCiudades .= ",".$to['total']."";	
		    						}

	                                $campos = 'count(*) as total';
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  AND inc_origen = 'ENFERMEDAD GENERAL' AND emd_ciudad = '".$value['emd_ciudad']."'";
	                                $eg = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'LICENCIA DE MATERNIDAD' AND emd_ciudad = '".$value['emd_ciudad']."'";
	                                $lm = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
	                                
	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'LICENCIA DE PATERNIDAD' AND emd_ciudad = '".$value['emd_ciudad']."'";
	                                $lp = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'ACCIDENTE LABORAL' AND emd_ciudad = '".$value['emd_ciudad']."' ";
	                                $al = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

	                                $condicion =  $whereCliente." inc_fecha_inicio BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_origen = 'ACCIDENTE TRANSITO' AND emd_ciudad = '".$value['emd_ciudad']."' ";
	                                $at = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

		    						echo '<tr>';
		    							echo "<td>".$i."</td>";
		    							echo "<td>".$value['emd_ciudad']."</td>";
		    							echo "<td>".$to['total']."</td>";
		    							echo "<td>".$eg['total']."</td>";
	                                    echo "<td>".$lm['total']."</td>";
	                                    echo "<td>".$lp['total']."</td>";
	                                    echo "<td>".$al['total']."</td>";
	                                    echo "<td>".$at['total']."</td>";
		    						echo '</tr>';
		    						$i++;
		    					}
			    			?>
			    		</tbody>
			    	</table>
				</div>
			</div>

		</div>
		<div class="col-md-6">
			<div class="box box-solid box-danger">
				<div class="box-header with-border">
				    <h3 class="box-title">GRÁFICA - SEDES</h3>
				</div>
				<div class="box-body">
					<canvas id="myChart_sedes" style="height:100px"></canvas>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box box-solid box-danger">
				<div class="box-header with-border">
				    <h3 class="box-title">GRÁFICA - CENTRO DE COSTOS</h3>
				</div>
				<div class="box-body">
					<canvas id="myChart_ccosto" style="height:100px"></canvas>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box box-solid box-danger">
				<div class="box-header with-border">
				    <h3 class="box-title">GRÁFICA - CIUDADES</h3>
				</div>
				<div class="box-body">
					<canvas id="myChart_ciudad" style="height:100px"></canvas>
				</div>
			</div>
			<div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li><a href="#tab_3" data-toggle="tab">GRÁFICA - CIUDADES</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="tab_3">
                    	<canvas id="myChart_ciudad" style="height:250px"></canvas>
                    </div>
                </div>
            </div>
		</div>
	</div>


<script type="text/javascript">
	$(function(){
        /*crerBarraSedes();
    	crerBarraCostos();
    	crerBarraCiudades();*/
    });

	function crerBarraSedes(){
	    var ctx = document.getElementById("myChart_sedes").getContext('2d');
	    var myChart = new Chart(ctx, {
	        type: 'bar',
	        data: {
	            labels: [<?php echo $labelSede; ?>],
	            datasets: [
	                {
	                    label: 'Incapacidades por sede',
	                    data: [<?php echo $valoresSede; ?>],
	                    backgroundColor: "#bf360c"
	                }
	            ]
	      }
	    });
	}

	function crerBarraCostos(){
	    var ctx = document.getElementById("myChart_ccosto").getContext('2d');
	    var myChart = new Chart(ctx, {
	        type: 'bar',
	        data: {
	            labels: [<?php echo $labelcCosto; ?>],
	            datasets: [
	                {
	                    label: 'Incapacidades por centro de costo',
	                    data: [<?php echo $valorescCosto; ?>],
	                    backgroundColor: "#1565c0"
	                }
	            ]
	      }
	    });
	}

	function crerBarraCiudades(){
	    var ctx = document.getElementById("myChart_ciudad").getContext('2d');
	    var myChart = new Chart(ctx, {
	        type: 'bar',
	        data: {
	            labels: [<?php echo $labelciudades; ?>],
	            datasets: [
	                {
	                    label: 'Incapacidades por ciudad',
	                    data: [<?php echo $valorCiudades; ?>],
	                    backgroundColor: "#fdd835"
	                }
	            ]
	      }
	    });
	}
</script>