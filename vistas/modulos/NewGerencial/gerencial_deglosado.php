<?php
    session_start();
    require_once '/../../../controladores/mail.controlador.php';
    require_once '/../../../controladores/plantilla.controlador.php'; 
    require_once '../../../controladores/incapacidades.controlador.php';
    require_once '../../../controladores/tesoreria.controlador.php';

    require_once '/../../../modelos/dao.modelo.php';
    require_once '../../../modelos/incapacidades.modelo.php';
    require_once '../../../modelos/tesoreria.modelo.php';


	$year = $_POST['anho'];
    $otherYear =  $_POST['anhoFinal'];
?>

<div class="row">
	<div class="col-md-12">
	    <div class="box box-solid box-danger">
	        <div class="box-header">
	            <h3 class="box-title">
	                INFORME GRAFICO - INCAPACIDADES PAGADAS POR NOMINA - EMPRESA VS ADMINISTRADORA / TOTAL
	            </h3>
	        </div>
	        <div class="box-body">
	            <canvas id="pieChart_1" style="height:334px"></canvas>
	        </div>
	    </div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
	    <div class="box box-solid box-danger">
	        <div class="box-header">
	            <h3 class="box-title">
	                INFORME GRAFICO - INCAPACIDADES PAGADAS EMPRESA VS ADMINISTRADORA / MES
	            </h3>
	        </div>
	        <div class="box-body">
	            
	            <canvas id="pieChart_3" style="height:334px"></canvas>
	        </div>
	    </div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
	    <div class="box box-solid box-danger">
	        <div class="box-header">
	            <h3 class="box-title">
	                INFORME GRAFICO - INCAPACIDADES POR ADMINISTRADORA VS PAGADAS
	            </h3>
	        </div>
	        <div class="box-body">
	            
	            <canvas id="pieChart_2" style="height:334px"></canvas>
	        </div>
	    </div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	    <div class="box box-solid box-danger">
	        <div class="box-header">
	            <h3 class="box-title">
	                INFORME GRAFICO - INCAPACIDADES PAGADAS POR MES
	            </h3>
	        </div>
	        <div class="box-body">
	            
	            <canvas id="pieChart_7" style="height:334px"></canvas>
	        </div>
	    </div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
	    <div class="box box-solid box-danger">
	        <div class="box-header">
	            <h3 class="box-title">
	                INFORME - INCAPACIDADES POR SUCURSAL
	            </h3>
	        </div>
	        <div class="box-body">
	            <table class="table table-border table-hover">
                    <thead>
                        <tr>
                            <th>SUCURSAL</th>
                            <th>ENERO</th>
                            <th>FEBRERO</th>
                            <th>MARZO</th>
                            <th>ABRIL</th>
                            <th>MAYO</th>
                            <th>JUNIO</th>
                            <th>JULIO</th>
                            <th>AGOSTO</th>
                            <th>SEPTIEMBRE</th>
                            <th>OCTUBRE</th>
                            <th>NOVIEMBRE</th>
                            <th>DICIEMBRE</th>
                        </tr>
                    </thead>
                    <tbody>
<?php 
    /*Primeor obtenemos las Sedes*/
    $strCampos___ = "emd_sede";
    $strTablas___ = "gi_empleados";
    $strWhere____ = "";
    if($_POST['cliente_id'] != 0){
        $strWhere____ =  "emd_emp_id = ".$_POST['cliente_id'];
    }

    $arryRespuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY emd_sede', 'ORDER BY emd_sede ASC');
    foreach ($arryRespuesta as $key => $value) {
        if($value['emd_sede'] != null && $value['emd_sede'] != ''){
            
            echo "<tr>";
                echo "<td>".$value['emd_sede']."</td>";
                
                /*Ahora obtenemos las incapacidades por mes*/
                $strCampos___ = "count(*) as total , MONTH(inc_fecha_pago_nomina) as mes";
                $strTablas___ = "gi_incapacidad JOIN gi_empleados ON emd_id = inc_emd_id";
                $strWhere____ = '';
                if($_POST['cliente_id'] != 0){
                    $strWhere____ =  "inc_empresa = ".$_POST['cliente_id']." AND ";
                }
                $strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."' AND emd_sede = '".$value['emd_sede']."'";
                $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');
                
                $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

                $actual = 0;
                $i = 0;
                foreach ($incapacidades as $inca => $inc) {
                    /*Tenemos las incapacidades*/ 
                    $icono = "";
                    if($i > 0){
                        if($inc['total'] > $actual){
                            $icono = "<i style='color:red;' class='fa fa-arrow-up'></i>";
                        }else if($inc['total'] < $actual){
                            $icono = "<i style='color:green;' class='fa fa-arrow-down'></i>";
                        }else {
                            $icono = "<i style='color:blue;' class='fa  fa-arrows-h'></i>";
                        }    
                    }
                    
                    echo "<td> ".$icono." ".$inc['total']."</td>";
                    $actual = $inc['total'];
                    $i++;
                    //$valores .= "'".."'"; 
                }
                if($i < 12){
                    for($i; $i < 12; $i++){
                        echo "<td></td>";
                    }
                }
            echo "</tr>";    
        }
    }
echo "<tr>";
    echo "<td>INCAPACIDADES SIN SUCURSAL</td>";
    
    /*Ahora obtenemos las incapacidades por mes*/
    $strCampos___ = "count(*) as total , MONTH(inc_fecha_pago_nomina) as mes";
    $strTablas___ = "gi_incapacidad JOIN gi_empleados ON emd_id = inc_emd_id";
    $strWhere____ = '';
    if($_POST['cliente_id'] != 0){
        $strWhere____ =  "inc_empresa = ".$_POST['cliente_id']." AND ";
    }
    $strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."' AND (emd_sede IS NULL OR emd_sede = '')";
    $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');
    
    $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

    $actual = 0;
    $i = 0;
    foreach ($incapacidades as $inca => $inc) {
        /*Tenemos las incapacidades*/ 
        $icono = "";
        if($i > 0){
            if($inc['total'] > $actual){
                $icono = "<i style='color:red;' class='fa fa-arrow-up'></i>";
            }else if($inc['total'] < $actual){
                $icono = "<i style='color:green;' class='fa fa-arrow-down'></i>";
            }else {
                $icono = "<i style='color:blue;' class='fa  fa-arrows-h'></i>";
            }    
        }
        
        echo "<td> ".$icono." ".$inc['total']."</td>";
        $actual = $inc['total'];
        $i++;
        //$valores .= "'".."'"; 
    }

    if($i < 12){
        for($j = $i; $j< 12; $j++){
            echo "<td>&nbsp;</td>";
        }
    }
echo "</tr>"; 
?>
                    </tbody>
                </table>
                
	        </div>
	    </div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	    <div class="box box-solid box-danger">
	        <div class="box-header">
	            <h3 class="box-title">
	                INFORME - INCAPACIDADES POR CENTRO DE COSTOS
	            </h3>
	        </div>
	        <div class="box-body">
	                           <table class="table table-border table-hover">
                    <thead>
                        <tr>
                            <th>CENTRO DE COSTO</th>
                            <th>ENERO</th>
                            <th>FEBRERO</th>
                            <th>MARZO</th>
                            <th>ABRIL</th>
                            <th>MAYO</th>
                            <th>JUNIO</th>
                            <th>JULIO</th>
                            <th>AGOSTO</th>
                            <th>SEPTIEMBRE</th>
                            <th>OCTUBRE</th>
                            <th>NOVIEMBRE</th>
                            <th>DICIEMBRE</th>
                        </tr>
                    </thead>
                    <tbody>
<?php 
    /*Primeor obtenemos las Sedes*/
    $strCampos___ = "emd_centro_costos";
    $strTablas___ = "gi_empleados";
    $strWhere____ = "";
    if($_POST['cliente_id'] != 0){
        $strWhere____ =  "emd_emp_id = ".$_POST['cliente_id'];
    }

    $arryRespuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY emd_centro_costos', 'ORDER BY emd_centro_costos ASC');
    foreach ($arryRespuesta as $key => $value) {
        if($value['emd_centro_costos'] != null && $value['emd_centro_costos'] != ''){
            
            echo "<tr>";
                echo "<td>".$value['emd_centro_costos']."</td>";
                
                /*Ahora obtenemos las incapacidades por mes*/
                $strCampos___ = "count(*) as total , MONTH(inc_fecha_pago_nomina) as mes";
                $strTablas___ = "gi_incapacidad JOIN gi_empleados ON emd_id = inc_emd_id";
                $strWhere____ = '';
                if($_POST['cliente_id'] != 0){
                    $strWhere____ =  "inc_empresa = ".$_POST['cliente_id']." AND ";
                }
                $strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."' AND emd_centro_costos = '".$value['emd_centro_costos']."'";
                $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');
                
                $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

                $actual = 0;
                $i = 0;
                foreach ($incapacidades as $inca => $inc) {
                    /*Tenemos las incapacidades*/ 
                    $icono = "";
                    if($i > 0){
                        if($inc['total'] > $actual){
                            $icono = "<i style='color:red;' class='fa fa-arrow-up'></i>";
                        }else if($inc['total'] < $actual){
                            $icono = "<i style='color:green;' class='fa fa-arrow-down'></i>";
                        }else {
                            $icono = "<i style='color:blue;' class='fa  fa-arrows-h'></i>";
                        }    
                    }
                    
                    echo "<td> ".$icono." ".$inc['total']."</td>";
                    $actual = $inc['total'];
                    $i++;
                    //$valores .= "'".."'"; 
                }

                if($i < 12){
                    for($i; $i < 12; $i++){
                        echo "<td></td>";
                    }
                }

            echo "</tr>";    
        }
        
    }

    echo "<tr>";
    echo "<td>INCAPACIDADES SIN CENTRO DE COSTO</td>";
    
    /*Ahora obtenemos las incapacidades por mes*/
    $strCampos___ = "count(*) as total , MONTH(inc_fecha_pago_nomina) as mes";
    $strTablas___ = "gi_incapacidad JOIN gi_empleados ON emd_id = inc_emd_id";
    $strWhere____ = '';
    if($_POST['cliente_id'] != 0){
        $strWhere____ =  "inc_empresa = ".$_POST['cliente_id']." AND ";
    }
    $strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."' AND (emd_centro_costos IS NULL OR emd_centro_costos = '')";
    $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');
    
    $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

    $actual = 0;
    $i = 0;
    foreach ($incapacidades as $inca => $inc) {
        /*Tenemos las incapacidades*/ 
        $icono = "";
        if($i > 0){
            if($inc['total'] > $actual){
                $icono = "<i style='color:red;' class='fa fa-arrow-up'></i>";
            }else if($inc['total'] < $actual){
                $icono = "<i style='color:green;' class='fa fa-arrow-down'></i>";
            }else {
                $icono = "<i style='color:blue;' class='fa  fa-arrows-h'></i>";
            }    
        }
        
        echo "<td> ".$icono." ".$inc['total']."</td>";
        $actual = $inc['total'];
        $i++;
        //$valores .= "'".."'"; 
    }

    if($i < 12){

        for($j = $i; $j< 12; $j++){
            echo "<td>&nbsp;</td>";
        }
    }
echo "</tr>"; 
?>
                    </tbody>
                </table>   
	        </div>
	    </div>
	</div>
</div>
<script type="text/javascript">
    
</script>
<?php 
	$month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
?>

<script type="text/javascript">
Chart.pluginService.register({
    beforeRender: function (chart) {
        if (chart.config.options.showAllTooltips) {
            // create an array of tooltips
            // we can't use the chart tooltip because there is only one tooltip per chart
            chart.pluginTooltips = [];
            chart.config.data.datasets.forEach(function (dataset, i) {
                chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                    chart.pluginTooltips.push(new Chart.Tooltip({
                        _chart: chart.chart,
                        _chartInstance: chart,
                        _data: chart.data,
                        _options: chart.options,
                        _active: [sector]
                    }, chart));
                });
            });

            // turn off normal tooltips
            chart.options.tooltips.enabled = false;
        }
    },
    afterDraw: function (chart, easing) {
        if (chart.config.options.showAllTooltips) {
            // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
            if (!chart.allTooltipsOnce) {
                if (easing !== 1)
                    return;
                chart.allTooltipsOnce = true;
            }

            // turn on tooltips
            chart.options.tooltips.enabled = true;
            Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                tooltip.initialize();
                tooltip.update();
                // we don't actually need this since we are not animating tooltips
                tooltip.pivot();
                tooltip.transition(easing).draw();
            });
            chart.options.tooltips.enabled = false;
        }
    }
});


	/*Este es para las incapacidades por empresa vs Administradora, primer grafica*/
	<?php 
		$strCampos___ = "count(*) as total , MONTH(inc_fecha_pago_nomina) as mes";
		$strTablas___ = "gi_incapacidad";
		$strWhere____ = '';
        if($_POST['cliente_id'] != 0){
            $strWhere____ =  "inc_empresa = ".$_POST['cliente_id']." AND ";
        }
		$strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."' AND inc_estado_tramite =  'EMPRESA' ";
		$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');

		$nombres = '';
        $valores = '';
        $i = 0;

        foreach ($incapacidades as $key => $value) {
            $numero = $value['mes'];
            if($i == 0){
                $nombres .= "'".$month[$numero -1]."'";
                $valores .= "'".$value['total']."'"; 
            }else{
                $nombres .= " , '".$month[$numero -1]."'";
                $valores .= " , '".$value['total']."'"; 
            }
            $i++;
        }


        $strCampos___ = "count(*) as total , MONTH(inc_fecha_pago_nomina) as mes";
		$strTablas___ = "gi_incapacidad";
		$strWhere____ = '';
        if($_POST['cliente_id'] != 0){
            $strWhere____ =  "inc_empresa = ".$_POST['cliente_id']." AND ";
        }
		$strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."' AND inc_estado_tramite !=  'EMPRESA' ";
		$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');

        $valoresP = '';
        $i = 0;

        foreach ($incapacidades as $key => $value) {
    
            if($i == 0){
                $valoresP .= "'".$value['total']."'"; 
            }else{
                $valoresP .= " , '".$value['total']."'"; 
            }
            $i++;
        }

	?>
	var densityCanvas = document.getElementById("pieChart_1").getContext('2d');

    var densityData = {
        label: 'Inc. Empresa',
        data: [<?php echo $valores;?>],
        borderColor:'#ff6384',
        backgroundColor : '#ff6384',
        borderWidth: 2,
        hoverBorderWidth: 0,
        fill: false
    };

    var densityData_pagadas = {
        label: 'Inc. Administradora',
        data: [<?php echo $valoresP; ?>],
        borderColor:'#36a2eb',
        backgroundColor : '#36a2eb',
        borderWidth: 2,
        hoverBorderWidth: 0,
        fill: false
    };

    var barChart = new Chart(densityCanvas, {
        type: 'bar',
        data: {
            labels: [<?php echo $nombres; ?>],
            datasets: [densityData, densityData_pagadas],
        },
        options:{
            showAllTooltips: true,
            tooltips: {
                position : "bottom"
            }
        }
    });
</script>


<script type="text/javascript">
	/*Este es para las incapacidades pagadas por empresa vs pagadas por Administradora*/
	<?php 
		$strCampos___ = "sum(inc_valor_pagado_empresa) as total , MONTH(inc_fecha_pago_nomina) as mes";
		$strTablas___ = "gi_incapacidad";
		$strWhere____ = '';
        if($_POST['cliente_id'] != 0){
            $strWhere____ =  "inc_empresa = ".$_POST['cliente_id']." AND ";
        }
		$strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."' AND inc_estado_tramite =  'EMPRESA' ";
		$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');

		$nombres = '';
        $valores = '';
        $i = 0;

        foreach ($incapacidades as $key => $value) {
            $numero = $value['mes'];
            if($i == 0){
                $nombres .= "'".$month[$numero -1]."'";
                $valores .= "'".$value['total']."'"; 
            }else{
                $nombres .= " , '".$month[$numero -1]."'";
                $valores .= " , '".$value['total']."'"; 
            }
            $i++;
        }


        $strCampos___ = "sum(inc_valor) as total , MONTH(inc_fecha_pago_nomina) as mes";
		$strTablas___ = "gi_incapacidad";
		$strWhere____ = '';
        if($_POST['cliente_id'] != 0){
            $strWhere____ =  "inc_empresa = ".$_POST['cliente_id']." AND ";
        }
		$strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."' AND inc_estado_tramite =  'PAGADA' ";
		$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');

        $valoresP = '';
        $i = 0;

        foreach ($incapacidades as $key => $value) {
    
            if($i == 0){
                $valoresP .= "'".$value['total']."'"; 
            }else{
                $valoresP .= " , '".$value['total']."'"; 
            }
            $i++;
        }

	?>
	var densityCanvas = document.getElementById("pieChart_3").getContext('2d');

    var densityData = {
        label: 'Inc. Empresa',
        data: [<?php echo $valores;?>],
        borderColor:'#ff6384',
        backgroundColor : '#ff6384',
        borderWidth: 2,
        hoverBorderWidth: 0,
        fill: false
    };

    var densityData_pagadas = {
        label: 'Inc. Administradora',
        data: [<?php echo $valoresP; ?>],
        borderColor:'#36a2eb',
        backgroundColor : '#36a2eb',
        borderWidth: 2,
        hoverBorderWidth: 0,
        fill: false
    };

    var barChart = new Chart(densityCanvas, {
        type: 'bar',
        data: {
            labels: [<?php echo $nombres; ?>],
            datasets: [densityData, densityData_pagadas],
        },
        options:{
            showAllTooltips: true,
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        return "Valor Pagado : $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                        });
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        callback: function (value) {
                            return addCommas(value)
                        }
                    }
                }]
            }
        }
    });

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function scaleLabel (valuePayload) {
        return Number(valuePayload.value).toFixed(2).replace('.',',') + '$';
    }

    function getRandomColor() {
       return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
    }
</script>


<script type="text/javascript">
	/*Este es para las incapacidades pagadas por empresa vs pagadas por Administradora*/
	<?php 
		$strCampos___ = "sum(inc_valor_pagado_eps) as total , MONTH(inc_fecha_pago_nomina) as mes";
		$strTablas___ = "gi_incapacidad";
		$strWhere____ = '';
        if($_POST['cliente_id'] != 0){
            $strWhere____ =  "inc_empresa = ".$_POST['cliente_id']." AND ";
        }
		$strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."' AND inc_estado_tramite !=  'EMPRESA' ";
		$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');

		$nombres = '';
        $valores = '';
        $i = 0;

        foreach ($incapacidades as $key => $value) {
            $numero = $value['mes'];
            if($i == 0){
                $nombres .= "'".$month[$numero -1]."'";
                $valores .= "'".$value['total']."'"; 
            }else{
                $nombres .= " , '".$month[$numero -1]."'";
                $valores .= " , '".$value['total']."'"; 
            }
            $i++;
        }


        $strCampos___ = "sum(inc_valor) as total , MONTH(inc_fecha_pago_nomina) as mes";
		$strTablas___ = "gi_incapacidad";
		$strWhere____ = '';
        if($_POST['cliente_id'] != 0){
            $strWhere____ =  "inc_empresa = ".$_POST['cliente_id']." AND ";
        }
		$strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."' AND inc_estado_tramite =  'PAGADA' ";
		$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');

        $valoresP = '';
        $i = 0;

        foreach ($incapacidades as $key => $value) {
    
            if($i == 0){
                $valoresP .= "'".$value['total']."'"; 
            }else{
                $valoresP .= " , '".$value['total']."'"; 
            }
            $i++;
        }

	?>
	var densityCanvas = document.getElementById("pieChart_2").getContext('2d');

    var densityData = {
        label: 'Inc. Administradora Total',
        data: [<?php echo $valores;?>],
        borderColor:'#ff6384',
        backgroundColor : '#ff6384',
        borderWidth: 2,
        hoverBorderWidth: 0,
        fill: false
    };

    var densityData_pagadas = {
        label: 'Inc. Administradora Pagadas',
        data: [<?php echo $valoresP; ?>],
        borderColor:'#36a2eb',
        backgroundColor : '#36a2eb',
        borderWidth: 2,
        hoverBorderWidth: 0,
        fill: false
    };

    var barChart = new Chart(densityCanvas, {
        type: 'line',
        data: {
            labels: [<?php echo $nombres; ?>],
            datasets: [densityData, densityData_pagadas],
        },
         options:{
            showAllTooltips: true,
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        return "Valor: $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                        });
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        callback: function (value) {
                            return addCommas(value)
                        }
                    }
                }]
            }
        }
    });

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function scaleLabel (valuePayload) {
        return Number(valuePayload.value).toFixed(2).replace('.',',') + '$';
    }

    function getRandomColor() {
       return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
    }
</script>


<script type="text/javascript">
	/*Este es para las incapacidades pagadas  Administradora pr mes*/
	<?php 
		

		$nombres = '';
        $valores = '';
        $i = 0;

        

        $strCampos___ = "sum(inc_valor) as total , MONTH(inc_fecha_pago_nomina) as mes";
		$strTablas___ = "gi_incapacidad";
		$strWhere____ = '';
        if($_POST['cliente_id'] != 0){
            $strWhere____ =  "inc_empresa = ".$_POST['cliente_id']." AND ";
        }
		$strWhere____ .= " inc_fecha_pago_nomina BETWEEN '".$_POST['anho']."' AND '".$_POST['anhoFinal']."' AND inc_estado_tramite =  'PAGADA' ";
		$incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos___, $strTablas___, $strWhere____, 'GROUP BY mes', 'ORDER BY mes ASC');

        foreach ($incapacidades as $key => $value) {
            $numero = $value['mes'];
            if($i == 0){
                $nombres .= "'".$month[$numero -1]."'";
                $valores .= "'".$value['total']."'"; 
            }else{
                $nombres .= " , '".$month[$numero -1]."'";
                $valores .= " , '".$value['total']."'"; 
            }
            $i++;
        }


	?>
	var densityCanvas = document.getElementById("pieChart_7").getContext('2d');

    var densityData = {
        label: 'Incapacidades Pagadas',
        data: [<?php echo $valores;?>],
        borderColor:'#36a2eb',
        backgroundColor : '#36a2eb',
        borderWidth: 2,
        hoverBorderWidth: 0,
        fill: false
    };

 

    var barChart = new Chart(densityCanvas, {
        type: 'line',
        data: {
            labels: [<?php echo $nombres; ?>],
            datasets: [densityData],
        },
        options:{
            showAllTooltips: true,
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        return "Valor: $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                        });
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        callback: function (value) {
                            return addCommas(value)
                        }
                    }
                }]
            }
        }
    });

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function scaleLabel (valuePayload) {
        return Number(valuePayload.value).toFixed(2).replace('.',',') + '$';
    }

    function getRandomColor() {
       return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
    }
</script>
