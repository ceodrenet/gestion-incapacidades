<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Informe Gerencial 
        </h1>
        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li><a href="reportes">Reportes</a></li>
            <li class="active">Informe Gerencial</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-danger box-solid">
            <div class="box-header">
                <h3 class="box-title">Filtros</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <form method="post" autocomplete="off">
                        <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['cliente_id'];?>">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar-o"></i>
                                    </span>
                                    <?php 
                                        $fecha = date('Y-m-d');
                                        $nuevafecha = strtotime (' - 1 month' , strtotime ( $fecha ) ) ;
                                        $nuevafecha = date('Y-m-d',$nuevafecha);
                                    ?>
                                    <input class="form-control fecha" type="text" name="NuevoFechaInicio" required id="NuevoFechaInicio2" placeholder="Ingresar Fecha Inicio" required="true" value="<?php echo $nuevafecha;?>">
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar-o"></i>
                                    </span>
                                    <input class="form-control fecha" type="text" name="NuevoFechaFinal" required id="NuevoFechaInicio3" placeholder="Ingresar Fecha Final" required="true" value="<?php echo date('Y-m-d');?>">
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <button class="btn btn-primary btn-block" type="button" id="btnBuscarRangos_general"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <a href="#" target="_blank" class="btn btn-primary btn-block" id="btnExportarReporteGerencial"><i class="fa fa-file-excel-o"></i>&nbsp;Exportar</a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" id="resultados">
                
            </div>
        </div>   
    </section>
</div>


<script type="text/javascript">
    $(function(){

        getDatosGerencial();

        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
            daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Today",
            clear: "Clear",
            format: "yyyy-mm-dd", 
            weekStart: 0
        };

        $("#NuevoFechaInicio2").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#NuevoFechaInicio3').datepicker('setStartDate', minDate);
        });


        $("#NuevoFechaInicio3").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });



        $("#btnBuscarRangos_general").click(function(){
            getDatosGerencial();
        });
        
        $("#btnExportarReporteGerencial").click(function(){
            var fechaInicio = $("#NuevoFechaInicio2").val();
            var fechaFinal  = $("#NuevoFechaInicio3").val();
            var paso = 0;
            if(fechaInicio.length < 0){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "La fecha inicial es necesaria",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });

            } 

            if(fechaFinal.length < 0){
                paso = 1;
                swal({
                    title : "Error al buscar",
                    text  : "La fecha final es necesaria",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });

            }
        });     

    });


function getDatosGerencial(){
    var fechaInicio = $("#NuevoFechaInicio2").val();
    var fechaFinal = $("#NuevoFechaInicio3").val();
    var paso = 0;
    if(fechaInicio.length < 0){
        paso = 1;
        swal({
            title : "Error al buscar",
            text  : "La fecha inicial es necesaria",
            type  : "error",
            confirmButtonText : "Cerrar"
        });

    } 

    if(fechaFinal.length < 0){
        paso = 1;
        swal({
            title : "Error al buscar",
            text  : "La fecha final es necesaria",
            type  : "error",
            confirmButtonText : "Cerrar"
        });

    }  

    if(paso == 0){
        $.ajax({
            url    : 'vistas/modulos/NewGerencial/gerencial_deglosado.php',
            type   : 'post',
            data   :{
                anho  : fechaInicio,
                anhoFinal  : fechaFinal,
                cliente_id    : $("#session").val()
            },
            dataType : 'html',
            beforeSend:function(){
                $.blockUI({ 
                    baseZ: 2000,
                    message : '<h3>Un momento por favor....</h3>',
                    css: { 
                        border: 'none', 
                        padding: '1px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5, 
                        color: '#fff'
                    } 
                }); 
                $("#btnExportarReporteGerencial").attr('href', 'index.php?exportar=pdfGerencial&NuevoFechaInicial='+$("#NuevoFechaInicio2").val()+'&NuevoFechaFinal='+$("#NuevoFechaInicio3").val());
            },
            complete:function(){
                $.unblockUI();
            },
            success : function(data){
                $("#resultados").html(data);
                
            }
        })
    } 
}
</script>
