<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REPORTES</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Reportes</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-md-6">
                <div class="card border ">
                    <div class="card-header bg-transparent border-danger">
                        <h5 class="my-0 text-danger">
                           <i class="fas fa-users"></i>&nbsp;REPORTES NOMINA
                        </h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <?php echo drawTableReports(13);?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card border ">
                    <div class="card-header bg-transparent border-danger">
                        <h5 class="my-0 text-danger">
                            <i class="fas fa-chart-bar"></i>&nbsp;REPORTES GERENCIA
                        </h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <?php echo drawTableReports(14);?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card border ">
                    <div class="card-header bg-transparent border-danger">
                        <h5 class="my-0 text-danger">
                            <i class="fas fa-file-medical-alt"></i>&nbsp;REPORTES SEGURIDAD Y SALUD
                        </h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <?php echo drawTableReports(12);?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card border ">
                    <div class="card-header bg-transparent border-danger">
                        <h5 class="my-0 text-danger">
                            <i class="fa fa-money-bill-wave"></i>&nbsp;REPORTES TESORERIA
                        </h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <?php echo drawTableReports(9);?>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<?php 
    function drawTableReports($reporteId){
        $submenus = ControladorPerfiles::ctrMostrarOpcionesReportePerfil($_SESSION['perfil'] , $reporteId);
        $result = '';
        foreach ($submenus as $key) {
            $result .= '<tr>';
                $result .= '<td>
                        <a href="'.$key['submenu_href'].'">
                            '.$key['submenu_nombre'].'
                        </a>
                    </td>';
            $result .= '</tr>';
        }
        return $result;
    }
?>