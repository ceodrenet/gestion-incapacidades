<?php
    $fecha_para_buscar = "inc_fecha_pago_nomina";
    if(isset($tipoReporte)){
        $fecha_para_buscar = "inc_fecha_pago";
    }
?>
<style type="text/css">
    .info-box{
        cursor: pointer;
    }
</style>
<div class="row">
    <div class="col-lg-2 col-xs-4">
        <div class="info-box info-box-click bg-aqua" nombre="EMPRESA" clasificacion='0' estadoTramite='EMPRESA'>
            <span class="info-box-icon"><i class="fa fa-building"></i></span>
            <?php
               
               $campos = "count(*) as estado";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = "".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }

                $respuestaEmpresa = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                $campos = "count(*) as estado";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' AND inc_valor_pagado_empresa IS NOT NULL AND inc_valor_pagado_empresa != ''";
                }else{
                    $condiciones = " ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' AND inc_valor_pagado_empresa IS NOT NULL AND inc_valor_pagado_empresa != '' ";
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaEmpresa['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaEmpresa['estado'];
                } else{
                    $porcentaje = 0;
                }
                
            ?>
                
            <div class="info-box-content">
                <span class="">EMPRESA</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>

    <div class="col-lg-2 col-xs-4">
        <!-- small box -->
        <div class="info-box info-box-click bg-aqua" nombre="POR RADICAR" clasificacion='0' estadoTramite='POR RADICAR'>
            <span class="info-box-icon"><i class="ion ion-android-bicycle"></i></span>
            <?php
                $campos = "count(inc_estado_tramite) as estado";
                $tabla = "gi_incapacidad";
                $condiciones = "inc_estado_tramite =  'POR RADICAR' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' ";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaX['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
                } else{
                    $porcentaje = 0;
                }
                
            ?>
            <div class="info-box-content">
                <span class="info-box-text">X RADICAR</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>

    </div>
    <!-- ./col -->
    <div class="col-lg-2 col-xs-4">
        <div class="info-box info-box-click bg-aqua" nombre="RADICADA" clasificacion='0' estadoTramite='RADICADA'>
            <span class="info-box-icon"><i class="ion-android-checkbox"></i></span>
            <?php
                $campos = "count(inc_estado_tramite) as estado";
                $tabla = "gi_incapacidad";
                $condiciones = "inc_estado_tramite =  'RADICADA' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' ";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaX['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
                }else{
                    $porcentaje = 0;
                }
                
            ?>
            <div class="info-box-content">
                <span class="info-box-text">RADICADAS</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>

    </div>
    <!-- ./col -->
    <div class="col-lg-2 col-xs-4">
        <div class="info-box info-box-click bg-aqua" nombre="SOLICITUD DE PAGO" clasificacion='0' estadoTramite='SOLICITUD DE PAGO'>
            <span class="info-box-icon"><i class="ion-android-done-all"></i></span>
            <?php

                $campos = "count(inc_estado_tramite) as estado";
                $tabla = "gi_incapacidad";
                $condiciones = "inc_estado_tramite =  'SOLICITUD DE PAGO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' ";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaX['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
                }else{
                    $porcentaje = 0;
                }
                
            ?>
            <div class="info-box-content">
                <span class="info-box-text">S. DE PAGO</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-2 col-xs-4">
        <div class="info-box info-box-click bg-aqua" nombre="PAGADA" clasificacion='0' estadoTramite='PAGADA'>
            <span class="info-box-icon"><i class="ion ion-cash"></i></span>
            <?php

                $campos = "count(inc_estado_tramite) as estado";
                $tabla = "gi_incapacidad";
                $condiciones = "inc_estado_tramite =  'PAGADA' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' ";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaX['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
                }else{
                    $porcentaje = 0;
                }
                
            ?>
            <div class="info-box-content">
                <span class="info-box-text">PAGADAS</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>

    <div class="col-lg-2 col-xs-4">
        <div class="info-box info-box-click bg-aqua" nombre="SIN RECONOCIMIENTO" clasificacion='0' estadoTramite='SIN RECONOCIMIENTO'>
            <span class="info-box-icon"><i class="fa fa-times"></i></span>
            <?php

                $campos = "count(inc_estado_tramite) as estado";
                $tabla = "gi_incapacidad";
                $condiciones = "inc_estado_tramite =  'SIN RECONOCIMIENTO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' ";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaX['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
                }else{
                    $porcentaje = 0;
                }
                
            ?>
            <div class="info-box-content">
                <span class="">SIN REC.</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
<!-- ./col -->
</div>

<div class="row">
    <div class="col-lg-2 col-xs-4">
        <div class="info-box info-box-click bg-green" nombre="EMPRESA" clasificacion='0' estadoTramite='EMPRESA'>
            <span class="info-box-icon"><i class="ion ion-cash"></i></span>
            <?php
               
                $campos = "sum(inc_valor_pagado_eps) as total ";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones = "inc_estado_tramite !=  'EMPRESA' AND inc_empresa = ".$_SESSION['cliente_id']." AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = "inc_estado_tramite !=  'EMPRESA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                }
                $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                $totaValor = $respuesta['total'];
                $intVariablePorcentage = $totaValor;



                $campos = "sum(inc_valor_pagado_empresa) as total";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_valor_pagado_empresa IS NOT NULL AND  ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = " inc_valor_pagado_empresa IS NOT NULL AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }
                $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                $totaValor1 = $respuesta2['total'];
                
                $intVariablePorcentage += $totaValor1;

                /*Obtenemos los porcentajes*/
                $porcentaje = 0;
                if($intVariablePorcentage > 0){
                    $porcentaje =  ($totaValor1 * 100) / $intVariablePorcentage;
                }
            ?>
                
            <div class="info-box-content">
                <span class="">EMPRESA</span>
                <span class="info-box-number" style="font-size:15px;"><?php echo number_format($totaValor1, 0, ',', '.'); ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>

    <div class="col-lg-2 col-xs-4">
        <!-- small box -->
        <div class="info-box info-box-click bg-green" nombre="POR RADICAR" clasificacion='0' estadoTramite='POR RADICAR'>
            <span class="info-box-icon"><i class="ion ion-cash"></i></span>
            <?php
                $campos = "sum(inc_valor_pagado_eps) as total";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'POR RADICAR' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = "inc_estado_tramite = 'POR RADICAR' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }
                $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);


                $totaValor1 = $respuesta2['total'];
               
                if($totaValor > 0){
                    $porcentaje =  ($totaValor1 * 100) / $totaValor;
                }
            ?>
            <div class="info-box-content">
                <span class="info-box-text">X RADICAR</span>
                <span class="info-box-number" style="font-size:15px;"><?php echo number_format($totaValor1, 0, ',', '.'); ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>

    </div>
    <!-- ./col -->
    <div class="col-lg-2 col-xs-4">
        <div class="info-box info-box-click bg-green" nombre="RADICADA" clasificacion='0' estadoTramite='RADICADA'>
            <span class="info-box-icon"><i class="ion ion-cash"></i></span>
            <?php
                $campos = "sum(inc_valor_pagado_eps) as total";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'RADICADA' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = "inc_estado_tramite = 'RADICADA' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }
                $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                $totaValor1 = $respuesta2['total'];
                if($totaValor > 0){
                    $porcentaje =  ($totaValor1 * 100) / $totaValor;
                }
            ?>
            <div class="info-box-content">
                <span class="info-box-text">RADICADAS</span>
                <span class="info-box-number" style="font-size:15px;"><?php echo number_format($totaValor1, 0, ',', '.'); ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>

    </div>
    <!-- ./col -->
    <div class="col-lg-2 col-xs-4">
        <div class="info-box info-box-click bg-green" nombre="SOLICITUD DE PAGO" clasificacion='0' estadoTramite='SOLICITUD DE PAGO'>
            <span class="info-box-icon"><i class="ion ion-cash"></i></span>
            <?php

                $campos = "sum(inc_valor_pagado_eps) as total";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'SOLICITUD DE PAGO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = "inc_estado_tramite = 'SOLICITUD DE PAGO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }
                $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                $totaValor1 = $respuesta2['total'];
                if($totaValor > 0){
                    $porcentaje =  ($totaValor1 * 100) / $totaValor;
                }
            ?>
            <div class="info-box-content">
                <span class="info-box-text">S. DE PAGO</span>
                <span class="info-box-number" style="font-size:15px;"><?php echo number_format($totaValor1, 0, ',', '.'); ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-2 col-xs-4">
        <div class="info-box info-box-click bg-green" nombre="PAGADA" clasificacion='0' estadoTramite='PAGADA'>
            <span class="info-box-icon"><i class="ion ion-cash"></i></span>
            <?php

                $campos = "sum(inc_valor) as total";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'PAGADA' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = "inc_estado_tramite = 'PAGADA' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }
                $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                $totaValor1 = $respuesta2['total'];
                if($totaValor > 0){
                    $porcentaje =  ($totaValor1 * 100) / $totaValor;
                }
            ?>
            <div class="info-box-content">
                <span class="info-box-text">PAGADAS</span>
                <span class="info-box-number" style="font-size:15px;"><?php echo number_format($totaValor1, 0, ',', '.'); ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>

    <div class="col-lg-2 col-xs-4">
        <div class="info-box info-box-click bg-green" nombre="SIN RECONOCIMIENTO" clasificacion='0' estadoTramite='SIN RECONOCIMIENTO'>
            <span class="info-box-icon"><i class="ion ion-cash"></i></span>
            <?php

                $campos = "sum(inc_valor_pagado_eps) as total";
                $tabla = "gi_incapacidad";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'SIN RECONOCIMIENTO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }else{
                    $condiciones = "inc_estado_tramite = 'SIN RECONOCIMIENTO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."'";
                }
                $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                $totaValor1 = $respuesta2['total'];
                if($totaValor > 0){
                    $porcentaje =  ($totaValor1 * 100) / $totaValor;
                }
            ?>
            <div class="info-box-content">
                <span class="">SIN REC.</span>
                <span class="info-box-number" style="font-size:15px;"><?php echo number_format($totaValor1, 0, ',', '.'); ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
<!-- ./col -->
</div>

<?php 
    $campos = "count(*) as estado";
    $tabla = "gi_incapacidad";
    if($_SESSION['cliente_id'] != 0){
        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }else{
        $condiciones = "inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }

    $respuestaXY = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
?>
<div class="row">
    <div class="col-lg-2 col-xs-4">
    <!-- small box -->
        <div class="info-box info-box-click bg-green" nombre="ENFERMEDAD GENERAL" clasificacion='X' estadoTramite='ENFERMEDAD GENERAL'>
            <span class="info-box-icon"><i class="fa fa-heartbeat"></i></span>
            <?php
                $campos = "count(*) as estado";
                $tabla = "gi_incapacidad";
                $condiciones = "inc_origen = 'ENFERMEDAD GENERAL' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' ";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaXY['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaXY['estado'];
                }else{
                    $porcentaje = 0;
                }
                
            ?>
            <div class="info-box-content">
                <span class="info-box-text">E. GENERAL</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <!-- ./col # -->
    <div class="col-lg-2 col-xs-4">
        <div class="info-box" style="background-color: #ff6384 !important; color: #FFF;" nombre="LICENCIA DE MATERNIDAD" clasificacion='X' estadoTramite='LICENCIA DE MATERNIDAD'>
            <span class="info-box-icon"><i class="fa fa-female"></i></span>
            <?php

                $campos = "count(*) as estado";
                $tabla = "gi_incapacidad";
                $condiciones = "inc_origen = 'LICENCIA DE MATERNIDAD' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' ";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaXY['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaXY['estado'];
                }else{
                    $porcentaje = 0;
                }
                
            ?>
            <div class="info-box-content">
                <span class="">MATERNIDAD</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>
                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>

    <div class="col-lg-2 col-xs-4">
        <!-- small box --> 
        <div class="info-box info-box-click bg-aqua" nombre="LICENCIA DE PATERNIDAD" clasificacion='X' estadoTramite='LICENCIA DE PATERNIDAD'>
            <span class="info-box-icon"><i class="fa fa-street-view"></i></span>
            <?php
                $campos = "count(*) as estado";
                $tabla = "gi_incapacidad";
                $condiciones = "inc_origen =  'LICENCIA DE PATERNIDAD' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' ";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaXY['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaXY['estado'];
                }else{
                    $porcentaje = 0;
                }
                
            ?>
            <div class="info-box-content">
                <span class="">PATERNIDAD</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>

    <!-- ./col -->
    <div class="col-lg-2 col-xs-4">
        <div class="info-box info-box-click bg-red" nombre="ACCIDENTE LABORAL" clasificacion='X' estadoTramite='ACCIDENTE LABORAL'>
            <span class="info-box-icon"><i class="fa fa-wheelchair"></i></span>
            <?php
                $campos = "count(*) as estado";
                $tabla = "gi_incapacidad";
                $condiciones = "inc_origen =  'ACCIDENTE LABORAL' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' ";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaXY['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaXY['estado'];
                }else{
                    $porcentaje = 0;
                }
                
            ?>
            <div class="info-box-content">
                <span class="info-box-text">A. LABORAL</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-2 col-xs-4">
        <div class="info-box" style="background-color: #e65100 !important; color: #FFF;" nombre="ACCIDENTE TRANSITO" clasificacion='X' estadoTramite='ACCIDENTE TRANSITO'>
            <span class="info-box-icon"><i class="fa fa-motorcycle"></i></span>
            <?php
                $campos = "count(*) as estado";
                $tabla = "gi_incapacidad";
                $condiciones = "inc_origen =  'ACCIDENTE TRANSITO' AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' ";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaX['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
                }else{
                    $porcentaje = 0;
                }
                
            ?>
            <div class="info-box-content">
                <span class="info-box-text">A. TRANSITO</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>

    <div class="col-lg-2 col-xs-4">
        <div class="info-box" style="background-color: #4e342e !important; color: #FFF;" nombre="+ 180 DÍAS" clasificacion='4' estadoTramite='+ 180 DÍAS'>
            <span class="info-box-icon"><i class="fa fa-plus"></i></span>
            <?php
                $item = 'inc_clasificacion';
                $value = '4';
                $campos = "count(inc_estado_tramite) as estado";
                $tabla = "gi_incapacidad";
                $condiciones = "inc_clasificacion =  4 AND ".$fecha_para_buscar." BETWEEN '".$year."' AND '".$otherYear."' ";
                if($_SESSION['cliente_id'] != 0){
                    $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
                }
                $respuesta =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                if($respuestaX['estado']){
                    $porcentaje = $respuesta['estado'] * 100 / $respuestaX['estado'];
                }else{
                    $porcentaje = 0;
                }
                
            ?>
            <div class="info-box-content">
                <span class="info-box-text">+ 180 DÍAS</span>
                <span class="info-box-number"><?php echo $respuesta['estado']; ?></span>

                <div class="progress">
                    <div class="progress-bar" style="width: <?php echo $porcentaje; ?>%"></div>
                </div>
                <span class="progress-description">
                    <?php echo number_format($porcentaje, 2)." %";?>
                </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
<!-- ./col -->
</div>

<div id="modalAgregarCliente" class="modal fade"  data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background: #dd4b39;color: white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="ttitleH4">INCAPACIDADES REPORTADAS POR </h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12" id="respuestaAjax">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnExportarIncapacidades" class="btn btn-primary pull-right" title="Exportar a Excel">
                        <i class="fa fa-file-excel-o"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function(){
        $(".info-box-click").click(function(){
            var tipoClasificacion = $(this).attr('clasificacion');
            var nombre__ = $(this).attr('nombre');
            var estado__ = $(this).attr('estadoTramite');
            //alert(tipoClasificacion+' '+nombre__ + ' '+ estado__);
            /*Toca invocar el Ajax que haga esto*/
            $("#ttitleH4").html("INCAPACIDADES REPORTADAS - "+ nombre__);
            $.ajax({
                url   : 'vistas/modulos/incapacidades_filtradas.php',
                type  : 'post',
                data  : {
                    filtro  : estado__,
                    clasificacion : tipoClasificacion,
                    inc_fecha_inicio : '<?php echo $year; ?>',
                    inc_fecha_final  : '<?php echo $otherYear; ?>'
                },
                dataType:'html',
                success : function(data){
                    $("#respuestaAjax").html(data);
                },
                beforeSend:function(){
                    $.blockUI({ 
                        message : '<h3>Un momento por favor....</h3>',
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        } 
                    }); 
                },
                complete:function(){
                    $.unblockUI();
                },
                error : function (){
                    console.log('ocurrio un error , en archivo, revisa que viene con error 500');
                }
            })
            $("#modalAgregarCliente").modal();
        });

        $("#btnExportarIncapacidades").click(function(){
            $("#formDescargaIncapacidades").submit();
        });
    })
</script>
