<?php
    //Pagadas
    $incPagadas = 0;
    $incPorRadicar = 0;
    $incRadicadas = 0;
    $incSinReconocimiento = 0;
    $incSolicitudPago = 0;
    $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

    $titulos = '';
    if(isset($_POST['selecTNominas']) && $_POST['selecTNominas'] != '0') {
        $titulos = mb_strtoupper($month[$_POST['selecTNominas'] -1])." ".$_POST['anho'];
    }else{
        if(isset($_POST['anho'])){
            $titulos = '- AÑO '.$_POST['anho'];
        }else{
            $titulos = '- AÑO '.date('Y');
        }
        
    }

    $campos = "sum(inc_valor) as total";
    $tabla = "gi_incapacidad";
    if($_SESSION['cliente_id'] != 0){
        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'PAGADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }else{
        $condiciones = "inc_estado_tramite = 'PAGADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }
    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $incPagadas = $respuesta2['total'];


    //Por Radicar
    $campos = "sum(inc_valor_pagado_eps) as total";
    $tabla = "gi_incapacidad";
    if($_SESSION['cliente_id'] != 0){
        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'POR RADICAR' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }else{
        $condiciones = "inc_estado_tramite = 'POR RADICAR' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }
    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $incPorRadicar = $respuesta2['total'];

    //Radicadas
    $campos = "sum(inc_valor_pagado_eps) as total";
    $tabla = "gi_incapacidad";
    if($_SESSION['cliente_id'] != 0){
        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'RADICADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }else{
        $condiciones = "inc_estado_tramite = 'RADICADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }
    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $incRadicadas = $respuesta2['total'];

    //Sin Reconocimiento
    $campos = "sum(inc_valor_pagado_eps) as total";
    $tabla = "gi_incapacidad";
    if($_SESSION['cliente_id'] != 0){
        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'SIN RECONOCIMIENTO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }else{
        $condiciones = "inc_estado_tramite = 'SIN RECONOCIMIENTO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }
    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $incSinReconocimiento = $respuesta2['total'];

    //Solicitud de Pago
    $campos = "sum(inc_valor_pagado_eps) as total";
    $tabla = "gi_incapacidad";
    if($_SESSION['cliente_id'] != 0){
        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'SOLICITUD DE PAGO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }else{
        $condiciones = "inc_estado_tramite = 'SOLICITUD DE PAGO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }
    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $incSolicitudPago = $respuesta2['total'];

    //TotalValores
    $incTotalAdministradora = $incPagadas + $incPorRadicar + $incRadicadas + $incSolicitudPago + $incSinReconocimiento;

?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">
                    
                    DATOS DE INCAPACIDADES - NOMINA <?php echo $titulos;?>
                </h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>DETALLE</th>
                            <th style="width: 30%;">CANTIDAD</th>
                            <th>VALOR</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                EMPRESA (1 Y 2 Días)
                            </td>
                            <td>
                                <?php
                                    
                                    $campos = "count(inc_estado_tramite) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_valor_pagado_empresa IS NOT NULL AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_valor_pagado_empresa != '' ";
                                    }else{
                                        $condiciones = "inc_valor_pagado_empresa IS NOT NULL AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_valor_pagado_empresa != ''";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $solicitudes = $respuesta['estado']; 
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    $campos = "sum(inc_valor_pagado_empresa) as total";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_valor_pagado_empresa IS NOT NULL AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = " inc_valor_pagado_empresa IS NOT NULL AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $totaValor1 = $respuesta2['total'];
                                    echo "$ ".number_format($totaValor1, 0, ',', '.');
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ADMINISTRADORAS (POR COBRAR)
                            </td>
                            <td>
                                <?php
                                    $campos = "count(*) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){ 
                                        $condiciones = "inc_estado_tramite != 'EMPRESA' AND inc_empresa = ".$_SESSION['cliente_id']." AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'   ";
                                    }else{
                                        $condiciones = "inc_estado_tramite != 'EMPRESA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' ";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                                    $totales = $respuesta['estado'];
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    $whereCliente = '';
                                    if($_SESSION['cliente_id'] != 0){ 
                                        $whereCliente = " inc_empresa = ".$_SESSION['cliente_id']." AND ";
                                    }
                                    $campos = 'SUM(inc_valor_pagado_eps) as total';
                                    $tabla  = 'gi_incapacidad';
                                    $condicion =  $whereCliente." inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != 'EMPRESA' ";
                                    $porpagar = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
                                    $incTotalAdministradora = $porpagar['total'];
                                    $intVariablePorcentage = $incTotalAdministradora;
                                    echo "$ ".number_format($incTotalAdministradora, 0, ',', '.');
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                PAGADAS
                            </td>
                            <td>
                                <?php

                                    $campos = "count(inc_estado_tramite) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'PAGADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'PAGADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
                                    $pagados = $respuesta['estado'];
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    echo "$".number_format($incPagadas, 0, ',', '.');
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                SIN RECONOCIMIENTO
                            </td>
                            <td>
                                <?php 

                                    $campos = "count(inc_estado_tramite) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'SIN RECONOCIMIENTO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'SIN RECONOCIMIENTO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);


                                    $sinReconocimiento = $respuesta['estado']; 
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    echo "$ ".number_format($incSinReconocimiento, 0, ',', '.');
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                SALDO A LA FECHA
                            </td>
                            <td>
                                <?php 
                                    $resta = $totales - $pagados-$sinReconocimiento;
                                    echo  $resta;
                                ?>
                            </td>
                            <td>
                                <?php 
                                    $resta = $incTotalAdministradora - $incPagadas - $incSinReconocimiento;
                                    echo  "$".number_format($resta, 0, ',', '.');
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 40%;">INCAPACIDADES POR ESTADO</th>
                            <th>TOTAL</th>
                            <th>VALOR</th>
                            <th>%</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                POR RADICAR
                            </td>
                            <td>
                                <?php
                                    
                                    $campos = "count(inc_estado_tramite) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'POR RADICAR' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'POR RADICAR' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $porRadicar = $respuesta['estado'];
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    echo "$ ".number_format($incPorRadicar, 0, ',', '.');
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($incTotalAdministradora > 0){
                                        echo number_format(($incPorRadicar * 100) / $incTotalAdministradora, 2)." %";
                                    }else{
                                        echo "0";
                                    }
                                    
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                RADICADAS
                            </td>
                            <td>
                                <?php
                                    
                                    $campos = "count(inc_estado_tramite) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'RADICADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'RADICADA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $radicadas = $respuesta['estado'];
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                
                                    echo "$ ".number_format($incRadicadas, 0, ',', '.');
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($incTotalAdministradora > 0){
                                        echo number_format(($incRadicadas * 100) / $incTotalAdministradora, 2)." %";
                                    }else{
                                        echo "0";
                                    }
                                    
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                SOLICITUDES DE PAGO
                            </td>
                            <td>
                                <?php
                                    
                                    $campos = "count(inc_estado_tramite) as estado";
                                    $tabla = "gi_incapacidad";
                                    if($_SESSION['cliente_id'] != 0){
                                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 'SOLICITUD DE PAGO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }else{
                                        $condiciones = "inc_estado_tramite = 'SOLICITUD DE PAGO' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                                    }
                                    $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                                    $solicitudes = $respuesta['estado']; 
                                    echo $respuesta['estado'];
                                ?>
                            </td>
                            <td>
                                <?php
                                    echo "$ ".number_format($incSolicitudPago, 0, ',', '.');
                                ?>
                            </td>
                            <td>
                                <?php
                                    
                                    if($incTotalAdministradora > 0){
                                        echo number_format(($incSolicitudPago * 100) / $incTotalAdministradora, 2)." %";
                                    }else{
                                        echo "0";
                                    }
                                    
                                    
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>  
    </div>

    <div class="col-md-6">
        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">
                    TOTAL PAGOS RECIBIDOS <?php echo $titulos;?>
                </h3>
            </div>
            <div class="box-body">
                <canvas id="pieChart_2" style="height:334px"></canvas>
            </div>
        </div>
    </div>
    
    
</div>