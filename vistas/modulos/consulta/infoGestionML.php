<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" /><!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Bienvenido/a <?php echo mb_strtoupper($_SESSION['nombres']); ?></h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active"><?php echo ModeloIncapacidades::getDatos('gi_empresa', 'emp_id', $_SESSION['cliente_id'])['emp_nombre']; ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php 
            if($_SESSION['cliente_id'] == '62'){
                echo '<iframe title="4. Informe de Casos Medicos - Comcel - Main" width="1024" height="612" src="https://app.powerbi.com/view?r=eyJrIjoiNzg3Mjk2ZWEtMjAzMi00NTBjLTg4M2MtYzVhZjE1OTA2NTg1IiwidCI6IjQwYjBjNGQ2LWM4MjEtNDM2Zi04MmE5LTIwYTIyNDQ0MjU4YyJ9" frameborder="0" allowFullScreen="true"></iframe>';
            }else if($_SESSION['cliente_id'] == '8'){
                echo '<iframe title="4. Informe de Casos Medicos - Cucinare - Main" width="1024" height="612" src="https://app.powerbi.com/view?r=eyJrIjoiYjRlYTk0MDgtZGRhMS00Mzg3LTg3YzYtMmE2MWFlZTQ4YzNhIiwidCI6IjQwYjBjNGQ2LWM4MjEtNDM2Zi04MmE5LTIwYTIyNDQ0MjU4YyJ9" frameborder="0" allowFullScreen="true"></iframe>';
            }else if($_SESSION['cliente_id'] == '49'){
                echo '<iframe title="4. Informe de Casos Medicos - Masser" width="1024" height="612" src="https://app.powerbi.com/view?r=eyJrIjoiZTkwZWExZWUtNjM2ZC00MDgzLTgyZDItMTRhN2M0MmM5NTBiIiwidCI6IjQwYjBjNGQ2LWM4MjEtNDM2Zi04MmE5LTIwYTIyNDQ0MjU4YyJ9&pageName=ReportSection" frameborder="0" allowFullScreen="true"></iframe>';
            }
        ?>
	</div>
</div>
<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>
<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script src="vistas/assets/assets/libs/chart.js/Chart.bundle.min.js"></script>