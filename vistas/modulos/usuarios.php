<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">USUARIOS</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Usuarios</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                   
                    <div class="row">
                        <div class="col-lg-11">
                        ADMINISTRAR USUARIOS
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">
                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <a class="dropdown-item" title="Agregar Usuario" data-bs-toggle="modal" data-bs-target="#modalAgregarUsuario" href="#">AGREGAR</a>
                                    <?php } ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
            <table style="width: 100%;" id="tablaUsuarios" class="table table-bordered table-striped dt-responsive tablas">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Correo</th>
                           <!-- <th>Foto</th>-->
                            <th>Perfil</th>
                            <th>Estado</th>
                            <th>Último login</th>
                            <th width="10%">Acciones</th>
                        </tr>
                    </thead>
                    
                </table>    
            </div>
        </div>
    </div>
</div>
<div id="modalAgregarUsuario" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form id="frmGuardarNuevoUsuario" role="form" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                <h5 class="modal-title"id="staticBackdropLabel">Agregar Usuario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Nombre</label>
                                <input class="form-control input-lg" type="text" name="NuevoNombre" placeholder="Ingresar nombre" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Correo</label>
                                <input class="form-control input-lg" type="text" name="NuevoUsuario" placeholder="Ingresar Correo" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Contraseña</label>
                                <input class="form-control input-lg" type="password" name="NuevoPassword" placeholder="Ingresar contraseña" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Seleccione Perfil de Usuario</label>
                                <select class="form-control input-lg" name="NuevoPerfil">
                                    <?php
                                       $Perfils = ControladorPerfiles::ctrMostrarPerfiles(NULL, NULL);
                                        foreach ($Perfils as $key => $value) {
                                            echo "<option value='".$value['perfiles_id_i']."'>".$value['perfiles_descripcion_v']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <?php if($_SESSION['cliente_id'] != 0) { ?>
                            <input type="hidden" name="NuevoclienteHiden" value="<?php echo $_SESSION['cliente_id']; ?>">
                        <?php }else{ ?>
                            <div class="form-group mb-3 ">
                                <div class="input-group">
                                    <label class="form-label">Seleccionar Empresas</label>
                                    <select style="width: 100%;" multiple="multiple" class="form-control input-lg" id="Nuevocliente" name="Nuevocliente[]">
                                        <?php
                                           $Perfils = ControladorClientes::ctrMostrarClientes(NULL, NULL);
                                            foreach ($Perfils as $key => $value) {
                                                echo "<option value='".$value['emp_id']."'>".$value['emp_nombre']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                        <label class="form-label">Seleccionar Imagen de Usuario</label>
                            <div class="panel">
                                <input type="file" class="NuevaFotos" name="NuevaFoto">
                                <p class="help-block">
                                    Peso maximo de la foto 200 MB
                                </p>
                                <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                    <button type="button" id="btnGuardarUsuario" class="btn btn-danger">Guardar Usuario</button>
                </div>
              
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->
<!-- Modal editar usuario -->
<div id="modalEditarUsuarios" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form id="frmEditarNuevoUsuario" role="form" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                <h5 class="modal-title"id="staticBackdropLabel">Editar Usuario</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Nombre</label>
                                <input class="form-control input-lg" type="text" name="EditarNombre"  id="EditarNombre" placeholder="Ingresar nombre" required="true"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Correo</label>
                                <input class="form-control input-lg" type="text" name="EditarUsuario" id="EditarUsuario" placeholder="Ingresar Correo" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Contraseña</label>
                            <input class="form-control input-lg" type="password" name="EditarPassword"  id="EditarPassword" placeholder="Ingresar contraseña" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Seleccione Perfil de Usuario</label>
                            <select class="form-control input-lg" name="EditarPerfil" id="EditarPerfil">
                                    <?php
                                       $Perfils = ControladorPerfiles::ctrMostrarPerfiles(NULL, NULL);
                                        foreach ($Perfils as $key => $value) {
                                            echo "<option value='".$value['perfiles_id_i']."'>".$value['perfiles_descripcion_v']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <?php if($_SESSION['cliente_id'] != 0) { ?>
                            <input type="hidden" name="EditarclienteHiden" value="<?php echo $_SESSION['cliente_id']; ?>">
                        <?php }else{ ?>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-users"></i>
                                    </span>
                                    <select style="width: 100%;" multiple="multiple" class="form-control input-lg" name="Editarcliente[]" id="Editarcliente">
                                        <?php
                                           $Perfils = ControladorClientes::ctrMostrarClientes(NULL, NULL);
                                            foreach ($Perfils as $key => $value) {
                                                echo "<option value='".$value['emp_id']."'>".$value['emp_nombre']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="panel">
                            <input type="file"  name="EditarFoto" class="NuevaFotos">
                                <p class="help-block">
                                    Peso maximo de la foto 200 MB
                                </p>
                                <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                    <button type="button" id="btnEditarUsuario" class="btn btn-danger">Editar Usuario</button>
                    <input type="hidden" name="passwordActual" id="passwordActual">
                    <input type="hidden" name="RecordatorioActual" id="RecordatorioActual">
                    <input type="hidden" name="FotoActual" id="FotoActual">
                    <input type="hidden" name="EditarUserID" id="EditarUserID">
                </div>
              
            </form>
        </div>
    </div>
</div>

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/usuarios.js?v=<?php echo rand();?>"></script>
