<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">MEDICOS</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Medicos</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->

<?php 
    $noValidados = ControladorMedicos::ctrCountMedicos(date("Y"), "0");
    $registraRethus = ControladorMedicos::ctrCountMedicos(date("Y"), "1");
    $noRegistraRethus = ControladorMedicos::ctrCountMedicos(date("Y"), "2");
    $totalMedicos = ControladorMedicos::ctrCountMedicos(date("Y"), null);

    $porcentajeNoValidados = 0;
    $porcentajeRegRethus = 0;
    $porcentajeNoRegRethus = 0;

    if ($noValidados > 0) {
        $porcentajeNoValidados = ($noValidados * 100) / $totalMedicos; 
    }

    if ($registraRethus > 0) {
        $porcentajeRegRethus = ($registraRethus * 100) / $totalMedicos; 
    }

    if ($noRegistraRethus > 0) {
        $porcentajeNoRegRethus = ($noRegistraRethus * 100) / $totalMedicos; 
    }
?>

<div class="row" id="FieldsFilters">
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">NO VALIDADO</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo $noValidados; ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger"><?php echo number_format($porcentajeNoValidados, 2). " %"; ?></span>
                    <span class="ms-1 text-muted font-size-13">PORCENTAJE ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div>

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-8">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">REGISTRA EN RETHUS</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo ControladorMedicos::ctrCountMedicos(date("Y"), "1"); ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger"><?php echo number_format($porcentajeRegRethus, 2). " %"; ?></span>
                    <span class="ms-1 text-muted font-size-13">PORCENTAJE ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div>

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-9">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">NO REGISTRA EN RETHUS</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo $noRegistraRethus; ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger"><?php echo number_format($porcentajeNoRegRethus, 2). " %"; ?></span>
                    <span class="ms-1 text-muted font-size-13">PORCENTAJE ACTUAL</span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-12">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">FILTRAR POR AÑO</span>
                        <h4 class="mb-3">
                           <select id="filtrosMedicosByYear" class="form-control">
                               <?php 
                                    $years = ControladorMedicos::ctrFiltroByYear();
                                    foreach ($years as $value) {
                                        echo "<option value='".$value['anho']."'>".$value['anho']."</option>";
                                    }
                               ?>
                               <option value="0">TODOS</option>
                           </select>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-primary text-primary"><?php echo date('Y');?></span>
                    <span class="ms-1 text-muted font-size-13">ACTUAL</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                   
                    <div class="row">
                        <div class="col-lg-11">
                        ADMINISTRAR MEDICOS
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">
                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <a class="dropdown-item" title="Agregar Médico" data-bs-toggle="modal" data-bs-target="#modalAgregarMedico" href="#">AGREGAR</a>
                                    <a class="dropdown-item" title="Cargar Médicos" data-bs-toggle="modal" data-bs-target="#modalCargarMedicos" href="#">CARGAR MÉDICOS</a>
                                    <?php } ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="https://incapacidades.app/services/exports/medicos/exportar" title="Exportar datos a Excel" target="_blank">EXPORTAR</a></li>
                                </div>
                            </div> 
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
            <table style="width: 100%;" id="tablaMedicos" class="table table-bordered table-striped dt-responsive tablas">
                    <thead>
                        <tr>
                            <th>Nombre y Apellidos</th>
                            <th>Identificación</th>
                            <th>N° Registro</th>
                            <!--<th>Fecha de Creación</th>-->
                            <th>Estado Rethus</th>
                            <th width="10%">Acciones</th>
                        </tr>
                    </thead>
                    
                </table>    
            </div>
        </div>
    </div>
</div>
<?php 
    $moduloInc = false;
    include 'modalAddMedico.php'; 
?>

<div id="modalEditarMedico" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form id="frmEditarMedico" role="form" autocomplete="off" method="post">
            <div class="modal-header">
                <h5 class="modal-title"id="staticBackdropLabel">Editar Médico</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Nombre</label>
                                <input class="form-control input-lg" type="text" name="EditarNombreMedico" id="EditarNombreMedico" placeholder="Ingresar nombre" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar Identificación</label>
                                <input class="form-control input-lg" type="text" name="EditarIdentificacion" id="EditarIdentificacion" placeholder="Ingresar Indentificación">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Ingresar N° de Registro</label>
                                <input class="form-control input-lg" type="text" name="EditarNumRegistro" id="EditarNumRegistro" placeholder="Ingresar N° de Registro" required="true">
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Seleccione Estado Rethus</label>
                                <select class="form-control input-lg" name="EditarEstadoRethus" id="EditarEstadoRethus">
                                    <option value="0" selected>NO VALIDADO</option>
                                    <option value="1">REGISTRA</option>
                                    <option value="2">NO REGISTRA</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="mb-3">
                            <label class="form-label">Seleccione Estado Médico</label>
                                <select class="form-control input-lg" name="EditarEstadoMedico" id="EditarEstadoMedico">
                                    <option value="0" selected>NO ACTIVO</option>
                                    <option value="1">ACTIVO</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group mb-3 ">
                            <div class="input-group">
                                <label class="form-label">Seleccionar Entidades IPS</label>
                                <select style="width: 100%;" multiple="multiple" class="form-control input-lg" id="EditarEntidadIps" name="EditarEntidadIps[]">
                                    <?php
                                        $entidadesIps = ControladorEntidadAtiende::ctrMostrarEntidades(NULL, NULL);
                                        foreach ($entidadesIps as $key => $value) {
                                            echo "<option value='".$value['enat_id_i']."'>".$value['enat_nombre_v']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                    <button type="button" id="btnEditarMedico" class="btn btn-danger">Editar Médico</button>
                    <input type="hidden" name="EditarMedicoID" id="EditarMedicoID">
                   
                </div>
              
            </form>
        </div>
    </div>
</div>

<div id="modalCargarMedicos" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" id="frmCargueMasivoMedicos" enctype="multipart/form-data">
                <div class="modal-header">

                    <h5 class="modal-title" id="staticBackdropLabel">Cargar Médicos Masivamente</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                   <div class="row panel">
                       <div class="col-md-12 col-xs-12">
                            <div class="mb-3">
                                <label class="form-label">Formato de carga</label>
                                <input type="file" class="NuevoExell" required valor='0' name="fileMedicos"  id="fileMedicos">
                                <p class="help-block">
                                    Peso maximo del archivo 3 MB
                                </p>
                                <p>
                                    Descarga el Formato de carga para médicos <a href="vistas/formatos/FC_MEDICOS.xlsx" download="FC_MEDICOS.xlsx">aqui</a>
                                </p>
                                <input type="hidden" name="userId" value="<?php echo $_SESSION['id_Usuario'];?>">
                            </div>
                        </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Cerrar</button>
                    <button type="button" id="cargarDatos" class="btn btn-primary">Cargar Médicos</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/medicos.js?v=<?php echo rand();?>"></script>
