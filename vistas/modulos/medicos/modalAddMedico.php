<div id="modalAgregarMedico" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content border border-danger">
            <form id="frmGuardarNuevoMedico" role="form" autocomplete="off" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Agregar Médico</h5>
                    
                    <?php if($moduloInc == false){ ?>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <?php } ?>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="mb-3">
                                <label class="form-label">Ingresar Nombre</label>
                                <input class="form-control input-lg" type="text" name="NuevoNombreMedico" placeholder="Ingresar nombre" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                                <label class="form-label">Ingresar Identificación</label>
                                <input class="form-control input-lg" type="text" name="NuevaIdentificacion" id="NuevaIdentificacion" placeholder="Ingresar Indentificación" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="mb-3">
                                <label class="form-label">Ingresar N° de Registro</label>
                                <input class="form-control input-lg" type="text" name="NuevoNumRegistro" placeholder="Ingresar N° de Registro" required="true">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="mb-3">
                                <label class="form-label">Seleccione Estado Rethus</label>
                                <select class="form-control input-lg" name="NuevoEstadoRethus">
                                    <option value="0" selected>NO VALIDADO</option>
                                    <option value="1">REGISTRA</option>
                                    <option value="2">NO REGISTRA</option>

                                </select>
                            </div>
                        </div>

                        <div class="form-group mb-3 ">
                            <div class="input-group">
                                <label class="form-label">Seleccionar Entidades IPS</label>
                                <select style="width: 100%;" multiple="multiple" class="form-control input-lg" id="NuevaEntidadIps" name="NuevaEntidadIps[]">
                                    <?php
                                        $entidadesIps = ControladorEntidadAtiende::ctrMostrarEntidades(NULL, NULL);
                                        foreach ($entidadesIps as $key => $value) {
                                            echo "<option value='".$value['enat_id_i']."'>".$value['enat_nombre_v']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <?php if($moduloInc == true){ ?>
                        <button type="button" id="btnGuardarMedico" class="btn btn-danger">Guardar Médico</button>
                    <?php }else{ ?>
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                        <button type="button" id="btnGuardarMedico" class="btn btn-danger">Guardar Médico</button>
                    <?php } ?>
                </div>

            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#btnGuardarMedico").click(function() {
        var formData = new FormData($("#frmGuardarNuevoMedico")[0]);
        console.log("flag" + $("#NuevaEntidadIps").val())
        
        
        $.ajax({
            url: 'ajax/medicos.ajax.php',
            method: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            beforeSend: function() {
                $.blockUI({
                    baseZ: 2000,
                    css: {
                        border: 'none',
                        padding: '1px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            },
            complete: function() {
                <?php if($moduloInc != true){ ?>
                    tablaMedicos.ajax.reload();                        
                <?php }?>
                $("#frmGuardarNuevoMedico")[0].reset();
                $("#modalAgregarMedico").modal('hide');               
                $.unblockUI();
            },
            //una vez finalizado correctamente
            success: function(data) {
                if (data.code == '1') {
                    alertify.success(data.desc);
                } else {
                    alertify.error(data.desc);
                }
            },
            //si ha ocurrido un error
            error: function() {
                //after_save_error();
                alertify.error('Error al realizar el proceso');
            }
        });
    });
</script>
<!-- /.Modal -->