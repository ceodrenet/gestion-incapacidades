<?php
    session_start();
    require_once '../../../controladores/mail.controlador.php';
    require_once '../../../controladores/plantilla.controlador.php';
    require_once '../../../controladores/medicos.controlador.php.';
    require_once '../../../controladores/tesoreria.controlador.php';
    require_once '../../../modelos/dao.modelo.php';
    require_once '../../../modelos/medicos.modelos.php';
    require_once '../../../modelos/tesoreria.modelo.php';

    if(isset($_POST['yearFilter'])){
		$year = null;
		if($_POST['yearFilter'] != 0){
			$year = $_POST['yearFilter'];
		}
        
        $noValidados = ControladorMedicos::ctrCountMedicos($year, "0");
        $registraRethus = ControladorMedicos::ctrCountMedicos($year, "1");
        $noRegistraRethus = ControladorMedicos::ctrCountMedicos($year, "2");
        $totalMedicos = ControladorMedicos::ctrCountMedicos($year, null);

        $porcentajeNoValidados = 0;
        $porcentajeRegRethus = 0;
        $porcentajeNoRegRethus = 0;

        if ($noValidados > 0) {
            $porcentajeNoValidados = ($noValidados * 100) / $totalMedicos; 
        }

        if ($registraRethus > 0) {
            $porcentajeRegRethus = ($registraRethus * 100) / $totalMedicos; 
        }

        if ($noRegistraRethus > 0) {
            $porcentajeNoRegRethus = ($noRegistraRethus * 100) / $totalMedicos; 
        }
?>
<div class="row" id="FieldsFilters">
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-6">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">NO VALIDADO</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo $noValidados; ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger"><?php echo number_format($porcentajeNoValidados, 2). " %"; ?></span>
                    <span class="ms-1 text-muted font-size-13">PORCENTAJE ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div>

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-8">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">REGISTRA EN RETHUS</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo $registraRethus; ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger"><?php echo number_format($porcentajeRegRethus, 2). " %"; ?></span>
                    <span class="ms-1 text-muted font-size-13">PORCENTAJE ACTUAL</span>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div>

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-9">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">NO REGISTRA EN RETHUS</span>
                        <h4 class="mb-3">
                            <span class="counter-value" data-target="<?php echo $noRegistraRethus; ?>">0</span>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-danger text-danger"><?php echo number_format($porcentajeNoRegRethus, 2). " %"; ?></span>
                    <span class="ms-1 text-muted font-size-13">PORCENTAJE ACTUAL</span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-h-100">
            <!-- card body -->
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-12">
                        <span class="text-muted mb-3 lh-1 d-block text-truncate">FILTRAR POR AÑO</span>
                        <h4 class="mb-3">
                           <select id="filtrosMedicosByYear" class="form-control">
                           <option value="0">TODOS</option>
                               <?php 
                                    $years = ControladorMedicos::ctrFiltroByYear();
                                    foreach ($years as $value) {
                                        if ($year == $value['anho']) {
                                            echo "<option selected value='".$value['anho']."'>".$value['anho']."</option>";
                                        }else {
                                            echo "<option value='".$value['anho']."'>".$value['anho']."</option>";
                                        }
                                    }
                               ?>
                           </select>
                        </h4>
                    </div>
                </div>
                <div class="text-nowrap">
                    <span class="badge bg-soft-primary text-primary"><?php echo date('Y');?></span>
                    <span class="ms-1 text-muted font-size-13">ACTUAL</span>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(function(){
		$("#filtrosMedicosByYear").on('change', function(){
	        var _miValor = $(this).val();
	        $.ajax({
	            url : 'vistas/modulos/medicos/medicosFiltroAnual.php',
	            type: 'post',
	            data: {
	                yearFilter : _miValor
	            },
	            success : function(data){
	                $("#FieldsFilters").html(data);
	            }
	        });
	    });

		initCounterNumber();

	    function initCounterNumber() {
	        var counter = document.querySelectorAll('.counter-value');
	        var speed = 250; // The lower the slower
	        counter.forEach(function (counter_value) {
	            function updateCount() {
	                var target = +counter_value.getAttribute('data-target');
	                var count = +counter_value.innerText;
	                var med = target / speed;
	                if (med < 1) {
	                    med = 1;
	                }
	                // Check if target is reached
	                if (count < target) {
	                    // Add med to count and output in counter_value
	                    counter_value.innerText = (count + med).toFixed(0);
	                    // Call function every ms
	                    setTimeout(updateCount, 1);
	                } else {
	                    counter_value.innerText = target;
	                }
	            };
	            updateCount();
	        });
	    }
	});
</script>

<?php		
	}
?>