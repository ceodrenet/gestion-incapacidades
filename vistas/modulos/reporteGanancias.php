<?php 
    $fechaFinal = $_POST['fechaFinal'];
    $fechaInicial = $_POST['fechaInicial'];
    $cliente_id = $_POST['cliente_id'];
    require_once '../../modelos/tesoreria.modelo.php';
?>



<table style="width:100%;" id="tablaClientes" class="table table-bordered table-striped dt-responsive tablas">
    <thead>
        <tr>
            <th>#</th>
            <th style="width: 10%;">NIT</th>
            <th>Empresa</th>
            <th>Ingreso Mes</th>
            <th>Porcentaje</th>
            <th style="width: 20%;">Comisión</th>
            <th style="width: 20%;">Facturado</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
            $valorTotal = 0;
            $strCampos = "emp_nombre , emp_id, emp_nit, emp_porcentaje_i ";
            $strTablas = "gi_empresa";
            $condicion = 'emp_estado  = 1';
            $resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos, $strTablas, $condicion, null, " ORDER BY emp_nombre ASC"); 

            foreach ($resultado as $key => $value) {

                /*Vamos a obtener las ganancias de esa empresa en ese intervalo de tiempo */
                $strNewCampos = "SUM(inc_valor) as total";
                $strNewTablas = "gi_incapacidad";
                $strCondicion = "inc_fecha_pago BETWEEN '".$fechaInicial."' AND '".$fechaFinal."' AND   inc_empresa = ".$value['emp_id'];
                $respuestaEmp = ModeloTesoreria::mdlMostrarUnitario($strNewCampos, $strNewTablas, $strCondicion);

                $valorInc = $respuestaEmp['total'] * ($value['emp_porcentaje_i']/100);
                $valorTotal += $valorInc;
                echo '<tr>';
                    echo '<td>'.($key+1).'</td>';
                    echo '<td>'.$value['emp_nit'].'</td>';
                    echo '<td>'.mb_strtoupper($value['emp_nombre']).'</td>';
                    echo '<td>'.number_format($respuestaEmp['total'], 2, ',', '.').'</td>';
                    echo '<td>'.$value['emp_porcentaje_i'].' %</td>';
                    echo '<td>'.number_format($valorInc, 2, ',', '.').'</td>';
                    echo '<td></td>';
                    echo '<td></td>';
                echo '</tr>';
            }
        ?>
    </tbody>

    <tfoot>
        <tr>
            <th></th>
            <th style="width: 10%;"></th>
            <th></th>
            <th></th>
            <th>Total Incapacidades.co</th>
            <th style="width: 20%;"><?php echo number_format($valorTotal, 2, ',','.')?></th>
            <th style="width: 20%;"></th>
            <th></th>
        </tr>
    </tfoot>
</table>

<script type="text/javascript">
    $(function(){
        /* para geenral de pagos */
        $("#tablaClientes").DataTable({
            "language" : {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "<<",
                    "sLast":     ">>",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });

    })
</script>