<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<style>
    table.dataTable.dataTable_width_auto {
  width: auto;
}
</style>
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">USUARIOS - REGISTRADOS </h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Administradores Registrados</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                   
                    <div class="row">
                        <div class="col-lg-11">
                        USUARIOS POR EPS
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">
                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <a class="dropdown-item" title="Agregar Usuario EPS" data-bs-toggle="modal" data-bs-target="#modalAdicionarUsuarioEps" href="#">AGREGAR</a>
                                   
                                    <?php } ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
                <table class="table-hover table table-bordered tblGeneralPagos" id="tablaUsuariosEps">
                    <thead>
                        <tr>
                            <th style='width: 20%;'>Entidad</th>
                            <th style='width: 10%;'>Usuario</th>
                            <th style='width: 10%;'>Password</th>
                            <th style='width: 20% !important;'>Url</th>
                            <th style='width: 10%;'></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style='width: 20%;'>Entidad</th>
                            <th style='width: 10%;'>Usuario</th>
                            <th style='width: 10%;'>Password</th>
                            <th style='width: 20% !important;'>Url</th>
                            <th style='width: 10%;'></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modal agregar usuario -->
<div id="modalAdicionarUsuarioEps" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg"  role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" id="AgragrNuevoUsuarioEps" autocomplete="off" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Registrar Usuario De Administradora</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>  
                </div>
               <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                    <div class="input-group">
                                    <label class="form-label">Seleccione Administradora</label>
                                        <select class="form-control" style="width: 100%;" required="true" id="NuevoIpsId" name="NuevoIpsId">
                                            <option value="">Selecionar Administradora</option>
                                            <?php
                                                $item = NULL;
                                                $valor = NULL;
                                                $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                                foreach ($IpsAfiliada as $key => $value) {
                                                    echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                   
                                          <label class="form-label">Ingresar Usuario</label>
                                         <input class="form-control" type="text" name="NuevoNombreUsuario" id="NuevoNombreUsuario" placeholder="Ingresar Usuario de Administradora" >
                                    
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                 <label class="form-label">Ingresar Contraseña</label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" name="NuevoPasswoUsuario" id="NuevoPasswoUsuario" placeholder="Ingresar Password de Administradora" >
                                    </div>
                                            </div>   
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <label class="form-label">Ingresar Url Administradora</label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" name="NuevoUrl___Usuario" id="NuevoUrl___Usuario" placeholder="Ingresar Url de Administradora" >
                                    </div>
                                     
                            </div>
                        </div>
                    <?php if($_SESSION['cliente_id'] == 0){ ?>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <label class="form-label">Seleccionar Empresa</label>
                                    <select class="form-control" id="NuevoIdEmpresa" placeholder="Observaciones" name="NuevoIdEmpresa" style="width: 100%;">
                                        <option value="0">Seleccione la empresa</option>
                                        <?php 
                                            $clientes = ControladorClientes::ctrMostrarClientes(NULL, NULL);
                                            foreach ($clientes as $key => $value) {
                                                echo "<option value='".$value['emp_id']."'>".$value['emp_nombre']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <input type="hidden" name="NuevoIdEmpresa" id="NuevoIdEmpresa" value="<?php if($_SESSION['cliente_id'] != 0) { echo $_SESSION['cliente_id']; }?>">
                    <?php } ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="nuevoObservacion" id="nuevoObservacion"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                 <?php 
                  //  $crearPaciente = new ControladorUsuariosEps();
                   // $crearPaciente->ctrCrearUsuariosEps();
                 ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                    <button type="button" id="btnNuevoUsuarioEps" class="btn btn-danger">Agregar Usuario EPS</button>
                </div>
            </form>
        </div>
    </div>
</div>
 
<div id="modalEditarUsuarioEps" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg"  role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" id="EditarUsuarioEps" autocomplete="off" method="post">
                <div class="modal-header">
                <h4 class="modal-title">Editar Usuario De Administradora</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>  
                </div>
               <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                <label class="form-label">Seleccione Administradora</label>
                                    <div class="input-group">
                                    <select class="form-control" style="width: 100%;" required="true" id="EditarIpsId" name="EditarIpsId">
                                            <option value="">Selecionar Administradora</option>
                                            <?php
                                                $item = NULL;
                                                $valor = NULL;
                                                $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                                foreach ($IpsAfiliada as $key => $value) {
                                                    echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                            </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <label class="form-label">Ingresar Usuario</label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" name="EditarNombreUsuario" id="EditarNombreUsuario" placeholder="Ingresar Usuario de Administradora" >
                                    </div>
                                      
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                 <label class="form-label">Ingresar Contraseña</label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" name="EditarPasswoUsuario" id="EditarPasswoUsuario" placeholder="Ingresar Password de Administradora" >
                                    </div>
                                </div>   
                            </div>
                            <div class="col-md-6 col-xs-12">
                            <label class="form-label">Ingresar Url Administradora</label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" name="EditarUrl___Usuario" id="EditarUrl___Usuario" placeholder="Ingresar Url de Administradora" >
                                    </div>
                                      
                            </div>
                        </div>
                    <?php if($_SESSION['cliente_id'] == 0){ ?>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <label class="form-label">Seleccionar Empresa</label>
                                    <select class="form-control" id="editarEmpresa" name="editarEmpresa" style="width: 100%;">
                                        <option value="0">Seleccione la empresa</option>
                                        <?php 
                                            $clientes = ControladorClientes::ctrMostrarClientes(NULL, NULL);
                                            foreach ($clientes as $key => $value) {
                                                echo "<option value='".$value['emp_id']."'>".$value['emp_nombre']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>                
                        </div>
                    <?php } else { ?>
                    <?php } ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="editarObservacion" id="editarObservacion"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                 <?php 
                  //  $crearPaciente = new ControladorUsuariosEps();
                   // $crearPaciente->ctrCrearUsuariosEps();
                 ?>
                </div>
                <div class="modal-footer">
                        <input type="hidden" name="EditarIdUsuario" id="EditarIdUsuario" value="0">
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>

                        <button type="button" id="btnEditarUsuarioDeEps" class="btn btn-danger">Editar Usuario EPS</button>
                    </div>
            </form>
        </div>
    </div>
</div>


<?php 
  //  $crearPaciente = new ControladorUsuariosEps();
    //$crearPaciente->ctrBorrarUsuariosEps();
?>



<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/usuariosEps.js?v=<?php echo rand();?>"></script>