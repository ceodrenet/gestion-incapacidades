<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- start page title -->

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">CONFIGURAR PERFILES DEL SISTEMA</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Perfiles </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    <div class="row">
                        <div class="col-lg-10">
                            
                        </div>
                        <div class="col-lg-2">
                            <div class="box-header with-border">
                                <?php if($_SESSION['adiciona'] == 1){ ?>
                                <button class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalAgregarPerfil">
                                    Agregar Perfil
                                </button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
                <table style="width: 100%;" id="tbl_Perfils" class="table table-bordered table-striped dt-responsive tablas">  
                    <thead>
                        <tr>
                            <th style="width: 10px;">#</th>
                            <th style="width: 80%;">Nombre</th>
                            <th style="text-align: center;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $Perfils = ControladorPerfiles::ctrMostrarPerfiles(NULL, NULL);
                            foreach ($Perfils as $key => $value) {
                                echo '  <tr>
                                            <td>
                                                '.($key+1).'
                                            </td>
                                            <td>
                                                '.$value['perfiles_descripcion_v'].'
                                            </td>';
                                echo '
                                    <td style="text-align:center;">';
                                    if($_SESSION['edita'] == 1){
                                        echo '  <button data-bs-toggle="modal" data-bs-target="#modalEditarPerfil" class="btn btn-warning btn-sm btnEditarPerfiles" title ="Editar Perfiles" idPerfiles ="'.$value['perfiles_id_i'].'">
                                                    <i class="fa fa-edit"></i>
                                                </button>';
                                    }

                                    if($_SESSION['elimina'] == 1){
                                        echo '  <button title="Eliminar Perfiles" class="btn btn-danger btn-sm btnEiminarPerfiles" idPerfiles ="'.$value['perfiles_id_i'].'">
                                                    <i class="fa fa-trash"></i>
                                                </button>';

                                    }

                        
                                echo '  
                                    </td>
                                </tr>'; 
                            }
                        ?>
                    </tbody>
                </table>      
            </div>
        </div>
    </div>
</div>


<!-- Modal agregar Perfil -->
<div id="modalAgregarPerfil" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
     <div class="modal-dialog modal-dialog-centered" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" id="formCrearPerfiles" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">AGREGAR PERFIL</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="mb-3">
                                <label>Nombre Del Perfil</label>
                                <input class="form-control input-lg" type="text" name="NuevoNombre" placeholder="Ingresar nombre" required="true">
                            </div>
                        </div> 
                        <div class="col-md-6 col-xs-12">
                            <div class="mb-3">
                                <label>Cliente</label>
                                <select class="form-control input-lg" name="NuevoCliente">
                                    <option value="0">Seleccione</option>
                                    <?php 
                                        
                                        $item = null;
                                        $valor = null;
                                        $clientes = ControladorClientes::ctrMostrarClientes($item, $valor);
                                        
                                        foreach ($clientes as $key => $value) {
                                            echo '<option value="'.$value["emp_id"].'">'.$value["emp_nombre"].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card border border-danger">
                        <div class="card-header bg-transparent border-danger">
                            <h5 class="my-0 text-danger">
                                PERMISOS DE ESTE PERFIL
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <?php
                                    $menus = ControladorPerfiles::ctrMostrarMenus(null, null);
                                    foreach ($menus as $key => $value) {
                                       echo '<div class="col-md-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="NuevoMenus[]" value="'.$value['menus_id_i'].'"> '.$value['menus_nombre_v'].'
                                                </label>
                                            </div>
                                        </div>';
                                    }
                                ?>    
                            </div>
                        </div>
                    </div>
                    <div class="card border border-danger">
                        <div class="card-header bg-transparent border-danger">
                            <h5 class="my-0 text-danger">
                                REPORTES QUE PUEDE VER ESTE PERFIL
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <?php
                                    $campos = "*";
                                    $tabla_ = "sys_submenu JOIN sys_menus ON menus_id_i = submenu_menu_id";
                                    $condi_ = "menus_reporte_i = 1";
                                    $reportes = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla_,$condi_, null, "ORDER BY menus_orden_i ASC");
                                    foreach ($reportes as $key => $value) {
                                       echo '<div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="NuevoMenusReportes[]" value="'.$value['submenu_id'].'"> '.$value['submenu_nombre'].'
                                                </label>
                                            </div>
                                        </div>';
                                    }
                                ?>     
                            </div>
                        </div>
                    </div>
                    <div class="card border border-danger">
                        <div class="card-header bg-transparent border-danger">
                            <h5 class="my-0 text-danger">
                                ESTE PERFIL PUEDE
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="NuevoAdicionar" value="1"> Adicionar
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="NuevoEditar" value="1"> Editar
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="NuevoEliminar" value="1"> Eliminar
                                        </label>
                                    </div>
                                </div>                                    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                    <button type="button" id="guardarPerfil" class="btn btn-danger">Guardar Perfil</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Modal editar Perfil -->
<div id="modalEditarPerfil"  data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog">
     <div class="modal-dialog modal-dialog-centered" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" id="formEditarPerfil" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">EDITAR PERFIL</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="mb-3">
                                <label>Nombre Del Perfil</label>
                                <input class="form-control input-lg" type="text" id="EditarNombre" name="EditarNombre" placeholder="Ingresar nombre" required="true">
                            </div>
                        </div> 
                        <div class="col-md-6 col-xs-12">
                            <div class="mb-3">
                                <label>Cliente</label>
                                <select class="form-control input-lg" name="EditarCliente" id="EditarCliente">
                                    <option value="0">Seleccione</option>
                                    <?php 
                                        if($_SESSION['cliente_id'] != 0){
                                            $item = 'emp_id';
                                            $valor = $_SESSION['cliente_id'];
                                            $clientes = ControladorClientes::ctrMostrarClientes_Sessions($item, $valor);
                                        }else{
                                            $item = null;
                                            $valor = null;
                                            $clientes = ControladorClientes::ctrMostrarClientes($item, $valor);
                                        }
                                        foreach ($clientes as $key => $value) {
                                            echo '<option value="'.$value["emp_id"].'">'.$value["emp_nombre"].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card border border-danger">
                        <div class="card-header bg-transparent border-danger">
                            <h5 class="my-0 text-danger">
                                PERMISOS DE ESTE PERFIL
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <?php
                                    $menus = ControladorPerfiles::ctrMostrarMenus(null, null);
                                    foreach ($menus as $key => $value) {
                                       echo '<div class="col-md-4">
                                            <div class="checkbox">
                                                <label>
                                                   <input type="checkbox" name="EditarMenus[]" id="EditarMenus_'.$value['menus_id_i'].'" value="'.$value['menus_id_i'].'"> '.$value['menus_nombre_v'].'
                                                </label>
                                            </div>
                                        </div>';
                                    }
                                ?>    
                            </div>
                        </div>
                    </div>
                    <div class="card border border-danger">
                        <div class="card-header bg-transparent border-danger">
                            <h5 class="my-0 text-danger">
                                REPORTES QUE PUEDE VER ESTE PERFIL
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <?php
                                    $campos = "*";
                                    $tabla_ = "sys_submenu JOIN sys_menus ON menus_id_i = submenu_menu_id";
                                    $condi_ = "menus_reporte_i = 1";
                                    $reportes = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla_,$condi_, null, "ORDER BY menus_orden_i ASC");
                                    foreach ($reportes as $key => $value) {
                                       echo '<div class="col-md-6">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="EditarMenusReportes[]" id="EditarMenusReportes_'.$value['submenu_id'].'"value="'.$value['submenu_id'].'"> '.$value['submenu_nombre'].'
                                                </label>
                                            </div>
                                        </div>';
                                    }
                                ?>     
                            </div>
                        </div>
                    </div>
                    <div class="card border border-danger">
                        <div class="card-header bg-transparent border-danger">
                            <h5 class="my-0 text-danger">
                                ESTE PERFIL PUEDE
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="EditarAdicionar" id="EditarAdicionar" value="1"> Adicionar
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="EditarEditar" id="EditarEditar" value="1"> Editar
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="EditarEliminar" name="EditarEliminar" value="1"> Eliminar
                                        </label>
                                    </div>
                                </div>                                    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                    <input type="hidden" name="EditarPerfil" id="EditarPerfilId">
                    <button type="button" id="editaperfilButton" class="btn btn-danger">Guardar Perfil</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/perfiles.js?v=<?php echo rand();?>"></script>