<?php 

    require_once __DIR__.'/../../../vendor/autoload.php';

	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
	use PhpOffice\PhpSpreadsheet\Style\Alignment;
 	$objPHPExcel = new Spreadsheet();
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getProperties()->setCreator("GEIN")
	                     ->setLastModifiedBy("".$_SESSION['nombres'])
	                     ->setTitle("GEIN - Casos especiales")
	                     ->setSubject("Casos especiales")
	                     ->setDescription("Descarga el reporte de Casos especiales, registrado en el sistema")
	                     ->setKeywords("office 2007 openxml php")
	                     ->setCategory("Casos especiales");

	$objPHPExcel->getActiveSheet();
	$objPHPExcel->getActiveSheet()->setTitle('Información');

	$borders = array(
			'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);


	$campos_x = "cae_id_i, emd_nombre, emd_cedula";
    $tabla_x = "gi_casos_especiales JOIN gi_empleados ON emd_id = cae_emd_id_i";
    $where_x = "";
    if(isset($_GET['soloCasos'])){
    	$where_x = " cae_tic_id_i IN (1, 3) ";
    }
    if(isset($_GET['soloIncapacitados'])){
    	$where_x = " cae_tic_id_i IN (3, 2) ";
    }
    if($_SESSION['cliente_id'] != 0){
        $where_x .= " AND emd_emp_id = ".$_SESSION['cliente_id'];
    }

    $reporte = ModeloTesoreria::mdlMostrarGroupAndOrder($campos_x,$tabla_x,$where_x, null, 'ORDER BY emd_nombre ASC');

	foreach ($reporte as $key => $valuePrincipal) {
		$newHoja = $objPHPExcel->createSheet($key);
		$newHoja->setTitle($valuePrincipal['emd_cedula']);

		$item  = 'emd_cedula';
		$valor = $valuePrincipal['emd_cedula']; 
		$empleado = ControladorEmpleados::ctrMostrarEmpleados($item, $valor);

		$item  = 'emp_id';
		$valor = $empleado['emd_emp_id']; 
		$empresa = ControladorClientes::ctrMostrarClientes($item, $valor);

		$newHoja->getStyle('B4:B8')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$newHoja->getStyle('B4:B8')->getFill()
		->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
		->getStartColor()
		->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);


		$newHoja->getStyle('B4:B10')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$newHoja->getStyle('B4:B10')->getFill()
		->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
		->getStartColor()
		->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
		$newHoja->getStyle('I4:I6')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$newHoja->getStyle('I4:I6')->getFill()
		->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
		->getStartColor()
		->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);


		$newHoja->mergeCells('B2:L2');
		$newHoja->setCellValue("B2", "HISTORICO DE INCAPACIDADES POR EMPLEADO"); 
		$newHoja->getStyle('B2:L2')->getFont()
		  ->setName('Arial')
		  ->setSize(24)
		  ->setBold(true);
		$newHoja->getStyle('B2:L2')
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


		/*CENTRO LAS LETRAS*/
		$newHoja->getStyle('B4:B10')
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$newHoja->getStyle('E4:E10')
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$newHoja->getStyle('I4:I10')
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$newHoja->getStyle('K4:K10')
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


		$newHoja->mergeCells('B4:D4');
		$newHoja->mergeCells('E4:H4');
		$newHoja->mergeCells('I4:J4');
		$newHoja->mergeCells('K4:L4');
		$newHoja->setCellValue("B4", "EMPRESA"); 
		$newHoja->setCellValue("E4", $empresa['emp_nombre']); 
		$newHoja->setCellValue("I4", "NIT"); 
		$newHoja->setCellValue("K4", $empresa['emp_nit']); 
		$newHoja->getStyle('B4')->getFont()->setBold( true );
		$newHoja->getStyle('I4')->getFont()->setBold( true );

		$newHoja->mergeCells('B5:D5');
		$newHoja->mergeCells('E5:H5');
		$newHoja->mergeCells('I5:J5');
		$newHoja->mergeCells('K5:L5');
		$newHoja->setCellValue("B5", "COLABORADOR"); 
		$newHoja->setCellValue("E5", $empleado['emd_nombre']); 
		$newHoja->setCellValue("I5", "IDENTIFICACION"); 
		$newHoja->setCellValue("K5", $empleado['emd_cedula']); 
		$newHoja->getStyle('B5')->getFont()->setBold( true );
		$newHoja->getStyle('I5')->getFont()->setBold( true );

		$newHoja->mergeCells('B6:D6');
		$newHoja->mergeCells('E6:H6');
		$newHoja->mergeCells('I6:J6');
		$newHoja->mergeCells('K6:L6');
		$newHoja->setCellValue("B6", "CARGO"); 
		$newHoja->setCellValue("E6", $empleado['emd_cargo']); 
		$newHoja->setCellValue("I6", "FECHA DE INGRESO"); 
		$date3 = new DateTime($empleado["emd_fecha_ingreso"]);
		$newHoja->setCellValue("K6", \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($date3));
		$newHoja->getStyle("K6")->getNumberFormat()->setFormatCode("dd/mm/yyyy");

		$newHoja->getStyle('B6')->getFont()->setBold( true );
		$newHoja->getStyle('I6')->getFont()->setBold( true );


		/*CENTRO LAS LETRAS*/
		$newHoja->getStyle('C7:C10')
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$newHoja->getStyle('G7:G10')
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$newHoja->getStyle('H7:H10')
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$newHoja->getStyle('J7:J10')
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$newHoja->getStyle('K7:K10')
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$newHoja->getStyle('G7:G10')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$newHoja->getStyle('G7:G10')->getFill()
		->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
		->getStartColor()
		->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);


		$newHoja->getStyle('J7:J10')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$newHoja->getStyle('J7:J10')->getFill()
		->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
		->getStartColor()
		->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);


		$newHoja->mergeCells('C7:F7');
		$newHoja->mergeCells('H7:I7');
		$newHoja->mergeCells('K7:L7');
		$newHoja->setCellValue("B7", "SEDE"); 
		$newHoja->setCellValue("C7", $empleado['emd_sede']); 
		$newHoja->setCellValue("G7", "CENTRO DE COSTOS"); 
		$newHoja->setCellValue("H7", $empleado['emd_centro_costos']); 
		$newHoja->setCellValue("J7", "CIUDAD"); 
		$newHoja->setCellValue("K7", $empleado['emd_ciudad']); 
		$newHoja->getStyle('B7')->getFont()->setBold( true );
		$newHoja->getStyle('G7')->getFont()->setBold( true );
		$newHoja->getStyle('J7')->getFont()->setBold( true );

		$newHoja->mergeCells('C8:F8');
		$newHoja->mergeCells('H8:I8');
		$newHoja->mergeCells('K8:L8');
		$newHoja->setCellValue("B8", "EPS"); 
		$newHoja->setCellValue("C8", $empleado['ips_nombre']); 
		$newHoja->setCellValue("G8", "FECHA AFILIACIÓN EPS"); 
		$newHoja->setCellValue("H8", $empleado['emd_fecha_afiliacion_eps']); 
		$newHoja->setCellValue("J8", "AFP"); 

		$item  = 'ips_id';
		$valor = $empleado['emd_afp_id']; 
		$afpX = ControladorEmpleados::getData('gi_ips', $item, $valor);
		
		if (!empty($afpX)) {
			$newHoja->setCellValue("K8", $afpX['ips_nombre']);
		}

		$newHoja->getStyle('B8')->getFont()->setBold( true );
		$newHoja->getStyle('G8')->getFont()->setBold( true );
		$newHoja->getStyle('J8')->getFont()->setBold( true );



		$item  = 'cae_emd_id_i';
		$valor = $empleado['emd_id']; 
		$concepto = ControladorEmpleados::getData('gi_casos_especiales', $item, $valor);


		$newHoja->mergeCells('C9:F9');
		$newHoja->mergeCells('H9:I9');
		$newHoja->mergeCells('K9:L9');
		$newHoja->setCellValue("B9", "CONCEPTO REHABILITACIÓN"); 
		$newHoja->setCellValue("C9", $concepto['cae_con_rea_v']); 
		$newHoja->setCellValue("G9", "FECHA CONCEPTO"); 

		if($concepto['cae_xon_fecha_d'] != null && $concepto['cae_xon_fecha_d'] != ''){
			$dateX = new DateTime($concepto['cae_xon_fecha_d']);
			$newHoja->setCellValue("H9", \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($dateX));
			$newHoja->getStyle("H9")->getNumberFormat()->setFormatCode("dd/mm/yyyy");
		}
			

		$newHoja->setCellValue("J9", "DIAGNOSTICO"); 
		$newHoja->setCellValue("K9", $concepto['cae_condia_id_i']);
		$newHoja->getStyle('B9')->getFont()->setBold( true );
		$newHoja->getStyle('G9')->getFont()->setBold( true );
		$newHoja->getStyle('J9')->getFont()->setBold( true );


		$newHoja->mergeCells('C10:F10');
		$newHoja->mergeCells('H10:I10');
		$newHoja->mergeCells('K10:L10');
		$newHoja->setCellValue("B10", "CALIFICACIÓN PCL"); 
		$newHoja->setCellValue("C10", $concepto['cae_cal_v']); 
		$newHoja->setCellValue("G10", "FECHA CALIFICACIÓN"); 
		$newHoja->setCellValue("H10", $concepto['cae_cal_fecha_d']); 
		$newHoja->setCellValue("J10", "ENTIDAD"); 
		$itemXD  = 'ips_id';
		$valorXD = $concepto['cae_cal_entidad_i']; 
		$afpXD = ControladorEmpleados::getData('gi_ips', $itemXD, $valorXD);
		if($afpXD != false)
			$newHoja->setCellValue("K10", $afpXD['ips_nombre']); 

		$newHoja->getStyle('B10')->getFont()->setBold( true );
		$newHoja->getStyle('G10')->getFont()->setBold( true );
		$newHoja->getStyle('J10')->getFont()->setBold( true );


		/*Incapacidades*/
		$newHoja->setCellValue("A12", "#"); 
		$newHoja->setCellValue("B12", "FECHA INICIAL"); 
		$newHoja->setCellValue("C12", "FECHA FINAL"); 
		$newHoja->setCellValue("D12", "DIAS"); 
		$newHoja->setCellValue("E12", "CLASIFICACIÓN"); 
		$newHoja->setCellValue("F12", "DX");
		$newHoja->setCellValue("G12", "ORIGEN"); 
		$newHoja->setCellValue("H12", "ESTADO TRAMITE"); 
		$newHoja->setCellValue("I12", "FECHA ESTADO");
		$newHoja->setCellValue("J12", "No. RADICADO"); 
		$newHoja->setCellValue("K12", "VALOR PAGADO"); 
		$newHoja->setCellValue("L12", "FECHA PAGO"); 
		$newHoja->getStyle('A12:L12')->getFont()->setBold( true );
		$newHoja->getStyle('A12:L12')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$newHoja->getStyle('A12:L12')->getFill()
		->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
		->getStartColor()
		->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
		$newHoja->getStyle('A12:L12')
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


		$item   = 'inc_emd_id';
		$valor  =  $empleado['emd_id'];
		$incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor, null, null, null, '%Y-%m-%d', null);

		$totalDIas = 0;
		$valoPagado = 0;
		$i = 13;
		foreach ($incapacidades as $key => $value) {
		 	 //$totalDIas += dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]);
		  	$valorP = 0;
		  	if($value["inc_valor"] != null && $value["inc_valor"] != ''){
	  	  		if(is_numeric($value["inc_valor"]))
		      		$valoPagado += $value["inc_valor"];
		      		$valorP = trim($value["inc_valor"]);
		  	} 

		  	$strFecha = $value['inc_Fecha_estado'];

			$newHoja->setCellValue("A".$i, ($key+1)); 
			$date3 = new DateTime(explode(' ', $value["inc_fecha_inicio"])[0]);
			$newHoja->setCellValue("B".$i, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($date3));
			$newHoja->getStyle("B".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
			$date4 = new DateTime(explode(' ', $value["inc_fecha_final"])[0]);
			$newHoja->setCellValue("C".$i, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($date4));
			$newHoja->getStyle("C".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
			$newHoja->setCellValue("D".$i, ControladorIncapacidades::dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"]));

			$newHoja->setCellValue("E".$i, $value["inc_clasificacion"] );
			$newHoja->setCellValue("F".$i, $value["inc_diagnostico"]);
			$newHoja->setCellValue("G".$i, $value["inc_origen"]); 
			$newHoja->setCellValue("H".$i, $value["inc_estado_tramite"]); 
			$date5 = new DateTime($strFecha);
			$newHoja->setCellValue("I".$i, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($date5));
			$newHoja->getStyle("I".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
			$newHoja->setCellValue("J".$i, $value["inc_numero_radicado_v"]); 
			$newHoja->setCellValue("K".$i, $value["inc_valor"]); 
			$newHoja->getStyle("K".$i)
				->getNumberFormat()
				->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
			if($value["inc_fecha_pago"] != null && $value["inc_fecha_pago"] != ''){
			  	$date6 = new DateTime($value["inc_fecha_pago"]);
			  	$newHoja->setCellValue("L".$i, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($date6));
			  	$newHoja->getStyle("L".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
			}
		         
		  	$i++;
		}

		foreach(range('A','Q') as $columnID) {
		  $newHoja->getColumnDimension($columnID)
		      ->setAutoSize(true);
		}

		$k = 2;
		$newHoja->mergeCells('O'.$k.':Q'.$k);
		$newHoja->setCellValue('O'.$k, "COMENTARIOS"); 
		$newHoja->getStyle('O'.$k)->getFont()->setBold( true );
		$newHoja->getStyle('O'.$k)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$newHoja->getStyle('O'.$k)->getFill()
		->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
		->getStartColor()
		->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
		$newHoja->getStyle('O'.$k)
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		/*Espacio para comentarios*/
		$comentarios = ControladorIncapacidades::getDataFromLsql('incm_comentario_t, inc_fecha_inicio ', 'gi_incapacidades_comentarios JOIN gi_incapacidad ON inc_id = inm_inc_id_i ', "inc_cc_afiliado = '".$valuePrincipal['emd_cedula']."'", null, 'ORDER BY incm_fecha_comentario ASC', null );
		$k = 3;
		foreach($comentarios as $key => $val){
		  $date3 = new DateTime(explode(' ', $val["inc_fecha_inicio"])[0]);
		  $newHoja->setCellValue("P".$k, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($date3));
		  $newHoja->getStyle("P".$k)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
		  $newHoja->setCellValue("Q".$k, $val["incm_comentario_t"]); 
		  $newHoja->setCellValue("O".$k, ($key+1)); 
		  $k++;
		}


		$i = $i+3;
		$newHoja->mergeCells('J'.$i.':K'.$i);
		$newHoja->setCellValue('J'.$i, "FECHA DE REPORTE"); 
		$newHoja->getStyle('J'.$i)->getFont()->setBold( true );
		$newHoja->getStyle('J'.$i)
		      ->getAlignment()
		      ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$date7 = new DateTime(date('Y-m-d'));
		$newHoja->setCellValue("L".$i, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($date7));
		$newHoja->getStyle("L".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
	        
	}

	$objPHPExcel->setActiveSheetIndex(0);

	/*************************NUEVA HOJA*************************/
   	// Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="reporte_casos_Especiales_consolidado'.date('Y_m_d_H_i_s').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer = new Xlsx($objPHPExcel);
    $writer->save('php://output');

