<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" /><!-- Content Wrapper. Contains page content -->

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">REPORTES CASOS ESPECIALES - INCAPACITADOS PERMANENTES</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Casos Especiales</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">

                    <div class="row">
                        <div class="col-lg-11">
                            CASOS ESPECIALES
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">
                                    <a class="dropdown-item" href="index.php?exportar=exportarCasosEspeciales&soloIncapacitados=si" title="Exportar a Excel">Exp. Casos</a>

                                    <div class="dropdown-divider"></div>

                                    <a class="dropdown-item" href="index.php?exportar=exportarCasosEspecialesConsolidado&soloIncapacitados=si" title="Exportar a Excel">Exp. Casos Con.</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
                <table class="table-hover table table-bordered tblGeneralPagos" id="tablaGeneralCasos">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Identificación</th>
                            <th>Apellidos y Nombres</th>
                            <th>Cargo</th>
                            <th>Entidad</th>
                            <th>Días Acum</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $campos = "cae_id_i, cae_emd_id_i, cae_con_rea_v, cae_xon_fecha_d, cae_condia_id_i, cae_con_entidad_id_i, cae_cal_v, cae_cal_fecha_d, cae_cal_dia_id_i, cae_cal_entidad_i, cae_cal_jur_v, cae_cal_jur_fecha_d, cae_cal_jur_dia_id_i, cae_cal_jur_entidad_v, cae_cal_jun_v, cae_cal_jun_fecha_d, cae_cal_jun_dia_id_i, cae_cal_jun_entidad_v, emd_nombre, emd_cedula, emd_cargo, ips_nombre";
                        $tabla = "gi_casos_especiales JOIN gi_empleados ON emd_id = cae_emd_id_i JOIN gi_ips ON emd_eps_id = ips_id";
                        $where = " cae_tic_id_i IN (2, 3) ";
                        if ($_SESSION['cliente_id'] != 0) {
                            $where .= " AND emd_emp_id = " . $_SESSION['cliente_id'];
                        }

                        $reporte = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $where, null, 'ORDER BY emd_nombre ASC');

                        foreach ($reporte as $key => $value) {
                            echo '<tr>';
                            echo '<td>' . ($key + 1) . '</td>';
                            echo '<td>' . $value['emd_cedula'] . '</td>';
                            echo '<td>' . $value['emd_nombre'] . '</td>';
                            echo '<td>' . $value['emd_cargo'] . '</td>';
                            echo '<td>' . $value['ips_nombre'] . '</td>';
                            echo '<td></td>';
                            echo '<td>
                                                <button title="Ver detalle del caso" class="btn btn-danger btn-sm btnVerCasosEspeciales" numeroId="' . $value['cae_id_i'] . '" data-bs-toggle="modal" data-bs-target="#modalVerCasosEspeciales"><i class="fa fa-search"></i></button>
                                                <button title="Notificar Casos Especiales" class="btn btn-info btn-sm btnEnviarCasosEspeciales" numeroId="' . $value['emd_cedula'] . '"><i class="fa fa-envelope"></i></button>
                                            </td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Identificación</th>
                            <th>Apellidos y Nombres</th>
                            <th>Cargo</th>
                            <th>Entidad</th>
                            <th>Días Acum</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="modalVerCasosEspeciales" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Detalles del caso</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="close"></button>
                </div>
                <div class="modal-body">
                    <div class="box-body" style="text-align: center;">
                        <table class="table table-bordered">
                            <tr>
                                <th style="width: 50%;">IDENTIFICACIÓN</th>
                                <th style="width: 50%;">APELLIDOS Y NOMBRES</th>
                            </tr>
                            <tr>
                                <td id="tdIdent"></td>
                                <td id="tdApell"></td>
                            </tr>
                        </table>
                        <br>
                        <table class="table table-bordered">
                            <tr>
                                <th style="width: 33%;">CARGO</th>
                                <th style="width: 33%;">FECHA DE INGRESO</th>
                                <th style="width: 33%;">SALARIO</th>
                            </tr>
                            <tr>
                                <td id="tdCargo"></td>
                                <td id="tdFechaIngreso"></td>
                                <td id="tdSalario"></td>
                            </tr>
                        </table>
                        <br />
                        <table class="table table-bordered">
                            <tr>
                                <th style="width: 33%;">EPS</th>
                                <th style="width: 33%;">AFP</th>
                                <th style="width: 34%;">ARL</th>
                            </tr>
                            <tr>
                                <td id="tdEps"></td>
                                <td id="tdAfp"></td>
                                <td id="tdArl"></td>
                            </tr>
                        </table>

                        <br>
                        <table class="table table-bordered">
                            <tr>
                                <th style="width: 25%;">SEDE</th>
                                <th style="width: 25%;">CENTRO DE COSTO</th>
                                <th style="width: 25%;">CIUDAD</th>
                                <th style="width: 25%;">SUBCENTRO DE COSTO</th>
                            </tr>
                            <tr>
                                <td id="tdSede"></td>
                                <td id="tdCeCo"></td>
                                <td id="tdCiud"></td>
                                <td id="tdSubC"></td>
                            </tr>
                        </table>
                        <br />
                        <table class="table table-bordered">
                            <tr>
                                <th style="width: 40%;">CONCEPTO REHABILITACIÓN</th>
                                <th style="width: 20%;">FECHA DE CONCEPTO</th>
                                <th style="width: 20%;">DIAGNOSTICO</th>
                                <th style="width: 20%;">ENTIDAD</th>
                            </tr>
                            <tr>
                                <td id="tdCoRe"></td>
                                <td id="tdFeCo"></td>
                                <td id="tdDiCo"></td>
                                <td id="tdEnCo"></td>
                            </tr>
                            <tr>
                                <th>CALIFICACIÓN DE PCL</th>
                                <th>FECHA DE CALIFICACIÓN</th>
                                <th>DIAGNOSTICO</th>
                                <th>ENTIDAD</th>
                            </tr>
                            <tr>
                                <td id="tdCalPCL"> </td>
                                <td id="tdFecPCL"></td>
                                <td id="tdDiaPCL"></td>
                                <td id="tdEntPCL"></td>
                            </tr>

                            <tr>
                                <th>CALIFICACIÓN JUNTA REGIONAL</th>
                                <th>FECHA DE CALIFICACIÓN</th>
                                <th>DIAGNOSTICO</th>
                                <th>ENTIDAD</th>
                            </tr>
                            <tr>
                                <td id="tdCalJuR"></td>
                                <td id="tdFecJuR"></td>
                                <td id="tdDiaJuR"></td>
                                <td id="tdEntJuR"></td>
                            </tr>

                            <tr>
                                <th>CALIFICACIÓN JUNTA NACIONAL</th>
                                <th>FECHA DE CALIFICACIÓN</th>
                                <th>DIAGNOSTICO</th>
                                <th>ENTIDAD</th>
                            </tr>
                            <tr>
                                <td id="tdCalJuN"></td>
                                <td id="tdFecJuN"></td>
                                <td id="tdDiaJuN"></td>
                                <td id="tdEntJuN"></td>
                            </tr>
                        </table>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea placeholder="Escribir un comentario..." id="txtComentario" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered table-hover" id="tablaComentarios">

                                </table>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <input type="hidden" name="txtIdCaso" id="txtIdCaso">
                    <input type="hidden" name="idComentari" id="idComentari" value="0">
                    <button type="button" class="btn btn-default pull-left" data-bs-dismiss="modal">Salir</button>
                    <button type="button" id="guardarInfo" class="btn btn-primary pull-rigth" >Adicionar Comentarios</button>
                    <a class="btn btn-danger" target="_blank" id="exportarPdfCasosEspeciales">Exportar PDF</a>
                    <a class="btn btn-danger" target="_blank" id="exportarExcelCasosEspeciales">Exportar Excel</a>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

<!-- Main content -->
<!-- <section class="content">
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-title">Casos especiales registrados en el sistema</h3>
                <div class="box-tools">
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                            ACCIONES
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                           
                            <li><a href="index.php?exportar=exportarCasosEspeciales&soloIncapacitados=si" title="Exportar a Excel">Exp. Casos</a></li>
                            
                            <li class="divider"></li>
                            
                            <li><a href="index.php?exportar=exportarCasosEspecialesConsolidado&soloIncapacitados=si" title="Exportar a Excel">Exp. Casos Con.</a></li>
                        
                        </ul>
                    </div>
                
                </div>
            </div>
            <div class="box-body">
                <table class="table table-hover table-bordered tblGeneralPagos" id="tablaGeneralCasos">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Identificación</th>
                            <th>Apellidos y Nombres</th>
                            <th>Cargo</th>
                            <th>Entidad</th>
                            <th>Días Acum</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $campos = "cae_id_i, cae_emd_id_i, cae_con_rea_v, cae_xon_fecha_d, cae_condia_id_i, cae_con_entidad_id_i, cae_cal_v, cae_cal_fecha_d, cae_cal_dia_id_i, cae_cal_entidad_i, cae_cal_jur_v, cae_cal_jur_fecha_d, cae_cal_jur_dia_id_i, cae_cal_jur_entidad_v, cae_cal_jun_v, cae_cal_jun_fecha_d, cae_cal_jun_dia_id_i, cae_cal_jun_entidad_v, emd_nombre, emd_cedula, emd_cargo, ips_nombre";
                        $tabla = "gi_casos_especiales JOIN gi_empleados ON emd_id = cae_emd_id_i JOIN gi_ips ON emd_eps_id = ips_id";
                        $where = " cae_tic_id_i IN (2, 3) ";
                        if ($_SESSION['cliente_id'] != 0) {
                            $where .= " AND emd_emp_id = " . $_SESSION['cliente_id'];
                        }

                        $reporte = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $where, null, 'ORDER BY emd_nombre ASC');

                        foreach ($reporte as $key => $value) {
                            echo '<tr>';
                            echo '<td>' . ($key + 1) . '</td>';
                            echo '<td>' . $value['emd_cedula'] . '</td>';
                            echo '<td>' . $value['emd_nombre'] . '</td>';
                            echo '<td>' . $value['emd_cargo'] . '</td>';
                            echo '<td>' . $value['ips_nombre'] . '</td>';
                            echo '<td></td>';
                            echo '<td>
                                        <button title="Ver detalle del caso" class="btn btn-danger btn-sm btnVerCasosEspeciales" numeroId="' . $value['cae_id_i'] . '" data-toggle="modal" data-target="#modalVerCasosEspeciales"><i class="fa fa-search"></i></button>
                                        <button title="Notificar Casos Especiales" class="btn btn-info btn-sm btnEnviarCasosEspeciales" numeroId="' . $value['emd_cedula'] . '"><i class="fa fa-envelope"></i></button>
                                    </td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Identificación</th>
                            <th>Apellidos y Nombres</th>
                            <th>Cargo</th>
                            <th>Entidad</th>
                            <th>Días Acum</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
</section> -->
<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript">
    $(function() {

        $('#tablaGeneralCasos').DataTable({
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }

        });

        $("#guardarInfo").click(function() {
            if ($("#txtComentario").val().length > 1) {
                var datas = new FormData();
                datas.append('ComentarioCasos', $("#txtComentario").val());
                datas.append('idCasoEspecial', $("#txtIdCaso").val());
                datas.append('userID', <?php echo $_SESSION['id_Usuario']; ?>);
                datas.append('idComentario', $("#idComentari").val());
                $.ajax({
                    url: 'ajax/empleados.ajax.php',
                    method: 'post',
                    data: datas,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data.code == '1') {
                            /*Bien*/
                        } else {
                            /*Ni verga*/
                        }
                    },
                    beforeSend: function() {
                        $.blockUI({
                            message: '<h3>Un momento por favor....</h3>',
                            baseZ: 2000,
                            css: {
                                border: 'none',
                                padding: '1px',
                                backgroundColor: '#000',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                        $("#txtComentario").val('');
                        getComentarios($("#txtIdCaso").val());
                    }
                });
            } else {
                alertify.error("No hay comentario que agregar!");
            }
        });

        /* Editar Empleados */
        $('#tablaGeneralCasos tbody').on("click", ".btnVerCasosEspeciales", function() {
            var x = $(this).attr('numeroId');
            var datas = new FormData();
            $("#txtIdCaso").val(x);
            datas.append('traerCasoEspecial', x);
            $("#exportarPdfCasosEspeciales").attr('href', '#');
            $.ajax({
                url: 'ajax/empleados.ajax.php',
                method: 'post',
                data: datas,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data) {
                    $("#tdIdent").html(data.emd_cedula);
                    $("#tdApell").html(data.emd_nombre);
                    $("#tdCargo").html(data.emd_cargo);
                    $("#tdEps").html(data.eps);
                    $("#tdAfp").html(data.afp);


                    $("#tdSede").html(data.emd_sede);
                    $("#tdCeCo").html(data.emd_centro_costos);
                    $("#tdCiud").html(data.emd_ciudad);
                    $("#tdSubC").html(data.emd_subcentro_costos);

                    $("#tdCoRe").html(data.cae_con_rea_v);
                    $("#tdFeCo").html(data.cae_xon_fecha_d);
                    $("#tdDiCo").html(data.cae_condia_id_i);
                    $("#tdEnCo").html(data.cae_con_entidad_id_i);

                    $("#tdCalPCL").html(data.cae_cal_v);
                    $("#tdFecPCL").html(data.cae_cal_fecha_d);
                    $("#tdDiaPCL").html(data.cae_cal_dia_id_i);
                    $("#tdEntPCL").html(data.cae_cal_entidad_i);

                    $("#tdCalJuR").html(data.cae_cal_jur_v);
                    $("#tdFecJuR").html(data.cae_cal_jur_fecha_d);
                    $("#tdDiaJuR").html(data.cae_cal_jur_dia_id_i);
                    $("#tdEntJuR").html(data.cae_cal_jur_entidad_v);

                    $("#tdCalJuN").html(data.cae_cal_jun_v);
                    $("#tdFecJuN").html(data.cae_cal_jun_fecha_d);
                    $("#tdDiaJuN").html(data.cae_cal_jun_dia_id_i);
                    $("#tdEntJuN").html(data.cae_cal_jun_entidad_v);

                    $("#tdFechaIngreso").html(data.emd_fecha_ingreso);
                    $("#tdSalario").html("$ " + addCommas(data.emd_salario));
                    $("#tdArl").html(data.arl);

                    getComentarios($("#txtIdCaso").val());
                    $("#exportarPdfCasosEspeciales").attr('href', 'index.php?exportar=exportarCasosEspecialesPDF&traerCasoEspecial=' + x);

                    $("#exportarExcelCasosEspeciales").attr('href', 'index.php?exportar=excelEmpleadosconsRepor&cc=' + data.emd_cedula);

                },
                beforeSend: function() {
                    $.blockUI({
                        message: '<h3>Un momento por favor....</h3>',
                        css: {
                            border: 'none',
                            padding: '1px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
                },
                complete: function() {
                    $.unblockUI();
                }
            });
        });

        $("#tablaGeneralCasos tbody").on('click', ".btnEnviarCasosEspeciales", function() {
            var x = $(this).attr('numeroId');
            var datas = new FormData();
            datas.append('sendMailIncapacidades', x);
            $.ajax({
                url: 'ajax/enviarCorreoCasosEspeciales.ajax.php',
                method: 'post',
                data: datas,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(data) {

                    if (data.code == '1') {
                        alertify.success("Correo enviado");
                    } else {
                        alertify.error("Correo no enviado");
                    }
                },
                beforeSend: function() {
                    $.blockUI({
                        message: '<h3>Un momento por favor....</h3>',
                        css: {
                            border: 'none',
                            padding: '1px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
                },
                complete: function() {
                    tablaIncapa.ajax.reload();
                    $.unblockUI();
                }
            });
        });
    });

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function getComentarios(idCaso) {
        var datas = new FormData();
        datas.append('getComentarios', idCaso);
        $.ajax({
            url: 'ajax/empleados.ajax.php',
            method: 'post',
            data: datas,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'html',
            success: function(data) {
                $("#tablaComentarios").html(data);
                $("#idComentari").val('0');

                $(".btnEliminarComentario").click(function() {
                    var comenrai = $(this).attr('idComentario');
                    var datas = new FormData();
                    datas.append('delComentarios', comenrai);
                    $.ajax({
                        url: 'ajax/empleados.ajax.php',
                        method: 'post',
                        data: datas,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        success: function(data) {
                            if (data.code == '1') {
                                /*Bien*/
                            } else {
                                /*Ni verga*/
                            }
                        },
                        beforeSend: function() {
                            $.blockUI({
                                message: '<h3>Un momento por favor....</h3>',
                                baseZ: 2000,
                                css: {
                                    border: 'none',
                                    padding: '1px',
                                    backgroundColor: '#000',
                                    '-webkit-border-radius': '10px',
                                    '-moz-border-radius': '10px',
                                    opacity: .5,
                                    color: '#fff'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                            $("#txtComentario").val('');
                            getComentarios($("#txtIdCaso").val());
                        }
                    });
                });

                $(".btnEditarComentario").click(function() {
                    var comenrai = $(this).attr('idComentario');
                    var datas = new FormData();
                    datas.append('getOneComentari', comenrai);
                    $.ajax({
                        url: 'ajax/empleados.ajax.php',
                        method: 'post',
                        data: datas,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        success: function(data) {
                            if (data.code != false) {
                                /*Bien*/
                                $("#txtComentario").val(data.cecom_comentario_v);
                                $("#idComentari").val(data.cecom_id_i);

                            }
                        },
                        beforeSend: function() {
                            $.blockUI({
                                message: '<h3>Un momento por favor....</h3>',
                                baseZ: 2000,
                                css: {
                                    border: 'none',
                                    padding: '1px',
                                    backgroundColor: '#000',
                                    '-webkit-border-radius': '10px',
                                    '-moz-border-radius': '10px',
                                    opacity: .5,
                                    color: '#fff'
                                }
                            });
                        },
                        complete: function() {
                            $.unblockUI();
                        }
                    });
                });

            }
        });
    }
</script>