<?php 
	if(isset($_GET['traerCasoEspecial'])){

	    $objPHPExcel = new PHPExcel();
		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getProperties()->setCreator("GEIN")
		                     ->setLastModifiedBy("".$_SESSION['nombres'])
		                     ->setTitle("GEIN - Casos especiales")
		                     ->setSubject("Casos especiales")
		                     ->setDescription("Descarga el reporte de Casos especiales, registrado en el sistema")
		                     ->setKeywords("office 2007 openxml php")
		                     ->setCategory("Casos especiales");

		$objPHPExcel->getActiveSheet();
		$objPHPExcel->getActiveSheet()->setTitle('Información');

		$borders = array(
				'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('argb' => 'FF000000'),
				)
			),
		);

	 	$campos = "cae_id_i, cae_emd_id_i, cae_con_rea_v, cae_xon_fecha_d, cae_condia_id_i, cae_con_entidad_id_i, cae_cal_v, cae_cal_fecha_d, cae_cal_dia_id_i, cae_cal_entidad_i, cae_cal_jur_v, cae_cal_jur_fecha_d, cae_cal_jur_dia_id_i, cae_cal_jur_entidad_v, cae_cal_jun_v, cae_cal_jun_fecha_d, cae_cal_jun_dia_id_i, cae_cal_jun_entidad_v, emd_nombre, emd_cedula, emd_cargo, a.ips_nombre as eps, b.ips_nombre as afp, emd_ciudad, emd_subcentro_costos , emd_centro_costos , emd_sede , c.ips_nombre as arl, emd_salario, emd_fecha_ingreso, emp_nombre " ;
        $tabla = "gi_casos_especiales JOIN gi_empleados ON emd_id = cae_emd_id_i JOIN gi_ips a ON emd_eps_id = a.ips_id LEFT JOIN gi_ips b ON emd_afp_id  = b.ips_id LEFT JOIN gi_ips c ON emd_arl_id  = c.ips_id JOIN gi_empresa ON emp_id = emd_emp_id";
        $where = "cae_id_i = ".$_GET['traerCasoEspecial'];
        
        $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos,$tabla,$where);


        $objPHPExcel->getActiveSheet()->setCellValue("A1", "IDENTIFICACIÓN");
        $objPHPExcel->getActiveSheet()->setCellValue("B1", "APELLIDOS Y NOMBRES");
        $objPHPExcel->getActiveSheet()->setCellValue("A2", $respuesta['emd_cedula']);
        $objPHPExcel->getActiveSheet()->setCellValue("B2", $respuesta['emd_nombre']);
       

        $objPHPExcel->getActiveSheet()->setCellValue("A4", "CARGO");
        $objPHPExcel->getActiveSheet()->setCellValue("B4", "FECHA DE INGRESO");
        $objPHPExcel->getActiveSheet()->setCellValue("C4", "SALARIO");
        $objPHPExcel->getActiveSheet()->setCellValue("A5", $respuesta['emd_cargo']);
        $date2 = new DateTime($respuesta['emd_fecha_ingreso']);
        $objPHPExcel->getActiveSheet()->setCellValue("B5", PHPExcel_Shared_Date::PHPToExcel($date2));
        $objPHPExcel->getActiveSheet()->getStyle("B5")->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $objPHPExcel->getActiveSheet()->setCellValue("C5", $respuesta['emd_salario']);
        $objPHPExcel->getActiveSheet()->getStyle('C5')->getNumberFormat()->setFormatCode('#,##0');

        $objPHPExcel->getActiveSheet()->setCellValue("A7", "EPS");
        $objPHPExcel->getActiveSheet()->setCellValue("B7", "AFP");
        $objPHPExcel->getActiveSheet()->setCellValue("C7", "ARL");
        $objPHPExcel->getActiveSheet()->setCellValue("A8", $respuesta['eps']);
        $objPHPExcel->getActiveSheet()->setCellValue("B8", $respuesta['afp']);
        $objPHPExcel->getActiveSheet()->setCellValue("C8", $respuesta['arl']);

       
        $objPHPExcel->getActiveSheet()->getStyle('A7:C7')->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle('A4:C4')->getFont()->setBold( true );
     	$objPHPExcel->getActiveSheet()->getStyle('A1:B1')->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle('A10:D10')->getFont()->setBold( true );

		$objPHPExcel->getActiveSheet()->getStyle('A13:D13')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('A15:D15')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('A17:D17')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('A19:D19')->getFont()->setBold( true );

        

     	$objPHPExcel->getActiveSheet()->setCellValue("A10", "SEDE");
        $objPHPExcel->getActiveSheet()->setCellValue("B10", "CENTRO DE COSTO");
        $objPHPExcel->getActiveSheet()->setCellValue("C10", "CIUDAD");
        $objPHPExcel->getActiveSheet()->setCellValue("D10", "SUBCENTRO DE COSTO");

        $objPHPExcel->getActiveSheet()->setCellValue("A11", $respuesta['emd_sede']);
        $objPHPExcel->getActiveSheet()->setCellValue("B11", $respuesta['emd_centro_costos']);
        $objPHPExcel->getActiveSheet()->setCellValue("C11", $respuesta['emd_ciudad']);
        $objPHPExcel->getActiveSheet()->setCellValue("D11", $respuesta['emd_subcentro_costos']);



        $objPHPExcel->getActiveSheet()->setCellValue("A13", "CONCEPTO REHABILITACIÓN");
        $objPHPExcel->getActiveSheet()->setCellValue("B13", "FECHA DE CONCEPTO");
        $objPHPExcel->getActiveSheet()->setCellValue("C13", "DIAGNOSTICO");
        $objPHPExcel->getActiveSheet()->setCellValue("D13", "ENTIDAD");

         $objPHPExcel->getActiveSheet()->setCellValue("A14", $respuesta['cae_con_rea_v']);
     	$date2 = new DateTime($respuesta['cae_xon_fecha_d']);
        $objPHPExcel->getActiveSheet()->setCellValue("B14", PHPExcel_Shared_Date::PHPToExcel($date2));
        $objPHPExcel->getActiveSheet()->getStyle("B14")->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $objPHPExcel->getActiveSheet()->setCellValue("C14", $respuesta['cae_condia_id_i']);
        $objPHPExcel->getActiveSheet()->setCellValue("D14", $respuesta['cae_con_entidad_id_i']);


        $objPHPExcel->getActiveSheet()->setCellValue("A15", "CALIFICACIÓN DE PCL");
        $objPHPExcel->getActiveSheet()->setCellValue("B15", "FECHA DE CALIFICACIÓN");
        $objPHPExcel->getActiveSheet()->setCellValue("C15", "DIAGNOSTICO");
        $objPHPExcel->getActiveSheet()->setCellValue("D15", "ENTIDAD");

        $objPHPExcel->getActiveSheet()->setCellValue("A16", $respuesta['cae_cal_v']);
        $date3 = new DateTime($respuesta['cae_cal_fecha_d']);
        $objPHPExcel->getActiveSheet()->setCellValue("B16", PHPExcel_Shared_Date::PHPToExcel($date3));
        $objPHPExcel->getActiveSheet()->getStyle("B16")->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $objPHPExcel->getActiveSheet()->setCellValue("C16", $respuesta['cae_cal_dia_id_i']);
        $objPHPExcel->getActiveSheet()->setCellValue("D16", $respuesta['cae_cal_entidad_i']);


        $objPHPExcel->getActiveSheet()->setCellValue("A17", "CALIFICACIÓN JUNTA REGIONAL");
        $objPHPExcel->getActiveSheet()->setCellValue("B17", "FECHA DE CALIFICACIÓN");
        $objPHPExcel->getActiveSheet()->setCellValue("C17", "DIAGNOSTICO");
        $objPHPExcel->getActiveSheet()->setCellValue("D17", "ENTIDAD");

        $objPHPExcel->getActiveSheet()->setCellValue("A18", $respuesta['cae_cal_jur_v']);
     	$date4 = new DateTime($respuesta['cae_cal_jur_fecha_d']);
        $objPHPExcel->getActiveSheet()->setCellValue("B18", PHPExcel_Shared_Date::PHPToExcel($date4));
        $objPHPExcel->getActiveSheet()->getStyle("B18")->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $objPHPExcel->getActiveSheet()->setCellValue("C18", $respuesta['cae_cal_jur_dia_id_i']);
        $objPHPExcel->getActiveSheet()->setCellValue("D18", $respuesta['cae_cal_jur_entidad_v']);


        $objPHPExcel->getActiveSheet()->setCellValue("A19", "CALIFICACIÓN JUNTA NACIONAL");
        $objPHPExcel->getActiveSheet()->setCellValue("B19", "FECHA DE CALIFICACIÓN");
        $objPHPExcel->getActiveSheet()->setCellValue("C19", "DIAGNOSTICO");
        $objPHPExcel->getActiveSheet()->setCellValue("D19", "ENTIDAD");

    
        $objPHPExcel->getActiveSheet()->setCellValue("A20", $respuesta['cae_cal_jun_v']);
        $date5 = new DateTime($respuesta['cae_cal_jun_fecha_d']);
        $objPHPExcel->getActiveSheet()->setCellValue("B20", PHPExcel_Shared_Date::PHPToExcel($date5));
        $objPHPExcel->getActiveSheet()->getStyle("B20")->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
        $objPHPExcel->getActiveSheet()->setCellValue("C20", $respuesta['cae_cal_jun_dia_id_i']);
        $objPHPExcel->getActiveSheet()->setCellValue("D20", $respuesta['cae_cal_jun_entidad_v']);


        $objPHPExcel->getActiveSheet()->mergeCells('A22:H22');
	 	$objPHPExcel->getActiveSheet()->getStyle('A22:H22')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("A22", "HISTORICO DE INCAPACIDADES"); 
		$objPHPExcel->getActiveSheet()->getStyle('A22:H22')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('A22:H22')
    	->getAlignment()
    	->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$campos = " inc_diagnostico , inc_fecha_inicio, inc_fecha_final, inc_estado_tramite,    inc_valor , inc_fecha_pago" ;
	    $tabla = "gi_incapacidad";
	    $where = "inc_emd_id = ".$respuesta['cae_emd_id_i'];
	    
	    $respuestaX = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$where, null, 'ORDER BY inc_fecha_inicio DESC');

	    $valorMax = 0;
	    $objPHPExcel->getActiveSheet()->setCellValue("A23", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("B23", "FECHA INICIO");
        $objPHPExcel->getActiveSheet()->setCellValue("C23", "FECHA FINAL");
        $objPHPExcel->getActiveSheet()->setCellValue("D23", "DÍAS");
        $objPHPExcel->getActiveSheet()->setCellValue("E23", "DIAGNOSTICO");
        $objPHPExcel->getActiveSheet()->setCellValue("F23", "ESTADO");
        $objPHPExcel->getActiveSheet()->setCellValue("G23", "VALOR");
        $objPHPExcel->getActiveSheet()->setCellValue("H23", "FECHA PAGO");
        
        $i = 24;
	    foreach ($respuestaX as $key => $value) {
    		$objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key+1));
		 	$date2 = new DateTime($value['inc_fecha_inicio']);
	        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, PHPExcel_Shared_Date::PHPToExcel($date2));
	        $objPHPExcel->getActiveSheet()->getStyle("B".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 

	        $date2 = new DateTime($value['inc_fecha_final']);
	        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, PHPExcel_Shared_Date::PHPToExcel($date2));
	        $objPHPExcel->getActiveSheet()->getStyle("C".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 

	        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, ControladorIncapacidades::dias_transcurridos($value['inc_fecha_inicio'], $value['inc_fecha_final']));
	        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value['inc_diagnostico']);
	        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value['inc_estado_tramite']);
	        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value['inc_valor']);
	        $objPHPExcel->getActiveSheet()->getStyle("G".$i)->getNumberFormat()->setFormatCode('#,##0');
	        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, "FECHA PAGO");
	        if($value['inc_fecha_pago'] != null && $value['inc_fecha_pago'] != ''){
	        	$date2 = new DateTime($value['inc_fecha_pago']);
	        	$objPHPExcel->getActiveSheet()->setCellValue("C".$i, PHPExcel_Shared_Date::PHPToExcel($date2));
	        	$objPHPExcel->getActiveSheet()->getStyle("C".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy"); 
	        }else{
	        	$objPHPExcel->getActiveSheet()->setCellValue("C".$i, "");
	        }	
	        $i++;
	    }


	    foreach(range('A','P') as $columnID) {
	        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
	            ->setAutoSize(true);
	    }
	   
		$newHoja = $objPHPExcel->createSheet(1);
    	$newHoja->setTitle("Comentarios");
    	$newHoja->getStyle('A1:AD1')
    	->getAlignment()
    	->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
   		$newHoja->getStyle('A2:D2')->getFont()->setBold( true );

	 	$newHoja->mergeCells('A1:D1');
	 	$newHoja->getStyle('A1:D1')->applyFromArray($borders);
		$newHoja->getStyle('A1:D1')->getFont()->setBold( true );
		$campos = "cecom_comentario_v, DATE_FORMAT(cecom_fecha_d,'%d/%m/%Y') as fechaComentari, CONCAT(user_nombre, ' ',user_apellidos) as usuario" ;
	    $tabla = "casos_especiales_comentarios JOIN gi_usuario ON user_id = cecom_usuario_i";
	    $where = "cecom_id_caes_i = ".$_GET['traerCasoEspecial'];

	    $respuestaComentari = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$where, null, 'ORDER BY cecom_fecha_d DESC');
	    
	    $newHoja->setCellValue("A1", "HISTORICO DE SEGUIMIENTO");
     	$newHoja->setCellValue("A2", "#");
        $newHoja->setCellValue("B2", "COMENTARIO");
        $newHoja->setCellValue("C2", "USUARIO");
        $newHoja->setCellValue("D2", "FECHA");
        $j = 3;
        foreach ($respuestaComentari as $key => $value) {
        	$newHoja->setCellValue("A".$j, ($key+1));
	        $newHoja->setCellValue("B".$j, $value['cecom_comentario_v']);
	        $newHoja->setCellValue("C".$j, $value['usuario']);
	        $newHoja->setCellValue("D".$j, $value['fechaComentari']);
	        $j++;
        }

        foreach(range('A','D') as $columnID) {
	        $newHoja->getColumnDimension($columnID)
	            ->setAutoSize(true);
	    }

        $objPHPExcel->setActiveSheetIndex(0);

    	/*************************NUEVA HOJA*************************/
       	// Write the Excel file to filename some_excel_file.xlsx in the current directory
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header('Content-Disposition: attachment;filename="reporte_casos_Especiales'.date('Y_m_d_H_i_s').'.xlsx"');
	    header('Cache-Control: max-age=0');
	    // If you're serving to IE 9, then the following may be needed
	    header('Cache-Control: max-age=1');

	    // If you're serving to IE over SSL, then the following may be needed
	    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	    header ('Pragma: public'); // HTTP/1.0

	    $writer->save('php://output');

	}
