<?php
	/** Error reporting */
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
 	$objPHPExcel = new Spreadsheet();
	

	// $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
	
	$objPHPExcel->setActiveSheetIndex(0);

	$objPHPExcel->getProperties()->setCreator("GEIN")
	                     ->setLastModifiedBy("".$_SESSION['nombres']);


	$objPHPExcel->getActiveSheet()
	    ->getStyle('A1:U1')
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$objPHPExcel->getActiveSheet()->getStyle('A1:X1')->getFont()->setBold( true );

	if(isset($_GET['soloCasos'])){
    	$objPHPExcel->getActiveSheet()->setTitle('Casos Especiales');
    }
    if(isset($_GET['soloIncapacitados'])){
    	$objPHPExcel->getActiveSheet()->setTitle('Incapacitados Permanentes');
    }
	


	$objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
	$objPHPExcel->getActiveSheet()->setCellValue("B1", "Identificacion"); 
	$objPHPExcel->getActiveSheet()->setCellValue("C1", "Apellidos y nombres"); 
	$objPHPExcel->getActiveSheet()->setCellValue("D1", "Cargo"); 
	$objPHPExcel->getActiveSheet()->setCellValue("E1", "Administradora"); 
	$objPHPExcel->getActiveSheet()->setCellValue("F1", "Dias Acum"); 

	$objPHPExcel->getActiveSheet()->setCellValue("G1", "Concepto de Rehabilitacion"); 
	$objPHPExcel->getActiveSheet()->setCellValue("H1", "Fecha Concepto");
	$objPHPExcel->getActiveSheet()->setCellValue("I1", "Diagnostico"); 
	$objPHPExcel->getActiveSheet()->setCellValue("J1", "Entidad");

	$objPHPExcel->getActiveSheet()->setCellValue("K1", "Calificacion de PCL"); 
	$objPHPExcel->getActiveSheet()->setCellValue("L1", "Fecha Calificacion");
	$objPHPExcel->getActiveSheet()->setCellValue("M1", "Diagnostico"); 
	$objPHPExcel->getActiveSheet()->setCellValue("N1", "Entidad");

	$objPHPExcel->getActiveSheet()->setCellValue("O1", "Calificacion Junta Regional"); 
	$objPHPExcel->getActiveSheet()->setCellValue("P1", "Fecha Calificacion");
	$objPHPExcel->getActiveSheet()->setCellValue("Q1", "Diagnostico"); 
	$objPHPExcel->getActiveSheet()->setCellValue("R1", "Entidad");

	$objPHPExcel->getActiveSheet()->setCellValue("S1", "Calificacion Junta Nacional"); 
	$objPHPExcel->getActiveSheet()->setCellValue("T1", "Fecha Calificacion");
	$objPHPExcel->getActiveSheet()->setCellValue("U1", "Diagnostico"); 
	$objPHPExcel->getActiveSheet()->setCellValue("V1", "Entidad");

	$objPHPExcel->getActiveSheet()->setCellValue("X1", "Empresa");


	$campos = "cae_id_i, cae_emd_id_i, cae_con_rea_v, cae_xon_fecha_d, cae_condia_id_i, b.ips_nombre as cae_con_entidad_id_i, cae_cal_v, cae_cal_fecha_d, cae_cal_dia_id_i, c.ips_nombre as cae_cal_entidad_i, cae_cal_jur_v, cae_cal_jur_fecha_d, cae_cal_jur_dia_id_i, cae_cal_jur_entidad_v, cae_cal_jun_v, cae_cal_jun_fecha_d, cae_cal_jun_dia_id_i, cae_cal_jun_entidad_v, emd_nombre, emd_cedula, emd_cargo, a.ips_nombre, emp_nombre";
    $tabla = "gi_casos_especiales JOIN gi_empleados ON emd_id = cae_emd_id_i JOIN gi_ips a ON emd_eps_id = a.ips_id JOIN gi_empresa ON emp_id = emd_emp_id LEFT JOIN gi_ips b ON cae_con_entidad_id_i = b.ips_id LEFT JOIN gi_ips c ON cae_cal_entidad_i = c.ips_id";
    $where = "";
    if(isset($_GET['soloCasos'])){
    	$where = " cae_tic_id_i IN (1, 3) ";
    }
    if(isset($_GET['soloIncapacitados'])){
    	$where = " cae_tic_id_i IN (3, 2) ";
    }
    if($_SESSION['cliente_id'] != 0){
        $where .= " AND emd_emp_id = ".$_SESSION['cliente_id'];
    }

    $reporte = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$where, null, 'ORDER BY emd_nombre ASC');
	
    $i = 2;
	foreach ($reporte as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue("A".$i, $i-1); 
		$objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value['emd_cedula']); 
		$objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value['emd_nombre']); 
		$objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value['emd_cargo']); 
		$objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value['ips_nombre']); 
		$objPHPExcel->getActiveSheet()->setCellValue("F".$i, ""); 

		$objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value['cae_con_rea_v']); 

		if($value['cae_xon_fecha_d'] != null && $value['cae_xon_fecha_d'] != ''){
			$fecha = explode('-', $value['cae_xon_fecha_d']);
			$objPHPExcel->getActiveSheet()->setCellValue("H".$i, $fecha[2]."/".$fecha[1]."/".$fecha[0]);	
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue("H".$i, '');
		}
		
		$objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value['cae_condia_id_i']); 
		$objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value['cae_con_entidad_id_i']);

		$objPHPExcel->getActiveSheet()->setCellValue("K".$i, $value['cae_cal_v']); 
		if($value['cae_cal_fecha_d'] != null && $value['cae_cal_fecha_d'] != ''){
			$fecha = explode('-', $value['cae_cal_fecha_d']);
			$objPHPExcel->getActiveSheet()->setCellValue("L".$i, $fecha[2]."/".$fecha[1]."/".$fecha[0]);
		
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue("L".$i, '');
		}
		$objPHPExcel->getActiveSheet()->setCellValue("M".$i, $value['cae_cal_dia_id_i']); 
		$objPHPExcel->getActiveSheet()->setCellValue("N".$i, $value['cae_cal_entidad_i']);

		$objPHPExcel->getActiveSheet()->setCellValue("O".$i, $value['cae_cal_jur_v']); 
		if($value['cae_cal_jur_fecha_d'] != null && $value['cae_cal_jur_fecha_d'] != ''){
			$fecha = explode('-', $value['cae_cal_jur_fecha_d']);
			$objPHPExcel->getActiveSheet()->setCellValue("P".$i, $fecha[2]."/".$fecha[1]."/".$fecha[0]);
		
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue("P".$i, '');
		}
		$objPHPExcel->getActiveSheet()->setCellValue("Q".$i, $value['cae_cal_jur_dia_id_i']); 
		$objPHPExcel->getActiveSheet()->setCellValue("R".$i, $value['cae_cal_jur_entidad_v']);

		$objPHPExcel->getActiveSheet()->setCellValue("S".$i, $value['cae_cal_jun_v']); 
		if($value['cae_cal_jun_fecha_d'] != null && $value['cae_cal_jun_fecha_d'] != ''){
			$fecha = explode('-', $value['cae_cal_jun_fecha_d']);
			$objPHPExcel->getActiveSheet()->setCellValue("T".$i, $fecha[2]."/".$fecha[1]."/".$fecha[0]);
		
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue("T".$i, '');
		}
		$objPHPExcel->getActiveSheet()->setCellValue("U".$i, $value['cae_cal_jun_dia_id_i']); 
		$objPHPExcel->getActiveSheet()->setCellValue("V".$i, $value['cae_cal_jun_entidad_v']);

		$objPHPExcel->getActiveSheet()->setCellValue("X".$i, $value['emp_nombre']); 
		$i++;
    }

    foreach(range('A','X') as $columnID) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
            ->setAutoSize(true);
    
    }


    // Write the Excel file to filename some_excel_file.xlsx in the current directory
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Casos_especiales_'.date('Y_m_d_H_i_s').'.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer = new Xlsx($objPHPExcel);
    $writer->save('php://output');

