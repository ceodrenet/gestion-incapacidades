<?php
    require_once __DIR__.'/../../../extenciones/TCPDF/tcpdf.php';

    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }

    class MYPDF extends TCPDF {

        protected $processId = 0;
        protected $header = '';
        protected $footer = '';
        static $errorMsg = '';
        //Page header
        public function Header() {
            $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
           $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

           $this->Line(5, 5, $this->getPageWidth()-5, 5); 

           $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
           $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
           $this->Line(5, 5, 5, $this->getPageHeight()-5);

        }

        // Page footer
        public function Footer() {
            // Position at 15 mm from bottom
            $this->SetY(-15);
            // Set font
            $this->SetFont('helvetica', 'N', 8);
            // Page number
            $this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }
    }


    /*empezar el proceso del PDF*/
    $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("DETALLE CASOS ESPECIALES");
    //$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    // set default header data

    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(true);
    $obj_pdf->setPrintFooter(true);
    $obj_pdf->SetAutoPageBreak(TRUE, 30);
    $obj_pdf->SetFont('times', '', 15);
    // set image scale factor

    $obj_pdf->AddPage();

if(isset($_GET['traerCasoEspecial'])){
        $campos = "cae_id_i, cae_emd_id_i, cae_con_rea_v, cae_xon_fecha_d, cae_condia_id_i, cae_con_entidad_id_i, cae_cal_v, cae_cal_fecha_d, cae_cal_dia_id_i, cae_cal_entidad_i, cae_cal_jur_v, cae_cal_jur_fecha_d, cae_cal_jur_dia_id_i, cae_cal_jur_entidad_v, cae_cal_jun_v, cae_cal_jun_fecha_d, cae_cal_jun_dia_id_i, cae_cal_jun_entidad_v, emd_nombre, emd_cedula, emd_cargo, a.ips_nombre as eps, b.ips_nombre as afp, emd_ciudad, emd_subcentro_costos , emd_centro_costos , emd_sede , c.ips_nombre as arl, emd_salario, emd_fecha_ingreso, emp_nombre " ;
        $tabla = "gi_casos_especiales JOIN gi_empleados ON emd_id = cae_emd_id_i JOIN gi_ips a ON emd_eps_id = a.ips_id LEFT JOIN gi_ips b ON emd_afp_id  = b.ips_id LEFT JOIN gi_ips c ON emd_arl_id  = c.ips_id JOIN gi_empresa ON emp_id = emd_emp_id";
        $where = "cae_id_i = ".$_GET['traerCasoEspecial'];
        
        $respuesta = ModeloTesoreria::mdlMostrarUnitario($campos,$tabla,$where);
        

        
    $content = '
    <br/>
    <br/>
    <table>
        <tr>
            <td width="80%">
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:20%"></td>
                        <td style="width:60%; text-align:center; font-size:12px;"><b>DETALLE DE CASOS ESPECIALES</b></td>
                        <td style="width:20%"></td>
                    </tr>
                </table>
                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
                    <tr>
                        <td style="width:30%"></td>
                        <td style="width:40%;text-align:center;">'.mb_strtoupper($respuesta['emd_nombre']).'</td>
                        <td style="width:30%"></td>
                    </tr>
                </table>
            </td>
            <td width="20%">
                <img src="vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
            </td>
        </tr>
    </table>
    
    <br/>
    <br/>
    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <td style="width:50%;text-align:justify;"><b>Incapacidades.co</b></td>
            <td style="width:25%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;"></td>
        </tr>
        <tr>
            <td style="width:50%;text-align:justify;"><b>'.$respuesta['emp_nombre'].'</b></td>
            <td style="width:25%;text-align:justify;"></td>
            <td style="width:25%;text-align:justify;">Fecha: '.date('d/m/Y H:i:s').'</td>
        </tr>
    </table>
    <br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <th style="text-align:center;">IDENTIFICACIÓN</th>
            <th style="text-align:center;">NOMBRES</th>
        </tr>
        <tr>
            <td id="tdIdent" style="text-align:center;">'.$respuesta['emd_cedula'].'</td>
            <td id="tdApell" style="text-align:center;">'.$respuesta['emd_nombre'].'</td>
        </tr>                            
    </table>
    <br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <th style="width: 33%;text-align:center;">CARGO</th>
            <th style="width: 33%;text-align:center;">FECHA DE INGRESO</th>
            <th style="width: 33%;text-align:center;">SALARIO</th>
        </tr>
        <tr>
            <td id="tdCargo" style="text-align:center;">'.$respuesta['emd_cargo'].'</td>
            <td id="tdApell" style="text-align:center;">'.$respuesta['emd_fecha_ingreso'].'</td>
            <td id="tdApell" style="text-align:center;">$ '.number_format($respuesta['emd_salario']).'</td>
        </tr>                            
    </table>
    <br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <th style="text-align:center;">EPS</th>
            <th style="text-align:center;">AFP</th>
            <th style="text-align:center;">ARL</th>
        </tr>
        <tr>
            
            <td id="tdEps" style="text-align:center;">'.$respuesta['eps'].'</td>
            <td id="tdAfp" style="text-align:center;">'.$respuesta['afp'].'</td>
            <td id="tdAfp" style="text-align:center;">'.$respuesta['arl'].'</td>
        </tr>                            
    </table>
    <br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <th style="text-align:center;">SEDE</th>
            <th style="text-align:center;">CENTRO DE COSTO</th>
            <th style="text-align:center;">CIUDAD</th>
            <th style="text-align:center;">SUBCENTRO DE COSTO</th>
        </tr>
        <tr>
            <td id="tdSede" style="text-align:center;">'.$respuesta['emd_sede'].'</td>
            <td id="tdCeCo" style="text-align:center;">'.$respuesta['emd_centro_costos'].'</td>
            <td id="tdCiud" style="text-align:center;">'.$respuesta['emd_ciudad'].'</td>
            <td id="tdSubC" style="text-align:center;">'.$respuesta['emd_subcentro_costos'].'</td>
        </tr>                            
    </table>
    <br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <th style="text-align:center;">CONCEPTO REHABILITACIÓN</th>
            <th style="text-align:center;">FECHA DE CONCEPTO</th>
            <th style="text-align:center;">DIAGNOSTICO</th>
            <th style="text-align:center;">ENTIDAD</th>
        </tr>
        <tr>
            <td id="tdCoRe" style="text-align:center;">'.$respuesta['cae_con_rea_v'].'</td>
            <td id="tdFeCo" style="text-align:center;">'.$respuesta['cae_xon_fecha_d'].'</td>
            <td id="tdDiCo" style="text-align:center;">'.$respuesta['cae_condia_id_i'].'</td>
            <td id="tdEnCo" style="text-align:center;">'.$respuesta['cae_con_entidad_id_i'].'</td>
        </tr>                            
    </table>
    <br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <th style="text-align:center;">CALIFICACIÓN DE PCL</th>
            <th style="text-align:center;">FECHA DE CALIFICACIÓN</th>
            <th style="text-align:center;">DIAGNOSTICO</th>
            <th style="text-align:center;">ENTIDAD</th>
        </tr>
        <tr>
            <td id="tdCalPCL" style="text-align:center;">'.$respuesta['cae_cal_v'].'</td>
            <td id="tdFecPCL" style="text-align:center;">'.$respuesta['cae_cal_fecha_d'].'</td>
            <td id="tdDiaPCL" style="text-align:center;">'.$respuesta['cae_cal_dia_id_i'].'</td>
            <td id="tdEntPCL" style="text-align:center;">'.$respuesta['cae_cal_entidad_i'].'</td>
        </tr>                            
    </table>
    <br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <th style="text-align:center;">CALIFICACIÓN JUNTA REGIONAL</th>
            <th style="text-align:center;">FECHA DE CALIFICACIÓN</th>
            <th style="text-align:center;">DIAGNOSTICO</th>
            <th style="text-align:center;">ENTIDAD</th>
        </tr>
        <tr>
            <td id="tdCalJuR" style="text-align:center;">'.$respuesta['cae_cal_jur_v'].'</td>
            <td id="tdFecJuR" style="text-align:center;">'.$respuesta['cae_cal_jur_fecha_d'].'</td>
            <td id="tdDiaJuR" style="text-align:center;">'.$respuesta['cae_cal_jur_dia_id_i'].'</td>
            <td id="tdEntJuR" style="text-align:center;">'.$respuesta['cae_cal_jur_entidad_v'].'</td>
        </tr>                            
    </table>
    <br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <th style="text-align:center;">CALIFICACIÓN JUNTA NACIONAL</th>
            <th style="text-align:center;">FECHA DE CALIFICACIÓN</th>
            <th style="text-align:center;">DIAGNOSTICO</th>
            <th style="text-align:center;">ENTIDAD</th>
        </tr>
        <tr>
            <td id="tdCalJuN" style="text-align:center;">'.$respuesta['cae_cal_jun_v'].'</td>
            <td id="tdFecJuN" style="text-align:center;">'.$respuesta['cae_cal_jun_fecha_d'].'</td>
            <td id="tdDiaJuN" style="text-align:center;">'.$respuesta['cae_cal_jun_dia_id_i'].'</td>
            <td id="tdEntJuN" style="text-align:center;">'.$respuesta['cae_cal_jun_entidad_v'].'</td>
        </tr>                            
    </table>

    <br/>
    <br/>
    <br/>
    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <th style="text-align:center;"></th>
            <th style="text-align:center;">HISTORICO DE INCAPACIDADES</th>
            <th style="text-align:center;"></th>
        </tr>                        
    </table>
    <br/>
    <br/>
    <table border="1" style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <th style="text-align:center;">FECHA INICIO</th>
            <th style="text-align:center;">FECHA FINAL</th>
            <th style="text-align:center;">DÍAS</th>
            <th style="text-align:center;">DIAGNOSTICO</th>
            <th style="text-align:center;">ESTADO</th>
            <th style="text-align:center;">VALOR</th>
            <th style="text-align:center;">FECHA PAGO</th>
        </tr>
        ';

    $campos = " inc_diagnostico , inc_fecha_inicio, inc_fecha_final, inc_estado_tramite,    inc_valor , inc_fecha_pago" ;
    $tabla = "gi_incapacidad";
    $where = "inc_emd_id = ".$respuesta['cae_emd_id_i'];
    
    $respuestaX = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$where, null, 'ORDER BY inc_fecha_inicio DESC');

    $valorMax = 0;
    foreach ($respuestaX as $key => $value) {
        $fecha1 = explode(' ', $value['inc_fecha_inicio'])[0];
        $fecha1 = explode('-', $fecha1);
        $fecha1 = $fecha1[2]."/".$fecha1[1]."/".$fecha1[0];

        $fecha2 = explode(' ', $value['inc_fecha_final'])[0];
        $fecha2 = explode('-', $fecha2);
        $fecha2 = $fecha2[2]."/".$fecha2[1]."/".$fecha2[0];

        $fecha3 = null;
        if($value['inc_fecha_pago'] != null){
            $fecha3 = explode(' ', $value['inc_fecha_pago'])[0];
            $fecha3 = explode('-', $fecha3);
            $fecha3 = $fecha3[2]."/".$fecha3[1]."/".$fecha3[0];
        }
        $valorMax += $value['inc_valor'];
        $content .= "<tr>";
        $content .= "<td style=\"text-align:center;\" >".$fecha1."</td>";
        $content .= "<td style=\"text-align:center;\" >".$fecha2."</td>";
        $content .= "<td style=\"text-align:center;\" >".dias_transcurridos($value["inc_fecha_inicio"] , $value["inc_fecha_final"])."</td>";
        $content .= "<td style=\"text-align:center;\" >".$value['inc_diagnostico']."</td>";
        $content .= "<td style=\"text-align:center;\" >".$value['inc_estado_tramite']."</td>";
        if($value['inc_valor'] != null){
            $content .= "<td style=\"text-align:center;\" >$ ".number_format($value['inc_valor'], 0)."</td>";    
        }else{
            $content .= "<td style=\"text-align:center;\" >0</td>";
        }
        $content .= "<td style=\"text-align:center;\" >".$fecha3."</td>";
        $content .= "</tr>";
    }

    $content .= "<tr>";
    $content .= "<td></td>";
    $content .= "<td></td>";
    $content .= "<td></td>";
    $content .= "<td></td>";
    $content .= "<td style='text-align:right'><b>Total</b></td>";
    if($valorMax != 0){
        $content .= "<td>$ ".number_format($valorMax, 0)."</td>";    
    }else{
        $content .= "<td>$ 0</td>";
    }
    
    $content .= "<td></td>";
    $content .= "</tr>";

   

    $content .='</table>
    <br/>
    <br/>
    <table style="font-family:tahoma;font-size:10;" width="100%" cellpadding="1px" cellspacing="0px" width="100%">
        <tr>
            <th style="text-align:center;"></th>
            <th style="text-align:center;">SEGUIMIENTO HISTORICO</th>
            <th style="text-align:center;"></th>
        </tr>                        
    </table>
    <table border="1" style="font-family:tahoma;font-size:10;" cellpadding="1px" cellspacing="0px" width="100%">';

    $campos = "cecom_comentario_v, DATE_FORMAT(cecom_fecha_d,'%d/%m/%Y') as fechaComentari, CONCAT(user_nombre, ' ',user_apellidos) as usuario" ;
    $tabla = "casos_especiales_comentarios JOIN gi_usuario ON user_id = cecom_usuario_i";
    $where = "cecom_id_caes_i = ".$_GET['traerCasoEspecial'];

    $respuestaComentari = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$where, null, 'ORDER BY cecom_fecha_d DESC');
        $content .= "<tr>";
            $content .= '<td style="text-align:center;" width="70%"><b>Comentario</b></td>';
            $content .= '<td style="text-align:center;" width="15%"><b>Usuario</b></td>';
            $content .= '<td style="text-align:center;" width="15%"><b>Fecha</b></td>';
        $content .= "</tr>";   
    foreach ($respuestaComentari as $key => $value) {
        $content .= "<tr>";
            $content .= '<td style="text-align:justify;" width="70%">'.$value['cecom_comentario_v'].'</td>';
            $content .= '<td style="text-align:center;" width="15%">'.$value['usuario'].'</td>';
            $content .= '<td style="text-align:center;" width="15%">'.$value['fechaComentari'].'</td>';
        $content .= "</tr>";
    }
    $content .='</table>';

   // echo $content;
    $obj_pdf->writeHTML($content, true, false, false, false, '');

    $nombre="Detalles_Casos_Especiales_".$respuesta['emd_cedula']."_".date('H:i:s').'.pdf';
    //header('Content-type: application/pdf');
    $obj_pdf->Output($nombre, 'I');

    
}