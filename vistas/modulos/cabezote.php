<style type="text/css">
	.jose{
		width: 95%;
		margin: 0px auto;
	}

	@media (max-width: 400px) {
	  	.jose{
			width: 70%;
			margin: 0px auto;
		}
	}

	@media (max-width: 600px) {
	  	.jose{
			width: 70%;
			margin: 0px auto;
		}
	}
</style>
<?php
    function getDataAlerta($alerta, $diasProcesar){
        $numeroAlertas = 0;
        foreach ($alerta as $key => $value) {
                                 
            $camposX = "inc_fecha_inicio, inc_fecha_final";
            $tablasX = "gi_incapacidad";
            $whereXX = "inc_cc_afiliado = ".$value['inc_cc_afiliado']." AND inc_empresa = ".$_SESSION['cliente_id'];
            $verificar = ModeloTesoreria::mdlMostrarGroupAndOrder($camposX, $tablasX, $whereXX, '', 'ORDER BY inc_fecha_inicio ASC');
            $valida = false;
            $dias_transcurridos = 0;
            $inc_fecha_inicio_Final = ''; 
            foreach ($verificar as $incapacidades => $incapacidad) {
                if($inc_fecha_inicio_Final != ''){
                    //tebemos que validar que la fecha inicio sea mayor a 30 dias de la final pasada
                    $dias = 0;
                    $dias = ControladorIncapacidades::dias_transcurridos( $inc_fecha_inicio_Final, $incapacidad['inc_fecha_inicio']);
                    if($dias <= 30){
                        $valida = true;
                        $dias_transcurridos += $dias;
                    }
                }
                $inc_fecha_inicio_Final = $incapacidad['inc_fecha_final'];
            }
            if($dias_transcurridos >= $diasProcesar){
                $numeroAlertas++;
            }
        }
            
        $sinS = 'Incapacidad';
        if($numeroAlertas > 1){
            $sinS = 'Incapacidades';
        }
        if($numeroAlertas > 0){
            $amostrar = '  <li>
                            <a href="'.$diasProcesar.'dias">
                                <i class="fa fa-warning text-yellow"></i> '.$numeroAlertas.' '.$sinS.' de mas '.$diasProcesar.' días
                            </a>
                        </li>';
            return $amostrar;    
        }else{
            return '';
        }
        
    }
	$campos = 'inc_cc_afiliado';
    $tablas = 'reporte_alarmas';

    $condiciones  = 'Tiempo_dias > 90 AND Tiempo_dias < 120';
    $condiciones1 = 'Tiempo_dias > 120 AND Tiempo_dias < 180';
    $condiciones2 = 'Tiempo_dias > 180 AND Tiempo_dias < 540';
    $condiciones3 = 'Tiempo_dias > 540';

    if($_SESSION['cliente_id'] != 0){
    	$condiciones .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
    	$condiciones1 .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
    	$condiciones2 .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
    	$condiciones3 .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
    }
    $alarma1 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones, '', '');
    $alarma2 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones1, '', '');
    $alarma3 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones2, '', '');
    $alarma4 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones3, '', '');
  
    $numeroAlarmas = 0;
    $amostrar = '';
    if($alarma1 != null && $alarma1 != false){
        $alertasString = getDataAlerta($alarma1, 90);
        if($alertasString != ''){
            $numeroAlarmas++;
            $amostrar .= $alertasString;
        }
    	
    }

    if($alarma2 != null && $alarma2 != false){
    	$alertasString = getDataAlerta($alarma2, 120);
        if($alertasString != ''){
            $numeroAlarmas++;
            $amostrar .= $alertasString;
        }
    }
    if($alarma3 != null && $alarma3 != null){
        $alertasString = getDataAlerta($alarma3, 180);
        if($alertasString != ''){
            $numeroAlarmas++;
            $amostrar .= $alertasString;
        }
    }
    if($alarma4 != null && $alarma4 != false){
    	$alertasString = getDataAlerta($alarma4, 540);
        if($alertasString != ''){
            $numeroAlarmas++;
            $amostrar .= $alertasString;
        }
    }

    $campos = 'count(*) as total';
    $tablas = 'gi_empresa_usuarios';

    $condiciones  = 'emu_id_usuario = '.$_SESSION['id_Usuario'];

    $empresas = ModeloTesoreria::mdlMostrarUnitario($campos, $tablas, $condiciones);

?>
<header class="main-header">
	<!-- Logo -->
    <a href="inicio" class="logo" style="background: white;">
      	<!-- mini logo for sidebar mini 50x50 pixels -->
     	<!--<span class="logo-mini logoJ">
      		<span>GI</span>
      	</span>
       logo for regular state and mobile devices -->
      	<!--<span class="logo-lg logoJ" style="font-size: 18px;">
      		Gestion Incapacidades
      	</span>-->
      	<span class="logo-mini">
      		<img src="vistas/img/plantilla/LCortado.png" class="img-responsive" style="padding: 10px;">	
      	</span>
      	<!-- logo for regular state and mobile devices -->
      	<span class="logo-lg">
      		<img src="vistas/img/plantilla/LOGO_1.png" class="img-responsive jose" >	
      	</span>
    </a>
    <!-- BARRA DE NAVEGACION -->
    <nav class="navbar navbar-static-top">
		<!-- Boton de Navegacion -->
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>

		<!--<form class="navbar-form navbar-left" role="search" style="margin-top: 5px;">
          	<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="true" style="border: none; font-size: 100%; padding: 0px; width: 160px; position: absolute; left: 55%;" id="SelectorProyecto"></button>
          	<ul class="dropdown-menu" style="text-align: center; left: 65%; border-color: darkgrey;">
	          	
          	</ul>
    	</form>-->

		<!-- Perfil de usuario -->
		<div class="navbar-custom-menu">

			<ul class="nav navbar-nav">
				<!-- Tasks: style can be found in dropdown.less -->
				<li class="dropdown notifications-menu" id="micalendario">
					<a href="calendario">
						<i class="fa fa-calendar"></i>
						<span class="label label-warning"></span>
					</a>
				</li>
			</ul>

			<ul class="nav navbar-nav">
				<li class="dropdown notifications-menu" id="misTareas">
					<a href="actividades" title="Registro de Actividades">
						<i class="fa fa-check-square"></i>
						<span class="label label-warning"></span>
					</a>
				</li>
			</ul>	
			
			<ul class="nav navbar-nav">
				<!-- Tasks: style can be found in dropdown.less -->
				<li class="dropdown notifications-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-bell"></i>
						<span class="label label-warning"><?php echo $numeroAlarmas;?></span>
					</a>
					<ul class="dropdown-menu">
						<?php if($numeroAlarmas > 0) { ?>
						<li class="header">Tienes <?php echo $numeroAlarmas;?> Alertas</li>
						<?php }else { ?>
						<li class="header">No tienes alertas</li>
						<?php } ?>
						<li>
							<!-- inner menu: contains the actual data -->
							<ul class="menu">
								<?php
									echo $amostrar;
								?>
							</ul>
						</li>					
					</ul>
				</li>
			</ul>

			<ul class="nav navbar-nav">
				<li class="user user-menu">
					<a href="#">
						<?php
							$imagen = 'vistas/img/usuarios/default/anonymous.png';
							if($_SESSION['imagen'] != null && $_SESSION['imagen'] != ''){
								$imagen = $_SESSION['imagen'];
							}
						?>
						<img src="<?php echo $imagen; ?>" class="user-image">
						<span class="hidden-xs">
							<?php 
								if(isset($_SESSION['nombres'])){
									echo $_SESSION['nombres'];
								}
							?>		
						</span>
					</a>
				</li>
			</ul>
			<?php
				if($empresas['total'] > 1 || $_SESSION['perfilN'] == 'SuperAdministrador'){ ?>
			<!-- Dropdown Toggle -->
			<ul class="nav navbar-nav">
				<li class="notifications-menu">
					<a title="Cambiar Empresa" id="ChangeEmpresa">
						<i class="glyphicon glyphicon-refresh"></i>
					</a>
				</li>
			</ul>

			<?php } ?>
			<ul class="nav navbar-nav">
				<li class="notifications-menu">
					<a title="Cambiar Contraseña" id="ChangePassWord">
						<i class="glyphicon glyphicon-lock"></i>
					</a>
				</li>
			</ul>

			<ul class="nav navbar-nav">
				<li class="notifications-menu">
					<a title="Salir de GEIN" id="LogOut" href="salir">
						<i class="glyphicon glyphicon-off"></i>
					</a>
				</li>
			</ul>

		</div>
		
    </nav>
</header>

<!-- Cambiar password -->
<div id="ModalCambiarPassWord" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" id="title_opciones_avanzadas" style="background: #d73925;color: white;">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="nombre_campo_h4">Cambiar Contraseña</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Contraseña Actual</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </span>
                        <input class="form-control" type="password" placeholder="Contraseña Actual" id="PassWordActual">
                    </div>
                </div>
                <div class="form-group">
                    <label>Contraseña Nueva</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-unlock-alt"></i>
                        </span>
                        <input class="form-control" type="password" placeholder="Contraseña Nueva" id="PassWordNuevo">
                    </div>
                </div>
                <div class="form-group">
                    <label>Confirmar Contraseña</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-unlock-alt"></i>
                        </span>
                        <input class="form-control" type="password" placeholder="Confirmar Contraseña" id="PassWordConfirmar">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                <button type="submit" class="btn btn-danger" id="SavePassword">Guardar</button>
            </div>
        </div>
      <!-- /.modal-content -->
    </div>
</div>
<!-- Cambiar empresa -->
<div id="ModalCambiarEmpresa" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" id="title_opciones_avanzadas" style="background: #d73925;color: white;">
              	<button type="button" class="close" data-dismiss="modal">&times;</button>
             	<h4 class="modal-title" id="nombre_campo_h4">Cambiar de empresa</h4>
            </div>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-md-12 col-xs-12">
            			<div class="form-group">
            				<select class="form-control proyectos" style="width: 100%;" id="selEmpresas">
            					<?php
					    			$campos = 'emp_id, emp_nombre';
					                $tablas = 'gi_empresa';
					                if($_SESSION['perfil'] == 7){
					                    $condiciones = '';
				                     	echo '<option class="proyectos" idproyecto="0" id_session="'.session_id().'">TODAS</option>';
					                }else{
					                    $condiciones = 'emp_id IN (SELECT emu_id_empresa FROM gi_empresa_usuarios WHERE emu_id_usuario = '.$_SESSION['id_Usuario'].')';
					                }
	                				$proyectos = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones, '', 'ORDER BY emp_nombre ASC');
	                				$activo = 1;
	               
					                foreach ($proyectos as $key => $value) {
					                	if($_SESSION['cliente_id'] == $value['emp_id']){
					                		echo '<option selected class="proyectos" idproyecto="'.$value["emp_id"].'" id_session="'.session_id().'">'.$value["emp_nombre"].'</option>';
					                	}else{
					                		echo '<option class="proyectos" idproyecto="'.$value["emp_id"].'" id_session="'.session_id().'">'.$value["emp_nombre"].'</option>';
					                	}
					                    
				                	}
				        		?>
            				</select>
            			</div>
            		</div>
            	</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
            </div>
        </div>
      <!-- /.modal-content -->
    </div>
</div>

<!-- registrar Actividades -->
<div id="ModalRegistroActividad" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" id="title_opciones_avanzadas" style="background: #d73925;color: white;">
              	<button type="button" class="close" data-dismiss="modal">&times;</button>
             	<h4 class="modal-title" id="nombre_campo_h4">Registro de Actividades Diario</h4>
            </div>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-md-6 col-xs-12">
            			<div class="form-group">
            				<label>Actividad Realizada</label>
            				<input type="text" name="txtActividadRealizada" required class="form-control" placeholder="Actividad Realizada">
            			</div>
            		</div>
            		<div class="col-md-3 col-xs-6">
            			<div class="form-group">
            				<label>Hora Inicio</label>
            				<select class="form-control" required name="cmbHoraInicio" id="cmbHoraInicio">
            					<option value="7:00">7:00 AM</option>
            					<option value="8:00">8:00 AM</option>
            					<option value="9:00">9:00 AM</option>
            					<option value="10:00">10:00 AM</option>
            					<option value="11:00">11:00 AM</option>
            					<option value="12:00">12:00 PM</option>
            					<option value="13:00">1:00 PM</option>
            					<option value="14:00">2:00 PM</option>
            					<option value="15:00">3:00 PM</option>
            					<option value="16:00">4:00 PM</option>
            					<option value="17:00">5:00 PM</option>
            					<option value="18:00">6:00 PM</option>
            					<option value="19:00">7:00 PM</option>
            				</select>
            			</div>
            		</div>
            		<div class="col-md-3 col-xs-6">
            			<div class="form-group">
            				<label>Hora Final</label>
            				<select class="form-control" required name="cmbHoraFinal" id="cmbHoraFinal">
            					<option value="8:00">8:00 AM</option>
            					<option value="9:00">9:00 AM</option>
            					<option value="10:00">10:00 AM</option>
            					<option value="11:00">11:00 AM</option>
            					<option value="12:00">12:00 PM</option>
            					<option value="13:00">1:00 PM</option>
            					<option value="14:00">2:00 PM</option>
            					<option value="15:00">3:00 PM</option>
            					<option value="16:00">4:00 PM</option>
            					<option value="17:00">5:00 PM</option>
            					<option value="18:00">6:00 PM</option>
            					<option value="19:00">7:00 PM</option>
            					<option value="20:00">8:00 PM</option>
            				</select>
            			</div>
            		</div>
            	</div>

            	<div class="row">	
            		<div class="col-md-12 col-xs-12">
            			<div class="form-group">
            				<label>Observación</label>
            				<textarea class="form-control" name="txtObservacion" id="txtObservacion" placeholder="Observación"></textarea>
            			</div>
            		</div>
            	</div>

            	<div class="row">
            		<div class="col-md-6 col-xs-12">
            			<div class="form-group">
            				<label>Empresa</label>
            				<select class="form-control proyectos" style="width: 100%;" id="selEmpresasActividad" name="selEmpresasActividad">
            					<?php
					    			$campos = 'emp_id, emp_nombre';
					                $tablas = 'gi_empresa';
					                if($_SESSION['perfil'] == 7){
					                    $condiciones = '';
				                     	echo '<option class="proyectos" idproyecto="0" id_session="'.session_id().'">TODAS</option>';
					                }else{
					                    $condiciones = 'emp_id IN (SELECT emu_id_empresa FROM gi_empresa_usuarios WHERE emu_id_usuario = '.$_SESSION['id_Usuario'].')';
					                }
	                				$proyectos = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones, '', 'ORDER BY emp_nombre ASC');
	                				$activo = 1;
	               
					                foreach ($proyectos as $key => $value) {
					                	if($_SESSION['cliente_id'] == $value['emp_id']){
					                		echo '<option selected class="proyectos" idproyecto="'.$value["emp_id"].'" id_session="'.session_id().'">'.$value["emp_nombre"].'</option>';
					                	}else{
					                		echo '<option class="proyectos" idproyecto="'.$value["emp_id"].'" id_session="'.session_id().'">'.$value["emp_nombre"].'</option>';
					                	}
					                    
				                	}
				        		?>
            				</select>
            			</div>
            		</div>
            		<div class="col-md-3 col-xs-12">
            			<div class="form-group">
            				<label>Estado</label>
            				<div class="radio">
            					<label>
            						<input type="radio" required name="radEstado" id="radEstadoTerminado" checked="true"> Terminado
            					</label>
            				</div>
            			</div>
            		</div>
            		<div class="col-md-3 col-xs-12">
            			<div class="form-group">
            				<label style="visibility: hidden;">Estado</label>
            				<div class="radio">
            					<label>
            						<input type="radio" required name="radEstado" id="radEstadoAgendado"> Agendado
            					</label>
            				</div>
            			</div>
            		</div>
            	</div>
            </div>
            <div class="modal-footer">
            	<input type="hidden" name="fechaInicio" id="fechaInicioActividad">
            	<input type="hidden" name="fechaFinal" id="fechaFinalActividad">
                <button type="submit" class="btn btn-danger pull-right" data-dismiss="modal">Guardar</button>
            </div>
        </div>
      <!-- /.modal-content -->
    </div>
</div>