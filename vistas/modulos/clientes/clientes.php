<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <!-- Content Header (Page header) -->
    <div class="col">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-sm-0 font-size-18">CLIENTES</h1>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Clientes</li>
                </ol>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <div class="row">
        <div class="col-12">
            <div class="card border border-danger">
                <div class="card-header bg-transparent border-danger">
                    <h5 class="my-0 text-danger">
                        <div class="row">
                            <div class="col-lg-11">
                                ADMINISTRAR CLIENTES
                            </div>
                            <div class="col-lg-1">
                                <div class="btn-group dropstart" role="group">
                                    <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        ACCIONES <i class="mdi mdi-chevron-left"></i>
                                    </button>
                                    <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">
                                    <?php if($_SESSION['adiciona'] == 1 && $_SESSION['cliente_id'] == 0){ ?>
                                        <a class="dropdown-item" title="agregar cliente" data-bs-toggle="modal" data-bs-target="#modalAgregarCliente" href="#">
                                            AGREGAR CLIENTE
                                        </a>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </h5>
                </div>
                <div class="card-body">
                    <table style="width: 100%;" id="tablaClientes" class="table table-bordered table-striped dt-responsive tablas">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th style="width: 10%;">Nit</th>
                                <th>Dirección</th>
                                <th>Teléfono</th>
                                <th>Estado</th>
                                <th style="width: 20%;">Documentos</th>
                                <th style="width: 20%;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            if($_SESSION['cliente_id'] != 0){
                                $item = 'emp_id';
                                $valor = $_SESSION['cliente_id'];
                                $clientes = ControladorClientes::ctrMostrarClientes_Sessions($item, $valor);
                            }else{
                                $item = null;
                                $valor = null;
                                $clientes = ControladorClientes::ctrMostrarClientes($item, $valor);
                            }
                           
                            foreach ($clientes as $key => $value) {
                                echo ' 
                                <tr>
                                    <td class="text-uppercase">'.$value["emp_nombre"].'</td>
                                    <td class="text-uppercase">'.$value["emp_nit"].'</td>
                                    <td class="text-uppercase">'.$value["emp_direccion"].'</td>
                                    <td class="text-uppercase">'.$value["emp_telefono"].'</td>';

                                if($value['emp_estado'] == 1){
                                    echo '
                                        <td>
                                            <button class="btn btn-success btn-xs btnActivar " id_cliente="'.$value['emp_id'].'" estado="0">
                                                    Activado
                                            </button>
                                        </td>';
                                }else{
                                    echo '
                                        <td>
                                            <button class="btn btn-danger btn-xs btnActivar" id_cliente="'.$value['emp_id'].'" estado="1">
                                                Desactivado
                                            </button>
                                        </td>';
                                }
                                /* Esto es para diferenciar que es una imagen o un PDF */
                                echo "<td style='text-align:center;'>";
                                if($value['emp_ruta_camara_comercio'] != ''){
                                    $tipoArchivo  = explode('.', $value['emp_ruta_camara_comercio'])[1];
                                    
                                    if($tipoArchivo == 'pdf'){
                                        echo "<a href=\"".$value['emp_ruta_camara_comercio']."\" class='josePdf' target='_blank'> <img src=\"vistas/img/plantilla/pdf.png\" class=\"img-thumbnail\" title='Empresa Camara de comercio' width=\"40px\"></a>";
                                    }else{
                                        echo "<a target='_blank' href=\"".$value['emp_ruta_camara_comercio']."\" data-lightbox=\"roadtrip\" download=\"Empresa Camara de comercio\"><img src=\"".$value['emp_ruta_camara_comercio']."\" title='Empresa Camara de comercio' class=\"img-thumbnail\" width=\"40px\"></a>";
                                    }
                                }

                                if($value['emp_ruta_cedula_representante'] != '' && $_SESSION['perfilN'] == 'SuperAdministrador' || $_SESSION['perfilN'] == 'Administrador'){
                                    $tipoArchivo  = explode('.', $value['emp_ruta_cedula_representante'])[1];
                                    
                                    if($tipoArchivo == 'pdf'){
                                        echo "<a href=\"".$value['emp_ruta_cedula_representante']."\" class='josePdf' target='_blank'> <img src=\"vistas/img/plantilla/pdf.png\" class=\"img-thumbnail\" title='Empresa Cedula Representante' width=\"40px\"></a>";
                                    }else{
                                        echo "<a target='_blank' href=\"".$value['emp_ruta_cedula_representante']."\" data-lightbox=\"roadtrip\" download=\"Empresa Cedula Representante\" ><img src=\"".$value['emp_ruta_cedula_representante']."\" title='Empresa Cedula Representante' class=\"img-thumbnail\" width=\"40px\"></a>";
                                    }
                                }

                                if($value['emp_ruta_cuenta_bancaria'] != '' && $_SESSION['perfilN'] == 'SuperAdministrador' || $_SESSION['perfilN'] == 'Administrador' ){
                                    $tipoArchivo  = explode('.', $value['emp_ruta_cuenta_bancaria'])[1];
                                    
                                    if($tipoArchivo == 'pdf'){
                                        echo "<a href=\"".$value['emp_ruta_cuenta_bancaria']."\" class='josePdf' target='_blank'> <img src=\"vistas/img/plantilla/pdf.png\" class=\"img-thumbnail\" title='Empresa Cuenta Bancaria' width=\"40px\"></a>";
                                    }else{
                                        echo "<a target='_blank' href=\"".$value['emp_ruta_cuenta_bancaria']."\" data-lightbox=\"roadtrip\" download=\"Empresa Cuenta Bancaria\"><img src=\"".$value['emp_ruta_cuenta_bancaria']."\" title='Empresa Cuenta Bancaria' class=\"img-thumbnail\" width=\"40px\"></a>";
                                    }
                                }


                                if($value['emp_ruta_rut'] != ''){
                                    $tipoArchivo  = explode('.', $value['emp_ruta_rut'])[1];
                                    
                                    if($tipoArchivo == 'pdf'){
                                        echo "<a  href=\"".$value['emp_ruta_rut']."\" class='josePdf' target='_blank'> <img src=\"vistas/img/plantilla/pdf.png\" title='Empresa Rut' class=\"img-thumbnail\" width=\"40px\"></a>";
                                    }else{
                                        echo "<a target='_blank' href=\"".$value['emp_ruta_rut']."\" data-lightbox=\"roadtrip\" download=\"Empresa Rut\"><img src=\"".$value['emp_ruta_rut']."\" title='Empresa Rut' class=\"img-thumbnail\" width=\"40px\"></a>";
                                    }
                                }
                                

                                echo "</td>";

                                echo '
                                    <td style="text-align:center;">';
                                echo '<div class="btn-group dropstart" role="group">';
                                echo '<button id="btnGroupVerticalDrop1" type="button"  class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                                echo '<i class="fa fa-info-circle"></i>';
                                echo '</button>';
                                echo '<ul class="dropdown-menu" role="menu">';
                                    echo '<input type="hidden" name="clientId" id="clientId" value="'.$value["emp_id"].'"/>';
                                    if($_SESSION['edita'] == 1){
                                        echo '<li><a title="Editar cliente" class="dropdown-item btnEditarCliente" idCliente="'.$value["emp_id"].'" href="#">EDITAR</a></li>';

                                        //
                                    }

                                    if($_SESSION['elimina'] == 1){
                                        echo '<li><a title="Eliminar Cliente" class="dropdown-item btnEliminarCliente" idCliente="'.$value["emp_id"].'" href="#">ELIMINAR</a></li>';

                                    }                               
                                if(isset($_SESSION['cliente_id']) && $_SESSION['cliente_id'] == 0){
                                    echo '
                                        <li><a title="Ver colaboradores registrados" class="dropdown-item btnVerEmpleados" idCliente="'.$value["emp_id"].'" href="#">VER COLABORADORES</a></li>';
                                }
                                echo '</ul>';
                                echo '</div>';
                                echo '    
                                    </td>
                                </tr>';                            
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Modal agregar cliente -->
<div id="modalAgregarCliente" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <form id="frmRegistroCliente" role="form" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Cliente</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Ingresar Nombre</label>
                                    <div class="input-group">
                                        <input class="form-control input-lg" type="text" name="NuevoNombre" placeholder="Ingresar Nombre" required="true">
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12">
                                        <div class="form-group">
                                            <label class="form-label">Ingresar Identificación / Nit</label>
                                            <div class="input-group">
                                                <input class="form-control input-lg" type="text" name="NuevoNit" id="NuevoNit" maxlength="9" placeholder="Ingresar Identificación / Nit" required="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <label>Verificación</label>
                                            <div class="input-group">
                                                <input class="form-control" type="text" name="NuevoNitVerificacion" maxlength="1" id="NuevoNitVerificacion" placeholder="Verificación">
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Ingresar Dirección</label>
                                    <div class="input-group">
                                        <input class="form-control input-lg" type="text" name="NuevoDireccion" id="NuevoDireccion" placeholder="Ingresar Dirección" >
                                    </div> 
                                </div>       
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Ingresar Teléfono</label>
                                    <div class="input-group">
                                        <input class="form-control " type="text" name="NuevoTelefono" id="NuevoTelefono" placeholder="Ingresar Teléfono" >
                                    </div>
                                </div>      
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Ingresar Email</label>
                                    <div class="input-group">
                                        <input class="form-control input-g" type="text" name="NuevoEmail" id="NuevoEmail" placeholder="Ingresar Email" >
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Fecha Pago seguridad Social</label>
                                    <div class="input-group">
                                        <input class="form-control flatpickr-input flatpickr-format" type="text" name="NuevoFechaPagoSeguridad" id="NuevoFechaPagoSeguridad" placeholder="Fecha Pago seguridad Social" >
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row panel" >
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Nombre del Representante Legal</label>
                                    <div class="input-group">
                                        <input class="form-control input-lg" type="text" name="NuevoRepresentante" id="NuevoRepresentante" placeholder="Nombre del Representante Legal" >
                                    </div>
                                </div>       
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Ingresar C.C Representante Legal</label>
                                    <div class="input-group">
                                        <input class="form-control input-lg" type="text" name="NuevoCCRepresentante" id="NuevoCCRepresentante" placeholder="Ingresar C.C Representante Legal" >
                                    </div>
                                </div>     
                            </div>
                        </div>
                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Ingresar Contacto Gestión</label>
                                    <div class="input-group">
                                        <input class="form-control input-lg" type="text" name="NuevoContacto" id="NuevoContacto" placeholder="Ingresar Contacto Gestión">
                                    </div>
                                </div>     
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Email Contacto Gestión</label>
                                    <div class="input-group">
                                        <input class="form-control input-lg" type="text" name="NuevoEmailGestion" id="NuevoEmailGestion" placeholder="Email Contacto Gestión" >
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="form-label">Teléfono Fijo Contacto Gestión</label>
                                            <div class="input-group">
                                                <input class="form-control input-lg" type="text" name="NuevoTelefonoGestion" id="NuevoTelefonoGestion" placeholder="Teléfono Fijo Contacto Gestión" >
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="form-label">Ext.</label>
                                            <div class="input-group">
                                                <input class="form-control input-lg" type="text" name="NuevoTelefonoGestionExt" id="NuevoTelefonoGestionExt" placeholder="Ext." >
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Celular Contacto Gestión</label>
                                    <div class="input-group">
                                        <input class="form-control input-lg" type="text" name="NuevoCelular" id="NuevoCelular" placeholder="Celular Contacto Gestión" >
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Ingresar Contacto Tesoreria</label>
                                    <div class="input-group">
                                        <input class="form-control input-lg" type="text" name="NuevoContactoTesoreria" id="NuevoContactoTesoreria" placeholder="Ingresar Contacto Tesoreria">
                                    </div>
                                </div>     
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Email Contacto Tesoreria</label>
                                    <div class="input-group">
                                        <input class="form-control input-lg" type="text" name="NuevoEmailTesoreria" id="NuevoEmailTesoreria" placeholder="Email Contacto Tesoreria" >
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="form-label">Teléfono Fijo Contacto Tesoreria</label>
                                            <div class="input-group">
                                                <input class="form-control input-lg" type="text" name="NuevoTelefonoTesoreria" id="NuevoTelefonoTesoreria" placeholder="Teléfono Fijo Contacto Tesoreria" >
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="form-label">Ext.</label>
                                            <div class="input-group">
                                                <input class="form-control input-lg" type="text" name="NuevoTelefonoTesoreriaExt" id="NuevoTelefonoTesoreriaExt" placeholder="Ext." >
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Celular Contacto Tesoreria</label>
                                    <div class="input-group">
                                        <input class="form-control input-lg" type="text" name="NuevoCelularTesoreria" id="NuevoCelularTesoreria" placeholder="Celular Contacto Tesoreria" >
                                    </div>
                                </div>        
                            </div>

                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label class="form-label">
                                        <input type="checkbox" name="NuevoLiquidacionAutomatica" value="1"> Liquidación Automatica
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">% Ganancia Incapacidades.co</label>
                                    <div class="input-group">
                                        <input class="form-control input-lg" type="text" name="NuevoPorcentajeGananciaInc" id="NuevoPorcentajeGananciaInc" placeholder="% Ganancia Incapacidades.co" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>     
                        <!-- Nuevos campos -->                                                    
                        <div class="col-md-6 col-xs-12">        
                             <label>Fecha vencimiento Camara de comercio</label>
                            <div class="calendario">
                                <input class="form-control flatpickr-input flatpickr-format" type="" name="NuevoFechaVenCamarac" id="NuevoFechaVenCamarac" placeholder="Fecha vencimiento Camara de comercio" >                                                       
                            </div>        
                        </div>    
                                                
                                    
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="form-label">Fecha vencimiento cuenta bancaria</label>
                                <div class="input-group">
                                <span class="input-group-addon">
                                <input class="form-control flatpickr-input flatpickr-format" type="text" name="NuevoFechaVenCuentab" id="NuevoFechaVenCuentab" placeholder="Fecha vencimiento cuenta bancaria" >
                            </div>
                        </div>        
                    </div>    
                </div>                                     

                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Correo notificación 1</label>
                                    <div class="input-group">
                                        <input class="form-control " type="text" name="NuevoCorreoContacto1" id="NuevoCorreoContacto1" placeholder="Correo notificación 1">
                                    </div>
                                </div>     
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <labe class="form-label">Correo notificación 2</label>
                                    <div class="input-group">
                                        <input class="form-control " type="text" name="NuevoCorreoContacto2" id="NuevoCorreoContacto2" placeholder="Correo notificación 2">
                                    </div>
                                </div>     
                            </div> 
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Correo notificación 3</label>
                                    <div class="input-group">
                                        <input class="form-control " type="text" name="NuevoCorreoContacto3" id="NuevoCorreoContacto3" placeholder="Correo notificación 3">
                                    </div>
                                </div>     
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Correo notificación 4</label>
                                    <div class="input-group">
                                        <input class="form-control " type="text" name="NuevoCorreoContacto4" id="NuevoCorreoContacto4" placeholder="Correo notificación 4">
                                    </div>
                                </div>     
                            </div>  
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Correo Incapacidades.co</label>
                                    <div class="input-group">
                                        <input class="form-control " type="text" name="emp_correo_incapacidades_v" placeholder="Correo Incapacidades.co">
                                    </div>
                                </div>     
                            </div> 
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Contraseña Correo Incapacidades.co</label>
                                    <div class="input-group">
                                        <input class="form-control " type="text" name="emp_crv_v" placeholder="Contraseña Correo Incapacidades.co">
                                    </div>
                                </div>     
                            </div>                         
                        </div>

                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Rut del cliente</label>
                                    <input type="file" class="NuevaFoto" valor='0' name="NuevoRutCliente" placeholder="Elegir rut del cliente">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar pull-right">
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Camara de Comercio</label>
                                    <input type="file" class="NuevaFoto" valor='_2' name="NuevoCamaraDeComercionFoto">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2 pull-right">
                                </div>
                            </div>
                            <?php if($_SESSION['perfilN'] == 'SuperAdministrador' || $_SESSION['perfilN'] == 'Administrador'){ ?>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label class="form-label">Cedula Representante</label>
                                    <input type="file" class="NuevaFoto" valor='_3' name="NuevoCedulaRepresentante">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_3 pull-right">
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Cuenta Bancaria</label>
                                    <input type="file" class="form-label NuevaFoto" valor='_4' name="NuevoCuentaBancaria">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_4 pull-right">
                                </div>
                            </div>
                        <?php }?>
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-bs-dismiss="modal">Salir</button>
                        <button id="btnGuardarCliente" type="button" class="btn btn-primary">Guardar cliente</button>
                    </div>
                <?php
                    // $crearcliente = new ControladorClientes();
                    // $crearcliente->ctrCrearCliente();
                ?>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Modal agregar cliente -->
<div id="modalEditarCliente" class="modal fade"  data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background: #dd4b39;color: white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Editar Cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Ingresar Nombre</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarNombre" id="EditarNombre" placeholder="Ingresar Nombre" required="true">
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="row">
                                    <div class="col-md-9 col-xs-12">
                                        <div class="form-group">
                                            <label>Ingresar Identificación / Nit</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-key"></i>
                                                </span>
                                                <input class="form-control " type="text" name="EditarNit" id="EditarNit" maxlength="9" placeholder="Ingresar Identificación / Nit" required="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            <label>Verificación</label>
                                            <div class="input-group">
                                                <input class="form-control " type="text" name="EditarNitVerificacion"  maxlength="1" id="EditarNitVerificacion" placeholder="Verificación" required="true">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                     
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Ingresar Dirección</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-map-marker"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarDireccion" id="EditarDireccion" placeholder="Ingresar Dirección" >
                                    </div> 
                                </div>       
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Ingresar Teléfono</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarTelefono" id="EditarTelefono" placeholder="Ingresar Teléfono" >
                                    </div>
                                </div>      
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Ingresar Email</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarEmail" id="EditarEmail" placeholder="Ingresar Email" >
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Fecha Pago seguridad Social</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar-o"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarFechaPagoSeguridad" id="EditarFechaPagoSeguridad" placeholder="Fecha Pago seguridad Social" >
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row panel" >
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Nombre del Representante Legal</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarRepresentante" id="EditarRepresentante" placeholder="Nombre del Representante Legal" >
                                    </div>
                                </div>       
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Ingresar C.C Representante Legal</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-cc"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarCCRepresentante" id="EditarCCRepresentante" placeholder="Ingresar C.C Representante Legal" >
                                    </div>
                                </div>     
                            </div>
                        </div>
                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Ingresar Contacto Gestión</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarContacto" id="EditarContacto" placeholder="Ingresar Contacto Gestión">
                                    </div>
                                </div>     
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Email Contacto Gestión</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarEmailGestion" id="EditarEmailGestion" placeholder="Email Contacto Gestión" >
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label>Teléfono Fijo Contacto Gestión</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                                <input class="form-control " type="text" name="EditarTelefonoGestion" id="EditarTelefonoGestion" placeholder="Teléfono Fijo Contacto Gestión" >
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Ext.</label>
                                            <div class="input-group">
                                                <input class="form-control " type="text" name="EditarTelefonoGestionExt" id="EditarTelefonoGestionExt" placeholder="Ext." >
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Celular Contacto Gestión</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarCelular" id="EditarCelular" placeholder="Celular Contacto Gestión" >
                                    </div>
                                </div>        
                            </div>

                        </div>
                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Ingresar Contacto Tesoreria</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarContactoTesoreria" id="EditarContactoTesoreria" placeholder="Ingresar Contacto Tesoreria">
                                    </div>
                                </div>     
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Email Contacto Tesoreria</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarEmailTesoreria" id="EditarEmailTesoreria" placeholder="Email Contacto Tesoreria" >
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label>Teléfono Fijo Contacto Tesoreria</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                                <input class="form-control " type="text" name="EditarTelefonoTesoreria" id="EditarTelefonoTesoreria" placeholder="Teléfono Fijo Contacto Tesoreria" >
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Ext.</label>
                                            <div class="input-group">
                                                <input class="form-control " type="text" name="EditarTelefonoTesoreriaExt" id="EditarTelefonoTesoreriaExt" placeholder="Ext." >
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Celular Contacto Tesoreria</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarCelularTesoreria" id="EditarCelularTesoreria" placeholder="Celular Contacto Tesoreria" >
                                    </div>
                                </div>        
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="EditarLiquidacionAutomatica" id="EditarLiquidacionAutomatica" value="1"> Liquidación Automatica
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>% Ganancia Incapacidades.co</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </span>
                                            <input class="form-control " type="text" name="EditarPorcentajeGananciaInc" id="EditarPorcentajeGananciaInc" placeholder="% Ganancia Incapacidades.co" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                        </div>

                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Correo notificación 1</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarCorreoContacto1" id="EditarCorreoContacto1" placeholder="Correo notificación 1">
                                    </div>
                                </div>     
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Correo notificación 2</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarCorreoContacto2" id="EditarCorreoContacto2" placeholder="Correo notificación 2">
                                    </div>
                                </div>     
                            </div> 
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Correo notificación 3</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarCorreoContacto3" id="EditarCorreoContacto3" placeholder="Correo notificación 3">
                                    </div>
                                </div>     
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Correo notificación 4</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input class="form-control " type="text" name="EditarCorreoContacto4" id="EditarCorreoContacto4" placeholder="Correo notificación 4">
                                    </div>
                                </div>     
                            </div>  
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Correo Incapacidades.co</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input class="form-control " type="text" name="emp_correo_incapacidades_v" id="emp_correo_incapacidades_v" placeholder="Correo Incapacidades.co">
                                    </div>
                                </div>     
                            </div>   
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Contraseña Correo Incapacidades.co</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input class="form-control " type="text" name="emp_crv_v" id="emp_crv_v" placeholder="Contraseña Correo Incapacidades.co">
                                    </div>
                                </div>     
                            </div>                      
                        </div>

                        <div class="row panel">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Rut del cliente</label>
                                    <input type="file" class="NuevaFoto" valor='0' name="EditarRutCliente" placeholder="Elegir rut del cliente">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar pull-right">
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Camara de Comercio</label>
                                    <input type="file" class="NuevaFoto" valor='_2' name="EditarCamaraDeComercionFoto">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2 pull-right">
                                </div>
                            </div>
                            <?php if($_SESSION['perfilN'] == 'SuperAdministrador' || $_SESSION['perfilN'] == 'Administrador'){ ?>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Cedula Representante</label>
                                    <input type="file" class="NuevaFoto" valor='_3' name="EditarCedulaRepresentante">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_3 pull-right">
                                </div>
                            </div>

                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Cuenta Bancaria</label>
                                    <input type="file" class="NuevaFoto" valor='_4' name="EditarCuentaBancaria">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                    <img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_4 pull-right">
                                </div>
                            </div>
                            <?php } ?>
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                        <input type="hidden" name="Editar_idEmpresa" id="Editar_idEmpresa">
                        <input type="hidden" name="rutarutActual" id="rutarutActual">
                        <input type="hidden" name="rutaCamaracomercioActual" id="rutaCamaracomercioActual">
                        <input type="hidden" name="rutaCedulaRepresentanteActual" id="rutaCedulaRepresentanteActual">
                        <input type="hidden" name="rutaCuentaBancariaActual" id="rutaCuentaBancariaActual">
                        <button type="submit" class="btn btn-primary">Editar cliente</button>
                    </div>
                <?php
                    $crearcliente = new ControladorClientes();
                    $crearcliente->ctrEditarClientes();
                ?>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->

<script type="text/javascript">
    $("#tablaClientes tbody").on("click", ".btnEditarCliente", function () {   
        var idCliente = $(this).attr('idCliente');

        var datas = new FormData();
        datas.append('redirectClient', idCliente);
        $.ajax({
            url   : 'ajax/clientes.ajax.php',
            method: 'post',
            data  : datas,
            cache : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            success     : function(data){  
                if (data.code == "1") {
                    window.location = "index.php?ruta=clientes";
                }
            }
        });
    });

    $("#tablaClientes tbody").on("click", ".btnVerEmpleados", function () {   
        var idCliente = $(this).attr('idCliente');

        var datas = new FormData();
        datas.append('redirectEmpleados', idCliente);
        $.ajax({
            url   : 'ajax/clientes.ajax.php',
            method: 'post',
            data  : datas,
            cache : false,
            contentType : false,
            processData : false,
            dataType    : 'json',
            success     : function(data){  
                if (data.code == "1") {
                    window.location = "index.php?ruta=empleados";
                }
            }
        });
    });
</script>

<script src="vistas/js/moment.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/es.js"></script>
<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/clientes.js?v=<?php echo rand();?>"></script>

<?php 
    $crearPaciente = new ControladorClientes();
    $crearPaciente->ctrBorrarCliente();
?>
