<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">CLIENTES</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Actualizar Información Del Cliente</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">DATOS DEL CLIENTES</h5>
                <input type="hidden" name="idCLiente" id="clienteLogieado" value="<?php echo $_SESSION['cliente_id']; ?>">
            </div>

            <div class="card-body">
                <form role="form" id="infoCliente" autocomplete="off" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="mb-3">
                                <label>Ingresar Nombre</label>
                                <input class="form-control " type="text" name="EditarNombre" id="EditarNombre" placeholder="Ingresar Nombre" required="true">
                            </div>
                        </div>

                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Ingresar Identificación / Nit</label>
                                <input class="form-control " type="text" name="EditarNit" id="EditarNit" maxlength="9" placeholder="Ingresar Identificación / Nit" required="true">
                            </div>   
                        </div>  
                        
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Verificación</label>
                                <input class="form-control " type="text" name="EditarNitVerificacion"  maxlength="1" id="EditarNitVerificacion" placeholder="Verificación" required="true">
                            </div>
                        </div>   
                    </div><!--cierre div row-->

                    <div class="row">
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Ingresar Dirección</label>
                                <input class="form-control " type="text" name="EditarDireccion" id="EditarDireccion" placeholder="Ingresar Dirección" >
                            </div>   
                        </div>
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Ingresar Teléfono</label>
                                <input class="form-control " type="text" name="EditarTelefono" id="EditarTelefono" placeholder="Ingresar Teléfono" >
                            </div>
                        </div>

                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Ingresar Email</label>
                                <input class="form-control " type="text" name="EditarEmail" id="EditarEmail" placeholder="Ingresar Email" >
                            </div>   
                        </div>     
                    </div><!--cierre div row-->
                                    
                    <div class="row">
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Fecha Pago seguridad Social</label>
                                <input class="form-control " type="text" name="EditarFechaPagoSeguridad" id="EditarFechaPagoSeguridad" placeholder="Fecha Pago seguridad Social" >
                            </div>
                        </div>   
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Nombre del Representante Legal</label>
                                <input class="form-control " type="text" name="EditarRepresentante" id="EditarRepresentante" placeholder="Nombre del Representante Legal" >
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Ingresar C.C Representante Legal</label>
                                <input class="form-control " type="text" name="EditarCCRepresentante" id="EditarCCRepresentante" placeholder="Ingresar C.C Representante Legal" >
                            </div>
                        </div>        
                    </div><!--cierre div row-->

                    <div class="row">
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Ingresar Contacto Gestión</label>
                                <input class="form-control " type="text" name="EditarContacto" id="EditarContacto" placeholder="Ingresar Contacto Gestión">
                            </div>
                        </div>   
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Email Contacto Gestión</label>
                                <input class="form-control " type="text" name="EditarEmailGestion" id="EditarEmailGestion" placeholder="Email Contacto Gestión" >
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Teléfono Fijo Contacto Gestión</label>
                                <input class="form-control " type="text" name="EditarTelefonoGestion" id="EditarTelefonoGestion" placeholder="Teléfono Fijo Contacto Gestión" >
                            </div>
                        </div>        
                    </div><!--cierre div row-->

                    <div class="row">
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Ext.</label>
                                <input class="form-control " type="text" name="EditarTelefonoGestionExt" id="EditarTelefonoGestionExt" placeholder="Ext." >
                            </div>
                        </div>   
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Celular Contacto Gestión</label>
                                <input class="form-control " type="text" name="EditarCelular" id="EditarCelular" placeholder="Celular Contacto Gestión" >
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Ingresar Contacto Tesoreria</label>
                                <input class="form-control " type="text" name="EditarContactoTesoreria" id="EditarContactoTesoreria" placeholder="Ingresar Contacto Tesoreria">
                            </div>
                        </div>        
                    </div><!--cierre div row-->

                    <div class="row">
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Email Contacto Tesoreria</label>
                                <input class="form-control " type="text" name="EditarEmailTesoreria" id="EditarEmailTesoreria" placeholder="Email Contacto Tesoreria" >
                            </div>
                        </div>   
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Teléfono Fijo Contacto Tesoreria</label>
                                <input class="form-control " type="text" name="EditarTelefonoTesoreria" id="EditarTelefonoTesoreria" placeholder="Teléfono Fijo Contacto Tesoreria" >
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Ext.</label>
                                <input class="form-control " type="text" name="EditarTelefonoTesoreriaExt" id="EditarTelefonoTesoreriaExt" placeholder="Ext." >
                            </div>
                        </div>        
                    </div><!--cierre div row-->

                    <div class="row">
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Celular Contacto Tesoreria</label>
                                <input class="form-control " type="text" name="EditarCelularTesoreria" id="EditarCelularTesoreria" placeholder="Celular Contacto Tesoreria" >
                            </div>   
                        </div>       
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Fecha Creación Camara de comercio</label>
                                <input class="form-control flatpickr-input " type="text" name="EditarFechaVenCamarac" id="EditarFechaVenCamarac" placeholder="Fecha Creación Camara de comercio"> 
                            </div>
                        </div>

                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>% Ganancia Incapacidades.co</label>
                                <input class="form-control flatpickr-input " type="text" name="EditarPorcentajeGananciaInc" id="EditarPorcentajeGananciaInc" placeholder="% Ganancia Incapacidades.co" >
                            </div>
                        </div>        
                    </div><!--cierre div row-->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="EditarLiquidacionAutomatica" id="EditarLiquidacionAutomatica" value="1"> Liquidación Automatica
                                </label>
                            </div>
                        </div>
                    </div><!--cierre div row-->

                    <div class="row">
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Fecha Creación Cuenta bancaria</label>
                                <input class="form-control " type="text" name="EditarFechaVenCuentab" id="EditarFechaVenCuentab" placeholder="Fecha Creación Cuenta bancaria" >
                            </div>
                        </div>   
                               
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Correo notificación 1</label>
                                <input class="form-control " type="text" name="EditarCorreoContacto1" id="EditarCorreoContacto1" placeholder="Correo notificación 1">
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Correo notificación 2</label>
                                <input class="form-control " type="text" name="EditarCorreoContacto2" id="EditarCorreoContacto2" placeholder="Correo notificación 2">
                            </div>
                        </div>        
                    </div><!--cierre div row-->

                    <div class="row">
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Correo notificación 3</label>
                                <input class="form-control " type="text" name="EditarCorreoContacto3" id="EditarCorreoContacto3" placeholder="Correo notificación 3">
                            </div>   
                        </div> 
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Correo notificación 4</label>
                                <input class="form-control " type="text" name="EditarCorreoContacto4" id="EditarCorreoContacto4" placeholder="Correo notificación 4">
                            </div>
                        </div>

                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Correo Incapacidades.co</label>
                                <input class="form-control " type="text" name="emp_correo_incapacidades_v" id="emp_correo_incapacidades_v" placeholder="Correo Incapacidades.co">
                            </div>
                        </div>        
                    </div><!--cierre div row-->

                    <div class="row">
                        <div class="col-md-4 col-xs-12"> 
                            <div class="mb-3">
                                <label>Contraseña Correo Incapacidades.co</label>
                                <input class="form-control " type="text" name="emp_crv_v" id="emp_crv_v" placeholder="Contraseña Correo Incapacidades.co">
                            </div>
                        </div>     
                    </div><!--cierre div row-->

                    <div class="row">     
                        <div class="col-md-3 col-xs-12"> 
                            <div class="mb-3">
                                <label><b>Rut del cliente</b></label>
                                <input type="file" class="NuevaFoto" valor='0' name="EditarRutCliente" placeholder="Elegir rut del cliente">
                                <p class="help-block"> 
                                 Peso maximo del archivo 2 MB
                                </p>
                                <a class="rut" target="_blank"><img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar pull-right"></a>
                             </div>
                        </div>

                        <div class="col-md-3 col-xs-12"> 
                            <div class="mb-3">
                                <label><b>Camara de Comercio</b></label>
                                <input type="file" class="NuevaFoto" valor='_2' name="EditarCamaraDeComercionFoto">
                                <p class="help-block">
                                    Peso maximo del archivo 2 MB
                                </p>
                                <a class="camaraComercio" target="_blank"><img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_2 pull-right"></a>
                            </div>
                        </div>  
                             
                                 
                        <?php if($_SESSION['perfilN'] == 'SuperAdministrador' || $_SESSION['perfilN'] == 'Administrador'){ ?>
                        <div class="col-md-3 col-xs-12">
                            <div class="mb-3">
                                <label><b>Cedula Representante</b></label>
                                <input type="file" class="NuevaFoto" valor='_3' name="EditarCedulaRepresentante">
                                <p class="help-block">
                                    Peso maximo del archivo 2 MB
                                </p>
                                <a class="cedulaRepresentante" target="_blank"><img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_3 pull-right"></a>
                            </div>
                        </div>

                        <div class="col-md-3 col-xs-12">
                            <div class="mb-3">
                                <label><b>Cuenta Bancaria</b></label>
                                <input type="file" class="NuevaFoto" valor='_4' name="EditarCuentaBancaria">
                                <p class="help-block">
                                    Peso maximo del archivo 2 MB
                                </p>
                                <a class="cuentaBancaria" target="_blank"><img src="vistas/img/usuarios/default/anonymous.png" width="100px" class="img-thumbnail previsualizar_4 pull-right"></a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="row">     
                        <div class="col-md-9 col-xs-12"> 
                            <input type="hidden" name="Editar_idEmpresa" id="Editar_idEmpresa">
                            <input type="hidden" name="rutarutActual" id="rutarutActual">
                            <input type="hidden" name="rutaCamaracomercioActual" id="rutaCamaracomercioActual">
                            <input type="hidden" name="rutaCedulaRepresentanteActual" id="rutaCedulaRepresentanteActual">
                            <input type="hidden" name="rutaCuentaBancariaActual" id="rutaCuentaBancariaActual">
                        </div>
                        <div class="col-md-3">
                            <div class="d-grid gap-2">
                                <button type="button" id="enviarClientesInfo" class="btn btn-primary btn-lg">Guardar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="vistas/js/edit_client.js?v=<?php echo rand();?>"></script>

<script type="text/javascript">

    var EditarFechaCamaraC = flatpickr('#EditarFechaVenCamarac',{
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        locale: "es",
        onChange: function(selectedDates, dateStr, instance){
            var minDate = dateStr;
            var fecha1 = moment($("#EditarFechaVenCamarac").val());
            var fecha2 = moment(minDate);
            $("#EditarNumeroDias").val( (fecha2.diff(fecha1, 'days') + 1) + " Días");
        }
    });

    var EditarFechaVenCuentab = flatpickr('#EditarFechaVenCuentab',{
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        locale: "es",
        onChange: function(selectedDates, dateStr, instance){
            var minDate = dateStr;
            var fecha1 = moment($("#EditarFechaVenCuentab").val());
            var fecha2 = moment(minDate);
            $("#EditarNumeroDias").val( (fecha2.diff(fecha1, 'days') + 1) + " Días");
        }
    });
    
</script>