<!-- choices css -->
<link href="vistas/assets/assets/libs/choices.js/public/assets/styles/choices.min.css" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- datepicker css -->
<link rel="stylesheet" href="vistas/assets/assets/libs/flatpickr/flatpickr.min.css">
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/alertifyjs/build/css/alertify.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">CARTERA</h4>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Incapacidades.co</a></li>
                    <li class="breadcrumb-item active">Cartera De Incapacidades </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                   
                    <div class="row">
                        <div class="col-lg-11">
                        INCAPACIDADES POR ENTIDAD
                        </div>
                        <div class="col-lg-1">
                            <div class="btn-group dropstart" role="group">
                                <button id="btnGroupVerticalDrop1" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ACCIONES <i class="mdi mdi-chevron-left"></i>
                                </button>
                                <div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">
                                    <?php if($_SESSION['adiciona'] == 1){ ?>
                                    <a class="dropdown-item" title="Agregar Usuario EPS" data-bs-toggle="modal" data-bs-target="#modalAgregarCartera" href="#">AGREGAR</a>
                                    <div class="dropdown-divider"></div>
                                    <?php } ?>
                                </div>
                            </div> 
                        </div>
                    </div>
                </h5>
            </div>
            <div class="card-body">
                <table style="width:100%;" id="tablaCartera" class="table table-bordered table-striped dt-responsive tablas">
                        <thead>
                            <tr>
                                <th style="width: 10px;">#</th>
                                <th  style="text-align:center; width: 20%;">Entidad</th>
                                <th  style="text-align:center;">Paz y Salvo</th>
                                <th  style="text-align:center;">Valor</th>
                                <th  style="text-align:center;">F. Solicitud</th>
                                <th  style="text-align:center;">F. Radicaci&oacute;n</th>
                                <th  style="text-align:center;">Respuesta</th>
                                <th  style="text-align:center;">F. Respuesta</th>
                                <th  style="text-align:center;">Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style="width: 10px;">#</th>
                                <th  style="text-align:center;">Entidad</th>
                                <th  style="text-align:center;">Paz y Salvo</th>
                                <th  style="text-align:center;">Valor</th>
                                <th  style="text-align:center;">F. Solicitud</th>
                                <th  style="text-align:center;">F. Radicaci&oacute;n</th>
                                <th  style="text-align:center;">Respuesta</th>
                                <th  style="text-align:center;">F. Respuesta</th>
                                <th  style="text-align:center;">Acciones</th>
                            </tr>
                        </tfoot>
                    </table>  
            </div>
        </div>
    </div>
</div>
<!-- Modal agregar usuario -->
<div id="modalAgregarCartera" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" id="AgragrNuevaCartera" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                <h5 class="modal-title"id="staticBackdropLabel">Agregar Cartera</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                   
                </div>
                <div class="modal-body">
                    <div class="box-body">
                    <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="mb-3">
                                    <label class="form-label">Seleccionar Empresa</label>
                                    <select class="form-control" required="true" id="NuevoEps" name="NuevoEps">
                                                <option value="">Selecionar Empresa</option>
                                                <?php
                                                $item = NULL;
                                                $valor = NULL;
                                                $IpsAfiliada = ControladorClientes::ctrMostrarClientes($item, $valor);
                                                foreach ($IpsAfiliada as $key => $value) {
                                                    echo '<option value="'.$value["emp_id"].'">'.$value["emp_nombre"].'</option>';
                                                }
                                            ?>
                                            </select>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                          <label class="form-label">Fecha Solicitud</label>
                                          <input class="form-control fecha" type="text" name="NuevoFechaSolicitud" id="NuevoFechaSolicitud" placeholder="Ingresar Fecha Solicitud" >
                                    
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                 <div class="mb-3">
                                    <div class="form-group">
                                   
                                        <label class="form-label">Fecha Radicacion</label>
                                        <input class="form-control fecha" type="text" name="NuevoFechaRadicacion" id="NuevoFechaRadicacion" placeholder="Ingresar Fecha Radicaci&oacute;n" >
                                   
                                    </div>     
                                            </div>   
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    
                                        <label class="form-label">Ingresar Valor</label>
                                        <input class="form-control" type="text" name="NuevoValor" id="NuevoValor" placeholder="Ingresar Valor" >
                                    
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                      <textarea class="form-control fecha" name="NuevoObservacion" id="NuevoObservacion" placeholder="Ingresar Observaci&oacute;n" ></textarea>
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                 <div class="mb-3">
                                    <div class="input-group">
                                        <label>
                                            <input type="checkbox" class="minimal" name="NuevoPazSalvo" value="SI">&nbsp;A Paz y salvo
                                        </label>
                                    </div>
                                            </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>
                                            <input type="checkbox" class="minimal" name="NuevoRespuesta" id="NuevoRespuesta" value="SI">&nbsp;Tiene Respuesta
                                        </label>                                    
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control fecha"  disabled type="text" name="NuevoFechaRespuesta" id="NuevoFechaRespuesta" placeholder="Ingresar Fecha Respuesta" >
                                    </div>
                                </div>        
                            </div>
                            <?php
                                if($_SESSION['cliente_id'] != 0){
                            ?>
                                <input type="hidden" name="NuevoEmpresa" value="<?php echo $_SESSION['cliente_id'];?>">
                            <?php
                                }else{
                            ?>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select class="form-control" style="width: 100%;" required="true" id="NuevoEmpresa" name="NuevoEmpresa">
                                            <option value="">Selecionar Empresa</option>
                                            <?php
                                                $item = NULL;
                                                $valor = NULL;
                                                $IpsAfiliada = ControladorClientes::ctrMostrarClientes($item, $valor);
                                                foreach ($IpsAfiliada as $key => $value) {
                                                    echo '<option value="'.$value["emp_id"].'">'.$value["emp_nombre"].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>        
                            </div>
                            <?php                                    
                                }
                            ?>
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>Evidencia Cartera</label>
                                    <input type="file" class="NuevaFotoCartera" valor='_3' name="NuevoEvidenciaCartera">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Detalle de la cartera</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table style="width: 100%;" id="tablaCarteraDetalle" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Nombres</th>
                                                    <th>C&eacute;dula</th>
                                                    <th>Periodo</th>
                                                    <th>Valor</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyParaCartera">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="NumeroDetalleNuevo" id="NumeroDetalleNuevo" value="0">
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Salir</button>
                        <button type="button" id="btnAddIncapacidad" class="btn btn-primary">Agregar Incapacidad</button>
                        <!--<button type="submit" class="btn btn-primary">Guardar Archivo</button>-->
                        <button type="button" id="btnNuevaCartera" class="btn btn-danger">Guardar Archivo</button>
                    </div>
                <?php
                  /*$crearArchivo = new ControladorCartera();
                    $crearArchivo->ctrCrearCartera();*/
                ?>
                </div>
            </form>
        </div>
    </div>
</div>
 
<div id="modalEditarCartera" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form role="form" autocomplete="off" id="EditarCarteraExistente" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background: #dd4b39;color: white;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Editar Cartera</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <select class="form-control" required="true" id="EditarEps" name="EditarEps">
                                            <option value="">Selecionar Administradora</option>
                                            <?php
                                                $item = NULL;
                                                $valor = NULL;
                                                $IpsAfiliada = ControladorIps::ctrMostrarIps($item, $valor);
                                                foreach ($IpsAfiliada as $key => $value) {
                                                    echo '<option value="'.$value["ips_id"].'">'.$value["ips_nombre"].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <input class="form-control fecha" type="text" name="EditarFechaSolicitud" id="EditarFechaSolicitud" placeholder="Ingresar Fecha Solicitud" >
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <input class="form-control fecha" type="text" name="EditarFechaRadicacion" id="EditarFechaRadicacion" placeholder="Ingresar Fecha Radicaci&oacute;n" >
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <input class="form-control" type="text" name="EditarValor" id="EditarValor" placeholder="Ingresar Valor" >
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <textarea class="form-control fecha" name="EditarObservacion" id="EditarObservacion" placeholder="Ingresar Observaci&oacute;n" ></textarea>
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>
                                            <input type="checkbox" class="minimal" name="EditarPazSalvo" id="EditarPazSalvo" value="SI">&nbsp;A Paz y salvo
                                        </label>
                                    </div>
                                </div>        
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label>
                                            <input type="checkbox" class="minimal" name="EditarRespuesta" id="EditarRespuesta" value="SI">&nbsp;Tiene Respuesta
                                        </label>                                    
                                    </div>
                                </div>        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <input class="form-control fecha" type="text" name="EditarFechaRespuesta" id="EditarFechaRespuesta" placeholder="Ingresar Fecha Respuesta" >
                                    </div>
                                </div>        
                            </div>
                            <?php
                                if($_SESSION['cliente_id'] != 0){
                            ?>
                                <input type="hidden" name="EditarEmpresa" value="<?php echo $_SESSION['cliente_id'];?>">
                            <?php
                                }else{
                            ?>
                            <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-edit"></i>
                                        </span>
                                        <select class="form-control" style="width: 100%;" required="true" id="EditarEmpresa" name="EditarEmpresa">
                                            <option value="">Selecionar Empresa</option>
                                            <?php
                                                $item = NULL;
                                                $valor = NULL;
                                                $IpsAfiliada = ControladorClientes::ctrMostrarClientes($item, $valor);
                                                foreach ($IpsAfiliada as $key => $value) {
                                                    echo '<option value="'.$value["emp_id"].'">'.$value["emp_nombre"].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>        
                            </div>
                            <?php                                    
                                }
                            ?>
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>Evidencia Cartera</label>
                                    <input type="file" class="NuevaFotoCartera" valor='_3' name="EditarEvidenciaCartera">
                                    <p class="help-block">
                                        Peso maximo del archivo 2 MB
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Detalle de la cartera</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table style="width: 100%;" id="tablaCarteraDetalleEditar" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Nombres</th>
                                                    <th>C&eacute;dula</th>
                                                    <th>Periodo</th>
                                                    <th>Valor</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyParaCarteraEditar">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>


<?php 
  //  $crearPaciente = new ControladorUsuariosEps();
    //$crearPaciente->ctrBorrarUsuariosEps();
?>
<script src="vistas/js/moment.min.js"></script>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- choices js -->
<script src="vistas/assets/assets/libs/choices.js/public/assets/scripts/choices.min.js"></script>

<!-- datepicker js -->
<script src="vistas/assets/assets/libs/flatpickr/flatpickr.min.js"></script>

<!-- Buttons examples -->

<script src="vistas/assets/assets/libs/alertifyjs/build/alertify.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/plugins/dataTime/datetime.js"></script>
<script src="vistas/assets/assets/libs/sweetalert2/sweetalert2.min.js"></script>

<script type="text/javascript">
    $(function(){

        /*var edicion = '<button  title="Editar Entidad" class="btn btn-warning btnEditarEps" idcartera data-toggle="modal" data-target="#"><i class="fa fa-edit"></i></button>&nbsp;';
        edicion += '<button title="Eliminar Entidad" class="btn btn-danger btnEliminarEps" idcartera><i class="fa fa-times"></i></button>';
*/

    let edicion = '<div class="btn-group dropstart" role="group">';
    edicion += '<button type="button" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    edicion += '<i class="fa fa-info-circle"></i>';
    edicion += '</button>';
    edicion += '<div class="dropdown-menu  dropdownmenu-danger" aria-labelledby="btnGroupVerticalDrop1">';
    edicion += '<a class="dropdown-item btnEditarCartera" title="Editar Entidad" idcartera data-bs-toggle="modal" data-bs-target="#modalEditarEntidad">EDITAR</a>';
    edicion += '<div class="dropdown-divider"></div>';
    edicion += '<a class="dropdown-item btnEliminarCartera" title="Eliminar Entidad" idcartera codigo imagen>ELIMINAR</a>';



        var tablaIncapa = $('#tablaCartera').DataTable({
            "ajax": "ajax/cartera.ajax.php?getDataCartera=owi",
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "defaultContent": edicion
                }
            ],
            "language" : {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            } 
        });


        $('#tablaCartera tbody').on( 'click', 'button', function () {
            var data = tablaIncapa.row( $(this).parents('tr') ).data();
            $(this).attr("idcartera", data[7]);
        });
        $('#tablaCartera tbody').on( 'click', 'a', function () {
            var data = tablaIncapa.row( $(this).parents('tr') ).data();
            console.log(data);
            $(this).attr("idcartera", data[7]);
        });
        $('#tablaCartera tbody').on("click", ".btnEditarCartera", function(){
                var x = $(this).attr('idcartera');
                var datas = new FormData();
                console.log($(this));
                datas.append('idcartera', x);
                $.ajax({
                url   : 'ajax/cartera.ajax.php?getDataCartera',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success     : function(data){
                    $("#EditarEps").val(data[0].ips_nombre); 
                    $("#EditarCodigoEps").val(data[0].ips_codigo);        
                    $("#EditarNitEps").val(data[0].ips_nit);
                    $("#EditarTipoEps").val(data[0].ips_tipo);
                    $("#EditarCorreoCartera").val(data[0].ips_correo_cartera);
                    $("#EditarCorreoPQRS").val(data[0].ips_correo_pqr_v);
                    $("#EditarCorreoRadicar").val(data[0].ips_correo_radicacion);
                    $("#EditarCorreoNOtificacion").val(data[0].ips_correo_not_v);
                    $("#EditarId").val(data[0].ips_id);
                }

            });
        });

        /* Eliminar administradoras */
    $('#tablaCartera tbody').on("click", ".btnEliminarEps", function(){
        var x = $(this).attr('idcartera');
        swal.fire({
            title: '¿Está seguro de borrar el usuario?',
            text: "¡Si no lo está puede cancelar la accíón!",
        /* type: 'warning',*/
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, borralo!'
        }).then(function (result) {
            if (result.value) {
                //window.location = "index.php?ruta=usuariosEps&id_usuario_eps="+x ;
                var datas = new FormData();
                datas.append('ips_id', x);
                $.ajax({
                url   : 'ajax/cartera.ajax.php?getDataCartera',
                method: 'post',
                data  : datas,
                cache : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                   // tabla_usuariosEps.ajax.reload();
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    if(data.code == '1'){
                        alertify.success( data.desc );    
                    }else{
                        alertify.error( data.desc );
                    } 
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        }
    })
});

  


        $("#enviarEdicion").click(function(){
            var form = $("#EdicionEntidad");
            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#EdicionEntidad")[0]);
            $.ajax({
                url: 'ajax/cartera.ajax.php?getDataCartera',
                type  : 'post',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    tablaIncapa.ajax.reload();
                    $("#modalEditarEntidad").modal('hide');
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    if(data.code == '1'){
                        alertify.success( data.message );    
                        $("#EdicionEntidad")[0].reset(); 
                    }else{
                        alertify.error( data.message );
                    }
                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        });

        $("#guardarEntidadNueva").click(function(){
            var form = $("#nuevaEntidadIps");
            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#nuevaEntidadIps")[0]);
            $.ajax({
                url: 'ajax/cartera.ajax.php?getDataCartera',
                type  : 'post',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff'
                        } 
                    }); 
                },
                complete:function(){
                    tablaIncapa.ajax.reload();
                    $("#modalAgregarEntidad").modal('hide');
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    if(data.code == '1'){
                        alertify.success( data.message );  
                        $("#nuevaEntidadIps")[0].reset();  
                    }else{
                        alertify.error( data.message );
                    }


                },
                //si ha ocurrido un error
                error: function(){
                    //after_save_error();
                    alertify.error('Error al realizar el proceso');
                }
            });
        });

      
    });

</script>

