<?php 

    function getDataAlertaModulo($alerta, $diasProcesar){
        $numeroAlertas = 0;
        foreach ($alerta as $key => $value) {
                                 
            $camposX = "inc_fecha_inicio, inc_fecha_final";
            $tablasX = "gi_incapacidad";
            $whereXX = "inc_cc_afiliado = ".$value['inc_cc_afiliado']." AND inc_empresa = ".$_SESSION['cliente_id'];
            $verificar = ModeloTesoreria::mdlMostrarGroupAndOrder($camposX, $tablasX, $whereXX, '', 'ORDER BY inc_fecha_inicio ASC');
            $valida = false;
            $dias_transcurridos = 0;
            $inc_fecha_inicio_Final = ''; 
            foreach ($verificar as $incapacidades => $incapacidad) {
                if($inc_fecha_inicio_Final != ''){
                    //tebemos que validar que la fecha inicio sea mayor a 30 dias de la final pasada
                    $dias = 0;
                    $dias = ControladorIncapacidades::dias_transcurridos( $inc_fecha_inicio_Final, $incapacidad['inc_fecha_inicio']);
                    if($dias <= 30){
                        $valida = true;
                        $dias_transcurridos += $dias;
                    }
                }
                $inc_fecha_inicio_Final = $incapacidad['inc_fecha_final'];
            }
            if($dias_transcurridos >= $diasProcesar){
                $numeroAlertas++;
            }
        }
            
        $sinS = 'Incapacidad';
        if($numeroAlertas > 1){
            $sinS = 'Incapacidades';
        }
        if($numeroAlertas > 0){
            $amostrar = ' <div class="col-xl-3 col-md-6">
                             <div class="card card-h-100">
                                <div class="card-body">
                                     <div class="row align-items-center">
                                        <div class="col-6">
                                            <h3>'.$numeroAlertas.'</h3>
                                            <p>+'.$diasProcesar.' DÍAS</p>
                                        </div>
                                     </div>
                                     <div class="text-nowrap">
                                    
                                     <a href="'.$diasProcesar.'dias" class="small-box-footer">VER INCAPACITADOS <i class="fa fa-arrow-circle-right"></i></a>
                                 </div>
                                </div>
                             </div>
                        </div>';
            return $amostrar;    
        }else{
            $amostrar = ' <div class="col-xl-3 col-md-6">
                            <div class="card card-h-100">
                            <div class="card-body">
                                    <div class="row align-items-center">
                                    <div class="col-6">
                                        <h3>'.$numeroAlertas.'</h3>
                                        <p>+'.$diasProcesar.' DÍAS</p>
                                    </div>
                                    </div>
                                    <div class="text-nowrap">
                                
                                    <a href="'.$diasProcesar.'dias" class="small-box-footer">VER INCAPACITADOS <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            </div>
                    </div>';
            return $amostrar; 
        }
        
    }

    $campos = 'inc_cc_afiliado';
    $tablas = 'reporte_alarmas';

    $condiciones  = 'Tiempo_dias > 90 AND Tiempo_dias < 120';
    $condiciones1 = 'Tiempo_dias > 120 AND Tiempo_dias < 180';
    $condiciones2 = 'Tiempo_dias > 180 AND Tiempo_dias < 540';
    $condiciones3 = 'Tiempo_dias > 540';

    if($_SESSION['cliente_id'] != 0){
        $condiciones .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
        $condiciones1 .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
        $condiciones2 .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
        $condiciones3 .= ' AND inc_empresa = '.$_SESSION['cliente_id'];
    }
    $alarma1 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones, '', '');
    $alarma2 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones1, '', '');
    $alarma3 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones2, '', '');
    $alarma4 = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $condiciones3, '', '');

    $faIcono = "fa-warning";
?>
<div class="row">
    <?php
        $amostrar = '';
        if($alarma1 != null && $alarma1 != false){
            $alertasString = getDataAlertaModulo($alarma1, 90);
            if($alertasString != ''){
                $amostrar .= $alertasString;
            }
            
        }

        if($alarma2 != null && $alarma2 != false){
            $alertasString = getDataAlertaModulo($alarma2, 120);
            if($alertasString != ''){
                $amostrar .= $alertasString;
            }
        }
        if($alarma3 != null && $alarma3 != null){
            $alertasString = getDataAlertaModulo($alarma3, 180);
            if($alertasString != ''){
                $amostrar .= $alertasString;
            }
        }

        if($alarma4 != null && $alarma4 != false){
            $alertasString = getDataAlertaModulo($alarma4, 540);
            if($alertasString != ''){
                $amostrar .= $alertasString;
            }
        }

        echo $amostrar;
    ?>
</div>