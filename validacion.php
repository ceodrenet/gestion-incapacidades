<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    date_default_timezone_set('America/Bogota');

    /* extender */
    require_once 'controladores/mail.controlador.php';
    require_once 'controladores/plantilla.controlador.php'; 
    require_once 'controladores/usuarios.controlador.php';

    require_once 'modelos/dao.modelo.php';
    require_once 'modelos/usuarios.modelo.php';
?>
<!DOCTYPE html>
    <head>
        <title>Registro | I-Reports</title>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Premium Multipurpose Admin & Dashboard Template" name="description"/>
        <meta content="Themesbrand" name="author"/>
        <!-- App favicon -->
        <link rel="shortcut icon" href="vistas/assets/assets/images/favicon.ico">
        <!-- preloader css -->
        <link rel="stylesheet" href="vistas/assets/assets/css/preloader.min.css" type="text/css" />
        <!-- Bootstrap Css -->
        <link href="vistas/assets/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="vistas/assets/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="vistas/assets/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
    </head>
 <body>
    <div class="auth-page">
        <div class="container-fluid p-0">
            <div class="row g-0">
                <div class="col-xxl-3 col-lg-4 col-md-5">
                    <div class="auth-full-page-content d-flex p-sm-5 p-4">
                        <div class="w-100">
                            <div class="d-flex flex-column h-100">
                                <div class="mb-4 mb-md-5 text-center">
                                    <a href="index.php" class="d-block auth-logo">
                                        <img src="vistas/img/plantilla/LOGO_1.png" style="width: 60%;">
                                    </a>
                                </div>
                                <div class="auth-content my-auto">
                                    <div class="text-center">
                                       <?php 
                                            if(isset($_GET['validate'])){
                                                $sigin = new ControladorUsuarios();
                                                echo $sigin->crtValidarCodigo($_GET['validate']);
                                            }
                                       ?>
                                    </div>
                                  

                                    <div class="mt-5 text-center">
                                        <p class="text-muted mb-0">Ya tienes una cuenta ? <a href="login" class="text-primary fw-semibold"> Ingresar </a> </p>
                                    </div>
                                </div>
                                <div class="mt-4 mt-md-5 text-center">
                                    <p class="mb-0">© 2018 - <script>
                                            document.write(new Date().getFullYear())
                                        </script> Incapacidades.co, Hecho con <i class="mdi mdi-heart text-danger"></i></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end auth full page content -->
                </div>
                <!-- end col -->
                <div class="col-xxl-9 col-lg-8 col-md-7">
                    <div class="auth-bg pt-md-5 p-4 d-flex">
                        <div class="bg-overlay bg-primary"></div>
                        <ul class="bg-bubbles">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <!-- end bubble effect -->
                        <div class="row justify-content-center align-items-center">
                            <div class="col-xl-7">
                                <div class="p-0 p-sm-4 px-xl-0">
                                    <div id="reviewcarouselIndicators" class="carousel slide" data-bs-ride="carousel">
                                        <div class="carousel-indicators carousel-indicators-rounded justify-content-start ms-0 mb-0">
                                            <button type="button" data-bs-target="#reviewcarouselIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                            <button type="button" data-bs-target="#reviewcarouselIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                            <button type="button" data-bs-target="#reviewcarouselIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                            <button type="button" data-bs-target="#reviewcarouselIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
                                        </div>
                                        <!-- end carouselIndicators -->
                                        <div class="carousel-inner">
                                            <!--<div class="carousel-item active">
                                            <div class="testi-contain text-white">
                                                <i class="bx bxs-quote-alt-left text-success display-6"></i>

                                                <h4 class="mt-4 fw-medium lh-base text-white">“No hay secretos para el éxito. Es el resultado de la preparación, el trabajo en equipo y el aprendizaje del fracaso.”
                                                </h4>
                                                <div class="mt-4 pt-3 pb-5">
                                                    <div class="d-flex align-items-start">
                                                        <div class="flex-shrink-0">
                                                            <img src="vistas/assets/assets/images/users/Secretario.jpg" class="avatar-md img-fluid rounded-circle" alt="...">
                                                        </div>
                                                        <div class="flex-grow-1 ms-3 mb-4">
                                                            <h5 class="font-size-18 text-white">Colin Powell
                                                            </h5>
                                                            <p class="mb-0 text-white-50">Ex Secretario de Estado de los Estados Unidos</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="carousel-item">
                                            <div class="testi-contain text-white">
                                                <i class="bx bxs-quote-alt-left text-success display-6"></i>

                                                <h4 class="mt-4 fw-medium lh-base text-white">“El liderazgo efectivo no consiste en hacer discursos o ser querido; el liderazgo se define por los resultados no por los atributos.”</h4>
                                                <div class="mt-4 pt-3 pb-5">
                                                    <div class="d-flex align-items-start">
                                                        <div class="flex-shrink-0">
                                                            <img src="vistas/assets/assets/images/users/Otro.jpg" class="avatar-md img-fluid rounded-circle" alt="...">
                                                        </div>
                                                        <div class="flex-grow-1 ms-3 mb-4">
                                                            <h5 class="font-size-18 text-white">Peter Drucker
                                                            </h5>
                                                            <p class="mb-0 text-white-50">Abogado</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="carousel-item">
                                                <div class="testi-contain text-white">
                                                    <i class="bx bxs-quote-alt-left text-success display-6"></i>

                                                    <h4 class="mt-4 fw-medium lh-base text-white">“Lo que buscamos nos busca. Aquello que más deseamos en la vida está aguardando que lo encontremos.”</h4>
                                                    <div class="mt-4 pt-3 pb-5">
                                                        <div class="d-flex align-items-start">
                                                            <img src="vistas/assets/assets/images/users/RUMI.jpg" class="avatar-md img-fluid rounded-circle" alt="...">
                                                            <div class="flex-1 ms-3 mb-4">
                                                                <h5 class="font-size-18 text-white">Rumi</h5>
                                                                <p class="mb-0 text-white-50">Poeta
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="carousel-item">
                                                <div class="testi-contain text-white">
                                                    <i class="bx bxs-quote-alt-left text-success display-6"></i>

                                                    <h4 class="mt-4 fw-medium lh-base text-white">“Seamos los mejores; hay poca competencia para un producto o servicio extraordinario, la competencia existe cuando ofreces lo ordinario.”</div></h4>
                                                    <div class="mt-4 pt-3 pb-5">
                                                        <div class="d-flex align-items-start">
                                                            <img src="vistas/assets/assets/images/users/Luis.jpg" class="avatar-md img-fluid rounded-circle" alt="...">
                                                            <div class="flex-1 ms-3 mb-4">
                                                                <h5 class="font-size-18 text-white">Luis Carlos Garcia</h5>
                                                                <p class="mb-0 text-white-50">CEO - Incapacidades.co
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>-->
                                        </div>
                                        <!-- end carousel-inner -->
                                    </div>
                                    <!-- end review carousel -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container fluid -->
    </div>

<!-- JAVASCRIPT -->

<script src="vistas/assets/assets/libs/jquery/jquery.min.js"></script>
<script src="vistas/assets/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="vistas/assets/assets/libs/metismenu/metisMenu.min.js"></script>
<script src="vistas/assets/assets/libs/simplebar/simplebar.min.js"></script>
<script src="vistas/assets/assets/libs/node-waves/waves.min.js"></script>
<script src="vistas/assets/assets/libs/feather-icons/feather.min.js"></script>
<!-- pace js -->
<script src="vistas/assets/assets/libs/pace-js/pace.min.js"></script>

<!-- validation init -->
<script src="vistas/assets/assets/js/pages/validation.init.js"></script>

</body>

</html>