<?php

	class ControladorCartera extends ControladorPlantilla
	{
		/**
		*Desc.  => Mostrar toda la cartera, pero puede ser filtrada
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarCartera($item, $valor){
			$tabla = "gi_cartera";
			$respuesta = ModeloCartera::getDatos($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => ver el detalle de una cartera
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarDetalleCartera($item, $valor){
			$tabla = "gi_cartera_incapacidades";
			$respuesta = ModeloCartera::mdlMostrarDetalleCartera($tabla, $item, $valor);
			return $respuesta;
		}

	
		/**
		*Desc.  => ver el detalle de una cartera, pero con los JOIN y sus descripciones
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarCarteraDeglosada($item, $valor){
			$tabla = "gi_cartera";
			$respuesta = ModeloCartera::mdlMostrarCarteraDeglosada($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Crear datos de cartera
		*params => NuevoEvidenciaCartera, NuevoRespuesta, NuevoEmpresa, NuevoEps, NuevoFechaRadicacion, NuevoFechaSolicitud, NuevoValor, NuevoObservacion
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrCrearCartera(){
			if(isset($_POST['NuevoEps']) && $_POST['NuevoEps'] != '' ){
				/*Procedemos a insertar la cartera*/

				$ruta =  "";
				if(isset($_FILES['NuevoEvidenciaCartera']['tmp_name']) && !empty($_FILES['NuevoEvidenciaCartera']['tmp_name']) ){
	                $ruta = selft::putImage($_FILES['NuevoEvidenciaCartera']['tmp_name'], $_FILES["NuevoEvidenciaCartera"]["type"] , __DIR__."/../vistas/img/cartera/", 'vistas/img/cartera/');
	            }

				$pazSalvo = 'NO';
				$respuest = 'NO';
				$fechaRes = null;

				if(isset($_POST['NuevoPazSalvo'])){
					$pazSalvo = 'SI';
				}

				if(isset($_POST['NuevoRespuesta'])){
					$respuest = 'SI';
					$fechaRes = $_POST['NuevoFechaRespuesta'];
				}
				$datos = array(
					'car_emp_id' => $_POST['NuevoEmpresa'],
					'car_eps_id' => $_POST['NuevoEps'],
					'car_paz_salvo' => $pazSalvo,
					'car_fecha_rad' => $_POST['NuevoFechaRadicacion'],
					'car_fecha_cre' => $_POST['NuevoFechaSolicitud'],
					'car_fecha_res' => $fechaRes,
					'car_respuesta' => $respuest,
					'car_valor' => $_POST['NuevoValor'],
					'car_observacion' => $_POST['NuevoObservacion'],
					'car_ruta_pdf'	=> $ruta
				);

				$respuesta = ModeloCartera::mdlIngresarCartera('gi_cartera', $datos);
				self::setAuditoria('ControladorCartera', 'ctrCrearCartera',json_encode($datos), $respuesta);
				if($respuesta != 'Error'){
					$idCartera = $respuesta;
					
					if(isset($_POST['NumeroDetalleNuevo'])){
						/*Hay detalle que insertar*/
						$to = $_POST['NumeroDetalleNuevo'] +1;
						for ($i=0; $i < $to; $i++) { 
							# code...
							if(isset($_POST['NuevoNombreDetalle_'.$i]) && !empty($_POST['NuevoNombreDetalle_'.$i])){
								$datosNew = array(
									'ci_nombre' => $_POST['NuevoNombreDetalle_'.$i],
									'ci_identificacion' => $_POST['NuevoCedulaDetalle_'.$i],
									'ci_periodo' => $_POST['NuevoPeriodoDetalle_'.$i],
									'ci_valor' => $_POST['NuevoValorDetalle_'.$i],
									'ci_car_id' => $idCartera
								);
								$newRespuesta = ModeloCartera::mdlIngresarDetalleCartera('gi_cartera_incapacidades', $datosNew);
							}
						}
					}					
					echo json_encode( array('code' => 1, 'desc' => "Cartera registrada correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "Cartera no registrada"));
				}
			}
		}


		/**
		*Desc.  => Editar los datos de una cartera
		*params => EditarEvidenciaCartera, EditarRespuesta, EditarEmpresa, EditarEps, EditarFechaRadicacion, EditarFechaSolicitud, EditarValor, EditarObservacion
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrEditarCartera(){
			if(isset($_POST['EditarEps']) && $_POST['EditarEps'] != '' ){
				/*Procedemos a insertar la cartera*/
				$ruta =  $_POST['rutaEvidencia'];
	            if(isset($_FILES['EditarEvidenciaCartera']['tmp_name']) && !empty($_FILES['EditarEvidenciaCartera']['tmp_name']) ){
	                $ruta = selft::putImage($_FILES['EditarEvidenciaCartera']['tmp_name'], $_FILES["EditarEvidenciaCartera"]["type"] , __DIR__."/../vistas/img/cartera/", 'vistas/img/cartera/');
	            }

				$pazSalvo = 'NO';
				$respuest = 'NO';
				$fechaRes = null;
				if(isset($_POST['EditarPazSalvo'])){
					$pazSalvo = 'SI';
				}

				if(isset($_POST['EditarRespuesta'])){
					$respuest = 'SI';
					$fechaRes = $_POST['EditarFechaRespuesta'];
				}
				$datos = array(
					'car_emp_id' => $_POST['EditarEmpresa'],
					'car_eps_id' => $_POST['EditarEps'],
					'car_paz_salvo' => $pazSalvo,
					'car_fecha_rad' => $_POST['EditarFechaRadicacion'],
					'car_fecha_cre' => $_POST['EditarFechaSolicitud'],
					'car_fecha_res' => $fechaRes,
					'car_respuesta' => $respuest,
					'car_valor' => $_POST['EditarValor'],
					'car_observacion' => $_POST['EditarObservacion'],
					'car_id'		=> $_POST['idCartera'],
					'car_ruta_pdf'	=> $ruta
				);

				$respuesta = ModeloCartera::mdlEditarCartera('gi_cartera', $datos);
				self::setAuditoria('ControladorCartera', 'ctrEditarCartera',json_encode($datos), $respuesta);
				if($respuesta == 'ok'){

					$idCartera = $_POST['idCartera'];
					$borrarRespuesta = ModeloCartera::mdlBorrarDetalleCartera('gi_cartera_incapacidades', $idCartera);
					if(isset($_POST['NumeroDetalleEditar'])){
						/*Hay detalle que insertar*/
						$to = $_POST['NumeroDetalleEditar'] + 1;
						for ($i=0; $i < $to; $i++) { 
							# code...
							if(isset($_POST['NuevoNombreDetalle_'.$i]) && !empty($_POST['NuevoNombreDetalle_'.$i])){
								$datosNew = array(
									'ci_nombre' => $_POST['NuevoNombreDetalle_'.$i],
									'ci_identificacion' => $_POST['NuevoCedulaDetalle_'.$i],
									'ci_periodo' => $_POST['NuevoPeriodoDetalle_'.$i],
									'ci_valor' => $_POST['NuevoValorDetalle_'.$i],
									'ci_car_id' => $idCartera
								);
								$newRespuesta = ModeloCartera::mdlIngresarDetalleCartera('gi_cartera_incapacidades', $datosNew);

								//var_dump($newRespuesta);
							}
						}
					}

					echo json_encode( array('code' => 1, 'desc' => "Cartera registrada correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "Cartera no registrada"));
				}
			}
		}


		/**
		*Desc.  => ELiminar los datos de una cartera
		*params => id_Cartera
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrBorrarCartera(){

			if(isset($_POST["id_Cartera"])){
				$tabla ="gi_cartera";
				$datos = $_POST["id_Cartera"];
				$respuesta = ModeloCartera::mdlBorrarCartera($tabla, $datos);
				self::setAuditoria('ControladorCartera', 'ctrBorrarCartera',$datos,$respuesta);
				if($respuesta == "ok"){
					echo json_encode( array('code' => 1, 'desc' => "Cartera eliminada correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "Cartera no eliminada"));
				}	
			}
		}	
	}