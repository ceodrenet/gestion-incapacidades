<?php
	/**
	* Clientes
	*/
	class ControladorClientes extends ControladorPlantilla
	{
		/**
		*Desc.  => Mostrar todas los clientes o uno solo depende que se envie en los parametros
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarClientes($item, $valor){
			$tabla = "gi_empresa";
			$respuesta = ModeloClientes::getDatos($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar todas los clientes
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarClientesExternos($item, $valor){
			$tabla = "gi_empresa";
			$respuesta = ModeloClientes::getDatos($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar info de un cliente
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarClientes_Sessions($item, $valor){
			$tabla = "gi_empresa";
			$respuesta = ModeloClientes::getDatos($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar las empresas con mas incapacidades TOP 10
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarClientesLimit($item, $valor){
			$tabla = "gi_empresa";
			$respuesta = ModeloClientes::mdlMostrarClientesLimit($tabla, $item, $valor);
			return $respuesta;
		}


		/**
		*Desc.  => Crear datos de cliente
		*params => Llega es un FormData
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrCrearCliente(){
			if(isset($_POST['NuevoNombre'])){
				
				$ruta =  "";
	            $ruta_2 =  "";
	            $ruta_3 =  "";
	            $ruta_4 =  "";
	        
	            if(isset($_FILES['NuevoRutCliente']['tmp_name']) && !empty($_FILES['NuevoRutCliente']['tmp_name']) ){
	                $ruta = self::putImage($_FILES['NuevoRutCliente']['tmp_name'], $_FILES["NuevoRutCliente"]["type"] , __DIR__."/../vistas/img/clientes/".$_POST['NuevoNit']."-".$_POST['NuevoNitVerificacion'], 'vistas/img/clientes/'.$_POST['NuevoNit']."-".$_POST['NuevoNitVerificacion']);
	            }

	            if(isset($_FILES['NuevoCamaraDeComercionFoto']['tmp_name']) && !empty($_FILES['NuevoCamaraDeComercionFoto']['tmp_name']) ){
	                $ruta_2 = self::putImage($_FILES['NuevoCamaraDeComercionFoto']['tmp_name'], $_FILES["NuevoCamaraDeComercionFoto"]["type"] , __DIR__."/../vistas/img/clientes/".$_POST['NuevoNit']."-".$_POST['NuevoNitVerificacion'], 'vistas/img/clientes/'.$_POST['NuevoNit']."-".$_POST['NuevoNitVerificacion']);
	            }

	            if(isset($_FILES['NuevoCedulaRepresentante']['tmp_name']) && !empty($_FILES['NuevoCedulaRepresentante']['tmp_name']) ){
	                $ruta = self::putImage($_FILES['NuevoCedulaRepresentante']['tmp_name'], $_FILES["NuevoCedulaRepresentante"]["type"] , __DIR__."/../vistas/img/clientes/".$_POST['NuevoNit']."-".$_POST['NuevoNitVerificacion'], 'vistas/img/clientes/'.$_POST['NuevoNit']."-".$_POST['NuevoNitVerificacion']);
	            }

	            if(isset($_FILES['NuevoCuentaBancaria']['tmp_name']) && !empty($_FILES['NuevoCuentaBancaria']['tmp_name']) ){
	                $ruta = self::putImage($_FILES['NuevoCuentaBancaria']['tmp_name'], $_FILES["NuevoCuentaBancaria"]["type"] , __DIR__."/../vistas/img/clientes/".$_POST['NuevoNit']."-".$_POST['NuevoNitVerificacion'], 'vistas/img/clientes/'.$_POST['NuevoNit']."-".$_POST['NuevoNitVerificacion']);
	            }

				$nuevoFechaPagoSeguridad = null;
				if (isset($_POST['NuevoFechaPagoSeguridad']) && !empty($_POST['NuevoFechaPagoSeguridad'])) {
					$nuevoFechaPagoSeguridad = $_POST['NuevoFechaPagoSeguridad'];
				}
				$nuevoFechaVenCamarac = null;
				if (isset($_POST['NuevoFechaVenCamarac']) && !empty($_POST['NuevoFechaVenCamarac'])) {
					$nuevoFechaVenCamarac = $_POST['NuevoFechaVenCamarac'];
				}
				$nuevoFechaVenCuentab = null;
				if (isset($_POST['NuevoFechaVenCuentab']) && !empty($_POST['NuevoFechaVenCuentab'])) {
					$nuevoFechaVenCamarac = $_POST['NuevoFechaVenCuentab'];
				}
	            
	            $NuevoLiquidacionAutomatica = 0;
	            if(isset($_POST['NuevoLiquidacionAutomatica'])){
	            	$NuevoLiquidacionAutomatica = 1;
	            }

				$nuevoPorcentajeGananciaInc = 0;
				if (isset($_POST['NuevoPorcentajeGananciaInc']) && !empty($_POST['NuevoPorcentajeGananciaInc'])) {
					$nuevoPorcentajeGananciaInc = $_POST['NuevoPorcentajeGananciaInc'];
				}
	            
				$tabla = "gi_empresa";
				$datos = array(
							'emp_nombre' 				=> $_POST['NuevoNombre'], 
							'emp_direccion' 			=> $_POST['NuevoDireccion'],
							'emp_nit'  					=> $_POST['NuevoNit']."-".$_POST['NuevoNitVerificacion'],
							'emp_telefono'				=> $_POST['NuevoTelefono'],
							'emp_email' 				=> $_POST['NuevoEmail'],
							'emp_representante'  		=> $_POST['NuevoRepresentante'],
							'emp_cc_representante'		=> $_POST['NuevoCCRepresentante'],
							'emp_fecha_pago_seguridad' 	=> $nuevoFechaPagoSeguridad,
							'emp_contacto_gestion' 		=> $_POST['NuevoContacto'],
							'emp_celular'				=> $_POST['NuevoCelular'],

							'emp_ruta_camara_comercio'			=> $ruta_2,
							'emp_ruta_cedula_representante' 	=> $ruta_3,
							'emp_ruta_cuenta_bancaria'  		=> $ruta_4,
							'emp_ruta_rut'				=> $ruta,

							'emp_email_gestion' 		=> $_POST['NuevoEmailGestion'],
							'emp_contacto_tesoreria' 	=> $_POST['NuevoContactoTesoreria'],
							'emp_telefono_tesoreria'	=> $_POST['NuevoTelefonoTesoreria'].' ext '.$_POST['NuevoTelefonoTesoreriaExt'],
							'emp_email_tesoreria'		=> $_POST['NuevoEmailTesoreria'],
							'emp_telefono_gestion' 		=> $_POST['NuevoTelefonoGestion'].' ext '.$_POST['NuevoTelefonoGestionExt'],
							'emp_celular_tesoreria' 	=> $_POST['NuevoCelularTesoreria'],
							'emp_liquidacion_automatica_i' => $NuevoLiquidacionAutomatica,
							'emp_porcentaje_i'			=> $nuevoPorcentajeGananciaInc,
							'emp_correo_1'	=> $_POST['NuevoCorreoContacto1'],
							'emp_correo_2'	=> $_POST['NuevoCorreoContacto2'],
							'emp_correo_3'	=> $_POST['NuevoCorreoContacto3'],
							'emp_correo_4'	=> $_POST['NuevoCorreoContacto4'],
							'emp_correo_incapacidades_v' => $_POST['emp_correo_incapacidades_v'],
							'emp_crv_v'  => $_POST['emp_crv_v'],
							// -- DWY -- Nuevos campos para alerta de vencimiento
							'emp_fecha_venc_cam_com_d'  => $nuevoFechaVenCamarac,
							'emp_fecha_venc_cert_ban_d'  => $nuevoFechaVenCuentab
						);

				$respuesta = ModeloClientes::mdlIngresarCliente($tabla, $datos);
				ControladorPlantilla::setAuditoria('ControladorClientes', 'ctrCrearCliente',json_encode($datos),$respuesta);
				if($respuesta == 'ok'){
					return json_encode( array('code' => 1, 'desc' => "Cliente registrado correctamente"));
				}else{
					return json_encode( array('code' => 0, 'desc' => "Cliente no registrado"));
				}
			}
		}

		/**
		*Desc.  => Crear datos de cliente
		*params => Llega es un FormData
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrEditarClientes(){
			if(isset($_POST['EditarNombre'])){	

				$ruta =  $_POST['rutarutActual'];
	            $ruta_2 = $_POST['rutaCamaracomercioActual'];
	            $ruta_3 = $_POST['rutaCedulaRepresentanteActual'];
	            $ruta_4 = $_POST['rutaCuentaBancariaActual'];


	            if(isset($_FILES['EditarRutCliente']['tmp_name']) && !empty($_FILES['EditarRutCliente']['tmp_name']) ){
	                $ruta = self::putImage($_FILES['EditarRutCliente']['tmp_name'], $_FILES["EditarRutCliente"]["type"] , __DIR__."/../vistas/img/clientes/".$_POST['EditarNit']."-".$_POST['EditarNitVerificacion'], 'vistas/img/clientes/'.$_POST['EditarNit']."-".$_POST['EditarNitVerificacion']);
	            }

	            if(isset($_FILES['EditarCamaraDeComercionFoto']['tmp_name']) && !empty($_FILES['EditarCamaraDeComercionFoto']['tmp_name']) ){
	                $ruta_2 = self::putImage($_FILES['EditarCamaraDeComercionFoto']['tmp_name'], $_FILES["EditarCamaraDeComercionFoto"]["type"] , __DIR__."/../vistas/img/clientes/".$_POST['EditarNit']."-".$_POST['EditarNitVerificacion'], 'vistas/img/clientes/'.$_POST['EditarNit']."-".$_POST['EditarNitVerificacion']);
	            }

	            if(isset($_FILES['EditarCedulaRepresentante']['tmp_name']) && !empty($_FILES['EditarCedulaRepresentante']['tmp_name']) ){
	                $ruta_3 = self::putImage($_FILES['EditarCedulaRepresentante']['tmp_name'], $_FILES["EditarCedulaRepresentante"]["type"] , __DIR__."/../vistas/img/clientes/".$_POST['EditarNit']."-".$_POST['EditarNitVerificacion'], 'vistas/img/clientes/'.$_POST['EditarNit']."-".$_POST['EditarNitVerificacion']);
	            }

	            if(isset($_FILES['EditarCuentaBancaria']['tmp_name']) && !empty($_FILES['EditarCuentaBancaria']['tmp_name']) ){
	                $ruta_4 = self::putImage($_FILES['EditarCuentaBancaria']['tmp_name'], $_FILES["EditarCuentaBancaria"]["type"] , __DIR__."/../vistas/img/clientes/".$_POST['EditarNit']."-".$_POST['EditarNitVerificacion'], 'vistas/img/clientes/'.$_POST['EditarNit']."-".$_POST['EditarNitVerificacion']);
	            }

	          
	            $EditarLiquidacionAutomatica = 0;
	            if(isset($_POST['EditarLiquidacionAutomatica'])){
	            	$EditarLiquidacionAutomatica = 1;
	            }

				$editarFechaPagoSeguridad = null;
				if (isset($_POST['EditarFechaPagoSeguridad']) && !empty($_POST['EditarFechaPagoSeguridad'])) {
					$editarFechaPagoSeguridad = $_POST['EditarFechaPagoSeguridad'];
				}

				$editarFechaVenCamarac = null;
				if (isset($_POST['EditarFechaVenCamarac']) && !empty($_POST['EditarFechaVenCamarac'])) {
					$editarFechaPagoSeguridad = $_POST['EditarFechaVenCamarac'];
				}

				$editarFechaVenCuentab = null;
				if (isset($_POST['EditarFechaVenCuentab']) && !empty($_POST['EditarFechaVenCuentab'])) {
					$editarFechaVenCuentab = $_POST['EditarFechaVenCuentab'];
				}

				$tabla = "gi_empresa";
				$datos = array(
							'emp_nombre' 				=> $_POST['EditarNombre'], 
							'emp_direccion' 			=> $_POST['EditarDireccion'],
							'emp_nit'  					=> $_POST['EditarNit']."-".$_POST['EditarNitVerificacion'],
							'emp_telefono'				=> $_POST['EditarTelefono'],
							'emp_email' 				=> $_POST['EditarEmail'],
							'emp_representante'  		=> $_POST['EditarRepresentante'],
							'emp_cc_representante'		=> $_POST['EditarCCRepresentante'],
							'emp_fecha_pago_seguridad' 	=> $editarFechaPagoSeguridad,
							'emp_contacto_gestion' 		=> $_POST['EditarContacto'],
							'emp_celular'				=> $_POST['EditarCelular'],

							'emp_ruta_camara_comercio'			=> $ruta_2,
							'emp_ruta_cedula_representante' 	=> $ruta_3,
							'emp_ruta_cuenta_bancaria'  		=> $ruta_4,
							'emp_ruta_rut'				=> $ruta,

							'emp_email_gestion' 		=> $_POST['EditarEmailGestion'],
							'emp_contacto_tesoreria' 	=> $_POST['EditarContactoTesoreria'],
							'emp_telefono_tesoreria'	=> $_POST['EditarTelefonoTesoreria'].' ext '.$_POST['EditarTelefonoTesoreriaExt'],
							'emp_email_tesoreria'		=> $_POST['EditarEmailTesoreria'],
							'emp_telefono_gestion' 		=> $_POST['EditarTelefonoGestion'].' ext '.$_POST['EditarTelefonoGestionExt'],
							'emp_celular_tesoreria' 	=> $_POST['EditarCelularTesoreria'],
							'emp_id'					=> $_POST['Editar_idEmpresa'],
							'emp_liquidacion_automatica_i' => $EditarLiquidacionAutomatica,
							'emp_porcentaje_i'					=> $_POST['EditarPorcentajeGananciaInc'],
							'emp_correo_1'	=> $_POST['EditarCorreoContacto1'],
							'emp_correo_2'	=> $_POST['EditarCorreoContacto2'],
							'emp_correo_3'	=> $_POST['EditarCorreoContacto3'],
							'emp_correo_4'	=> $_POST['EditarCorreoContacto4'],
							'emp_correo_incapacidades_v' => $_POST['emp_correo_incapacidades_v'],
							'emp_crv_v'  => $_POST['emp_crv_v'],
							 // -- DWY -- Nuevos campos para alerta de vencimiento
							'emp_fecha_venc_cam_com_d'  => $editarFechaVenCamarac,
							'emp_fecha_venc_cert_ban_d'  => $editarFechaVenCuentab
						);
					
				$respuesta = ModeloClientes::mdlEditarClientes($tabla, $datos);
				ControladorPlantilla::setAuditoria('ControladorClientes', 'ctrEditarClientes',json_encode($datos),$respuesta);
				if($respuesta == 'ok'){
					return json_encode( array('code' => 1, 'desc' => "Cliente editado correctamente"));
				}else{
					return json_encode( array('code' => 0, 'desc' => "Cliente no editado"));
				}
			}
		}


		/**
		*Desc.  => eliminar datos de un cliente
		*params => Llega es un FormData
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrBorrarCliente(){

			if(isset($_POST["borrarCliente"])){
				$tabla ="gi_empresa";
				$datos = $_POST["borrarCliente"];
				$respuestaIncapacidades = ModeloClientes::mdlBorrar('gi_incapacidad', 'inc_empresa = '.$datos);
				$respuestaEmpleados = ModeloClientes::mdlBorrar('gi_empleados', 'emd_emp_id = '.$datos);
				$respuestaUsuariosEps = ModeloClientes::mdlBorrar('gi_usuarios_eps', 'use_emp_id_i = '.$datos);
				$respuestaUsuarios = ModeloClientes::mdlBorrar('gi_usuario', 'user_cliente_id = '.$datos);
				$respuestaUsuarios = ModeloClientes::mdlBorrar('gi_incapacidades_nomina', 'incn_emp_id = '.$datos);
				$respuestaUsuarios = ModeloClientes::mdlBorrar('gi_empresa_usuarios', 'emu_id_empresa = '.$datos);
				$respuestaUsuarios = ModeloClientes::mdlBorrar('gi_cartera', 'car_emp_id = '.$datos);
				$respuestaUsuarios = ModeloClientes::mdlBorrar('gi_archivos', 'arc_empresa_id_i = '.$datos);
				$respuesta = ModeloClientes::mdlBorrarCliente($tabla, $datos);
				ControladorPlantilla::setAuditoria('ControladorClientes', 'ctrBorrarCliente',$datos,$respuesta);
				if($respuesta == "ok"){
					return json_encode( array('code' => 1, 'desc' => "Cliente borrado correctamente"));
				}else{
					return json_encode( array('code' => 0, 'desc' => "Cliente no borrado"));
				}	
			}
		}	
	}