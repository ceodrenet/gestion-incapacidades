<?php
	class ControladorEmpleados extends ControladorPlantilla
	{
		/**
		*Desc.  => Mostrar todas los empleados o el dato de uno solo
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarEmpleados($item, $valor){
			$tabla = "gi_empleados";
			$respuesta = ModeloEmpleados::mdlMostrarEmpleados($tabla, $item, $valor);
			return $respuesta;
		}


		/**
		*Desc.  => Mostrar todas los empleados por nombres
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarNombres($item, $valor){
			$tabla = "gi_empleados";
			$respuesta = ModeloEmpleados::mdlMostrarNombres($tabla, $item, $valor);
			return $respuesta;
		}


		/**
		*Desc.  => Mostrar datos de empleados y su empresa + EPS
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarEmpleados_incapacidad($item, $valor, $empresa){
			$tabla = "gi_empleados";
			$respuesta = ModeloEmpleados::mdlMostrarEmpleados_incapacidad($tabla, $item, $valor, $empresa);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar datos de empleados y su EPS
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarEmpleadosTotales(){
			$tabla = "gi_empleados";
			$respuesta = ModeloEmpleados::mdlMostrarEmpleadosTotales($tabla);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar datos de empleados filtrado por su empresa, trae la EPS
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarEmpleados_ByEmpresa($item, $valor){
			$tabla = "gi_empleados";
			$respuesta = ModeloEmpleados::mdlMostrarEmpleados_byEmpresa($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar datos de empleados filtrado por su empresa no trae la eps , solo los codigos y trae solo uno solo valor
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarEmpleados_ByEmpresaX2($item, $valor, $valor2){
			$tabla = "gi_empleados";
			$respuesta = ModeloEmpleados::mdlMostrarEmpleadosX2($tabla, $item, $valor, $valor2);
			return $respuesta;
		}


		/**
		*Desc.  => Crear datos de un empleado
		*params => Llega es un FormData
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		//RFB---- Se han adicionado dos campos para validar suspencion de salario 01062022
		static public function ctrCrearEmpleado(){
			if(isset($_POST['NuevoNombre'])){
				if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["NuevoNombre"]) &&
				   preg_match('/^[a-zA-Z0-9-]+$/', $_POST["NuevoCedula"]))
				{	
					
					/* cargue de fotos */
					$ruta =  "";
		            $ruta_2 =  "";
					
		            if(isset($_FILES['NuevoCedulaEmpleado']['tmp_name']) && !empty($_FILES['NuevoCedulaEmpleado']['tmp_name']) ){
		                $ruta = "";
						//selft::putImage($_FILES['NuevoCedulaEmpleado']['tmp_name'], $_FILES["NuevoCedulaEmpleado"]["type"] , __DIR__."/../vistas/img/empleados/".$_POST['NuevoCedula'], 'vistas/img/empleados/'.$_POST['NuevoCedula']);
		            }
					
		            if(isset($_FILES['NuevoOtroDocumento']['tmp_name']) && !empty($_FILES['NuevoOtroDocumento']['tmp_name']) ){
		                $ruta_2 = "";
						//selft::putImage($_FILES['NuevoOtroDocumento']['tmp_name'], $_FILES["NuevoOtroDocumento"]["type"] , __DIR__."/../vistas/img/empleados/".$_POST['NuevoCedula'], 'vistas/img/empleados/'.$_POST['NuevoCedula']);
		            }
					//die(var_dump($_FILES));
		            $casoEspecial = 0;
		            if(isset($_POST['NuevoCasosEspeciales'])){
		            	$casoEspecial = 1;
		            }

					$suspSalario = 0;
		            if(isset($_POST['NuevoSuspSalario'])){
		            	$suspSalario = 1;
		            }

		            $fechaSusP = null;
		            if(isset($_POST['NuevoFechaSusp'])){
		            	$fechaSusP = $_POST['NuevoFechaSusp'];
		            }

					$tabla = "gi_empleados";
					$datos = array(
								'emd_nombre' 				=> $_POST['NuevoNombre'], 
								'emd_cedula' 				=> $_POST['NuevoCedula'],
								'emd_fecha_ingreso'  		=> $_POST['NuevoFechaIngreso'],
								'emd_salario'				=> $_POST['NuevoSalario'],
								'emd_emp_id'				=> $_POST['Nuevo_idEmpresa'],
								'emd_tipo_identificacion' 	=> $_POST['NuevoTipoIdentificacion'],
								'emd_fecha_retiro'			=> $_POST['NuevoFechaRetiro'],
								'emd_salario_promedio'		=> $_POST['NuevoSalarioPromedio'],
								'emd_fecha_nacimiento'  	=> $_POST['NuevoFechaNacimiento'],
								'emd_eps_id'				=> $_POST['NuevoEps'],
								'emd_fecha_afiliacion_eps' 	=> $_POST['NuevoFechaAfiliacionEps'],
								'emd_afp_id'				=> $_POST['NuevoAfp'],
								'emd_arl_id'				=> $_POST['NuevoArl'],
								'emd_fecha_calificacion_PCL'	=> $_POST['NuevoFechaCalificacionPCL'],
								'emd_entidad_calificadora'	=> $_POST['NuevoEntidadCalificadora'],
								'emd_diagnostico'			=> $_POST['NuevoDiagnostico'],
								'emd_cargo'					=> $_POST['NuevoCargo'],
								'emd_sede'					=> $_POST['NuevoSede'],
								'emd_ruta_cedula'			=> $ruta,
								'emd_ruta_otro'				=> $ruta_2,
								'emd_tipo_empleado'			=> $_POST['NuevoTipoEmpleado'],
								'emd_genero'				=> $_POST['NuevoGenero'],
								'emd_codigo_nomina'			=> $_POST['NuevoCodigoNomina'],
								'emd_centro_costos'			=> $_POST['NuevoCentroDecostos'],
								'emd_ciudad'				=> $_POST['NuevoCiudadNomina'],
								'emd_subcentro_costos'		=> $_POST['NuevoSubcentroCostos'],
								'emd_estado'				=> $_POST['NuevoEstado'],
								'emd_telefono_v'			=> $_POST['NuevoNumeroTelefonico'],
								'emd_correo_v'				=> $_POST['NuevoCorreo'],
								'emd_susp_salario_i'		=> $suspSalario,
								'emd_fech_susp_salario'		=> $fechaSusP,
								'emd_caso_especial_i'		=> $casoEspecial

							);

					
					$respuesta = ModeloEmpleados::mdlIngresarEmpleados_return($tabla, $datos);
					self::setAuditoria('ControladorEmpleados', 'ctrCrearEmpleado',json_encode($datos),$respuesta);
					if($respuesta != 'error'){
						/* Entonces vamos a ver que le metemos */
						if(isset($_POST['NuevoCasosEspeciales'])){
							$campos = "cae_emd_id_i, cae_con_rea_v, cae_xon_fecha_d, cae_condia_id_i, cae_con_entidad_id_i, cae_cal_v, cae_cal_fecha_d, cae_cal_dia_id_i, cae_cal_entidad_i, cae_cal_jur_v, cae_cal_jur_fecha_d, cae_cal_jur_dia_id_i, cae_cal_jur_entidad_v, cae_cal_jun_v, cae_cal_jun_fecha_d, cae_cal_jun_dia_id_i, cae_cal_jun_entidad_v, cae_tic_id_i";

							$valore = $respuesta;

							if($_POST['NuevoConceptoRehabilitacion'] !=''){ $valore.= ", '".$_POST['NuevoConceptoRehabilitacion']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoFechaConcepto'] !=''){ $valore.= ", '".$_POST['NuevoFechaConcepto']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoDiagnosticoConcepto'] !=''){ $valore.= ", '".$_POST['NuevoDiagnosticoConcepto']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoEntidadConcepto'] !=''){ $valore.= ", '".$_POST['NuevoEntidadConcepto']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoCalificacionPCLCS'] !=''){ $valore.= ", '".$_POST['NuevoCalificacionPCLCS']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoFechaCaliciacion'] !=''){ $valore.= ", '".$_POST['NuevoFechaCaliciacion']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoDiagnosticoPCL'] !=''){ $valore.= ", '".$_POST['NuevoDiagnosticoPCL']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoEntidadPCL'] !=''){ $valore.= ", '".$_POST['NuevoEntidadPCL']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoJuntaRegional'] !=''){ $valore.= ", '".$_POST['NuevoJuntaRegional']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoFechaCalificacionJR'] !=''){ $valore.= ", '".$_POST['NuevoFechaCalificacionJR']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoDiagnosticoJR'] !=''){ $valore.= ", '".$_POST['NuevoDiagnosticoJR']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoEntidadJR'] !=''){ $valore.= ", '".$_POST['NuevoEntidadJR']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoCalificacionJN'] !=''){ $valore.= ", '".$_POST['NuevoCalificacionJN']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoFechaCalificacionJN'] !=''){ $valore.= ", '".$_POST['NuevoFechaCalificacionJN']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoDiagnosticoJN'] !=''){ $valore.= ", '".$_POST['NuevoDiagnosticoJN']."'"; }else{$valore .= ", NULL"; }
							if($_POST['NuevoEntidadJN'] !=''){ $valore.= ", '".$_POST['NuevoEntidadJN']."'"; }else{$valore .= ", NULL"; }
							if($_POST["NuevoTipoDecaso"] != ''){ $valore.= ", '".$_POST['NuevoTipoDecaso']."'"; }else{$valore .= ", NULL"; }

							$respuestaCE = Modeloclientes::mdlCrear("gi_casos_especiales", $campos, $valore);

							if($respuestaCE == 'Error'){
								return json_encode( array('code' => 1, 'desc' => "No registro el dato de especiales, pero si registro el empleado "));
							}
						}
						return json_encode( array('code' => 1, 'desc' => "Empleado registrado correctamente"));
					}else{
						return json_encode( array('code' => 0, 'desc' => "Empleado no registrado"));
					}

				}else{
					return json_encode( array('code' => -1, 'desc' => "No puede ir vacio el nombre del empleado"));
				}
			}
		}

		/**
		*Desc.  => Editar datos de un empleado
		*params => Llega es un FormData
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		//RFB---- Se han adicionado dos campos para validar suspencion de salario 01062022
		static public function ctrEditarEmpleados(){
			if(isset($_POST['EditarNombre'])){
				if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["EditarNombre"]) &&
				   preg_match('/^[a-zA-Z0-9-]+$/', $_POST["EditarCedula"]))
				{	
					/* cargue de fotos */
					$ruta =  $_POST['ruta_cedula'];
		            $ruta_2 =  $_POST['ruta_otroDocumento'];

		            if(isset($_FILES['EditarCedulaEmpleado']['tmp_name']) && !empty($_FILES['EditarCedulaEmpleado']['tmp_name']) ){
		                $ruta = selft::putImage($_FILES['EditarCedulaEmpleado']['tmp_name'], $_FILES["EditarCedulaEmpleado"]["type"] , __DIR__."/../vistas/img/empleados/".$_POST['EditarCedulaEmpleado'], 'vistas/img/empleados/'.$_POST['EditarCedulaEmpleado']);
		            }

		            if(isset($_FILES['EditarOtroDocumento']['tmp_name']) && !empty($_FILES['EditarOtroDocumento']['tmp_name']) ){
		                $ruta_2 = selft::putImage($_FILES['EditarOtroDocumento']['tmp_name'], $_FILES["EditarOtroDocumento"]["type"] , __DIR__."/../vistas/img/empleados/".$_POST['EditarCedulaEmpleado'], 'vistas/img/empleados/'.$_POST['EditarCedulaEmpleado']);
		            }

		            $strRespuestaEmp = ModeloEmpleados::mdlMostrarUnitario("emd_emp_id","gi_empleados","emd_id = ".$_POST['Editar_idEmpleado']);

		            $casoEspecial = 0;
		            if(isset($_POST['EditarCasosEspeciales'])){
		            	$casoEspecial = 1;
		            }

					$suspSalario = 0;
		            if(isset($_POST['EditarSuspSalario'])){
		            	$suspSalario = 1;
		            }

		            $fechaSusP = null;
		            if(isset($_POST['EditarFechaSusp'])){
		            	$fechaSusP = $_POST['EditarFechaSusp'];
		            }

					
					$tabla = "gi_empleados";
					$datos = array(
								'emd_nombre' 			=> $_POST['EditarNombre'], 
								'emd_cedula' 			=> $_POST['EditarCedula'],
								'emd_fecha_ingreso'  	=> $_POST['EditarFechaIngreso'],
								'emd_salario'			=> $_POST['EditarSalario'],
								'emd_emp_id'			=> $strRespuestaEmp['emd_emp_id'],
								'emd_id'				=> $_POST['Editar_idEmpleado'],
								'emd_tipo_identificacion' 	=> $_POST['EditarTipoIdentificacion'],
								'emd_fecha_retiro'			=> $_POST['EditarFechaRetiro'],
								'emd_salario_promedio'		=> $_POST['EditarSalarioPromedio'],
								'emd_fecha_nacimiento'  	=> $_POST['EditarFechaNacimiento'],
								'emd_eps_id'				=> $_POST['EditarEps'],
								'emd_fecha_afiliacion_eps' 	=> $_POST['EditarFechaAfiliacionEps'],
								'emd_afp_id'				=> $_POST['EditarAfp'],
								'emd_arl_id'				=> $_POST['EditarArl'],
								'emd_fecha_calificacion_PCL'	=> $_POST['EditarFechaCalificacionPCL'],
								'emd_entidad_calificadora'	=> $_POST['EditarEntidadCalificadora'],
								'emd_diagnostico'			=> $_POST['EditarDiagnostico'],
								'emd_cargo'					=> $_POST['EditarCargo'],
								'emd_sede'					=> $_POST['EditarSede'],
								'emd_ruta_cedula'			=> $ruta,
								'emd_ruta_otro'				=> $ruta_2,
								'emd_tipo_empleado'			=> $_POST['EditarTipoEmpleado'],
								'emd_genero'				=> $_POST['EditarGenero'],
								'emd_codigo_nomina'			=> $_POST['EditarCodigoNomina'],
								'emd_centro_costos'			=> $_POST['EditarCentroDecostos'],
								'emd_ciudad'				=> $_POST['EditarCiudadNomina'],
								'emd_subcentro_costos'		=> $_POST['EditarSubcentroCostos'],
								'emd_estado'				=> $_POST['EditarEstado'],
								'emd_telefono_v'			=> $_POST['EditarTelefono'],
								'emd_correo_v'				=> $_POST['EditarCorreo'],
								'emd_susp_salario_i'		=> $suspSalario,
								'emd_fech_susp_salario'		=> $fechaSusP,
								'emd_caso_especial_i'		=> $casoEspecial
							);
							//die(var_dump($datos));
					$respuesta = ModeloEmpleados::mdlEditarEmpleados($tabla, $datos);
					self::setAuditoria('ControladorEmpleados', 'ctrEditarEmpleados',json_encode($datos),$respuesta);
					if($respuesta == 'ok'){

						/**** Hacemos la vaina de los casos especiales ****/
						if(isset($_POST['EditarCasosEspeciales'])){ //Ya con esta validacion sabes mo si llego o no el check, si no viene el no hace nada
							/* El man es un caso especial */
							ModeloEmpleados::mdlBorrar("gi_casos_especiales", 'cae_emd_id_i = '.$_POST['Editar_idEmpleado']);
							/* Entonces vamos a ver que le metemos */
							$campos = "cae_emd_id_i, cae_con_rea_v, cae_xon_fecha_d, cae_condia_id_i, cae_con_entidad_id_i, cae_cal_v, cae_cal_fecha_d, cae_cal_dia_id_i, cae_cal_entidad_i, cae_cal_jur_v, cae_cal_jur_fecha_d, cae_cal_jur_dia_id_i, cae_cal_jur_entidad_v, cae_cal_jun_v, cae_cal_jun_fecha_d, cae_cal_jun_dia_id_i, cae_cal_jun_entidad_v, cae_tic_id_i";

							$valore = $_POST['Editar_idEmpleado'];

							if($_POST['EditarConceptoRehabilitacion'] !=''){ $valore.= ", '".$_POST['EditarConceptoRehabilitacion']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarFechaConcepto'] !=''){ $valore.= ", '".$_POST['EditarFechaConcepto']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarDiagnosticoConcepto'] !=''){ $valore.= ", '".$_POST['EditarDiagnosticoConcepto']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarEntidadConcepto'] !=''){ $valore.= ", '".$_POST['EditarEntidadConcepto']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarCalificacionPCLCS'] !=''){ $valore.= ", '".$_POST['EditarCalificacionPCLCS']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarFechaCaliciacion'] !=''){ $valore.= ", '".$_POST['EditarFechaCaliciacion']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarDiagnosticoPCL'] !=''){ $valore.= ", '".$_POST['EditarDiagnosticoPCL']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarEntidadPCL'] !=''){ $valore.= ", '".$_POST['EditarEntidadPCL']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarJuntaRegional'] !=''){ $valore.= ", '".$_POST['EditarJuntaRegional']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarFechaCalificacionJR'] !=''){ $valore.= ", '".$_POST['EditarFechaCalificacionJR']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarDiagnosticoJR'] !=''){ $valore.= ", '".$_POST['EditarDiagnosticoJR']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarEntidadJR'] !=''){ $valore.= ", '".$_POST['EditarEntidadJR']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarCalificacionJN'] !=''){ $valore.= ", '".$_POST['EditarCalificacionJN']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarFechaCalificacionJN'] !=''){ $valore.= ", '".$_POST['EditarFechaCalificacionJN']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarDiagnosticoJN'] !=''){ $valore.= ", '".$_POST['EditarDiagnosticoJN']."'"; }else{$valore .= ", NULL"; }
							if($_POST['EditarEntidadJN'] !=''){ $valore.= ", '".$_POST['EditarEntidadJN']."'"; }else{$valore .= ", NULL"; }
							if($_POST["NuevoTipoDecasoEdicion"] != ''){ $valore.= ", '".$_POST['NuevoTipoDecasoEdicion']."'"; }else{$valore .= ", NULL"; }
							
							$respuestaCE = ModeloEmpleados::mdlCrear("gi_casos_especiales", $campos, $valore);

							if($respuestaCE == 'Error'){
								return json_encode( array('code' => 1, 'desc' => "No registro el dato de especiales, pero si edito el empleado "));
							}
						}else{
							/*El man o viene de casos especiales y lo quitaron o algo por el estilo*/
							/* El man ya no sera un caso especial */
							ModeloEmpleados::mdlBorrar("gi_casos_especiales", 'cae_emd_id_i = '.$_POST['Editar_idEmpleado']);
							/*Listo, ya se borra enseguida*/
							/*\(o__0)/*/ 
						}
						return json_encode( array('code' => 1, 'desc' => "Empleado edito correctamente"));

						
					}

				}else{
					return json_encode( array('code' => -1, 'desc' => "No puede ir vacio el nombre del empleado"));
				}
			}
		}


		/**
		*Desc.  => Eliminar datos de un empleado
		*params => Llega es un FormData
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrBorrarEmpleado(){

			if(isset($_POST["id_Empleado"])){
				$tabla ="gi_empleados";
				$datos = $_POST["id_Empleado"];
				$respuesta = ModeloEmpleados::mdlBorrarEmpleado($tabla, $datos);
				self::setAuditoria('ControladorEmpleados', 'ctrEditarEmpleados',$datos,$respuesta);
				if($respuesta == "ok"){
					return json_encode( array('code' => 1, 'desc' => "Empleado eliminado correctamente"));
				}else{
					return json_encode( array('code' => 0, 'desc' => "Empleado no eliminado"));
				}
			}
		}	

		/**
		*Desc.  => Cargue Massivo de empleados
		*params => Llega es un FormData
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrCargaMaxivaEmpleados(){
			if(isset($_FILES['NuevoEmpleados']['tmp_name']) && !empty($_FILES['NuevoEmpleados']['tmp_name']) ){
				$name   = $_FILES['NuevoEmpleados']['name'];
        		$tname  = $_FILES['NuevoEmpleados']['tmp_name'];
        		$empresa = $_POST['empresa'];
        		ini_set('memory_limit','128M');


        		$obj_excel = PHPExcel_IOFactory::load($tname);
        		$sheetData = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
		        $arr_datos = array();
		        $highestColumm = $obj_excel->setActiveSheetIndex(0)->getHighestColumn(); // e.g. "EL"
				$highestRow = $obj_excel->setActiveSheetIndex(0)->getHighestRow();
				
				$aciertos = 0;
				$fallos   = 0;
				$total = 0;
				$existentes = 0;
				$badFormat = 0;
				foreach ($sheetData as $index => $value) {
            		if ( $index > 1 ){
            			/* si es 1 es porque esta el formato de empleados */
            			
            			if($value['A'] == 1){
            				$valido = 0;
	            			$existe = false;
	            			$separador = '';
	            			$total++;
            				if((!is_null($value['A']) && !empty($value['A'])) && 
	                    		(!is_null($value['B']) && !empty($value['B'])) && 
	                    		(!is_null($value['C']) && !empty($value['C']))
	                		){
	                			/* Iniciamos la consulta Sql */
	                			$datos = array();

	                			/* tipo de identificacion */
	                			if(!is_null($value['B']) && !empty($value['B']) ){
	                				$valido = 1;
	                				$datos['emd_tipo_identificacion'] = $value['B'];
	                			}else{
	                				$datos['emd_tipo_identificacion'] = NULL;
	                			}

	                			/* Identificacion */
	                			if(!is_null($value['C']) && !empty($value['C']) ){
	                				$valido = 1;
	                				$datos['emd_cedula'] = $value['C'];	

	                				$item = 'emd_cedula';
	                				$valuess = $value['C'];
	                				$tabla = 'gi_empleados';
	                				$value2 = $empresa;
	                				$res = ModeloEmpleados::mdlMostrarEmpleadosX2($tabla, $item, $valuess, $value2);
	                				if($res){
	                					if($res['emd_cedula'] == $value['C']){
		                					$existe = true;
		                					$existentes++;
		                					$datos['emd_id'] = $res['emd_id'];
		                				}	
	                				}
	                				
	                			}else{
	                				$datos['emd_cedula'] = NULL;
	                			}

	                			/* Nombres */
	                			if(!is_null($value['D']) && !empty($value['D']) ){
	                				$valido = 1;
	                				$datos['emd_nombre'] = $value['D'];	
	                			}else{
	                				$datos['emd_nombre'] = NULL;
	                			}

	                			/* Fecha Nacimiento */
	                			if(!is_null($value['E']) && !empty($value['E']) ){
	                				$datos['emd_fecha_nacimiento'] = $value['E'];
	                				$valido = 1;
	                			}else{
	                				$datos['emd_fecha_nacimiento'] = NULL;
	                			}

	                			/* Fecha ingreso */
	                			if(!is_null($value['F']) && !empty($value['F']) ){
	                				$datos['emd_fecha_ingreso'] = $value['F'];
	                				$valido = 1;
	                			}else{
	                				$datos['emd_fecha_ingreso'] = NULL;
	                			}

	                			/* Feccha de retiro */
	                			if(!is_null($value['G']) && !empty($value['G']) ){
	                				$valido = 1;
	                				$datos['emd_fecha_retiro'] = $value['G'];
	                			}else{
	                				$datos['emd_fecha_retiro'] = NULL;
	                			}

	                			/* Salario Base */
	                			if(!is_null($value['H']) && !empty($value['H']) ){
	                				$valido = 1;
	                				$datos['emd_salario'] = $value['H'];
	                			}else{
	                				$datos['emd_salario'] = NULL;
	                			}

	                			/* Salario Promedio */
	                			if(!is_null($value['I']) && !empty($value['I']) ){
	                				$valido = 1;
	                				$datos['emd_salario_promedio'] = $value['I'];
	                			}else{
	                				$datos['emd_salario_promedio'] = NULL;
	                			}

	                			/* Cargo */
	                			if(!is_null($value['J']) && !empty($value['J']) ){
	                				$valido = 1;
	                				$datos['emd_cargo'] = $value['J'];
	                			}else{
	                				$datos['emd_cargo'] = NULL;
	                			}

	                			/* Sede */
	                			if(!is_null($value['K']) && !empty($value['K']) ){
	                				$valido = 1;
	                				$datos['emd_sede'] = $value['K'];
	                			}else{
	                				$datos['emd_sede'] = NULL;
	                			}

	                			/* EPS */
	                			if(!is_null($value['L']) && !empty($value['L']) ){
	                				$valido = 1;
	                				$item = 'ips_codigo';
	                				$valuess = $value['L'];
	                				$resp = ControladorIps::ctrMostrarIpsById($item, $valuess);
	                				if($resp){
	                					$datos['emd_eps_id'] = $resp['ips_id'];
	                				}else{
	                					$datos['emd_eps_id'] = NULL;	
	                				}
	                			}else{
	                				$datos['emd_eps_id'] = NULL;
	                			}

	                			/* Feccha Ingreso EPS */
	                			if(!is_null($value['M']) && !empty($value['M']) ){
	                				$valido = 1;
	                				$datos['emd_fecha_afiliacion_eps'] = $value['M'];
	                			}else{
	                				$datos['emd_fecha_afiliacion_eps'] = NULL;
	                			}

	                			/* AFP */
	                			if(!is_null($value['N']) && !empty($value['N']) ){
	                				$valido = 1;
	                				$item = 'ips_codigo';
	                				$valuess = $value['N'];
	                				$resp = ControladorIps::ctrMostrarIpsById($item, $valuess);
	                				if($resp){
	                					$datos['emd_afp_id'] = $resp['ips_id'];
	                				}else{
	                					$datos['emd_afp_id'] = NULL;	
	                				}
	                			}else{
	                				$datos['emd_afp_id'] = NULL;
	                			}

	                			/*ARL */
	                			if(!is_null($value['O']) && !empty($value['O']) ){
	                				$valido = 1;
	                				$item = 'ips_codigo';
	                				$valuess = $value['O'];
	                				$resp = ControladorIps::ctrMostrarIpsById($item, $valuess);
	                				if($resp){
	                					$datos['emd_arl_id'] = $resp['ips_id'];
	                				}else{
	                					$datos['emd_arl_id'] = NULL;	
	                				}
	                			}else{
	                				$datos['emd_arl_id'] = NULL;
	                			}

	                			/*Genero*/
	                			if(!is_null($value['P']) && !empty($value['P']) ){
	                				$valido = 1;
	                				$datos['emd_genero'] = $value['P'];
	                			}else{
	                				$datos['emd_genero'] = NULL;
	                			}

	                			/*Tipo de empleados*/
	                			if(!is_null($value['Q']) && !empty($value['Q']) ){
	                				$valido = 1;
	                				$datos['emd_tipo_empleado'] = $value['Q'];
	                			}else{
	                				$datos['emd_tipo_empleado'] = NULL;
	                			}

	                			/*Tipo de empleados*/
	                			if(!is_null($value['R']) && !empty($value['R']) ){
	                				$valido = 1;
	                				$datos['emd_codigo_nomina'] = $value['R'];
	                			}else{
	                				$datos['emd_codigo_nomina'] = NULL;
	                			}

	                			/*Tipo de empleados*/
	                			if(!is_null($value['S']) && !empty($value['S']) ){
	                				$valido = 1;
	                				$datos['emd_centro_costos'] = $value['S'];
	                			}else{
	                				$datos['emd_centro_costos'] = NULL;
	                			}

	                			/*Tipo de empleados*/
	                			if(!is_null($value['T']) && !empty($value['T']) ){
	                				$valido = 1;
	                				$datos['emd_ciudad'] = $value['T'];
	                			}else{
	                				$datos['emd_ciudad'] = NULL;
	                			}

	                			/*Tipo de empleados*/
	                			if(!is_null($value['U']) && !empty($value['U']) ){
	                				$valido = 1;
	                				$datos['emd_subcentro_costos'] = $value['U'];
	                			}else{
	                				$datos['emd_subcentro_costos'] = NULL;
	                			}
	                			
	                			$tabla = "gi_empleados";							
								if($valido == 1){
									$datos['emd_emp_id'] = $empresa;

									if(!$existe){
										$respuesta = ModeloEmpleados::mdlIngresarEmpleados($tabla, $datos);
									}else{
										$respuesta = ModeloEmpleados::mdlEditarEmpleados($tabla, $datos);
									}
									
									if($respuesta == 'ok'){
										$aciertos++;
									}else{
										$fallos++;
									}
								}
	                		}
            			}
                	}
                }

                return json_encode( array('code' => 1, 
                							'total' => $total, 
                							'Existentes' => $existentes,
                							'Exito' => $aciertos,
                							'Fallaron' => $fallos
                						));
			}
		}

		/*Function para tener los totales de Empleados*/
		static function getTotalEmpleadosByEmpresayTipo($empresa, $condicion){
			return ModeloEmpleados::mdlGetCantidadEmpleadosByEmpresaByTipo($empresa, $condicion)['estado'];
		}				

		static public function ctrBuscarEmpleadosSelect($valor, $idCliente){
			$respuesta = ModeloEmpleados::mdlBuscarEmpleadoBySelect($valor, $idCliente);
			return $respuesta;
		}
	}	