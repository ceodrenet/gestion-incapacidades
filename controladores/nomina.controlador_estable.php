<?php
	/**
	* Archivos
	*/
	class ControladorNomina
	{
		
		/* editar Archivos */
		static public function ctrEditarNomina(){
			if(isset($_POST['idIncapacidadEdicion'])){

				$fechaI = explode('/', $_POST['EditarFechaInicio']);
				$fechaF = explode('/', $_POST['EditarFechaFinal']);

				if(count($fechaI) > 1){
					$fechaI = $fechaI[2].'-'.$fechaI[1].'-'.$fechaI[0];
				}else{
					$fechaI = $fechaI[0];
				}

				if(count($fechaF) > 1){
					$fechaF = $fechaF[2].'-'.$fechaF[1].'-'.$fechaF[0];
				}else{
					$fechaF = $fechaF[0];
				}

				$motivo = 0;
				if(isset($_POST['EditarMotivo'])){
					$motivo = $_POST['EditarMotivo'];
				}

				/*Primero necesito sabewr si esta vaina estaba rechazada*/
				$estado = ModeloTesoreria::mdlMostrarUnitario('incn_estado_v', 'gi_incapacidades_nomina', " incn_id_i = ".$_POST['idIncapacidadEdicion']);

				$campos = "incn_cc_v = '".$_POST['Editarcedula']."', incn_fecha_inicio_d = '".$fechaI."', incn_fecha_fin_d = '".$fechaF."', incn_estado_v= '".$_POST['EditarEstado']."', incn_nomina_i= '".$_POST['EditarNomina']."', incn_anho_i= '".$_POST['EditarAnhoNomina']."', incn_razon_v= '".$motivo."', inc_observacion_v = '".$_POST['txtObservaciones']."' ";
				$condic = " incn_id_i = ".$_POST['idIncapacidadEdicion'];
				$respuesta = ModeloTesoreria::mdlActualizar('gi_incapacidades_nomina', $campos, $condic);
				if($respuesta == "ok"){

					if( $_POST['EditarEstado'] == 'ACEPTADA' && $estado['incn_estado_v'] == 'RECHAZADA' ){
						/*Cambio a aceptada*/
						$item1 = "inc_cc_afiliado";
                        $valor1 = $_POST['Editarcedula'];
                        $item2 = 'inc_fecha_inicio';
                        $valor2 = $fechaI;
                        $existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2, $_SESSION['cliente_id']);
                        $incapacidaDato = null;
                        if(!$existeInc){
                        	//No existe toca insertarla vamos a crearla (y)
	                		$item = 'emd_cedula';
	        				$valuess = $_POST['Editarcedula'];
	        				$tabla = 'gi_empleados';
	        				$res = ModeloEmpleados::mdlMostrarEmpleados($tabla, $item, $valuess);
	        				if($res != null && $res != false){
	        					$campos = "inc_emd_id, inc_empresa, inc_ips_afiliado, inc_cc_afiliado, inc_fecha_inicio, inc_fecha_final, inc_estado, inc_estado_tramite";
	        					$valore = "'".$res['emd_id']."', '".$res['emd_emp_id']."', '".$res['emd_eps_id']."', '".$_POST['Editarcedula_'.$i]."', '".$_POST['EditarFechaInicio_'.$i]."', '".$_POST['EditarFechaFinal_'.$i]."', 1, 'POR RADICAR' ";
	        					$resultadoInca = ModeloTesoreria::mdlInsertar('gi_incapacidad', $campos, $valore);
								
	        					if($resultadoInca != 'Error'){
	        						echo'<script>
										swal({
											  	type: "success",
											  	title: "La incapacidad ha sido aceptada, correctamente y se creo en el sistema, con el estado POR RADICAR",
											  	showConfirmButton: true,
											  	confirmButtonText: "Cerrar",
											  	closeOnConfirm: false
										  	},
										  	function(result){	
												window.location = "incapacidadesnomina"
											});
										</script>';
	        					}
	        				}
    					} else {
    						/*La incapacidad ya existia*/
    						echo'<script>
								swal({
									  	type: "success",
									  	title: "La incapacidad ha sido aceptada, correctamente, esta incapacidad ya existe en el sistema",
									  	showConfirmButton: true,
									  	confirmButtonText: "Cerrar",
									  	closeOnConfirm: false
								  	},
								  	function(result){	
										window.location = "incapacidadesnomina"
									});
								</script>';
    					}  

					}else{
						echo'<script>
							swal({
								  	type: "success",
								  	title: "La incapacidad ha sido editada correctamente",
								  	showConfirmButton: true,
								  	confirmButtonText: "Cerrar",
								  	closeOnConfirm: false
							  	},
							  	function(result){	
									window.location = "incapacidadesnomina"
								});
							</script>';
					}
					
				}else{
					print_r($respuesta);
				}
			}
		}

		static public function ctrEliminarNomina(){
			if(isset($_GET["idIncapacidad"])){
				$tabla ="gi_incapacidades_nomina";
				$datos = $_GET["idIncapacidad"];
				$respuesta = ModeloTesoreria::mdlBorrar($tabla, "incn_id_i = ".$datos);
				self::setAuditoria('ControladorNomina', 'ctrEliminarNomina',$datos,$respuesta);
				if($respuesta == "ok"){
					echo'<script>
					swal({
						  	type: "success",
						  	title: "La incapacidad ha sido borrada correctamente",
						  	showConfirmButton: true,
						  	confirmButtonText: "Cerrar",
						  	closeOnConfirm: false
					  	},
					  	function(result){	
							window.location = "incapacidadesnomina"
						});
					</script>';
				}		
			}
		}

	}