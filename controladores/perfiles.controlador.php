<?php

	class ControladorPerfiles extends ControladorPlantilla
	{
		
		/**
		*Desc.  => Mostrar los perfiles registrados en el sistema
		*params => $item @nullable, $valor @nullable
		*Method => POST
		*Return => array []
		**/
		static public function ctrMostrarPerfiles($item, $valor){
			$tabla = 'sys_perfiles';
			$respuesta = ModeloPerfiles::mdlMostrarPerfiles($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar los permisos, para cada perfil
		*params => $item , $valor
		*Method => POST
		*Return => array []
		**/
		static public function ctrMostrarMenusPermisos($item, $valor){
			$tabla = 'sys_perfiles_permisos';
			$respuesta = ModeloPerfiles::mdlMostrarMenusPermisos($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar los menus por permiso, para cada perfil
		*params => $item , $valor
		*Method => POST
		*Return => array []
		**/
		static public function ctrMostrarMenus($item, $valor){
			$tabla = 'sys_menus';
			$respuesta = ModeloPerfiles::mdlMostrarMenus($tabla, $item, $valor);
			return $respuesta;
		}	


		/**
		*Desc.  => Mostrar los menu de reportes por cada perfil que se les permite ver
		*params => $item , $valor
		*Method => POST
		*Return => array []
		**/
		static public function ctrMostrarMenuReporte(){
			$tabla = 'sys_menus';
			$respuesta = ModeloPerfiles::mdlMostrarMenusReportes($tabla);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar los menu de reportes permitidos
		*params => $item , $valor
		*Method => POST
		*Return => array []
		**/
		static public function ctrMostrarMenusReportesPermisos($item, $valor){
			$tabla = 'sys_perfiles_reportes';
			$respuesta = ModeloPerfiles::mdlMostrarMenusPermisosReportes($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar los submenu de reportes permitidos
		*params => $item , $valor
		*Method => POST
		*Return => array []
		**/
		static public function ctrMostrarOpciones($item, $valor){
			$tabla = 'sys_submenu';
			$respuesta = ModeloPerfiles::mdlMostrarOpciones($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar opciones de menu para reportes
		*params => $item , $valor
		*Method => POST
		*Return => array []
		**/
		static public function ctrMostrarOpcionesReportePerfil($perfil, $menu){
			$respuesta = ModeloPerfiles::mdlMostrarOpcionesPerfilReporte($perfil, $menu);
			return $respuesta;
		}

		/**
		*Desc.  => Crear el Perfil
		*params => NuevoNombre , NuevoAdicionar, NuevoEditar, NuevoEliminar, NuevoCliente
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrCrearPerfil(){
			if(isset($_POST['NuevoNombre'])){
				if($_POST['NuevoNombre'] != '' && !empty($_POST['NuevoNombre'])){

					$adiciona = 0;
					$edita = 0;
					$elimina = 0;

					if(isset($_POST['NuevoAdicionar'])){ $adiciona = 1; }

					if(isset($_POST['NuevoEditar'])){ $edita = 1; }

					if(isset($_POST['NuevoEliminar'])){ $elimina = 1; }

					$tabla = 'sys_perfiles';
					$datos = array(	'perfiles_clientes_id_i' => $_POST['NuevoCliente'] , 
									'perfiles_descripcion_v' => $_POST['NuevoNombre'],
									'perfilies_estado_i'	 => 1,
									'perfiles_adiciona_i' 	 =>	$adiciona,	
									'perfiles_edita_i' 		 => $edita,
									'perfiles_elimina_i'	 => $elimina
								);

					$respuesta = ModeloPerfiles::mdlIngresarPerfiles($tabla, $datos);
					self::setAuditoria('ControladorPerfiles', 'ctrCrearPerfil',json_encode($datos),$respuesta);
					if($respuesta == "ok"){
						/* traigo el id del perfil */
						$respuesta = ModeloPerfiles::mdlMostrarPerfiles($tabla, 'perfiles_descripcion_v', $_POST['NuevoNombre']);
						$id_perfil = $respuesta['perfiles_id_i'];

						/* ahora tneenemos que meter los permisos */
						if(isset($_POST['NuevoMenus'])){
							self::ctrInsertarPermisos($id_perfil, $_POST['NuevoMenus']);
						}

						/*Metemos los reportes en caso de que tenga seleccionados*/
						if(isset($_POST['NuevoMenusReportes'])){
							self::ctrInsertarPermisosMenuReportes($id_perfil, $_POST['NuevoMenusReportes']);
						}

						echo json_encode( array('code' => 1, 'desc' => "Perfil creado correctamente"));
					}else{
						echo json_encode( array('code' => 0, 'desc' => "Perfil no creado"));
					}

				}else{
					echo json_encode( array('code' => -1, 'desc' => "Nombre de perfil, necesario"));
				}
			}
		}

		/**
		*Desc.  => Editar el Perfil
		*params => EditarNombre , EditarAdicionar, EditarEditar, EditarEliminar, EditarCliente, EditarPerfil
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrEditarPerfil(){
			if(isset($_POST['EditarNombre'])){
				if($_POST['EditarNombre'] != '' && !empty($_POST['EditarNombre'])){
					
					$adiciona = 0;
					$edita = 0;
					$elimina = 0;

					if(isset($_POST['EditarAdicionar'])){ $adiciona = 1; }

					if(isset($_POST['EditarEditar'])){ $edita = 1; }

					if(isset($_POST['EditarEliminar'])){ $elimina = 1; }

					$tabla = 'sys_perfiles';
					$datos = array(	'perfiles_clientes_id_i' => $_POST['EditarCliente'] , 
									'perfiles_descripcion_v' => $_POST['EditarNombre'],
									'perfiles_id_i'	 		 => $_POST['EditarPerfil'],
									'perfiles_adiciona_i' 	 =>	$adiciona,	
									'perfiles_edita_i' 		 => $edita,
									'perfiles_elimina_i'	 => $elimina
								);

					$respuesta = ModeloPerfiles::mdlEditarPerfiles($tabla, $datos);
					self::setAuditoria('ControladorPerfiles', 'ctrEditarPerfil',json_encode($datos),$respuesta);
					if($respuesta == "ok"){

						/* Trae el id del perfil */
						$id_perfil = $_POST['EditarPerfil'];
						
						if(isset($_POST['EditarMenus'])){	
							self::ctrInsertarPermisos($id_perfil, $_POST['EditarMenus']);
						}

						if(isset($_POST['EditarMenusReportes'])){
							self::ctrInsertarPermisosMenuReportes($id_perfil, $_POST['EditarMenusReportes']);
						}

						echo json_encode( array('code' => 1, 'desc' => "Perfil editado correctamente"));
					}else{
						echo json_encode( array('code' => 0, 'desc' => "Perfil no editado"));
					}

				}else{
					echo json_encode( array('code' => -1, 'desc' => "Nombre de perfil, necesario"));
				}
			}
		}


		/**
		*Desc.  => Borrar el Perfil
		*params => EditarNombre , EditarAdicionar, EditarEditar, EditarEliminar, EditarCliente, EditarPerfil
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrBorrarPerfil(){
			if(isset($_POST["id_perfiles"])){
				
				$tabla ="sys_perfiles";
				$datos = $_POST["id_perfiles"];
				$respuesta = ModeloPerfiles::mdlBorrarPerfil($tabla, $datos);
				self::setAuditoria('ControladorPerfiles', 'ctrBorrarPerfil',$datos,$respuesta);
				if($respuesta == "ok"){
					echo json_encode( array('code' => 1, 'desc' => "Perfil borrado correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "Perfil no borrado"));
				}
			}	
		}

		/**
		*Desc.  => Insertar Permisos
		*params => idPerfil, permisos
		*Method => POST
		*Return null
		**/
		static public function ctrInsertarPermisos($id_perfil, $EditarMenus){
			/* borro los menus de ese perfi */
			$other = ModeloPerfiles::mdlBorrarPermisos('sys_perfiles_permisos', $id_perfil);
			foreach ($EditarMenus as $key) {
				$tabla = "sys_perfiles_permisos";
				$datos = array ( 
					'perfiles_permisos_perfil_id_i' => $id_perfil,
					'perfiles_permisos_menu_id_i' 	=> $key
				);
				$respuesta = ModeloPerfiles::mdlIngresarMenu($tabla, $datos);
			}
		}

		/**
		*Desc.  => Insertar Menu Reportes permisos
		*params => idPerfil, reportes
		*Method => POST
		*Return null
		**/
		static public function ctrInsertarPermisosMenuReportes($id_perfil, $EditarMenusReportes){
			/* borro los reportes de ese perfil */
			$other = ModeloPerfiles::mdlBorrarPermisos('sys_perfiles_reportes', $id_perfil);
			foreach ($EditarMenusReportes as $key) {
				$tabla = "sys_perfiles_reportes";
				$datos = array ( 
					'perfiles_permisos_reportes_perfil_id_i' => $id_perfil,
					'perfiles_permisos_reportes_submenu_id_i' 	=> $key
				);
				$respuesta = ModeloPerfiles::mdlIngresarMenuReportes($tabla, $datos);
			}
		}
	}