<?php
	/*Controlador para visualizar la data de gi_auditoria, se reciben las peticiones por POST*/
	/*Los metodos para hacer consultas o SELECT estan en plantilla, asi que se heredan*/
	/*Fecha Creacion: 06/09/2022 */
	/*Autor Nombre del Autor:Richard Fonseca */
	class ControladorAuditoria extends ControladorPlantilla // Siempre extiende de Plantilla
	{
		/*
		*Funcion para Traer los datos, se invoca por Form, Method POST
		*Return JSON => Code 1 Exito, 0 Error, message con el mensaje a mostrar en cada caso
		**/
        public static function ctrListarAuditoria(){
            return ModeloAudtoria::mdlListarFecha();
        }
		public static function ctrUltimoMovimiento($idSession){
            return ModeloAudtoria::mdlUltimoMovimiento($idSession)['aud_hora_d'];
        }
		
    }