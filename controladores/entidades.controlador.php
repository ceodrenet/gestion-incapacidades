<?php

	class ControladorEntidades extends ControladorPlantilla
	{
		

		/**
		*Desc.  => Crear una nueva Entidad, son diferentes a las EPS, aunquedeben ir enlazadas,
		*params => NuevoFormato1, NuevoFormato2, NuevoFormato3, NuevoNombre, NuevoCodigo, NuevoCiudad,      * NuevoDireccion, NuevoTelefono, NuevoHorario, NuevoObservacion
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrCrearEntidad(){
			if(isset($_POST['NuevoNombre'])){
					
				$ruta =  "";
	            $ruta_2 =  "";
	            $ruta_3 =  "";

	            if(isset($_FILES['NuevoFormato1']['tmp_name']) && !empty($_FILES['NuevoFormato1']['tmp_name']) ){
	                $ruta = selft::putImage($_FILES['NuevoFormato1']['tmp_name'], $_FILES["NuevoFormato1"]["type"] , __DIR__."/../vistas/img/entidades/".$_POST['NuevoCodigo'], 'vistas/img/entidades/'.$_POST['NuevoCodigo']);
	            }

	            if(isset($_FILES['NuevoFormato2']['tmp_name']) && !empty($_FILES['NuevoFormato2']['tmp_name']) ){
 					$ruta_2 = selft::putImage($_FILES['NuevoFormato2']['tmp_name'], $_FILES["NuevoFormato2"]["type"] , __DIR__."/../vistas/img/entidades/".$_POST['NuevoCodigo'], 'vistas/img/entidades/'.$_POST['NuevoCodigo']);
	            }

	            if(isset($_FILES['NuevoFormato3']['tmp_name']) && !empty($_FILES['NuevoFormato3']['tmp_name']) ){
	                $ruta_3 = selft::putImage($_FILES['NuevoFormato3']['tmp_name'], $_FILES["NuevoFormato3"]["type"] , __DIR__."/../vistas/img/entidades/".$_POST['NuevoCodigo'], 'vistas/img/entidades/'.$_POST['NuevoCodigo']);
	            }

				$tabla = "gi_entidades";
				$datos = array(
							'ent_nombres' 				=> $_POST['NuevoNombre'], 
							'ent_codigo' 				=> $_POST['NuevoCodigo'],
							'ent_ciudad'  				=> $_POST['NuevoCiudad'],
							'ent_direccion'				=> $_POST['NuevoDireccion'],
							'ent_telefono' 				=> $_POST['NuevoTelefono'],
							'ent_horario'  				=> $_POST['NuevoHorario'],
							'ent_observacion'			=> $_POST['NuevoObservacion'],
							'ent_formato_3'				=> $ruta_3,
							'ent_formato_2' 			=> $ruta_2,
							'ent_formato_1'				=> $ruta,
							'ent_estado' 				=> 1
						);

				$respuesta = ModeloEntidades::mdlIngresarEntidad($tabla, $datos);
				self::setAuditoria('ControladorEntidades', 'ctrCrearEntidad',json_encode($datos),$respuesta);
				if($respuesta == "ok"){
					return json_encode(array('code' => 1, 'message' => 'Entidad guardadada con exito'));
				}else{	
					return json_encode(array('code' => 0, 'message' => 'Entidad no guardadada'));
				}
			}
		}

		/**
		*Desc.  => Crear una nueva Entidad, son diferentes a las EPS, aunquedeben ir enlazadas,
		*params => EditarFormato1, EditarFormato2, EditarFormato3, EditarNombre, EditarCodigo, EditarCiudad,      * EditarDireccion, EditarTelefono, EditarHorario, EditarObservacion, EditarId
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrEditarEntidad(){
			if(isset($_POST['EditarNombre'])){
					
				$ruta =  $_POST['formato1'];
	            $ruta_2 =  $_POST['formato2'];
	            $ruta_3 =  $_POST['formato3'];
	            
	            if(isset($_FILES['EditarFormato1']['tmp_name']) && !empty($_FILES['EditarFormato1']['tmp_name']) ){
	                $ruta = selft::putImage($_FILES['EditarFormato1']['tmp_name'], $_FILES["EditarFormato1"]["type"] , __DIR__."/../vistas/img/entidades/".$_POST['NuevoCodigo'], 'vistas/img/entidades/'.$_POST['NuevoCodigo']);
	            }

	            if(isset($_FILES['EditarFormato2']['tmp_name']) && !empty($_FILES['EditarFormato2']['tmp_name']) ){
 					$ruta_2 = selft::putImage($_FILES['EditarFormato2']['tmp_name'], $_FILES["EditarFormato2"]["type"] , __DIR__."/../vistas/img/entidades/".$_POST['NuevoCodigo'], 'vistas/img/entidades/'.$_POST['NuevoCodigo']);
	            }

	            if(isset($_FILES['EditarFormato3']['tmp_name']) && !empty($_FILES['EditarFormato3']['tmp_name']) ){
	                $ruta_3 = selft::putImage($_FILES['EditarFormato3']['tmp_name'], $_FILES["EditarFormato3"]["type"] , __DIR__."/../vistas/img/entidades/".$_POST['NuevoCodigo'], 'vistas/img/entidades/'.$_POST['NuevoCodigo']);
	            }

				$tabla = "gi_entidades";
				$datos = array(
							'ent_nombres' 				=> $_POST['EditarNombre'], 
							'ent_codigo' 				=> $_POST['EditarCodigo'],
							'ent_ciudad'  				=> $_POST['EditarCiudad'],
							'ent_direccion'				=> $_POST['EditarDireccion'],
							'ent_telefono' 				=> $_POST['EditarTelefono'],
							'ent_horario'  				=> $_POST['EditarHorario'],
							'ent_observacion'			=> $_POST['EditarObservacion'],
							'ent_formato_3'				=> $ruta_2,
							'ent_formato_2' 			=> $ruta_3,
							'ent_formato_1'				=> $ruta,
							'ent_estado' 				=> 1,
							'ent_id'					=> $_POST['EditarId']
						);

				
				$respuesta = ModeloEntidades::mdlEditarEntidades($tabla, $datos);
				self::setAuditoria('ControladorEntidades', 'ctrEditarEntidad',json_encode($datos),$respuesta);
				if($respuesta == "ok"){
					return json_encode(array('code' => 1, 'message' => 'Entidad editada con exito'));
				}else{	
					return json_encode(array('code' => 0, 'message' => 'Entidad no editada'));
				}
			}
		}

		 /*Mostrar Entidades*/
		static public function ctrMostrarEntidades($item, $valor){
			$tabla = "gi_entidades";
			$respuesta = ModeloEntidades::mdlMostrarEntidades($tabla, $item, $valor);
			return $respuesta;
		}

		/* Eliminar cliente */
		static public function ctrBorrarEntidad(){

			if(isset($_POST["id_entidad"])){
				$tabla ="gi_entidades";
				$datos = $_POST["id_entidad"];
				$respuesta = ModeloEntidades::mdlBorrarEntidad($tabla, $datos);
				self::setAuditoria('ControladorEntidades', 'ctrBorrarEntidad',$datos,$respuesta);
				if($respuesta == "ok"){
					return json_encode(array('code' => 1, 'message' => 'Entidad borrada con exito'));
				}else{	
					return json_encode(array('code' => 0, 'message' => 'Entidad no borrada'));
				}		
			}
		}		
	}