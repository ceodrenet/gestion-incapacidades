<?php

	class ControladorUsuariosEps extends ControladorPlantilla
	{	
		/**
		*Desc.  => Agregar Usuarios de Administradoras
		*params => NuevoNombreUsuario, NuevoIpsId, NuevoNombreUsuario, NuevoPasswoUsuario, NuevoUrl___Usuario, 
		* NuevoIdEmpresa, nuevoObservacion
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/

		static public function ctrCrearUsuariosEps(){
			if(isset($_POST['NuevoNombreUsuario'])){
				if($_POST['NuevoNombreUsuario'] != '' && !empty($_POST['NuevoNombreUsuario'])){

					$tabla = 'gi_usuarios_eps';
					$datos = array(	'use_ips_id_i' 		=> $_POST['NuevoIpsId'] , 
									'use_usuario_v' 	=> $_POST['NuevoNombreUsuario'],
									'use_password_v'	=> $_POST['NuevoPasswoUsuario'] ,	
									'use_url_v' 		=> $_POST['NuevoUrl___Usuario'],
									'use_emp_id_i'		=> $_POST['NuevoIdEmpresa'],
									'use_observacion_v'	=> $_POST['nuevoObservacion']
								);
					$respuesta = ModeloUsuariosEps::mdlIngresarUsuariosEps($tabla, $datos);
					self::setAuditoria('ControladorUsuariosEps', 'ctrCrearUsuariosEps',json_encode($datos),$respuesta);
					if($respuesta == "ok"){
						echo json_encode( array('code' => 1, 'desc' => "usuario administradora creado correctamente"));
					}
				}else{
					echo json_encode( array('code' => -1, 'desc' => "Nombre de usuario es necesario"));
				}
			}
		}

		/**
		*Desc.  => Editar Usuarios de Administradoras
		*params => EditarNombreUsuario, EditarIpsId, EditarNombreUsuario, EditarPasswoUsuario, EditarUrl___Usuario, 
		* EditarIdEmpresa, EditarObservacion, EditarIdUsuario
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrEditarUsuariosEps(){
			if(isset($_POST['EditarNombreUsuario'])){
				if($_POST['EditarNombreUsuario'] != '' && !empty($_POST['EditarNombreUsuario'])){
					$tabla = 'gi_usuarios_eps';
					$datos = array(	'use_ips_id_i' 		 => $_POST['EditarIpsId'] , 
									'use_usuario_v' 	 => $_POST['EditarNombreUsuario'],
									'use_id_i'	 		 => $_POST['EditarIdUsuario'],
									'use_password_v' 	 =>	$_POST['EditarPasswoUsuario'],	
									'use_url_v' 		 => $_POST['EditarUrl___Usuario'],
									'use_observacion_v'	 => $_POST['editarObservacion'] //esta linea esta fallando
								);

					$respuesta = ModeloUsuariosEps::mdlEditarUsuariosEps($tabla, $datos);
					self::setAuditoria('ControladorUsuariosEps', 'ctrEditarUsuariosEps',json_encode($datos),$respuesta);
					if($respuesta == "ok"){
						echo json_encode( array('code' => 1, 'desc' => "usuario administradora editado correctamente"));
					}else{
						echo json_encode( array('code' => 0, 'desc' => "usuario administradora no editado"));
					}

				}else{
					echo json_encode( array('code' => -1, 'desc' => "Nombre de usuario es necesario"));
				}
			}
		}

		/**
		*Desc.  => Eliminar Usuarios de Administradoras
		*params => id_usuario_eps
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrBorrarUsuariosEps(){
			if(isset($_POST["id_usuario_eps"])){
				$tabla ="gi_usuarios_eps";
				$datos = $_POST["id_usuario_eps"];
				$respuesta = ModeloUsuariosEps::mdlBorrarUsuariosEps($tabla, $datos);
				self::setAuditoria('ControladorUsuariosEps', 'ctrBorrarUsuariosEps',$datos,$respuesta);
				if($respuesta == "ok"){
					echo json_encode( array('code' => 1, 'desc' => "usuario administradora eliminado correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "usuario administradora no eliminado"));
				}
			}	
		}

	}