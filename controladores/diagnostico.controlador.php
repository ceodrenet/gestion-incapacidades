<?php
	class ControladorDiagnostico extends ControladorPlantilla
	{ 
		/**
		*Desc.  => Mostrar los Diagnosticos,
		*params => $item @nullable, $valor @nullable
		*Method => POST
		*Return => array []
		**/
		static public function ctrMostrarDiagnostico($item, $valor){
			$tabla = "gi_diagnostico";
			$respuesta = ModeloDiagnostico::mdlMostrarDiagnostico($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar el detalle de uno o varios diagnostico, para autocompletar en select2
		*params => valor
		*Method => POST
		*Return => array []
		**/
		static public function ctrBuscarDiagnosticos($valor){
			$tabla = "gi_diagnostico";
			$respuesta = ModeloDiagnostico::mdlCompletarDiagnostico($tabla, $valor);
			return $respuesta;
		}
	}