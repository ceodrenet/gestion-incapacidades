<?php
	/**
	* 
	*/
	class ControladorSeguridad extends ControladorPlantilla
	{
		
		/**
		*Desc.  => Ver los reportes de las tablas reporte_seguridad_salud y gi_diagnostico
		*params => item, valor, fecha1, fecha2, $tipoReporte
		*Method => POST
		*Return => array []
		**/
		static public function ctrVerReportesTesoreria($item, $valor, $fecha1= null, $fecha2= null, $tipoReporte = null){
			$tabla = 'reporte_seguridad_salud';	
			$respuesta = null;
			switch ($tipoReporte){
				case 'mostrarGeneralPagos':
					$respuesta = ModeloSeguridad::mdlMostrarGeneralPagos($tabla, $item, $valor);
					break;
					
				case 'mostrarGeneralPagos_byCliente':
					$respuesta = ModeloSeguridad::mdlMostrarGeneralPagos_byCliente($tabla, $item, $valor);
					break;

				case 'mostrarGeneralPagos_byClienteFactura':
					$respuesta = ModeloSeguridad::mdlMostrarGeneralPagos_byClienteFactura($tabla, $item, $valor, $fecha1, $fecha2);
					break;

				case 'contarValorIncapacidades':
					$respuesta = ModeloSeguridad::mdlDiagnosticosTop($tabla, $item, $valor);
					break;

				case 'totalValorIncapacidades':
					$respuesta = ModeloSeguridad::mdlTotalIncapacidad($tabla, $item, $valor);
					break;

				case 'getMasRepetido':
					$respuesta = ModeloSeguridad::mdlDiagnosticosMasRepetido($tabla, $item,$valor);
					break;

				case 'getMasRepetidoNombre':
					$respuesta = ModeloSeguridad::mdlNombreDiagnostico($tabla, $item, $valor);
					break;
				
				default:
					$respuesta = ModeloSeguridad::mdlMostrarGeneralPagos($tabla, $item, $valor);
					break;
			}
			return $respuesta;
		}
	}