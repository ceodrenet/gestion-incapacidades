<?php
	/**
	* Archivos
	*/
	class ControladorArchivos extends ControladorPlantilla
	{
		
		/**
		*Desc.  => Mostrar todos los archivos
		*params => $item , campo a buscar, $valor , valor a buscar
		*Method => GET
		*Return => array {}
		**/
		static public function ctrMostrarArchivos($item, $valor){
			$tabla = "gi_archivos";
			$respuesta = ModeloArchivos::mdlMostrarArchivos($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar los valores de un archivo para editarlo
		*params => $item , campo a buscar, $valor , valor a buscar
		*Method => GET
		*Return => array {}
		**/
		static public function ctrMostrarArchivos_edicion($item, $valor){
			$tabla = "gi_archivos";
			$respuesta = ModeloArchivos::mdlMostrarArchivos_e($tabla, $item, $valor);
			return $respuesta;
		}


		/**
		*Desc.  => Crear nuevos Archivos enviados a EPS
		*params => NuevoFormato1, NuevoFormato2, NuevoDescripcion, NuevoEntidad, NuevoOrigen, NuevoFecha, Nuevoarc_empresa_id_i
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrCrearArchivo(){
			if(isset($_POST['NuevoEntidad'])){
				$ruta =  "";
	            $ruta_2 =  "";
	            if(isset($_FILES['NuevoFormato1']['tmp_name']) && !empty($_FILES['NuevoFormato1']['tmp_name']) ){
	                $ruta = selft::putImage($_FILES['NuevoFormato1']['tmp_name'], $_FILES["NuevoFormato1"]["type"] , __DIR__."/../vistas/img/Archivos/".$_POST['NuevoCodigo'], 'vistas/img/Archivos/'.$_POST['NuevoCodigo']);
	            }

	            if(isset($_FILES['NuevoFormato2']['tmp_name']) && !empty($_FILES['NuevoFormato2']['tmp_name']) ){
	                $ruta_2 = selft::putImage($_FILES['NuevoFormato2']['tmp_name'], $_FILES["NuevoFormato2"]["type"] , __DIR__."/../vistas/img/Archivos/".$_POST['NuevoCodigo'], 'vistas/img/Archivos/'.$_POST['NuevoCodigo']);
	            }

				$tabla = "gi_archivos";
				$datos = array(
							'arc_descripcion' 			=> $_POST['NuevoDescripcion'], 
							'arc_entidad' 				=> $_POST['NuevoEntidad'],
							'arc_origen'  				=> $_POST['NuevoOrigen'],
							'arc_fecha'					=> $_POST['NuevoFecha'],
							'arc_ruta_1' 				=> $ruta,
							'arc_ruta_2'  				=> $ruta_2,
							'arc_empresa_id_i'			=> $_POST['Nuevoarc_empresa_id_i']
						);
				
				$respuesta = ModeloArchivos::mdlIngresarArchivo($tabla, $datos);
				self::setAuditoria('ControladorArchivos', 'ctrCrearArchivo', json_encode($datos), $respuesta);
				if($respuesta == 'ok'){
					echo json_encode( array('code' => 1, 'desc' => "entidad creada correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "entidad no creada"));
				}
			}else{
				echo json_encode( array('code' => -1, 'desc' => "Nombre de entidad es necesario"));
			}
		}

		/**
		*Desc.  => edicion de Archivos enviados a EPS
		*params => EditarFormato1, EditarFormato2, EditarDescripcion, EditarEntidad, EditarOrigen, EditarFecha, Editararc_empresa_id_i, EditarId
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrEditarArchivos(){
			if(isset($_POST['EditarEntidad'])){	

				$ruta 	=  $_POST['formato1'];
	            $ruta_2 =  $_POST['formato2'];
	            if(isset($_FILES['EditarFormato1']['tmp_name']) && !empty($_FILES['EditarFormato1']['tmp_name']) ){
	                $ruta = selft::putImage($_FILES['EditarFormato1']['tmp_name'], $_FILES["EditarFormato1"]["type"] , __DIR__."/../vistas/img/Archivos/".$_POST['NuevoCodigo'], 'vistas/img/Archivos/'.$_POST['NuevoCodigo']);
	            }

	            if(isset($_FILES['EditarFormato2']['tmp_name']) && !empty($_FILES['EditarFormato2']['tmp_name']) ){
	                $ruta_2 = selft::putImage($_FILES['EditarFormato2']['tmp_name'], $_FILES["EditarFormato2"]["type"] , __DIR__."/../vistas/img/Archivos/".$_POST['NuevoCodigo'], 'vistas/img/Archivos/'.$_POST['NuevoCodigo']);
	            }

				$tabla = "gi_archivos";
				$datos = array(
							'arc_descripcion' 			=> $_POST['EditarDescripcion'], 
							'arc_entidad' 				=> $_POST['EditarEntidad'],
							'arc_origen'  				=> $_POST['EditarOrigen'],
							'arc_fecha'					=> $_POST['EditarFecha'],
							'arc_ruta_1' 				=> $ruta,
							'arc_ruta_2'  				=> $ruta_2,
							'arc_id'					=> $_POST['EditarId'],
							'arc_empresa_id_i'			=> $_POST['Editararc_empresa_id_i']
						);
						self::setAuditoria('ControladorArchivos', 'ctrEditarArchivos', json_encode($datos), $respuesta);
						$respuesta = ModeloArchivos::mdlEditarArchivos($tabla, $datos);
						if($respuesta == 'ok'){
							echo json_encode( array('code' => 1, 'desc' => "entidad editada correctamente"));
						}else{
							echo json_encode( array('code' => 0, 'desc' => "entidad no editada"));
						}
					}else{
						echo json_encode( array('code' => -1, 'desc' => "Nombre de entidad es necesario"));
					}
				}


		/* Eliminar Archivo */
		static public function ctrBorrarArchivo(){

			if(isset($_POST["id_Archivo"])){
				$tabla ="gi_archivos";
				$datos = $_POST["id_Archivo"];
				$respuesta = ModeloArchivos::mdlBorrarArchivo($tabla, $datos);
				self::setAuditoria('ControladorArchivos', 'ctrBorrarArchivo', $datos, $respuesta);
				if($respuesta == 'ok'){

					echo json_encode( array('code' => 1, 'desc' => "entidad eliminada correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "entidad no eliminada"));
				}	
			}
		}		
	}