<?php

	class ControladorCalendario extends ControladorPlantilla
	{

		/**
		*Desc.  => ver todos los eventos del calendario
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarEventos($item, $valor){
			$tabla = "gi_eventos";
			$respuesta = ModeloCalendario::mdlMostrarEventos($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Crear nuevos eventos en la base de datos
		*params => txtFechaInicioEventoT, txtHoraInicioEvento, txtFechaFinalEventoT, txtHoraFinalEvento, checkboxTodoElDia, id_Usuario, txtDecripcionEvento
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrCrearEvento(){
			if(isset($_POST['txtNombreEvento'])){
				$todo_dia = 0;

				$fechaInicial = $_POST['txtFechaInicioEventoT'].' '.$_POST['txtHoraInicioEvento'];
				$fechaFinal__ = $_POST['txtFechaFinalEventoT'].' '.$_POST['txtHoraFinalEvento'];
				$fechaPostFinal = $_POST['txtFechaFinalEventoT'];
				$horaPostFinal_ = $_POST['txtHoraFinalEvento'];
				if(isset($_POST['checkboxTodoElDia'])){
					$todo_dia = -1;
					$fechaFinal__ = $_POST['txtFechaInicioEventoT'].' 23:59:59';
					$fechaPostFinal = $_POST['txtFechaInicioEventoT'];
					$horaPostFinal_ = '23:59:59';
				}
				

				$horaInicio = new DateTime($fechaInicial);
				$horaTermino = new DateTime($fechaFinal__);

				$interval = $horaInicio->diff($horaTermino);
				$duracion = $interval->format('%H');

				$fechaFinalPost = null;
				$horaFinalPost_ = null;

				$tabla = "gi_eventos";
				$datos = array(
							'evn_asunto' 			=> $_POST['txtNombreEvento'], 
							'evn_fecha_inicio' 		=> $_POST['txtFechaInicioEventoT'],
							'evn_hora_inicio'  		=> $_POST['txtHoraInicioEvento'],
							'evn_fecha_final'		=> $fechaPostFinal,
							'evn_hora_final' 		=> $horaPostFinal_,
							'evn_organizador'  		=> $_SESSION['nombres'],
							'evn_lugar'				=> NULL,
							'evn_asistencia'		=> 1,
							'evn_duracion' 			=> $duracion,
							'evn_observacion'  		=> $_POST['txtDecripcionEvento'],
							'evn_tododia'			=> $todo_dia
						);

				$respuesta = ModeloCalendario::mdlIngresarEvento($tabla, $datos);
				self::setAuditoria('ControladorCalendario', 'ctrCrearEvento',json_encode($datos),$respuesta);
				if($respuesta == 'ok'){

					$otros_Correos = '';
					if(!is_null($_POST['txtPersonasventos'])){
						$otros_Correos = $_POST['txtPersonasventos'];
					}

					self::EnviarAgenda($_POST['txtNombreEvento'], $otros_Correos, $_POST['txtFechaInicioEventoT'], $_POST['txtHoraInicioEvento'], $duracion, $_SESSION['nombres'], 'Las oficinas de GEIN', 1);

					echo json_encode( array('code' => 1, 'desc' => "Evento registrado correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "Evento no registrado"));
				}
			}
		}

		/**
		*Desc.  => Editar eventos en la base de datos
		*params => txtEditarFechaInicioEventoT, txtEditarHoraInicioEvento, txtEditarFechaFinalEventoT, txtEditarHoraFinalEvento, txtEditarNombreEvento, id_Usuario, txtEditarDecripcionEvento
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrEditarEvento(){
			if(isset($_POST['txtEditarNombreEvento'])){
				$todo_dia = 0;
				if(isset($_POST['checkboxEditarTodoElDia'])){
					$todo_dia = -1;
				}

				$fechaInicial = $_POST['txtEditarFechaInicioEventoT'].' '.$_POST['txtEditarHoraInicioEvento'];
				$fechaFinal__ = $_POST['txtEditarFechaFinalEventoT'].' '.$_POST['txtEditarHoraFinalEvento'];

				$horaInicio = new DateTime($fechaInicial);
				$horaTermino = new DateTime($fechaFinal__);

				$interval = $horaInicio->diff($horaTermino);
				$duracion = $interval->format('%H');

				$tabla = "gi_eventos";
				$datos = array(
							'evn_asunto' 			=> $_POST['txtEditarNombreEvento'], 
							'evn_fecha_inicio' 		=> $_POST['txtEditarFechaInicioEventoT'],
							'evn_hora_inicio'  		=> $_POST['txtEditarHoraInicioEvento'],
							'evn_fecha_final'		=> $_POST['txtEditarFechaFinalEventoT'],
							'evn_hora_final' 		=> $_POST['txtEditarHoraFinalEvento'],
							'evn_organizador'  		=> $_SESSION['nombres'],
							'evn_lugar'				=> NULL,
							'evn_asistencia'		=> 1,
							'evn_duracion' 			=> $duracion,
							'evn_observacion'  		=> $_POST['txtEditarDecripcionEvento'],
							'evn_tododia'			=> $todo_dia,
							'evn_id'				=> $_POST['txtValorDelEvento']
						);

				//var_dump($datos);
				
				$respuesta = ModeloCalendario::mdlEditarEvento($tabla, $datos);
				self::setAuditoria('ControladorCalendario', 'ctrEditarEvento',json_encode($datos),$respuesta);
				if($respuesta == 'ok'){

					$otros_Correos = '';
					if(!is_null($_POST['txtEditarPersonasventos']) && $_POST['txtEditarPersonasventos'] != ''){
						$otros_Correos = $_POST['txtEditarPersonasventos'];
					}

					self::EnviarAgenda($_POST['txtEditarNombreEvento'], $otros_Correos, $_POST['txtEditarFechaInicioEventoT'], $_POST['txtEditarHoraInicioEvento'], $duracion, $_SESSION['nombres'], 'Las oficinas de GEIN', 1);

					echo json_encode( array('code' => 1, 'desc' => "Evento editado correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "Evento no editado"));
				}
			}
		}

		/**
		*Desc.  => Borrar los eventos en la base de datos
		*params => idEvento
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		public static function deleteEvento(){
			if(isset($_POST['idEvento']) && $_POST['idEvento'] != ''){
				$tabla ="gi_eventos";
				$datos = $_POST["idEvento"];
				$respuesta = ModeloCalendario::mdlBorrarEvento($tabla, $datos);
				self::setAuditoria('ControladorCalendario', 'ctrEditarEvento',$datos,$respuesta);
				if($respuesta == "ok"){
					echo json_encode( array('code' => 1, 'desc' => "Evento eliminado correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "Evento no eliminado"));
				}	
			}
		}

	}
