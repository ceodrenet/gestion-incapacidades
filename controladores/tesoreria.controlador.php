<?php
	class ControladorTesoreria extends ControladorPlantilla
	{
		
		/**
		*Desc.  => Ver los reportes de las tablas reporte_tesoreria y all_valores_nomina
		*params => item, valor, fecha1, fecha2, $tipoReporte
		*Method => POST
		*Return => array []
		**/
		static public function ctrVerReportesTesoreria($item, $valor, $fecha1= null, $fecha2= null, $tipoReporte = null){
			$tabla = 'reporte_tesoreria';
			$respuesta = null;
			switch ($tipoReporte){
				case 'mostrarGeneralPagos':
					$respuesta = ModeloTesoreria::mdlMostrarGeneralPagos($tabla, $item, $valor, $fecha1, $fecha2);
					break;
					
				case 'mostrarGeneralPagos_byCliente':
					$respuesta = ModeloTesoreria::mdlMostrarGeneralPagos_byCliente($tabla, $item, $valor, $fecha1, $fecha2);
					break;

				case 'mostrarGeneralPagos_byClienteFactura':
					$respuesta = ModeloTesoreria::mdlMostrarGeneralPagos_byClienteFactura($tabla, $item, $valor, $fecha1, $fecha2);
					break;

				case 'contarValorIncapacidades':
					$respuesta = ModeloTesoreria::mdlValorIncapacidad($tabla, $item, $valor, $fecha1, $fecha2);
					break;

				case 'contarValorIncapacidadesAll':
					$respuesta = ModeloTesoreria::mdlValorIncapacidad('all_valores_nomina', $item, $valor, $fecha1, $fecha2);
					break;

				case 'contarValorIncapacidadesAll2':
					$respuesta = ModeloTesoreria::mdlValorIncapacidad('all_valores_nomina_2', $item, $valor, $fecha1, $fecha2);
					break;

				case 'contarValorIncapacidadesAll3':
					$respuesta = ModeloTesoreria::mdlValorIncapacidad('all_valores_nomina_3', $item, $valor, $fecha1, $fecha2);
					break;

				case 'totalValorIncapacidades':
					$respuesta = ModeloTesoreria::mdlTotalIncapacidad($tabla, $item, $valor, $fecha1, $fecha2);
					break;

				default:
					$respuesta = ModeloTesoreria::mdlMostrarGeneralPagos($tabla, $item, $valor, $fecha1, $fecha2);
					break;
			}
			return $respuesta;
		}
	}