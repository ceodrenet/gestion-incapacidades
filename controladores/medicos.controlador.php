<?php

class ControladorMedicos extends ControladorPlantilla
{

    static public function ctrMostrarMedicos($item, $valor){
        $tabla = 'gi_medicos';
        $respuesta = ModeloMedicos::getDatos($tabla,$item,$valor);
        return $respuesta;
    }

    static public function ctrMedicosEntidades($item, $valor){
        $campo = "ment_id_entidad_ips";
        $tabla = "gi_medicos_entidad_atiende";
        $condicion = "$item = $valor";
        $respuesta = ModeloDAO::mdlMostrarGroupAndOrder($campo, $tabla, $condicion);
        return $respuesta;
    }

    static public function existeIdMedico($campo, $valor){
        $tabla = 'gi_medicos';
        $cond = "$campo = $valor";
        $respuesta = ModeloMedicos::mdlMostrarUnitario($campo,$tabla, $cond);
        return $respuesta;
    }

    static public function ctrCrearMedicos()
    {
        if (isset($_POST['NuevaIdentificacion'])) {

            if ($_POST['NuevoNombreMedico'] != '' && !empty($_POST['NuevoNombreMedico'])) {
                $tabla = 'gi_medicos';
                $datos = array(
                    'med_nombre'         => $_POST['NuevoNombreMedico'],
                    'med_identificacion' => empty($_POST['NuevaIdentificacion']) ? null : $_POST['NuevaIdentificacion'],
                    'med_num_registro'   => empty($_POST['NuevoNumRegistro']) ? null : $_POST['NuevoNumRegistro'],
                    'med_estado_rethus'  => $_POST['NuevoEstadoRethus'],
                    'med_usuario_crea'   => $_SESSION['id_Usuario']
                );
                
                $respuesta = ModeloMedicos::mdlIngresarMedicos($tabla, $datos);
                self::setAuditoria('ControladorMedicos', 'ctrCrearMedicos', json_encode($datos), $respuesta);

                if ($respuesta != "Error") {
                    if (isset($_POST["NuevaEntidadIps"]) && $_POST["NuevaEntidadIps"] != '') {
                        foreach ($_POST["NuevaEntidadIps"] as $key) {
                            ModeloMedicos::mdlCrear("gi_medicos_entidad_atiende", "ment_id_medico, ment_id_entidad_ips", $respuesta.",".$key);
                        }
                    }
                    echo json_encode(array('code' => 1, 'desc' => "medico creado correctamente"));
                } else {
                    echo json_encode( array('code' => 0, 'desc' => "medico no creado"));
                }
            } else {
                echo json_encode(array('code' => -1, 'desc' => "Nombre de usuario es necesario"));
            }
        }
    }


    static public function ctrEditarMedicos(){
        if ($_POST['EditarNombreMedico'] != '' && !empty($_POST['EditarNombreMedico'])) {
            $tabla = 'gi_medicos';
            $datoId = $_POST['EditarMedicoID'];
            $datos = array(
                'med_nombre'          => $_POST['EditarNombreMedico'],
                'med_identificacion'  => $_POST['EditarIdentificacion'],
                'med_num_registro'    => $_POST['EditarNumRegistro'],
                'med_estado_rethus'   => $_POST['EditarEstadoRethus'],
                'med_estado'          => $_POST['EditarEstadoMedico'],
                'med_fecha_edicion'   => date('Y-m-d H:i:s'),
                'med_usuario_edita'   => $_SESSION['id_Usuario'],
                'med_id'              => $datoId
            );

            $respuesta = ModeloMedicos::mdlEditarMedicos($tabla, $datos);
            self::setAuditoria('ControladorMedicos', 'ctrEditarMedicos', json_encode($datos), $respuesta);

            if ($respuesta == "ok") {
                if (isset($_POST["EditarEntidadIps"]) && $_POST["EditarEntidadIps"] != '') {
                    ModeloDAO::mdlBorrar('gi_medicos_entidad_atiende', "ment_id_medico = $datoId");
                    foreach ($_POST["EditarEntidadIps"] as $key) {
                        ModeloMedicos::mdlCrear("gi_medicos_entidad_atiende", "ment_id_medico, ment_id_entidad_ips", $datoId.",".$key);
                    }    
                }
                echo json_encode(array('code' => 1, 'desc' => "medico editado correctamente"));
            }
        } else {
            echo json_encode(array('code' => -1, 'desc' => "Nombre medico es necesario"));
        }
    }


    static public function ctrBorrarMedicos()
    {
        if (isset($_POST["id_medico"])) {
            $tabla = "gi_medicos";
            $dato = $_POST["id_medico"];
            $respuestaDao = ModeloDAO::mdlBorrar('gi_medicos_entidad_atiende', "ment_id_medico = $dato");
            /*
                validamos que se eliminen primero los registros que están en la tabla gi_medicos_entidad_atiende
                que soporta la relacion con la table gi_medicos y posterior se eliminan los registros de la tabla
                gi_medicos
             */
            if ($respuestaDao == "ok") {
                $respuesta = ModeloMedicos::mdlBorrarMedicos($tabla, $dato);
                self::setAuditoria('ControladorMedicos', 'ctrBorrarMedicos', $dato, $respuesta);
                if ($respuesta == "ok") {
                    echo json_encode(array('code' => 1, 'desc' => "medico eliminado correctamente"));
                } else {
                    echo json_encode(array('code' => 0, 'desc' => "medico no eliminado"));
                }
            }
            
        }
    }

    static public function ctrBuscarMedicos($valor){
        $tabla = 'gi_medicos';
        $respuesta = ModeloMedicos::mdlBuscarMedicosByNombre($tabla,$valor);
        return $respuesta;
    }

    static public function ctrCountMedicos($year = null, $tipo = null){
        return ModeloMedicos::mdlCountMedicos($year, $tipo)['total'];
    }

    static public function ctrFiltroByYear(){
        return ModeloMedicos::mdlFiltrarByYear();
    }
}
