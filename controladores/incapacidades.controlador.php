<?php
	 ini_set('display_errors', 'On');
  ini_set('display_errors', 1);
	/**
	* 
	*/
	class ControladorIncapacidades extends ControladorPlantilla
	{
		/**
		*Desc.  => Mostrar incapacidades ultimos 6 meses, por año
		*params => $item campo para buscar, $valor valor a buscar, $anho=null puede venir o no
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarLastSixMonth($item, $valor, $anho=null){
			
			$respuesta = ModeloIncapacidades::mdlValoresIncapacidadesx6($item, $valor, $anho);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar incapacidades ultimos 6 meses, pero tiene fecha inicio y fecha final
		*params => $item campo para buscar, $valor valor a buscar, $fechaInici=null, $fechaFinal = null
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarLastSixMonth_2($item, $valor, $fechaInici=null, $fechaFinal = null){
			$respuesta = ModeloIncapacidades::mdlValoresIncapacidadesx62($item, $valor, $fechaInici, $fechaFinal);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar incapacidades si viene el iten retorna el dato de una sola, si no viene toda la 
		* info de las incapacidades y con sus descripciones.
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarIncapacidades($item, $valor){
			$tabla = "gi_incapacidad";
			$respuesta = ModeloIncapacidades::mdlMostrarIncapacidades($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar incapacidades si viene el iten retorna el dato de una sola, si no viene toda la 
		* info de las incapacidades y con sus descripciones, pero viene de la vista gi_atencion
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarAtencion($item, $valor){
			$tabla = "gi_atencion";
			$respuesta = ModeloIncapacidades::mdlMostrarIncapacidades($tabla, $item, $valor);
			return $respuesta;
		}

		
		/**
		*Desc.  => Mostrar incapacidades por empresa
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarIncapacidades_Clientes($item, $valor){
			$tabla = "gi_incapacidad";
			$respuesta = ModeloIncapacidades::mdlMostrarIncapacidadesClientes($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar incapacidades por empresa, pero filtrando por fecha inicio y fecha final
		*params => $item campo para buscar, $valor valor a buscar, $fecha1 fecha inicio, $fecha2 fecha final
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarIncapacidades_ClientesBetween($item, $valor, $fecha1, $fecha2){
			$tabla = "gi_incapacidad";
			$respuesta = ModeloIncapacidades::mdlMostrarIncapacidadesClientesBetween($tabla, $item, $valor, $fecha1, $fecha2);
			return $respuesta;
		}

		

		/**
		*Desc.  => Mostrar incapacidades para editar los datos
		*params => $item campo para buscar, $valor valor a buscar
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarIncapacidades_edicion($item, $valor){
			$tabla = "gi_incapacidad";
			$respuesta = ModeloIncapacidades::mdlMostrarIncapacidades_edicion($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar incapacidades para exportar los datos
		*params => $item campo para buscar, $valor valor a buscar, $fechaInicio = null, $fechaFinal = null
		*Method => POST, GET
		*Return => array {}
		**/
		//RFB--- 300622-- se coloca la variable $estadoTramite para exportar por estado tramite
		static public function ctrMostrarIncapacidades_exportar($item, $valor, $fechaInicio = null, $fechaFinal = null, $pagado = null, $formato = null, $estadoTramite = null, $fechaPago = null){
			$tabla = "gi_incapacidad";
			$respuesta = ModeloIncapacidades::mdlMostrarIncapacidadesExportar($tabla, $item, $valor, $fechaInicio, $fechaFinal, $pagado, $formato, $estadoTramite);
			return $respuesta;
		}
		/**
		*Desc.  => Mostrar datos de notificacion por incapacidad
		*params => $idIncapacidad id de la incapacidad
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrGetDatoNotificacionById($idIncapacidad){
				return ModeloIncapacidades::mdlGetDatoNotificacionById($idIncapacidad);
		}


		/**
		*Desc.  => Mostrar incapacidades pagadas por fechas
		*params => $item campo para buscar, $valor valor a buscar, $fechaInicio = null, $fechaFinal = null
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarIncapacidadesPagadasEmpleado_exportar($item, $valor, $fechaInicio = null, $fechaFinal = null){
			$tabla = "gi_incapacidad";
			$respuesta = ModeloIncapacidades::mdlGetPagosIncapacidades($tabla, $item, $valor, $fechaInicio, $fechaFinal);
			return $respuesta;
		}

		

		/**
		*Desc.  => Mostrar incapacidades pero se filtra por empresa y fecha de inicio o fecha final
		*params => $empresa empresa,  $fecha_inicio, $fecha_final
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarIncapacidades_Consolidado($empresa, $fecha_inicio, $fecha_final){
			$tabla = "gi_incapacidad";
			$respuesta = ModeloIncapacidades::mdlMostrarConsolidado($tabla, $empresa, $fecha_inicio, $fecha_final);
			return $respuesta;
		}

		/**
		*Desc.  => Mostrar incapacidades exportar todas las incapacidades por estado y fechas
		*params => $empresa, $fecha_inicio, $fecha_final, $estado_tramite
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrMostrarIncapacidades_ConsolidadoEstado($empresa, $fecha_inicio, $fecha_final, $estado_tramite){
			$tabla = "gi_incapacidad";
			$respuesta = ModeloIncapacidades::mdlMostrarConsolidadoEstados($tabla, $empresa, $fecha_inicio, $fecha_final, $estado_tramite);
			return $respuesta;
		}

		/**
		*Desc.  => Traer los estados, pero para dibujar las estadisticas
		*params => $item, $valor
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrVerEstadosChart( $item, $valor){
			$respuesta = ModeloIncapacidades::mdlVerEstadosChart( $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => ver estados de las incapacidades para chart x clientes (estadisticas)
		*params => $item, $valor
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrVerEstadosChart_clientes( $item, $valor , $item2, $valor2){
			$respuesta = ModeloIncapacidades::mdlVerEstadosChart_clientes( $item, $valor , $item2, $valor2);
			return $respuesta;
		}

		
		/**
		*Desc.  => ver estados de las incapacidades para chart (estadisticas)
		*params => $item, $valor
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrVerEstadosChart_w( $item, $valor){
			$respuesta = ModeloIncapacidades::mdlVerEstadosChart_w( $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => ver estados de las incapacidades para chart (estadisticas)
		*params => $item, $valor
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrVerEstadosChart_C( $item, $valor, $item2, $valor2){
			$respuesta = ModeloIncapacidades::mdlVerEstadosChart_c( $item, $valor, $item2, $valor2);
			return $respuesta;
		}

		/**
		*Desc.  => ver estados de las incapacidades
		*params => $item, $valor
		*Method => POST, GET
		*Return => array {}
		**/
		static public function ctrVerEstadosChart_Cx(){
			$respuesta = ModeloIncapacidades::mdlVerEstadosChart_cx();
			return $respuesta;
		}

		

		static public function ctrVerReporteCLienteGerencial($item, $valor, $item2, $valor2){
			$respuesta = ModeloIncapacidades::mdlVerEstadovalorPagado_e($item, $valor, $item2, $valor2);
			return $respuesta;
		}

		static public function ctrVerReporteGerencial($item , $valor){
			$respuesta = ModeloIncapacidades::mdlVerEstadovalorPagado($item, $valor);
			return $respuesta;
		}

		static public function ctrVerValorPagadoOficial($item, $valor){
			$respuesta = ModeloIncapacidades::mdlVerEstadovalorPagadoR($item, $valor);
			return $respuesta;
		}

		static public function ctrVerValorPagadoOficialcliente($item, $valor, $item2, $valor2){
			$respuesta = ModeloIncapacidades::mdlVerEstadovalorPagadoRC($item, $valor,  $item2, $valor2);
			return $respuesta;
		}

		/* Ver estado oirginales*/
		static public function ctrVerEstadosOrigen( $item, $valor){
			$respuesta = ModeloIncapacidades::mdlVerEstados_Originales_Chart_c($item, $valor);
			return $respuesta;
		}

		/* ver incapacidades de 15 dias*/
		static public function ctrVerIncapacidades15Dias( $item, $valor, $item2, $valor2){
			$respuesta = ModeloIncapacidades::mdlVerIncapacidades15Dias($item, $valor, $item2, $valor2);
			return $respuesta;
		}

		/* ver incapacidades de 30 dias*/
		static public function ctrVerIncapacidades30Dias( $item, $valor, $item2, $valor2){
			$respuesta = ModeloIncapacidades::mdlVerIncapacidades30Dias($item, $valor, $item2, $valor2);
			return $respuesta;
		}

		/* ver incapacidades de 45 dias*/
		static public function ctrVerIncapacidades45Dias( $item, $valor, $item2, $valor2){
			$respuesta = ModeloIncapacidades::mdlVerIncapacidades45Dias($item, $valor, $item2, $valor2);
			return $respuesta;
		}

		/* ver incapacidades de todos*/
		static public function ctrVerIncapacidadesMax( $item, $valor, $item2, $valor2){
			$respuesta = ModeloIncapacidades::mdlVerIncapacidadesMax($item, $valor, $item2, $valor2);
			return $respuesta;
		}

		/* Crear Incapacidad */
		static public function ctrCrearIncapacidad(){
			if(isset($_POST["NuevoCedulaIncapacidad"])){
				if(preg_match('/^[0-9]+$/', $_POST["NuevoCedulaIncapacidad"]))
				{
					date_default_timezone_set('America/Bogota');
					/*Validamos que la incapacidad no exista*/
				 	$item1 = "inc_cc_afiliado";
                    $valor1 = $_POST["NuevoCedulaIncapacidad"];
                    $item2 = 'inc_fecha_inicio';
                    $valor2 = $_POST["NuevoFechaInicio"];
                    $existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2, $_SESSION['cliente_id']);
                    $incapacidaDato = null;
                    if(!$existeInc){

						/* cargue de fotos */
						$tabla = "gi_incapacidad";
						$estado = "2";
						if(isset($_POST['NuevoEstadoTramite']) && $_POST['NuevoEstadoIncapacidad'] != 0){
							if($_POST['NumeroDias'] < 3 && $_POST['NuevoClasificacion'] == 1 ){
								/*Es empresa*/
								if($_POST["NuevoOrigen"] == '1' || $_POST["NuevoOrigen"] == '4'){
									$estado = "1";	
								}else if($_POST["NuevoOrigen"] == '4' && $_POST['NumeroDias'] < 2){
									$estado = "1";
								} 
							}else{
								$estado = $_POST['NuevoEstadoTramite'];
							}	
						}
						
						$NuevoClasificacion = 1;
						

						$NuevotxtFechaPagoNomina = null;
						if(isset($_POST['NuevotxtFechaPagoNomina']) && !is_null($_POST['NuevotxtFechaPagoNomina']) && $_POST['NuevotxtFechaPagoNomina'] != ''){
							$NuevotxtFechaPagoNomina = $_POST['NuevotxtFechaPagoNomina'];
						}

						$NuevoFechaSinReconocimiento = NULL;
						if(isset($_POST['NuevoFechaSinReconocimiento']) && $_POST['NuevoFechaSinReconocimiento'] != '' && !is_null($_POST['NuevoFechaSinReconocimiento'])){
							$NuevoFechaSinReconocimiento = $_POST['NuevoFechaSinReconocimiento'] ;
						}


						$NuevoFechaPagoIncapacidad = NULL;
						if(isset($_POST['NuevoFechaPagoIncapacidad']) && $_POST['NuevoFechaPagoIncapacidad'] != '' && !is_null($_POST['NuevoFechaPagoIncapacidad'])){
							$NuevoFechaPagoIncapacidad = $_POST['NuevoFechaPagoIncapacidad'] ;
						}
						

						$NuevoFechaRadicacionIncapacidad = NULL;
						if(isset($_POST['NuevoFechaRadicacionIncapacidad']) && $_POST['NuevoFechaRadicacionIncapacidad'] != '' && !is_null($_POST['NuevoFechaRadicacionIncapacidad'])){
							$NuevoFechaRadicacionIncapacidad = $_POST['NuevoFechaRadicacionIncapacidad'] ;
						}
						

						$NuevoFechaSolicitudIncapacidad = NULL;
						if(isset($_POST['NuevoFechaSolicitudIncapacidad']) && $_POST['NuevoFechaSolicitudIncapacidad'] != '' && !is_null($_POST['NuevoFechaSolicitudIncapacidad'])){
							$NuevoFechaSolicitudIncapacidad = $_POST['NuevoFechaSolicitudIncapacidad'] ;
						}

						$atencionIPS = 0;
						if(isset($_POST['NuevoAtencion']) &&  $_POST['NuevoAtencion'] != ''){
							$atencionIPS = $_POST['NuevoAtencion'];
						}

						$NuevoValorSolicitud = null;
						if(isset($_POST['NuevoValorSolicitud']) &&  $_POST['NuevoValorSolicitud'] != ''){
							$NuevoValorSolicitud = $_POST['NuevoValorSolicitud'];
						}

						$NuevoNumeroRadicado = null;
						if(isset($_POST['NuevoNumeroRadicado']) &&  $_POST['NuevoNumeroRadicado'] != ''){
							$NuevoNumeroRadicado = $_POST['NuevoNumeroRadicado'];
						}

						$NuevoMotivo = null;
						if(isset($_POST['NuevoMotivo']) &&  $_POST['NuevoMotivo'] != ''){
							$NuevoMotivo = $_POST['NuevoMotivo'];
						}

						$NuevoObservaciones = null;
						if(isset($_POST['NuevoObservaciones']) &&  $_POST['NuevoObservaciones'] != ''){
							$NuevoObservaciones = $_POST['NuevoObservaciones'];
						}

						$NuevoValorIncapacidad = null;
						if(isset($_POST['NuevoValorIncapacidad']) &&  $_POST['NuevoValorIncapacidad'] != ''){
							$NuevoValorIncapacidad = $_POST['NuevoValorIncapacidad'];
						}

						$subEstadoIncapacidad = 0;
						$clasificacion=1;
						if(isset($_POST['NuevoSubEstadoTramite']) && ($estado == 6 || $estado == 7)){
							$subEstadoIncapacidad = $_POST['NuevoSubEstadoTramite'];
						}

						$perteneceRed = 0;
						if (isset($_POST["NuevoPerteneceRed"]) && $_POST["NuevoPerteneceRed"] != '') {
							$perteneceRed = $_POST["NuevoPerteneceRed"];
						}

						$registradoEntidad = $_POST['NuevoRegistradoEntidad'];		

						$ruta =  "";
			            $ruta_2 =  "";

						$datos = array(
									"inc_ips_afiliado" 				=> $_POST["NuevoIpsAfiliado"],
									"inc_afp_afiliado" 				=> $_POST["NuevoAfpAfiliado"],
									"inc_arl_afiliado" 				=> $_POST["NuevoArlAfiliado"],
									"inc_cc_afiliado" 				=> $_POST["NuevoCedulaIncapacidad"],
									"inc_ivl_v"						=> $_POST["NuevoIvlempleado"],
									"inc_diagnostico" 				=> $_POST["NuevoDiagnostico"],
									"inc_origen" 					=> $_POST["NuevoOrigen"],
									"inc_clasificacion" 			=> $_POST['NuevoClasificacion'],
									"inc_fecha_inicio" 				=> $_POST["NuevoFechaInicio"],
									"inc_fecha_final" 				=> $_POST["NuevoFechaFinal"],
									"inc_tipo_generacion" 			=> $_POST["NuevoTipoGeneracion"],
									"inc_profesional_responsable" 	=> $_POST["NuevoProfesional"],
									"inc_donde_se_genera" 			=> $_POST["NuevoInstitucionGenera"],
									"inc_pertenece_red" 			=> $perteneceRed,
									"inc_estado" 					=> $_POST['NuevoEstadoIncapacidad'],
									"inc_empresa"					=> $_POST['NuevoNombreEmpresa'],
									"inc_emd_id"					=> $_POST['NuevoEmpleado'],
									"inc_estado_tramite"			=> $estado,
									"inc_ruta_incapacidad"			=> $ruta,
									"inc_ruta_incapacidad_transcrita" => $ruta_2,
									"inc_fecha_pago_nomina" 		=> $NuevotxtFechaPagoNomina,
									"inc_valor_pagado_empresa" 		=> $_POST['NuevoValorPagadoEmpresa'],
									"inc_valor_pagado_eps" 			=> $_POST['NuevoValorPagoEPS'],
									"inc_valor_pagado_ajuste" 		=> $_POST['NuevoValorPagoAjuste'],
									'inc_atn_id'                    => $atencionIPS,
									"inc_valor" 				=> $NuevoValorIncapacidad,
									"inc_fecha_pago" 			=> $NuevoFechaPagoIncapacidad,
									'inc_fecha_radicacion'		=> $NuevoFechaRadicacionIncapacidad,
									'inc_fecha_solicitud'		=> $NuevoFechaSolicitudIncapacidad,
									'inc_fecha_negacion_'		=> $NuevoFechaSinReconocimiento,
									'inc_observacion_neg'		=> null,
									'inc_valor_solicitado'      => $NuevoValorSolicitud,
									'inc_numero_radicado_v'		=> $NuevoNumeroRadicado,
									'inc_mot_rech_i' 			=> $NuevoMotivo,
									'inc_observacion_rech_t' 	=> $NuevoObservaciones,
									'inc_user_id_i'				=> $_SESSION['id_Usuario'],
									'inc_fecha_recepcion_d'     => $_POST['NuevoFechaRecepcion'],
									'inc_sub_estado_i'          => $subEstadoIncapacidad,
									'inc_registrada_entidad_i'  => $registradoEntidad
	 							); 
						$respuesta = ModeloIncapacidades::mdlIngresarIncapacidad($tabla, $datos);
						
						if($respuesta != 'Error'){
							self::setAuditoria('ControladorIncapacidades', 'ctrCrearIncapacidad',json_encode($datos),$respuesta, $respuesta);
							/*no se sabe si editaron las fechas*/
							if($_POST['NuevoEstadoIncapacidad'] == '0' && isset($_POST['EnvioCorreoRechazado']) ){
								self::sendCorreoIncompleto($respuesta);
							}
						 	date_default_timezone_set('America/Bogota');
							if(isset($_POST["NuevoObservacion"]) && $_POST["NuevoObservacion"] != ''){
								$campos = "inm_inc_id_i, incm_usuer_id_i, incm_comentario_t, incm_fecha_comentario";
								$valores = 	$respuesta.", ".$_SESSION['id_Usuario'].", '".$_POST["NuevoObservacion"]."','".date('Y-m-d H:i:s')."' ";
								ModeloIncapacidades::mdlCrear('gi_incapacidades_comentarios', $campos, $valores);
							}else if(isset($_POST["NuevoObservacion_segunda"]) && $_POST["NuevoObservacion_segunda"] != ''){
								$campos = "inm_inc_id_i, incm_usuer_id_i, incm_comentario_t, incm_fecha_comentario";
								$valores = 	$respuesta.", ".$_SESSION['id_Usuario'].", '".$_POST["NuevoObservacion_segunda"]."','".date('Y-m-d H:i:s')."' ";
								ModeloIncapacidades::mdlCrear('gi_incapacidades_comentarios', $campos, $valores);
							}

							$ruta =  "";
				            $ruta_2 =  "";
				            if(isset($_FILES['NuevoFotoIncapacidad']['tmp_name']) && !empty($_FILES['NuevoFotoIncapacidad']['tmp_name']) ){
				            	$nombre = $respuesta."-O-".date('Y_m_d_H_i_s');
				                $ruta = self::putImage($_FILES['NuevoFotoIncapacidad']['tmp_name'], $_FILES["NuevoFotoIncapacidad"]["type"] , __DIR__."/../vistas/img/incapacidades/".$_POST['NuevoCedulaIncapacidad'].'/original/', 'vistas/img/incapacidades/'.$_POST['NuevoCedulaIncapacidad'].'/original/', null, $nombre);
				            }

				            if(isset($_FILES['NuevoOtroIncapacidad']['tmp_name']) && !empty($_FILES['NuevoOtroIncapacidad']['tmp_name']) ){
				            	$nombre = $respuesta."-T-".date('Y_m_d_H_i_s');
				                $ruta_2 = self::putImage($_FILES['NuevoOtroIncapacidad']['tmp_name'], $_FILES["NuevoOtroIncapacidad"]["type"] , __DIR__."/../vistas/img/incapacidades/".$_POST['NuevoCedulaIncapacidad'].'/transcripcion/', 'vistas/img/incapacidades/'.$_POST['NuevoCedulaIncapacidad'].'/transcripcion/', null, $nombre);
				            }
				            /*Actualizo el tema de la imagen*/
				            $campos = "inc_ruta_incapacidad = '".$ruta."', inc_ruta_incapacidad_transcrita = '".$ruta_2."' ";
							$condiciones = 	"inc_id = ".$respuesta;
							ModeloIncapacidades::mdlEditar('gi_incapacidad', $campos, $condiciones);

							//Self::ctrLiquidacionAutomatica($respuesta);
							echo json_encode( array('code' => 1, 'message' => 'Incapacidad Creada con exito' ));
						}else {
							self::setAuditoria('ControladorIncapacidades', 'ctrCrearIncapacidad',json_encode($datos),$respuesta, $respuesta);
							
							echo json_encode( array('code' => 0,  'message' => 'Error, no se pudo insertar la incapacidad', 'error' => $respuesta ));
						}
					}else{
						echo json_encode( array('code' => -1 ,  'message' => 'Error, esta incapacidad ya esta creada en el sistema'));	
					}
				}else{
					echo json_encode( array('code' => -2 , 'message'=> 'La cedula no puede ir vacio o llevar caracteres especiales!'));
				}
			}
		}


		/* Editar Incapacidad */
		static public function ctrEditarIncapacidad(){
			if(isset($_POST["EditarCedulaIncapacidad"])){
				if(preg_match('/^[0-9]+$/', $_POST["EditarCedulaIncapacidad"]))
				{
					/* cargue de fotos */
					$ruta =  $_POST["ruta_incapacidad"];
		            $ruta_2 =  $_POST["ruta_otroincapacidad"];
		            date_default_timezone_set('America/Bogota');
					if(isset($_FILES['EditarFotoIncapacidad']['tmp_name']) && !empty($_FILES['EditarFotoIncapacidad']['tmp_name']) ){
							$nombre = $_POST['id_incapacidad']."-O-".date('Y_m_d_H_i_s');
			                $ruta = self::putImage($_FILES['EditarFotoIncapacidad']['tmp_name'], $_FILES["EditarFotoIncapacidad"]["type"] , __DIR__."/../vistas/img/incapacidades/".$_POST['EditarCedulaIncapacidad'].'/original/', 'vistas/img/incapacidades/'.$_POST['EditarCedulaIncapacidad'].'/original/', null, $nombre);
			            }

			            if(isset($_FILES['EditarOtroIncapacidad']['tmp_name']) && !empty($_FILES['EditarOtroIncapacidad']['tmp_name']) ){
			            	$nombre = $_POST['id_incapacidad']."-T-".date('Y_m_d_H_i_s');
			                $ruta_2 = self::putImage($_FILES['EditarOtroIncapacidad']['tmp_name'], $_FILES["EditarOtroIncapacidad"]["type"] , __DIR__."/../vistas/img/incapacidades/".$_POST['EditarCedulaIncapacidad'].'/transcripcion/', 'vistas/img/incapacidades/'.$_POST['EditarCedulaIncapacidad'].'/transcripcion/', null, $nombre);
			            }

					$tabla = "gi_incapacidad";

					$EditartxtFechaPagoNomina = null;
					if(!is_null($_POST['EditartxtFechaPagoNomina']) && $_POST['EditartxtFechaPagoNomina'] != ''){
						$EditartxtFechaPagoNomina = $_POST['EditartxtFechaPagoNomina'];
					}

					$estado = "2";
					if(isset($_POST['EditarEstadoTramite']) && $_POST['EditarEstadoTramite'] != "0"){
						$estado = $_POST['EditarEstadoTramite'];
						//sssecho "N entramos ".$estado;
					}

					$EditarFechaSinReconocimiento = NULL;
					if($_POST['EditarFechaSinReconocimiento'] != '' && !is_null($_POST['EditarFechaSinReconocimiento'])){
						$EditarFechaSinReconocimiento = $_POST['EditarFechaSinReconocimiento'] ;
					}


					$EditarFechaPagoIncapacidad = NULL;
					if($_POST['EditarFechaPagoIncapacidad'] != '' && !is_null($_POST['EditarFechaPagoIncapacidad'])){
						$EditarFechaPagoIncapacidad = $_POST['EditarFechaPagoIncapacidad'] ;
					}
					

					$EditarFechaRadicacionIncapacidad = NULL;
					if($_POST['EditarFechaRadicacionIncapacidad'] != '' && !is_null($_POST['EditarFechaRadicacionIncapacidad'])){
						$EditarFechaRadicacionIncapacidad = $_POST['EditarFechaRadicacionIncapacidad'] ;
					}
					

					$EditarFechaSolicitudIncapacidad = NULL;
					if($_POST['EditarFechaSolicitudIncapacidad'] != '' && !is_null($_POST['EditarFechaSolicitudIncapacidad'])){
						$EditarFechaSolicitudIncapacidad = $_POST['EditarFechaSolicitudIncapacidad'] ;
					}

					$atencionIPS = 0;
					if(isset($_POST['EditarAtencion']) &&  $_POST['EditarAtencion'] != ''){
						$atencionIPS = $_POST['EditarAtencion'];
					}

					$EditarValorSolicitud = null;
					if(isset($_POST['EditarValorSolicitud']) &&  $_POST['EditarValorSolicitud'] != ''){
						$EditarValorSolicitud = $_POST['EditarValorSolicitud'];
					}

					$EditarNumeroRadicado = null;
					if(isset($_POST['EditarNumeroRadicado']) &&  $_POST['EditarNumeroRadicado'] != ''){
						$EditarNumeroRadicado = $_POST['EditarNumeroRadicado'];
					}

					$EditarMotivo = null;
					if(isset($_POST['EditarMotivo']) &&  $_POST['EditarMotivo'] != ''){
						$EditarMotivo = $_POST['EditarMotivo'];
					}

					$txtObservaciones = null;
					if(isset($_POST['txtObservaciones']) &&  $_POST['txtObservaciones'] != ''){
						$txtObservaciones = $_POST['txtObservaciones'];
					}

					$EditarValorIncapacidad = null;
					if(isset($_POST['EditarValorIncapacidad']) &&  $_POST['EditarValorIncapacidad'] != ''){
						$EditarValorIncapacidad = $_POST['EditarValorIncapacidad'];
					}

					$editarTipoGeneracion = '0';
					if(isset($_POST["EditarTipoGeneracion"])){
						$editarTipoGeneracion = $_POST["EditarTipoGeneracion"];
					}

					$subEstadoIncapacidad = 0;
					if(isset($_POST['EditarSubEstadoTramite']) && ($estado == 6 || $estado == 7) ){
						$subEstadoIncapacidad = $_POST['EditarSubEstadoTramite'];
					}

					$editarPerteneceRed = 0;
					if (isset($_POST["EditarPerteneceRed"]) && $_POST["EditarPerteneceRed"] != '') {
						$editarPerteneceRed = $_POST["EditarPerteneceRed"];
					}

					$registradoEntidad = $_POST['EditarRegistradoEntidad'];

					$datos = array(
								"inc_ips_afiliado" 				=> $_POST["EditarIpsAfiliado"],
								"inc_afp_afiliado" 				=> $_POST["EditarAfpAfiliado"],
								"inc_arl_afiliado" 				=> $_POST["EditarArlAfiliado"],
								"inc_cc_afiliado" 				=> $_POST["EditarCedulaIncapacidad"],
								"inc_ivl_v"						=> $_POST["EditarIvlempleado"],
								"inc_diagnostico" 				=> $_POST["EditarDiagnostico"],
								"inc_origen" 					=> $_POST["EditarOrigen"],
								"inc_clasificacion" 			=> $_POST["EditarClasificacion"],
								"inc_fecha_inicio" 				=> $_POST["EditarFechaInicio"],
								"inc_fecha_final" 				=> $_POST["EditarFechaFinal"],
								"inc_tipo_generacion" 			=> $editarTipoGeneracion,
								"inc_profesional_responsable" 	=> $_POST["EditarProfesional"],
								"inc_donde_se_genera" 			=> $_POST["EditarInstitucionGenera"],
								"inc_pertenece_red"				=> $editarPerteneceRed,
								"inc_estado" 					=> $_POST['EditarEstadoIncapacidad'],
								"inc_id"						=> $_POST['id_incapacidad'],
								"inc_empresa"					=> $_POST['EditarNombreEmpresa'],
								"inc_emd_id"					=> $_POST['EditarEmpleado'],
								"inc_ruta_incapacidad"			=> $ruta,
								"inc_estado_tramite"			=> $estado,
								"inc_ruta_incapacidad_transcrita" => $ruta_2,
								"inc_fecha_pago_nomina" 		=> $EditartxtFechaPagoNomina,
								"inc_valor_pagado_empresa" 		=> $_POST['EditarValorPagadoEmpresa'],
								"inc_valor_pagado_eps" 			=> $_POST['EditarValorPagoEPS'],
								"inc_valor_pagado_ajuste" 		=> $_POST['EditarValorPagoAjuste'],
								'inc_atn_id'                    => $atencionIPS,
								"inc_valor" 				=> $EditarValorIncapacidad,
								"inc_fecha_pago" 			=> $EditarFechaPagoIncapacidad,
								'inc_fecha_radicacion'		=> $EditarFechaRadicacionIncapacidad,
								'inc_fecha_solicitud'		=> $EditarFechaSolicitudIncapacidad,
								'inc_fecha_negacion_'		=> $EditarFechaSinReconocimiento,
								'inc_observacion_neg'		=> null,
								'inc_valor_solicitado'      => $EditarValorSolicitud,
								'inc_numero_radicado_v'		=> $EditarNumeroRadicado,
								'inc_mot_rech_i' 			=> $EditarMotivo,
								'inc_observacion_rech_t' 	=> $txtObservaciones,
								'inc_edi_user_id_i' 		=> $_SESSION['id_Usuario'] ,
								'inc_fecha_recepcion_d'     => $_POST['EditarFechaRecepcion'],
								'inc_sub_estado_i'          => $subEstadoIncapacidad,
								'inc_registrada_entidad_i' 	=> $registradoEntidad
 							); 
					//var_dump($datos);
					$respuesta = ModeloIncapacidades::mdlEditarIncapacidad($tabla, $datos);
					self::setAuditoria('ControladorIncapacidades', 'ctrEditarIncapacidad',json_encode($datos),$respuesta, $_POST['id_incapacidad']);
					if($respuesta == 'ok'){
						/*no se sabe si editaron las fechas*/
						//Self::ctrLiquidacionAutomatica($_POST['id_incapacidad']);

						if(isset($_POST['nuevoEnvioCorreo'])){
							/*Toca enviar el correo*/
							selft::sendMailPagoIncapacidad($_POST['id_incapacidad']);
						}

						if($_POST["EditarEstadoTramite"] == 'SIN RECONOCIMIENTO'){
							/*Toca enviar el correo*/
							//self::sendMailSinReconocimiento($_POST['id_incapacidad']);
						}

						date_default_timezone_set('America/Bogota');
						if($_POST["EditarObservacion"] != '' ){
							if($_POST['idObservacionEditar'] == 0){
								$campos = "inm_inc_id_i, incm_usuer_id_i, incm_comentario_t, incm_fecha_comentario";
								$valores = 	$_POST['id_incapacidad'].", ".$_SESSION['id_Usuario'].", '".$_POST["EditarObservacion"]."','".date('Y-m-d H:i:s')."' ";
								ModeloIncapacidades::mdlCrear('gi_incapacidades_comentarios', $campos, $valores);
							}else{
								$campos = "inm_inc_id_i= '".$_POST['id_incapacidad']."', incm_usuer_id_i = '".$_SESSION['id_Usuario']."', incm_comentario_t='".$_POST["EditarObservacion"]."'";
								$condiciones = 	"incm_id_i = ".$_POST['idObservacionEditar'];
								ModeloIncapacidades::mdlEditar('gi_incapacidades_comentarios', $campos, $condiciones);
							}
							
						}

						echo json_encode(array('code' => 1, 'message' => 'Se edito la incapacidad'));
						
					}else {
						echo json_encode( array('code' => 0,  'message' => 'Error, no se pudo editar la incapacidad', 'error' => $respuesta ));
					}
				}else{
					echo json_encode( array('code' => -2 , 'message'=> 'La cedula no puede ir vacio o llevar caracteres especiales!'));
				}
			}
		}

		/*enviar correo de notificacion Pago*/
		static public function sendMailPagoIncapacidad($idIncapacidad)
		{
			$campos = " emp_email_gestion, inc_cc_afiliado, DATE_FORMAT(inc_fecha_inicio, \"%Y-%m-%d\") as inc_fecha_inicio, DATE_FORMAT(inc_fecha_final, \"%Y-%m-%d\") as inc_fecha_final, DATE_FORMAT(inc_fecha_pago, \"%Y-%m-%d\") as inc_fecha_pago, inc_valor, emd_nombre, inc_clasificacion, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado ,  ip4.ips_nombre as inc_afp_afiliado , inc_origen, ip2.ips_nit as eps_nit, ip3.ips_nit as arl_nit, ip4.ips_nit as afp_nit, emp_nombre , emp_nit, emp_correo_incapacidades_v, emp_crv_v, inc_ruta_incapacidad";
			$tabla_ = " gi_empresa JOIN gi_incapacidad ON inc_empresa = emp_id JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id LEFT JOIN gi_ips ip2 ON ip2.ips_id = inc_ips_afiliado ";
			$where_ = " inc_id = ".$_POST['id_incapacidad'];

			$resultado = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla_, $where_);
			if($resultado != null){
				if($resultado['emp_email_gestion'] != null && $resultado['emp_email_gestion'] != '' ){
					/*Procedemos a enviar el Hp correo*/

					$administradora = null;
					$ips_nit = null;
					if($resultado['inc_clasificacion'] != 4){
			            if($resultado['inc_origen'] == 'ACCIDENTE LABORAL'){
			                $administradora =  $resultado["inc_arl_afiliado"];  
			                $ips_nit = $resultado['arl_nit'];
			            }else{
			                $administradora = $resultado["inc_ips_afiliado"];  
			                $ips_nit = $resultado['eps_nit'];
			            }
			        }else{
			            $administradora = $resultado["inc_afp_afiliado"];  
				            $ips_nit = $resultado['afp_nit'];  
				        }

						$para  = $resultado['emp_email_gestion'];
						$titulo = 'Notificación pagos de incapacidades';
						$mensaje = '
<html>
	<head>
		<title>Notificaci&oacute;n pagos de incapacidades</title>
	</head>
	<body style="text-align:justify;">
  		<p>Saludos cordiales,</p>
  		<p style="text-align:justify;">
  			Nos permitimos informarles que le ha sido abonada a su cuenta bancaria un pago por concepto de incapacidades, de acuerdo a la informaci&oacute;n que a continuaci&oacute;n detallamos:
  		</p>
  		<div style="width=100%;">
  			<table border="1" style="margin: 0 auto 0;" cellpadding="1px" cellspacing="0px"  width="90%">
	  			<tr>
	  				<td width="50%">EMPRESA</td>
	  				<td>'.mb_strtoupper($resultado['emp_nombre']).'</td>
	  			</tr>
	  			<tr>
	  				<td width="50%">NIT</td>
	  				<td>'.mb_strtoupper($resultado['emp_nit']).'</td>
	  			</tr>
	  			<tr>
	  				<td width="50%">EMPLEADO</td>
	  				<td>'.mb_strtoupper($resultado['emd_nombre']).'</td>
	  			</tr>
	  			<tr>
	  				<td>CEDULA</td>
	  				<td>'.$resultado['inc_cc_afiliado'].'</td>
	  			</tr>
	  			<tr>
	  				<td>VALOR PAGADO</td>
	  				<td>$ '.number_format($resultado['inc_valor'], '0', ',', '.').'</td>
	  			</tr>
	  			<tr>
	  				<td>FECHA DE PAGO</td>
	  				<td>'.$resultado['inc_fecha_pago'].'</td>
	  			</tr>
	  			<tr>
	  				<td>ENTIDAD</td>
	  				<td>'.mb_strtoupper($administradora).'</td>
	  			</tr>
	  			<tr>
	  				<td>NIT ENTIDAD</td>
	  				<td>'.$ips_nit.'</td>
	  			</tr>
	  		</table>
	  		<br/>
	  		<p>DETALLE INCAPACIDAD</p>
	  		<table border="1" style="margin: 0 auto 0;" cellpadding="1px" cellspacing="0px" width="90%">
	  			<tr>
	  				<td>FECHA INICIO</td>
	  				<td>FECHA FINAL</td>
	  			</tr>
	  			<tr>
	  				<td>'.$resultado['inc_fecha_inicio'].'</td>
	  				<td>'.$resultado['inc_fecha_final'].'</td>
	  			</tr>
	  		</table>
  		</div>
  		
  		<p style="text-align:justify;">Esta informaci&oacute;n tambi&eacute;n puede obtenerla a trav&eacute;s de nuestro portal transaccional en <a href="https://incapacidades.co">www.incapacidades.co</a>, acceso portal ingresando con su usuario y contraseña; si a&uacute;n no cuenta con la clave podr&aacute; solicitarla en <a href="mailto:info@incapacidades.co">info@incapacidades.co</a>.</p>
  		<br/>
  		<br/>
  		<p>Cordialmente,</p>
  		<br/>
  		<br/>
  		<p>Prestaciones Econ&oacute;micas<br/>www.incapacidades.co</p>
		<br/>
  		<br/>
  		<p style="text-align:justify;">
  			<b>CONFIDENCIALIDAD:</b> La informaci&oacute;n transmitida a trav&eacute;s de este correo electr&oacute;nico es confidencial y dirigida &Uacute;nica y exclusivamente para uso de su(s) detinatario(s). Su reproducci&oacute;n, lectura o uso est&aacute; prohibido a cualquier persona o entidad diferente, sin autorizaci&oacute;n previa por escrito. Si usted lo ha recibido por error, por favor notifiquelo inmediatamente al remitente y elim&iacute;nelo de su sistema. Cualquier uso, divulgaci&oacute;n copia, distribuci&oacute;n impresi&oacute;n o acto derivado del conocimiento total o parcial de este mensaje sin autorizaci&oacute;n del remitente ser&aacute; sancionado de acuerdo con las normas legales vigentes.
  		</p>
	</body>
</html>';
					/*$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
					$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$cabeceras .= 'Bcc: Jose Giron <jgiron9001@gmail.com>' . "\r\n";
					$cabeceras .= 'From: Información Incapacidades.co <info@incapacidades.co>' . "\r\n";
					//mail($para, $titulo, $mensaje, $cabeceras);*/
					$adjunto = null;
		            if($resultado['inc_ruta_incapacidad'] != null){
		               $adjunto = __DIR__.'/../'.$resultado['inc_ruta_incapacidad'];
		            } 
					self::EnviarMailWithEmailAndPass($resultado['emp_correo_incapacidades_v'], $resultado['emp_crv_v'], 'Notificaciones '.$resultado['emp_nombre'], 'Notificación pagos de incapacidades', $mensaje, $para, null );
				}
			}
		}

		/*funcion para notificar una incapacidad sin reconocimiento*/
		static public function sendMailSinReconocimiento($idIncapacidad){
			$campos = "inc_id,  inc_fecha_negacion_, inc_observacion_neg, DATE_FORMAT(inc_fecha_inicio, '%d/%m/%Y') as inc_fecha_inicio, DATE_FORMAT(inc_fecha_final, '%d/%m/%Y') AS inc_fecha_final, emp_correo_1, emp_correo_2, emp_correo_3, emp_correo_4, emd_sede, DATEDIFF(inc_fecha_final,inc_fecha_inicio) as total_dias, emd_nombre, emd_cedula, emd_cargo, ips_nombre, emp_correo_incapacidades_v, emp_crv_v, emp_nombre, inc_ruta_incapacidad";
			$tabla_ = "gi_incapacidad JOIN gi_empleados ON gi_empleados.emd_id = gi_incapacidad.inc_emd_id JOIN gi_empresa ON gi_empleados.emd_emp_id = gi_empresa.emp_id JOIN gi_ips ON gi_empleados.emd_eps_id = gi_ips.ips_id";
			$where_ = "inc_id = ".$_POST['id_incapacidad'];

			$resultado = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla_, $where_ );

			if($resultado != false){

				$titulo = 'INCAPACIDADES SIN RECONOCIMIENTO '.$resultado['emd_cedula'];
				$mensaje = '
<html>
<head>
<title>INCAPACIDADES SIN RECONOCIMIENTO '.$resultado['inc_id'].'</title>
</head>
<body style="text-align:justify;">
<p style="text-align:justify;">
	Saludos cordiales,
	<br/>
	Este mensaje ha sido enviado debido a que su EPS y/o ARL ha negado el reconocimiento económico de su incapacidad y/o licencia. 
<br/>
Datos de la incapacidad y/o licencia: 
</p>
<div>
	<table border="1" style="margin: 0 auto 0;" cellpadding="1px" cellspacing="0px"  width="60%">
		<tr>
			<td width="30%">EMPLEADO</td>
			<td>'.mb_strtoupper($resultado['emd_nombre']).'</td>
		</tr>
		<tr>
			<td width="30%">IDENTIFICACIÓN</td>
			<td>'.mb_strtoupper($resultado['emd_cedula']).'</td>
		</tr>
		<tr>
			<td width="30%">CARGO</td>
			<td>'.mb_strtoupper($resultado['emd_cargo']).'</td>
		</tr>
		<tr>
			<td>SEDE</td>
			<td>'.$resultado['emd_sede'].'</td>
		</tr>
		<tr>
			<td>FECHA INICIO</td>
			<td>'.$resultado['inc_fecha_inicio'].'</td>
		</tr>
		<tr>
			<td>FECHA FINAL</td>
			<td>'.$resultado['inc_fecha_final'].'</td>
		</tr>
		<tr>
			<td>DÍAS</td>
			<td>'.($resultado['total_dias'] + 1).'</td>
		</tr>
		<tr>
			<td>ENTIDAD</td>
			<td>'.mb_strtoupper($resultado['ips_nombre']).'</td>
		</tr>
		<tr>
			<td>MOTIVO</td>
			<td>'.$resultado['inc_observacion_neg'].'</td>
		</tr>
	</table>
</div>

<p style="text-align:justify;">Muchas gracias</p>
<br/>
<br/>
<p>Cordialmente,</p>
<br/>
<br/>
<p>GESTIÓN DE INCAPACIDADES LABORALES <br/>www.incapacidades.co</p>
<br/>
<br/>
<p style="text-align:justify;">
	<b>CONFIDENCIALIDAD:</b> La informaci&oacute;n transmitida a trav&eacute;s de este correo electr&oacute;nico es confidencial y dirigida &Uacute;nica y exclusivamente para uso de su(s) detinatario(s). Su reproducci&oacute;n, lectura o uso est&aacute; prohibido a cualquier persona o entidad diferente, sin autorizaci&oacute;n previa por escrito. Si usted lo ha recibido por error, por favor notifiquelo inmediatamente al remitente y elim&iacute;nelo de su sistema. Cualquier uso, divulgaci&oacute;n copia, distribuci&oacute;n impresi&oacute;n o acto derivado del conocimiento total o parcial de este mensaje sin autorizaci&oacute;n del remitente ser&aacute; sancionado de acuerdo con las normas legales vigentes.
</p>
</body>
</html>';

				$correosEbviad = '';
	            if($resultado['emp_correo_1'] != null && $resultado['emp_correo_1'] != ''){
	              //mail($resultado['emp_correo_1'], $titulo, $mensajex, $cabeceras);
	               $correosEbviad .= $resultado['emp_correo_1']; 
	            }
	    
	            if($resultado['emp_correo_2'] != null && $resultado['emp_correo_2'] != ''){
	              //mail($resultado['emp_correo_2'], $titulo, $mensajex, $cabeceras);
	              $separador = ',';
	              if( $correosEbviad  == ''){
	                  $separador = '';    
	              }
	              $correosEbviad .=  $separador.$resultado['emp_correo_2'];
	            }
	    
	            if($resultado['emp_correo_3'] != null && $resultado['emp_correo_3'] != ''){
	              //mail($resultado['emp_correo_3'], $titulo, $mensajex, $cabeceras);
	              //mail($resultado['emp_correo_2'], $titulo, $mensajex, $cabeceras);
	              $separador = ',';
	              if( $correosEbviad  == ''){
	                  $separador = '';    
	              }
	              $correosEbviad .=  $separador.$resultado['emp_correo_3'];
	            }
	    
	            if($resultado['emp_correo_4'] != null && $resultado['emp_correo_4'] != ''){
	              //mail($resultado['emp_correo_4'], $titulo, $mensajex, $cabeceras);
	              //mail($resultado['emp_correo_2'], $titulo, $mensajex, $cabeceras);
	              $separador = ',';
	              if( $correosEbviad  == ''){
	                  $separador = '';    
	              }
	              $correosEbviad .=  $separador.$resultado['emp_correo_4'];
	            }

	            $adjunto = null;
	            if($resultado['inc_ruta_incapacidad'] != null){
	               $adjunto = __DIR__.'/../'.$resultado['inc_ruta_incapacidad'];
	            } 

				self::EnviarMailWithEmailAndPass($resultado['emp_correo_incapacidades_v'], $resultado['emp_crv_v'], 'Notificaciones '.$resultado['emp_nombre'], $titulo, $mensaje, $correosEbviad, $adjunto );

			}

		}

		static public function sendCorreoIncompleto($incapacidad)
		{
			
			$campo_ = "inc_id, inc_cc_afiliado, DATE_FORMAT(inc_fecha_inicio, '%d/%m/%Y') as inc_fecha_inicio , DATE_FORMAT(inc_fecha_final, '%d/%m/%Y') as inc_fecha_final  , inc_estado, inc_fecha_generada, inc_emd_id, emd_nombre, emd_cargo, emp_nombre, mot_desc_v, emp_correo_1, emp_correo_2, emp_correo_3, emp_correo_4, emd_sede, mot_observacion_v, emd_subcentro_costos, inc_ruta_incapacidad, emp_correo_incapacidades_v, emp_crv_v, ips_nombre, inc_fecha_inicio as Fi, inc_fecha_final as Ff";
        	$tabla_ = " gi_incapacidad JOIN gi_empleados ON inc_emd_id = emd_id JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_motivos_rechazo ON inc_mot_rech_i = mot_id_i JOIN gi_ips ON emd_eps_id = ips_id";
        	$condi_ = " inc_id = ".$incapacidad;

        	$resultado = ModeloIncapacidades::mdlMostrarUnitario($campo_, $tabla_, $condi_ );
        
        	
	        /*si el resultado no es Null*/
	        if($resultado != false){

	        $resultadoObser = ModeloIncapacidades::mdlMostrarGroupAndOrder('incm_comentario_t','gi_incapacidades_comentarios', 'inm_inc_id_i = '.$incapacidad, null, 'ORDER BY incm_fecha_comentario ASC', 'LIMIT 1');
	        $obser = '';
	        foreach($resultadoObser as $obser => $valueObse){
	        	$obser = $valueObse['incm_comentario_t'];
	        }
	          
	        $titulo = 'SOPORTES INCOMPLETOS INCAPACIDADES_'.$resultado['inc_id'];
	        $mensaje = '
<html>
  <head>
    <title>SOPORTES INCOMPLETOS INCAPACIDADES_'.$resultado['inc_id'].'</title>
  </head>
  <body style="text-align:justify;">
      <p>Saludos Cordiales,</p>
      <p>Este mensaje ha sido enviado debido a que los soportes de su incapacidad y/o licencia, se encuentran incompletos </p>
      <p>Datos de la incapacidad y/o licencia:</p>
      <p>
        <table border="1" width="100%" style="border:solid 1px;">
          
            <tr style="border:solid 1px;">
              <th>Empleado</th>
              <td>'.$resultado['emd_nombre'].'</td>
            </tr>
            <tr style="border:solid 1px;">
              <th>Identificación</th>
              <td>'.$resultado['inc_cc_afiliado'].'</td>
            </tr>
            <tr style="border:solid 1px;">  
              <th>Cargo</th>
              <td>'.$resultado['emd_cargo'].'</td>
            </tr>
            <tr style="border:solid 1px;">
              <th>Sede</th>
              <td>'.$resultado['emd_sede'].'</td>
            </tr>
            <tr style="border:solid 1px;">
              <th>EPS</th>
              <td>'.$resultado['ips_nombre'].'</td>
            </tr>
            <tr style="border:solid 1px;">
              <th>Fecha Inicio</th>
              <td>'.$resultado['inc_fecha_inicio'].'</td>
            </tr>
            <tr style="border:solid 1px;">
              <th>Fecha Final</th>
              <td>'.$resultado['inc_fecha_final'].'</td>
            </tr>
            <tr style="border:solid 1px;">
              <th>Días</th>
              <td>'.self::dias_transcurridos($resultado['Fi'], $resultado['Ff']).'</td>
            </tr>
            <tr style="border:solid 1px;">
              <th>Motivo</th>
              <td>'.$resultado['mot_desc_v'].'</td>
            </tr>
            <tr style="border:solid 1px;">
              <th>Observaci&oacute;n</th>
              <td>'.$resultado['mot_observacion_v'].'</td>
            </tr>
        </table>
      </p>
      <p>Favor enviar los soportes pendientes en el menor tiempo posible, a su departamento de nómina y beneficios.</p>
      <p>
        Documentos para pago de incapacidades:
        <table border="1" style="border:solid 1px;">
          <tr style="border:solid 1px;">
            <th>Enfermedad General y Accidente Laboral</th>
            <td><ul><li>Certificado de incapacidad original</li><li>Copia de historia clínica</li></td>
          </tr>
          <tr>
            <th>Accidente de Tránsito</th>
            <td><ul><li>Certificado de incapacidad original</li><li>Copia de historia clínica</li><li>FURIPS (Reporte de Accidente de Tránsito) y SOAT</li></td>
          </tr>
          <tr>
            <th>Licencia de Maternidad</th>
            <td><ul><li>Certificado de incapacidad original</li><li>Copia de historia clínica (Debe contener semanas de gestación y fecha probable de parto)</li><li>Registro civil de nacimiento</li></td>
          </tr>
          <tr>
            <th>Licencia de Paternidad</th>
            <td><ul><li>Registro civil de nacimiento</li><li>Cédula del padre</li><li>Copia de historia clínica de la madre</li></td>
          </tr>
        </table>
      </p>

      <p>Muchas gracias,</p>
      <br/>
      <br/>
      <p><b>EQUIPO INCAPACIDADES LABORALES</b></p>
  </body>
</html>';
      

         
	            $correosEbviad = '';
	            if($resultado['emp_correo_1'] != null && $resultado['emp_correo_1'] != ''){
	              //mail($resultado['emp_correo_1'], $titulo, $mensajex, $cabeceras);
	               $correosEbviad .= $resultado['emp_correo_1']; 
	            }
	    
	            if($resultado['emp_correo_2'] != null && $resultado['emp_correo_2'] != ''){
	         
	              $separador = ',';
	              if( $correosEbviad  == ''){
	                  $separador = '';    
	              }
	              $correosEbviad .=  $separador.$resultado['emp_correo_2'];
	            }
	    
	            if($resultado['emp_correo_3'] != null && $resultado['emp_correo_3'] != ''){
	           
	              $separador = ',';
	              if( $correosEbviad  == ''){
	                  $separador = '';    
	              }
	              $correosEbviad .=  $separador.$resultado['emp_correo_3'];
	            }
	    
	            if($resultado['emp_correo_4'] != null && $resultado['emp_correo_4'] != ''){
	          
	              $separador = ',';
	              if( $correosEbviad  == ''){
	                  $separador = '';    
	              }
	              $correosEbviad .=  $separador.$resultado['emp_correo_4'];
	            }
	            
	            $adjunto = null;
	            if($resultado['inc_ruta_incapacidad'] != null){
	               $adjunto = __DIR__.'/../'.$resultado['inc_ruta_incapacidad'];
	            } 

	              
	            date_default_timezone_set('America/Bogota');
	            $resulCont = ModeloIncapacidades::mdlMostrarUnitario('inc_numero_notificaciones_i', 'gi_incapacidad', "inc_id = ".$incapacidad );

	            $xCampos = '';
	            if($resulCont['inc_numero_notificaciones_i'] == '0' || $resulCont['inc_numero_notificaciones_i'] == null){
	            	$xCampos = " inc_numero_notificaciones_i = 1 , 	inc_fecha_primera_notificacion_d = '".date('Y-m-d H:i:s')."'";
	            }else{
	            	$xCampos = " inc_numero_notificaciones_i = inc_numero_notificaciones_i + 1 , 	inc_fecha_ultima_notificacion_d = '".date('Y-m-d H:i:s')."'";
	            }

	            $condi_ = " inc_id = ".$incapacidad;
	            $xRespuesta = ModeloIncapacidades::mdlEditar('gi_incapacidad', $xCampos, $condi_);

	         
	           
	            self::EnviarMailWithEmailAndPass($resultado['emp_correo_incapacidades_v'], $resultado['emp_crv_v'], 'Notificaciones '.$resultado['emp_nombre'], $titulo, $mensaje, $correosEbviad, $adjunto );
	        }
		}

		/* Edtar los datos de la factura*/
		static public function ctrEditarFacturacion(){
			if(isset($_POST["NuevoNumeroFactura"])){
				
				$tabla = "gi_incapacidad";
				$datos = array(
							"inc_numero_factura" 				=> $_POST["NuevoNumeroFactura"],
							"inc_fecha_emision_factura" 		=> $_POST["NuevoFechaRadicacion"],
							"inc_fecha_pago_factura" 			=> $_POST["NuevoFechaPago"],
							"fecha1"							=> $_POST['fecha1'],
							"fecha2"							=> $_POST['fecha2'],
							"empresa"							=> $_POST['EmpresaFacturada']
							); 
				
				$respuesta = ModeloIncapacidades::mdlAgregarDatosFacturacion($tabla, $datos);
				if($respuesta == 'ok'){
					echo'<script>
						swal({
							  	type: "success",
							 	title: "Facturacion creada correctamente",
							  	showConfirmButton: true,
							  	confirmButtonText: "Cerrar",
							  	closeOnConfirm: false
						  	}, function() {
								
							});
						</script>';
				}else {
					var_dump($respuesta);
				}
			}
		}

		/* Eliminar incapacidad */
		static public function ctrBorrarIncapacidad(){

			if(isset($_POST["borrarIncapacidad"])){
				$tabla ="gi_incapacidad";
				$datos = $_POST["borrarIncapacidad"];
				$respuesta = ModeloIncapacidades::mdlBorrarIncapacidades($tabla, $datos);
				self::setAuditoria('ControladorIncapacidades', 'ctrBorrarIncapacidad',$datos,$respuesta,  $_POST["borrarIncapacidad"]);
				if($respuesta == "ok"){
					echo json_encode( array('code' =>1, 'message' => "incapacidad Borrada"));
				}else{
					echo json_encode( array('code' =>0, 'message' => "incapacidad no Borrada"));
				}
			}
		}	

		/* Cargue maxivo de Incapacidades */
		static public function ctrCargaMaxivaIncapacidades(){
			if(isset($_FILES['NuevoIncapacidad']['tmp_name']) && !empty($_FILES['NuevoIncapacidad']['tmp_name']) ){
				$name   = $_FILES['NuevoIncapacidad']['name'];
        		$tname  = $_FILES['NuevoIncapacidad']['tmp_name'];
        		ini_set('memory_limit','128M');


        		$obj_excel = PHPExcel_IOFactory::load($tname);
        		$sheetData = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
		        $arr_datos = array();
		        $highestColumm = $obj_excel->setActiveSheetIndex(0)->getHighestColumn(); // e.g. "EL"
				$highestRow = $obj_excel->setActiveSheetIndex(0)->getHighestRow();
				
				$aciertos = 0;
				$fallos   = 0;
				$total = 0;
				$existentes = 0;
				$datoCsv = array();
				$noexiste = 0;
				$incapacidadesExistentes = 0;
				foreach ($sheetData as $index => $value) {
            		if ( $index > 1 ){
            			if($value['A'] == 2){
            				$valido = 0;
	            			$existe = false;
	            			$separador = '';
	            			$total++;

	                		if((!is_null($value['B']) && !empty($value['B'])) && 
	                    		(!is_null($value['D']) && !empty($value['D'])) && 
	                    		(!is_null($value['E']) && !empty($value['E']))
	                		){
	                			/* Iniciamos la consulta Sql */
	                			$datos = array();

	                			/* Identificacion */
	                			if(!is_null($value['B']) && !empty($value['B']) ){
	                				$valido = 1;
	                				$datos['inc_cc_afiliado'] = $value['B'];	

	                				$item = 'emd_cedula';
	                				$valuess = $value['B'];
	                				$tabla = 'gi_empleados';
	                				$res = ModeloEmpleados::mdlMostrarEmpleados($tabla, $item, $valuess);
	                				
	                				if($res['emd_cedula'] == $value['B']){
	                					/*Empleado Existente*/
	                					$existe = true;
	                					$datos['inc_emd_id'] = $res['emd_id'];
	                					$datos['inc_empresa'] = $res['emd_emp_id'];
	                					$datos['inc_ips_afiliado'] = $res['emd_eps_id'];
	                				}else{
	                					/* No existe */
	                					$existe = false;
	                					$datoCsv[$noexiste]['Tipo de Identificacion'] = $value['A'];
	                					$datoCsv[$noexiste]['Identificacion'] = $value['B'];
	                					$datoCsv[$noexiste]['Nombres'] = $value['C'];
	                					$datoCsv[$noexiste]['Fecha Inicio'] = $value['D'];
	                					$datoCsv[$noexiste]['Fecha Final'] = $value['E'];
	                					$datoCsv[$noexiste]['Origen'] = $value['F'];
	                					$datoCsv[$noexiste]['Diagnostico'] = $value['G'];
	                					$datoCsv[$noexiste]['Estado'] = $value['H'];
	                					$datoCsv[$noexiste]['Observacion'] = $value['I'];
	                					$noexiste++;
	                				}
	                			}else{
	                				$datos['inc_cc_afiliado'] = NULL;
	                			}

	                			/* Nombres */
	                			if(!is_null($value['D']) && !empty($value['D']) ){
	                				$valido = 1;
	                				$datos['inc_fecha_inicio'] = $value['D'];	
	                			}else{
	                				$datos['inc_fecha_inicio'] = NULL;
	                			}

	                			/* Fecha Nacimiento */
	                			if(!is_null($value['E']) && !empty($value['E']) ){
	                				$datos['inc_fecha_final'] = $value['E'];
	                				$valido = 1;
	                			}else{
	                				$datos['inc_fecha_final'] = NULL;
	                			}

	                			/* Fecha ingreso */
	                			if(!is_null($value['F']) && !empty($value['F']) ){
	                				$datos['inc_origen'] = $value['F'];
	                				$valido = 1;
	                			}else{
	                				$datos['inc_origen'] = NULL;
	                			}

	                			/* Diagnostico */
	                			if(!is_null($value['G']) && !empty($value['G']) ){
	                				$valido = 1;
	                				$datos['inc_diagnostico'] = $value['G'];	
	                			}else{
	                				$datos['inc_diagnostico'] = NULL;
	                			}

	                			/* Fecha Nacimiento */
	                			if(!is_null($value['H']) && !empty($value['H']) ){
	                				if($value['H'] == 'Original'){
	                					$datos['inc_tipo_generacion'] = 1;
	                				}else{
	                					$datos['inc_tipo_generacion'] = 2;
	                				}
	                				$valido = 1;
	                			}else{
	                				$datos['inc_tipo_generacion'] = NULL;
	                			}

	                			/* Fecha ingreso */
	                			if(!is_null($value['I']) && !empty($value['I']) ){
	                				if($value['I'] == 'Inicial'){
	                					$datos['inc_clasificacion'] = 1;
	                				}else if($value['I'] == 'Prorroga'){
	                					$datos['inc_clasificacion'] = 2;
	                				}else if($value['I'] == 'Prorroga + 90 día'){
	                					$datos['inc_clasificacion'] = 3;
	                				}else if($value['I'] == 'Prorroga + 180 días'){
	                					$datos['inc_clasificacion'] = 4;
	                				}else if($value['I'] == 'Prorroga + 540 días'){
	                					$datos['inc_clasificacion'] = 5;
	                				}

	                				$valido = 1;
	                			}else{
	                				$datos['inc_clasificacion'] = NULL;
	                			}

	                			/* Fecha ingreso */
	                			if(!is_null($value['J']) && !empty($value['J']) ){
	                				$datos['inc_observacion'] = $value['J'];
	                				$valido = 1;
	                			}else{
	                				$datos['inc_observacion'] = NULL;
	                			}

	                			/* Estado tramite */
	                			if(!is_null($value['K']) && !empty($value['K']) ){
	                				$datos['inc_estado_tramite'] = $value['K'];
	                				$valido = 1;
	                			}else{
	                				$datos['inc_estado_tramite'] = NULL;
	                			}

	                			/* valor pagado */
	                			if(!is_null($value['L']) && !empty($value['L']) ){
	                				$datos['inc_valor'] = $value['L'];
	                				$valido = 1;
	                			}else{
	                				$datos['inc_valor'] = NULL;
	                			}

	                			/* Fecha ingreso */
	                			if(!is_null($value['M']) && !empty($value['M']) ){
	                				$datos['inc_fecha_pago'] = $value['M'];
	                				$valido = 1;
	                			}else{
	                				$datos['inc_fecha_pago'] = NULL;
	                			}


	                			/* Valor Pagado Nomina */
	                			if(!is_null($value['N']) && !empty($value['N']) ){
	                				$datos['inc_valor_pagado_nomina'] = $value['N'];
	                				$valido = 1;
	                			}else{
	                				$datos['inc_valor_pagado_nomina'] = NULL;
	                			}

	                			/* Fecha Pago Nomina */
	                			if(!is_null($value['O']) && !empty($value['O']) ){
	                				$datos['inc_fecha_pago_nomina'] = $value['O'];
	                				$valido = 1;
	                			}else{
	                				$datos['inc_fecha_pago_nomina'] = NULL;
	                			}


	                			$tabla = "gi_incapacidad";	
	                			$datos['inc_estado'] = 3;						
								if($valido == 1){
									if($existe){
										$item1 = "inc_cc_afiliado";
										$valor1 = $value['B'];
										$item2 = 'inc_fecha_inicio';
										$valor2 = $value['D'];
										$existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2);
										if(!$existeInc){
											$respuesta = ModeloIncapacidades::mdlIngresarIncapacidad($tabla, $datos);
											if($respuesta != 'Error'){
												$aciertos++;
												/*LiquidacionAutomatica*/
												Self::ctrLiquidacionAutomatica($respuesta);
											}else{
												$fallos++;
											}
										}else{
											$incapacidadesExistentes++;
										}	
									}
								}
	                		}
            			}else if($value['A'] == 3){

            				$valido = 0;
	            			$existe = false;
	            			$separador = '';
	            			$total++;

            				if((!is_null($value['B']) && !empty($value['B'])) && 
	                    		(!is_null($value['D']) && !empty($value['D'])) && 
	                    		(!is_null($value['E']) && !empty($value['E']))
	                		){
	                			/* Iniciamos la consulta Sql */
	                			$datos = array();

	                			/* Identificacion */
	                			if(!is_null($value['B']) && !empty($value['B']) ){
	                				$valido = 1;
	                				$datos['inc_cc_afiliado'] = $value['B'];	

	                				$item = 'emd_cedula';
	                				$valuess = $value['B'];
	                				$tabla = 'gi_empleados';
	                				$res = ModeloEmpleados::mdlMostrarEmpleados($tabla, $item, $valuess);
	                				
	                				if($res['emd_cedula'] == $value['B']){
	                					/*Empleado Existente*/
	                					$existe = true;
	                					$datos['inc_emd_id'] = $res['emd_id'];
	                					$datos['inc_empresa'] = $res['emd_emp_id'];
	                					$datos['inc_ips_afiliado'] = $res['emd_eps_id'];
	                				}else{
	                					/* No existe */
	                					$existe = false;
	                					$datoCsv[$noexiste]['Tipo de Identificacion'] = $value['A'];
	                					$datoCsv[$noexiste]['Identificacion'] = $value['B'];
	                					$datoCsv[$noexiste]['Nombres'] = $value['C'];
	                					$datoCsv[$noexiste]['Fecha Inicio'] = $value['D'];
	                					$datoCsv[$noexiste]['Fecha Final'] = $value['E'];
	                					$datoCsv[$noexiste]['Origen'] = $value['F'];
	                					$datoCsv[$noexiste]['Diagnostico'] = $value['G'];
	                					$datoCsv[$noexiste]['Estado'] = $value['H'];
	                					$datoCsv[$noexiste]['Observacion'] = $value['I'];
	                					$noexiste++;
	                				}
	                			}else{
	                				$datos['inc_cc_afiliado'] = NULL;
	                			}


	                			if($existe){

									$item1 = "inc_cc_afiliado";
									$valor1 = $value['B'];
									$item2 = 'inc_fecha_inicio';
									$valor2 = $value['D'];
									$existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2);
									if($existeInc){
										$id_incapacidad = ModeloIncapacidades::mdlVerIdIncapacidades($item1, $valor1, $item2, $valor2);

										if(!is_null($value['E']) && !empty($value['E']) ){
			                				$datos['inc_estado_tramite'] = $value['E'];
			                				$valido = 1;
			                			}else{
			                				$datos['inc_estado_tramite'] = NULL;
			                			}

			                			if(!is_null($value['F']) && !empty($value['F']) ){
	                						if($value['E'] == 'SOLICITUD DE PAGO'){
	                							$datos['inc_valor_solicitado'] = $value['F'];
	                						}else if($value['E'] == 'PAGADA'){
	                							$datos['inc_valor'] = $value['F'];
	                						}else{
	                							$datos['inc_valor'] = NULL;
	                							$datos['inc_valor_solicitado'] = NULL;
	                						}
			                				$valido = 1;
			                			}else{
			                				$datos['inc_valor'] = NULL;
                							$datos['inc_valor_solicitado'] = NULL;
			                			}

	                					if(!is_null($value['G']) && !empty($value['G']) ){
	                						if($value['E'] == 'RADICADA'){
	                							$datos['inc_fecha_radicacion'] = $value['G'];
	                							$datos['inc_fecha_solicitud'] = NULL;
	                							$datos['inc_fecha_pago'] = NULL;
	                							$datos['inc_fecha_negacion_'] = NULL;
	                						}else  if($value['E'] == 'SOLICITUD DE PAGO'){
	                							$datos['inc_fecha_solicitud'] = $value['G'];
	                							$datos['inc_fecha_radicacion'] = NULL;
	                							$datos['inc_fecha_pago'] = NULL;
	                							$datos['inc_fecha_negacion_'] = NULL;
	                						}else if($value['E'] == 'PAGADA'){
	                							$datos['inc_fecha_pago'] = $value['G'];
	                							$datos['inc_fecha_solicitud'] = NULL;
	                							$datos['inc_fecha_radicacion'] = NULL;
	                							$datos['inc_fecha_negacion_'] = NULL;
	                						}else if($value['E'] == 'SIN RECONOCIMIENTO'){
	                							$datos['inc_fecha_negacion_'] = $value['G'];
	                							$datos['inc_fecha_solicitud'] = NULL;
	                							$datos['inc_fecha_radicacion'] = NULL;
	                							$datos['inc_fecha_pago'] = NULL;
	                						}

			                				
			                				$valido = 1;
			                			}else{
			                				$datos['inc_fecha_pago'] = NULL;
			                				$datos['inc_fecha_solicitud'] = NULL;
                							$datos['inc_fecha_radicacion'] = NULL;
                							$datos['inc_fecha_negacion_'] = NULL;
			                			}

			                			if(!is_null($value['H']) && !empty($value['H']) ){
			                				if($value['E'] == 'SIN RECONOCIMIENTO'){
	                							$datos['inc_observacion_neg'] = $value['H'];
	                							$datos['inc_observacion'] = NULL;
	                						}else{
	                							$datos['inc_observacion'] = $value['H'];
	                							$datos['inc_observacion_neg'] = NULL;
	                						}
			                				
			                				$valido = 1;
			                			}else{
			                				$datos['inc_observacion'] = NULL;
                							$datos['inc_observacion_neg'] = NULL;
			                			}

			                			$tabla = "gi_incapacidad";
										$datos["inc_id"] = $id_incapacidad['inc_id']; 
										

										//var_dump($datos);

										//Echo "El otro \n";
										$respuesta = ModeloIncapacidades::mdlAgregarDatosEconomicos($tabla, $datos);
										if($respuesta == 'ok'){
											$aciertos++;
										}else{
											$fallos++;
										}
									}else{
										$incapacidadesExistentes++;
									}
								}
                			}	
            			}
            			
                	}
                }

                if($noexiste > 0){
                	header('Content-Type: application/octet-stream');
					header("Content-Transfer-Encoding: Binary"); 
					header("Content-disposition: attachment; filename=\"Fallaron.csv\"");
                	$f = fopen("tmp.csv", "w");
					foreach ($array as $line) {
					    fputcsv($f, $line);
					}
					fclose($f);
                }

                if(isset($_POST['chekUpdate']) && $_POST['chekUpdate'] != 0){
                	echo "<script>
						swal({
							title  : 'Proceso terminado, total registros ".$total.", se cargaron con exito ".$aciertos.", fallaron ".$fallos.", incapacidades no existentes ".$incapacidadesExistentes." ',
							type   : 'success',
							configButtonText : \"Cerrar\",
							closeOnConfirm: false
						},function(){
							window.location = \"incapacidades\";
						});
				</script>";
                }else{
                	echo "<script>
						swal({
							title  : 'Proceso terminado, total registros ".$total.", se cargaron con exito ".$aciertos.", fallaron ".$fallos.", incapacidades existentes ".$incapacidadesExistentes." ',
							type   : 'success',
							configButtonText : \"Cerrar\",
							closeOnConfirm: false
						},function(){
							window.location = \"incapacidades\";
						});
					</script>";
                }
				
					
			}
		}

		static public function ctrValidarIncapacidades($item1, $valor1, $item2, $valor2){
			$existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2, $_SESSION['cliente_id']);
			return $existeInc;
		}


		static public function ctrGetTopTenIncapacidades($item1, $valor1, $fecha1, $fecha2){
			$respuesta = ModeloIncapacidades::mdlGetTopTenIncapacitados($item1, $valor1, $fecha1, $fecha2);
			return $respuesta;
		}

		static public function ctrGetTopTenIncapacidadesByMaternidad($item1, $valor1, $fecha1, $fecha2){
			$respuesta = ModeloIncapacidades::mdlGetTopTenIncapacitadosByMaternidad($item1, $valor1, $fecha1, $fecha2);
			return $respuesta;
		}

		

		static public function ctrGetTopTenIncapacidadesByday($item1, $valor1, $fecha1, $fecha2){
			$respuesta = ModeloIncapacidades::mdlGetTopTenIncapacitadosByday($item1, $valor1, $fecha1, $fecha2);
			return $respuesta;
		}

		static public function ctrGetConsolidadoDeEstados($item1, $valor1, $fecha1, $fecha2){
			$respuesta = ModeloIncapacidades::mdlconsolidarEstadosIncapacidades($item1, $valor1, $fecha1, $fecha2);
			return $respuesta;
		}
		

		static public function ctrGetTopTenDiagnosticos($item1, $valor1, $fecha1, $fecha2){
			$respuesta = ModeloIncapacidades::mdlGetTopTenDiagnosticos($item1, $valor1, $fecha1, $fecha2);
			return $respuesta;
		}

		static public function ctrGetTopTenFechasSinLimite($item1, $valor1, $fecha1, $fecha2){
			$respuesta = ModeloIncapacidades::mdlGetTopTenIncapacitadosSinLimites($item1, $valor1, $fecha1, $fecha2);
			return $respuesta;
			
		}

		static public function ctrLiquidacionAutomatica($idIncapacidad){

			//$empresa, $fechaInicio, $fechaFinal, $estadoInicial, , $ccEmpleado
			$respuestaIncapacidad = Self::ctrMostrarIncapacidades('inc_id', $idIncapacidad);
			if($respuestaIncapacidad != null && $respuestaIncapacidad != '' && $respuestaIncapacidad != false){
				$respuestaEmpresa = ControladorClientes::ctrMostrarClientes('emp_id', $respuestaIncapacidad['inc_empresa']);
			
				if($respuestaEmpresa != null){
					if($respuestaEmpresa['emp_liquidacion_automatica_i'] == '1'){
						
					}	
				}				
			}

		}

		static public function dias_transcurridos($fecha_i, $fecha_f)
	    {
	        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
	        $dias   = abs($dias); $dias = floor($dias);     
	        return $dias + 1;
	    }
			
		/*Esto es para el reporte de cartera antigua*/
		static public function getSumIncapacidades($anho = null, $tipo = null){
			return ModeloIncapacidades::getSumIncapacidades($anho, $tipo, $_SESSION['cliente_id'])['Total'];
		}

		/*Esto es para el reporte de cartera antigua*/
		static public function getCountIncapacidades($anho = null, $tipo = null){
			return ModeloIncapacidades::getCountIncapacidades($anho, $tipo, $_SESSION['cliente_id'])['Total'];
		}

		static public function getTotalEps($tipo = null){
			return ModeloIncapacidades::getTotalEps($tipo, $_SESSION['cliente_id']);
		}

		static public function getTotalEpsIndividual($tipo = null, $eps){
			return ModeloIncapacidades::getTotalEpsIndividual($tipo, $eps, $_SESSION['cliente_id'])['total'];
		}

		static public function getTotalValorEntranteAnual($anho){
			return ModeloIncapacidades::getSumValorEntranteAnual($anho, $_SESSION['cliente_id'])['total'];
		}

		/*Incompletas*/
		

		
		static public function getSumIncapacidadesIncompletas(){
			return ModeloIncapacidades::getSumIncapacidadesIncompletas($_SESSION['cliente_id'])['Total'];
		}

		static public function getCountIncapacidadesIncompletas(){
			return ModeloIncapacidades::getCountIncapacidadesIncompletas($_SESSION['cliente_id'])['Total'];
		}
	}