<?php
	class ControladorNomina extends ControladorPlantilla
	{
		
		/**
		*Desc.  => Cambia los estados de las incapacidades, en recepcion
		*params => EditarFechaInicio, EditarFechaFinal, EditarMotivo, idIncapacidadEdicion, EditarEstado, Editarcedula,
		* EditarNomina, txtObservaciones
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrEditarNomina(){

			if(isset($_POST['idIncapacidadEdicion'])){

				$fechaI = explode('/', $_POST['EditarFechaInicio']);
				$fechaF = explode('/', $_POST['EditarFechaFinal']);

				if(count($fechaI) > 1){
					$fechaI = $fechaI[2].'-'.$fechaI[1].'-'.$fechaI[0];
				}else{
					$fechaI = $fechaI[0];
				}

				if(count($fechaF) > 1){
					$fechaF = $fechaF[2].'-'.$fechaF[1].'-'.$fechaF[0];
				}else{
					$fechaF = $fechaF[0];
				}

				$motivo = 0;
				if(isset($_POST['EditarMotivo'])){
					$motivo = $_POST['EditarMotivo'];
				}

				/*Primero necesito sabewr si esta incapacidad estaba rechazada*/
				$estado = ModeloTesoreria::mdlMostrarUnitario('incn_estado_v', 'gi_incapacidades_nomina', " incn_id_i = ".$_POST['idIncapacidadEdicion']);

				$campos = "incn_cc_v = '".$_POST['Editarcedula']."', incn_fecha_inicio_d = '".$fechaI."', incn_fecha_fin_d = '".$fechaF."', incn_estado_v= '".$_POST['EditarEstado']."', incn_nomina_i= '".$_POST['EditarNomina']."', incn_anho_i= '".$_POST['EditarAnhoNomina']."', incn_razon_v= '".$motivo."', inc_observacion_v = '".$_POST['txtObservaciones']."', inc_edi_user_id_i =  ".$_SESSION['id_Usuario'];
				$condic = " incn_id_i = ".$_POST['idIncapacidadEdicion'];

				$respuesta = ModeloTesoreria::mdlEditar('gi_incapacidades_nomina', $campos, $condic);

				if($respuesta == "ok"){

					if( $_POST['EditarEstado'] == 'ACEPTADA' && $estado['incn_estado_v'] == 'RECHAZADA' ){
						/*Cambio a aceptada*/
						$item1 = "inc_cc_afiliado";
                        $valor1 = $_POST['Editarcedula'];
                        $item2 = 'inc_fecha_inicio';
                        $valor2 = $fechaI;
                        $existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2, $_SESSION['cliente_id']);
                        $incapacidaDato = null;
                        if(!$existeInc){
                        	//No existe toca insertarla vamos a crearla (y)
	                		$item = 'emd_cedula';
	        				$valuess = $_POST['Editarcedula'];
	        				$tabla = 'gi_empleados';
	        				$res = ModeloEmpleados::mdlMostrarEmpleados($tabla, $item, $valuess);
	        				if($res != null && $res != false){
	        					$campos = "inc_emd_id, inc_empresa, inc_ips_afiliado, inc_cc_afiliado, inc_fecha_inicio, inc_fecha_final, inc_estado, inc_estado_tramite, inc_user_id_i";
	        					$valore = "'".$res['emd_id']."', '".$res['emd_emp_id']."', '".$res['emd_eps_id']."', '".$_POST['Editarcedula_'.$i]."', '".$_POST['EditarFechaInicio_'.$i]."', '".$_POST['EditarFechaFinal_'.$i]."', 1, 'POR RADICAR',  ".$_SESSION['id_Usuario']." ";
	        					$resultadoInca = ModeloTesoreria::mdlCrear('gi_incapacidad', $campos, $valore);
	        					if($resultadoInca != 'Error'){
	        						echo json_encode( array('code' => 1, 'desc' => "Incapacidad editada correctamente"));
								}else{
									echo json_encode( array('code' => 0, 'desc' => "Incapacidad no editada"));
								}
	        				}

    					} else {
    						/*La incapacidad ya existia*/
    						echo json_encode( array('code' => 1, 'desc' => "Incapacidad aceptada correctamente, aunque ya estaba creada en el sistema"));
    					}  

					}else{
						echo json_encode( array('code' => 1, 'desc' => "Incapacidad Editada Correctamente"));
					}
					
				}else{
					echo json_encode( array('code' => 0, 'desc' => "Incapacidad no editada"));
				}
			}
		}

		/**
		*Desc.  => Elimina las incapacidades de recepcion
		*params => idIncapacidad
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrEliminarNomina(){
			if(isset($_POST["idIncapacidad"])){
				$tabla ="gi_incapacidades_nomina";
				$datos = $_POST["idIncapacidad"];
				$respuesta = ModeloTesoreria::mdlBorrar($tabla, "incn_id_i = ".$datos);
				if($respuesta == "ok"){
					echo json_encode( array('code' => 1, 'desc' => "Incapacidad eliminada correctamentee"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "Incapacidad no eliminada"));
				}	
			}
		}

	}