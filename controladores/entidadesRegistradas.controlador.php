<?php

/**
 * Entidadesatiende
 */
class ControladorEntidadAtiende extends ControladorPlantilla
{

	/**
	 *Desc.  => Crear nuevos Entidadesatiende enviados a EPS
	 *params => IngresarNombre,IngresarNit,IngresarFecha,IngresarEstado
	 *Method => POST
	 *Return => array {
	 *	code => [exito : 1, Falla : 0, Otro : -1],
	 *   desc => 'Mensaje de resultado'	
	 *}
	 **/
	static public function ctrCrearEntidadesAtiende()
	{
		if ($_POST['IngresarNombre'] != '' && !empty($_POST['IngresarNombre'])) {
			$tabla = "gi_entidad_atiende";
			$datos = array(
				'enat_nombre_v' 			=> $_POST['IngresarNombre'],
				'enat_nit_v' 				=> $_POST['IngresarNit'],
				// 'enat_fecha_creacion_d'  	=> $_POST['IngresarFecha'],
				//  'enat_estado'				=> $_POST['IngresarEstado']
			);
			$respuesta = ModeloEntidadAtiende::mdlIngresarEntidadesNuevas($tabla, $datos);
			self::setAuditoria('ControladorEntidadAtiende', 'ctrCrearEntidad', json_encode($datos), $respuesta);
			if ($respuesta != 'Error') {
				if (isset($_POST["RedToPertenece"]) && $_POST["RedToPertenece"] != '') {
					foreach ($_POST["RedToPertenece"] as $key) {
						ModeloEntidadAtiende::mdlCrear("gi_entidad_atiende_ips","ei_ent_id, ei_ips_id", $respuesta.",".$key);
					}
				}
				echo json_encode(array('code' => 1, 'desc' => "entidad creada correctamente"));
			} else {
				echo json_encode(array('code' => 0, 'desc' => "entidad no creada"));
			}
		} else {
			echo json_encode(array('code' => -1, 'desc' => "Nombre de la entidad es necesario"));
		}
	}

	static public function ctrEditarEntidadesAtiende()
	{
		if ($_POST['EditarNombre'] != '' && !empty($_POST['EditarNombre'])){
			$tabla = "gi_entidad_atiende";
			$datoId = $_POST['EditarEntidadID'];
			$datos = array(
				'enat_nombre_v' 			=> $_POST['EditarNombre'],
				'enat_nit_v' 				=> $_POST['EditarNit'],
				'enat_estado'				=> $_POST['EditarEstado'],
				'enat_id_i'				    => $datoId
			);

			$respuesta = ModeloEntidadAtiende::mdlEditarEntidadesRegistradas($tabla, $datos);
			self::setAuditoria('ControladorEntidadAtiende', 'ctrEditarEntidadesAtiende', json_encode($datos), $respuesta);
			
			if ($respuesta == 'ok') {
				if (isset($_POST["EditarRedToPertenece"]) && $_POST["EditarRedToPertenece"] != '') {
					ModeloDAO::mdlBorrar("gi_entidad_atiende_ips", "ei_ent_id = ${datoId}");
					foreach ($_POST["EditarRedToPertenece"] as $key) {
						ModeloEntidadAtiende::mdlCrear("gi_entidad_atiende_ips","ei_ent_id, ei_ips_id", $datoId.",".$key);
					}	
				}
				echo json_encode(array('code' => 1, 'desc' => "entidad editada correctamente"));
			} else {
				echo json_encode(array('code' => 0, 'desc' => "entidad no editada"));
			}
			
		} else {
			echo json_encode(array('code' => -1, 'desc' => "Nombre de la entidad es necesario"));
		}
	}

	static public function ctrBorrarEntidadAtiende()
	{

		if (isset($_POST["id_Entidad"])) {
			$tabla = "gi_entidad_atiende";
			$datos = $_POST["id_Entidad"];

			$respuestaDao = ModeloDAO::mdlBorrar("gi_entidad_atiende_ips", "ei_ent_id = ${datos}");
			if ($respuestaDao == "ok") {
				$respuesta = ModeloEntidadAtiende::mdlBorrarEntidadesRegistradas($tabla, $datos);
				self::setAuditoria('ControladorEntidadAtiende', 'ctrEditarEntidadesAtiende', $datos, $respuesta);
				if ($respuesta == 'ok') {

					echo json_encode(array('code' => 1, 'desc' => "entidad eliminada correctamente"));
				} else {
					echo json_encode(array('code' => 0, 'desc' => "entidad no eliminada"));
				}
			}
		}
	}

	static public function ctrMostrarEntidades($item, $valor)
	{
		$tabla = "gi_entidad_atiende";
		$respuesta = ModeloEntidadAtiende::getDatos($tabla, $item, $valor);
		return $respuesta;
	}

	static public function ctrValidarEntidad($item, $valor)
	{
		$tabla = "gi_entidad_atiende";
		$respuesta = ModeloEntidadAtiende::getDatos($tabla, $item, $valor);
		//$respuesta = ModeloEntidadAtiende::mdlMostrarUnitario('*', $tabla, $item."='".$valor."'" );
		return $respuesta;
	}

	static public function ctrBuscarEntidadesAtiende($valor){
        $tabla = 'gi_entidad_atiende';
        $respuesta = ModeloEntidadAtiende::mdlBuscarEntidadesIpsByNombre($tabla,$valor);
        return $respuesta;
    }

	static public function ctrMostrarRedEntidad($item, $valor){
		$campo = 'ei_ips_id';
		$tabla = 'gi_entidad_atiende_ips';
		$condicion = "${item} = ${valor}";
		$respuesta = ModeloDAO::mdlMostrarGroupAndOrder($campo,$tabla,$condicion);
		return $respuesta;
	}
}
