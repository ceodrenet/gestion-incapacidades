<?php
	/**
	* 
	*/
	class ControladorUsuarios extends ControladorPlantilla
	{

		/*=============================================
					Mostrar Usuarios
		=============================================*/
		static public function ctrMostrarUsuarios($item, $valor){
			$tabla = "gi_usuario";
			$respuesta = ModeloUsuarios::mdlMostrarUsuariosIndex($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Crear usuarios en el sistema desde dentro del sistema
		*params => NuevoUsuario, NuevoNombre, NuevoPerfil, NuevoPassword, Imagen
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrCrearUsuario(){
			if(isset($_POST['NuevoUsuario'])){
				if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["NuevoNombre"]) &&
				   filter_var($_POST['NuevoUsuario'], FILTER_VALIDATE_EMAIL) &&
				   preg_match('/^[a-zA-Z0-9.]+$/', $_POST["NuevoPassword"]))
				{

					$rutaReal = 'vistas/img/usuarios/'.$_POST['NuevoUsuario'];
					$ruta = __DIR__."/../vistas/img/usuarios/".$_POST['NuevoUsuario'];

					if(isset($_FILES['NuevaFoto']['tmp_name']) && !empty($_FILES['NuevaFoto']['tmp_name']) ){
		                $ruta = selft::putImage($_FILES['NuevaFoto']['tmp_name'], $_FILES["NuevaFoto"]["type"] , $ruta, $rutaReal);           
		            }

					$tabla = "gi_usuario";
					$contrasenha = md5($_POST['NuevoPassword']);
					$datos = array(
								'user_nombre' 		=> $_POST['NuevoNombre'], 
								'usu_correo_v' 		=> $_POST['NuevoUsuario'],
								'user_password'  	=> $contrasenha,
								'user_perfil_id'	=> $_POST['NuevoPerfil'],
								'user_ruta_imagen_v'=> $ruta
							);

					$datosAUditoria = array(
								'user_nombre' 		=> $_POST['NuevoNombre'], 
								'usu_correo_v' 		=> $_POST['NuevoUsuario'],
								'user_perfil_id'	=> $_POST['NuevoPerfil'],
							);

					$respuesta = ModeloUsuarios::mdlIngresarUsuario($tabla, $datos);
					self::setAuditoria('ControladorUsuarios', 'ctrCrearUsuario',json_encode($datosAUditoria),$respuesta);
					if($respuesta != 'Error'){
						/*Si es el administrador ese usuario quedara en la empresa que este seleccionada en ese momento*/
						if($_SESSION['cliente_id'] != 0){
							
							$item1 = "user_cliente_id";
							$valor1 = $_POST['NuevoclienteHiden'];
							$item2 = "user_id";
							$valor2 = $respuesta;
							$ultimoLogin = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
						
						}else{
							/*no es el administrador, puede que se le quiera asignar a varias empresas*/
							if(isset($_POST['Nuevocliente']) && $_POST['Nuevocliente'] != ''){
								$i = 0;	
				                foreach ($_POST['Nuevocliente'] as $key) {
				                	if($i == 0){
										$item1 = "user_cliente_id";
										$valor1 = $key;
										$item2 = "user_id";
										$valor2 = $respuesta;
										$ultimoLogin = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
				                	}
				                    ModeloUsuarios::mdlCrear("gi_empresa_usuarios", "emu_id_usuario, emu_id_empresa", $respuesta.",".$key);
				                    $i++;
				      			}
				            }
						}
						
						echo json_encode( array('code' => 1, 'desc' => "usuario creado correctamente"));
					}else{
						echo json_encode( array('code' => 0, 'desc' => "usuario no creado"));
					}
				}else{
					echo json_encode( array('code' => -1, 'desc' => "Nombre de usuario es necesario"));
				}
			}
		}

		/**
		*Desc.  => Editar usuarios en el sistema desde dentro del sistema
		*params => EditarUsuario, EditarNombre, EditarPerfil, EditarPassword, Imagen, EditarUserID
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrEditarUsuario(){
			if(isset($_POST['EditarUsuario'])){
				if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["EditarNombre"]) &&
				   filter_var($_POST['EditarUsuario'], FILTER_VALIDATE_EMAIL) )
				{
					$ruta = $_POST['FotoActual'];
					if(isset($_FILES['EditarFoto']['tmp_name']) && !empty($_FILES['EditarFoto']['tmp_name']) ){
		                $rutaReal = 'vistas/img/usuarios/'.$_POST['EditarUsuario'];
						$ruta = __DIR__."/../vistas/img/usuarios/".$_POST['EditarUsuario']; 
						$ruta = self::putImage($_FILES['EditarFoto']['tmp_name'], $_FILES["EditarFoto"]["type"] , $ruta, $rutaReal);              
		            }

					$tabla = "gi_usuario";
					if($_POST['EditarPassword'] != ''){
						if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["EditarPassword"])){
							$contrasenha = md5($_POST['EditarPassword']);
							$EditarPassword = $_POST['EditarPassword'];
						}else{
							echo json_encode( array('code' => -1, 'desc' => "El password no puede ir vacio, ni llevar caracteres Especiales"));
						}
					}else{
						$contrasenha = $_POST['passwordActual'];
						$EditarPassword = $_POST['RecordatorioActual'];
					}

					$editarclienteHiden = 0;
					if (isset($_POST['EditarclienteHiden']) && !empty($_POST['EditarclienteHiden'])) {
						$editarclienteHiden = $_POST['EditarclienteHiden'];
					}

					$datos = array(
								'user_nombre' 		=> $_POST['EditarNombre'], 
								'usu_correo_v' 		=> $_POST['EditarUsuario'],
								'user_password'  	=> $contrasenha,
								'user_perfil_id'	=> $_POST['EditarPerfil'],
								'user_cliente_id'	=> $editarclienteHiden,
								'user_ruta_imagen_v'=> $ruta,
								'user_id'			=> $_POST['EditarUserID']
							);
					
					$datosAuditoria = array(
								'user_nombre' 		=> $_POST['EditarNombre'], 
								'usu_correo_v' 		=> $_POST['EditarUsuario'],
								'user_perfil_id'	=> $_POST['EditarPerfil'],
								'user_cliente_id'	=> $editarclienteHiden,
								'user_id'			=> $_POST['EditarUserID']
							);
					
					$respuesta = ModeloUsuarios::mdlEditaruser_usuario($tabla, $datos);
					self::setAuditoria('ControladorUsuarios', 'ctrEditarUsuario',json_encode($datosAuditoria),$respuesta);
					if($respuesta == 'ok'){
						/*Si es el administrador ese usuario quedara en la empresa que este seleccionada en ese momento*/
						if($_SESSION['cliente_id'] != 0){
							$item1 = "user_cliente_id";
							$valor1 = $_POST['EditarclienteHiden'];
							$item2 = "user_id";
							$valor2 = $_POST['EditarUserID'];
							$ultimoLogin = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
						}else{
							/*no es el administrador, puede que se le quiera asignar a varias empresas*/
							if(isset($_POST['Editarcliente']) && $_POST['Editarcliente'] != ''){
								$i = 0;
								ModeloTesoreria::mdlBorrar("gi_empresa_usuarios", "emu_id_usuario=".$_POST['EditarUserID']);
				                foreach ($_POST['Editarcliente'] as $key) {
				                	if($i == 0){
										$item1 = "user_cliente_id";
										$valor1 = $key;
										$item2 = "user_id";
										$valor2 = $_POST['EditarUserID'];
										$ultimoLogin = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
				                	}
				                    ModeloDAO::mdlCrear("gi_empresa_usuarios", "emu_id_usuario, emu_id_empresa", $_POST['EditarUserID'].",".$key);
				                    $i++;
				      			}
				                
				            }
						}
						echo json_encode( array('code' => 1, 'desc' => "usuario editado correctamente"));
					}else{
						echo json_encode( array('code' => 0, 'desc' => "usuario no editado"));
					}
				}else{
					echo json_encode( array('code' => -1, 'desc' => "Nombre de usuario es necesario"));
				}
			}
		}

		/**
		*Desc.  => Eliminar usuarios en el sistema desde dentro del sistema
		*params => id_Usuario
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrBorrarUsuario(){

			if(isset($_POST["id_Usuario"])){
				$tabla ="gi_usuario";
				$dato = $_POST["id_Usuario"];

				$respuesta = ModeloUsuarios::mdlBorrarUsuario($tabla, $dato);
				self::setAuditoria('ControladorUsuarios', 'ctrBorrarUsuario',$dato,$respuesta);
				if($respuesta == "ok"){
					echo json_encode( array('code' => 1, 'desc' => "usuario eliminado correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "usuario no eliminado"));
				}

				if ($_SESSION['cliente_id'] == 0) {
					$tabla ="gi_empresa_usuarios";
					$dato = $_POST["id_Usuario"];
					$cond = "emu_id_usuario=${dato}";
					ModeloDAO::mdlBorrar($tabla,$cond);
				}
			}
		}


		/*Estas funciones se quedan en StanDBy por el momento, falta detallarlas mas*/
		/*static public function ctrMostrarUsuarios_Clientes_internos($item, $valor){
			$tabla = "gi_usuario";
			$respuesta = ModeloUsuarios::mdlMostrarUsuarios_Clientes_Internos($tabla, $item, $valor);
			return $respuesta;
		}

		static public function ctrMostrarUsuarios_Clientes_externos($item, $valor){
			$tabla = "gi_usuario";
			$respuesta = ModeloUsuarios::mdlMostrarUsuarios_Clientes_Externos($tabla, $item, $valor);
			return $respuesta;
		}*/
		
	public static function ctrRegistrarUsuarios(){
		if(isset($_POST['registroCorreo'])){ 
			$perfil = 8;
			if(isset($_POST['tipoCuenta']) && $_POST['tipoCuenta'] == '2'){
				$perfil = 14;
			}
			$codigo = password_hash($_POST['registroCorreo'], PASSWORD_DEFAULT);
			$datos = array(
				'usu_correo_v' 			=> $_POST['registroCorreo'], 
				'user_nombre' 			=> $_POST['registroNombre'],
				'user_password' 		=> crypt($_POST['registroPassword'], '$2a$07$usesomesillystringforsalt$'),
				'user_perfil_id'		=> $perfil,
				'user_cod_validacion_v' => $codigo,
				'user_estado_i'			=> 3
			);
			$respuesta = ModeloUsuarios::mdlRegistrarIngreso($datos);
			if($respuesta != "error"){
				/*Aqui lo regresamos a la pagina de inicio*/
				self::enviarCorreoBienvenida($_POST['registroCorreo'], $codigo, $_POST['registroNombre'] );
				//header('Location: index.php?ruta=mensaje');
				echo "<script>window.location.href='index.php?ruta=mensaje';</script>";
				die();
			}else{	
				return json_encode(array('code' => 0, 'message' => 'No se registro este usuario intentelo nuevamente'));
			}
		}
	}

	public static function crtValidarCodigo($codigo){
		if($codigo != null){
			$resultado  = ModeloUsuarios::mdlValidarCodigo($codigo);
			if($resultado != false){
				if($resultado['user_estado_i'] == '3'){
					
					/*Lo creamos como una empresa propia*/
					$registroEmpresa = ModeloUsuarios::mdlCrear('gi_empresa', 'emp_nombre,	emp_email, emp_estado', "'".$resultado["user_nombre"]."', '".$resultado["usu_correo_v"]."', 1");
					if($registroEmpresa != 'error'){
						$registroCambio = ModeloUsuarios::mdlEditar("gi_usuario", "user_estado_i = '1', user_cliente_id = '".$registroEmpresa."'", "user_id = '".$resultado["user_id"]."'");
						$mensaje = "<h5 class=\"mb-0\">Mensaje de Registro</h5>";
						$mensaje .= "<p class=\"text-muted mt-2\" style=\"text-align: justify;\">Lo lograste, puedes iniciar session en la aplicación!</p>";
						return $mensaje;
					}
					
				}else{
					$mensaje = "<h5 class=\"mb-0\">Mensaje de Registro</h5>";
					$mensaje .= "<p class=\"text-muted mt-2\" style=\"text-align: justify;\">EL USUARIO QUE INTENTAS VALIDAR YA ESTA VALIDADO!!</p>";
					return $mensaje;
				}
			}else{
				$mensaje = "<h5 class=\"mb-0\">Mensaje de Registro</h5>";
				$mensaje .= "<p class=\"text-muted mt-2\" style=\"text-align: justify;\">EL CODIGO ENVIADO NO EXISTE!!!</p>";
				return $mensaje;
			}
		}else{

			$mensaje = "<h5 class=\"mb-0\">Mensaje de Registro</h5>";
			$mensaje .= "<p class=\"text-muted mt-2\" style=\"text-align: justify;\">Ehhh donde esta el codigo?!!!</p>";
			return $mensaje;
		}
	}

	public static function ctrValidarEmail($correo){
		$login =  ModeloUsuarios::mdlValidarEmail($correo);
		if(!$login){
			return json_encode(array('code' => 0, 'message' => 'Correo valido'));
		}else{	
			return json_encode(array('code' => 1, 'message' => 'Este correo ya exite en el sistema'));
		}
	}

	public static function enviarCorreoBienvenida($correo, $claveValidacion, $nombre){
		$titulo = 'ACTIVACIÓN DE CUENTA I-REPORTS';
        $mensaje = '
<html>
  	<head>
    	<title>ACTIVACIÓN DE CUENTA I-REPORTS</title>
  	</head>
  	<body style="text-align:justify;">
  		<p style="text-align:justify;"><img src="https://incapacidades.app/vistas/img/plantilla/LOGO_1.png"/></p>
      	<p>Hola, '.strtoupper($nombre).'</p>
      	<p>Nuestro mayor anhelo es hacer parte de tu día a día 💚.</p>
      	<p>Para lograrlo te pedimos sigas el siguiente link, para activar tu cuenta, si no puedes hacerlo solo copia y pega el vinculo en el navegador</p>
      	<p><a href = "https://incapacidades.app/validacion.php?validate='.$claveValidacion.'" target = "blank">https://incapacidades.app/validacion.php?validate='.$claveValidacion.'</a></p>      

		<p>Esperamos apoyarte y convertirnos en tu aliado mas importante,</p>
		<br/>
		<br/>
		<p><b>EQUIPO INCAPACIDADES LABORALES</b></p>
		<br/>
		<br/>
		<p>Por favor no responda este mensaje ha sido generado automaticamente y es solo para valiaci&oacute;n de su cuenta en I-Reports.</p>
	</body>
</html>';
		
		/*// Para enviar un correo HTML, debe establecerse la cabecera Content-type
		$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
		$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Cabeceras adicionales
		$cabeceras .= 'To: '.$nombre.' <'.$correo.'>' . "\r\n";
		$cabeceras .= 'From: Notificaciones <notificaciones@incapacidades.app>' . "\r\n";
		//$cabeceras .= 'Cc: birthdayarchive@example.com' . "\r\n";
		//$cabeceras .= 'Bcc: birthdaycheck@example.com' . "\r\n";

		// Enviarlo
		mail($correo, $titulo, $mensaje, $cabeceras);*/
		self::EnviarMail($titulo, $mensaje, $correo, null);
	}

}
