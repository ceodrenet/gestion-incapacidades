<?php
	session_start();
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once 'mail.controlador.php';
    require_once 'plantilla.controlador.php';
	class ControladorAuth extends ControladorPlantilla
	{
		/**
		*Desc.  => Controlar el Acceso de los usuarios al sistema
		*params => ingUsuario, ingPassword, g-recaptcha-response
		*Method => POST
		*Return => redirecciona a la pagina principal o al login con un mensaje en caso de no ser exitoso.
		**/
		static public function ctrIngresoUsuario(){
			if(isset($_POST['ingUsuario']) && isset($_POST['ingPassword'])){
				require_once __DIR__.'/../modelos/dao.modelo.php';
    			require_once __DIR__.'/../modelos/usuarios.modelo.php';
				if(filter_var($_POST['ingUsuario'], FILTER_VALIDATE_EMAIL) && preg_match('/^[a-zA-Z0-9.]+$/', $_POST['ingPassword'])){
					$tabla 	= "gi_usuario";
					$item 	= "usu_correo_v";
					$valor	= $_POST['ingUsuario'];
					$contrasenha = md5($_POST['ingPassword']);
					//echo $contrasenha;
					$respuesta = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);
					if($respuesta['usu_correo_v'] == $_POST['ingUsuario'] && $respuesta['user_password'] == $contrasenha){
						if($respuesta['user_estado_i'] == 1){
							$timeRand = Time().rand();
							$apiKey = base64_encode($respuesta["user_id"].":".$timeRand);
							$paginaInicio = $respuesta['perfiles_inicio'];
							$_SESSION['iniciarSession'] = 'ok';
							$_SESSION['nombres'] = $respuesta['user_nombre'].' '.$respuesta['user_apellidos'];
							$_SESSION['perfil'] 	= $respuesta['user_perfil_id'];
							$_SESSION['perfilN'] 	= $respuesta['perfiles_descripcion_v'];
							$_SESSION['adiciona'] 	= $respuesta['perfiles_adiciona_i'];
							$_SESSION['edita'] 		= $respuesta['perfiles_edita_i'];
							$_SESSION['elimina'] 	= $respuesta['perfiles_elimina_i'];
							$_SESSION['cliente_id'] = $respuesta['user_cliente_id'];
							$_SESSION['anho'] = date('Y');
							$_SESSION['imagen'] 	= $respuesta['user_ruta_imagen_v'];
							$_SESSION['id_Usuario'] = $respuesta["user_id"];
							$_SESSION['idSession']  = $timeRand;
							$_SESSION['apiKey']  = $apiKey;
							$_SESSION['start'] = time();
							$navegador = self::getBrowser($_SERVER['HTTP_USER_AGENT']);
							$_SESSION['navegador'] = $navegador;
							
							$ip = self::getRealIP();
							$_SESSION['ipactual'] = $ip;

							$_SESSION['ciudad']='Barranquilla';
							$_SESSION['pais']='Colombia';
							
							/*if ($ip == '127.0.0.1' ||  $ip == 'localhost'  ||  $ip == '::1'  ){ //Esto tambien es valido te faltaba 1 y que qutaras lo qu etenbias arriba de la ciudad
								
								$_SESSION['ciudad']='Barranquilla';
								$_SESSION['pais']='Colombia';
								
							}else{
								$datos = self::detect_city($ip);
								$_SESSION['ciudad']=$datos['city'];
								$_SESSION['pais'] =$datos['country'];
							}*/
							/*=============================================
								REGISTRAR FECHA PARA SABER EL ÚLTIMO LOGIN
							=============================================*/
							date_default_timezone_set('America/Bogota');
							$fecha = date('Y-m-d');
							$hora = date('H:i:s');
							$fechaActual = $fecha.' '.$hora;
							$ultimoLogin = ModeloUsuarios::mdlEditar("gi_usuario", "user_ultimo_login = '".$fechaActual."', user_online_v = 1", "user_id = '".$respuesta["user_id"]."'");

							$datosArray = array('email' => $_POST['ingUsuario']);
							self::setAuditoria('ControladorAuth', 'ctrIngresoUsuario', json_encode($datosArray ), json_encode(array('code' => 1)));

							if($ultimoLogin == "ok"){
								/*Guardamos la Session*/
								$campos = "session_user_id_i, session_fecha_inicio_d,  	session_idSession_v, session_api_key_v";
								$valore = $respuesta["user_id"].", '".$fecha.' '.$hora."', '".$timeRand."', '".$apiKey."'";
								$respuesta = ModeloUsuarios::mdlCrear("gi_sessiones", $campos, $valore);
								echo json_encode(array('code' => 1 , 'Message' => 'Ingreso Correcto, un momento por favor...', 'inicio' => $paginaInicio));
							}
						}else{
							echo json_encode(array('code' => 0 , 'Message' => 'Usuario Inactivo'));
						}
					}else{
						echo json_encode(array('code' => -1 , 'Message' => 'Usuario y/o password Incorrecto'));
					}
				}else{
					echo json_encode(array('code' => -2 , 'Message' => 'Usuario y/o password formato incorrecto'));
				}
				
			}
		}
		

		
		public static function detect_city($ip) {
			$ch = curl_init('http://ipwhois.app/json/' . $ip);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$json = curl_exec($ch);
			curl_close($ch);
			// Decode JSON response
			$ipWhoIsResponse = json_decode($json, true);
			// Country code output, field "country_code"
			return $ipWhoIsResponse ;
			
		  }

	}

	if(isset($_POST['ingUsuario']) && isset($_POST['ingPassword'])){
		ControladorAuth::ctrIngresoUsuario();
	}