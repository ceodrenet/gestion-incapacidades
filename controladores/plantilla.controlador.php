<?php
	/**
	* 
	*/
	class ControladorPlantilla extends ctrMail
	{
		
		static public function ctrPlantilla(){
            include "vistas/plantilla.php";
		}

        static public function getData($table, $item, $value){
            return ModeloDAO::getDatos($table, $item, $value);
        }

        static public function getDataFromLsql($campos, $tabla, $where = null, $group = null, $order = null, $limit = null){
            return ModeloDAO::mdlMostrarGroupAndOrder($campos, $tabla, $where, $group, $order, $limit);
        }

		static public function putImage($fila, $typo, $ruta, $rutaReal, $extension = null, $name = null){
			if($typo == "image/jpeg" || $typo == "image/png"){
                list($ancho, $alto) = getimagesize($fila);
                $nuevoAncho = 900;
                $nuevoAlto  = 500;
            }
            
            if (!file_exists($ruta)) {
                //echo $ruta;
                mkdir($ruta, 0777, true);
            }

            if($typo == "image/jpeg"){
                if($name != null){
                    $ruta =  $ruta.$name.".jpg";
                    $rutaReal =  $rutaReal.$name.".jpg";
                }else{
                    $aleatorio = mt_rand(1000, 9999); //esto puede ser el tema de que se ponga mal el tema
                    $ruta =  $ruta.$aleatorio.".jpg";
                    $rutaReal =  $rutaReal.$aleatorio.".jpg";
                }
                
                $origen  = imagecreatefromjpeg($fila);
                $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                imagejpeg($destino, $ruta);
            }elseif($typo == "image/png"){
                if($name != null){
                    $ruta =  $ruta.$name.".png";
                    $rutaReal =  $rutaReal.$name.".png";
                }else{
                    $aleatorio = mt_rand(1000, 9999);
                    $ruta =  $ruta.$aleatorio.".png";
                    $rutaReal =  $rutaReal.$aleatorio.".png";
                }
                $origen  = imagecreatefrompng($fila);
                $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                imagepng($destino, $ruta);
            }else{
                if($name != null){
                    $ruta =  $ruta.$name.".pdf";
                    $rutaReal =  $rutaReal.$name.".pdf";
                }else{
                    $aleatorio = mt_rand(100, 999);
                    $rutaReal =  $rutaReal.$aleatorio.".pdf";
                    $ruta =  $ruta.$aleatorio.".pdf";
                }
                copy($fila, $ruta);
            }
            return $rutaReal;
		}

        
        public static function getBrowser($user_agent){

            if(strpos($user_agent, 'MSIE') !== FALSE)
                   return 'Internet explorer';
                elseif(strpos($user_agent, 'Edge') !== FALSE) //Microsoft Edge
                   return 'Microsoft Edge';
                elseif(strpos($user_agent, 'Trident') !== FALSE) //IE 11
                    return 'Internet explorer';
                elseif(strpos($user_agent, 'Opera Mini') !== FALSE)
                   return "Opera Mini";
                elseif(strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== FALSE)
                   return "Opera";
                elseif(strpos($user_agent, 'Firefox') !== FALSE)
                   return 'Mozilla Firefox';
                elseif(strpos($user_agent, 'Chrome') !== FALSE)
                   return 'Google Chrome';
                elseif(strpos($user_agent, 'Safari') !== FALSE)
                   return "Safari";
                else
                   return 'No hemos podido detectar su navegador';
        }

        /**
         * Funcion para guardar en auditoria, ya tenemos aqui las sessiones y la fecha esperamos el 
         * controlador y la accion.
         * */
        public static function setAuditoria($audControladorV, $audMetodoV,$request, $response, $idIncapacidad='NA'){
            /* $jsonCurl = array(
                "aud_id_usuario_i" => $_SESSION['id_Usuario'],
                "aud_fecha_d" => date('Y-m-d'),
                "aud_hora_d" =>  date('H:i:s'),
                "aud_controlador_v" =>  $audControladorV,
                "aud_metodo_v" => $audMetodoV,
                "aud_ip_i" =>  $_SESSION['ipactual'],
                "aud_navegador_v" =>  $_SESSION['navegador'],
                "aud_data_t" =>  "{".$request."},{".$response."}",
                "aud_id_session" =>  $_SESSION['idSession']
            );
            /*$postdata = json_encode($jsonCurl);
            $ch = curl_init("http://localhost:8000/api/user"); 
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            $result = curl_exec($ch);
            curl_close($ch);*/
            $campos = "aud_id_usuario_i, aud_fecha_d, aud_hora_d, aud_controlador_v, aud_metodo_v, aud_ip_i,  aud_navegador_v, aud_data_t, aud_id_session,aud_empresa_v,aud_id_incapacidad_i,aud_ciudad_v,aud_pais_v";
            $valores = $_SESSION['id_Usuario'].", '".date('Y-m-d')."', '".date('H:i:s')."', '".$audControladorV."', '".$audMetodoV."', '".$_SESSION['ipactual']."', '".$_SESSION['navegador']."', 'request:{".$request."}, response:{".$response."}', '".$_SESSION['idSession']."', '".$_SESSION['cliente_id']."', '".$idIncapacidad."','".$_SESSION['ciudad']."', '".$_SESSION['pais']."'";
            ModeloDAO::mdlCrear('gi_auditoria', $campos, $valores);
        }

        /**
         * Funcion para obtener la ip
         * */

        public static function getRealIP() {

            if (!empty($_SERVER['HTTP_CLIENT_IP']))
                return $_SERVER['HTTP_CLIENT_IP'];
               
            if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
           
            return $_SERVER['REMOTE_ADDR'];
    }

}