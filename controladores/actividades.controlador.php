<?php
	/**
	* Actividades
	*/
	class ControladorActividades extends ControladorPlantilla
	{
		/**
		*Desc.  => Guardar Actividades de los usuarios del Sistema
		*params => txtActividadRealizada_, txtActividadRealizadaObservacion_, rango_, empresa_, NuevoOrigen, id_Usuario
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		function ctrGuardarActividades(){
			if(isset($_POST['rango_0'])){
				$contado = 0;
				ModeloTesoreria::mdlBorrar("gi_actividades", "act_fecha_d ='".date('Y-m-d')."'");
				for ($i=0; $i < 13; $i++) { 
					if($_POST['txtActividadRealizada_'.$i] != '' && $_POST['empresa_'.$i] != 0){
						$campos = "act_hora_rango_v, act_actividad_v, act_emp_id_i, act_observacion_v, act_user_id_i, act_fecha_d";
						$valore = "'".$_POST['rango_'.$i]."','".$_POST['txtActividadRealizada_'.$i]."','".$_POST['empresa_'.$i]."','".$_POST['txtActividadRealizadaObservacion_'.$i]."','".$_SESSION['id_Usuario']."', '".date('Y-m-d')."'";
						$respuesta = ModeloTesoreria::mdlInsertar("gi_actividades", $campos, $valore);
						//echo print_r($respuesta);
						if($respuesta != 'Error'){
							$contado++;
						}
					} 
				}
				self::setAuditoria('ControladorActividades', 'ctrGuardarActividades', $campos.$valore, $respuesta);
				if($contado > 0){
					echo json_encode( array('code' => 1, 'desc' => "actividades registradas correctamente"));
				}else{
					echo json_encode( array('code' => 0, 'desc' => "actividades no resgistradas"));
				}
			}
		}
	}