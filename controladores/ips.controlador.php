<?php

	class ControladorIps extends ControladorPlantilla
	{

		
		/**
		*Desc.  => Obtener el conjunto de todas las EPS, bien sea solo o filtrados
		*params => $item @nullable, $valor @nullable
		*Method => POST
		*Return => array []
		**/
		static public function ctrMostrarIps($item, $valor){
			$tabla = "gi_ips";
			$respuesta = ModeloIps::mdlMostrarIps($tabla, $item, $valor);
			return $respuesta;
		}

		/**
		*Desc.  => Obtener el detalle de una EPS
		*params => $item, $valor
		*Method => POST
		*Return => array []
		**/
		static public function ctrMostrarIpsById($item, $valor){
			$tabla = "gi_ips";
			$respuesta = ModeloIps::mdlMostrarIpsById($tabla, $item, $valor);
			return $respuesta;
		}


		/**
		*Desc.  => Crear una nueva EPS
		*params => NuevoNombreEps, NuevoCodigoEps, NuevoNitEps, NuevoTipoEps, NuevoCorreoCartera, NuevoCorreoRadicar,      * NuevoCorreoPQRS
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrCrearIps(){
			if(isset($_POST['NuevoNombreEps'])){
				if($_POST['NuevoNombreEps'] != '' && !empty($_POST['NuevoNombreEps'])){
					$tabla = 'gi_ips';
					$datos = array(	'ips_codigo' => $_POST['NuevoCodigoEps'] , 
									'ips_nombre' => $_POST['NuevoNombreEps'],
									'ips_nit'	 => $_POST['NuevoNitEps'],
									'ips_tipo' 	 =>	$_POST['NuevoTipoEps'],	
									'ips_correo_cartera' 	=>	$_POST['NuevoCorreoCartera'],
									'ips_correo_radicacion'	=> $_POST['NuevoCorreoRadicar'],
									'ips_correo_pqr_v'	 	=> $_POST['NuevoCorreoPQRS'],
									'ips_correo_not_v'	=> $_POST['NuevoCorreoNOtificacion']
								);
					$respuesta = ModeloIps::mdlIngresarIps($tabla, $datos);
					self::setAuditoria('ControladorIps', 'ctrCrearIps',json_encode($datos),$respuesta);
					if($respuesta == "ok"){
						return json_encode(array('code' => 1, 'message' => 'Entidad guardadada con exito'));
					}else{	
						return json_encode(array('code' => 0, 'message' => 'Entidad no guardadada'));
					}
				}
			}
		}

		/**
		*Desc.  => Editar las EPS
		*params => EditarNombreEps, EditarCodigoEps, EditarNitEps, EditarTipoEps, EditarCorreoCartera, 
		* EditarCorreoRadicar,  EditarCorreoPQRS, EditarId
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrEditarIps(){
			if(isset($_POST['EditarNombreEps'])){
				if($_POST['EditarNombreEps'] != '' && !empty($_POST['EditarNombreEps'])){
					$tabla = 'gi_ips';
					$datos = array(	'ips_codigo' => $_POST['EditarCodigoEps'] , 
									'ips_nombre' => $_POST['EditarNombreEps'],
									'ips_nit'	 => $_POST['EditarNitEps'],
									'ips_tipo' 	 =>	$_POST['EditarTipoEps'],	
									'ips_correo_cartera' 	=>	$_POST['EditarCorreoCartera'],
									'ips_correo_radicacion'	=> $_POST['EditarCorreoRadicar'],
									'ips_correo_pqr_v'	 	=> $_POST['EditarCorreoPQRS'],
									'ips_id'	 			=> $_POST['EditarId'],
									'ips_correo_not_v'	=> $_POST['EditarCorreoNOtificacion']
								);
					$respuesta = ModeloIps::mdlEditarIps($tabla, $datos);
					self::setAuditoria('ControladorIps', 'ctrEditarIps',json_encode($datos),$respuesta);
					if($respuesta == "ok"){
						return json_encode(array('code' => 1, 'message' => 'Entidad editada con exito'));
					}else{	
						return json_encode(array('code' => 0, 'message' => 'Entidad no editada'));
					}
				}
			}
		}

		/**
		*Desc.  => ELiminar las EPS
		*params => ips_id
		*Method => POST
		*Return => array {
		*	code => [exito : 1, Falla : 0, Otro : -1],
		*   desc => 'Mensaje de resultado'	
		*}
		**/
		static public function ctrBorrarIps(){
			if(isset($_POST["ips_id"])){
				$tabla ="gi_ips";
				$datos = $_POST["ips_id"];
				$respuesta = ModeloIps::mdlBorrarIps($tabla, $datos);
				self::setAuditoria('ControladorIps', 'ctrEditarIps',$datos,$respuesta);
				if($respuesta == "ok"){
					return json_encode(array('code' => 1, 'message' => 'Entidad borrada con exito'));
				}else{	
					return json_encode(array('code' => 0, 'message' => 'Entidad no borrada'));
				}
			}
		}
	}