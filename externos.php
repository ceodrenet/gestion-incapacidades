<?php
    session_start();
    require_once 'modelos/tesoreria.modelo.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>GEIN - Gestor de Incapacidades</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="vistas/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="vistas/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="vistas/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="vistas/dist/css/AdminLTE.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="vistas/dist/css/skins/_all-skins.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>

        <!-- DataTables -->
        <link rel="stylesheet" href="vistas/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="vistas/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css">

        <!-- DatePicker -->
        <link rel="stylesheet" type="text/css" href="vistas/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">
        <!-- Alertify -->
        <link rel="stylesheet" href="vistas/plugins/alertify/alertify.core.css"/>
        <link rel="stylesheet" href="vistas/plugins/alertify/alertify.default.css"/>

        <link rel="shortcut icon" href="vistas/img/plantilla/LCortado.png">
   
    </head>
    <body class="hold-transition skin-red layout-top-nav fixed">
        <div class="wrapper">
            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">    
                            
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                            
            
                        </div>
                    <!-- /.navbar-collapse -->
                    <!-- Navbar Right Menu -->
                    <!-- /.navbar-custom-menu -->
                    </div>
                <!-- /.container-fluid -->
                </nav>
            </header>
            <!-- Full Width Column -->
            <div class="content-wrapper">
                <div class="container">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#" class="navbar-brand">
                                    <img src="vistas/img/plantilla/LOGO_1.png" class="img-responsive jose" > 
                                </a>
                            </div>
                        </div>
                    </section>

                    <!-- Main content -->
                    <section class="content">
                        <br/>
                        <div class="box box-danger solid">
                            <div class="box-header">
                                <h3 class="box-title">Datos de la Empresa</h3>
                            </div> 
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group" id="nitempresaDiv">
                                            <label>NIT de la empresa</label>
                                            <input type="text" name="txtNitEmpresa" id="txtNitEmpresa" class="form-control" placeholder="NIT de la empresa">
                                            <span class='help-block' id='spanHelblok'></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nombre de la empresa</label>
                                            <input type="text" readonly name="txtNombreEmpresa" id="txtNombreEmpresa" class="form-control" placeholder="Nombre de la empresa">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Nomina</label>
                                        <select class="form-control" name="txtNomina" id="txtNomina" placeholder="Nomina">
                                            <?php
                                                $campo = "nom_id_i, nom_desc_v";
                                                $tabla = "gi_nominas";
                                                $condi = "nom_tipo_v = 'Mensual'";
                                                $resultado = ModeloTesoreria::mdlMostrarGroupAndOrder($campo, $tabla, $condi, null, " ORDER BY nom_id_i ASC");
                                                // var_dump($resultado);
                                                foreach ($resultado as $key => $value) {
                                                    echo "<option value='".$value['nom_id_i']."'>".$value['nom_desc_v']."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>       
                        <div class="box box-danger solid">
                            <div class="box-header">
                                <h3 class="box-title">Cargar Incapacidades</h3>
                            </div> 
                            <div class="box-body">
                                <form id="foermularioMaxivoDocu"  role="form" autocomplete="off" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label style="text-align: center;">Identificación</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <label style="text-align: center;">Fecha inicio</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <label style="text-align: center;">Incapacidad imagen</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <label style="text-align: center;">Incapacidad transcrita</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-xs-12" id="filas">
                                             
                                                 
                                        </div>
                                   </div>
                                </form>
                                
                            </div>
                            <div class="box-footer">
                                <button type="button" id="AddcargaInformacion" class="btn btn-danger">Agregar fila</button>
 
                                <button type="button" id="cargarInformacionDocumentosMax" class="btn btn-danger">Guardar Incapacidades</button>
                            </div>
                        </div>            
                        <!-- /.box -->
                    </section>
                    <!-- /.content -->
                </div>
            <!-- /.container -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.1
                </div>
                <strong>Copyright &copy; 2018 <a href="http://groupge.co" target="_blank">Incapacidades.co</a>.</strong> Todos los derechos reservados.
            </footer>
            </div>
            <!-- ./wrapper -->

            <!-- jQuery 3 -->
            <script src="vistas/bower_components/jquery/dist/jquery.min.js"></script>
            <!-- Bootstrap 3.3.7 -->
            <script src="vistas/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- SlimScroll -->
            <script src="vistas/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
            <!-- FastClick -->
            <script src="vistas/bower_components/fastclick/lib/fastclick.js"></script>
            <!-- AdminLTE App -->
            <script src="vistas/dist/js/adminlte.min.js"></script>

            <!-- alertify -->
            <script src="vistas/plugins/alertify/alertify.js"></script>
            <!-- Blokear pr ajax -->
            <script src="vistas/plugins/blockui/blockUi.js"></script>
            <!-- DatePicker -->
            <script type="text/javascript" src="vistas/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

            <!-- AdminLTE for demo purposes -->
            <script type="text/javascript">
                var contadorFilas = 0;  
                var miEmpresa = 0;

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    weekStart: 0
                };
                
                $(function(){
                    $("#AddcargaInformacion").click(function(){
                        var campos = "<div class='row' id='row_"+contadorFilas+"'>";
                        campos += "<div class='col-md-3'>";
                        campos += "<div class='form-group' id='divInputCedula_"+contadorFilas+"'><input type='text' class='form-control cedulasI' name='Editarcedula_"+contadorFilas+"' numero='"+contadorFilas+"' id='Editarcedula_"+contadorFilas+"' placeholder='Identificación'><span class='help-block' id='spanHelblok_"+contadorFilas+"'></span></div>";
                        campos += "</div>";
                        campos += "<div class='col-md-2'>";
                        campos += "<div class='form-group'><input type='text' class='form-control fechasinicio' name='EditarFechaInicio_"+contadorFilas+"' id='EditarFechaInicio_"+contadorFilas+"' placeholder='Fecha Inicio'></div>";
                        campos += "</div>";
                        campos += "<div class='col-md-3'>";
                        campos += "<div class='form-group'><input type='file' class='form-control cedulasI' name='EditarFilePrimerImagen_"+contadorFilas+"' id='EditarFilePrimerImagen_"+contadorFilas+"' placeholder='Identificación'></div>";
                        campos += "</div>";
                        campos += "<div class='col-md-3'>";
                        campos += "<div class='form-group'><input type='file' class='form-control cedulasI' name='EditarSegundaImagen_"+contadorFilas+"' id='EditarSegundaImagen_"+contadorFilas+"' placeholder='Identificación'></div>";
                        campos += "</div>";
                        campos += "<div class='col-md-1'>";
                        campos += "<button type='button' class='btn btn-danger eliminarImagenes' id = 'borrar_"+contadorFilas+"' idfila='"+contadorFilas+"' title='Borrar fila'><i class='fa fa-trash-o'></i></button>";
                        campos += "</div>";
                 
                        $("#filas").append(campos);
                 
                        $("#borrar_"+contadorFilas).click(function(){
                            var id = $(this).attr('idfila');
                            $("#row_"+id).remove();
                        });
                 
                        $("#Editarcedula_"+contadorFilas).change(function(){
                            $(".alert").remove();
                            var usuario = $(this).val();
                            var datos = new FormData();
                            var numero = $(this).attr('numero');
                            datos.append('validarCedula', usuario);
                            datos.append('empresa', miEmpresa);
                            $.ajax({
                                url   : 'ajax/incapacidades.ajax.php',
                                method: 'post',
                                data  : datos,
                                cache : false,
                                contentType : false,
                                processData : false,
                                dataType    : 'json',
                                success     : function(respuesta){
                                    if(respuesta == false){
                                        alertify.error('Esta cedula no existe');
                                        $("#Editarcedula_"+numero).focus();
                                        $("#spanHelblok_"+numero).html('Identificación no registrada');
                                        $("#divInputCedula_"+numero).removeClass( "has-warning has-success" ).addClass("has-error");
                                    }else{
                                        console.log("Soy Batman ::> respuesta.emp_estado ::> "+ respuesta.emd_estado)
                                        if(respuesta.emd_estado == '1'){
                                            console.log("Soy Batman ::> seguimiento ::> "+ respuesta.emd_estado)
                                           $("#spanHelblok_"+numero).html('Empleado valido');
                                            $("#divInputCedula_"+numero).removeClass( "has-error has-warning" ).addClass("has-success");
                                        }else{
                                            console.log("Soy Batman ::> seguimiento ::> "+ respuesta.emd_estado)
                                            $("#spanHelblok_"+numero).html('Empleado valido pero no activo');
                                            $("#divInputCedula_"+numero).removeClass( "has-error has-success" ).addClass("has-warning");
                                        }
                                    }
                                }
                 
                            })
                        });
                        
                        $('#EditarFechaInicio_'+contadorFilas).datepicker({
                            language: "es",
                            autoclose: true,
                            todayHighlight: true    
                        });
                        contadorFilas++;
                    });

                    $("#cargarInformacionDocumentosMax").click(function(){
                        if(miEmpresa != 0){
                            var form = $("#foermularioMaxivoDocu");
                            //Se crean un array con los datos a enviar, apartir del formulario 
                            var formData = new FormData($("#foermularioMaxivoDocu")[0]);
                            formData.append('contados', contadorFilas);
                            formData.append('empresa', miEmpresa);
                            formData.append('txtNomina', $("#txtNomina").val());
                            $.ajax({
                                url: 'ajax/solo_cargar_imagenes_nomina.php',
                                type  : 'post',
                                data: formData,
                                dataType : 'json',
                                cache: false,
                                contentType: false,
                                processData: false,
                                beforeSend:function(){
                                    $.blockUI({ 
                                        baseZ: 2000,
                                        css: { 
                                            border: 'none', 
                                            padding: '1px', 
                                            backgroundColor: '#000', 
                                            '-webkit-border-radius': '10px', 
                                            '-moz-border-radius': '10px', 
                                            opacity: .5, 
                                            color: '#fff'
                                        } 
                                    }); 
                                },
                                complete:function(){
                                    $.unblockUI();
                                },
                                //una vez finalizado correctamente
                                success: function(data){
                                   
                                    alertify.success('Proceso terminado, total registros '+ data.total +', se cargaron con exito '+ data.exito +', Datos Errados '+ data.fallas );
                                
                                       
                                },
                                //si ha ocurrido un error
                                error: function(){
                                    //after_save_error();
                                    alertify.error('Error al realizar el proceso');
                                }
                            });
                        }else{
                            alertify.error("Digite el NIT de la empresa y seleccione la nomina a cargar");
                        }
                         
                    });
                    
                    $("#txtNitEmpresa").change(function(){
                        $("#spanHelblok").html('');
                        $("#nitempresaDiv").removeClass("has-success");
                        $("#nitempresaDiv").removeClass("has-error");
                        $("#nitempresaDiv").removeClass("has-warning");
                        
                        var nit = $(this).val();
                        var datas = new FormData();
                        datas.append('GetClienteId', nit);
                        $.ajax({
                            url   : 'ajax/clientes.ajax.php',
                            method: 'post',
                            data  : datas,
                            cache : false,
                            contentType : false,
                            processData : false,
                            dataType    : 'json',
                            success     : function(respuesta){
                                if(respuesta == false){
                                    alertify.error('Este NIT no existe');
                                    $("#txtNitEmpresa").focus();
                                    $("#spanHelblok").html('NIT no registrado');
                                    $("#nitempresaDiv").removeClass( "has-success has-warning" ).addClass("has-error");
                                    $("#txtNombreEmpresa").val("");
                                    miEmpresa = 0;
                                }else{
                                    console.log("Soy Batman ::> respuesta.emp_estado ::> "+ respuesta.emp_estado)
                                    if(respuesta.emp_estado == '1'){
                                        console.log("Soy Batman ::> seguimiento ::> "+ respuesta.emp_estado)

                                        $("#txtNombreEmpresa").val(respuesta.emp_nombre);
                                        miEmpresa = respuesta.emp_id;
                                        $("#spanHelblok").html('NIT valido');
                                        $("#nitempresaDiv").removeClass( "has-error has-warning" ).addClass("has-success");
                                        
                                    }else{
                                        $("#spanHelblok").html('Empresa no activa');
                                        $("#nitempresaDiv").removeClass( "has-error has-success" ).addClass("has-warning");
                                        miEmpresa = 0;
                                    }
                                }
                            }
             
                        })
                    });
                });
            </script>
    </body>
</html>