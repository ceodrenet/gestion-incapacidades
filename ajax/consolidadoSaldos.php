<?php 
	session_start();
	$_SESSION['start'] = time();
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
    require_once '../controladores/incapacidades.controlador.php';
    require_once '../controladores/tesoreria.controlador.php';
    
    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';
    require_once '../vendor/autoload.php';
    require_once '../extenciones/Excel.php';

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

	if(isset($_POST['selecTNominas'])){
		
    

		$month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	    $nomina = $_POST['selecTNominas'];
	    if($nomina < 10){
	        $nomina = '0'.$nomina;
	    }

	    if($nomina > 0){
	    	$year = $_POST['txtYearOfInitSaldos'].'-'.$nomina.'-01';
	    	$otherYear =  date("Y-m-d",strtotime($year."+ 1 month"));// $_POST['anho'].'-'.$nomina.'-31';
	    	$otherYear =  date("Y-m-d",strtotime($otherYear."- 1 days"));
	    }else{
	    	$year = $_POST['txtYearOfInitSaldos'].'-01-01';
	    	$otherYear =  $_POST['txtYearOfInitSaldos'].'-12-31';
	    }
	    

	    $objPHPExcel = new Spreadsheet();

		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->getProperties()->setCreator("GEIN")
		                     ->setLastModifiedBy("".$_SESSION['nombres'])
		                     ->setTitle("GEIN - Consolidado de saldos")
		                     ->setSubject("Consolidado de saldos")
		                     ->setDescription("Descarga la Consolidado de saldos, registrado en el sistema")
		                     ->setKeywords("office 2007 openxml php")
		                     ->setCategory("Consolidado de saldos");

		$objPHPExcel->getActiveSheet();

		$borders = array(
				'borders' => array(
				'allborders' => array(
					'style' =>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => array('argb' => 'FF000000'),
				)
			),
		);


		$campos = "emp_nombre, emp_direccion, emp_nit, emp_telefono";
   		$tablas = "gi_empresa";
   		$condicion = "emp_id = ".$_SESSION['cliente_id'];
   		$newEmpresa = ModeloTesoreria::mdlMostrarUnitario($campos, $tablas, $condicion);

   		$objPHPExcel->getActiveSheet()->setTitle('INFORME');
   		$objPHPExcel->getActiveSheet()->getStyle('B4:I4')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle('B4:I4')->applyFromArray($borders);
		//$objPHPExcel->getActiveSheet()->getStyle('B2:G2')->applyFromArray($borders);

		$objPHPExcel->getActiveSheet()->mergeCells('B4:I4');
		$objPHPExcel->getActiveSheet()->getStyle("B4:I4")->getFont()->setSize(16); 
		$objPHPExcel->getActiveSheet()->setCellValue("B4", "REPORTE CONSOLIDADO DE SALDOS"); 

		$objPHPExcel->getActiveSheet()
	        ->getStyle('B4')
	        ->getAlignment()
	        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    $objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
	    $objPHPExcel->getActiveSheet()->getStyle('B4')->getFill()
	        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
	        ->getStartColor()
	        ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

    $objPHPExcel->getActiveSheet()->getStyle('A1:AR1')->getFont()->setBold( true );

	    $objPHPExcel->getActiveSheet()->setCellValue("B6", "EMPRESA"); 
     	$objPHPExcel->getActiveSheet()->setCellValue("C6", mb_strtoupper($newEmpresa['emp_nombre'])); 
	    $objPHPExcel->getActiveSheet()->setCellValue("B7", "NIT"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("C7", mb_strtoupper($newEmpresa['emp_nit']));
	    $objPHPExcel->getActiveSheet()->setCellValue("B8", "NÓMINA"); 
	    if($_POST['selecTNominas'] == 0){
	    	$objPHPExcel->getActiveSheet()->setCellValue("C8", " Nomina Total Año ".$_POST['txtYearOfInitSaldos']); 
	    }else{
	    	$objPHPExcel->getActiveSheet()->setCellValue("C8", $month[$_POST['selecTNominas'] -1]." ".$_POST['txtYearOfInitSaldos']); 
	    }
	   	

	    $objPHPExcel->getActiveSheet()->getStyle('B4:I4')->getFont()->setBold( true );

	    $objPHPExcel->getActiveSheet()->setCellValue("H7", "FECHA"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("I7", date('d/m/Y')); 
	    $objPHPExcel->getActiveSheet()->getStyle('H7')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT );
	    $objPHPExcel->getActiveSheet()->getStyle('I7')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT );

	    $objPHPExcel->getActiveSheet()->mergeCells('B11:C11');
	    $objPHPExcel->getActiveSheet()->getStyle('B11:E11')->applyFromArray($borders);
	    $objPHPExcel->getActiveSheet()->getStyle('B11:E11')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->setCellValue("B11", "DETALLE"); 
		$objPHPExcel->getActiveSheet()->getStyle('B11')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue("D11", "#"); 
		$objPHPExcel->getActiveSheet()->setCellValue("E11", "VALOR"); 
		$objPHPExcel->getActiveSheet()->mergeCells('B12:C12');
	    $objPHPExcel->getActiveSheet()->getStyle('B12:E12')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B12", "TOTAL INCAPACIDADES NÓMINA"); 
		$objPHPExcel->getActiveSheet()->getStyle('B12')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		/*Vamos a obtener todas las Incapacidades de la nomina*/
		$campos = "sum(inc_valor_pagado_eps) as totalAdmin, sum(inc_valor_pagado_empresa) as totalEmp, sum(inc_valor_pagado_ajuste) as totalAjuste, count(*) as totalInca";
	    $tabla = "gi_incapacidad";
	    $condiciones = '';
	    if($_SESSION['cliente_id'] != 0){
	        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND ";
	    }
        $condiciones .= " inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
	    $respuesta2 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
	    $incAdministradora = $respuesta2['totalAdmin'];
	    $incEmpresa_______ = $respuesta2['totalEmp'];
	    $incTotalIncapacid = $respuesta2['totalInca'];
	    $incAjuste________ = $respuesta2['totalAjuste'];

	    $objPHPExcel->getActiveSheet()->setCellValue("D12", $incTotalIncapacid); 
		$objPHPExcel->getActiveSheet()->setCellValue("E12", ($incEmpresa_______ + $incAdministradora + $incAjuste________)); 
		$objPHPExcel->getActiveSheet()->getStyle('E12')->getNumberFormat()->setFormatCode('#,##0');


		$objPHPExcel->getActiveSheet()->mergeCells('B14:C14');
	 	$objPHPExcel->getActiveSheet()->getStyle('B14:E14')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B14", "EMPRESA ( 1 Y 2 DÍAS)"); 
		$objPHPExcel->getActiveSheet()->mergeCells('B15:C15');
		$objPHPExcel->getActiveSheet()->getStyle('B15:E15')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B15", "ADMINISTRADORAS (POR COBRAR)");
		$objPHPExcel->getActiveSheet()->mergeCells('B16:C16'); 
		$objPHPExcel->getActiveSheet()->getStyle('B16:E16')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B16", "AJUSTE DE INCAPACIDAD"); 
		$objPHPExcel->getActiveSheet()->mergeCells('B17:C17');
		$objPHPExcel->getActiveSheet()->getStyle('B17:E17')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B17", "TOTAL"); 
		$objPHPExcel->getActiveSheet()->getStyle('B17')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('B11:C11')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('B17')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		/*Vamos a deglosar el Reporte*/
		$campos = "count(*) as totalInca";
	    $tabla = "gi_incapacidad";
	    $condiciones = '';
	    if($_SESSION['cliente_id'] != 0){
	        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND ";
	    }
        $condiciones .= " inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = '1'";
	    $respuestaEpres = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

	    $campos = "count(*) as totalInca";
	    $tabla = "gi_incapacidad";
	    $condiciones = '';
	    if($_SESSION['cliente_id'] != 0){
	        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND ";
	    }
        $condiciones .= " inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != '1'";
	    $respuestaAmdin = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
	    $totalEmpresa__ = $respuestaEpres['totalInca'];
	    $totalIncapa___ = $respuestaAmdin['totalInca'];

	    $objPHPExcel->getActiveSheet()->setCellValue("D14", $totalEmpresa__); 
		$objPHPExcel->getActiveSheet()->setCellValue("D15", $totalIncapa___); 
		$objPHPExcel->getActiveSheet()->setCellValue("D16", '0');
		$objPHPExcel->getActiveSheet()->setCellValue("D17", ($totalEmpresa__ + $totalIncapa___));

		$objPHPExcel->getActiveSheet()->setCellValue("E14", $incEmpresa_______); 
		$objPHPExcel->getActiveSheet()->setCellValue("E15", $incAdministradora); 
		$objPHPExcel->getActiveSheet()->setCellValue("E16", $incAjuste________);
		$objPHPExcel->getActiveSheet()->setCellValue("E17", ($incEmpresa_______ + $incAdministradora+$incAjuste________)); 
		$objPHPExcel->getActiveSheet()->getStyle('E14')->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('E15')->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('E16')->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('E17')->getNumberFormat()->setFormatCode('#,##0');


		/*HASTA AQUI SE DEGLOSO POR ADMINUSTRADORA Y EMPRESAS*/
		$objPHPExcel->getActiveSheet()->mergeCells('B19:E19');
		
		$objPHPExcel->getActiveSheet()->setCellValue("B19", "CLASIFICACIÓN POR ORIGEN (POR COBRAR)");
		$objPHPExcel->getActiveSheet()->getStyle('B19')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('B19')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objPHPExcel->getActiveSheet()->mergeCells('B20:C20');
	    $objPHPExcel->getActiveSheet()->getStyle('B20:E20')->applyFromArray($borders);
	    $objPHPExcel->getActiveSheet()->getStyle('B20:E20')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->setCellValue("B20", "DETALLE"); 
		$objPHPExcel->getActiveSheet()->setCellValue("D20", "#"); 
		$objPHPExcel->getActiveSheet()->setCellValue("E20", "VALOR"); 
		$objPHPExcel->getActiveSheet()->getStyle('B20:E20')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->mergeCells('B21:C21');
	    $objPHPExcel->getActiveSheet()->getStyle('B21:E21')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B21", "ENFERMEDAD GENERAL"); 
		$objPHPExcel->getActiveSheet()->mergeCells('B22:C22');
	    $objPHPExcel->getActiveSheet()->getStyle('B22:E22')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B22", "ACCIDENTE DE TRANSITO"); 
		$objPHPExcel->getActiveSheet()->mergeCells('B23:C23');
	    $objPHPExcel->getActiveSheet()->getStyle('B23:E23')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B23", "LICENCIA DE MATERNIDAD"); 
		$objPHPExcel->getActiveSheet()->mergeCells('B24:C24');
	    $objPHPExcel->getActiveSheet()->getStyle('B24:E24')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B24", "LICENCIA DE PATERNIDAD"); 
		$objPHPExcel->getActiveSheet()->mergeCells('B25:C25');
	    $objPHPExcel->getActiveSheet()->getStyle('B25:E25')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B25", "ACCIDENTE LABORAL"); 
		$objPHPExcel->getActiveSheet()->mergeCells('B26:C26');
	    $objPHPExcel->getActiveSheet()->getStyle('B26:E26')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B26", "TOTAL"); 
		$objPHPExcel->getActiveSheet()->getStyle('B26:C26')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('B26')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		/*DEGLOSAMOS POR TIPO DE INCAPACIDAD*/

		/*ENFERMEDAD GENERAL*/
		$hwre = '';
		if($_SESSION['cliente_id'] != 0){
            $hwre = " inc_empresa = ".$_SESSION['cliente_id']." AND ";
        }
		$campos = "count(*) as totalIncapacidades, sum(inc_valor_pagado_eps) as totalDeuda";
        $tabla = "gi_incapacidad";
        /****************/
        $condicionesEG = $hwre ."inc_origen = '1' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != '1' ";
        $respuestaeEG =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicionesEG);

        /****************/
        $condicionesLM = $hwre."inc_origen = '2' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != '1' ";
        $respuestaeLM =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicionesLM);

        /****************/
        $condicionesLP = $hwre."inc_origen = '3' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != '1' ";
        $respuestaeLP =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicionesLP);

        /****************/
        $condicionesAL = $hwre."inc_origen = '4' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != '1'";
        $respuestaeAL =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicionesAL);

        /****************/
        $condicionesAT = $hwre."inc_origen = '5' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != '1'";
        $respuestaeAT =  ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicionesAT);


        $numIncaEG____ = $respuestaeEG['totalIncapacidades'];
        $numIncaLM____ = $respuestaeLM['totalIncapacidades'];
        $numIncaLP____ = $respuestaeLP['totalIncapacidades'];
        $numIncaAL____ = $respuestaeAL['totalIncapacidades'];
        $numIncaAT____ = $respuestaeAT['totalIncapacidades'];
        $total________ = ($numIncaEG____ + $numIncaLM____+ $numIncaLP____+ $numIncaAL____ + $numIncaAT____);

        $totalIncaEG____ = $respuestaeEG['totalDeuda'];
        $totalIncaLM____ = $respuestaeLM['totalDeuda'];
        $totalIncaLP____ = $respuestaeLP['totalDeuda'];
        $totalIncaAL____ = $respuestaeAL['totalDeuda'];
        $totalIncaAT____ = $respuestaeAT['totalDeuda'];
        $totalValor_____ = ($totalIncaEG____ + $totalIncaLM____+ $totalIncaLP____+ $totalIncaAL____ + $totalIncaAT____);

		$objPHPExcel->getActiveSheet()->setCellValue("D21", $numIncaEG____); 
		$objPHPExcel->getActiveSheet()->setCellValue("E21", $totalIncaEG____); 
		$objPHPExcel->getActiveSheet()->setCellValue("D22", $numIncaAT____); 
		$objPHPExcel->getActiveSheet()->setCellValue("E22", $totalIncaAT____); 
		$objPHPExcel->getActiveSheet()->setCellValue("D23", $numIncaLM____); 
		$objPHPExcel->getActiveSheet()->setCellValue("E23", $totalIncaLM____); 
		$objPHPExcel->getActiveSheet()->setCellValue("D24", $numIncaLP____); 
		$objPHPExcel->getActiveSheet()->setCellValue("E24", $totalIncaLP____); 
		$objPHPExcel->getActiveSheet()->setCellValue("D25", $numIncaAL____); 
		$objPHPExcel->getActiveSheet()->setCellValue("E25", $totalIncaAL____); 
		$objPHPExcel->getActiveSheet()->setCellValue("D26", $total________); 
		$objPHPExcel->getActiveSheet()->setCellValue("E26", $totalValor_____); 
		$objPHPExcel->getActiveSheet()->getStyle('E21')->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('E22')->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('E23')->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('E24')->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('E25')->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('E26')->getNumberFormat()->setFormatCode('#,##0');
		/*HASTA AQUI*/


		$objPHPExcel->getActiveSheet()->mergeCells('B28:E28');
		$objPHPExcel->getActiveSheet()->getStyle('B28:E28')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B28", "ENTIDADES (POR COBRAR)");
		$objPHPExcel->getActiveSheet()->getStyle('B28:E28')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('B29:E29')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("B29", "NIT"); 
		$objPHPExcel->getActiveSheet()->setCellValue("C29", "ENTIDAD"); 
		$objPHPExcel->getActiveSheet()->setCellValue("D29", "#"); 
		$objPHPExcel->getActiveSheet()->setCellValue("E29", "VALOR"); 
		$objPHPExcel->getActiveSheet()->getStyle('B29:E29')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('B28')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		/****************DEGLOSADO DE ENTIDADES***************/
		/**************ARL***************/
		$campos___ = 'DISTINCT ips_nombre, ips_nit';
		$tablas___ = 'gi_empleados JOIN gi_ips ON ips_id = emd_arl_id';
		$respuestaeARL =  ModeloTesoreria::mdlMostrarUnitario($campos___, $tablas___, 'emd_emp_id = '.$_SESSION['cliente_id']);

	
		$objPHPExcel->getActiveSheet()->setCellValue("B30", $respuestaeARL['ips_nit']); 
		$objPHPExcel->getActiveSheet()->setCellValue("C30", $respuestaeARL['ips_nombre']); 
		$objPHPExcel->getActiveSheet()->setCellValue("D30", $numIncaAL____); 
		$objPHPExcel->getActiveSheet()->setCellValue("E30", $totalIncaAL____); 
 		$objPHPExcel->getActiveSheet()->getStyle('B30:E30')->applyFromArray($borders);
 		$objPHPExcel->getActiveSheet()->getStyle('E30')->getNumberFormat()->setFormatCode('#,##0');
 		/****************ARL***************/
 		/****************EPS***************/
 		$strCampo = "ips_nombre, ips_nit, ips_id";
		$strTabla = " gi_ips JOIN gi_empleados ON emd_eps_id = ips_id";
		$strWhere = '1 = 1';
		if($_SESSION['cliente_id'] != 0){
		    $strWhere .= " AND emd_emp_id = ".$_SESSION['cliente_id'];
		}
		$arrResul = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampo,$strTabla,$strWhere, 'GROUP BY ips_id', ' ORDER BY ips_nombre ASC');
		$i = 31;
		$valorDeudaTotal = $totalIncaAL____;
		$incaTotales____ = $numIncaAL____;
		foreach ($arrResul as $key => $eps) {
			$strCampo = "sum(inc_valor_pagado_eps) as totalDeuda, count(*) as totalInc";
	        $strTabla = "gi_incapacidad JOIN gi_empleados ON emd_id = inc_emd_id";
	        $strWhere = " inc_origen != '4' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND emd_eps_id = ".$eps['ips_id']." AND inc_estado_tramite != '1'";
	        if($_SESSION['cliente_id'] != 0){
	            $strWhere .= " AND inc_empresa = ".$_SESSION['cliente_id'];
	        }
	        $arrRespuestaIP = ModeloTesoreria::mdlMostrarUnitario($strCampo, $strTabla, $strWhere);
	    	
	    	if($arrRespuestaIP['totalInc'] > 0 && $arrRespuestaIP['totalDeuda'] > 0){
	    		$objPHPExcel->getActiveSheet()->setCellValue("B".$i, $eps['ips_nit']); 
				$objPHPExcel->getActiveSheet()->setCellValue("C".$i, $eps['ips_nombre']); 
				$objPHPExcel->getActiveSheet()->setCellValue("D".$i, $arrRespuestaIP['totalInc']); 
				$objPHPExcel->getActiveSheet()->setCellValue("E".$i, $arrRespuestaIP['totalDeuda']); 
		 		$objPHPExcel->getActiveSheet()->getStyle('B'.$i.':E'.$i)->applyFromArray($borders);
		 		$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('#,##0');
		 		$valorDeudaTotal += $arrRespuestaIP['totalDeuda'];
		 		$incaTotales____ += $arrRespuestaIP['totalInc'];
		 		$i++;
	    	}
	    	
		}

		$objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':C'.$i);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$i.':C'.$i)->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('B'.$i.':E'.$i)->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("D".$i, $incaTotales____); 
		$objPHPExcel->getActiveSheet()->setCellValue("E".$i, $valorDeudaTotal);
		$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('#,##0');
 		/****************EPS***************/
		/****************FIN DEGLOSADO DE ENTIDADES***************/



		/****************************************************************************************/    
		 /*columna desde la G . I*/
	 	$objPHPExcel->getActiveSheet()->getStyle('G11:I11')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    $objPHPExcel->getActiveSheet()->getStyle('G11:I11')->applyFromArray($borders);
	    $objPHPExcel->getActiveSheet()->getStyle('G11:I11')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->setCellValue("G11", "DURACIÓN"); 
		$objPHPExcel->getActiveSheet()->setCellValue("H11", "#"); 
		$objPHPExcel->getActiveSheet()->setCellValue("I11", "VALOR"); 

	    $objPHPExcel->getActiveSheet()->getStyle('G12:I12')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("G12", "INCAPACIDADES ENTRE 1 Y 2 DÍAS"); 

	    $objPHPExcel->getActiveSheet()->getStyle('G13:I13')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("G13", "INCAPACIDADES ENTRE 3 Y 5 DÍAS"); 
	
	    $objPHPExcel->getActiveSheet()->getStyle('G14:I14')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("G14", "INCAPACIDADES ENTRE 6 Y 15 DÍAS"); 

	    $objPHPExcel->getActiveSheet()->getStyle('G15:I15')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("G15", "INCAPACIDADES ENTRE 16 Y 30 DÍAS"); 

	    $objPHPExcel->getActiveSheet()->getStyle('G16:I16')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("G16", "INCAPACIDADES MÁS DE 30 DÍAS"); 
	
	    $objPHPExcel->getActiveSheet()->getStyle('G17:I17')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("G17", "TOTAL"); 
		$objPHPExcel->getActiveSheet()->getStyle('G17')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('G17')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		/*********************INCAPACIDADES POR DIAS****************/
		$campos = "inc_fecha_inicio, inc_fecha_final, inc_valor_pagado_eps, inc_valor_pagado_empresa";
	    $tabla = "gi_incapacidad";
	    $condiciones = "inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' ";
	    if($_SESSION['cliente_id'] != 0){
	        $condiciones .= "AND inc_empresa = ".$_SESSION['cliente_id'];
	    }
	    $incapacidades =  ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$condiciones, $groupBy = null, $orderBy = null);
		$d1=0;
		$d3=0;
		$d6=0;
		$d16=0;
		$d31=0;

		$Vd1=0;
		$Vd3=0;
		$Vd6=0;
		$Vd16=0;
		$Vd31=0;
	    foreach ($incapacidades as $key => $value) {
	    	$dias = ControladorIncapacidades::dias_transcurridos($value['inc_fecha_inicio'], $value['inc_fecha_final']);
	    	if($dias < 3){
	    		$d1++;
	    		if (is_numeric( $value['inc_valor_pagado_empresa']) ) {
				  	$Vd1 += $value['inc_valor_pagado_empresa'];
				}
	    	}else if($dias>2 && $dias < 6){
	    		$d3++;
	    		if (is_numeric( $value['inc_valor_pagado_eps']) ) {
				  	$Vd3 += $value['inc_valor_pagado_eps'];
				}

	    	}else if($dias>5 && $dias < 16){
	    		$d6++;
	    		if (is_numeric( $value['inc_valor_pagado_eps']) ) {
				  	$Vd6 += $value['inc_valor_pagado_eps'];
				}
	    	}else if($dias>15 && $dias < 31){
	    		$d16++;
	    		if (is_numeric( $value['inc_valor_pagado_eps']) ) {
				  	$Vd16 += $value['inc_valor_pagado_eps'];
				}
	    	}else{
	    		$d31++;
	    		if (is_numeric( $value['inc_valor_pagado_eps']) ) {
				  	$Vd31 += $value['inc_valor_pagado_eps'];
				}
	    	}
	    }


	    $objPHPExcel->getActiveSheet()->setCellValue("H12", $d1); 
		$objPHPExcel->getActiveSheet()->setCellValue("I12", $Vd1); 
		$objPHPExcel->getActiveSheet()->setCellValue("H13", $d3); 
		$objPHPExcel->getActiveSheet()->setCellValue("I13", $Vd3); 
		$objPHPExcel->getActiveSheet()->setCellValue("H14", $d6); 
		$objPHPExcel->getActiveSheet()->setCellValue("I14", $Vd6); 
		$objPHPExcel->getActiveSheet()->setCellValue("H15", $d16); 
		$objPHPExcel->getActiveSheet()->setCellValue("I15", $Vd16); 
		$objPHPExcel->getActiveSheet()->setCellValue("H16", $d31); 
		$objPHPExcel->getActiveSheet()->setCellValue("I16", $Vd31); 
		$objPHPExcel->getActiveSheet()->setCellValue("H17", ($d31+$d16+$d6+$d3+$d1)); 
		$objPHPExcel->getActiveSheet()->setCellValue("I17", ($Vd31+$Vd16+$Vd6+$Vd3+$Vd1)); 

		$objPHPExcel->getActiveSheet()->getStyle('I12:I17')->getNumberFormat()->setFormatCode('#,##0');
		/*********************INCAPACIDADES POR DIAS****************/

		$objPHPExcel->getActiveSheet()->getStyle('G21:I21')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    $objPHPExcel->getActiveSheet()->getStyle('G21:I21')->applyFromArray($borders);
	    $objPHPExcel->getActiveSheet()->getStyle('G21:I21')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->setCellValue("G21", "TIPO"); 
		$objPHPExcel->getActiveSheet()->setCellValue("H21", "#"); 
		$objPHPExcel->getActiveSheet()->setCellValue("I21", "VALOR"); 

	    $objPHPExcel->getActiveSheet()->getStyle('G22:I22')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("G22", "INICIAL"); 

	    $objPHPExcel->getActiveSheet()->getStyle('G23:I23')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("G23", "PRÓRROGA"); 

	    $objPHPExcel->getActiveSheet()->getStyle('G24:I24')->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("G24", "TOTAL"); 
		$objPHPExcel->getActiveSheet()->getStyle('G24')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('G24')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		/****************INCAPACIADES POR CLASIFICACION****************/
		$campos___ = 'count(*) as original, SUM(inc_valor_pagado_eps) as total ';
		$tablas___ = 'gi_incapacidad';
		$condicion = "inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_clasificacion = 1 ";
		if($_SESSION['cliente_id'] != 0){
		    $condicion .= " AND inc_empresa = ".$_SESSION['cliente_id'];
		}
		$respuestaeClasificacion =  ModeloTesoreria::mdlMostrarGroupAndOrder($campos___, $tablas___, $condicion, ' GROUP BY inc_clasificacion', null);
		$totalCount = 0;
		$totalSumVa = 0;
		foreach ($respuestaeClasificacion as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue("H22", $value['original']); 
			$objPHPExcel->getActiveSheet()->setCellValue("I22", $value['total']); 
			$objPHPExcel->getActiveSheet()->getStyle('I22')->getNumberFormat()->setFormatCode('#,##0');
			$totalCount += $value['original'] ;
			$totalSumVa += $value['total'];
		}


		$campos___ = 'count(*) as proroga, SUM(inc_valor_pagado_eps) as total ';
		$tablas___ = 'gi_incapacidad';
		$condicion = "inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_clasificacion != 1 ";
		if($_SESSION['cliente_id'] != 0){
		    $condicion .= " AND inc_empresa = ".$_SESSION['cliente_id'];
		}
		$respuestaeClasificacionP =  ModeloTesoreria::mdlMostrarGroupAndOrder($campos___, $tablas___, $condicion, ' GROUP BY inc_clasificacion', null);
		foreach ($respuestaeClasificacionP as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValue("H23", $value['proroga']); 
			$objPHPExcel->getActiveSheet()->setCellValue("I23", $value['total']); 
			$objPHPExcel->getActiveSheet()->getStyle('I23')->getNumberFormat()->setFormatCode('#,##0');
			$totalCount += $value['proroga'] ;
			$totalSumVa += $value['total'];
		}


		$objPHPExcel->getActiveSheet()->setCellValue("H24", $totalCount); 
		$objPHPExcel->getActiveSheet()->setCellValue("I24", $totalSumVa); 
		$objPHPExcel->getActiveSheet()->getStyle('I24')->getNumberFormat()->setFormatCode('#,##0');
		/****************INCAPACIADES POR CLASIFICACION****************/

		$objPHPExcel->getActiveSheet()->setCellValue("G28", "CENTROS DE COSTO (POR COBRAR)");
		$objPHPExcel->getActiveSheet()->getStyle('G28')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->getStyle('G28')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle('G29:I29')
		    ->getAlignment()
		    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    $objPHPExcel->getActiveSheet()->getStyle('G29:I29')->applyFromArray($borders);
	    $objPHPExcel->getActiveSheet()->getStyle('G29:I29')->getFont()->setBold( true );
		$objPHPExcel->getActiveSheet()->setCellValue("G29", "CENTRO DE COSTO"); 
		$objPHPExcel->getActiveSheet()->setCellValue("H29", "#"); 
		$objPHPExcel->getActiveSheet()->setCellValue("I29", "VALOR"); 

		/*******************INCAPACIDAES POR CENTRO DE COSTOS************************/
		$campos___ = 'count(*) as total, SUM(inc_valor_pagado_eps) as valorInc, emd_centro_costos';
		$tablas___ = 'gi_incapacidad JOIN gi_empleados ON emd_id = inc_emd_id ';
		$condicion = "inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
		if($_SESSION['cliente_id'] != 0){
		    $condicion .= " AND inc_empresa = ".$_SESSION['cliente_id'];
		}

		//echo "SELECT $campos___ FROM $tablas___ WHERE $condicion  GROUP BY emd_centro_costos";
		$respuestaCentroCosto =  ModeloTesoreria::mdlMostrarGroupAndOrder($campos___, $tablas___, $condicion, ' GROUP BY emd_centro_costos', null);
		$i = 30;
		$totalCen = 0;
		$totalVcn = 0;
		//print_r($respuestaCentroCosto);
		foreach ($respuestaCentroCosto as $key => $value) {
			$objPHPExcel->getActiveSheet()->getStyle('G'.$i.':I'.$i)->applyFromArray($borders);
			$objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value['emd_centro_costos']); 
			$objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value['total']); 
			$objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value['valorInc']); 
			$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode('#,##0');
			$totalCen += $value['total'] ;
			$totalVcn += $value['valorInc'];
			$i++;
		}
		$objPHPExcel->getActiveSheet()->getStyle('G'.$i.':I'.$i)->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->setCellValue("G".$i, "TOTAL"); 
		$objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalCen); 
		$objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalVcn); 
		$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->getNumberFormat()->setFormatCode('#,##0');

		/*******************INCAPACIDAES POR CENTRO DE COSTOS************************/

		foreach(range('B','I') as $columnID) {
		    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
		        ->setAutoSize(true);
		}

		/*************************NUEVA HOJA*************************/
		$newHoja = $objPHPExcel->createSheet(1);
    	$newHoja->setTitle("LISTADO");
    	$newHoja->getStyle('A1:AD1')
    	->getAlignment()
    	->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
   		$newHoja->getStyle('A1:AD1')->getFont()->setBold( true );

		$newHoja->setCellValue("A1", "#"); 
		$newHoja->setCellValue("B1", "Empresa");
		$newHoja->setCellValue("C1", "Identificación");    
		$newHoja->setCellValue("D1", "Apellidos y Nombres"); 
		$newHoja->setCellValue("E1", "Cargo"); 
		$newHoja->setCellValue("F1", "Salario"); 
		$newHoja->setCellValue("G1", "Fecha Inicio"); 
		$newHoja->setCellValue("H1", "Fecha Final"); 
		$newHoja->setCellValue("I1", "Días"); 
		$newHoja->setCellValue("J1", "Clasificación"); 
		$newHoja->setCellValue("K1", "Diagnostico"); 
		$newHoja->setCellValue("L1", "Origen"); 
		$newHoja->setCellValue("M1", "Valor Empresa"); 
		$newHoja->setCellValue("N1", "Valor Administradora"); 
		$newHoja->setCellValue("O1", "Valor Ajuste"); 
		$newHoja->setCellValue("P1", "Fecha Pago En Nomina");  
		$newHoja->setCellValue("Q1", "Estado Tramite");
		$newHoja->setCellValue("R1", "Fecha de Estado");
		$newHoja->setCellValue("S1", "Valor Pagado"); 
		$newHoja->setCellValue("T1", "Fecha Pago"); 
		$newHoja->setCellValue("U1", "Sede"); 
		$newHoja->setCellValue("V1", "Centro de Costos"); 
		$newHoja->setCellValue("W1", "SubCentro de Costos");
		$newHoja->setCellValue("X1", "Ciudad"); 
		$newHoja->setCellValue("Y1", "EPS"); 
		$newHoja->setCellValue("Z1", "AFP"); 
		$newHoja->setCellValue("AA1", "Observación");
		$newHoja->setCellValue("AB1", "Atención");
		$newHoja->setCellValue("AC1", "Documentos");  
		$newHoja->setCellValue("AD1", "Estado Empleado"); 									

		$item = null;
	    $valor = null;
	    if($_SESSION['cliente_id'] != 0){
	        $item = 'inc_empresa';
	        $valor = $_SESSION['cliente_id'];
	    }
		$incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_exportar($item, $valor, $year, $otherYear, null, '%Y-%m-%d');
		$i = 2;
	    foreach ($incapacidades as $key => $value) {

	        $strClasificacion = $value["inc_clasificacion"];
	    
	        $strFecha = $value["inc_Fecha_estado"];
	        
	        $newHoja->setCellValue("A".$i, ($key+1)); 
	        $newHoja->setCellValue("B".$i, $value["emp_nombre"]);  
	        $newHoja->setCellValue("C".$i, $value["emd_cedula"]); 
	        $newHoja->setCellValue("D".$i, $value["emd_nombre"]); 
	        $newHoja->setCellValue("E".$i, $value["emd_cargo"]); 
	        $newHoja->setCellValue("F".$i, $value["emd_salario"]);  
			$newHoja->getStyle('F'.$i)->getNumberFormat()->setFormatCode('#,##0');

	        $date = new DateTime($value["inc_fecha_inicio"]);
	        $date2 = new DateTime($value["inc_fecha_final"]);
	        $newHoja->setCellValue("G".$i, PHPExcel_Shared_Date::PHPToExcel($date));
	        $newHoja->getStyle("G".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");  
	        $newHoja->setCellValue("H".$i, PHPExcel_Shared_Date::PHPToExcel($date2)); 
	        $newHoja->getStyle("H".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
	        $newHoja->setCellValue("I".$i, $value['dias']);
	        $newHoja->setCellValue("J".$i, $strClasificacion);
	        $newHoja->setCellValue("K".$i, $value["inc_diagnostico"]); 
	        $newHoja->setCellValue("L".$i, $value["inc_origen"]); 
	        $newHoja->setCellValue("M".$i, $value["inc_valor_pagado_empresa"]); 
	        $newHoja->setCellValue("N".$i, $value["inc_valor_pagado_eps"]); 
	        $newHoja->setCellValue("O".$i, $value["inc_valor_pagado_ajuste"]); 
	        $newHoja->getStyle('M'.$i)->getNumberFormat()->setFormatCode('#,##0');
	        $newHoja->getStyle('N'.$i)->getNumberFormat()->setFormatCode('#,##0');
	        $newHoja->getStyle('O'.$i)->getNumberFormat()->setFormatCode('#,##0');
	        $date3 = new DateTime($value["inc_fecha_pago_nomina"]);
	        $newHoja->setCellValue("P".$i, PHPExcel_Shared_Date::PHPToExcel($date3));
	        $newHoja->getStyle("P".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
	        $newHoja->setCellValue("Q".$i, $value["inc_estado_tramite"]);
	        $date5 = new DateTime($strFecha);
	        $newHoja->setCellValue("R".$i, PHPExcel_Shared_Date::PHPToExcel($date5));
	        $newHoja->getStyle("R".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
	        $newHoja->setCellValue("S".$i, $value["inc_valor"]); 
	        $newHoja->getStyle('S'.$i)->getNumberFormat()->setFormatCode('#,##0');
	        if($value["inc_fecha_pago"] != null && $value["inc_fecha_pago"] != ''){
	            $date6 = new DateTime($value["inc_fecha_pago"]); 
	            $newHoja->setCellValue("T".$i, PHPExcel_Shared_Date::PHPToExcel($date6)); 
	            $newHoja->getStyle("T".$i)->getNumberFormat()->setFormatCode("dd/mm/yyyy");
	        }else{
	            $newHoja->setCellValue("T".$i, $value["inc_fecha_pago"] ); 
	        }

	        $newHoja->setCellValue("U".$i, $value["emd_sede"]); 
	        $newHoja->setCellValue("V".$i, $value["emd_centro_costos"]); 
	        $newHoja->setCellValue("W".$i, $value["emd_subcentro_costos"]); 
	        $newHoja->setCellValue("X".$i, $value["emd_ciudad"]);
	        $newHoja->setCellValue("Y".$i, $value["inc_ips_afiliado"]);
	        $newHoja->setCellValue("Z".$i, $value["inc_afp_afiliado"]); 
	        //$newHoja->setCellValue("AA".$i, ); 
	        $newHoja->setCellValue("AB".$i, $value["atn_descripcion"]);
	        $newHoja->setCellValue("AC".$i, $value["inc_tipo_generacion"]);
	        $newHoja->setCellValue("AD".$i, $value["est_emp_desc_v"]);
	        $i++;      
   
	    }

	    foreach(range('A','AD') as $columnID) {
	        $newHoja->getColumnDimension($columnID)
	            ->setAutoSize(true);
	    }


	 	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header('Content-Disposition: attachment;filename="Informe_consolidado_saldos.xlsx"');
	    header('Cache-Control: max-age=0');
	    // If you're serving to IE 9, then the following may be needed
	    header('Cache-Control: max-age=1');

	    // If you're serving to IE over SSL, then the following may be needed
	    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	    header ('Pragma: public'); // HTTP/1.0
	    $writer = new Xlsx($objPHPExcel);
	    $writer->save("php://output");
	}
	
?>