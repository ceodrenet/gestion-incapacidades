<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/entidadesRegistradas.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/entidadesRegistradas.modelo.php';

    class AjaxRegistroEntidades
	{
		public $id_entidadesreg;
		public $validarNitEntidad= null;
		public $dataSelectInt;

		public function ajaxValidarNit(){
			$item ="enat_nit_v";
			$valor = $this-> validarNitEntidad;
			$respuesta = ControladorEntidadAtiende::ctrValidarEntidad($item,$valor);
			echo json_encode($respuesta);
		}

		public function ajaxEditarEntidad(){
			$item = 'enat_id_i';
			$valor = $this->id_entidadesreg;
			$respuesta = ControladorEntidadAtiende::getData('gi_entidad_atiende', $item, $valor);
			echo json_encode($respuesta);
		}

		public function ajaxRedEntidad(){
			$item = 'ei_ent_id';
			$valor = $this->id_entidadesreg;
			$respuesta = ControladorEntidadAtiende::ctrMostrarRedEntidad($item, $valor);
			echo json_encode($respuesta);
		}
		
		public function getDataSelectInt(){
			$valor = $this->dataSelectInt;
			$respuesta = ControladorEntidadAtiende::ctrBuscarEntidadesAtiende($valor);
			$i = 0;
			$datos = array();
			foreach ($respuesta as $key => $value) {
				$datos[$i]['id'] = $value['id'];
				$datos[$i]['text'] = $value['nit'].' - '.$value['name'];
				$i++;
			} 
			echo json_encode($datos);
		}
	}

	if(isset($_GET['getEntidadesSelect'])){
		$AjaxEntidadIps = new AjaxRegistroEntidades();
		$AjaxEntidadIps->dataSelectInt = $_POST['query'];
		$AjaxEntidadIps->getDataSelectInt();
	}

	if(isset($_POST['validarNit'])){
		$validarEntidad = new AjaxRegistroEntidades();
		$validarEntidad->validarNitEntidad =$_POST['validarNit'];
		$validarEntidad->ajaxValidarNit();
	}

	if(isset($_POST['id_Entidad']))
	{
	echo ControladorEntidadAtiende::ctrBorrarEntidadAtiende();
	}

	if (isset($_POST['EditarEntidadId'])) 
	{
		$editar = new AjaxRegistroEntidades();
		$editar->id_entidadesreg = $_POST['EditarEntidadId'];
		$editar->ajaxEditarEntidad();
	}

	if (isset($_POST['idEntidadRed'])) 
	{
		$editar = new AjaxRegistroEntidades();
		$editar->id_entidadesreg = $_POST['idEntidadRed'];
		$editar->ajaxRedEntidad();
	}

	if(isset($_POST['EditarNombre']))
	{
		echo ControladorEntidadAtiende::ctrEditarEntidadesAtiende();
	}

// invoca al controlador para ingresar el nuevo registro
	if(isset($_POST['IngresarNombre']))
	{
		echo ControladorEntidadAtiende::ctrCrearEntidadesAtiende();
	}
// invoca el controlador para mostrar los datos en la tabla
    if(isset($_GET['getEntidades']))
	{
        $clientes = ControladorEntidadAtiende::ctrMostrarEntidades(null, null);
echo '{
  	"data" : [';
  			$i = 0;
		 	foreach ($clientes as $key => $value) {
		 		if($i != 0){
            		echo ",";
            	}
				echo '[';
				echo '"'.$value['enat_nombre_v'].'",';
				echo '"'.$value['enat_nit_v'].'",';
                echo '"'.$value['enat_fecha_creacion_d'].'",';
				if ($value['enat_estado'] == 0) {
					echo '"INACTIVA",';
				}else {
					echo '"ACTIVA",';
				}
				echo '"'.$value['enat_id_i'].'"'; 
				echo ']';
            	$i++;
		 	}
		echo ']
}';
	}