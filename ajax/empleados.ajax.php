<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/empleados.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/empleados.modelo.php';
	require_once '../modelos/tesoreria.modelo.php';

	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxEmpleados
	{
		public $id_Empleado;
		public $activarId;
		public $activarEmpleado;
		public $validaEmpleado;
		public $empresa;
		public $dataSelectEmpleado;
		
		public function ajaxEditarEmpleado(){
			$item = 'emd_id';
			$valor = $this->id_Empleado;
			$respuesta = ControladorEmpleados::ctrMostrarEmpleados($item, $valor);
			echo json_encode($respuesta);
		}

		/* Activar Empleado */
		public function ajaxActivarEmpleado(){
			$tabla = 'gi_empleados';
			$item1 = 'emd_estado';
			$valor1 = $this->activarEmpleado;
			$item2 = 'emp_id';
			$valor2 = $this->activarId;
			$respuesta = ModeloEmpleados::mdlActualizarEmpleado($tabla, $item1, $valor1, $item2, $valor2);
		}

		/* validar que el usuairo no este repetido */
		public function ajaxValidarEmpleado(){
			$item = 'emd_cedula';
			$valor = $this->validaEmpleado;
			$valor2 = $this->empresa;
			$respuesta = ControladorEmpleados::ctrMostrarEmpleados_ByEmpresaX2($item, $valor, $valor2);
			echo json_encode($respuesta);
		}

		public function getDataSelectEmpleado(){
			$valor = $this->dataSelectEmpleado;
			$respuesta = ControladorEmpleados::ctrBuscarEmpleadosSelect($valor,$_SESSION['cliente_id']);
			$i = 0;
			$datos = array();
			foreach ($respuesta as $value) {
				$datos[$i]['id'] = $value['ident'];
				$datos[$i]['text'] = $value['ident'].' - '.$value['name'];
				$i++;
			} 
			echo json_encode($datos);
		}

	}
	
	
	if(isset($_POST['EditarEmpleadoId'])){
		if($_POST['EditarEmpleadoId'] != ''){
			$editar = new AjaxEmpleados();
			$editar->id_Empleado = $_POST['EditarEmpleadoId'];
			$editar->ajaxEditarEmpleado();
		}	
	}

	if(isset($_POST['ActivarId'])){
		$activarUsuarios = new AjaxEmpleados();
		$activarUsuarios->activarId = $_POST['ActivarId'];
		$activarUsuarios->activarEmpleado = $_POST['estado'];
		$activarUsuarios->ajaxActivarEmpleado();
	}


	if(isset($_POST['validarNit'])){
		$activarUsuarios = new AjaxEmpleados();
		$activarUsuarios->validaEmpleado = $_POST['validarNit'];
		$activarUsuarios->empresa 		= $_POST['empresaValidate'];
		$activarUsuarios->ajaxValidarEmpleado();
	}

	if(isset($_POST['verificarCasoEspecial'])){
		$campos = "cae_id_i, cae_emd_id_i, cae_con_rea_v, cae_xon_fecha_d, cae_condia_id_i, cae_con_entidad_id_i, cae_cal_v, cae_cal_fecha_d, cae_cal_dia_id_i, cae_cal_entidad_i, cae_cal_jur_v, cae_cal_jur_fecha_d, cae_cal_jur_dia_id_i, cae_cal_jur_entidad_v, cae_cal_jun_v, cae_cal_jun_fecha_d, cae_cal_jun_dia_id_i, cae_cal_jun_entidad_v, cae_tic_id_i";
		$tabla = "gi_casos_especiales";
		$where = "cae_emd_id_i = ".$_POST['verificarCasoEspecial'];
		$respuesta = ModeloTesoreria::mdlMostrarUnitario($campos,$tabla,$where);
		echo json_encode($respuesta);
	}

	if(isset($_POST['traerCasoEspecial'])){
		$campos = "cae_id_i, cae_emd_id_i, cae_con_rea_v, cae_xon_fecha_d, cae_condia_id_i, cae_con_entidad_id_i, cae_cal_v, cae_cal_fecha_d, cae_cal_dia_id_i, cae_cal_entidad_i, cae_cal_jur_v, cae_cal_jur_fecha_d, cae_cal_jur_dia_id_i, cae_cal_jur_entidad_v, cae_cal_jun_v, cae_cal_jun_fecha_d, cae_cal_jun_dia_id_i, cae_cal_jun_entidad_v, emd_nombre, emd_cedula, emd_cargo, a.ips_nombre as eps, b.ips_nombre as afp, emd_ciudad, emd_subcentro_costos , emd_centro_costos , emd_sede, emd_salario, c.ips_nombre as arl, emd_fecha_ingreso " ;
        $tabla = "gi_casos_especiales JOIN gi_empleados ON emd_id = cae_emd_id_i JOIN gi_ips a ON emd_eps_id = a.ips_id LEFT JOIN gi_ips b ON emd_afp_id  = b.ips_id LEFT JOIN gi_ips c ON emd_arl_id  = c.ips_id";
        $where = "cae_id_i = ".$_POST['traerCasoEspecial'];
        
		$respuesta = ModeloTesoreria::mdlMostrarUnitario($campos,$tabla,$where);
		echo json_encode($respuesta);
	}

	if(isset($_POST['ComentarioCasos'])){
	 	date_default_timezone_set('America/Bogota');
	 	if(isset($_POST['idComentario']) && $_POST['idComentario'] != '0'){
	 		/*Edicion*/
			$campos = "cecom_comentario_v = '".$_POST['ComentarioCasos']."' ";
			$valore = "cecom_id_i = ".$_POST['idComentario'];
			$respuestaCE = ModeloTesoreria::mdlActualizar("casos_especiales_comentarios", $campos, $valore);
			//print_r($respuestaCE);
			if($respuestaCE != 'ok'){
				echo json_encode(array('code' => 0 , 'message' => "Paren todo que no se guardo nada"));
			}else{
				echo json_encode(array('code' => 1 , 'message' => "Exito"));
			}
	 	}else{
	 		/*Ingreso*/
	 		$campos = "cecom_id_caes_i, cecom_comentario_v, cecom_usuario_i, cecom_fecha_d";
			$valore = $_POST['idCasoEspecial'].", '".$_POST['ComentarioCasos']."', ".$_POST['userID'].", '".date('Y-m-d')."'";
			$respuestaCE = ModeloTesoreria::mdlInsertar("casos_especiales_comentarios", $campos, $valore);
			if($respuestaCE == 'Error'){
				echo json_encode(array('code' => 0 , 'message' => "Paren todo que no se guardo nada"));
			}else{
				echo json_encode(array('code' => 1 , 'message' => "Exito"));
			}
	 	}
		
		
	}

	if(isset($_POST['getComentarios'])){
		$campos = "cecom_id_i, cecom_comentario_v, DATE_FORMAT(cecom_fecha_d,'%d/%m/%Y') as fechaComentari, CONCAT(user_nombre, ' ',user_apellidos) as usuario" ;
        $tabla = "casos_especiales_comentarios JOIN gi_usuario ON user_id = cecom_usuario_i";
        $where = "cecom_id_caes_i = ".$_POST['getComentarios'];
        
		$respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tabla,$where, null, 'ORDER BY cecom_fecha_d DESC');
		echo "<thead>";
			echo "<tr>";
				echo "<th style='width:10px;'>#</th>";
				echo "<th style='text-align:center;width:60%;'>Comentario</th>";
				echo "<th style='text-align:center;width:15%;'>Usuario</th>";
				echo "<th style='text-align:center;width:10%;'>Fecha</th>";
				echo "<th style='text-align:center;width:15%;'></th>";
			echo "</tr>";	
		echo "</thead>";
		echo "<tbody>";
		foreach ($respuesta as $key => $value) {
			echo "<tr>";
				echo "<td style='text-align:center;'>".($key+1)."</td>";
				echo "<td style='text-align:justify;width:60%;'>".$value['cecom_comentario_v']."</td>";
				echo "<td style='text-align:center;width:15%;'>".$value['usuario']."</td>";
				echo "<td style='text-align:center;width:10%;'>".$value['fechaComentari']."</td>";
				echo "<td style='text-align:center;width:15%;'>";
					echo '<button type="button" title="Editar comentario" class="btn btn-warning btnEditarComentario" idComentario="'.$value["cecom_id_i"].'" ><i class="fa fa-edit"></i></button>';
					echo '&nbsp;<button type="button" title="Eliminar comentario" class="btn btn-danger btnEliminarComentario" idComentario="'.$value["cecom_id_i"].'" ><i class="fa fa-trash-o"></i></button>';
				echo "</td>";
			echo "</tr>";
		}
		echo "</tbody>";
	}


	if(isset($_POST['delComentarios'])){
		$respuesta = ModeloTesoreria::mdlBorrar('casos_especiales_comentarios', 'cecom_id_i = '.$_POST['delComentarios']);
		if($respuesta == 'ok'){
			echo json_encode(array('code' => 1 , 'message' => "Exito"));
		}else{
			echo json_encode(array('code' => 0 , 'message' => "Paren todo que no se guardo nada"));
		}
	}

	if(isset($_POST['getOneComentari'])){
	 	$campos = "cecom_id_i, cecom_comentario_v";
		$tabla = "casos_especiales_comentarios";
		$where = "cecom_id_i = ".$_POST['getOneComentari'];
		$respuesta = ModeloTesoreria::mdlMostrarUnitario($campos,$tabla,$where);
		echo json_encode($respuesta);
	}
	
	if(isset($_POST["NuevoNombre"])){
		echo ControladorEmpleados::ctrCrearEmpleado();
	}

	if(isset($_POST["EditarNombre"])){
		echo ControladorEmpleados::ctrEditarEmpleados();
	}

	if(isset($_POST["id_Empleado"])){
		echo ControladorEmpleados::ctrBorrarEmpleado();
	}

	
	if(isset($_GET['getEmpleadosSelect'])){
		$AjaxEmpleados = new AjaxEmpleados();
		$AjaxEmpleados->dataSelectEmpleado = $_POST['query'];
		$AjaxEmpleados->getDataSelectEmpleado();
	}
	