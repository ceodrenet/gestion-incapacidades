<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/incapacidades.controlador.php';
	require_once '../controladores/empleados.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/incapacidades.modelo.php';
	require_once '../modelos/empleados.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';
	require_once '../extenciones/Excel.php';


	if(isset($_FILES['NuevoIncapacidad']['tmp_name']) && !empty($_FILES['NuevoIncapacidad']['tmp_name']) ){
		$name   = $_FILES['NuevoIncapacidad']['name'];
		$tname  = $_FILES['NuevoIncapacidad']['tmp_name'];
		ini_set('memory_limit','1028M');


		$obj_excel = PHPExcel_IOFactory::load($tname);
		$sheetData = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
        $arr_datos = array();
        $highestColumm = $obj_excel->setActiveSheetIndex(0)->getHighestColumn(); // e.g. "EL"
		$highestRow = $obj_excel->setActiveSheetIndex(0)->getHighestRow();
		
		$aciertos = 0;
		$fallos   = 0;
		$total = 0;
		$existentes = 0;
		$datoCsv = array();
		$noexiste = 0;
		$incapacidadesExistentes = 0;
		foreach ($sheetData as $index => $value) {
    		if ( $index > 1 ){
    			if($value['A'] == 2){
    				$valido = 0;
        			$existe = false;
        			$separador = '';
        			$total++;

            		if((!is_null($value['B']) && !empty($value['B'])) && 
                		(!is_null($value['D']) && !empty($value['D'])) && 
                		(!is_null($value['E']) && !empty($value['E']))
            		){
            			/* Iniciamos la consulta Sql */
            			$datos = array();

            			/* Identificacion */
            			if(!is_null($value['B']) && !empty($value['B']) ){
            				$valido = 1;
            				$datos['inc_cc_afiliado'] = $value['B'];	

            				$item = 'emd_cedula';
            				$valuess = $value['B'];
            				$tabla = 'gi_empleados';
            				$res = ModeloEmpleados::mdlMostrarEmpleados($tabla, $item, $valuess);
            				
            				if($res['emd_cedula'] == $value['B']){
            					/*Empleado Existente*/
            					$existe = true;
            					$datos['inc_emd_id'] = $res['emd_id'];
            					$datos['inc_empresa'] = $res['emd_emp_id'];
            					$datos['inc_ips_afiliado'] = $res['emd_eps_id'];
            				}else{
            					/* No existe */
            					$existe = false;
            					$datx  = array(   
                                            'filas_fallo_fila' => $index,
                                            'fila_fallo_mensaje' => 'Esta cedula => '.$value['B'].' no esta registrada en la base de datos',
                                            'fila_fallo_session_id' => $_SESSION['idSession']
                                        );

                                $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datx);

            					$noexiste++;
            				}
            			}else{
            				$datos['inc_cc_afiliado'] = NULL;
            			}

            			/* Nombres */
            			if(!is_null($value['D']) && !empty($value['D']) ){
            				$valido = 1;
            				$datos['inc_fecha_inicio'] = $value['D'];	
            			}else{
            				$datos['inc_fecha_inicio'] = NULL;
            			}

            			/* Fecha Nacimiento */
            			if(!is_null($value['E']) && !empty($value['E']) ){
            				$datos['inc_fecha_final'] = $value['E'];
            				$valido = 1;
            			}else{
            				$datos['inc_fecha_final'] = NULL;
            			}

            			/* Fecha ingreso */
            			if(!is_null($value['F']) && !empty($value['F']) ){
            				$datos['inc_origen'] = $value['F'];
            				$valido = 1;
            			}else{
            				$datos['inc_origen'] = NULL;
            			}

            			/* Diagnostico */
            			if(!is_null($value['G']) && !empty($value['G']) ){
            				$valido = 1;
            				$datos['inc_diagnostico'] = $value['G'];	
            			}else{
            				$datos['inc_diagnostico'] = NULL;
            			}

            			/* Fecha Nacimiento */
            			if(!is_null($value['H']) && !empty($value['H']) ){
            				if($value['H'] == 'Original'){
            					$datos['inc_tipo_generacion'] = 1;
            				}else{
            					$datos['inc_tipo_generacion'] = 2;
            				}
            				$valido = 1;
            			}else{
            				$datos['inc_tipo_generacion'] = NULL;
            			}

            			/* Clasificacion */
            			if(!is_null($value['I']) && !empty($value['I']) ){
            				if($value['I'] == 'Inicial'){
            					$datos['inc_clasificacion'] = 1;
            				}else if($value['I'] == 'Prorroga'){
            					$datos['inc_clasificacion'] = 2;
            				}else if($value['I'] == 'Prorroga + 90 día'){
            					$datos['inc_clasificacion'] = 3;
            				}else if($value['I'] == 'Prorroga + 180 días'){
            					$datos['inc_clasificacion'] = 4;
            				}else if($value['I'] == 'Prorroga + 540 días'){
            					$datos['inc_clasificacion'] = 5;
            				}else if($value['I'] == 'Empresa'){
                                $datos['inc_clasificacion'] = 6;
                            }

            				$valido = 1;
            			}else{
            				$datos['inc_clasificacion'] = NULL;
            			}

                        /* Valor pagado empresa  */
                        if(!is_null($value['J']) && !empty($value['J']) ){
                            $datos['inc_valor_pagado_empresa'] = $value['J'];
                            $valido = 1;
                        }else{
                            $datos['inc_valor_pagado_empresa'] = NULL;
                        }

                        /* Valor pagado Administradora  */
                        if(!is_null($value['K']) && !empty($value['K']) ){
                            $datos['inc_valor_pagado_eps'] = $value['K'];
                            $valido = 1;
                        }else{
                            $datos['inc_valor_pagado_eps'] = NULL;
                        }

                        /* Valor Pagado Ajuste  */
                        if(!is_null($value['L']) && !empty($value['L']) ){
                            $datos['inc_valor_pagado_ajuste'] = $value['L'];
                            $valido = 1;
                        }else{
                            $datos['inc_valor_pagado_ajuste'] = NULL;
                        }

            			/* Observacion */
            			if(!is_null($value['O']) && !empty($value['0']) ){
            				$datos['inc_observacion'] = $value['O'];
            				$valido = 1;
            			}else{
            				$datos['inc_observacion'] = NULL;
            			}

            			/* Estado tramite */
            			if(!is_null($value['P']) && !empty($value['P']) ){
                            if($value['I'] == 'Empresa'){
                                $datos['inc_estado_tramite'] = 'EMPRESA';
                            }else{
                                $datos['inc_estado_tramite'] = $value['P'];
                            }
            				$valido = 1;
            			}else{
            				$datos['inc_estado_tramite'] = NULL;
            			}

            			/* valor pagado */
            			if(!is_null($value['Q']) && !empty($value['Q']) ){
            				$datos['inc_valor'] = $value['Q'];
            				$valido = 1;
            			}else{
            				$datos['inc_valor'] = NULL;
            			}

            			
            			/* Valor Pagado Nomina */
            			if(!is_null($value['R']) && !empty($value['R']) ){
            				$datos['inc_fecha_pago'] = $value['R'];
            				$valido = 1;
            			}else{
            				$datos['inc_fecha_pago'] = NULL;
            			}

            			/* Fecha Pago Nomina */
            			if(!is_null($value['N']) && !empty($value['N']) ){
            				$datos['inc_fecha_pago_nomina'] = $value['N'];
            				$valido = 1;
            			}else{
            				$datos['inc_fecha_pago_nomina'] = NULL;
            			}

                        /* IVL empleado */
                        if(!is_null($value['M']) && !empty($value['M']) ){
                            $datos['inc_ivl_v'] = $value['M'];
                            $valido = 1;
                        }else{
                            $datos['inc_ivl_v'] = NULL;
                        }


            			$tabla = "gi_incapacidad";	
            			$datos['inc_estado'] = 3;						
						if($valido == 1){
							if($existe){
								$item1 = "inc_cc_afiliado";
								$valor1 = $value['B'];
								$item2 = 'inc_fecha_inicio';
								$valor2 = $value['D'];
								$existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2);
								if(!$existeInc){
									$respuesta = ModeloIncapacidades::mdlIngresarIncapacidad($tabla, $datos);
									if($respuesta == 'ok'){
										$aciertos++;
									}else{
										$fallos++;
                                        $datos  = array(   
                                            'filas_fallo_fila' => $index,
                                            'fila_fallo_mensaje' => 'Esta fila => '.$index.' , no se pudo guardar revisala, debe tener alguna celda mal configurada',
                                            'fila_fallo_session_id' => $_SESSION['idSession']
                                        );

                                        $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);
									}
								}else{
									$incapacidadesExistentes++;
                                    $datos  = array(   
                                            'filas_fallo_fila' => $index,
                                            'fila_fallo_mensaje' => 'Esta fila => '.$index.' , tiene una cedula y una fecha de inicio de incapacidad repetida',
                                            'fila_fallo_session_id' => $_SESSION['idSession']
                                        );

                                    $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);
								}	
							}else{
                                $fallos++;
                            }
						}
            		}
    			}else if($value['A'] == 3){

    				$valido = 0;
        			$existe = false;
        			$separador = '';
        			$total++;

    				if((!is_null($value['B']) && !empty($value['B'])) && 
                		(!is_null($value['D']) && !empty($value['D'])) && 
                		(!is_null($value['E']) && !empty($value['E']))
            		){
            			/* Iniciamos la consulta Sql */
            			$datos = array();

            			/* Identificacion */
            			if(!is_null($value['B']) && !empty($value['B']) ){
            				$valido = 1;
            				$datos['inc_cc_afiliado'] = $value['B'];	

            				$item = 'emd_cedula';
            				$valuess = $value['B'];
            				$tabla = 'gi_empleados';
            				$res = ModeloEmpleados::mdlMostrarEmpleados($tabla, $item, $valuess);
            				
            				if($res['emd_cedula'] == $value['B']){
            					/*Empleado Existente*/
            					$existe = true;
            					$datos['inc_emd_id'] = $res['emd_id'];
            					$datos['inc_empresa'] = $res['emd_emp_id'];
            					$datos['inc_ips_afiliado'] = $res['emd_eps_id'];
            				}else{
            					/* No existe */
            					$existe = false;
                                $datos  = array(   
                                            'filas_fallo_fila' => $index,
                                            'fila_fallo_mensaje' => 'Esta cedula => '.$value['B'].' , no esta registrada en la base de datos',
                                            'fila_fallo_session_id' => $_SESSION['idSession']
                                        );

                                $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);
            					$noexiste++;
            				}
            			}else{
            				$datos['inc_cc_afiliado'] = NULL;
            			}


            			if($existe){

							$item1 = "inc_cc_afiliado";
							$valor1 = $value['B'];
							$item2 = 'inc_fecha_inicio';
							$valor2 = $value['D'];
							$existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2);
							if($existeInc){
								$id_incapacidad = ModeloIncapacidades::mdlVerIdIncapacidades($item1, $valor1, $item2, $valor2);

								if(!is_null($value['E']) && !empty($value['E']) ){
	                				$datos['inc_estado_tramite'] = $value['E'];
	                				$valido = 1;
	                			}else{
	                				$datos['inc_estado_tramite'] = NULL;
	                			}

	                			if(!is_null($value['F']) && !empty($value['F']) ){
            						if($value['E'] == 'SOLICITUD DE PAGO'){
            							$datos['inc_valor_solicitado'] = $value['F'];
            						}else if($value['E'] == 'PAGADA'){
            							$datos['inc_valor'] = $value['F'];
            						}else{
            							$datos['inc_valor'] = NULL;
            							$datos['inc_valor_solicitado'] = NULL;
            						}
	                				$valido = 1;
	                			}else{
	                				$datos['inc_valor'] = NULL;
        							$datos['inc_valor_solicitado'] = NULL;
	                			}

            					if(!is_null($value['G']) && !empty($value['G']) ){
            						if($value['E'] == 'RADICADA'){
            							$datos['inc_fecha_radicacion'] = $value['G'];
            							$datos['inc_fecha_solicitud'] = NULL;
            							$datos['inc_fecha_pago'] = NULL;
            							$datos['inc_fecha_negacion_'] = NULL;
            						}else  if($value['E'] == 'SOLICITUD DE PAGO'){
            							$datos['inc_fecha_solicitud'] = $value['G'];
            							$datos['inc_fecha_radicacion'] = NULL;
            							$datos['inc_fecha_pago'] = NULL;
            							$datos['inc_fecha_negacion_'] = NULL;
            						}else if($value['E'] == 'PAGADA'){
            							$datos['inc_fecha_pago'] = $value['G'];
            							$datos['inc_fecha_solicitud'] = NULL;
            							$datos['inc_fecha_radicacion'] = NULL;
            							$datos['inc_fecha_negacion_'] = NULL;
            						}else if($value['E'] == 'SIN RECONOCIMIENTO'){
            							$datos['inc_fecha_negacion_'] = $value['G'];
            							$datos['inc_fecha_solicitud'] = NULL;
            							$datos['inc_fecha_radicacion'] = NULL;
            							$datos['inc_fecha_pago'] = NULL;
            						}

	                				
	                				$valido = 1;
	                			}else{
	                				$datos['inc_fecha_pago'] = NULL;
	                				$datos['inc_fecha_solicitud'] = NULL;
        							$datos['inc_fecha_radicacion'] = NULL;
        							$datos['inc_fecha_negacion_'] = NULL;
	                			}

	                			if(!is_null($value['H']) && !empty($value['H']) ){
	                				if($value['E'] == 'SIN RECONOCIMIENTO'){
            							$datos['inc_observacion_neg'] = $value['H'];
            							$datos['inc_observacion'] = NULL;
            						}else{
            							$datos['inc_observacion'] = $value['H'];
            							$datos['inc_observacion_neg'] = NULL;
            						}
	                				
	                				$valido = 1;
	                			}else{
	                				$datos['inc_observacion'] = NULL;
        							$datos['inc_observacion_neg'] = NULL;
	                			}

	                			$tabla = "gi_incapacidad";
								$datos["inc_id"] = $id_incapacidad['inc_id']; 
								

								//var_dump($datos);

								//Echo "El otro \n";
								$respuesta = ModeloIncapacidades::mdlAgregarDatosEconomicos($tabla, $datos);
								if($respuesta == 'ok'){
									$aciertos++;
								}else{
                                    $datos  = array(   
                                            'filas_fallo_fila' => $index,
                                            'fila_fallo_mensaje' => 'Esta fila => '.$index.' , no se pudo guardar revisala, debe tener alguna celda mal configurada',
                                            'fila_fallo_session_id' => $_SESSION['idSession']
                                        );

                                    $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);

									$fallos++;
								}
							}else{
								$incapacidadesExistentes++;
                                $datos  = array(   
                                    'filas_fallo_fila' => $index,
                                    'fila_fallo_mensaje' => 'Esta fila => '.$index.' , no se pudo guardar NO existe una incapacidad registrada con esa cedula y esa fecha de inicio',
                                    'fila_fallo_session_id' => $_SESSION['idSession']
                                );

                                $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);
                                $fallos++;
							}
						}else{
                            $fallos++;
                        }
        			}	
    			}
    			
        	}
        }

       /* if($noexiste > 0){
        	header('Content-Type: application/octet-stream');
			header("Content-Transfer-Encoding: Binary"); 
			header("Content-disposition: attachment; filename=\"Fallaron.csv\"");
        	$f = fopen("tmp.csv", "w");
			foreach ($array as $line) {
			    fputcsv($f, $line);
			}
			fclose($f);
        }*/

        $check = 0;

        if(isset($_POST['chekUpdate']) && $_POST['chekUpdate'] != 0){
        	$check = 1;
        }
        echo json_encode(array('total' => $total , 'exito' => $aciertos , 'fallaron' => $fallos, 'totalesAciertosNo' => $incapacidadesExistentes, 'check' => $check));
        
	}
?>