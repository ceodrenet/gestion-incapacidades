<?php
	session_start();
	$_SESSION['start'] = time();
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/clientes.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/clientes.modelo.php';
	require_once '../modelos/usuarios.modelo.php';

	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxClientes
	{
		public $id_cliente;
		public $activarId;
		public $activarCliente;
		public $validaCliente;
		
		public function ajaxEditarCliente(){
			$item = 'emp_id';
			$valor = $this->id_cliente;
			$respuesta = ControladorClientes::ctrMostrarClientes($item, $valor);
			echo json_encode($respuesta);
		}


		public function ajaxGetCliente(){
			$item = 'emp_nit';
			$valor = $this->id_cliente;
			$respuesta = ControladorClientes::ctrMostrarClientesExternos($item, $valor);
			echo json_encode($respuesta);
		}

		

		/* Activar cliente */
		public function ajaxActivarCliente(){
			$tabla = 'gi_empresa';
			$item1 = 'usuarios_estado_i';
			$valor1 = $this->activarCliente;
			$item2 = 'emp_id';
			$valor2 = $this->activarId;
			$respuesta = Modeloclientes::mdlActualizarCliente($tabla, $item1, $valor1, $item2, $valor2);
		}
		 
		public function ajaxConsultarFechaVencimiento($valor){
			$tabla = 'gi_empresa';
			$respuesta = Modeloclientes::mdlAlertavencimientocc($tabla, $valor);
			echo json_encode($respuesta);
		}

		
		public function ajaxConsultarFechaVencimientocuentab($valor){
			$tabla = 'gi_empresa';
			$respuesta = Modeloclientes::mdlAlertavencimientocuentab($tabla, $valor);
			echo json_encode($respuesta);
		}

		/* validar que el usuairo no este repetido */
		public function ajaxValidarCliente(){
			$item = 'emp_nit';
			$valor = $this->validaCliente;
			$respuesta = ControladorClientes::ctrMostrarClientes($item, $valor);
			echo json_encode($respuesta);
		}

		public function ajaxCambiarPassword($idusuario, $correo, $usuario){
			$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
	        $password = "";
	        //Reconstruimos la contraseña segun la longitud que se quiera
	        for($i = 0; $i < 8; $i++) {
	            //obtenemos un caracter aleatorio escogido de la cadena de caracteres
	            $password .= substr($str,rand(0,62),1);
	        }

	        $pass = crypt($password, '$2a$07$usesomesillystringforsalt$');
			$tabla = 'gi_usuario';
			$item1 = "user_password";
			$valor1 = $pass;
			$item2 = "user_id";
			$valor2 = $idusuario;
			$resultado = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
			if($resultado === 'ok'){
				$tabla = 'gi_usuario';
				$item1 = "user_recordatorio_v";
				$valor1 = $password;
				$item2 = "user_id";
				$valor2 = $idusuario;
				$resultado = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
					
				
				$para  = $correo;
				$titulo = 'Información cambio de contraseña';
				$mensaje = '
<html>
	<head>
		<title>Información cambio de contraseña</title>
	</head>
	<body style="text-align:justify;">
  		<p>incapacidades.co, le informa que sus nuevos datos para ingresar al sistema son:</p>
  		<p>Usuario    : '.$usuario.'</p>
  		<p>Contraseña : '.$password.'</p>
  		<p>Puedes ingresar haciendo click en el siguiente enlace <a href="https://app.incapacidades.co">app.incapacidades.co</a></p>
  		<p>Atentamente el equipo de soporte de incapacidades.co</p>
	</body>
</html>';
				$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
				$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$cabeceras .= 'Bcc: Jose Giron <jgiron9001@gmail.com>, Luis Garcia <clientesgroup@gmail.com>' . "\r\n";
				$cabeceras .= 'From: soporte incapacidades.co <soporte@incapacidades.co>' . "\r\n";
				//$cabeceras .= 'Cc: birthdayarchive@example.com' . "\r\n";
				//
				mail($para, $titulo, $mensaje, $cabeceras);
				echo 'ok';
			}else{
				echo 'error';
			}
		}

	}
	
	
	if(isset($_POST['EditarClienteId'])){
		if($_POST['EditarClienteId'] != ''){
			$editar = new AjaxClientes();
			$editar->id_cliente = $_POST['EditarClienteId'];
			$editar->ajaxEditarCliente();
		}	
	}

	if(isset($_POST['GetClienteId'])){
		if($_POST['GetClienteId'] != ''){
			$editar = new AjaxClientes();
			$editar->id_cliente = $_POST['GetClienteId'];
			$editar->ajaxGetCliente();
		}	
	}


	

	if(isset($_POST['ActivarId'])){
		$activarUsuarios = new AjaxClientes();
		$activarUsuarios->activarId = $_POST['ActivarId'];
		$activarUsuarios->activarCliente = $_POST['estado'];
		$activarUsuarios->ajaxActivarCliente();
	}


	if(isset($_POST['validarNit'])){
		$activarUsuarios = new AjaxClientes();
		$activarUsuarios->validaCliente = $_POST['validarNit'];
		$activarUsuarios->ajaxValidarCliente();
	}
	

	if(isset($_POST['enviarCorreoPass'])){
		$activarUsuarios = new AjaxClientes();
		$activarUsuarios->ajaxCambiarPassword($_POST['enviarCorreoPass'], $_POST['email'], $_POST['usuario']);
	}

	
	if(isset($_POST['dclient'])){
		$activarUsuarios = new AjaxClientes();
		$activarUsuarios->ajaxConsultarFechaVencimiento($_POST['dclient']);
	}

	if(isset($_POST['cbcliente'])){
		$activarUsuarios = new AjaxClientes();
		$activarUsuarios->ajaxConsultarFechaVencimientocuentab($_POST['cbcliente']);
	}
	
	if(isset($_POST["NuevoNombre"])){
		echo ControladorClientes::ctrCrearCliente();
	}

	if(isset($_POST["EditarNombre"])){
		echo ControladorClientes::ctrEditarClientes();
	}

	if(isset($_POST["borrarCliente"])){
		echo ControladorClientes::ctrBorrarCliente();
	}

	if (isset($_POST["redirectClient"])) {
		$_SESSION["cliente_id"] = $_POST["redirectClient"];
		echo json_encode( array('code' => 1, 'desc' => "ok"));
	}

	if (isset($_POST["redirectEmpleados"])) {
		$_SESSION["cliente_id"] = $_POST["redirectEmpleados"];
		echo json_encode( array('code' => 1, 'desc' => "ok"));
	}