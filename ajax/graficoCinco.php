<?php 

session_start();
$_SESSION['start'] = time();

require_once '../controladores/mail.controlador.php';
require_once '../controladores/plantilla.controlador.php'; 
require_once '../modelos/dao.modelo.php';

$year = $_SESSION['anho'].'-01-01';
$otherYear = $_SESSION['anho'].'-12-31';
if(isset($_POST['month']) && $_POST['month'] != '0'){
	$year = $_SESSION['anho'].'-'.$_POST['month'].'-01';
	$otherYear = $_SESSION['anho'].'-'.$_POST['month'].'-31';
}

/*Obtenemos las afp porque son por clasificacion +180 */        
$campo = 'SUM(inc_valor) as total, afp_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
$tabla = 'reporte_tesoreria';
$condicion = 'inc_clasificacion_id = 4 ';
if($_SESSION['cliente_id'] != 0){
    $condicion .= ' AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND estado_tramite_id = 5 ";
}else{
    $condicion .= " AND inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND estado_tramite_id = 5";
}

$respuesta = ControladorPlantilla::getDataFromLsql($campo,$tabla,$condicion, 'GROUP BY afp_nombre', '');

$label = '';
$Qdias = '';
$colors = '';
foreach ($respuesta as $key => $value) {
    /* primero traemos las empresas que vamos a mostrar osea todas */
    /* traemos las de 15 dias */

    $label .= "\"".$value['afp_nombre']."\" , ";
    $Qdias .= "{value:".$value['total'].", name:'".$value['afp_nombre']."', formater:'".number_format($value['total'], 0, ',', '.')."'},";
    $colors .= "getRandomColor(),";
}

/*Obtenemos las ARL porque son por accidente de transito*/
$campo = 'SUM(inc_valor) as total, arl_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
$tabla = 'reporte_tesoreria';
$condicion = "inc_clasificacion_id != 4 AND inc_origen_id = 4 ";
if($_SESSION['cliente_id'] != 0){
    $condicion .= ' AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."'  AND estado_tramite_id = 5 ";
}else{
    $condicion .= " AND inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."'  AND estado_tramite_id = 5";
}

$respuesta2 = ControladorPlantilla::getDataFromLsql($campo,$tabla,$condicion, 'GROUP BY arl_nombre', '');

foreach ($respuesta2 as $key => $value) {
    /* primero traemos las empresas que vamos a mostrar osea todas */
    /* traemos las de 15 dias */

    $label .= "\"".$value['arl_nombre']."\" , ";
    $Qdias .= "{value:".$value['total'].", name:'".$value['arl_nombre']."', formater:'".number_format($value['total'], 0, ',', '.')."'},";
    $colors .= "getRandomColor(),";
}

/*Obtenemos las EPS porque son por accidente de transito*/
$campo = 'SUM(inc_valor) as total, ips_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
$tabla = 'reporte_tesoreria';
$condicion = "inc_clasificacion_id != 4 AND inc_origen_id != 4 ";
if($_SESSION['cliente_id'] != 0){
    $condicion .= ' AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND estado_tramite_id = 5";
}else{
    $condicion .= " AND inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."'  AND estado_tramite_id = 5";
}

$respuesta3 = ControladorPlantilla::getDataFromLsql($campo,$tabla,$condicion, 'GROUP BY ips_nombre', '');

foreach ($respuesta3 as $key => $value) {
    /* primero traemos las empresas que vamos a mostrar osea todas */
    /* traemos las de 15 dias */

    $label .= "\"".$value['ips_nombre']."\" , ";
    $Qdias .= "{value:".$value['total'].", name:'".$value['ips_nombre']."', formater:'".number_format($value['total'], 0, ',', '.')."'},";
    $colors .= "getRandomColor(),";
}

?>
<div id="pie-chart" data-colors='["#fd625e", "#2ab57d", "#4ba6ef", "#ffbf53", "#5156be", "#F50909", "#F5F509", "#8AF509", "#09F550", "#09F5B4", "#09F1F5", "#F50969", "#9109F5", "#AADAC9", "#512746", "#27512A", "#B24A55", "#A46817", "#067C48", "#06187C", "#7915D7", "#05CCC9", "#C4F3F8", "#F8C4E5", "#D2F8C4", "#F8F6C4"]' class="e-charts"></div>

<script type="text/javascript">
	$(function(){
        var pieColors = getChartColorsArray("#pie-chart");
        var dom = document.getElementById("pie-chart");
        var myChart = echarts.init(dom);
        var app = {};
        option = null;
        option = {
            tooltip : {
                trigger: 'item',
                formatter: function (params) {
                    return `${params.seriesName}<br />
                            ${params.name}: ${params.data.formater} (${params.percent}%)<br />`;
                }
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: [<?php echo $label;?>],
                textStyle: {color: '#858d98'}
            },
            color: pieColors, //['#fd625e', '#2ab57d', '#4ba6ef', '#ffbf53', '#5156be'],
            series : [
                {
                    name: 'Pagos Totales',
                    type: 'pie',
                    radius : '80%',
                    center: ['72%', '55%'],
                    data:[
                        <?php echo $Qdias;?>
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        ;
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
	})
</script>