<?php
	session_start();
	$_SESSION['start'] = time();
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
    require_once '../controladores/incapacidades.controlador.php';
	require_once '../controladores/empleados.controlador.php';
	require_once '../controladores/clientes.controlador.php';
	require_once '../controladores/medicos.controlador.php';
	require_once '../controladores/entidadesRegistradas.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/incapacidades.modelo.php';
	require_once '../modelos/empleados.modelo.php';
	require_once '../modelos/tesoreria.modelo.php';
	require_once '../modelos/clientes.modelo.php';
	require_once '../modelos/medicos.modelos.php';
	require_once '../modelos/entidadesRegistradas.modelo.php';
	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxIncapacidades
	{
		public $id_incapacidad;
		public $validaCedula = null;
		public $validarMedico = null;
		public $validarEntidad = null;
		public $clienteId;
		public $empresa;
		public $item = null;

		public function ajaxEditarIncapacidad(){
			$item = 'inc_id';
			$valor = $this->id_incapacidad;
			$respuesta = ControladorIncapacidades::ctrMostrarIncapacidades_edicion($item, $valor);
			echo json_encode($respuesta);
		}


		public function ajaxValidarCedula(){
			$item = 'emd_cedula';
			$valor = $this->validaCedula;
			$empresa = $this->empresa;
			$respuesta = ControladorEmpleados::ctrMostrarEmpleados_incapacidad($item, $valor, $empresa);
			echo json_encode($respuesta);
		}

		public function ajaxValidarMedico(){
			$item = 'med_id';
			$valor = $this->validarMedico;
			$respuesta = ControladorMedicos::ctrMostrarMedicos($item,$valor);
			echo json_encode($respuesta);
		}

		public function ajaxValidarEntidad(){
			$item = 'enat_id_i';
			$valor = $this->validarEntidad;
			$respuesta = ControladorEntidadAtiende::ctrMostrarEntidades($item,$valor);
			echo json_encode($respuesta);
		}

		public function ajaxValidarIncapacidad(){
			$item1 = "inc_cc_afiliado";
			$valor1 = $this->validaCedula;
			$item2 = 'inc_fecha_inicio';
			$valor2 = $this->id_incapacidad;
			$respuesta = ControladorIncapacidades::ctrValidarIncapacidades($item1, $valor1, $item2, $valor2);
			if($respuesta){
				echo "1";
			}else{
				echo "0";
			}
		}

		public function ajaxgetIncapacidadesTotal(){
			$item = null;
			$valor = null;
            $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades($item, $valor);
echo '{
  	"data" : [';
  			$i = 0;
		 	foreach ($incapacidades as $key => $value) {
		 		if($i != 0){
            		echo ",";
            	}
				echo '[';
				echo '"'.($key+1).'",';
				echo '"'.($value["inc_empresa"]).'",';
				echo '"'.$value["inc_nombres_afiliado"].'",';

				if($value['inc_clasificacion'] != 4){
                    if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                        echo '"'.($value["inc_arl_afiliado"]).'",'; 
                    }else{
                        echo '"'.($value["inc_ips_afiliado"]).'",'; 
                    }
                }else{
                    echo '"'.($value["inc_afp_afiliado"]).'",';    
                }

				echo '"'.($value["inc_diagnostico"]).'",';
				echo '"'.explode(' ', $value["inc_fecha_inicio"])[0].'",';  
				if($value['estadoInc'] == '1'){
					echo '"'.$value["inc_estado"].'",'; 
				}else{
					echo '"INCOMPLETA",'; 
				} 
				echo '"'.$value["inc_ruta_incapacidad"].'",'; 
				echo '"'.$value["inc_ruta_incapacidad_transcrita"].'",';
				echo '"'.$value["inc_id"].'"'; 
				echo ']';

            	$i++;
		 	}
		echo ']
}';

		}

		public function ajaxgetIncapacidadesTotalx(){
			$item = $this->item;
			$valor = $this->validaCedula;
			/*$incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades($item, $valor);*/
			$campos = 'emp_nombre, emd_cedula, emd_nombre, inc_diagnostico, inc_origen, inc_clasificacion, inc_fecha_inicio, inc_fecha_final, inc_tipo_generacion, inc_profesional_responsable, inc_donde_se_genera, inc_fecha_generada, inc_observacion, inc_estado_tramite, inc_fecha_radicacion, inc_fecha_solicitud, inc_fecha_pago, inc_valor, inc_fecha_pago_nomina, inc_valor_pagado_nomina, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado ,  ip4.ips_nombre as inc_afp_afiliado, inc_fecha_negacion_, inc_observacion_neg';
	        $tabla  = 'gi_incapacidad LEFT JOIN gi_ips as ip2 ON ip2.ips_id = inc_ips_afiliado LEFT JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id';
			$condicion = " emd_cedula = '".$valor."' OR emd_nombre LIKE '%".$valor."%' ";
	        $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, '', 'ORDER BY inc_id ASC');
	        echo json_encode($incapacidades);

		}


		public function ajaxgetIncapacidadesTotalEmpresa(){
			$item = 'inc_empresa';
            $valor = $_SESSION['cliente_id'];
            $incapacidades = ControladorIncapacidades::ctrMostrarIncapacidades_Clientes($item, $valor);
echo '{
  	"data" : [';
  			$i = 0;
		 	foreach ($incapacidades as $key => $value) {
		 		if($i != 0){
            		echo ",";
            	}
				echo '[';
				echo '"'.($key+1).'",';
				echo '"'.($value["emd_cedula"]).'",';
				echo '"'.$value["inc_nombres_afiliado"].'",';

				if($value['inc_clasificacion'] != 4){
                    if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                        echo '"'.($value["inc_arl_afiliado"]).'",'; 
                    }else{
                        echo '"'.($value["inc_ips_afiliado"]).'",'; 
                    }
                }else{
                    echo '"'.($value["inc_afp_afiliado"]).'",';    
                }

				echo '"'.($value["inc_diagnostico"]).'",';
				echo '"'.explode(' ', $value["inc_fecha_inicio"])[0].'",'; 
				echo '"'.explode(' ', $value["inc_fecha_final"])[0].'",';  
				if($value['estadoInc'] == '1'){
					echo '"'.$value["inc_estado"].'",'; 
				}else{
					echo '"INCOMPLETA",'; 
				}
				
				echo '"'.$value["inc_ruta_incapacidad"].'",'; 
				echo '"'.$value["inc_ruta_incapacidad_transcrita"].'",'; 
				echo '"'.$value["inc_id"].'",';
				echo '"'.$value["inc_numero_notificaciones_i"].'",';
				echo '"'.$value['emd_estado'].'"';
				echo ']';

            	$i++;
		 	}
		echo ']
}';
		}

	}
	
	
	if(isset($_POST['EditarIncapacidades'])){
		if($_POST['EditarIncapacidades'] != ''){
			$editar = new AjaxIncapacidades();
			$editar->id_incapacidad = $_POST['EditarIncapacidades'];
			$editar->ajaxEditarIncapacidad();
		}	
	}

	if(isset($_GET['getIncapaciadesEmpresa'])){
		if($_GET['getIncapaciadesEmpresa'] != ''){
			$editar = new AjaxIncapacidades();
			$editar->clienteId = $_GET['getIncapaciadesEmpresa'];
			$editar->ajaxgetIncapacidadesTotalEmpresa();
		}
	}

	if(isset($_GET['getIncapaciades'])){
		if($_GET['getIncapaciades'] != ''){
			$editar = new AjaxIncapacidades();
			$editar->ajaxgetIncapacidadesTotal();
		}
	}
	
	if(isset($_POST['getIncapaciades'])){
		if($_POST['getIncapaciades'] != ''){
			$editar = new AjaxIncapacidades();
			$editar->item = 'inc_cc_afiliado';
			$editar->validaCedula = $_POST['getIncapaciades'];
			$editar->ajaxgetIncapacidadesTotalx();
		}	
	}

	if(isset($_POST['validarCedula'])){
		$activarUsuarios = new AjaxIncapacidades();
		$activarUsuarios->validaCedula = $_POST['validarCedula'];
		$activarUsuarios->empresa = $_POST['empresa'];
		$activarUsuarios->ajaxValidarCedula();
	}

	if (isset($_POST['validarMedico'])) {
		$validar = new AjaxIncapacidades();
		$validar->validarMedico = $_POST['validarMedico'];
		$validar->ajaxValidarMedico();
	}

	if (isset($_POST['validarEntidad'])) {
		$validar = new AjaxIncapacidades();
		$validar->validarEntidad = $_POST['validarEntidad'];
		$validar->ajaxValidarEntidad();
	}

	if(isset($_POST['validarIncapacidad'])){
		$activarUsuarios = new AjaxIncapacidades();
		$activarUsuarios->validaCedula = $_POST['txtCedula'];
		$activarUsuarios->id_incapacidad = $_POST['txtFechaInicio'];
		$activarUsuarios->ajaxValidarIncapacidad();
	}

	if(isset($_POST['EditarIncapacidadesFacturacion'])){
		$tabla = "gi_incapacidad";
		if(isset($_POST['checkEnviados'])){
			$che = explode('checkEnviado=', $_POST['checkEnviados']);
			foreach($che as $key){
				if($key != ""){
					$key = str_replace('&', '', $key);
					$datos = array(
						"inc_numero_factura" 				=> $_POST["NuevoNumeroFactura"],
						"inc_fecha_pago_factura" 			=> $_POST["NuevoFechaPago"],
						"incapacidadesID"					=> $key,
						"inc_fecha_consiliacion_d"			=> $_POST['NuevoFechaConsiliacion'],
						"inc_fecha_fecturacion_d"			=> $_POST['NuevoFechaFacturacion'],
						"inc_condicion_id_i"				=> $_POST['condicionNuevo']
					);
					$respuesta = ModeloIncapacidades::mdlAgregarDatosFacturacionById($tabla, $datos);	
				} 
			}
			
			if($respuesta == 'ok'){
				echo json_encode(array('code' => 1, 'message' => 'exitoso'));
			}else{
				echo json_encode(array('code' => 0, 'message' => 'nada'));
			}
		}else{
			$datos = array(
				"inc_numero_factura" 				=> $_POST["NuevoNumeroFactura"],
				"inc_fecha_emision_factura" 		=> $_POST["NuevoFechaRadicacion"],
				"inc_fecha_pago_factura" 			=> $_POST["NuevoFechaPago"],
				"fecha1"							=> $_POST['fecha1'],
				"fecha2"							=> $_POST['fecha2'],
				"empresa"							=> $_POST['EmpresaFacturada']
			); 
		
			if($_POST['EmpresaFacturada'] != 0 || $_POST['EmpresaFacturada'] != '' ){
				$respuesta = ModeloIncapacidades::mdlAgregarDatosFacturacion($tabla, $datos);
			}else{
				$respuesta = ModeloIncapacidades::mdlAgregarDatosFacturacionT($tabla, $datos);
			}
			
			if($respuesta == 'ok'){
				echo json_encode(array('code' => 1, 'message' => 'exitoso'));
			}else{
				echo json_encode(array('code' => 0, 'message' => 'nada'));
			}
		}
	}

	if(isset($_POST['getDatosPagoInca']) && $_POST['getDatosPagoInca'] != ''){
		/*Buscamos los datos de esa incapacidad en los pagos*/
		$strCampos = "pag_id_i, pag_inc_id_i, pag_valor_pagado_v, pag_fecha_pago_d ";
		$strTablas = "gi_pagos_incapacidades";
		$strWheres = "pag_inc_id_i = ".$_POST['getDatosPagoInca'];
		$respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos,$strTablas,$strWheres, NULL, "ORDER BY pag_fecha_pago_d DESC");
		echo json_encode($respuesta);
	}

	if(isset($_POST['eliminaDatosPago']) && $_POST['eliminaDatosPago'] != ''){
		$strCampos = "pag_id_i, pag_inc_id_i, pag_valor_pagado_v, pag_fecha_pago_d ";
		$strTablas = "gi_pagos_incapacidades";
		$strWheres = "pag_id_i = ".$_POST['eliminaDatosPago'];
		$dato = ModeloTesoreria::mdlMostrarUnitario($strCampos,$strTablas,$strWheres);

		if($dato != null && $dato != false){
			/*Si habia informacion*/
			/*Restamos el pago de la incapacidad*/
			$newRespuesta = ModeloTesoreria::mdlActualizar('gi_incapacidad', 'inc_valor = inc_valor-'.$dato['pag_valor_pagado_v'], 'inc_id = '.$dato['pag_inc_id_i']);
			if($newRespuesta == 'ok'){
				$otraRespuesta = ModeloTesoreria::mdlBorrar($strTablas,$strWheres);
				if($otraRespuesta == 'ok'){
					echo json_encode(array('code' => 1));
				}else{
					echo json_encode(array('code' => 0, 'msg' => 'no pude borrar esa fila'));
				}
			}else{
				echo json_encode(array('code' => 0, 'msg' => 'no pude restar el pago de la inc'));
			}
		}else{
			echo json_encode(array('code' => 0, 'msg' => 'no existe ese pago'));
		}

	}

	if(isset($_POST['getIncapaciadesAnteriores']) && $_POST['getIncapaciadesAnteriores'] != ''){
		/*Vamos a traer las incapacidades que son anteriores*/
		$strCampos = "inc_fecha_inicio, inc_fecha_final, inc_diagnostico, ori_descripcion_v as inc_origen, cla_descripcion";
		$strTablas = "gi_incapacidad JOIN gi_origen ON inc_origen = ori_id_i JOIN gi_clasificacion ON cla_id = inc_clasificacion";
		$strWheres = "inc_cc_afiliado = '".$_POST['getIncapaciadesAnteriores']."'";
		$respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($strCampos, $strTablas, $strWheres, null, "ORDER BY inc_fecha_inicio DESC", null);
		$html = '';
		foreach ($respuesta as $key => $value) {
			$di = '';
			if($value["inc_diagnostico"] != ''){
				$strCampo = "dia_descripcion";
	        	$strTabla = "gi_diagnostico";
	        	$strWhere = "dia_codigo = '".$value["inc_diagnostico"]."'";
	        	$respuest = ModeloTesoreria::mdlMostrarUnitario($strCampo, $strTabla, $strWhere);
	        	$di = $respuest['dia_descripcion'];
			}

			$html .= '<tr>';
			$html .= '<td>'.($key+1).'</td>';
			$html .= '<td>'.date("d/m/Y", strtotime($value['inc_fecha_inicio'])).'</td>';
			$html .= '<td>'.date("d/m/Y", strtotime($value['inc_fecha_final'])).'</td>';
			$html .= '<td>'.ControladorIncapacidades::dias_transcurridos($value['inc_fecha_inicio'], $value['inc_fecha_final']).'</td>';
			
			$html .= '<td>'.$value['inc_origen'].'</td>';
			$html .= '<td>'.$value['cla_descripcion'].'</td>';
			$html .= '<td>'.$value['inc_diagnostico'].'-'.$di.'</td>';
			$html .= '</tr>';
		}

		echo $html;
	}	
	

	if(isset($_POST['liquidarIncapacidad']) && $_POST['liquidarIncapacidad'] != ''){
		$respuestaEmpleado = ControladorEmpleados::ctrMostrarEmpleados('emd_id', $_POST['liquidarIncapacidad']);
		$jsonArray = array();						
		if($respuestaEmpleado != null){
			/*Liquidacion Automatica*/
			$jsonArray['code'] = 1;
			$anho = explode('-', $_POST['NuevoFechaInicio'])[0];
			$strCampo = "min_valor_d";
			$strTabla = "gi_minimo_vigente";
			$strWhere = "min_vigencia_d = ".$anho;
			$respiest = ModeloTesoreria::mdlMostrarUnitario($strCampo , $strTabla, $strWhere);
			//print_r($respiest);
			
			$diasIncapacidad = explode(' ',$_POST['diasIncapacidad'])[0];

			if($_POST['clasifIncapacidad'] == 1){
				/*Inicial, Valor_empresa*/
				if($_POST['origenIncapacidad'] == '1' || $_POST['origenIncapacidad'] == '5'){

					/*Obtenemos el valor del dia del empleado*/
					$valorPromedio = $respuestaEmpleado['emd_salario_promedio'] * 66.67 / 100;
					$valorDelDia = $valorPromedio / 30;
					$valorDelDiaMin = $respiest['min_valor_d'] / 30;
					//$valorDelDiaMin = str_replace('.', '', $valorDelDiaMin);
					if($valorDelDia < $valorDelDiaMin){
						$valorDelDia = $valorDelDiaMin;
					}

					if($diasIncapacidad < 3){
						/*Toda para la empresa*/
						$valorIncapacidadEmpresa = $diasIncapacidad * $valorDelDia;
						$jsonArray['tipo'] = 1;
						$jsonArray['valE'] = round($valorIncapacidadEmpresa);
					}else{
						/*2 dias para la empresa y el resto a la administradora*/
						$diasAdministradora = ($diasIncapacidad - 2);
						$valorIncapacidadEmpresa = 2 * $valorDelDia;
						$valorIncapacidadAdministradora = $diasAdministradora * $valorDelDia;
					
						$jsonArray['tipo'] = 2;
						$jsonArray['valE'] = round($valorIncapacidadEmpresa);
						$jsonArray['valI'] = round($valorIncapacidadAdministradora);
					}
				}else if($_POST['origenIncapacidad'] == '4'){

					$valorPromedio = $respuestaEmpleado['emd_salario_promedio'];
					$valorDelDia = $valorPromedio / 30;

					if($diasIncapacidad < 2){
						/*Toda para la empresa*/
						$valorIncapacidadEmpresa = $diasIncapacidad * $valorDelDia;
						$jsonArray['tipo'] = 1;
						$jsonArray['valE'] = round($valorIncapacidadEmpresa);
					}else{
						/*1 dias para la empresa y el resto a la administradora*/
						$diasAdministradora = ($diasIncapacidad - 1);
						$valorIncapacidadEmpresa = 1 * $valorDelDia;
						$valorIncapacidadAdministradora = $diasAdministradora * $valorDelDia;
					
						$jsonArray['tipo'] = 2;
						$jsonArray['valE'] = round($valorIncapacidadEmpresa);
						$jsonArray['valI'] = round($valorIncapacidadAdministradora);
					}
				}else if($_POST['origenIncapacidad'] == '2' || $_POST['origenIncapacidad'] == '3'){


					$valorPromedio = $respuestaEmpleado['emd_salario_promedio'];
					$valorDelDia = $valorPromedio / 30;
					
					/*Todo a la administradora*/
					$valorIncapacidadAdministradora = $diasIncapacidad * $valorDelDia;
				
					$jsonArray['tipo'] = 2;
					$jsonArray['valE'] = 0;
					$jsonArray['valI'] = round($valorIncapacidadAdministradora);
					
				}
				
			}else{
				/*Prorrogas, Valor_administradora*/
				if($_POST['clasifIncapacidad'] != 2){

					$valorPromedio = $respuestaEmpleado['emd_salario_promedio'] * 50 / 100;
					$valorDelDia = $valorPromedio / 30;
					$valorDelDiaMin = $respiest['min_valor_d'] / 30;
					//$valorDelDiaMin = str_replace('.', '', $valorDelDiaMin);
					if($valorDelDia < $valorDelDiaMin){
						$valorDelDia = $valorDelDiaMin;
					}


					$valorIncapacidadAdministradora = $diasIncapacidad * $valorDelDia;
					$jsonArray['tipo'] = 2;
					$jsonArray['valE'] = 0;
					$jsonArray['valI'] = round($valorIncapacidadAdministradora);	

				}else{

					$valorPromedio = $respuestaEmpleado['emd_salario_promedio'] * 66.67 / 100;
					$valorDelDia = $valorPromedio / 30;
					$valorDelDiaMin = $respiest['min_valor_d'] / 30;
					//$valorDelDiaMin = str_replace('.', '', $valorDelDiaMin);
					if($valorDelDia < $valorDelDiaMin){
						$valorDelDia = $valorDelDiaMin;
					}


					$valorIncapacidadAdministradora = $diasIncapacidad * $valorDelDia;
					$jsonArray['tipo'] = 2;
					$jsonArray['valE'] = 0;
					$jsonArray['valI'] = round($valorIncapacidadAdministradora);	
				}
				
			}	
		}else{
			$jsonArray['code'] = 0;
			$jsonArray['message'] = 'No existe el empleado';
		}

		echo json_encode($jsonArray);
	}


	if(isset($_POST["NuevoCedulaIncapacidad"])){
		$crearIncapacidad = new ControladorIncapacidades();
        $crearIncapacidad->ctrCrearIncapacidad(); 
	}
         
	if(isset($_POST["EditarCedulaIncapacidad"])){
		$crearIncapacidad = new ControladorIncapacidades();
        $crearIncapacidad->ctrEditarIncapacidad(); 
	}

	if(isset($_POST["borrarIncapacidad"])){
		$crearIncapacidad = new ControladorIncapacidades();
        $crearIncapacidad->ctrBorrarIncapacidad();
	}   

	if(isset($_POST['getComentariosIncapacidad'])){
		$campos = "incm_id_i , incm_comentario_t, DATE_FORMAT(incm_fecha_comentario,'%d/%m/%Y') as fechaComentari, user_nombre, user_apellidos, incm_usuer_id_i" ;
        $tabla = "gi_incapacidades_comentarios JOIN gi_usuario ON user_id = incm_usuer_id_i";
        $where = "inm_inc_id_i = ".$_POST['getComentariosIncapacidad'];
        
		$respuesta = ModeloIncapacidades::mdlMostrarGroupAndOrder($campos,$tabla,$where, null, 'ORDER BY incm_fecha_comentario DESC');
		echo "<thead>";
			echo "<tr>";
				echo "<th style='width:10px;'>#</th>";
				echo "<th style='text-align:center;width:60%;'>Comentario</th>";
				echo "<th style='text-align:center;width:15%;'>Usuario</th>";
				echo "<th style='text-align:center;width:10%;'>Fecha</th>";
			if($_POST['opcion'] == 'editar'){
				echo "<th style='text-align:center;width:15%;'></th>";
			}
			echo "</tr>";	
		echo "</thead>";
		echo "<tbody>";
		foreach ($respuesta as $key => $value) {
			echo "<tr>";
				echo "<td style='text-align:center;width:10px;'>".($key+1)."</td>";
				echo "<td style='text-align:justify;width:60%;'>".$value['incm_comentario_t']."</td>";
				echo "<td style='text-align:center;width:15%;'>".$value['user_nombre'].' '.$value['user_apellidos']."</td>";
				echo "<td style='text-align:center;width:10%;'>".$value['fechaComentari']."</td>";
				if($_POST['opcion'] == 'editar'){
				echo "<td style='text-align:center;width:15%;'>";
				//if($_SESSION['id_Usuario'] == $value['incm_usuer_id_i']){
					echo '<button type="button" title="Editar comentario" class="btn btn-success btn-sm btnEditarComentario" idObservacion="'.$value["incm_id_i"].'" ><i class="fa fa-edit"></i></button>';
					echo '&nbsp;';
					echo '<button type="button" title="Eliminar comentario" class="btn btn-danger btn-sm btnEliminarComentario" idObservacion="'.$value["incm_id_i"].'" ><i class="fa fa-trash"></i></button>';
				//}
				echo "</td>";
				}
			echo "</tr>";
		}
		echo "</tbody>";
	}

	if(isset($_POST['delComentarios']) && $_POST['delComentarios'] != ''){
		$respuesta = ModeloIncapacidades::mdlBorrar('gi_incapacidades_comentarios', 'incm_id_i='.$_POST['delComentarios']);
		if($respuesta == 'ok'){
			echo json_encode(array('code' => 1, 'respuesta' => 'borrado'));
		}else{
			echo json_encode(array('code' => 0, 'respuesta' => 'no borrado'));			
		}
	}  

	if(isset($_POST['getOneComentari']) && $_POST['getOneComentari'] != ''){
		$respuesta = ModeloIncapacidades::mdlMostrarUnitario('*','gi_incapacidades_comentarios', 'incm_id_i='.$_POST['getOneComentari']);
		echo json_encode($respuesta);
	}   

	if(isset($_POST['VerDetalleIncapacidades'])){
		$item = 'inc_id';
		$valor = $_POST['VerDetalleIncapacidades'];
		$respuesta = ControladorIncapacidades::ctrMostrarIncapacidades_edicion($item, $valor);
		

		echo "<table class='table table-border table-hover' style='width:100%;'>";
			echo "<tr>";
				echo "<th>IDENTIFICACIÓN</th>";
				echo "<td>".mb_strtoupper($respuesta['emd_cedula'])."</td>";
				echo "<th>NOMBRES</th>";
				echo "<td>".mb_strtoupper($respuesta['emd_nombre'])."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<th>CARGO DEL EMPLEADO</th>";
				echo "<td>".mb_strtoupper($respuesta['emd_cargo'])."</td>";
				echo "<th>EMPRESA</th>";
				echo "<td>".mb_strtoupper($respuesta['emp_nombre'])."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<th>SALARIO IBC</th>";
				echo "<td>".$respuesta['emd_salario']."</td>";
				echo "<th>ADMINISTRADORA</th>";
				echo "<td>".mb_strtoupper($respuesta['eps'])."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<th>FECHA AFLIACIÓN EPS</th>";
				$newDate = null;
				if($respuesta['emd_fecha_afiliacion_eps'] != null){
					$timestamp = strtotime($respuesta['emd_fecha_afiliacion_eps']); 
					$newDate = date("d/m/Y", $timestamp );
				}
				echo "<td>".$newDate."</td>";
				echo "<th>ARL</th>";
				echo "<td>".mb_strtoupper($respuesta['arl'])."</td>";
			echo "</tr>";
		echo "</table>";
		echo "<table class='table table-border table-hover' style='width:100%;'>";
			echo "<tr>";
				echo "<th>ESTADO EMPLEADO</th>";
				echo "<td>".mb_strtoupper(ModeloIncapacidades::getDatos('gi_estado_empleados', 'est_emp_id_i', $respuesta['emd_estado'])['est_emp_desc_v'])."</td>";
				echo "<th>FECHA INGRESO</th>";
				$newDate = null;
				if($respuesta['emd_fecha_ingreso'] != null){
					$timestamp = strtotime($respuesta['emd_fecha_ingreso']); 
					$newDate = date("d/m/Y", $timestamp );
				}
				echo "<td>".$newDate."</td>";
				echo "<th>FECHA RETIRO</th>";
				$newDate = null;
				if($respuesta['emd_fecha_retiro'] != null){
					$timestamp = strtotime($respuesta['emd_fecha_retiro']); 
					$newDate = date("d/m/Y", $timestamp );
				}
				echo "<td>".$newDate."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<th>NOVEDAD</th>";
				echo "<td colspan='5'></td>";
			echo "</tr>";
			
			echo "<tr>";
				echo "<th>USUARIO</th>";
				echo "<td>".mb_strtoupper(ModeloIncapacidades::getDatos('gi_usuario', 'user_id', $respuesta['inc_user_id_i'])['user_nombre'])."</td>";
				echo "<th>FECHA REGISTRO</th>";
				$newDate = null;
				if($respuesta['inc_fecha_generada'] != null){
					$timestamp = strtotime($respuesta['inc_fecha_generada']); 
					$newDate = date("d/m/Y", $timestamp );
				}
				echo "<td>".$newDate."</td>";
				echo "<th>FECHA RECEPCIÓN</th>";
				$newDate = null;
				if($respuesta['inc_fecha_recepcion_d'] != null){
					$timestamp = strtotime($respuesta['inc_fecha_recepcion_d']); 
					$newDate = date("d/m/Y", $timestamp );
				}
				echo "<td>".$newDate."</td>";
			echo "</tr>";

			echo "<tr>";
				echo "<th>ESTADO</th>";
				echo "<td>".mb_strtoupper(ModeloIncapacidades::getDatos('gi_estado_incapacidad', 'est_inc_id_i', $respuesta['inc_estado'])['est_inc_desc_i'])."</td>";
				echo "<th>ORIGEN</th>";
				echo "<td>".mb_strtoupper(ModeloIncapacidades::getDatos('gi_origen', 'ori_id_i', $respuesta['inc_origen'])['ori_descripcion_v'])."</td>";
				echo "<th>CLASIFICACIÓN</th>";
				echo "<td>".mb_strtoupper(ModeloIncapacidades::getDatos('gi_clasificacion', 'cla_id', $respuesta['inc_clasificacion'])['cla_descripcion'])."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<th>ATENCIÓN</th>";
				echo "<td>".mb_strtoupper(ModeloIncapacidades::getDatos('gi_atencion', 'atn_id', $respuesta['inc_atn_id'])['atn_descripcion'])."</td>";
				echo "<th>DOCUMENTOS</th>";
				echo "<td>".mb_strtoupper(ModeloIncapacidades::getDatos('gi_documentos', 'doc_id_i', $respuesta['inc_tipo_generacion'])['doc_descripcion_v'])."</td>";
				echo "<td></td>";
				echo "<td></td>";
			echo "</tr>";
		echo "</table>";
		echo "<table class='table table-border table-hover' style='width:100%;'>";
			echo "<tr>";
				echo "<th>FECHA INICIO</th>";
				$newDate = null;
				if($respuesta['inc_fecha_inicio'] != null){
					$timestamp = strtotime($respuesta['inc_fecha_inicio']); 
					$newDate = date("d/m/Y", $timestamp );
				}
				echo "<td>".$newDate."</td>";
				echo "<th>FECHA FINAL</th>";
				$newDate = null;
				if($respuesta['inc_fecha_final'] != null){
					$timestamp = strtotime($respuesta['inc_fecha_final']); 
					$newDate = date("d/m/Y", $timestamp );
				}
				echo "<td>".$newDate."</td>";
				echo "<th>DÍAS</th>";
				echo "<td>".ControladorIncapacidades::dias_transcurridos($respuesta['inc_fecha_inicio'], $respuesta['inc_fecha_final'])."</td>";
				echo "<th>FECHA PAGO NOMINA</th>";
				$newDate = null;
				if($respuesta['inc_fecha_pago_nomina'] != null){
					$timestamp = strtotime($respuesta['inc_fecha_pago_nomina']); 
					$newDate = date("d/m/Y", $timestamp );
				}
				echo "<td>".$newDate."</td>";
			echo "</tr>";
			echo "<tr>";
				echo "<th>VALOR EMPRESA</th>";
				if($respuesta['inc_valor_pagado_empresa'] == null){
					echo "<td>0</td>";
				}else{
					echo "<td>".number_format($respuesta['inc_valor_pagado_empresa'], 0, ',', '.')."</td>";
				}
				
				echo "<th>VALOR ADMINISTRADORA</th>";
				if($respuesta['inc_valor_pagado_eps'] != null){
					echo "<td>".number_format($respuesta['inc_valor_pagado_eps'], 0, ',', '.')."</td>";
				}else{
					echo "<td>0</td>";
				}
				
				echo "<th>VALOR AJUSTE</th>";
				echo "<td>".$respuesta['inc_valor_pagado_ajuste']."</td>";
				echo "<td></td>";
				echo "<td></td>";
			echo "</tr>";
		echo "</table>";
		echo "<table class='table table-border table-hover' style='width:100%;'>";
			echo "<tr>";
				echo "<th>DIAGNÓSTICO</th>";
				echo "<td>".mb_strtoupper(ModeloIncapacidades::getDatos('gi_diagnostico', 'dia_codigo', $respuesta['inc_diagnostico'])['dia_descripcion'])."</td>";
			echo "</tr>";
		echo "</table>";
		echo "<table class='table table-border table-hover' style='width:100%;'>";
			echo "<tr>";
				echo "<th>PROFESIONAL RESPONSABLE</th>";
				echo "<td>".mb_strtoupper($respuesta['med_nombre'])."</td>";
				echo "<th>REGISTRO MEDICO</th>";
				echo "<td>".mb_strtoupper($respuesta['med_num_registro'])."</td>";
				echo "<th>INSTITUCIÓN QUE GENERA</th>";
				echo "<td>".mb_strtoupper($respuesta['enat_nombre_v'])."</td>";
			echo "</tr>";
		echo "</table>";
		
		$estadoTramite = ModeloIncapacidades::mdlMostrarUnitario("est_tra_desc_i", "gi_estado_tramite", "est_tra_id_i = ".intval($respuesta['inc_estado_tramite']));
		$subEstadoTramite = ModeloIncapacidades::mdlMostrarUnitario("set_descripcion_v", "gi_sub_estado_tramite", "set_id_i = ".intval($respuesta["inc_sub_estado_i"]));
		$fechasEstTra = ["2" => "inc_fecha_generada", "3" => "inc_fecha_radicacion", "4" => "inc_fecha_solicitud", "5" => "inc_fecha_pago", "6" => "inc_fecha_negacion_", "7" => "inc_fecha_negacion_"];
		$currentFechaEst = ($respuesta["inc_estado_tramite"] != "1") ? $respuesta[$fechasEstTra[$respuesta["inc_estado_tramite"]]] : null;
		$formated = !is_null($currentFechaEst) ? date("d/m/Y", strtotime($currentFechaEst)) : "";

		echo "<table class='table table-border table-hover' style='width:90%;'>";
			echo "<tr>";
				echo "<th>ESTADO ACTUAL</th>";
				echo "<td>".$estadoTramite["est_tra_desc_i"]."</td>";
				echo "<th>SUB ESTADO</th>";
				if (!empty($respuesta["inc_sub_estado_i"]) && $respuesta["inc_sub_estado_i"] != "0") {
					echo "<td>".$subEstadoTramite["set_descripcion_v"]."</td>";
				} else {
					echo "<td></td>";
				}
				echo "<th>FECHA DE ESTADO</th>";
				echo "<td>".$formated."</td>";
			echo "</tr>";
		echo "</table>";

		$campos = "incm_id_i , incm_comentario_t, DATE_FORMAT(incm_fecha_comentario,'%d/%m/%Y') as fechaComentari, user_nombre, user_apellidos, incm_usuer_id_i" ;
		$tabla = "gi_incapacidades_comentarios JOIN gi_usuario ON user_id = incm_usuer_id_i";
		$where = "inm_inc_id_i = ".$_POST['VerDetalleIncapacidades'];
		$respuestaComenta = ModeloIncapacidades::mdlMostrarGroupAndOrder($campos,$tabla,$where, null, 'ORDER BY incm_fecha_comentario DESC');
		if (!empty($respuestaComenta)) {
			echo "<table class='table table-border table-hover' style='width:100%;'>";
				echo "<tr>";
					echo "<th colspan='2' style='text-align:center;'>HISTORICO COMENTARIOS</th>";
				echo "</tr>";
				foreach($respuestaComenta as $key => $valorx){
					echo "<tr>";
						echo "<th style='text-align:center;'>".$valorx['fechaComentari']."</th>";
						echo "<td style='text-align:justify;'>".$valorx['incm_comentario_t']."</td>";
					echo "</tr>";
				
				}
			echo "</table>";
		}

		echo "<table class='table table-border table-hover''>";
			echo "<tr>";
				echo "<td style='width:60%;'>";
					echo "<table class='table table-border table-hover' style='width:100%;'>";
						echo "<tr>";
							echo "<th colspan='4' style='text-align:center;'>HISTORICO ESTADO TRAMITE</th>";
						echo "</tr>";
						echo "<tr>";
							echo "<th>ESTADO TRAMITE</th>";
							echo "<th>FECHA ESTADO</th>";
							echo "<th>NUMERO RADICADO</th>";
							echo "<th>TIEMPO TRANSCURRIDO</th>";
						echo "</tr>";
						echo "<tr>";
							echo "<th>RECIBIDA</th>";
							$newDate = null;
							if($respuesta['inc_fecha_recepcion_d'] != null){
								$timestamp = strtotime($respuesta['inc_fecha_recepcion_d']); 
								$newDate = date("d/m/Y", $timestamp );
							}
							echo "<td>".$newDate."</td>";
							echo "<td></td>";
							echo "<td></td>";
						echo "</tr>";
						echo "<tr>";
							echo "<th>REGISTRADA</th>";
							$newDate = null;
							if($respuesta['inc_fecha_generada'] != null){
								$timestamp = strtotime($respuesta['inc_fecha_generada']); 
								$newDate = date("d/m/Y", $timestamp );
							}
							echo "<td>".$newDate."</td>";
							echo "<td></td>";
							if($respuesta['inc_fecha_generada'] != null){
								
								if($respuesta['inc_fecha_recepcion_d'] != null){
									echo "<td>+ ".ControladorIncapacidades::dias_transcurridos($respuesta['inc_fecha_recepcion_d'], $respuesta['inc_fecha_generada'])."</td>";
								}
							}else{
								echo "<td></td>";
							}
						echo "</tr>";
						echo "<tr>";
							echo "<th>POR RADICAR</th>";
							$newDate = null;
							if($respuesta['inc_fecha_generada'] != null){
								$timestamp = strtotime($respuesta['inc_fecha_generada']); 
								$newDate = date("d/m/Y", $timestamp );
							}
							echo "<td>".$newDate."</td>";
							echo "<td></td>";
							echo "<td></td>";
						echo "</tr>";
						echo "<tr>";
							echo "<th>RADICADA</th>";
							$newDate = null;
							if($respuesta['inc_fecha_radicacion'] != null){
								$timestamp = strtotime($respuesta['inc_fecha_radicacion']); 
								$newDate = date("d/m/Y", $timestamp );
							}
							echo "<td>".$newDate."</td>";
							echo "<td>".$respuesta['inc_numero_radicado_v']."</td>";
							if($respuesta['inc_fecha_radicacion'] != null){
								echo "<td>+ ".ControladorIncapacidades::dias_transcurridos($respuesta['inc_fecha_generada'], $respuesta['inc_fecha_radicacion'])."</td>";
							}else{
								echo "<td></td>";
							}
							
						echo "</tr>";
						echo "<tr>";
							echo "<th>SOLICITUD DE PAGO</th>";
							$newDate = null;
							if($respuesta['inc_fecha_solicitud'] != null){
								$timestamp = strtotime($respuesta['inc_fecha_solicitud']); 
								$newDate = date("d/m/Y", $timestamp );
							}
							echo "<td>".$newDate."</td>";
							echo "<td>".$respuesta['inc_numero_radicado_v']."</td>";
							if($respuesta['inc_fecha_solicitud'] != null){
								if($respuesta['inc_fecha_recepcion_d'] != null){
									echo "<td>+ ".ControladorIncapacidades::dias_transcurridos($respuesta['inc_fecha_recepcion_d'], $respuesta['inc_fecha_solicitud'])."</td>";
								}else{
									echo "<td></td>";
								}
							}else{
								echo "<td></td>";
							}
						echo "</tr>";
						echo "<tr>";
							echo "<th>PAGADA</th>";
							$newDate = null;
							if($respuesta['inc_fecha_pago'] != null){
								$timestamp = strtotime($respuesta['inc_fecha_pago']); 
								$newDate = date("d/m/Y", $timestamp );
							}
							echo "<td>".$newDate."</td>";
							echo "<td>".$respuesta['inc_numero_radicado_v']."</td>";
							if($respuesta['inc_fecha_pago'] != null){
								
								if($respuesta['inc_fecha_recepcion_d'] != null){
									echo "<td>+ ".ControladorIncapacidades::dias_transcurridos($respuesta['inc_fecha_recepcion_d'], $respuesta['inc_fecha_pago'])."</td>";
								}
							}else{
								echo "<td></td>";
							}
						echo "</tr>";
						echo "<tr>";
							echo "<th>NEGADA</th>";
							$newDate = null;
							if($respuesta['inc_fecha_negacion_'] != null){
								$timestamp = strtotime($respuesta['inc_fecha_negacion_']); 
								$newDate = date("d/m/Y", $timestamp );
							}
							echo "<td>".$newDate."</td>";
							echo "<td>".$respuesta['inc_numero_radicado_v']."</td>";
							if($respuesta['inc_fecha_negacion_'] != null){
								if($respuesta['inc_fecha_recepcion_d'] != null){
									echo "<td>+ ".ControladorIncapacidades::dias_transcurridos($respuesta['inc_fecha_recepcion_d'], $respuesta['inc_fecha_negacion_'])."</td>";
								}
							}else{
								echo "<td></td>";
							}
						echo "</tr>";
						echo "<tr>";
							echo "<th>VALOR INCAPACIDAD</th>";
							if($respuesta['inc_valor'] != null){
								echo "<td colspan='3'>".number_format($respuesta['inc_valor'], 0, ',', '.')."</td>";
							}else if($respuesta['inc_valor_pagado_eps'] != null){
								echo "<td colspan='3'>".number_format($respuesta['inc_valor_pagado_eps'], 0, ',', '.')."</td>";
							}else{
								echo "<td colspan='3'>0</td>";
							}
						echo "</tr>";
					echo "</table>";		
				echo "</td>";
				echo "<td style='width:40%;'>";
							
				echo "</td>";
			echo "</tr>";
			
		echo "</table>";

	}      
                
?>