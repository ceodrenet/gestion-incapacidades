<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/ips.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/ips.modelo.php';

	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxIps
	{
		public $tipo_administradora;
		
		public function ajaxTraeAdministradora(){
			$item = 'ips_tipo';
			$valor = $this->tipo_administradora;
			$respuesta = ControladorIps::ctrMostrarIps($item, $valor);
			echo json_encode($respuesta);
		}

		public function ajaxEditarEntidades($id_ips){
			$item = 'ips_id';
			$valor = $id_ips;
			$respuesta =  ModeloIps::mdlMostrarIps('gi_ips', $item, $valor);
			echo json_encode($respuesta);
		}




	}
	
	
	if(isset($_POST['TipoAdministradora'])){
		if($_POST['TipoAdministradora'] != ''){
			$editar = new AjaxIps();
			$editar->tipo_administradora = $_POST['TipoAdministradora'];
			$editar->ajaxTraeAdministradora();
		}	
	}

	if(isset($_POST['EditarEntidadId'])){
		if($_POST['EditarEntidadId'] != ''){
			$editar = new AjaxIps();
			$editar->ajaxEditarEntidades($_POST['EditarEntidadId']);
		}	
	}
	
	if(isset($_POST['idEntidades'])){
		if($_POST['idEntidades'] != ''){
			$editar = new AjaxIps();
			$editar->ajaxEditarEntidades($_POST['idEntidades']);
		}	
	}
	if(isset($_POST['NuevoNombreEps'])){
		if($_POST['NuevoNombreEps'] != ''){
			echo ControladorIps::ctrCrearIps();
		}	
	}

	if(isset($_POST['EditarNombreEps'])){
		if($_POST['EditarNombreEps'] != ''){
			echo ControladorIps::ctrEditarIps();
		}	
	}

	if(isset($_POST['ips_id'])){
		if($_POST['ips_id'] != ''){
			echo ControladorIps::ctrBorrarIps();
		}	
	}

	if(isset($_GET['dameEpsPorFavor'])){
            $entidades = ModeloIps::mdlMostrarIps('gi_ips', null, null);
echo '{
  	"data" : [';
  			$i = 0;
		 	foreach ($entidades as $key => $value) {
		 		if($i != 0){
            		echo ",";
            	}
				echo '[';
				echo '"'.$value["ips_nombre"].'",';
				echo '"'.$value["ips_nit"].'",';
				echo '"'.$value["ips_correo_cartera"].'",'; 
				echo '"'.$value["ips_correo_radicacion"].'",'; 
				echo '"'.$value["ips_correo_pqr_v"].'",'; 
				echo '"'.$value["ips_correo_not_v"].'",'; 
				echo '"'.$value["ips_id"].'"';
				echo ']';
            	$i++;
		 	}
		echo ']
}';
	}