<?php
    session_start();
    $_SESSION['start'] = time();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
    require_once '../controladores/incapacidades.controlador.php';
    require_once '../controladores/tesoreria.controlador.php';

    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';

    $months = array( "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    );

    $month = $months[intval($_POST["month"])];


    $option = (isset($_POST["options"])) ? $_POST["options"] : "no option";


    $fieldDays = "EXTRACT(DAY FROM inc_fecha_recepcion_d) as dia,COUNT(*) as cant ";
    $fieldValEnt = "gi_ips.ips_nombre AS entidad, SUM(IF(inc_estado_tramite=5, inc_valor, 0)) + SUM(IF(inc_estado_tramite=1, inc_valor_pagado_empresa,0)) +
        SUM(IF(inc_estado_tramite=3 OR inc_estado_tramite=4 OR inc_estado_tramite=6, inc_valor_pagado_eps, 0)) AS valor";

    $fieldSaldos = "SUM(IF(inc_estado_tramite=5, inc_valor, 0)) + SUM(IF(inc_estado_tramite=1, inc_valor_pagado_empresa,0)) +
        SUM(IF(inc_estado_tramite=3 OR inc_estado_tramite=4 OR inc_estado_tramite=6, inc_valor_pagado_eps, 0)) AS valor";

    $fieldAllMesesVal = "MONTH(inc_fecha_recepcion_d) as mes, SUM(IF(inc_estado_tramite = 1,inc_valor_pagado_empresa,0)) as emp, 
        SUM(IF(inc_estado_tramite=5 OR inc_estado_tramite=8, inc_valor,0)) as pag,
        SUM(IF(inc_estado_tramite=2 OR inc_estado_tramite=3 OR inc_estado_tramite=4 OR inc_estado_tramite=7, inc_valor_pagado_eps,0)) as pen,
        SUM(IF(inc_estado_tramite=6, inc_valor_pagado_eps,0)) as sin";
    
    $fieldAllMesesCant = "MONTH(inc_fecha_recepcion_d) as mes, 
        COUNT(CASE WHEN inc_estado_tramite=1 THEN 1 END) as emp, 
        COUNT(CASE WHEN inc_estado_tramite=5 THEN 1 END) as pag, 
        COUNT(CASE WHEN inc_estado_tramite=2 OR inc_estado_tramite=3 OR inc_estado_tramite=4 OR inc_estado_tramite=7 THEN 1 END) as pen,
        COUNT(CASE WHEN inc_estado_tramite=6 THEN 1 END) as sin";

    $incaJoinEstTra = "gi_incapacidad gi JOIN gi_estado_tramite et ON gi.inc_estado_tramite = et.est_tra_id_i";
    $incaJoinIpsEps = "gi_incapacidad JOIN gi_ips ON inc_ips_afiliado = gi_ips.ips_id";
    $incaJoinIpsArl = "gi_incapacidad JOIN gi_ips ON inc_arl_afiliado = gi_ips.ips_id";
    $incaJoinOrigen = "gi_incapacidad JOIN gi_origen o ON inc_origen = o.ori_id_i";
    $incaJoinAtencion = "gi_incapacidad LEFT JOIN gi_atencion a ON inc_atn_id = a.atn_id";

    $where = "";
    $year = "";

    if ($_POST["year"] != 0) {
        $where.="YEAR(inc_fecha_recepcion_d) = '".$_POST["year"]."' ";
        $year = $_POST["year"];
    }

    if ($_POST["month"] != "all") {
        $where.=($_POST["year"] != 0)?"AND MONTH(inc_fecha_recepcion_d) = '".($_POST["month"]+1)."' ":"MONTH(inc_fecha_recepcion_d) = '".($_POST["month"]+1)."' ";
    }

    if ($_SESSION['cliente_id'] != 0) {
        $where .=($_POST["month"] != "all" || $_POST["year"] != 0)?"AND inc_empresa = " . $_SESSION['cliente_id']:"inc_empresa = " . $_SESSION['cliente_id'];
    }

    //consulta para el grafico de calendario
    $days = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldDays, "gi_incapacidad", $where, "GROUP BY dia", "ORDER BY dia ASC");

    /*A partir de aquí se hacen las consultas para traer los valores o en algunos casos los valores y cantidades
        para luego calcular los porcentajes y no hacer la consulta nuevamente en el if que valida las opciones,
        aunque ya no me convence esta forma de hacerlo lo llevo bastante adelantado y no quiero complicarme en refactorizar,
        aunque luego se tendrá que hacer :(*/

    //consulta del total en la cabecera del grafico de estado tramite
    $cantIncRec = ModeloTesoreria::mdlMostrarUnitario("COUNT(*) as cant", "gi_incapacidad", $where);

    //consultas para valores de saldo en el grafico de estado tramite
    $valIncEstPag = ModeloTesoreria::mdlMostrarUnitario("SUM(inc_valor) as valor,  et.est_tra_desc_i as estado", $incaJoinEstTra, $where . " AND inc_estado_tramite = 5");
    $valIncEstEmp = ModeloTesoreria::mdlMostrarUnitario("SUM(inc_valor_pagado_empresa) as valor,  et.est_tra_desc_i as estado", $incaJoinEstTra, $where . " AND inc_estado_tramite = 1");
    $valIncEstOther = ModeloTesoreria::mdlMostrarGroupAndOrder("SUM(inc_valor_pagado_eps) as valor,  et.est_tra_desc_i as estado", $incaJoinEstTra, $where . " AND inc_estado_tramite != 1 AND inc_estado_tramite != 5", "GROUP BY estado", "ORDER BY valor DESC");

    //consultas para los valores del grafico de entidades por eps
    $valEntEps = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldValEnt, $incaJoinIpsEps, $where . " AND inc_origen != 4", "GROUP BY entidad", "ORDER BY valor DESC");
    $valEntArl = ModeloTesoreria::mdlMostrarUnitario($fieldValEnt, $incaJoinIpsArl, $where . " AND inc_origen = 4");

    //consultas para los valores y cantidades del grafico de rango de edades
    //$where0y10 = $where11y20 = $where21y30 = $where31y60 = $where61y90 = $whereSup90 = $where;

    $datos0y10 = ModeloTesoreria::mdlMostrarUnitario($fieldSaldos . ",COUNT(*) AS cant", "gi_incapacidad", $where . " AND DATEDIFF(inc_fecha_recepcion_d, inc_fecha_inicio) <= 10");
    $datos11y20 = ModeloTesoreria::mdlMostrarUnitario($fieldSaldos . ",COUNT(*) AS cant", "gi_incapacidad", $where . " AND DATEDIFF(inc_fecha_recepcion_d, inc_fecha_inicio) BETWEEN 11 AND 20");
    $datos21y30 = ModeloTesoreria::mdlMostrarUnitario($fieldSaldos . ",COUNT(*) AS cant", "gi_incapacidad", $where . " AND DATEDIFF(inc_fecha_recepcion_d, inc_fecha_inicio) BETWEEN 21 AND 30");
    $datos31y60 = ModeloTesoreria::mdlMostrarUnitario($fieldSaldos . ",COUNT(*) AS cant", "gi_incapacidad", $where . " AND DATEDIFF(inc_fecha_recepcion_d, inc_fecha_inicio) BETWEEN 31 AND 60");
    $datos61y90 = ModeloTesoreria::mdlMostrarUnitario($fieldSaldos . ",COUNT(*) AS cant", "gi_incapacidad", $where . " AND DATEDIFF(inc_fecha_recepcion_d, inc_fecha_inicio) BETWEEN 61 AND 90");
    $datosSup90 = ModeloTesoreria::mdlMostrarUnitario($fieldSaldos . ",COUNT(*) AS cant", "gi_incapacidad", $where . " AND DATEDIFF(inc_fecha_recepcion_d, inc_fecha_inicio) > 90");

    //consulta para los valores del grafico de origen
    $valOrigen = ModeloTesoreria::mdlMostrarGroupAndOrder("o.ori_descripcion_v as origen, " . $fieldSaldos, $incaJoinOrigen, $where, "GROUP BY origen", "ORDER BY valor DESC");

    //consulta para los valores del grafico de atención
    $valAtencion = ModeloTesoreria::mdlMostrarGroupAndOrder("IF(a.atn_descripcion IS NOT NULL,a.atn_descripcion,'(En blanco)') as atencion, " . $fieldSaldos, $incaJoinAtencion, $where, "GROUP BY atencion", "ORDER BY valor DESC");

    //consulta para los valores del grafico de todos los meses
    $whereAllMeses = "MONTH(inc_fecha_recepcion_d) BETWEEN 1 AND 12 AND ".$where;
    $valAllMeses = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldAllMesesVal, "gi_incapacidad", $whereAllMeses, "GROUP BY mes", "ORDER BY mes");
    
    
    //arrays para pasar a los chart graphics
    $dataIncEstTra = [];
    $dataIncEntidades = [];
    $dataIncEdades = [];
    $dataIncOrigen = [];
    $dataIncAtencion = [];
    $dataIncAllMeses = [];

    //Saldo total
    $saldoTotalIncRec = $valIncEstPag["valor"] + $valIncEstEmp["valor"];
    foreach ($valIncEstOther as $value) {
        $saldoTotalIncRec += $value["valor"];
    }

    if ($option == "#") { //validamos la opcion
        //cantidades para el grafico de estado tramite
        $dataIncEstTra = ModeloTesoreria::mdlMostrarGroupAndOrder("COUNT(*) as valor,  et.est_tra_desc_i as estado", $incaJoinEstTra, $where, "GROUP BY estado", "ORDER BY valor DESC");

        //cantidades para el grafico de entidades
        $dataIncEntidades = ModeloTesoreria::mdlMostrarGroupAndOrder("gi_ips.ips_nombre AS entidad, COUNT(*) as valor", $incaJoinIpsEps, $where . " AND inc_origen != 4", "GROUP BY entidad", "ORDER BY valor DESC");
        $dataArl = ModeloTesoreria::mdlMostrarUnitario("gi_ips.ips_nombre AS entidad, COUNT(*) as valor", $incaJoinIpsArl, $where . " AND inc_origen = 4");
        $dataIncEntidades[] = $dataArl;

        //cantidades para el grafico de edades
        $dataEdad1["label"] = "1) 0 a 10 Días";
        $dataEdad1["valor"] = $datos0y10["cant"];
        $dataEdad2["label"] = "2) 11 a 20 Días";
        $dataEdad2["valor"] = $datos11y20["cant"];
        $dataEdad3["label"] = "3) 21 a 30 Días";
        $dataEdad3["valor"] = $datos21y30["cant"];
        $dataEdad4["label"] = "4) 31 a 60 Días";
        $dataEdad4["valor"] = $datos31y60["cant"];
        $dataEdad5["label"] = "5) 61 a 90 Días";
        $dataEdad5["valor"] = $datos61y90["cant"];
        $dataEdad6["label"] = "6) Sup a 90 Días";
        $dataEdad6["valor"] = $datosSup90["cant"];
        $dataIncEdades[] = $dataEdad1;
        $dataIncEdades[] = $dataEdad2;
        $dataIncEdades[] = $dataEdad3;
        $dataIncEdades[] = $dataEdad4;
        $dataIncEdades[] = $dataEdad5;
        $dataIncEdades[] = $dataEdad6;

        //cantidades para el grafico de origen
        $dataIncOrigen = ModeloTesoreria::mdlMostrarGroupAndOrder("o.ori_descripcion_v as origen, COUNT(*) AS valor", $incaJoinOrigen, $where, "GROUP BY origen", "ORDER BY valor DESC");

        //cantidades para el grafico de atención
        $dataIncAtencion = ModeloTesoreria::mdlMostrarGroupAndOrder("IF(a.atn_descripcion IS NOT NULL,a.atn_descripcion,'(En blanco)') as atencion, COUNT(*) AS valor", $incaJoinAtencion, $where, "GROUP BY atencion", "ORDER BY valor DESC");


        //cantidades para el grafico de todos los meses
        $dataIncAllMeses = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldAllMesesCant, "gi_incapacidad", $whereAllMeses, "GROUP BY mes", "ORDER BY mes");

    } elseif ($option == "$") {

        //valores para el grafico de estado tramite
        $dataIncEstTra[] = $valIncEstPag;
        $dataIncEstTra[] = $valIncEstEmp;
        foreach ($valIncEstOther as $value) {
            $data["valor"] = $value["valor"];
            $data["estado"] = $value["estado"];
            $dataIncEstTra[] = $data;
        }

        //valores para el grafico de entidades
        $dataIncEntidades[] = $valEntArl;
        foreach ($valEntEps as $value) {
            $data["valor"] = $value["valor"];
            $data["entidad"] = $value["entidad"];
            $dataIncEntidades[] = $data;
        }

        //valores para el grafico de edades
        $dataEdad1["label"] = "1) 0 a 10 Días";
        $dataEdad1["valor"] = $datos0y10["valor"];
        $dataEdad2["label"] = "2) 11 a 20 Días";
        $dataEdad2["valor"] = $datos11y20["valor"];
        $dataEdad3["label"] = "3) 21 a 30 Días";
        $dataEdad3["valor"] = $datos21y30["valor"];
        $dataEdad4["label"] = "4) 31 a 60 Días";
        $dataEdad4["valor"] = $datos31y60["valor"];
        $dataEdad5["label"] = "5) 61 a 90 Días";
        $dataEdad5["valor"] = $datos61y90["valor"];
        $dataEdad6["label"] = "6) Sup a 90 Días";
        $dataEdad6["valor"] = $datosSup90["valor"];
        $dataIncEdades[] = $dataEdad1;
        $dataIncEdades[] = $dataEdad2;
        $dataIncEdades[] = $dataEdad3;
        $dataIncEdades[] = $dataEdad4;
        $dataIncEdades[] = $dataEdad5;
        $dataIncEdades[] = $dataEdad6;

        //valores para el grafico de origen
        $dataIncOrigen = $valOrigen;

        //valores para el grafico de atención
        $dataIncAtencion = $valAtencion;

        //valores para el grafico de todos los meses
        $dataIncAllMeses = $valAllMeses;
    } else {
        //porcentajes para el grafico de estado de tramite
        $porIncEstPag["valor"] = number_format(($valIncEstPag["valor"] * 100) / $saldoTotalIncRec, 2);
        $porIncEstPag["estado"] = $valIncEstPag["estado"];

        $porIncEstEmp["valor"] = number_format(($valIncEstEmp["valor"] * 100) / $saldoTotalIncRec, 2);
        $porIncEstEmp["estado"] = $valIncEstEmp["estado"];

        $dataIncEstTra[] = $porIncEstPag;
        $dataIncEstTra[] = $porIncEstEmp;
        foreach ($valIncEstOther as $value) {
            $data["valor"] = number_format(($value["valor"] * 100) / $saldoTotalIncRec, 2);
            $data["estado"] = $value["estado"];
            $dataIncEstTra[] = $data;
        }

        //porcentajes para el grafico de entidades
        $porIncEnt["valor"] = number_format(($valEntArl["valor"] * 100) / $saldoTotalIncRec, 2);
        $porIncEnt["entidad"] = $valEntArl["valor"];
        $dataIncEntidades[] = $porIncEnt;

        foreach ($valEntEps as $value) {
            $data["valor"] = number_format(($value["valor"] * 100) / $saldoTotalIncRec, 2);
            $data["entidad"] = $value["entidad"];
            $dataIncEntidades[] = $data;
        }

        //porcentajes para el grafico de edades
        $dataEdad1["label"] = "1) 0 a 10 Días";
        $dataEdad1["valor"] = number_format(($datos0y10["valor"] * 100) / $saldoTotalIncRec, 2);
        $dataEdad2["label"] = "2) 11 a 20 Días";
        $dataEdad2["valor"] = number_format(($datos11y20["valor"] * 100) / $saldoTotalIncRec, 2);
        $dataEdad3["label"] = "3) 21 a 30 Días";
        $dataEdad3["valor"] = number_format(($datos21y30["valor"] * 100) / $saldoTotalIncRec, 2);
        $dataEdad4["label"] = "4) 31 a 60 Días";
        $dataEdad4["valor"] = number_format(($datos31y60["valor"] * 100) / $saldoTotalIncRec, 2);
        $dataEdad5["label"] = "5) 61 a 90 Días";
        $dataEdad5["valor"] = number_format(($datos61y90["valor"] * 100) / $saldoTotalIncRec, 2);
        $dataEdad6["label"] = "6) Sup a 90 Días";
        $dataEdad6["valor"] = number_format(($datosSup90["valor"] * 100) / $saldoTotalIncRec, 2);
        $dataIncEdades[] = $dataEdad1;
        $dataIncEdades[] = $dataEdad2;
        $dataIncEdades[] = $dataEdad3;
        $dataIncEdades[] = $dataEdad4;
        $dataIncEdades[] = $dataEdad5;
        $dataIncEdades[] = $dataEdad6;

        //porcentajes para el grafico de origen
        foreach ($valOrigen as $value) {
            $data["origen"] = $value["origen"];
            $data["valor"] = number_format(($value["valor"] * 100) / $saldoTotalIncRec, 2);
            $dataIncOrigen[] = $data;
        }

        //porcentajes para el grafico de atención
        foreach ($valAtencion as $value) {
            $data["atencion"] = $value["atencion"];
            $data["valor"] = number_format(($value["valor"] * 100) / $saldoTotalIncRec, 2);
            $dataIncAtencion[] = $data;
        }

        //porcentajes para el grafico de todos los meses
        foreach ($valAllMeses as $value) {
            $data["mes"] = $value["mes"];
            $data["emp"] = number_format(($value["emp"] * 100) / $saldoTotalIncRec, 2);
            $data["pag"] = number_format(($value["pag"] * 100) / $saldoTotalIncRec, 2);
            $data["pen"] = number_format(($value["pen"] * 100) / $saldoTotalIncRec, 2);
            $data["sin"] = number_format(($value["sin"] * 100) / $saldoTotalIncRec, 2);
            $dataIncAllMeses[] = $data;
        }
    }

    //ordenamos en forma descendente
    array_multisort(array_column($dataIncEstTra, 'valor'), SORT_DESC, $dataIncEstTra);
    array_multisort(array_column($dataIncEntidades, 'valor'), SORT_DESC, $dataIncEntidades);
    array_multisort(array_column($dataIncOrigen, 'valor'), SORT_DESC, $dataIncOrigen);
    array_multisort(array_column($dataIncAtencion, 'valor'), SORT_DESC, $dataIncAtencion);
?>

<style>
    .wraper {
        background-color: #FFF1F1;
    }

    .wraper-graphics {
        width: 100%;
    }

    .wraper-graphics header {
        display: flex;
        align-items: center;
        padding: 20px 30px 5px;
        justify-content: center;
        flex-direction: column;
    }

    header .current-date,
    .title-header {
        font-size: 1.45rem;
        font-weight: 500;
    }

    .calendar .weeks {
        display: grid;
        grid-template-columns: repeat(7, 1fr);
    }

    .calendar .weeks .week {
        text-align: center;
    }

    .calendar .days {
        display: grid;
        grid-template-columns: repeat(7, 1fr);
        gap: 1px;
    }

    .calendar .days .day {
        background-color: #EFDDDD;
        height: 35px;
    }

    .calendar .days .day .num-day {
        font-size: 9.5px;
        padding: 0;
        margin: 0;
    }

    .cant-day {
        font-size: 13px;
        text-align: center;
        /* position: absolute; */
        top: 50%;
        transform: translate(0, -35%);
    }

    .calendar .days .inactive {
        background-color: #EFDDDD;
    }

    .scroll{
        scrollbar-width: thin;
    }
    .scroll::-webkit-scrollbar{
        width: 10px;
    }
    .scroll::-webkit-scrollbar-track {
        background-clip: content-box;
    }
    .scroll::-webkit-scrollbar-thumb {
        border-radius: 2px;
        background-color: #D3D3D3;
    }
</style>

<div class="card border border-danger wraper">
    <div class="row">
        <?php if($_POST["month"] != "all"){?>
        <div class="col-xs-4 col-ms-4 col-md-6 col-lg-4">
            <div class="wraper-graphics">
                <header>
                    <strong class="text-danger">Calendario de recepción de Incapacidades</strong>
                    <p class="current-date"><?php echo $month . " " . $year ?></p>
                </header>
                <div class="calendar">
                    <div class="weeks">
                    </div>
                    <div class="days">
                    </div>
                    <input type="hidden" id="valMonth" value="<?php echo $_POST["month"]; ?>">
                    <input type="hidden" id="valYear" value="<?php echo $_POST["year"]; ?>">
                </div>
            </div>
        </div>
        <?php } else {?>
        <div class="col-xs-4 col-ms-4 col-md-6 col-lg-4">
            <div class="wraper-graphics">
                <header>
                    <strong class="text-danger">Valor(<?php echo $option ?>) por año, mes y estado de incapacidad</strong>
                    <p class="title-header text-white">.</p>
                </header>
                <div>
                    <canvas  id="chartAllMeses"></canvas>
                </div>
            </div>
        </div>
        <?php } ?>

        <div class="col-xs-4 col-ms-4 col-md-6 col-lg-4">
            <div class="wraper-graphics">
                <header>
                    <strong class="text-danger">No. Total Incapacidades Recibidas</strong>
                    <p class="title-header"><?php echo $cantIncRec["cant"] ?></p>
                </header>
                <div>
                    <canvas id="chartEstTramite"></canvas>
                </div>
            </div>
        </div>

        <div class="col-xs-4 col-ms-4 col-md-6 col-lg-4">
            <div class="wraper-graphics">
                <header>
                    <strong class="text-danger">($) Valor Total Incapacidades Recibidas</strong>
                    <p class="title-header"><?php echo number_format($saldoTotalIncRec, 0, ',', '.') ?></p>
                </header>
                <div class="scroll" style="overflow-y: scroll; height: 200px;">
                    <canvas height="290" id="chartEntidades"></canvas>
                </div>
            </div>
        </div>

        <div class="col-xs-4 col-ms-4 col-md-6 col-lg-4">
            <div class="wraper-graphics">
                <header>
                    <strong class="text-danger">Edades por Rango</strong>
                    <!-- <p class="title-header">0 a 90+ Días</p> -->
                </header>
                <div>
                    <canvas id="chartEdades"></canvas>
                </div>
            </div>
        </div>

        <div class="col-xs-4 col-ms-4 col-md-6 col-lg-4">
            <div class="wraper-graphics">
                <header>
                    <strong class="text-danger">Origen</strong>
                </header>
                <div>
                    <canvas id="chartOrigen"></canvas>
                </div>
            </div>
        </div>

        <div class="col-xs-4 col-ms-4 col-md-6 col-lg-4">
            <div class="wraper-graphics">
                <header>
                    <strong class="text-danger">Vía de Atención</strong>
                </header>
                <div>
                    <canvas id="chartViaAtencion"></canvas>
                </div>
            </div>
        </div>
    </div>
    <?php //echo print_r($dataIncAllMeses) ?>
</div>

<script src="vistas/assets/assets/libs/chart.js/Chart.bundle.min.js"></script>
<script type="text/javascript">
    $(function() {
        var validMonth = '<?php echo $_POST["month"] ?>'
        if (validMonth != 'all') {
            calendarGraphic()
        } else {
            barAllMeses()
        }
        barEstTramite()
        barEntidades()
        barEdades()
        barOrigen()
        barAtencion()
    });

    function calendarGraphic() {
        var days = document.querySelector(".days");
        var weeks = document.querySelector(".weeks");

        var nameDays = ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"];
        var valMonth = $("#valMonth").val();
        var valYear = $("#valYear").val();

        var firstDay = new Date(valYear, valMonth, 1).getDay();
        var lastDate = new Date(valYear, parseInt(valMonth) + 1, 0).getDate(),
            lastDatePrev = new Date(valYear, valMonth, 0).getDate();

        var dayAndCant = <?php echo json_encode($days) ?>;

        for (let i = 0; i <= nameDays.length - 1; i++) {
            weeks.insertAdjacentHTML('beforeend', `<div class="week">${nameDays[i]}</div>`)
        }

        for (let j = firstDay; j > 0; j--) {
            days.insertAdjacentHTML('beforeend', `<div class="inactive"></div>`)
        }

        for (let day = 1; day <= lastDate; day++) {
            if (dayAndCant.some(e => e.dia == day)) {
                dayAndCant.forEach(element => {
                    if (element.dia == day) {
                        days.insertAdjacentHTML('beforeend', `<div class="day"><p class="num-day text-muted">${day}</p> <p class="cant-day">${element.cant}</p></div>`)
                        return
                    }
                });
            } else {
                days.insertAdjacentHTML('beforeend', `<div class="day"><p class="num-day text-muted">${day}</p> <p class="cant-day"></p></div>`)
            }
            // days.insertAdjacentHTML('beforeend', `<div class="day"><p class="num-day text-muted">${day}</p> <p class="text-center fs-5"></p></div>`)
        }
    }

    function barEstTramite() {
        var ctx = document.getElementById('chartEstTramite').getContext('2d');

        dataEstTramite = <?php echo json_encode($dataIncEstTra) ?>;

        var labels = dataEstTramite.map(e => e.estado)
        var values = dataEstTramite.map(e => e.valor)
        var option = '<?php echo $option ?>'

        var colors = []
        for (let i = 0; i < labels.length; i++) {
            colors.push('rgb(255, 99, 132)')
        }

        var data = {}
        var options = {}

        if (option == "#" || option == "%") {
            data = {
                labels: labels,
                datasets: [{
                    label: 'Valor(<?php echo $option ?>) por estado',
                    barPercentage: 1,
                    maxBarThickness: 23,
                    // barThickness: 17,
                    data: values,
                    backgroundColor: colors,
                    borderWidth: 2
                }]
            }

            options = {
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Estado Actual",
                            fontColor: "red",
                            position: 'top'
                        }
                    }]

                }
            }
        } else if (option == "$") {
            data = {
                labels: labels,
                datasets: [{
                    label: 'Valor(<?php echo $option ?>) por entidad responsable',
                    barPercentage: 1,
                    maxBarThickness: 23,
                    data: values,
                    backgroundColor: colors,
                    borderWidth: 2
                }]
            }

            options = {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor($) por estado actual: " + Number(tooltipItem.xLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function(value) {
                                return addCommas(value)
                            }
                        },

                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Estado Actual",
                            fontColor: "red"
                        }
                    }]

                }
            }
        }
        new Chart(ctx, {
            type: 'horizontalBar',
            data: data,
            options: options
        })
    }

    function barEntidades() {
        var ctx = document.getElementById('chartEntidades').getContext('2d');

        var dataEntidades = <?php echo json_encode($dataIncEntidades) ?>

        var labels = dataEntidades.map(e => e.entidad)
        var values = dataEntidades.map(e => e.valor)
        var option = '<?php echo $option ?>'

        var colors = []
        for (let i = 0; i < labels.length; i++) {
            colors.push('rgb(255, 99, 132)')
        }

        var data = {}
        var options = {}

        if (option == "#" || option == "%") {
            data = {
                labels: labels,
                datasets: [{
                    label: 'Valor(<?php echo $option ?>) por entidad responsable',
                    barPercentage: 1,
                    maxBarThickness: 23,
                    // barThickness: 15,
                    data: values,
                    backgroundColor: colors,
                    borderWidth: 2
                }]
            }

            options = {
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Entidad Responsable",
                            fontColor: "red"
                        }
                    }]

                }
            }


        } else {

            data = {
                labels: labels,
                datasets: [{
                    label: 'Valor(<?php echo $option ?>) por entidad responsable',
                    barPercentage: 1,
                    maxBarThickness: 23,
                    data: values,
                    backgroundColor: colors,
                    borderWidth: 2
                }]
            }

            options = {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor($) por estado actual: " + Number(tooltipItem.xLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function(value) {
                                return addCommas(value)
                            }
                        },
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Entidad Responsable",
                            fontColor: "red"
                        }
                    }]

                }
            }
        }

        new Chart(ctx, {
            type: 'horizontalBar',
            data: data,
            options: options
        })
    }

    function barEdades() {
        var ctx = document.getElementById('chartEdades').getContext('2d');

        var dataEdades = <?php echo json_encode($dataIncEdades) ?>

        var labels = dataEdades.map(e => e.label)
        var values = dataEdades.map(e => e.valor)
        var option = '<?php echo $option ?>'

        var colors = []
        for (let i = 0; i < labels.length; i++) {
            colors.push('rgb(255, 99, 132)')
        }

        if (option == "#" || option == "%") {
            data = {
                labels: labels,
                datasets: [{
                    label: 'Valor(<?php echo $option ?>) por edad a recibir rango',
                    barPercentage: 1,
                    data: values,
                    backgroundColor: colors,
                    borderWidth: 2
                }]
            }
            options = {
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Edad a Recibir Rango",
                            fontColor: "red"
                        }
                    }]

                }
            }

        } else {

            data = {
                labels: labels,
                datasets: [{
                    label: 'Valor(<?php echo $option ?>) por edad a recibir rango',
                    barPercentage: 1,
                    data: values,
                    backgroundColor: colors,
                    borderWidth: 2
                }]
            }
            options = {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor($) por entidad responsable: " + Number(tooltipItem.xLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function(value) {
                                return addCommas(value)
                            }
                        },
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Entidad Responsable",
                            fontColor: "red"
                        }
                    }]
                }
            }
        }
        new Chart(ctx, {
            type: 'horizontalBar',
            data: data,
            options: options
        })
    }

    function barOrigen() {
        var ctx = document.getElementById('chartOrigen').getContext('2d');

        var dataOrigen = <?php echo json_encode($dataIncOrigen) ?>

        var labels = dataOrigen.map(e => e.origen)
        var values = dataOrigen.map(e => e.valor)
        var option = '<?php echo $option ?>'

        var colors = []
        for (let i = 0; i < labels.length; i++) {
            colors.push('rgb(255, 99, 132)')
        }

        if (option == "#" || option == "%") {

            data = {
                labels: labels,
                datasets: [{
                    label: 'Valor(<?php echo $option ?>) por origen',
                    barPercentage: 1,
                    data: values,
                    backgroundColor: colors,
                    borderWidth: 2
                }]
            }
            options = {
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Origen",
                            fontColor: "red"
                        }
                    }]

                }
            }

        } else {
            data = {
                labels: labels,
                datasets: [{
                    label: 'Valor(<?php echo $option ?>) por origen',
                    barPercentage: 1,
                    data: values,
                    backgroundColor: colors,
                    borderWidth: 2
                }]
            }
            options = {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor($) por origen: " + Number(tooltipItem.xLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function(value) {
                                return addCommas(value)
                            }
                        },
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Origen",
                            fontColor: "red"
                        }
                    }]
                }
            }
        }

        new Chart(ctx, {
            type: 'horizontalBar',
            data: data,
            options: options
        })
    }

    function barAtencion() {
        var ctx = document.getElementById('chartViaAtencion').getContext('2d');

        var dataEdades = <?php echo json_encode($dataIncAtencion) ?>

        var labels = dataEdades.map(e => e.atencion)
        var values = dataEdades.map(e => e.valor)
        var option = '<?php echo $option ?>'

        var colors = []
        for (let i = 0; i < labels.length; i++) {
            colors.push('rgb(255, 99, 132)')
        }

        var data = {}
        var options = {}

        if (option == "#" || option == "%") {

            data = {
                labels: labels,
                datasets: [{
                    label: 'Valor(<?php echo $option ?>) por atención',
                    barPercentage: 1,
                    maxBarThickness: 23,
                    data: values,
                    backgroundColor: colors,
                    borderWidth: 2
                }]
            }

            options = {
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Atención",
                            fontColor: "red"
                        }
                    }]

                }
            }

        } else {
            data = {
                labels: labels,
                datasets: [{
                    label: 'Valor(<?php echo $option ?>) por atención',
                    barPercentage: 1,
                    maxBarThickness: 23,
                    data: values,
                    backgroundColor: colors,
                    borderWidth: 2
                }]
            }

            options = {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor($) por atención: " + Number(tooltipItem.xLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function(value) {
                                return addCommas(value)
                            }
                        },
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Atención",
                            fontColor: "red"
                        }
                    }]
                }
            }
        }

        new Chart(ctx, {
            type: 'horizontalBar',
            data: data,
            options: options
        })
    }

    function barAllMeses() {
        var months = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",
        "Ago", "Sep", "Oct", "Nov", "Dic"]
        
        var ctx = document.getElementById('chartAllMeses').getContext('2d');

        var dataAllMeses = <?php echo json_encode($dataIncAllMeses) ?>
        
        var labels = dataAllMeses.map(e => months[parseInt(e.mes)-1])
    
        var valuesEmp = dataAllMeses.map(e => e.emp)
        var valuesPag = dataAllMeses.map(e => e.pag)
        var valuesPen = dataAllMeses.map(e => e.pen)
        var valuesSin = dataAllMeses.map(e => e.sin)

        var option = '<?php echo $option ?>'

        var colorsSin = []
        var colorsPag = []
        var colorsPen = []
        var colorsEmp = []
        for (let i = 0; i < labels.length; i++) {
            colorsPag.push('rgb(47, 242, 62)')
            colorsEmp.push('rgb(47, 80, 242)')
            colorsPen.push('rgb(221, 242, 47)')
            colorsSin.push('rgb(242, 47, 47')
        }
        

        var data = {}
        var options = {}

        if (option == "#" || option == "%") {

            data = {
                labels: labels,
                datasets: [
                    {
                        label: '<?php echo $option ?> empresa',
                        barPercentage: 1,
                        maxBarThickness: 23,
                        data: valuesEmp,
                        backgroundColor: colorsEmp,
                        borderWidth: 2
                    },
                    {
                        label: '<?php echo $option ?> pagadas',
                        barPercentage: 1,
                        maxBarThickness: 23,
                        data: valuesPag,
                        backgroundColor: colorsPag,
                        borderWidth: 2
                    },
                    {
                        label: '<?php echo $option ?> pendientes',
                        barPercentage: 1,
                        maxBarThickness: 23,
                        data: valuesPen,
                        backgroundColor: colorsPen,
                        borderWidth: 2
                    },
                    {
                        label: '<?php echo $option ?> sin reconocimiento',
                        barPercentage: 1,
                        maxBarThickness: 23,
                        data: valuesSin,
                        backgroundColor: colorsSin,
                        borderWidth: 2
                    },
                ]
            }

            options = {
                scales: {
                    xAxes: [{
                        stacked: true
                        
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Valor(<?php echo $option ?>)',
                            fontColor: "red"
                        }
                    }]

                }
            }

        } else {
            data = {
                labels: labels,
                datasets: [
                    {
                        label: '<?php echo $option ?> empresa',
                        barPercentage: 1,
                        maxBarThickness: 23,
                        data: valuesEmp,
                        backgroundColor: colorsEmp,
                        borderWidth: 2
                    },
                    {
                        label: '<?php echo $option ?> pagadas',
                        barPercentage: 1,
                        maxBarThickness: 23,
                        data: valuesPag,
                        backgroundColor: colorsPag,
                        borderWidth: 2
                    },
                    {
                        label: '<?php echo $option ?> pendientes',
                        barPercentage: 1,
                        maxBarThickness: 23,
                        data: valuesPen,
                        backgroundColor: colorsPen,
                        borderWidth: 2
                    },
                    {
                        label: '<?php echo $option ?> sin reconocimiento',
                        barPercentage: 1,
                        maxBarThickness: 23,
                        data: valuesSin,
                        backgroundColor: colorsSin,
                        borderWidth: 2
                    }
                ]
            }

            options = {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor($): " + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        stacked: true
                        // ticks: {
                        //     beginAtZero: true,
                        //     callback: function(value) {
                        //         return addCommas(value)
                        //     }
                        // },
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            callback: function(value) {
                                return addCommas(value)
                            }
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Valor(<?php echo $option ?>)',
                            fontColor: "red"
                        }
                    }]
                }
            }
        }

        new Chart(ctx, {
            type: 'bar',
            data: data,
            options: options
        })
    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
</script>