<?php
   session_start();
   $_SESSION['start'] = time();
   require_once '../controladores/mail.controlador.php';
   require_once '../controladores/plantilla.controlador.php';
   require_once '../controladores/auditoria.controlador.php';
   
   require_once '../modelos/dao.modelo.php';
   require_once '../modelos/auditoria.modelo.php';
    
// informacion general de ingreso al sistema
    $campos = " * ";
    $tabla = "gi_auditoria INNER JOIN gi_usuario ON aud_id_usuario_i= user_id LEFT JOIN gi_empresa ON aud_empresa_v = emp_id";
    $condiciones = "aud_metodo_v != 'ctrIngresoUsuario'";
    $and = ' AND ';
    if(isset($_POST['NuevoFechaInicio2']) && $_POST['NuevoFechaFinal2'] != ''){
        if(isset($_POST['NuevoFechaFinal2']) && $_POST['NuevoFechaFinal2'] != ''){
            $condiciones .= $and."aud_fecha_d BETWEEN '".$_POST['NuevoFechaInicio2']."' AND '".$_POST['NuevoFechaFinal2']."' ";
            $and = " AND ";
        } else{
            $condiciones .= $and."aud_fecha_d = '".$_POST['NuevoFechaInicio2']."'";
            $and = " AND ";
        }
    }
    //USUARIO
    if(isset($_POST['buscarUsuario']) && $_POST['buscarUsuario'] != 0){
        $condiciones .= $and."aud_id_usuario_i = ".$_POST['buscarUsuario'];
        $and = " AND ";
    }

    //Empresa
    if(isset($_POST['buscarEmpresa']) && $_POST['buscarEmpresa'] != 0){
        $condiciones .= $and."emp_id = ".$_POST['buscarEmpresa'];
        $and = " AND ";
    }

    //echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condiciones;
    $respuestaX = ControladorAuditoria::getDataFromLsql($campos, $tabla, $condiciones);
   //print_r( $respuestaX);
   //die(var_dump($respuestaX));


    /*consultas para ingreso  */
    $campos = " date_format(aud_fecha_d,'%d/%m/%Y') as aud_fecha_d,aud_hora_d,aud_id_session,aud_metodo_v,user_nombre,user_apellidos,aud_ciudad_v,aud_pais_v,aud_ip_i,aud_navegador_v";
    $tabla = "gi_auditoria JOIN gi_usuario ON aud_id_usuario_i= user_id";
    $condiciones = "aud_metodo_v = 'ctrIngresoUsuario'";
    $and = ' AND ';
    if(isset($_POST['NuevoFechaInicio2']) && $_POST['NuevoFechaInicio2'] != ''){
        if(isset($_POST['NuevoFechaFinal2']) && $_POST['NuevoFechaFinal2'] != ''){
            $condiciones .= $and."aud_fecha_d BETWEEN '".$_POST['NuevoFechaInicio2']."' AND '".$_POST['NuevoFechaFinal2']."' ";
          
        } else{
            $condiciones .= $and."aud_fecha_d = '".$_POST['NuevoFechaInicio2']."'";
          
        }
    }
    //USUARIO
    if(isset($_POST['buscarUsuario']) && $_POST['buscarUsuario'] != 0){
        $condiciones .= $and."aud_id_usuario_i = ".$_POST['buscarUsuario'];
        
    }

    //echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condiciones;
    $respuestaAuth= ControladorAuditoria::getDataFromLsql($campos, $tabla, $condiciones);
    //print_r( $respuestaAuth);
?>
<div class="row">
    <div class="col-12">
            <div class="card border border-danger">
                <div class="card-header bg-transparent border-danger">
                    <h5 class="my-0 text-danger">
                    DATOS DE REPORTE AUDITORIA
                    </h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="tablaAuditoria">
                        <thead>
                            <tr>
                            <th>#</th>
                                <th>USUARIO</th>
                                <th>EMPRESA</th>
                                <th>FECHA</th>
                                <th>HORA</th>
                                <th>GESTION1</th> <!--es la clase---> 
                                <th>GESTION2</th> <!--es el metodo---> 
                                <th>DIRECCION IP</th>
                                <th>INCAPACIDAD</th>
                                <th>CIUDAD</th>
                                <th>PAIS</th>
                              
                            </tr>
                        </thead>
                        <tbody>
                                    <?php
                                            foreach ($respuestaX as $key => $value) {
                                                
                                                echo '<tr>';
                                                    echo '<td>'.($key+1).'</td>';
                                                     echo '<td>'.$value["user_nombre"].' '.$value["user_apellidos"].'</td>';
                                                    echo '<td>'.$value["emp_nombre"].'</td>';
                                                    echo '<td>'.$value["aud_fecha_d"].'</td>';
                                                    echo '<td>'.$value["aud_hora_d"].'</td>';
                                                    echo '<td>'.$value["aud_metodo_v"].'</td>';
                                                    echo '<td>'.$value["aud_controlador_v"].'</td>';
                                                    echo '<td>'.$value["aud_ip_i"].'</td>';
                                                    if($value["aud_id_incapacidad_i"]=='0'){
                                                        echo '<td>NO APLICA</td>';
                                                    }else{
                                                        echo '<td>'.$value["aud_id_incapacidad_i"].'</td>';
                                                    }
                                                    
                                                    echo '<td>'.$value["aud_ciudad_v"].'</td>';
                                                    echo '<td>'.$value["aud_pais_v"].'</td>';
                                                   
                                                echo '</tr>';
                                            }
                                    ?>
                           
                        </tbody>
                    </table>

                </div>
            </div>  
    </div>
       
    <div class="col-12">
    
            <div class="card border border-danger">
                <div class="card-header bg-transparent border-danger">
                    <h5 class="my-0 text-danger">
                    DATOS DE INICIO DE SECCION 
                    </h5>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="tablaIniSeccion">
                        <thead>
                            <tr>
                            <th>#</th>
                                <th>USUARIO</th>
                                <th>FECHA</th>
                                <th>H.INICIO</th>
                                <th>DIRECCION IP</th>
                                <th>NAVEGADOR</th>
                               
                                <th>CIUDAD</th>
                                <th>PAIS</th>
                                <th>H.FINAL</th>

                            </tr>
                        </thead>
                        <tbody>
                                    <?php
                                            
                                            foreach ($respuestaAuth as $key => $value) {
                                                
                                                echo '<tr>';
                                                    echo '<td>'.($key+1).'</td>';
                                                    echo '<td>'.$value["user_nombre"].' '.$value["user_apellidos"].'</td>';
                                                    echo '<td>'.$value["aud_fecha_d"].'</td>';
                                                    echo '<td>'.$value["aud_hora_d"].'</td>';
                                                    echo '<td>'.$value["aud_ip_i"].'</td>';
                                                    echo '<td>'.$value["aud_navegador_v"].'</td>';
                                                    echo '<td>'.$value["aud_ciudad_v"].'</td>';
                                                    echo '<td>'.$value["aud_pais_v"].'</td>';
                                                    echo '<td>'.ControladorAuditoria::ctrUltimoMovimiento($value["aud_id_session"]).'</td>';
                                                echo '</tr>';
                                            }
                                    ?>
                           
                        </tbody>
                    </table>

                </div>
            </div>  
    
    </div>
</div>
<script type="text/javascript">
$(function(){
    var edicion =  '<div class="btn-group dropstart" role="group">';
    edicion += '<button type="button" type="button" class="btn btn-sm btn-outline-danger waves-effect waves-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    edicion += '<i class="fa fa-info-circle"></i>';
    edicion += '</button>';
    edicion += '<ul class="dropdown-menu" role="menu">';
    edicion += '<li><a class="dropdown-item btnVerAuditoria" title="Ver" idArchivo data-bs-toggle="modal" data-bs-target="#modalEditarArchivos" href="#">VER</a></li>';
    edicion += '<li class="divider"></li>';
    edicion += '<li><a class="dropdown-item btnExportarAuditoria" title="Exportar" idArchivo href="#">EXPORTAR</a></li>';

    var tabla_auditoria = $('#tablaAuditoria').DataTable({
        responsive: true,
        "language" : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        } 
        });
        var tabla_seccion = $('#tablaIniSeccion').DataTable({
        responsive: true,
        "language" : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        } 
        });
        });
</script>