<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
	require_once '../modelos/dao.modelo.php';
	/**
	* Clase para utilizar comandos desde PHP shell
	*/
	class AjaxServicesPython
	{	
		public function exportarConsolidado()
		{
			$response = '/BdPython/Exportar_Consolidado_'.$_SESSION['cliente_id'].'_'.$_SESSION['idSession'].'.xlsx';
			$request = 'python3 /var/www/html/BdPython/exportar.py "'.$_SESSION['idSession'].'" "'.$_SESSION['cliente_id'].'"';
			$result = shell_exec($request);
			self::auditorias($request, $response);
			echo json_encode(array('code' => 1, 'ruta' => $response  , 'archivo' => 'Exportar_Consolidado_'.$_SESSION['cliente_id'].'_'.$_SESSION['idSession'].'.xlsx'));
		}

		public function exportarConsolidadoComentarios()
		{	
			$response = '/BdPython/Exportar_Comentarios_'.$_SESSION['cliente_id'].'_'.$_SESSION['idSession'].'.xlsx' ;
			$request = 'python3 /var/www/html/BdPython/exportar_comentarios.py "'.$_SESSION['idSession'].'" "'.$_SESSION['cliente_id'].'"';
			$result = shell_exec($request);
			self::auditorias($request, $response);
			echo json_encode(array('code' => 1, 'ruta' =>$response, 'archivo' => 'Exportar_Comentarios_'.$_SESSION['cliente_id'].'_'.$_SESSION['idSession'].'.xlsx' ));
		}

		public function exportarConsolidadoIncompletas()
		{	
			$response ='/BdPython/Consolidado_incapacidades_incompletas_'.$_SESSION['cliente_id'].'_'.$_SESSION['idSession'].'.xlsx';
			$request ='python3 /var/www/html/BdPython/exportarIncompletas.py "'.$_SESSION['idSession'].'" "'.$_SESSION['cliente_id'].'"';
			$result = shell_exec($request);
			self::auditorias($request, $response);
			echo json_encode(array('code' => 1, 'ruta' => $response , 'archivo' => 'Exportar_Comentarios_'.$_SESSION['cliente_id'].'_'.$_SESSION['idSession'].'.xlsx'));
		}

		public function exportarMedicos(){
			$response = '/BdPython/Exportar_Medicos_'.$_SESSION['idSession'].'.xlsx';
			$request ='python3 /var/www/html/BdPython/exportar_medicos.py "'.$_SESSION['idSession'].'"';
			$result = shell_exec($request);
			self::auditorias($request, $response);
			echo json_encode(array('code' => 1, 'ruta' => $response, 'archivo' => 'Exportar_Medicos_'.$_SESSION['idSession'].'.xlsx'));
		}

		public function auditorias($request, $response)
		{
				ControladorPlantilla::setAuditoria('ControladorPlantilla', 'Exportar', $request, $response);
		}
	}

	if($_POST['getConsolidado']){
		$ajax = new AjaxServicesPython();
		$ajax->exportarConsolidado();
	}

	if($_POST['getConsolidadoComentarios']){
		$ajax = new AjaxServicesPython();
		$ajax->exportarConsolidadoComentarios();
	}

	if($_POST['getConsolidadoIncompletas']){
		$ajax = new AjaxServicesPython();
		$ajax->exportarConsolidadoComentarios();
	}

	if ($_POST['exportarDatosMed']) {
		$ajax = new AjaxServicesPython();
		$ajax->exportarMedicos();
	}