<?php
session_start();
$_SESSION['start'] = time();

require_once '../controladores/mail.controlador.php';
require_once '../controladores/plantilla.controlador.php';
require_once '../controladores/medicos.controlador.php';
require_once '../modelos/dao.modelo.php';
require_once '../modelos/medicos.modelos.php';

class AjaxMedicos{
    
    public $id_medico;
    public $validarIdMed = null;
    public $dataSelectMed;

    public function ajaxEditarMedicos(){
        $item = 'med_id';
        $valor = $this->id_medico;
        $respuesta = ControladorMedicos::getData('gi_medicos',$item, $valor);
        echo json_encode($respuesta);
    }

    public function ajaxMedicosIps(){
        $item = 'ment_id_medico';
        $valor = $this->id_medico;
        $respuesta = ControladorMedicos::ctrMedicosEntidades($item, $valor);
        echo json_encode($respuesta);
    }

    public function ajaxValidarIdentificacion(){
        $campo = 'med_identificacion';
        $valor = $this->validarIdMed;
        $respuesta = ControladorMedicos::existeIdMedico($campo, $valor);
        echo json_encode($respuesta);
    }

    public function getDataSelectMed(){
        $valor = $this->dataSelectMed;
        $respuesta = ControladorMedicos::ctrBuscarMedicos($valor);
        $i = 0;
        $datos = array();
        foreach ($respuesta as $key => $value) {
            $datos[$i]['id'] = $value['id'];
            $datos[$i]['text'] = $value['ident'].' - '.$value['name'];
            $i++;
        } 
        echo json_encode($datos);
    }
}

if(isset($_GET['getMedicosSelect'])){
    $AjaxMedicos = new AjaxMedicos();
    $AjaxMedicos->dataSelectMed = $_POST['query'];
    $AjaxMedicos->getDataSelectMed();
}

if (isset($_POST['EditarMedicoId']) && $_POST['EditarMedicoId'] != '') {
    $editar = new AjaxMedicos();
    $editar->id_medico = $_POST['EditarMedicoId'];
    $editar->ajaxEditarMedicos();
}

if (isset($_POST['idMedicoEnt'])) {
    $medEnt = new AjaxMedicos();
    $medEnt->id_medico = $_POST['idMedicoEnt'];
    $medEnt->ajaxMedicosIps();
}

if (isset($_POST['validarIdentificacion'])) {
    $validarMed = new AjaxMedicos();
    $validarMed->validarIdMed = $_POST['validarIdentificacion'];
    $validarMed->ajaxValidarIdentificacion();
}

if (isset($_POST['NuevoNombreMedico'])) {
    echo ControladorMedicos::ctrCrearMedicos();
}

if (isset($_POST['EditarNombreMedico'])) {
    echo ControladorMedicos::ctrEditarMedicos();
}

if (isset($_POST['id_medico'])) {
    echo ControladorMedicos::ctrBorrarMedicos();
}

if (isset($_GET['getMedicos'])) {
    $where = '';
    $campos = "med_nombre, med_identificacion, med_num_registro, med_fecha_creacion, med_estado_rethus, med_id";
    $tabla = "gi_medicos";
    
    $medicos = ControladorMedicos::getDataFromLsql($campos, $tabla, null, null,null);

    // echo '{
    //     "data" : [';
    //             $i = 0;
    //             foreach ($medicos as $key => $value) {
    //                if($i != 0){
    //                   echo ",";
    //               }
    //               echo '[';
    //               echo '"'.$value['med_nombre'].'",';
    //               echo '"'.$value['med_identificacion'].'",';    
    //               echo '"'.$value['med_num_registro'].'",';
    //               //echo '"'.$value['med_fecha_creacion'].'",';  
    //               if($value['med_estado_rethus'] == '0'){
    //                   echo '"NO VALIDADO",'; 	
    //               }else if($value['med_estado_rethus'] == '1'){
    //                   echo '"REGISTRA",'; 
    //               }else {
    //                 echo '"NO REGISTRA",'; 
    //               } 
    //               echo '"'.$value['med_id'].'"'; 
    //               echo ']';
    //               $i++;
    //             }
    //       echo ']
    //     }';

    $data = array();
    foreach ($medicos as $value) {
        $estado = '';
        if ($value['med_estado_rethus'] == '0') {
            $estado = 'NO VALIDADO';
        } else if ($value['med_estado_rethus'] == '1') {
            $estado = 'REGISTRA';
        } else {
            $estado = 'NO REGISTRA';
        }

        $data[] = array(
            $value['med_nombre'],
            $value['med_identificacion'],
            $value['med_num_registro'],
            $estado,
            $value['med_id']
        );
    }

    $response = array('data' => $data);
    echo json_encode($response);
}