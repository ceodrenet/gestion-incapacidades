<?php
	session_start();
	$_SESSION['start'] = time();
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/tesoreria.modelo.php';

	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/incapacidades.controlador.php';

	require_once '../vendor/autoload.php';

	date_default_timezone_set('America/Bogota');

	$mes_en_ingles = date('j \d\e F \d\e Y');;
	$mes_en_espanol = str_replace(
	    ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
	    ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
	    $mes_en_ingles
	);

	$campo_ = "gi_empresa.emp_direccion, gi_empresa.emp_nombre, gi_empresa.emp_nit, gi_empleados.emd_nombre, gi_empleados.emd_cedula, DATE_FORMAT(inc_fecha_inicio, '%d/%m/%Y') as inc_fecha_inicio, DATE_FORMAT(inc_fecha_final, '%d/%m/%Y') AS inc_fecha_final, gi_ips.ips_nombre, DATEDIFF(inc_fecha_final,inc_fecha_inicio) as total_dias, DATE_FORMAT(	inc_fecha_radicacion, '%d/%m/%Y') as 	inc_fecha_radicacion, inc_numero_radicado_v, ori_descripcion_v, gi_empresa.emp_email_gestion" ;
 	$tabla_ = " gi_incapacidad JOIN gi_origen ON inc_origen = ori_id_i JOIN gi_empleados ON gi_empleados.emd_id = gi_incapacidad.inc_emd_id JOIN gi_empresa ON gi_empleados.emd_emp_id = gi_empresa.emp_id JOIN gi_ips ON gi_incapacidad.inc_ips_afiliado = gi_ips.ips_id";
	$condi_ = " inc_id = ".$_GET['idIncapacidad'];

	$resultado = ModeloTesoreria::mdlMostrarUnitario($campo_, $tabla_, $condi_ );


	$mpdf = new \Mpdf\Mpdf([
	    'mode' => 'utf-8',
	    'format' => 'A4',
	]);
	$stylesheet = file_get_contents('estilo.css');
	$mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);

	$mpdf->SetHTMLFooter('Pag. {PAGENO} de {nb}');

	$carta = "<br/><br/>";
	$carta .= "<p>Barranquilla, ".$mes_en_espanol."</p>";
	$carta .= "<br/>";
	$carta .= "<p>Señores:<br/>";
	$carta .= "<b>".mb_strtoupper($resultado['ips_nombre'])."</b><br/>";
	$carta .= "<b>Prestaciones Económicas</b><br/>";
	$carta .= "Ciudad</p>";
	$carta .= "<br/>";
	$carta .= "<p><b>PETICION: SOLICITUD DE PAGO DE INCAPACIDADES </b></p><br/>";
	$carta .= "<p style='text-align:justify;'>Con la presente ".mb_strtoupper($resultado['emd_nombre'])." identificada con NIT. ".$resultado['emp_nit']." solicitamos a ".mb_strtoupper($resultado['ips_nombre'])." el reconocimiento y pago de las incapacidades de nuestro colaborador@ ".mb_strtoupper($resultado['emd_nombre'])." identificado con CC. ".$resultado['emd_cedula'].", toda vez que ".mb_strtoupper($resultado['ips_nombre'])." ha demorado de manera injustificada el pago de las incapacidades, incumpliendo así con su deber de pago oportuno establecido en el artículo 24 del Decreto 4023 del 2011.</p><br/>";	

	$carta .= "<p><b>DATOS DE LAS INCAPACIDADES</b><br/>";
	$carta .= '<table width="100%"  class="blueTable">
		 	<tr style="border:solid 1px;">
				<th align="center">FECHA INICIO</th>
				<th align="center">DÍAS</th>
				<th align="center">TIPO</th>
				<th align="center">FECHA RADICACIÓN</th>
				<th align="center">NUMERO RADICADO</th>
            </tr>
            <tr style="border:solid 1px;">
				<td align="center">'.$resultado['inc_fecha_inicio'].'</td>
				<td align="center">'.($resultado['total_dias']+1).'</td>
				<td align="center">'.$resultado['ori_descripcion_v'].'</td>
				<td align="center">'.$resultado['inc_fecha_radicacion'].'</td>
				<td align="center">'.$resultado['inc_numero_radicado_v'].'</td>
            </tr>
		</table></p>';
	$carta .= "<br/><br/><br/><p><b>DECRETO 4023 DE 2011</b></p>";
	$carta .= "<p style='text-align:justify;'><b>Artículo 24.</b> Pago de prestaciones económicas. A partir de la fecha de entrada en vigencia de las cuentas maestras de recaudo, los aportantes y trabajadores independientes, no podrán deducir de las cotizaciones en salud, los valores correspondientes a incapacidades por enfermedad general y licencias de maternidad y/o paternidad. El pago de estas prestaciones económicas al aportante será realizado directamente por la EPS y EOC, a través de reconocimiento directo o transferencia electrónica <b>en un plazo no mayor a cinco (5) días hábiles</b> contados a partir de la autorización de la prestación económica por parte de la EPS o EOC. La revisión y liquidación de las solicitudes de reconocimiento de prestaciones económicas <b>se efectuará dentro de los quince (15) días hábiles siguientes a la solicitud del aportante.</b> En todo caso, para la autorización y pago de las prestaciones económicas, las EPS y las EOC deberán verificar la cotización al Régimen Contributivo del SGSSS, efectuada por el aportante beneficiario de las mismas. </p>";

	$mpdf->WriteHTML($carta);

	$mpdf->AddPage();

	$peticion = "<p style='text-align:justify;'><b>Parágrafo 1°.</b> La EPS o la EOC que no cumpla con el plazo definido para el trámite y pago de las prestaciones económicas, <b>deberá realizar el reconocimiento y pago de intereses moratorios al aportante</b>, de acuerdo con lo definido en el artículo 4° del Decreto 1281 de 2002. </p>";
	$peticion .= "<p style='text-align:justify;'><b>Parágrafo 2°. De presentarse incumplimiento</b> del pago de las prestaciones económicas por parte de la EPS o EOC, el aportante <b>deberá informar a la Superintendencia Nacional de Salud, para que de acuerdo con sus competencias, esta entidad adelante las acciones a que hubiere lugar</b>.</p><br/>";
	$peticion .= "<p style='text-align:center;'><b>PETICIÓN</b></p>";
	/*$peticion .= "<p style='text-align:justify;'>Solicitamos a <span style='background:yellow;'>".mb_strtoupper($resultado['ips_nombre'])."</span> realizar el pago de la incapacidad de nuestro colaborador@ <span style='background:yellow;'>".mb_strtoupper($resultado['emd_nombre'])."</span> identificado con CC. <span style='background:yellow;'>".$resultado['emd_cedula']."</span> y pago de intereses moratorios según lo establecido en el artículo 24 del Decreto 4023 del 201</p>";
	$peticion .= "<br/><br/><p style='text-align:left;'>Solicitamos la respuesta a esta petición sea enviada al correo electrónico: <span style='background:yellow;'>".$resultado['emp_email_gestion']."</span></p>";*/


	$peticion .= "<p style='text-align:justify;'>Solicitamos a ".mb_strtoupper($resultado['ips_nombre'])." realizar el pago de la incapacidad de nuestro colaborador@ ".mb_strtoupper($resultado['emd_nombre'])." identificado con CC. ".$resultado['emd_cedula']." y pago de intereses moratorios según lo establecido en el artículo 24 del Decreto 4023 del 201</p>";
	$peticion .= "<br/><br/><p style='text-align:left;'>Solicitamos la respuesta a esta petición sea enviada al correo electrónico: ".$resultado['emp_email_gestion']."</p>";

	$mpdf->WriteHTML($peticion);

	$mpdf->Output();