<?php
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    session_start();
    $_SESSION['start'] = time();

    require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/incapacidades.controlador.php';
	require_once '../controladores/empleados.controlador.php';
    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
	require_once '../modelos/empleados.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';
    require_once '../extenciones/Excel.php';

	if(isset($_FILES['NuevoIncapacidad']['tmp_name']) && !empty($_FILES['NuevoIncapacidad']['tmp_name']) ){
		$name   = $_FILES['NuevoIncapacidad']['name'];
		$tname  = $_FILES['NuevoIncapacidad']['tmp_name'];
		ini_set('memory_limit','1028M');
        $obj_excel = PHPExcel_IOFactory::load($tname);
        $sheetData = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
        $arr_datos = array();
        $highestColumm = $obj_excel->setActiveSheetIndex(0)->getHighestColumn(); // e.g. "EL"
        $highestRow = $obj_excel->setActiveSheetIndex(0)->getHighestRow();
        $aciertos = 0;
        $fallos   = 0;
        $total = 0;
        $existentes = 0;
        $datoCsv = array();
        $noexiste = 0;
        $incapacidadesExistentes = 0;
        $vacios = 0;
		
        foreach ($sheetData as $index => $value) {
            if ( $index > 1 ){
                $valido = 0;
                $existe = false;
                $separador = '';
                $total++;
                $empresa = 0;
                if((!is_null($value['A']) && !empty($value['A'])) && 
                        (!is_null($value['C']) && !empty($value['C']))
                    ){
                    /* Iniciamos la consulta Sql */
                    $datos = array();

                    /* Identificacion */
                    if(!is_null($value['A']) && !empty($value['A']) ){
                        $valido = 1;
                        $datos['inc_cc_afiliado'] = $value['A'];    

                        $item = 'emd_cedula';
                        $valuess = $value['A'];
                        $tabla = 'gi_empleados';
                        $value2 = $_SESSION['cliente_id'];
                       
                        $res = ModeloEmpleados::mdlMostrarEmpleadosX2($tabla, $item, $valuess, $value2);
                        if($res != false){
                            if($res['emd_cedula'] == $value['A']){
                                /*Empleado Existente*/
                                $existe = true;
                                $datos['inc_emd_id'] = $res['emd_id'];
                                $datos['inc_empresa'] = $res['emd_emp_id'];
                                $datos['inc_ips_afiliado'] = $res['emd_eps_id'];
                            
                            }
                        } else{
                            /* No existe */
                            $existe = false;
                            $datx  = array(   
                                        'filas_fallo_fila' => $index,
                                        'fila_fallo_mensaje' => 'Esta cedula => '.$value['A'].' no esta registrada en la base de datos',
                                        'fila_fallo_session_id' => $_SESSION['idSession'],
                                        'fila_fallo_cedula' => $value['A'],
                                    );

                            $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datx);

                            $noexiste++;
                        }
                    }else{
                        $datos['inc_cc_afiliado'] = NULL;
                    }

                    if($existe == true){
                        //Preguntamos si la incapacidad existe
                        $item1 = "inc_cc_afiliado";
                        $valor1 = $value['A'];
                        $item2 = 'inc_fecha_inicio';
                        $valor2 = $value['C'];
                        $existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2, $_SESSION['cliente_id']);
                        $incapacidaDato = null;
                        if($existeInc){
                            //Traemos los datos de la incapacidad, porque ya validamos que esta existe
                            $strCampos = "*";
                            $strTabla_ = "gi_incapacidad";
                            $strWhere_ = "inc_cc_afiliado = '".$value['A']."' AND inc_fecha_inicio = '".$value['C']."'";
                            $incapacidaDato = ModeloTesoreria::mdlMostrarUnitario($strCampos, $strTabla_, $strWhere_);
                            $datos["inc_id"] = $incapacidaDato['inc_id'];
                        }

                        /* Nombres */
                        if(!is_null($value['C']) && !empty($value['C']) ){
                            $valido = 1;
                            $datos['inc_fecha_inicio'] = $value['C'];   
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_fecha_inicio'] = $incapacidaDato['inc_fecha_inicio'];
                            }else{
                                $datos['inc_fecha_inicio'] = NULL;    
                            }
                        }

                        /* Fecha Nacimiento */
                        if(!is_null($value['D']) && !empty($value['D']) ){
                            $datos['inc_fecha_final'] = $value['D'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_fecha_final'] = $incapacidaDato['inc_fecha_final'];
                            }else{
                                $datos['inc_fecha_final'] = NULL;    
                            }
                        }

                        /* Fecha ingreso */
                        if(!is_null($value['E']) && !empty($value['E']) ){
                            $datos['inc_origen'] = $value['E'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_origen'] = $incapacidaDato['inc_origen'];
                            }else{
                                $datos['inc_origen'] = NULL;    
                            }
                        }

                        /* Diagnostico */
                        if(!is_null($value['F']) && !empty($value['F']) ){
                            $valido = 1;
                            $datos['inc_diagnostico'] = $value['F'];    
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_diagnostico'] = $incapacidaDato['inc_diagnostico'];
                            }else{
                                $datos['inc_diagnostico'] = NULL;    
                            }
                        }

                        /* Fecha Nacimiento */
                        if(!is_null($value['G']) && !empty($value['G']) ){
                            $datos['inc_tipo_generacion'] = $value['G'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_tipo_generacion'] = $incapacidaDato['inc_tipo_generacion'];
                            }else{
                                $datos['inc_tipo_generacion'] = NULL;    
                            }
                        }

                        /* Clasificacion */
                        if(!is_null($value['H']) && !empty($value['H']) ){
                            $datos['inc_clasificacion'] = $value['H'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_clasificacion'] = $incapacidaDato['inc_clasificacion'];
                            }else{
                                $datos['inc_clasificacion'] = NULL;    
                            }
                        }

                        /* Valor pagado empresa  */
                        if(!is_null($value['I']) && !empty($value['I']) ){
                            //$datos['inc_valor_pagado_empresa'] = $value['I'];
                            $valor = str_replace('.', ',', $value['I']);
                            $datos['inc_valor_pagado_empresa'] = $valor;

                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_valor_pagado_empresa'] = $incapacidaDato['inc_valor_pagado_empresa'];
                            }else{
                                $datos['inc_valor_pagado_empresa'] = NULL;    
                            }
                        }

                        /* Valor pagado Administradora  */
                        if(!is_null($value['J']) && !empty($value['J']) ){
                            //$datos['inc_valor_pagado_eps'] = $value['J'];
                            $valor = str_replace('.', ',', $value['J']);
                            $datos['inc_valor_pagado_eps'] = $valor;

                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_valor_pagado_eps'] = $incapacidaDato['inc_valor_pagado_eps'];
                            }else{
                                $datos['inc_valor_pagado_eps'] = NULL;    
                            }
                        }

                        /* Valor Pagado Ajuste  */
                        if(!is_null($value['K']) && !empty($value['K']) ){
                            $valor = str_replace('.', ',', $value['K']);
                            $datos['inc_valor_pagado_ajuste'] = $valor;
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_valor_pagado_ajuste'] = $incapacidaDato['inc_valor_pagado_ajuste'];
                            }else{
                                $datos['inc_valor_pagado_ajuste'] = NULL;    
                            }
                        }

                        /* IVL empleado */
                        if(!is_null($value['L']) && !empty($value['L']) ){
                            $datos['inc_ivl_v'] = $value['L'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_ivl_v'] = $incapacidaDato['inc_ivl_v'];
                            }else{
                                $datos['inc_ivl_v'] = NULL;    
                            }
                        }

                        /* Fecha Pago Nomina */
                        if(!is_null($value['M']) && !empty($value['M']) ){
                            $datos['inc_fecha_pago_nomina'] = $value['M'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_fecha_pago_nomina'] = $incapacidaDato['inc_fecha_pago_nomina'];
                            }else{
                                $datos['inc_fecha_pago_nomina'] = NULL;    
                            }
                        }

                        /* Observacion */
                        if(!is_null($value['N']) && !empty($value['N']) ){
                            $datos['inc_observacion'] = $value['N'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_observacion'] = $incapacidaDato['inc_observacion'];
                            }else{
                                $datos['inc_observacion'] = NULL;    
                            }
                        }

                        /* Estado tramite */
                        if(!is_null($value['O']) && !empty($value['O']) ){
                            $datos['inc_estado_tramite'] = $value['O'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_estado_tramite'] = $incapacidaDato['inc_estado_tramite'];
                            }else{
                                $datos['inc_estado_tramite'] = NULL;    
                            }
                        }

                        if(!is_null($value['P']) && !empty($value['P']) ){
                            $datos['inc_sub_estado_i'] = $value['P'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_sub_estado_i'] = $incapacidaDato['inc_sub_estado_i'];
                            }else{
                                $datos['inc_sub_estado_i'] = NULL;    
                            }
                        }

                        /* valor pagado */
                        if(!is_null($value['Q']) && !empty($value['Q']) ){
                            $valor = str_replace('.', ',', $value['Q']);
                            $datos['inc_valor'] = $valor;
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_valor'] = $incapacidaDato['inc_valor'];
                            }else{
                                $datos['inc_valor'] = NULL;    
                            }
                        }

                        /* Estado tramite */
                        if(!is_null($value['R']) && !empty($value['R']) ){
                            if(!isset($_POST['chekUpdateDatos'])){
                                if($value['O'] == '3'){
                                    $datos['inc_fecha_radicacion'] = $value['R'];
                                    $datos['inc_fecha_solicitud'] = $incapacidaDato['inc_fecha_solicitud'];
                                    $datos['inc_fecha_pago'] = $incapacidaDato['inc_fecha_pago'];
                                    $datos['inc_fecha_negacion_'] = $incapacidaDato['inc_fecha_negacion_'];
                                }else  if($value['O'] == '4'){
                                    $datos['inc_fecha_solicitud'] = $value['R'];
                                    $datos['inc_fecha_radicacion'] = $incapacidaDato['inc_fecha_radicacion'];
                                    $datos['inc_fecha_pago'] = $incapacidaDato['inc_fecha_pago'];;
                                    $datos['inc_fecha_negacion_'] = $incapacidaDato['inc_fecha_negacion_'];
                                }else if($value['O'] == '5' || $value['O'] == '8'){
                                    $datos['inc_fecha_pago'] = $value['R'];
                                    $datos['inc_fecha_solicitud'] = $incapacidaDato['inc_fecha_solicitud'];
                                    $datos['inc_fecha_radicacion'] = $incapacidaDato['inc_fecha_radicacion'];
                                    $datos['inc_fecha_negacion_'] = $incapacidaDato['inc_fecha_negacion_'];
                                }else if($value['O'] == '6' || $value['O'] == '7'){
                                    $datos['inc_fecha_negacion_'] = $value['R'];
                                    $datos['inc_fecha_solicitud'] = $incapacidaDato['inc_fecha_solicitud'];
                                    $datos['inc_fecha_radicacion'] = $incapacidaDato['inc_fecha_radicacion'];
                                    $datos['inc_fecha_pago'] = $incapacidaDato['inc_fecha_pago'];
                                }
                            }else{
                                if($value['O'] == '3'){
                                    $datos['inc_fecha_radicacion'] = $value['R'];
                                    $datos['inc_fecha_solicitud'] = null;
                                    $datos['inc_fecha_pago'] = null;
                                    $datos['inc_fecha_negacion_'] = null;
                                }else  if($value['O'] == '4'){
                                    $datos['inc_fecha_solicitud'] = $value['R'];
                                    $datos['inc_fecha_radicacion'] = NULL;
                                    $datos['inc_fecha_pago'] = null;
                                    $datos['inc_fecha_negacion_'] = null;
                                }else if($value['O'] == '5' || $value['O'] == '8'){
                                    $datos['inc_fecha_pago'] = $value['R'];
                                    $datos['inc_fecha_solicitud'] = null;
                                    $datos['inc_fecha_radicacion'] = null;
                                    $datos['inc_fecha_negacion_'] = null;
                                }else if($value['O'] == '6' || $value['O'] == '7'){
                                    $datos['inc_fecha_negacion_'] = $value['R'];
                                    $datos['inc_fecha_solicitud'] = null;
                                    $datos['inc_fecha_radicacion'] = null;
                                    $datos['inc_fecha_pago'] = null;
                                }
                            }
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_fecha_pago'] = $incapacidaDato['inc_fecha_pago'];
                                $datos['inc_fecha_solicitud'] = $incapacidaDato['inc_fecha_solicitud'];
                                $datos['inc_fecha_radicacion'] = $incapacidaDato['inc_fecha_radicacion'];
                                $datos['inc_fecha_negacion_'] = $incapacidaDato['inc_fecha_negacion_'];
                            }else{
                                $datos['inc_fecha_pago'] = null;
                                $datos['inc_fecha_solicitud'] = null;
                                $datos['inc_fecha_radicacion'] = null;
                                $datos['inc_fecha_negacion_'] = null;
                            }
                            
                        }

                    
                        /* inc_numero_radicado_v */
                        if(!is_null($value['S']) && !empty($value['S']) ){
                            $datos['inc_numero_radicado_v'] = $value['S'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_numero_radicado_v'] = $incapacidaDato['inc_numero_radicado_v'];
                            }else{
                                $datos['inc_numero_radicado_v'] = null;
                            }
                        }

                        /* inc_fecha_recepcion_d */
                        if(!is_null($value['T']) && !empty($value['T']) ){
                            $datos['inc_fecha_recepcion_d'] = $value['T'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_fecha_recepcion_d'] = $incapacidaDato['inc_fecha_recepcion_d'];
                            }else{
                                $datos['inc_fecha_recepcion_d'] = null;
                            }
                            
                        }

                        /* inc_registrada_entidad_i */
                        if(!is_null($value['U']) && !empty($value['U']) ){
                            $datos['inc_registrada_entidad_i'] = $value['U'];
                            $valido = 1;
                        }else{
                            if($incapacidaDato != null){
                                $datos['inc_registrada_entidad_i'] = $incapacidaDato['inc_registrada_entidad_i'];
                            }else{
                                $datos['inc_registrada_entidad_i'] = null;
                            }
                        }
                        $tabla = "gi_incapacidad";  
                        $datos['inc_estado'] = 1;                       
                        if($valido == 1){
                            if($existe){
                                if(!$existeInc){
                                    if(isset($_POST['chekUpdateDatos'])){
                                        $respuesta = ModeloIncapacidades::mdlIngresarIncapacidad($tabla, $datos);
                                        if($respuesta != 'Error'){
                                            $aciertos++;
                                        }else{
                                            $fallos++;
                                            //var_dump($respuesta);
                                            $datos  = array(   
                                                'filas_fallo_fila' => $i,
                                                'fila_fallo_mensaje' => 'No se pudo guardar revisala, debe tener alguna celda mal configurada',
                                                'fila_fallo_session_id' => $_SESSION['idSession'],
                                                'fila_fallo_texto_error_v' => $respuesta,
                                                'fila_fallo_cedula' => $valor1,
                                            );

                                            $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);
                                        }
                                    }
                                }else{
                                    if(!isset($_POST['chekUpdateDatos'])){
                                        /*Pidieron Actualizar los datos*/                                        
                                        $tabla = "gi_incapacidad";
                                        $datos['inc_ruta_incapacidad'] = $incapacidaDato['inc_ruta_incapacidad'];
                                        $datos['inc_ruta_incapacidad_transcrita'] = $incapacidaDato['inc_ruta_incapacidad_transcrita'];
                                        $datos['inc_profesional_responsable'] = $incapacidaDato['inc_profesional_responsable'];
                                        $datos['inc_prof_registro_medico'] = $incapacidaDato['inc_prof_registro_medico'];
                                        $datos['inc_donde_se_genera'] = $incapacidaDato['inc_donde_se_genera'];
                                        $respuesta = ModeloIncapacidades::mdlEditarIncapacidad($tabla, $datos);
                                        if($respuesta != 'Error'){
                                            $aciertos++;
                                        }else{
                                            $fallos++;
                                            //var_dump($respuesta);
                                            $datos  = array(   
                                                'filas_fallo_fila' => $index,
                                                'fila_fallo_mensaje' => 'No se pudo guardar revisala, debe tener alguna celda mal configurada',
                                                'fila_fallo_session_id' => $_SESSION['idSession'],
                                                'fila_fallo_texto_error_v' => $respuesta,
                                                'fila_fallo_cedula' => $valor1,
                                            );

                                            $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);
                                        }
                                    }else{
                                        $incapacidadesExistentes++;
                                        $datos  = array(   
                                                'filas_fallo_fila' => $index,
                                                'fila_fallo_mensaje' => 'Cedula '.$valor1.' y fecha inicio '.$valor2.' es de una incapacidad ya guardada',
                                                'fila_fallo_cedula' => $valor1,
                                                'fila_fallo_session_id' => $_SESSION['idSession']
                                            );

                                        $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);    
                                    }
                                    
                                }   
                            }else{
                                $fallos++;
                            }
                        }
                    }
                }else{
                    $vacios++;
                    if($vacios> 2){
                        break;
                    }
                }
        
            }
        }


        $check = 0;

        if(isset($_POST['chekUpdate']) && $_POST['chekUpdate'] != 0){
        	$check = 1;
        }

        ControladorPlantilla::setAuditoria('ControladorIncapacidades', 'SoloCargarIncapacidades', 'Archivo Excel', json_encode(array('total' => $total , 'exito' => $aciertos , 'fallaron' => $fallos, 'totalesAciertosNo' => $incapacidadesExistentes, 'check' => $check)) );

        echo json_encode(array('total' => $total , 'exito' => $aciertos , 'fallaron' => $fallos, 'totalesAciertosNo' => $incapacidadesExistentes, 'check' => $check));
        
	}
?>