<?php
	session_start();
    $_SESSION['start'] = time();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
    require_once '../modelos/dao.modelo.php';
    require_once '../controladores/plantilla.controlador.php';

  function auditorias($request, $response)
  {
      ControladorPlantilla::setAuditoria('ControladorPlantilla', 'ImportarEmpleadosPython', $request, $response);
  }
	if(isset($_FILES['NuevoEmpleados']['tmp_name']) && !empty($_FILES['NuevoEmpleados']['tmp_name']) ){

    $aleatorio = mt_rand(100, 999);
    $ruta =  __DIR__."/../BdPython/".$aleatorio.".xlsx";
		copy($_FILES['NuevoEmpleados']['tmp_name'], $ruta);
    $check = 0;
    if(isset($_POST['chekUpdateDatos'])){
      $check = 1;
    }
    $request = 'python3 /var/www/html/BdPython/importarEmpleados.py "'.$aleatorio.".xlsx".'" "'.$_SESSION['cliente_id'].'" "'.$check.'"';
    $result = shell_exec($request);
    auditorias($request, "El servicio se ejecuto correctamente");
    if (unlink($ruta)) {
      // file was successfully deleted
    } else {
    }
    echo json_encode(array('code' => 1, 'message' => 'proceso Ejecutado, revisar por favor'));

	}

  