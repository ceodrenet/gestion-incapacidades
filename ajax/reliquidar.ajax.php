<?php 
	session_start();
	$_SESSION['start'] = time();
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
    require_once '../controladores/incapacidades.controlador.php';
	require_once '../controladores/empleados.controlador.php';
	require_once '../controladores/clientes.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/incapacidades.modelo.php';
	require_once '../modelos/empleados.modelo.php';
	require_once '../modelos/tesoreria.modelo.php';
	require_once '../modelos/clientes.modelo.php';

	if(isset($_GET['liquidarIncapacidad']) && $_GET['liquidarIncapacidad'] != ''){
		
		$campos = "inc_origen, inc_clasificacion, inc_fecha_inicio, inc_fecha_final, inc_emd_id, inc_empresa, inc_id";
		$tabla = "gi_incapacidad";
		$where = "inc_empresa = ".$_GET['empresa'];
		$repuesta = ModeloIncapacidades::mdlMostrarGroupAndOrder($campos,$tabla, $where, null,  null, null);
		$exito = 0;
		$incapacidades = 0;
		$fallos = 0;
		foreach($repuesta as $key => $value){
			$respuestaEmpleado = ControladorEmpleados::ctrMostrarEmpleados('emd_id', $value['inc_emd_id']);
			$jsonArray = array();						
			if($respuestaEmpleado != null){
				/*Liquidacion Automatica*/
				$jsonArray['code'] = 1;
				$anho = explode('-', $value['inc_fecha_inicio'])[0];
				$strCampo = "min_valor_d";
				$strTabla = "gi_minimo_vigente";
				$strWhere = "min_vigencia_d = ".$anho;
				$respiest = ModeloTesoreria::mdlMostrarUnitario($strCampo , $strTabla, $strWhere);
				//print_r($respiest);
				
				$diasIncapacidad = ControladorIncapacidades::dias_transcurridos($value['inc_fecha_inicio'], $value['inc_fecha_final']);

				if($value['inc_clasificacion'] == 1){
					/*Inicial, Valor_empresa*/
					if($value['inc_origen'] == 'ENFERMEDAD GENERAL' || $value['inc_origen'] == 'ACCIDENTE TRANSITO'){

						/*Obtenemos el valor del dia del empleado*/
						$valorPromedio = $respuestaEmpleado['emd_salario_promedio'] * 66.67 / 100;
						$valorDelDia = $valorPromedio / 30;
						$valorDelDiaMin = $respiest['min_valor_d'] / 30;
						//$valorDelDiaMin = str_replace('.', '', $valorDelDiaMin);
						if($valorDelDia < $valorDelDiaMin){
							$valorDelDia = $valorDelDiaMin;
						}

						if($diasIncapacidad < 3){
							/*Toda para la empresa*/
							$valorIncapacidadEmpresa = $diasIncapacidad * $valorDelDia;
							$jsonArray['tipo'] = 1;
							$jsonArray['valE'] = round($valorIncapacidadEmpresa);
							$jsonArray['valI'] = 0;
						}else{
							/*2 dias para la empresa y el resto a la administradora*/
							$diasAdministradora = ($diasIncapacidad - 2);
							$valorIncapacidadEmpresa = 2 * $valorDelDia;
							$valorIncapacidadAdministradora = $diasAdministradora * $valorDelDia;
						
							$jsonArray['tipo'] = 2;
							$jsonArray['valE'] = round($valorIncapacidadEmpresa);
							$jsonArray['valI'] = round($valorIncapacidadAdministradora);
						}
					}else if($value['inc_origen'] == 'ACCIDENTE LABORAL'){

						$valorPromedio = $respuestaEmpleado['emd_salario_promedio'];
						$valorDelDia = $valorPromedio / 30;

						if($diasIncapacidad < 2){
							/*Toda para la empresa*/
							$valorIncapacidadEmpresa = $diasIncapacidad * $valorDelDia;
							$jsonArray['tipo'] = 1;
							$jsonArray['valE'] = round($valorIncapacidadEmpresa);
							$jsonArray['valI'] = 0;
						}else{
							/*1 dias para la empresa y el resto a la administradora*/
							$diasAdministradora = ($diasIncapacidad - 1);
							$valorIncapacidadEmpresa = 1 * $valorDelDia;
							$valorIncapacidadAdministradora = $diasAdministradora * $valorDelDia;
						
							$jsonArray['tipo'] = 2;
							$jsonArray['valE'] = round($valorIncapacidadEmpresa);
							$jsonArray['valI'] = round($valorIncapacidadAdministradora);
						}
					}else if($value['inc_origen'] == 'LICENCIA DE MATERNIDAD' || $value['inc_origen'] == 'LICENCIA DE PATERNIDAD'){


						$valorPromedio = $respuestaEmpleado['emd_salario_promedio'];
						$valorDelDia = $valorPromedio / 30;
						
						/*Todo a la administradora*/
						$valorIncapacidadAdministradora = $diasIncapacidad * $valorDelDia;
					
						$jsonArray['tipo'] = 2;
						$jsonArray['valE'] = 0;
						$jsonArray['valI'] = round($valorIncapacidadAdministradora);
						
					}
					
				}else{
					/*Prorrogas, Valor_administradora*/
					if($value['inc_clasificacion'] != 2){

						$valorPromedio = $respuestaEmpleado['emd_salario_promedio'] * 50 / 100;
						$valorDelDia = $valorPromedio / 30;
						$valorDelDiaMin = $respiest['min_valor_d'] / 30;
						//$valorDelDiaMin = str_replace('.', '', $valorDelDiaMin);
						if($valorDelDia < $valorDelDiaMin){
							$valorDelDia = $valorDelDiaMin;
						}


						$valorIncapacidadAdministradora = $diasIncapacidad * $valorDelDia;
						$jsonArray['tipo'] = 2;
						$jsonArray['valE'] = 0;
						$jsonArray['valI'] = round($valorIncapacidadAdministradora);	

					}else{

						$valorPromedio = $respuestaEmpleado['emd_salario_promedio'] * 66.67 / 100;
						$valorDelDia = $valorPromedio / 30;
						$valorDelDiaMin = $respiest['min_valor_d'] / 30;
						//$valorDelDiaMin = str_replace('.', '', $valorDelDiaMin);
						if($valorDelDia < $valorDelDiaMin){
							$valorDelDia = $valorDelDiaMin;
						}


						$valorIncapacidadAdministradora = $diasIncapacidad * $valorDelDia;
						$jsonArray['tipo'] = 2;
						$jsonArray['valE'] = 0;
						$jsonArray['valI'] = round($valorIncapacidadAdministradora);	
					}
					
				}	

				//Actualizamos las incapacidades
				$campos_ = "inc_valor_pagado_empresa = '".$jsonArray['valE']."' , inc_valor_pagado_eps = '".$jsonArray['valI']."'";
				$tabla_ = "gi_incapacidad";
				$condiciones_ = "inc_id = ".$value['inc_id'];
				$update = ModeloIncapacidades::mdlEditar($tabla_, $campos_, $condiciones_);
				if($update == 'ok'){
					$exito++;
				}else{
					$fallos++;
				}
				$incapacidades++;

			}else{
				$jsonArray['code'] = 0;
				$jsonArray['message'] = 'No existe el empleado';
			}
		}

		echo json_encode(array("code" => 1, "message" => "proceso terminado", "Incapacidades" => $incapacidades, "Actualizadas" => $exito, "No Actualizadas" => $fallos));
	}