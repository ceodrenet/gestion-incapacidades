<?php
    session_start();
    $_SESSION['start'] = time();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
    require_once '../controladores/incapacidades.controlador.php';
    require_once '../controladores/tesoreria.controlador.php';
    
    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';

?>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <div class="col-lg-6 col-xs-12">
            <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            <?php
                                $item = null;
                                $valor = null;
                                if($_SESSION['cliente_id'] != 0){
                                    $item = 'inc_empresa';
                                    $valor = $_SESSION['cliente_id'];
                                    $pagadas = ControladorTesoreria::ctrVerReportesTesoreria($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal'], 'mostrarGeneralPagos_byCliente');
                                }else{
                                    $pagadas = ControladorTesoreria::ctrVerReportesTesoreria($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal'], 'mostrarGeneralPagos');
                                }

                                echo count($pagadas);
                            ?>
                        </h3>
                        <p>PAGADAS</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-done-all"></i>
                    </div>
                    <a href="incapacidades" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-xs-12">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            <?php
                                $item = null;
                                $valor = null;
                                if($_SESSION['cliente_id'] != 0){
                                    $item = 'inc_empresa';
                                    $valor = $_SESSION['cliente_id']; 
                                }

                                $pagadas = ControladorTesoreria::ctrVerReportesTesoreria($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal'], 'totalValorIncapacidades');
                                echo number_format($pagadas['total'], 0, ',', '.');
                            ?>
                        </h3>
                        <p>VALOR PAGADO</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-cash"></i>
                    </div>
                    <a href="incapacidades" class="small-box-footer">Mas Información <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>  
    </div>
    <div class="col-md-7">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Grafico de pagos por EPS</h3>
            </div>
            <div class="box-body">
                <canvas id="myChart" style="height:250px"></canvas>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Registro de pagos periodo consultado entre <?php echo  $_POST['fechaInicial']; ?> y <?php echo $_POST['fechaFinal']; ?></h3>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-hover table-condensed tblGeneralPagos" style="width: 100%;">
                    <thead>
                        <tr>
                            <th style="width: 10px;">#</th>
                            <?php 
                                if($_SESSION['cliente_id'] == 0){ 
                                    echo '<th>EMPRESA</th>';
                                }
                            ?>
                            <th>ADMINISTRADORA</th>
                            <th>VALOR PAGADO</th>
                            <th>FECHA DE PAGO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $where = '';
                            $totalPagado = 0;
                            if($_SESSION['cliente_id'] != 0){
                                $where = 'inc_empresa = '.$_SESSION['cliente_id']." AND ";
                                //$pagadas = ControladorTesoreria::ctrMostrarGeneralPagos_byCliente($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                            }/*else{
                                $pagadas = ControladorTesoreria::ctrMostrarGeneralPagos($item, $valor, $_POST['fechaInicial'], $_POST['fechaFinal']);
                            }*/

                            $campos = "SUM(inc_valor) as total, ips_nombre, inc_fecha_pago, emp_nombre";
                            $tablas = "reporte_tesoreria";
                            $where_ = $where." inc_fecha_pago between '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_clasificacion != '4' AND inc_origen != 'ACCIDENTE LABORAL' ";
                            $pagadasIps = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tablas,$where_, 'group by ips_nombre, inc_fecha_pago', 'ORDER By inc_fecha_pago DESC');

                            foreach ($pagadasIps as $key => $value) {
                                echo '<tr>';
                                    echo '<td>'.($key+1).'</td>';
                                    if($_SESSION['cliente_id'] == 0){ 
                                        echo '<td>'.$value['emp_nombre'].'</td>';
                                    }
                                
                                    echo '<td class="text-uppercase">'.($value["ips_nombre"]).'</td>'; 
                                    if(!empty($value['total']) && $value['total'] != ''){
                                        echo '<td>$ '.number_format($value['total'], 0, ',', '.').'</td>';
                                        $totalPagado += $value['total'];
                                    }else{
                                        echo '<td></td>';
                                    }

                                    $fechaString = explode('-', $value['inc_fecha_pago']);
                                    echo '<td>'.$fechaString[2].'/'.$fechaString[1].'/'.$fechaString[0].'</td>';
                                echo '</tr>';

                            }

                            /*ARL*/
                            $campos = "SUM(inc_valor) as total, arl_nombre, inc_fecha_pago, emp_nombre";
                            $tablas = "reporte_tesoreria";
                            $where_ = $where." inc_fecha_pago between '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_clasificacion != '4' AND inc_origen = 'ACCIDENTE LABORAL' ";
                            $pagadasArl = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tablas,$where_, 'group by arl_nombre, inc_fecha_pago', 'ORDER By inc_fecha_pago DESC');
                            
                            foreach ($pagadasArl as $key => $value) {
                                echo '<tr>';
                                    echo '<td>'.($key+1).'</td>';
                                    if($_SESSION['cliente_id'] == 0){ 
                                        echo '<td>'.$value['emp_nombre'].'</td>';
                                    }
                                
                                    echo '<td class="text-uppercase">'.($value["arl_nombre"]).'</td>'; 
                                    if(!empty($value['total']) && $value['total'] != ''){
                                        echo '<td>$ '.number_format($value['total'], 0, ',', '.').'</td>';
                                        $totalPagado += $value['total'];
                                    }else{
                                        echo '<td></td>';
                                    }
                                    $fechaString = explode('-', $value['inc_fecha_pago']);
                                    echo '<td>'.$fechaString[2].'/'.$fechaString[1].'/'.$fechaString[0].'</td>';
                                echo '</tr>';

                            }

                            /*AFP*/
                            $campos = "SUM(inc_valor) as total, afp_nombre, inc_fecha_pago, emp_nombre";
                            $tablas = "reporte_tesoreria";
                            $where_ = $where." inc_fecha_pago between '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_clasificacion = '4' ";
                            $pagadasAfp = ModeloTesoreria::mdlMostrarGroupAndOrder($campos,$tablas,$where_, 'group by afp_nombre, inc_fecha_pago', 'ORDER By inc_fecha_pago DESC');
                            
                            foreach ($pagadasAfp as $key => $value) {
                                echo '<tr>';
                                    echo '<td>'.($key+1).'</td>';
                                    if($_SESSION['cliente_id'] == 0){ 
                                        echo '<td>'.$value['emp_nombre'].'</td>';
                                    }
                                
                                    echo '<td class="text-uppercase">'.($value["afp_nombre"]).'</td>'; 
                                    if(!empty($value['total']) && $value['total'] != ''){
                                        echo '<td>$ '.number_format($value['total'], 0, ',', '.').'</td>';
                                        $totalPagado += $value['total'];
                                    }else{
                                        echo '<td></td>';
                                    }
                                    $fechaString = explode('-', $value['inc_fecha_pago']);
                                    echo '<td>'.$fechaString[2].'/'.$fechaString[1].'/'.$fechaString[0].'</td>';
                                echo '</tr>';

                            }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th style="width: 10px;">#</th>
                            <?php 
                                if($_SESSION['cliente_id'] == 0){ 
                                    echo '<th></th>';
                                }
                            ?>
                            <th style="text-align: right;">TOTAL</th>
                            <th><?php echo number_format($totalPagado, 0, '.', '.');?></th>
                            <th>FECHA DE PAGO</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
   

<script type="text/javascript">
    $(function(){
        crearBarrasporPagar();

        $('.tblGeneralPagos').DataTable({
            "lengthMenu": [
                [10, 25, 50, 100, 200, -1], 
                [10, 25, 50, 100, 200, "Todos"]
            ],
            "language" : {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }

        });
    });

function crearBarrasporPagar(){
    <?php
        /*Obtenemos las afp porque son por clasificacion +180 */        
        $campo = 'SUM(inc_valor) as total, afp_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = 'inc_clasificacion = 4 ';
        if($_SESSION['cliente_id'] != 0){
            $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }else{
            $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY afp_nombre', '');
        
        $label = '';
        $Qdias = '';
        $colors = '';
        foreach ($respuesta as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */
            if($value['afp_nombre'] != null){
                $label .= "\"".$value['afp_nombre']."\" , ";
                $Qdias .= $value['total']." , ";
                $colors .= "getRandomColor(),";
            }
            
        }

        /*Obtenemos las ARL porque son por accidente de transito*/
        $campo = 'SUM(inc_valor) as total, arl_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = "inc_clasificacion != 4 AND inc_origen = 'ACCIDENTE LABORAL' ";
        if($_SESSION['cliente_id'] != 0){
            $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }else{
            $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $respuesta2 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY arl_nombre', '');
        foreach ($respuesta2 as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */
            if($value['arl_nombre'] != null){
                $label .= "\"".$value['arl_nombre']."\" , ";
                $Qdias .= $value['total']." , ";
                $colors .= "getRandomColor(),";
            }
            
        }

        /*Obtenemos las EPS porque son por accidente de transito*/
        $campo = 'SUM(inc_valor) as total, ips_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = "inc_clasificacion != 4 AND inc_origen != 'ACCIDENTE LABORAL' ";
        if($_SESSION['cliente_id'] != 0){
            $condicion .= 'AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' ";
        }else{
            $condicion .= "AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $respuesta3 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY ips_nombre', '');
        foreach ($respuesta3 as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */
            if($value['ips_nombre'] != null){
                $label .= "\"".$value['ips_nombre']."\" , ";
                $Qdias .= $value['total']." , ";
                $colors .= "getRandomColor(),";
            }
            
        }


    ?>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php echo $label; ?>],
            datasets: [
                {
                    label: 'Valor pagado por Administradora',
                    data: [<?php echo $Qdias; ?>],
                    backgroundColor: [<?php echo $colors; ?>]
                }
            ]
        },
        options: {
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        return "$" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                        });
                    }
                }
            }
        }
    });
}

function scaleLabel (valuePayload) {
    return Number(valuePayload.value).toFixed(2).replace('.',',') + '$';
}

function getRandomColor() {
   return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
}
</script>

