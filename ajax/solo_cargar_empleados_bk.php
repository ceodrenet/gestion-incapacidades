<?php
	session_start();
    $_SESSION['start'] = time();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/incapacidades.controlador.php';
	require_once '../controladores/ips.controlador.php';
    require_once '../controladores/empleados.controlador.php';
    require_once '../modelos/dao.modelo.php';
	require_once '../modelos/incapacidades.modelo.php';
	require_once '../modelos/empleados.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';
    require_once '../modelos/ips.modelo.php';
	require_once '../extenciones/Excel.php';


	if(isset($_FILES['NuevoEmpleados']['tmp_name']) && !empty($_FILES['NuevoEmpleados']['tmp_name']) ){
		$name   = $_FILES['NuevoEmpleados']['name'];
		$tname  = $_FILES['NuevoEmpleados']['tmp_name'];
		$empresa = $_POST['empresa'];
		ini_set('memory_limit','128M');


		$obj_excel = PHPExcel_IOFactory::load($tname);
		$sheetData = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
        $arr_datos = array();
        $highestColumm = $obj_excel->setActiveSheetIndex(0)->getHighestColumn(); // e.g. "EL"
		$highestRow = $obj_excel->setActiveSheetIndex(0)->getHighestRow();
		
		$aciertos = 0;
		$fallos   = 0;
		$total = 0;
		$existentes = 0;
		$badFormat = 0;
		foreach ($sheetData as $index => $value) {
    		if ( $index > 1 ){
    			/* si es 1 es porque esta el formato de empleados */
    			
    			if($value['A'] == 1){
    				$valido = 0;
        			$existe = false;
        			$separador = '';
        			$total++;
                    $res = 'NULL';
    				if((!is_null($value['A']) && !empty($value['A'])) && 
                		(!is_null($value['B']) && !empty($value['B'])) && 
                		(!is_null($value['C']) && !empty($value['C']))
            		){
            			/* Iniciamos la consulta Sql */
            			$datos = array();

            			/* tipo de identificacion */
            			if(!is_null($value['B']) && !empty($value['B']) ){
            				$valido = 1;
            				$datos['emd_tipo_identificacion'] = $value['B'];
            			}else{
            				$datos['emd_tipo_identificacion'] = 'NULL';
            			}

            			/* Identificacion */

            			if(!is_null($value['C']) && !empty($value['C']) ){
            				$valido = 1;
            				$datos['emd_cedula'] = $value['C'];	

            				$item = 'emd_cedula';
            				$valuess = $value['C'];
            				$tabla = 'gi_empleados';
            				$value2 = $empresa;
            				$res = ModeloEmpleados::mdlMostrarEmpleadosX2($tabla, $item, $valuess, $value2);
            				if($res){
            					if($res['emd_cedula'] == $value['C']){
                					$existe = true;
                					$existentes++;
                					$datos['emd_id'] = $res['emd_id'];
                				}	
            				}
            				
            			}else{
            				$datos['emd_cedula'] = 'NULL';
            			}

            			/* Nombres */
            			if(!is_null($value['D']) && !empty($value['D']) ){
            				$valido = 1;
                            $nombre = str_replace("\n", "", $value['D']);                          
            				$datos['emd_nombre'] = $nombre;
            			}else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_nombre'] = $res['emd_nombre'];
                            }else{
                                $datos['emd_nombre'] = 'NULL'; 
                            }
            			}

            			/* Fecha Nacimiento */
            			if(!is_null($value['E']) && !empty($value['E']) ){
            				$datos['emd_fecha_nacimiento'] = $value['E'];
            				$valido = 1;
            			}else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_fecha_nacimiento'] = $res['emd_fecha_nacimiento'];
                            }else{
                                $datos['emd_fecha_nacimiento'] = 'NULL'; 
                            }
            			}

            			/* Fecha ingreso */
            			if(!is_null($value['F']) && !empty($value['F']) ){
            				$datos['emd_fecha_ingreso'] = $value['F'];
            				$valido = 1;
            			}else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_fecha_ingreso'] = $res['emd_fecha_ingreso'];
                            }else{
                                $datos['emd_fecha_ingreso'] = 'NULL'; 
                            }
            			}

            			/* Feccha de retiro */
            			if(!is_null($value['G']) && !empty($value['G']) ){
            				$valido = 1;
            				$datos['emd_fecha_retiro'] = $value['G'];
            			}else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_fecha_retiro'] = $res['emd_fecha_retiro'];
                            }else{
                                $datos['emd_fecha_retiro'] = 'NULL'; 
                            }
            			}

            			/* Salario Base */
            			if(!is_null($value['H']) && !empty($value['H']) ){
            				$valido = 1;
            				$datos['emd_salario'] = $value['H'];
            			}else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_salario'] = $res['emd_salario'];
                            }else{
                                $datos['emd_salario'] = 'NULL'; 
                            }
            			}

            			/* Salario Promedio */
            			if(!is_null($value['I']) && !empty($value['I']) ){
            				$valido = 1;
            				$datos['emd_salario_promedio'] = $value['I'];
            			}else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_salario_promedio'] = $res['emd_salario_promedio'];
                            }else{
                                $datos['emd_salario_promedio'] = 'NULL'; 
                            }
            			}

            			/* Cargo */
            			if(!is_null($value['J']) && !empty($value['J']) ){
            				$valido = 1;
            				$datos['emd_cargo'] = $value['J'];
            			}else{
            				if($res != 'NULL' && $res != false){
                                $datos['emd_cargo'] = $res['emd_cargo'];
                            }else{
                                $datos['emd_cargo'] = 'NULL'; 
                            }
            			}

            			/* Sede */
            			if(!is_null($value['K']) && !empty($value['K']) ){
            				$valido = 1;
            				$datos['emd_sede'] = $value['K'];
            			}else{
            				if($res != 'NULL' && $res != false){
                                $datos['emd_sede'] = $res['emd_sede'];
                            }else{
                                $datos['emd_sede'] = 'NULL'; 
                            }
            			}

            			/* EPS */
            			if(!is_null($value['L']) && !empty($value['L']) ){
            				$valido = 1;
            				$item = 'ips_codigo';
            				$valuess = $value['L'];
            				$resp = ControladorIps::ctrMostrarIpsById($item, $valuess);
            				if($resp){
            					$datos['emd_eps_id'] = $resp['ips_id'];
            				}else{
            					$datos['emd_eps_id'] = 'NULL';	
            				}
            			}else{
            				if($res != 'NULL' && $res != false){
                                $datos['emd_eps_id'] = $res['emd_eps_id'];
                            }else{
                                $datos['emd_eps_id'] = 'NULL'; 
                            }
            			}

            			/* Feccha Ingreso EPS */
            			if(!is_null($value['M']) && !empty($value['M']) ){
            				$valido = 1;
            				$datos['emd_fecha_afiliacion_eps'] = $value['M'];
            			}else{
            				if($res != 'NULL' && $res != false){
                                $datos['emd_fecha_afiliacion_eps'] = $res['emd_fecha_afiliacion_eps'];
                            }else{
                                $datos['emd_fecha_afiliacion_eps'] = 'NULL'; 
                            }
            			}

            			/* AFP */
            			if(!is_null($value['N']) && !empty($value['N']) ){
            				$valido = 1;
            				$item = 'ips_codigo';
            				$valuess = $value['N'];
            				$resp = ControladorIps::ctrMostrarIpsById($item, $valuess);
            				if($resp){
            					$datos['emd_afp_id'] = $resp['ips_id'];
            				}else{
            					$datos['emd_afp_id'] = 'NULL';	
            				}
            			}else{
            				if($res != 'NULL' && $res != false){
                                $datos['emd_afp_id'] = $res['emd_afp_id'];
                            }else{
                                $datos['emd_afp_id'] = 'NULL'; 
                            }
            			}

            			/*ARL */
            			if(!is_null($value['O']) && !empty($value['O']) ){
            				$valido = 1;
            				$item = 'ips_codigo';
            				$valuess = $value['O'];
            				$resp = ControladorIps::ctrMostrarIpsById($item, $valuess);
            				if($resp){
            					$datos['emd_arl_id'] = $resp['ips_id'];
            				}else{
            					$datos['emd_arl_id'] = 'NULL';	
            				}
            			}else{
            				if($res != 'NULL' && $res != false){
                                $datos['emd_arl_id'] = $res['emd_arl_id'];
                            }else{
                                $datos['emd_arl_id'] = 'NULL'; 
                            }
            			}

            			/*Genero*/
            			if(!is_null($value['P']) && !empty($value['P']) ){
            				$valido = 1;
            				$datos['emd_genero'] = strtoupper($value['P']);
            			}else{
            				if($res != 'NULL' && $res != false){
                                $datos['emd_genero'] = $res['emd_genero'];
                            }else{
                                $datos['emd_genero'] = 'NULL'; 
                            }
            			}

            			/*Tipo de empleados*/
            			if(!is_null($value['Q']) && !empty($value['Q']) ){
            				$valido = 1;
            				$datos['emd_tipo_empleado'] = $value['Q'];
            			}else{
            				if($res != 'NULL' && $res != false){
                                $datos['emd_tipo_empleado'] = $res['emd_tipo_empleado'];
                            }else{
                                $datos['emd_tipo_empleado'] = 'NULL'; 
                            }
            			}

            			/*Tipo de empleados*/
            			if(!is_null($value['R']) && !empty($value['R']) ){
            				$valido = 1;
            				$datos['emd_codigo_nomina'] = $value['R'];
            			}else{
            				if($res != 'NULL' && $res != false){
                                $datos['emd_codigo_nomina'] = $res['emd_codigo_nomina'];
                            }else{
                                $datos['emd_codigo_nomina'] = 'NULL'; 
                            }
            			}

            			/*Centro de Costos*/
            			if(!is_null($value['S']) && !empty($value['S']) ){
            				$valido = 1;
            				$datos['emd_centro_costos'] = $value['S'];
            			}else{
            				if($res != 'NULL' && $res != false){
                                $datos['emd_centro_costos'] = $res['emd_centro_costos'];
                            }else{
                                $datos['emd_centro_costos'] = 'NULL'; 
                            }
            			}

            			/*Tipo de empleados*/
            			if(!is_null($value['T']) && !empty($value['T']) ){
            				$valido = 1;
            				$datos['emd_ciudad'] = $value['T'];
            			}else{
            				if($res != 'NULL' && $res != false){
                                $datos['emd_ciudad'] = $res['emd_ciudad'];
                            }else{
                                $datos['emd_ciudad'] = 'NULL'; 
                            }
            			}

                        /*Sub centro de costo*/
                        if(!is_null($value['U']) && !empty($value['U']) ){
                            $valido = 1;
                            $datos['emd_subcentro_costos'] = $value['U'];
                        }else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_subcentro_costos'] = $res['emd_subcentro_costos'];
                            }else{
                                $datos['emd_subcentro_costos'] = 'NULL'; 
                            }
                        }

                         /*Estado*/
                        if(!is_null($value['V']) && !empty($value['V']) ){
                            $valido = 1;
                            $stado = 0;
                            if($value['V'] == 'ACTIVO'){
                                $stado = 1;
                            }
                            $datos['emd_estado'] = $stado;
                        }else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_estado'] = $res['emd_estado'];
                            }else{
                                $datos['emd_estado'] = 'NULL'; 
                            }
                        }

                        $datos['emd_caso_especial_i'] = 0;
                        $datos['emd_telefono_v'] = 'NULL';
                        $datos['emd_correo_v'] = 0;
                        $datos['emd_susp_salario_i'] = 0;
                        $datos['emd_fech_susp_salario'] = 'NULL';
                        $datos['emd_fecha_calificacion_PCL'] = 'NULL';
                        $datos['emd_entidad_calificadora'] = 'NULL';
                        $datos['emd_diagnostico'] = 'NULL';
                        $datos['emd_ruta_cedula'] = 'NULL';
                        $datos['emd_ruta_otro'] = 'NULL';

            			$tabla = "gi_empleados";							
						if($valido == 1){
							$datos['emd_emp_id'] = $empresa;
                            $respuesta = null;
							if(!$existe){
                                if(isset($_POST['chekUpdateDatos'])){
                                    $campos = 'emd_nombre, emd_cedula, emd_emp_id, emd_fecha_ingreso, emd_salario, emd_tipo_identificacion, emd_fecha_retiro, emd_salario_promedio, emd_fecha_nacimiento, emd_eps_id, emd_fecha_afiliacion_eps, emd_afp_id, emd_arl_id, emd_fecha_calificacion_PCL, emd_entidad_calificadora, emd_diagnostico, emd_cargo, emd_sede, emd_ruta_cedula, emd_ruta_otro, emd_genero, emd_tipo_empleado, emd_codigo_nomina, emd_centro_costos, emd_ciudad, emd_subcentro_costos, emd_estado,emd_telefono_v, emd_correo_v,emd_susp_salario_i,emd_fech_susp_salario';
                                    $valores = "'".$datos['emd_nombre']."','".$datos['emd_cedula']."','".$datos['emd_emp_id']."','".$datos['emd_fecha_ingreso']."','".$datos['emd_salario']."','".$datos['emd_tipo_identificacion']."','".$datos['emd_fecha_retiro']."','".$datos['emd_salario_promedio']."','".$datos['emd_fecha_nacimiento']."','".$datos['emd_eps_id']."','".$datos['emd_fecha_afiliacion_eps']."','".$datos['emd_afp_id']."','".$datos['emd_arl_id']."','".$datos['emd_fecha_calificacion_PCL']."','".$datos['emd_entidad_calificadora']."','".$datos['emd_diagnostico']."','".$datos['emd_cargo']."','".$datos['emd_sede']."','".$datos['emd_ruta_cedula']."','".$datos['emd_ruta_otro']."','".$datos['emd_genero']."','".$datos['emd_tipo_empleado']."','".$datos['emd_codigo_nomina']."','".$datos['emd_centro_costos']."','".$datos['emd_ciudad']."','".$datos['emd_subcentro_costos']."','".$datos['emd_estado']."','".$datos['emd_telefono_v']."','".$datos['emd_correo_v']."','".$datos['emd_susp_salario_i']."','".$datos['emd_fech_susp_salario']."'"; 
                                    $respuesta = ModeloEmpleados::mdlCrear('gi_empleados', $campos, $valores);
                                }
                               
							}else{
								$respuesta = ModeloEmpleados::mdlEditarEmpleados($tabla, $datos);
							}
							
							if($respuesta == 'ok'){
								$aciertos++;
							}else{
								$fallos++;
							}
						}
            		}
    			}
        	}
        }

        echo json_encode(array('total' => $total , 'exito' => $aciertos , 'fallaron' => $fallos, 'existentes' => $existentes));
        
	}

     if(isset($_FILES['IblEmpleados']['tmp_name']) && !empty($_FILES['IblEmpleados']['tmp_name']) ){
        $name   = $_FILES['IblEmpleados']['name'];
        $tname  = $_FILES['IblEmpleados']['tmp_name'];
        $empresa = $_POST['empresa'];
        ini_set('memory_limit','128M');

        $obj_excel = PHPExcel_IOFactory::load($tname);
        $sheetData = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
        $arr_datos = array();
        $highestColumm = $obj_excel->setActiveSheetIndex(0)->getHighestColumn(); // e.g. "EL"
        $highestRow = $obj_excel->setActiveSheetIndex(0)->getHighestRow();
        
        $aciertos = 0;
        $fallos   = 0;
        $total = 0;
        $existentes = 0;
        $Noexiste = 0;
        $badFormat = 0;
        foreach ($sheetData as $index => $value) {
            if ( $index > 1 ){
                /* si es 1 es porque esta el formato de empleados */
                
                if($value['A'] == 1){
                    $valido = 0;
                    $existe = false;
                    $separador = '';
                    $total++;
                    $res = 'NULL';
                    if((!is_null($value['A']) && !empty($value['A'])) ){
                        /* Iniciamos la consulta Sql */
                        $datos = array();

                        /* Identificacion */

                        if(!is_null($value['A']) && !empty($value['A']) ){
                            $valido = 1;
                            $datos['emd_cedula'] = $value['A']; 

                            $item = 'emd_cedula';
                            $valuess = $value['A'];
                            $tabla = 'gi_empleados';
                            $value2 = $empresa;
                            $res = ModeloEmpleados::mdlMostrarEmpleadosX2($tabla, $item, $valuess, $value2);
                            if($res){
                                if($res['emd_cedula'] == $value['A']){
                                    $existe = true;
                                    $existentes++;
                                    $datos['emd_id'] = $res['emd_id'];
                                }   
                            }else{
                                $Noexiste++;
                            }
                            
                        }else{
                            $datos['emd_cedula'] = 'NULL';
                        }


                        if(!is_null($value['C']) && !empty($value['C']) ){
                            $valido = 1;
                            $datos['emd_salario_promedio'] = $value['C'];
                        }else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_salario_promedio'] = $res['emd_salario_promedio'];
                            }else{
                                $datos['emd_salario_promedio'] = 'NULL'; 
                            }
                        }


                        if(!is_null($value['D']) && !empty($value['D']) ){
                            $valido = 1;
                            $datos['emd_salario'] = $value['D'];
                        }else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_salario'] = $res['emd_salario'];
                            }else{
                                $datos['emd_salario'] = 'NULL'; 
                            }
                        }

                        if(!is_null($value['E']) && !empty($value['E']) ){
                            $valido = 1;
                            $item = 'ips_codigo';
                            $valuess = $value['E'];
                            $resp = ControladorIps::ctrMostrarIpsById($item, $valuess);
                            if($resp){
                                $datos['emd_eps_id'] = $resp['ips_id'];
                            }else{
                                $datos['emd_eps_id'] = 'NULL';  
                            }
                        }else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_eps_id'] = $res['emd_eps_id'];
                            }else{
                                $datos['emd_eps_id'] = 'NULL'; 
                            }
                        }

                        /* Feccha Ingreso EPS */
                        if(!is_null($value['F']) && !empty($value['F']) ){
                            $valido = 1;
                            $datos['emd_fecha_afiliacion_eps'] = $value['F'];
                        }else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_fecha_afiliacion_eps'] = $res['emd_fecha_afiliacion_eps'];
                            }else{
                                $datos['emd_fecha_afiliacion_eps'] = 'NULL'; 
                            }
                        }

                        /* Feccha Retiro */
                        if(!is_null($value['G']) && !empty($value['G']) ){
                            $valido = 1;
                            $datos['emd_fecha_retiro'] = $value['G'];
                        }else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_fecha_retiro'] = $res['emd_fecha_retiro'];
                            }else{
                                $datos['emd_fecha_retiro'] = 'NULL'; 
                            }
                        }

                        /* Feccha Ingreso EPS */
                        if(!is_null($value['H']) && !empty($value['H']) ){
                            $valido = 1;
                            $stado = 0;
                            if($value['H'] == 'ACTIVO'){
                                $stado = 1;
                            }
                            $datos['emd_estado'] = $stado;
                        }else{
                            if($res != 'NULL' && $res != false){
                                $datos['emd_estado'] = $res['emd_estado'];
                            }else{
                                $datos['emd_estado'] = 'NULL'; 
                            }
                        }

                        if($valido == 1){
                            $datos['emd_emp_id'] = $empresa;

                            $tabla = "gi_empleados";
                            if($existe){
                                $valores = "emd_salario_promedio = '".$datos['emd_salario_promedio']."', emd_salario = '".$datos['emd_salario']."', emd_eps_id = '".$datos['emd_eps_id']."', emd_fecha_afiliacion_eps = '".$datos['emd_fecha_afiliacion_eps']."', emd_fecha_retiro = '".$datos['emd_fecha_retiro']."',  emd_estado = '".$datos['emd_estado']."' ";
                                $respuesta = ModeloEmpleados::mdlEditar($tabla, $valores, "emd_id = ".$res['emd_id']);
                            }
                            
                            if($respuesta == 'ok'){
                                $aciertos++;
                            }else{
                                $fallos++;
                            }
                        }
                    }
                }
            }
        }

        echo json_encode(array('total' => $total , 'exito' => $aciertos , 'fallaron' => $fallos, 'Noexiste' => $Noexiste));
    }
