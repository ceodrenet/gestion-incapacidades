<?php
session_start();
$_SESSION['start'] = time();
ini_set('display_errors', 'On');
ini_set('display_errors', 1);
ini_set('memory_limit', '1650M');
require_once '../controladores/mail.controlador.php';
require_once '../controladores/plantilla.controlador.php';
require_once '../controladores/incapacidades.controlador.php';
require_once '../controladores/tesoreria.controlador.php';

require_once '../modelos/dao.modelo.php';
require_once '../modelos/incapacidades.modelo.php';
require_once '../modelos/tesoreria.modelo.php';
require_once '../vendor/autoload.php';
require_once '../extenciones/Excel.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$months = array(1 => "Enero", 2 => "Febrero", 3 => "Marzo", 4 => "Abril", 5 => "Mayo", 6 => "Junio", 7 => "Julio", 
8 => "Agosto", 9 => "Septiembre", 10 => "Octubre", 11 => "Noviembre", 12 => "Diciembre");

$borders = array(
    'borders' => array(
        'allborders' => array(
            'style' =>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
        )
    ),
);

$objPHPExcel = new Spreadsheet();

$objPHPExcel->getActiveSheet()->setTitle('INFORME');
$objPHPExcel->getActiveSheet()->getStyle('B1:P1')
	->getAlignment()
	->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

//$objPHPExcel->getActiveSheet()->getStyle('A1:M1')->applyFromArray($borders);
if ($_POST["filtroEstadoTramite"] == 0) {
    $objPHPExcel->getActiveSheet()->mergeCells('B1:P1');
    $objPHPExcel->getActiveSheet()->getStyle("B1:P1")->getFont()->setSize(16); 
} else {
    $objPHPExcel->getActiveSheet()->mergeCells('B1:J1');
    $objPHPExcel->getActiveSheet()->getStyle("B1:P1")->getFont()->setSize(16);
}
$objPHPExcel->getActiveSheet()->setCellValue("B1", "REPORTE - INFORME DE SALDOS"); 

$objPHPExcel->getActiveSheet()
    ->getStyle('A1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('B1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

$objPHPExcel->getActiveSheet()->getStyle('B1:AR1')->getFont()->setBold( true );

$objPHPExcel->getActiveSheet()->mergeCells('B2:D2');
$objPHPExcel->getActiveSheet()->getStyle('B2:D2')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->setCellValue("B2", "ESTADO DE INCAPACIDAD"); 

$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
$objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->setCellValue("B3", "Año"); 


$objPHPExcel->getActiveSheet()->mergeCells('B4:D4');
$objPHPExcel->getActiveSheet()->getStyle('B4:D4')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->getStyle('B4:P4')->getFont()->setBold( true );
if ($_POST["yearFilter"] != 0) {
    $objPHPExcel->getActiveSheet()->setCellValue("B4", $_POST["yearFilter"]); 
} else {
    $objPHPExcel->getActiveSheet()->setCellValue("B4", "ACUMULADO"); 
}

$objPHPExcel->getActiveSheet()->getStyle('B4')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$filtroFecha = $_POST['filtroFecha'];
$clienteId = 0;


//validamos el filtro por años, que no sea la opción "TODOS"
if ($_POST['yearFilter'] != 0) {

    //armamos las consultados de forma dinamica para traernos la información segmentada

    $whereTotalPag = "(inc_estado_tramite = 5 OR inc_estado_tramite = 8)";
    $whereTotalPen = "(inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7)";
    $whereTotalSin = "inc_estado_tramite = 6";

    $fieldPagByMes = "MONTH(".$filtroFecha.") mes, SUM(inc_valor) as total, COUNT(*) as cant_mes";
    $wherePagByMes = "MONTH(".$filtroFecha.") BETWEEN 1 AND 12 AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8)";

    $fieldPenByMes = "MONTH(".$filtroFecha.") mes, SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_mes";
    $wherePenByMes = "MONTH(".$filtroFecha.") BETWEEN 1 AND 12 AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7)";

    $fieldSinByMes = "MONTH(".$filtroFecha.") mes, SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_mes";
    $whereSinByMes = "MONTH(".$filtroFecha.") BETWEEN 1 AND 12 AND (inc_estado_tramite = 6)";

    $groupBy = "GROUP BY MONTH(".$filtroFecha.")";
    $orderBy = "ORDER BY MONTH(".$filtroFecha.")";    

    //mostramos los meses
    $i = 5;
    foreach ($months as $key => $value) {
        $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':'.'D'.$i); 
        $objPHPExcel->getActiveSheet()->getStyle('B'.$i)
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value); 
        $i++;
    }

    $fecha1 = $_POST['yearFilter']."-01-01";
    $fecha2 = $_POST['yearFilter']."-12-31";

    $whereTotalPag.=" AND ".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."'";
    $whereTotalPen.=" AND ".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."'";
    $whereTotalSin.=" AND ".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."'";

    $wherePagByMes.=" AND YEAR(".$filtroFecha.") = '".$_POST["yearFilter"]."'";
    $wherePenByMes.=" AND YEAR(".$filtroFecha.") = '".$_POST["yearFilter"]."'";
    $whereSinByMes.=" AND YEAR(".$filtroFecha.") = '".$_POST["yearFilter"]."'";

    //validamos el cliente
    if ($_SESSION['cliente_id'] != 0) {
        $whereTotalPag .= " AND inc_empresa = ".$_SESSION['cliente_id'];
        $whereTotalPen .= " AND inc_empresa = ".$_SESSION['cliente_id'];
        $whereTotalSin .= " AND inc_empresa = ".$_SESSION['cliente_id'];

        $wherePagByMes .= " AND inc_empresa = ".$_SESSION['cliente_id'];
        $wherePenByMes .= " AND inc_empresa = ".$_SESSION['cliente_id'];
        $whereSinByMes .= " AND inc_empresa = ".$_SESSION['cliente_id'];
        $clienteId = $_SESSION['cliente_id'];
    }

    //nos traemos el total de cada estado de tramite
    $totalPagadas = ModeloTesoreria::mdlMostrarUnitario("SUM(inc_valor) as total_pag, COUNT(*) as cantidad", "gi_incapacidad",$whereTotalPag);
    $totalPendientes = ModeloTesoreria::mdlMostrarUnitario("SUM(inc_valor_pagado_eps) as total_pen, COUNT(*) as cantidad", "gi_incapacidad",$whereTotalPen);
    $totalSinReconocimiento = ModeloTesoreria::mdlMostrarUnitario("SUM(inc_valor_pagado_eps) as total_sin, COUNT(*) as cantidad", "gi_incapacidad",$whereTotalSin);

    //nos traemos el valor de cada mes por estado de tramite
    $saldosPagByMes = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldPagByMes,"gi_incapacidad", $wherePagByMes, $groupBy, $orderBy);
    $saldosPenByMes = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldPenByMes,"gi_incapacidad", $wherePenByMes, $groupBy, $orderBy);
    $saldosSinByMes = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldSinByMes,"gi_incapacidad", $whereSinByMes, $groupBy, $orderBy);

    //validamos que estado de tramite se manda desde el front, en este caso se van a exportar todos los estados
    if ($_POST["filtroEstadoTramite"] == 0) {
        $objPHPExcel->getActiveSheet()->mergeCells('E2:G2');
        $objPHPExcel->getActiveSheet()->getStyle('E2:G2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("E2", "PAGADAS");

        $objPHPExcel->getActiveSheet()->setCellValue("E3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("F3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("G3", "% Periodo");

        $objPHPExcel->getActiveSheet()->mergeCells('H2:J2');
        $objPHPExcel->getActiveSheet()->getStyle('H2:J2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("H2", "PENDIENTES");

        $objPHPExcel->getActiveSheet()->setCellValue("H3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("I3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("J3", "% Periodo");

        $objPHPExcel->getActiveSheet()->mergeCells('K2:M2');
        $objPHPExcel->getActiveSheet()->getStyle('K2:M2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("K2", "SIN RECONOCIMIENTO");

        $objPHPExcel->getActiveSheet()->setCellValue("K3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("L3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("M3", "% Periodo");

        $objPHPExcel->getActiveSheet()->mergeCells('N2:P2');
        $objPHPExcel->getActiveSheet()->getStyle('N2:P2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("N2", "TOTAL RECIBIDAS"); 
        $objPHPExcel->getActiveSheet()->setCellValue("N3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("O3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("P3", "% Periodo");
        $objPHPExcel->getActiveSheet()->getStyle('N3:P3')->getFont()->setBold( true );

        $totalMesRecibidas = 0;
        $cantMesRecibidas = 0;

        $i = 5;
        $iPag = 0;
        $iPen = 0;
        $iSin = 0;
        foreach ($months as $key => $mes) {

            $totalPag = 0; $totalPen = 0; $totalSin = 0;
            $cantPag = 0; $cantPen = 0; $cantSin = 0;

            if (isset($saldosPagByMes[$iPag]["mes"])) {
                if ($key == $saldosPagByMes[$iPag]["mes"]) {
                    if (!empty($saldosPagByMes[$iPag]["total"])) {
                        $totalPag = $saldosPagByMes[$iPag]["total"];
                    }
                    if (!empty($saldosPagByMes[$iPag]["cant_mes"])) {
                        $cantPag = $saldosPagByMes[$iPag]["cant_mes"];
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $totalPag);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $cantPag);
                    $iPag++;
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
                }
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
            }

            if (isset($saldosPenByMes[$iPen]["mes"])) {
                if ($key == $saldosPenByMes[$iPen]["mes"]) {
                    if (!empty($saldosPenByMes[$iPen]["total"])) { 
                        $totalPen = $saldosPenByMes[$iPen]["total"];
                    }
                    if (!empty($saldosPenByMes[$iPen]["cant_mes"])) { 
                        $cantPen = $saldosPenByMes[$iPen]["cant_mes"];
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalPen);
                    $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $cantPen);
                    $iPen++;
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("H".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("I".$i, 0);
                }
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue("H".$i, 0);
                $objPHPExcel->getActiveSheet()->setCellValue("I".$i, 0);
            }

            if (isset($saldosSinByMes[$iSin]["mes"])) {
                if ($key == $saldosSinByMes[$iSin]["mes"]) {
                    if (!empty($saldosSinByMes[$iSin]["total"])) {
                        $totalSin = $saldosSinByMes[$iSin]["total"];
                    }
                    if (!empty($saldosSinByMes[$iSin]["cant_mes"])) {
                        $cantSin = $saldosSinByMes[$iSin]["cant_mes"];
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $totalSin);
                    $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $cantSin);
                    $iSin++;
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("K".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("L".$i, 0);
                }
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue("K".$i, 0);
                $objPHPExcel->getActiveSheet()->setCellValue("L".$i, 0);
            }

            $totalMes = $totalPag + $totalPen + $totalSin;
            $totalCant = $cantPag + $cantPen + $cantSin;

            $totalMesRecibidas += $totalMes;
            $cantMesRecibidas += $totalCant;

            $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $totalMes);
            $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $totalCant);

            $porcentajeMesPag = 0; $porcentajeMesPen = 0; $porcentajeMesSin = 0;

            if ($totalMes > 0) {
                $porcentajeMesPag = ($totalPag * 100)/$totalMes;
                $porcentajeMesPen = ($totalPen * 100)/$totalMes;
                $porcentajeMesSin = ($totalSin * 100)/$totalMes;

                $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porcentajeMesPag,2));
                $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($porcentajeMesPen,2));
                $objPHPExcel->getActiveSheet()->setCellValue("M".$i, number_format($porcentajeMesSin,2));
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue("G".$i, 0);
                $objPHPExcel->getActiveSheet()->setCellValue("J".$i, 0);
                $objPHPExcel->getActiveSheet()->setCellValue("M".$i, 0);
            }

            $totalPorcentajeMes = $porcentajeMesPag + $porcentajeMesPen + $porcentajeMesSin;
            
            if ($totalPorcentajeMes > 0) {
                $objPHPExcel->getActiveSheet()->setCellValue("P".$i, number_format($totalPorcentajeMes,2));
            } else {
                $objPHPExcel->getActiveSheet()->setCellValue("P".$i, 0);
            }

            $i++;
        }

        $porcentajePag = 0;
        $porcentajePen = 0;
        $porcentajeSin = 0;
        $porcentajeTotal = 0;

        if ($totalMesRecibidas > 0) {
            $porcentajePag = ($totalPagadas["total_pag"] * 100) / $totalMesRecibidas;
            $porcentajePen = ($totalPendientes["total_pen"] * 100) / $totalMesRecibidas;
            $porcentajeSin = ($totalSinReconocimiento["total_sin"] * 100) / $totalMesRecibidas;
            $porcentajeTotal = $porcentajePag + $porcentajePen + $porcentajeSin;
        }

        $objPHPExcel->getActiveSheet()->setCellValue("E4", $totalPagadas["total_pag"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F4", $totalPagadas["cantidad"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G4", number_format($porcentajePag, 2)."%");

        $objPHPExcel->getActiveSheet()->setCellValue("H4", $totalPendientes["total_pen"]);
        $objPHPExcel->getActiveSheet()->setCellValue("I4", $totalPendientes["cantidad"]);
        $objPHPExcel->getActiveSheet()->setCellValue("J4", number_format($porcentajePen, 2)."%");

        $objPHPExcel->getActiveSheet()->setCellValue("K4", $totalSinReconocimiento["total_sin"]);
        $objPHPExcel->getActiveSheet()->setCellValue("L4", $totalSinReconocimiento["cantidad"]);
        $objPHPExcel->getActiveSheet()->setCellValue("M4", number_format($porcentajePen, 2)."%");

        $objPHPExcel->getActiveSheet()->setCellValue("N4", $totalMesRecibidas);
        $objPHPExcel->getActiveSheet()->setCellValue("O4", $cantMesRecibidas);
        $objPHPExcel->getActiveSheet()->setCellValue("P4", number_format($porcentajeTotal, 2)."%");

        $objPHPExcel->getActiveSheet()->setCellValue("E17", $totalPagadas["total_pag"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F17", $totalPagadas["cantidad"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G17", number_format($porcentajePag, 2)."%");

        $objPHPExcel->getActiveSheet()->setCellValue("H17", $totalPendientes["total_pen"]);
        $objPHPExcel->getActiveSheet()->setCellValue("I17", $totalPendientes["cantidad"]);
        $objPHPExcel->getActiveSheet()->setCellValue("J17", number_format($porcentajePen, 2)."%");

        $objPHPExcel->getActiveSheet()->setCellValue("K17", $totalSinReconocimiento["total_sin"]);
        $objPHPExcel->getActiveSheet()->setCellValue("L17", $totalSinReconocimiento["cantidad"]);
        $objPHPExcel->getActiveSheet()->setCellValue("M17", number_format($porcentajePen, 2)."%");

        $objPHPExcel->getActiveSheet()->setCellValue("N17", $totalMesRecibidas);
        $objPHPExcel->getActiveSheet()->setCellValue("O17", $cantMesRecibidas);
        $objPHPExcel->getActiveSheet()->setCellValue("P17", number_format($porcentajeTotal, 2)."%");

        $objPHPExcel->getActiveSheet()->getStyle('B2:N2')->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle('N5:N16')->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle('O5:O16')->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle('P5:P16')->getFont()->setBold( true );

        $objPHPExcel->getActiveSheet()->getStyle('E4:E17')->getNumberFormat()->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle('H4:H17')->getNumberFormat()->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle('K4:K17')->getNumberFormat()->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle('N4:N17')->getNumberFormat()->setFormatCode('#,##0');

    } else {

        //si entra acá es porque se seleccionó un estado especifico a exportar
        if ($_POST["filtroEstadoTramite"] == 1) {
            $objPHPExcel->getActiveSheet()->mergeCells('E2:G2');
            $objPHPExcel->getActiveSheet()->getStyle('E2:G2')->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->setCellValue("E2", "PAGADAS");

            $objPHPExcel->getActiveSheet()->setCellValue("E3", "Valor ($)");
            $objPHPExcel->getActiveSheet()->setCellValue("F3", "#");
            $objPHPExcel->getActiveSheet()->setCellValue("G3", "% Periodo");

            $objPHPExcel->getActiveSheet()->mergeCells('H2:I2');
            $objPHPExcel->getActiveSheet()->getStyle('H2:I2')->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->setCellValue("H2", "TOTAL RECIBIDAS"); 
            $objPHPExcel->getActiveSheet()->setCellValue("H3", "Valor ($)");
            $objPHPExcel->getActiveSheet()->setCellValue("I3", "#");
            $objPHPExcel->getActiveSheet()->setCellValue("J3", "% Periodo");
            $objPHPExcel->getActiveSheet()->getStyle('H3:J3')->getFont()->setBold( true );

            $totalMesRecibidas = 0;
            $cantMesRecibidas = 0;
            $iPag = 0;
            $iPen = 0;
            $iSin = 0;
            $i = 5;
            foreach ($months as $key => $mes) {

                $totalPag = 0; $totalPen = 0; $totalSin = 0;
                $cantPag = 0; $cantPen = 0; $cantSin = 0;


                if (isset($saldosPagByMes[$iPag]["mes"])) {
                    if ($key == $saldosPagByMes[$iPag]["mes"]) {
                        if (!empty($saldosPagByMes[$iPag]["total"])) {
                            $totalPag = $saldosPagByMes[$iPag]["total"];
                        }
                        if (!empty($saldosPagByMes[$iPag]["cant_mes"])) {
                            $cantPag = $saldosPagByMes[$iPag]["cant_mes"];
                        }
                        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $totalPag);
                        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $cantPag);
                        $iPag++;
                    } else {
                        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
                    }
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
                }

                if (isset($saldosPenByMes[$iPen]["mes"])) {
                    if ($key == $saldosPenByMes[$iPen]["mes"]) {
                        if (!empty($saldosPenByMes[$iPen]["total"])) { 
                            $totalPen = $saldosPenByMes[$iPen]["total"];
                            
                        }
                        if (!empty($saldosPenByMes[$iPen]["cant_mes"])) { 
                            $cantPen = $saldosPenByMes[$iPen]["cant_mes"];
                        }
                        $iPen++;
                    }
                }
        
                if (isset($saldosSinByMes[$iSin]["mes"])) {
                    if ($key == $saldosSinByMes[$iSin]["mes"]) {
                        if (!empty($saldosSinByMes[$iSin]["total"])) {
                            $totalSin = $saldosSinByMes[$iSin]["total"];
                        }
                        if (!empty($saldosSinByMes[$iSin]["cant_mes"])) {
                            $cantSin = $saldosSinByMes[$iSin]["cant_mes"];
                        }
                        $iSin++;
                    }
                } 

                $totalMes = $totalPag + $totalPen + $totalSin;
                $totalCant = $cantPag + $cantPen + $cantSin;
                $totalMesRecibidas += $totalMes;
                $cantMesRecibidas += $totalCant;

                $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalMes);
                $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalCant);

                $porcentajeMesPag = 0; $porcentajeMesPen = 0; $porcentajeMesSin = 0;

                if ($totalMes > 0) {
                    $porcentajeMesPag = ($totalPag * 100)/$totalMes;
                    $porcentajeMesPen = ($totalPen * 100)/$totalMes;
                    $porcentajeMesSin = ($totalSin * 100)/$totalMes;
                    
                    $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porcentajeMesPag,2));
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("G".$i, 0);
                }

                $totalPorcentajeMes = $porcentajeMesPag + $porcentajeMesPen + $porcentajeMesSin;
                
                if ($totalPorcentajeMes > 0) {
                    $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($totalPorcentajeMes,2));
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("J".$i, 0);
                }

                $i++;
            }

            $porcentajePag = 0;
            $porcentajePen = 0;
            $porcentajeSin = 0;
            $porcentajeTotal = 0;

            if ($totalMesRecibidas > 0) {
                $porcentajePag = ($totalPagadas["total_pag"] * 100) / $totalMesRecibidas;
                $porcentajePen = ($totalPendientes["total_pen"] * 100) / $totalMesRecibidas;
                $porcentajeSin = ($totalSinReconocimiento["total_sin"] * 100) / $totalMesRecibidas;
                $porcentajeTotal = $porcentajePag + $porcentajePen + $porcentajeSin;
            }

            $objPHPExcel->getActiveSheet()->setCellValue("E4", $totalPagadas["total_pag"]);
            $objPHPExcel->getActiveSheet()->setCellValue("F4", $totalPagadas["cantidad"]);
            $objPHPExcel->getActiveSheet()->setCellValue("G4", number_format($porcentajePag, 2)."%");

            $objPHPExcel->getActiveSheet()->setCellValue("H4", $totalMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("I4", $cantMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("J4", number_format($porcentajeTotal, 2)."%");

            $objPHPExcel->getActiveSheet()->setCellValue("E17", $totalPagadas["total_pag"]);
            $objPHPExcel->getActiveSheet()->setCellValue("F17", $totalPagadas["cantidad"]);
            $objPHPExcel->getActiveSheet()->setCellValue("G17", number_format($porcentajePag, 2)."%");

            $objPHPExcel->getActiveSheet()->setCellValue("H17", $totalMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("I17", $cantMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("J17", number_format($porcentajeTotal, 2)."%");

            $objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getNumberFormat()->setFormatCode('#,##0');
            $objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getNumberFormat()->setFormatCode('#,##0');
        }

        if ($_POST["filtroEstadoTramite"] == 2) {
            $objPHPExcel->getActiveSheet()->mergeCells('E2:G2');
            $objPHPExcel->getActiveSheet()->getStyle('E2:G2')->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->setCellValue("E2", "PENDIENTES");

            $objPHPExcel->getActiveSheet()->setCellValue("E3", "Valor ($)");
            $objPHPExcel->getActiveSheet()->setCellValue("F3", "#");
            $objPHPExcel->getActiveSheet()->setCellValue("G3", "% Periodo");

            $objPHPExcel->getActiveSheet()->mergeCells('H2:I2');
            $objPHPExcel->getActiveSheet()->getStyle('H2:I2')->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->setCellValue("H2", "TOTAL RECIBIDAS"); 
            $objPHPExcel->getActiveSheet()->setCellValue("H3", "Valor ($)");
            $objPHPExcel->getActiveSheet()->setCellValue("I3", "#");
            $objPHPExcel->getActiveSheet()->setCellValue("J3", "% Periodo");
            $objPHPExcel->getActiveSheet()->getStyle('H3:J3')->getFont()->setBold( true );


            $totalMesRecibidas = 0;
            $cantMesRecibidas = 0;
            $iPag = 0;
            $iPen = 0;
            $iSin = 0;
            $i = 5;
            foreach ($months as $key => $mes) {

                $totalPag = 0; $totalPen = 0; $totalSin = 0;
                $cantPag = 0; $cantPen = 0; $cantSin = 0;


                if (isset($saldosPenByMes[$iPen]["mes"])) {
                    if ($key == $saldosPenByMes[$iPen]["mes"]) {
                        if (!empty($saldosPenByMes[$iPen]["total"])) { 
                            $totalPen = $saldosPenByMes[$iPen]["total"];
                            
                        }
                        if (!empty($saldosPenByMes[$iPen]["cant_mes"])) { 
                            $cantPen = $saldosPenByMes[$iPen]["cant_mes"];
                        }
                        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $totalPen);
                        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $cantPen);
                        $iPen++;
                    } else {
                        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
                    }
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
                }

                if (isset($saldosPagByMes[$iPag]["mes"])) {
                    if ($key == $saldosPagByMes[$iPag]["mes"]) {
                        if (!empty($saldosPagByMes[$iPag]["total"])) {
                            $totalPag = $saldosPagByMes[$iPag]["total"];   
                        }
                        if (!empty($saldosPagByMes[$iPag]["cant_mes"])) {
                            $cantPag = $saldosPagByMes[$iPag]["cant_mes"];
                        }
                        $iPag++;
                    }
                }
        
                if (isset($saldosSinByMes[$iSin]["mes"])) {
                    if ($key == $saldosSinByMes[$iSin]["mes"]) {
                        if (!empty($saldosSinByMes[$iSin]["total"])) {
                            $totalSin = $saldosSinByMes[$iSin]["total"];
                        }
                        if (!empty($saldosSinByMes[$iSin]["cant_mes"])) {
                            $cantSin = $saldosSinByMes[$iSin]["cant_mes"];
                        }
                        $iSin++;
                    }
                } 

                $totalMes = $totalPag + $totalPen + $totalSin;
                $totalCant = $cantPag + $cantPen + $cantSin;
                $totalMesRecibidas += $totalMes;
                $cantMesRecibidas += $totalCant;

                $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalMes);
                $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalCant);

                $porcentajeMesPag = 0; $porcentajeMesPen = 0; $porcentajeMesSin = 0;

                if ($totalMes > 0) {
                    $porcentajeMesPag = ($totalPag * 100)/$totalMes;
                    $porcentajeMesPen = ($totalPen * 100)/$totalMes;
                    $porcentajeMesSin = ($totalSin * 100)/$totalMes;
                    $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porcentajeMesPag,2));
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("G".$i, 0);
                }

                $totalPorcentajeMes = $porcentajeMesPag + $porcentajeMesPen + $porcentajeMesSin;
                
                if ($totalPorcentajeMes > 0) {
                    $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($totalPorcentajeMes,2));
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("J".$i, 0);
                }

                $i++;
            }

            $porcentajePag = 0;
            $porcentajePen = 0;
            $porcentajeSin = 0;
            $porcentajeTotal = 0;

            if ($totalMesRecibidas > 0) {
                $porcentajePag = ($totalPagadas["total_pag"] * 100) / $totalMesRecibidas;
                $porcentajePen = ($totalPendientes["total_pen"] * 100) / $totalMesRecibidas;
                $porcentajeSin = ($totalSinReconocimiento["total_sin"] * 100) / $totalMesRecibidas;
                $porcentajeTotal = $porcentajePag + $porcentajePen + $porcentajeSin;
            }

            $objPHPExcel->getActiveSheet()->setCellValue("E4", $totalPendientes["total_pen"]);
            $objPHPExcel->getActiveSheet()->setCellValue("F4", $totalPendientes["cantidad"]);
            $objPHPExcel->getActiveSheet()->setCellValue("G4", number_format($porcentajePen, 2)."%");

            $objPHPExcel->getActiveSheet()->setCellValue("H4", $totalMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("I4", $cantMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("J4", number_format($porcentajeTotal, 2)."%");

            $objPHPExcel->getActiveSheet()->setCellValue("E17", $totalPendientes["total_pen"]);
            $objPHPExcel->getActiveSheet()->setCellValue("F17", $totalPendientes["cantidad"]);
            $objPHPExcel->getActiveSheet()->setCellValue("G17", number_format($porcentajePen, 2)."%");

            $objPHPExcel->getActiveSheet()->setCellValue("H17", $totalMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("I17", $cantMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("J17", number_format($porcentajeTotal, 2)."%");

            $objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getNumberFormat()->setFormatCode('#,##0');
            $objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getNumberFormat()->setFormatCode('#,##0');
        }

        if ($_POST["filtroEstadoTramite"] == 3) {
            $objPHPExcel->getActiveSheet()->mergeCells('E2:G2');
            $objPHPExcel->getActiveSheet()->getStyle('E2:G2')->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->setCellValue("E2", "SIN RECONOCIMIENTO");

            $objPHPExcel->getActiveSheet()->setCellValue("E3", "Valor ($)");
            $objPHPExcel->getActiveSheet()->setCellValue("F3", "#");
            $objPHPExcel->getActiveSheet()->setCellValue("G3", "% Periodo");

            $objPHPExcel->getActiveSheet()->mergeCells('H2:I2');
            $objPHPExcel->getActiveSheet()->getStyle('H2:I2')->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->setCellValue("H2", "TOTAL RECIBIDAS"); 
            $objPHPExcel->getActiveSheet()->setCellValue("H3", "Valor ($)");
            $objPHPExcel->getActiveSheet()->setCellValue("I3", "#");
            $objPHPExcel->getActiveSheet()->setCellValue("J3", "% Periodo");
            $objPHPExcel->getActiveSheet()->getStyle('H3:J3')->getFont()->setBold( true );

            $totalMesRecibidas = 0;
            $cantMesRecibidas = 0;
            $iPag = 0;
            $iPen = 0;
            $iSin = 0;
            $i = 5;
            foreach ($months as $key => $mes) {

                $totalPag = 0; $totalPen = 0; $totalSin = 0;
                $cantPag = 0; $cantPen = 0; $cantSin = 0;


                if (isset($saldosSinByMes[$iSin]["mes"])) {
                    if ($key == $saldosSinByMes[$iSin]["mes"]) {
                        if (!empty($saldosSinByMes[$iSin]["total"])) { 
                            $totalSin = $saldosSinByMes[$iSin]["total"];
                            
                        }
                        if (!empty($saldosSinByMes[$iSin]["cant_mes"])) { 
                            $cantSin = $saldosSinByMes[$iSin]["cant_mes"];
                        }
                        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $totalSin);
                        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $cantSin);
                        $iSin++;
                    } else {
                        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
                    }
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
                }

                if (isset($saldosPagByMes[$iPag]["mes"])) {
                    if ($key == $saldosPagByMes[$iPag]["mes"]) {
                        if (!empty($saldosPagByMes[$iPag]["total"])) {
                            $totalPag = $saldosPagByMes[$iPag]["total"];   
                        }
                        if (!empty($saldosPagByMes[$iPag]["cant_mes"])) {
                            $cantPag = $saldosPagByMes[$iPag]["cant_mes"];
                        }
                        $iPag++;
                    }
                }
        
                if (isset($saldosPenByMes[$iPen]["mes"])) {
                    if ($key == $saldosPenByMes[$iPen]["mes"]) {
                        if (!empty($saldosPenByMes[$iPen]["total"])) { 
                            $totalPen = $saldosPenByMes[$iPen]["total"];
                            
                        }
                        if (!empty($saldosPenByMes[$iPen]["cant_mes"])) { 
                            $cantPen = $saldosPenByMes[$iPen]["cant_mes"];
                        }
                        $iPen++;
                    }
                } 

                $totalMes = $totalPag + $totalPen + $totalSin;
                $totalCant = $cantPag + $cantPen + $cantSin;
                $totalMesRecibidas += $totalMes;
                $cantMesRecibidas += $totalCant;

                $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalMes);
                $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalCant);

                $porcentajeMesPag = 0; $porcentajeMesPen = 0; $porcentajeMesSin = 0;

                if ($totalMes > 0) {
                    $porcentajeMesPag = ($totalPag * 100)/$totalMes;
                    $porcentajeMesPen = ($totalPen * 100)/$totalMes;
                    $porcentajeMesSin = ($totalSin * 100)/$totalMes;
                    $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porcentajeMesPag,2));
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("G".$i, 0);
                }

                $totalPorcentajeMes = $porcentajeMesPag + $porcentajeMesPen + $porcentajeMesSin;
                
                if ($totalPorcentajeMes > 0) {
                    $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($totalPorcentajeMes,2));
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("J".$i, 0);
                }

                $i++;
            }

            $porcentajePag = 0;
            $porcentajePen = 0;
            $porcentajeSin = 0;
            $porcentajeTotal = 0;

            if ($totalMesRecibidas > 0) {
                $porcentajePag = ($totalPagadas["total_pag"] * 100) / $totalMesRecibidas;
                $porcentajePen = ($totalPendientes["total_pen"] * 100) / $totalMesRecibidas;
                $porcentajeSin = ($totalSinReconocimiento["total_sin"] * 100) / $totalMesRecibidas;
                $porcentajeTotal = $porcentajePag + $porcentajePen + $porcentajeSin;
            }

            $objPHPExcel->getActiveSheet()->setCellValue("E4", $totalSinReconocimiento["total_sin"]);
            $objPHPExcel->getActiveSheet()->setCellValue("F4", $totalSinReconocimiento["cantidad"]);
            $objPHPExcel->getActiveSheet()->setCellValue("G4", number_format($porcentajeSin, 2)."%");

            $objPHPExcel->getActiveSheet()->setCellValue("H4", $totalMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("I4", $cantMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("J4", number_format($porcentajeTotal, 2)."%");

            $objPHPExcel->getActiveSheet()->setCellValue("E17", $totalSinReconocimiento["total_sin"]);
            $objPHPExcel->getActiveSheet()->setCellValue("F17", $totalSinReconocimiento["cantidad"]);
            $objPHPExcel->getActiveSheet()->setCellValue("G17", number_format($porcentajeSin, 2)."%");

            $objPHPExcel->getActiveSheet()->setCellValue("H17", $totalMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("I17", $cantMesRecibidas);
            $objPHPExcel->getActiveSheet()->setCellValue("J17", number_format($porcentajeTotal, 2)."%");

            $objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getNumberFormat()->setFormatCode('#,##0');
            $objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getNumberFormat()->setFormatCode('#,##0');
        }

        $objPHPExcel->getActiveSheet()->getStyle('H5:H16')->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle('I5:I16')->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle('J5:J16')->getFont()->setBold( true );
    }

    $objPHPExcel->getActiveSheet()->getStyle('B2:P2')->getFont()->setBold( true );

    $objPHPExcel->getActiveSheet()->mergeCells('B17:D17');
    $objPHPExcel->getActiveSheet()->getStyle('B17:D17')->applyFromArray($borders);
    $objPHPExcel->getActiveSheet()->getStyle('B17:P17')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->setCellValue("B17", "TOTAL"); 
    $objPHPExcel->getActiveSheet()->getStyle('B17')
        ->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
} else {
   
    //si la condición entra acá mostraremos la información acumulada (saldos) por año   
    
    $fieldYears = "YEAR($filtroFecha) as year ";
    $whereYears = "(inc_estado_tramite != 1) ";
    $groupBy = "GROUP BY YEAR($filtroFecha) ";
    $orderBy = "ORDER BY YEAR($filtroFecha) DESC";

    $fieldYearsPag = "YEAR($filtroFecha) as year, SUM(inc_valor) as total, COUNT(*) as cant_year";
    $fieldYearsPen = "YEAR($filtroFecha) as year, SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_year";
    $fieldYearsSin = "YEAR($filtroFecha) as year, SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_year";
    $fieldTotalYearPag = "SUM(inc_valor) as total, COUNT(*) as cant_year";
    $fieldTotalYearPen = "SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_year";
    $fieldTotalYearSin = "SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_year";

    $whereYearsPag = "(inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereYearsPen = "(inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereYearsSin = "(inc_estado_tramite = 6) ";


    //validamos el cliente
    if($_SESSION['cliente_id'] != 0) {

        $whereYears.="AND inc_empresa = ".$_SESSION['cliente_id'];
        $whereYearsPag.="AND inc_empresa = ".$_SESSION['cliente_id'];
        $whereYearsPen.="AND inc_empresa = ".$_SESSION['cliente_id'];
        $whereYearsSin.="AND inc_empresa = ".$_SESSION['cliente_id'];

        $clienteId = $_SESSION['cliente_id'];
    }

    
    $totalYearPag = ModeloTesoreria::mdlMostrarUnitario($fieldTotalYearPag, "gi_incapacidad", $whereYearsPag);
    $totalYearPen = ModeloTesoreria::mdlMostrarUnitario($fieldTotalYearPen, "gi_incapacidad", $whereYearsPen);
    $totalYearSin = ModeloTesoreria::mdlMostrarUnitario($fieldTotalYearSin, "gi_incapacidad", $whereYearsSin);

    $allYears = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldYears, "gi_incapacidad", $whereYears, $groupBy, $orderBy);
    $dataYearsPag = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldYearsPag, "gi_incapacidad", $whereYearsPag, $groupBy, $orderBy);
    $dataYearsPen = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldYearsPen, "gi_incapacidad", $whereYearsPen, $groupBy, $orderBy);
    $dataYearsSin = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldYearsSin, "gi_incapacidad", $whereYearsSin, $groupBy, $orderBy);

    //validamos el estado de tramite
    if ($_POST["filtroEstadoTramite"] == 0) {
        $objPHPExcel->getActiveSheet()->mergeCells('E2:G2');
        $objPHPExcel->getActiveSheet()->getStyle('E2:G2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("E2", "PAGADAS");

        $objPHPExcel->getActiveSheet()->setCellValue("E3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("F3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("G3", "% Periodo");

        $objPHPExcel->getActiveSheet()->mergeCells('H2:J2');
        $objPHPExcel->getActiveSheet()->getStyle('H2:J2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("H2", "PENDIENTES");

        $objPHPExcel->getActiveSheet()->setCellValue("H3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("I3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("J3", "% Periodo");

        $objPHPExcel->getActiveSheet()->mergeCells('K2:M2');
        $objPHPExcel->getActiveSheet()->getStyle('K2:M2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("K2", "SIN RECONOCIMIENTO");

        $objPHPExcel->getActiveSheet()->setCellValue("K3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("L3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("M3", "% Periodo");

        $objPHPExcel->getActiveSheet()->mergeCells('N2:P2');
        $objPHPExcel->getActiveSheet()->getStyle('N2:P2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("N2", "TOTAL RECIBIDAS"); 
        $objPHPExcel->getActiveSheet()->setCellValue("N3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("O3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("P3", "% Periodo");
        $objPHPExcel->getActiveSheet()->getStyle('N3:P3')->getFont()->setBold( true );
        $i = 5;
        foreach ($allYears as $value) {
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':'.'D'.$i); 
            $objPHPExcel->getActiveSheet()->getStyle('B'.$i)
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["year"]);
            
            $yearPag = 0; $yearPen = 0; $yearSin = 0;
            $cantPag = 0; $cantPen = 0; $cantSin = 0;

            foreach($dataYearsPag as $yearEst1){
                if($yearEst1["year"] == $value["year"]){
                    $yearPag = $yearEst1["total"];
                    $cantPag = $yearEst1["cant_year"];
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $yearPag);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $cantPag);
                    break;
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
                }
            }

            foreach($dataYearsPen as $yearEst2){
                if($yearEst2["year"] == $value["year"]){
                    $yearPen = $yearEst2["total"];
                    $cantPen = $yearEst2["cant_year"];
                    $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $yearPen);
                    $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $cantPen);
                    break;
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("H".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("I".$i, 0);
                }
            }

            foreach($dataYearsSin as $yearEst3){
                if($yearEst3["year"] == $value["year"]){
                    $yearSin = $yearEst3["total"];
                    $cantSin = $yearEst3["cant_year"];
                    $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $yearSin);
                    $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $cantSin);
                    break;
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("K".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("L".$i, 0);
                }
            }

            $totalSaldosYear = $yearPag + $yearPen + $yearSin;
            $totalCantYear = $cantPag + $cantPen + $cantSin;
            $porPag = 0;
            $porPen = 0;
            $porSin = 0;

            if($totalSaldosYear > 0){
                $porPag = ($yearPag * 100) / $totalSaldosYear;
                $porPen = ($yearPen * 100) / $totalSaldosYear;
                $porSin = ($yearSin * 100) / $totalSaldosYear;
            }

            $porTotalYear = $porPag+$porPen+$porSin;

            $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porPag,2));
            $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($porPen,2));
            $objPHPExcel->getActiveSheet()->setCellValue("M".$i, number_format($porSin,2));

            $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $totalSaldosYear);
            $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $totalCantYear);
            $objPHPExcel->getActiveSheet()->setCellValue("P".$i, number_format($porTotalYear,2));

            $i++;
        }

        $porTotalPag = 0;
        $porTotalPen = 0;
        $porTotalSin = 0;
        $porTotalAll = 0;

        $totalSaldosAll = $totalYearPag["total"] + $totalYearPen["total"] + $totalYearSin["total"];
        $totalCantAll = $totalYearPag["cant_year"] + $totalYearPen["cant_year"] + $totalYearSin["cant_year"];

        if ($totalSaldosAll > 0) {
            $porTotalPag = ($totalYearPag["total"] * 100) / $totalSaldosAll;
            $porTotalPen = ($totalYearPen["total"] * 100) / $totalSaldosAll;
            $porTotalSin = ($totalYearSin["total"] * 100) / $totalSaldosAll;
        }

        $porTotalAll = $porTotalPag + $porTotalPen + $porTotalSin;

        $objPHPExcel->getActiveSheet()->setCellValue("E4", $totalYearPag["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F4", $totalYearPag["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G4", number_format($porTotalPag,2));

        $objPHPExcel->getActiveSheet()->setCellValue("H4", $totalYearPen["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("I4", $totalYearPen["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("J4", number_format($porTotalPen,2));

        $objPHPExcel->getActiveSheet()->setCellValue("K4", $totalYearSin["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("L4", $totalYearSin["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("M4", number_format($porTotalSin,2));

        $objPHPExcel->getActiveSheet()->setCellValue("N4", $totalSaldosAll);
        $objPHPExcel->getActiveSheet()->setCellValue("O4", $totalCantAll);
        $objPHPExcel->getActiveSheet()->setCellValue("P4", number_format($porTotalAll,2));

        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $totalYearPag["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $totalYearPag["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porTotalPag,2));

        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalYearPen["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalYearPen["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($porTotalPen,2));

        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $totalYearSin["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $totalYearSin["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("M".$i, number_format($porTotalSin,2));

        $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $totalSaldosAll);
        $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $totalCantAll);
        $objPHPExcel->getActiveSheet()->setCellValue("P".$i, number_format($porTotalAll,2));
        
        $objPHPExcel->getActiveSheet()->getStyle('E4:E17')->getNumberFormat()->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle('H4:H17')->getNumberFormat()->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle('K4:K17')->getNumberFormat()->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle('N4:N17')->getNumberFormat()->setFormatCode('#,##0');

        $objPHPExcel->getActiveSheet()->getStyle("N5:N$i")->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle("O5:O$i")->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle("P5:P$i")->getFont()->setBold( true );
    }
    if ($_POST["filtroEstadoTramite"] == 1) {
        $objPHPExcel->getActiveSheet()->mergeCells('E2:G2');
        $objPHPExcel->getActiveSheet()->getStyle('E2:G2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("E2", "PAGADAS");

        $objPHPExcel->getActiveSheet()->setCellValue("E3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("F3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("G3", "% Periodo");

        $objPHPExcel->getActiveSheet()->mergeCells('H2:I2');
        $objPHPExcel->getActiveSheet()->getStyle('H2:I2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("H2", "TOTAL RECIBIDAS"); 
        $objPHPExcel->getActiveSheet()->setCellValue("H3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("I3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("J3", "% Periodo");
        $objPHPExcel->getActiveSheet()->getStyle('H3:J3')->getFont()->setBold( true );

        $i = 5;
        foreach ($allYears as $value) {
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':'.'D'.$i); 
            $objPHPExcel->getActiveSheet()->getStyle('B'.$i)
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["year"]);
            
            $yearPag = 0; $yearPen = 0; $yearSin = 0;
            $cantPag = 0; $cantPen = 0; $cantSin = 0;

            foreach($dataYearsPag as $yearEst1){
                if($yearEst1["year"] == $value["year"]){
                    $yearPag = $yearEst1["total"];
                    $cantPag = $yearEst1["cant_year"];
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $yearPag);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $cantPag);
                    break;
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
                }
            }

            foreach($dataYearsPen as $yearEst2){
                if($yearEst2["year"] == $value["year"]){
                    $yearPen = $yearEst2["total"];
                    $cantPen = $yearEst2["cant_year"];
                    break;
                }
            }

            foreach($dataYearsSin as $yearEst3){
                if($yearEst3["year"] == $value["year"]){
                    $yearSin = $yearEst3["total"];
                    $cantSin = $yearEst3["cant_year"];
                    break;
                }
            }

            $totalSaldosYear = $yearPag + $yearPen + $yearSin;
            $totalCantYear = $cantPag + $cantPen + $cantSin;
            $porPag = 0;
            $porPen = 0;
            $porSin = 0;

            if($totalSaldosYear > 0){
                $porPag = ($yearPag * 100) / $totalSaldosYear;
                $porPen = ($yearPen * 100) / $totalSaldosYear;
                $porSin = ($yearSin * 100) / $totalSaldosYear;
            }

            $porTotalYear = $porPag+$porPen+$porSin;

            $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porPag,2));

            $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalSaldosYear);
            $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalCantYear);
            $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($porTotalYear,2));

            $i++;
        }
        $porTotalPag = 0;
        $porTotalPen = 0;
        $porTotalSin = 0;
        $porTotalAll = 0;

        $totalSaldosAll = $totalYearPag["total"] + $totalYearPen["total"] + $totalYearSin["total"];
        $totalCantAll = $totalYearPag["cant_year"] + $totalYearPen["cant_year"] + $totalYearSin["cant_year"];

        if ($totalSaldosAll > 0) {
            $porTotalPag = ($totalYearPag["total"] * 100) / $totalSaldosAll;
            $porTotalPen = ($totalYearPen["total"] * 100) / $totalSaldosAll;
            $porTotalSin = ($totalYearSin["total"] * 100) / $totalSaldosAll;
        }

        $porTotalAll = $porTotalPag + $porTotalPen + $porTotalSin;

        $objPHPExcel->getActiveSheet()->setCellValue("E4", $totalYearPag["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F4", $totalYearPag["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G4", number_format($porTotalPag,2));

        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $totalYearPag["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $totalYearPag["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porTotalPag,2));

        $objPHPExcel->getActiveSheet()->setCellValue("H4", $totalSaldosAll);
        $objPHPExcel->getActiveSheet()->setCellValue("I4", $totalCantAll);
        $objPHPExcel->getActiveSheet()->setCellValue("J4", number_format($porTotalAll,2));

        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalSaldosAll);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalCantAll);
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($porTotalAll,2));

        $objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getNumberFormat()->setFormatCode('#,##0');

        $objPHPExcel->getActiveSheet()->getStyle("H5:H$i")->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle("I5:I$i")->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle("J5:J$i")->getFont()->setBold( true );
    }
    if ($_POST["filtroEstadoTramite"] == 2) {
        $objPHPExcel->getActiveSheet()->mergeCells('E2:G2');
        $objPHPExcel->getActiveSheet()->getStyle('E2:G2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("E2", "PENDIENTES");

        $objPHPExcel->getActiveSheet()->setCellValue("E3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("F3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("G3", "% Periodo");

        $objPHPExcel->getActiveSheet()->mergeCells('H2:I2');
        $objPHPExcel->getActiveSheet()->getStyle('H2:I2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("H2", "TOTAL RECIBIDAS"); 
        $objPHPExcel->getActiveSheet()->setCellValue("H3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("I3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("J3", "% Periodo");
        $objPHPExcel->getActiveSheet()->getStyle('H3:J3')->getFont()->setBold( true );

        $i = 5;
        foreach ($allYears as $value) {
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':'.'D'.$i); 
            $objPHPExcel->getActiveSheet()->getStyle('B'.$i)
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["year"]);
            
            $yearPag = 0; $yearPen = 0; $yearSin = 0;
            $cantPag = 0; $cantPen = 0; $cantSin = 0;

            foreach($dataYearsPag as $yearEst1){
                if($yearEst1["year"] == $value["year"]){
                    $yearPag = $yearEst1["total"];
                    $cantPag = $yearEst1["cant_year"];
                    break;
                }
            }

            foreach($dataYearsPen as $yearEst2){
                if($yearEst2["year"] == $value["year"]){
                    $yearPen = $yearEst2["total"];
                    $cantPen = $yearEst2["cant_year"];
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $yearPen);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $cantPen);
                    break;
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
                }
            }

            foreach($dataYearsSin as $yearEst3){
                if($yearEst3["year"] == $value["year"]){
                    $yearSin = $yearEst3["total"];
                    $cantSin = $yearEst3["cant_year"];
                    break;
                }
            }

            $totalSaldosYear = $yearPag + $yearPen + $yearSin;
            $totalCantYear = $cantPag + $cantPen + $cantSin;
            $porPag = 0;
            $porPen = 0;
            $porSin = 0;

            if($totalSaldosYear > 0){
                $porPag = ($yearPag * 100) / $totalSaldosYear;
                $porPen = ($yearPen * 100) / $totalSaldosYear;
                $porSin = ($yearSin * 100) / $totalSaldosYear;
            }

            $porTotalYear = $porPag+$porPen+$porSin;

            $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porPen,2));

            $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalSaldosYear);
            $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalCantYear);
            $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($porTotalYear,2));

            $i++;
        }
        $porTotalPag = 0;
        $porTotalPen = 0;
        $porTotalSin = 0;
        $porTotalAll = 0;

        $totalSaldosAll = $totalYearPag["total"] + $totalYearPen["total"] + $totalYearSin["total"];
        $totalCantAll = $totalYearPag["cant_year"] + $totalYearPen["cant_year"] + $totalYearSin["cant_year"];

        if ($totalSaldosAll > 0) {
            $porTotalPag = ($totalYearPag["total"] * 100) / $totalSaldosAll;
            $porTotalPen = ($totalYearPen["total"] * 100) / $totalSaldosAll;
            $porTotalSin = ($totalYearSin["total"] * 100) / $totalSaldosAll;
        }

        $porTotalAll = $porTotalPag + $porTotalPen + $porTotalSin;

        $objPHPExcel->getActiveSheet()->setCellValue("E4", $totalYearPen["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F4", $totalYearPen["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G4", number_format($porTotalPen,2));

        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $totalYearPen["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $totalYearPen["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porTotalPen,2));

        $objPHPExcel->getActiveSheet()->setCellValue("H4", $totalSaldosAll);
        $objPHPExcel->getActiveSheet()->setCellValue("I4", $totalCantAll);
        $objPHPExcel->getActiveSheet()->setCellValue("J4", number_format($porTotalAll,2));

        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalSaldosAll);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalCantAll);
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($porTotalAll,2));

        $objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getNumberFormat()->setFormatCode('#,##0');

        $objPHPExcel->getActiveSheet()->getStyle("H5:H$i")->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle("I5:I$i")->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle("J5:J$i")->getFont()->setBold( true );
    }
    if ($_POST["filtroEstadoTramite"] == 3) {
        $objPHPExcel->getActiveSheet()->mergeCells('E2:G2');
        $objPHPExcel->getActiveSheet()->getStyle('E2:G2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("E2", "SIN RECONOCIMIENTO");

        $objPHPExcel->getActiveSheet()->setCellValue("E3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("F3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("G3", "% Periodo");

        $objPHPExcel->getActiveSheet()->mergeCells('H2:I2');
        $objPHPExcel->getActiveSheet()->getStyle('H2:I2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->setCellValue("H2", "TOTAL RECIBIDAS"); 
        $objPHPExcel->getActiveSheet()->setCellValue("H3", "Valor ($)");
        $objPHPExcel->getActiveSheet()->setCellValue("I3", "#");
        $objPHPExcel->getActiveSheet()->setCellValue("J3", "% Periodo");
        $objPHPExcel->getActiveSheet()->getStyle('H3:J3')->getFont()->setBold( true );

        $i = 5;
        foreach ($allYears as $value) {
            $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':'.'D'.$i); 
            $objPHPExcel->getActiveSheet()->getStyle('B'.$i)
                ->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["year"]);
            
            $yearPag = 0; $yearPen = 0; $yearSin = 0;
            $cantPag = 0; $cantPen = 0; $cantSin = 0;

            foreach($dataYearsPag as $yearEst1){
                if($yearEst1["year"] == $value["year"]){
                    $yearPag = $yearEst1["total"];
                    $cantPag = $yearEst1["cant_year"];
                    break;
                }
            }

            foreach($dataYearsPen as $yearEst2){
                if($yearEst2["year"] == $value["year"]){
                    $yearPen = $yearEst2["total"];
                    $cantPen = $yearEst2["cant_year"];
                    break;
                }
            }

            foreach($dataYearsSin as $yearEst3){
                if($yearEst3["year"] == $value["year"]){
                    $yearSin = $yearEst3["total"];
                    $cantSin = $yearEst3["cant_year"];
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $yearSin);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $cantSin);
                    break;
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, 0);
                    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, 0);
                }
            }

            $totalSaldosYear = $yearPag + $yearPen + $yearSin;
            $totalCantYear = $cantPag + $cantPen + $cantSin;
            $porPag = 0;
            $porPen = 0;
            $porSin = 0;

            if($totalSaldosYear > 0){
                $porPag = ($yearPag * 100) / $totalSaldosYear;
                $porPen = ($yearPen * 100) / $totalSaldosYear;
                $porSin = ($yearSin * 100) / $totalSaldosYear;
            }

            $porTotalYear = $porPag+$porPen+$porSin;

            $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porSin,2));

            $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalSaldosYear);
            $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalCantYear);
            $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($porTotalYear,2));

            $i++;
        }
        $porTotalPag = 0;
        $porTotalPen = 0;
        $porTotalSin = 0;
        $porTotalAll = 0;

        $totalSaldosAll = $totalYearPag["total"] + $totalYearPen["total"] + $totalYearSin["total"];
        $totalCantAll = $totalYearPag["cant_year"] + $totalYearPen["cant_year"] + $totalYearSin["cant_year"];

        if ($totalSaldosAll > 0) {
            $porTotalPag = ($totalYearPag["total"] * 100) / $totalSaldosAll;
            $porTotalPen = ($totalYearPen["total"] * 100) / $totalSaldosAll;
            $porTotalSin = ($totalYearSin["total"] * 100) / $totalSaldosAll;
        }

        $porTotalAll = $porTotalPag + $porTotalPen + $porTotalSin;

        $objPHPExcel->getActiveSheet()->setCellValue("E4", $totalYearSin["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F4", $totalYearSin["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G4", number_format($porTotalSin,2));

        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $totalYearSin["total"]);
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $totalYearSin["cant_year"]);
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, number_format($porTotalSin,2));

        $objPHPExcel->getActiveSheet()->setCellValue("H4", $totalSaldosAll);
        $objPHPExcel->getActiveSheet()->setCellValue("I4", $totalCantAll);
        $objPHPExcel->getActiveSheet()->setCellValue("J4", number_format($porTotalAll,2));

        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalSaldosAll);
        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $totalCantAll);
        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, number_format($porTotalAll,2));

        $objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)->getNumberFormat()->setFormatCode('#,##0');
        $objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getNumberFormat()->setFormatCode('#,##0');

        $objPHPExcel->getActiveSheet()->getStyle("H5:H$i")->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle("I5:I$i")->getFont()->setBold( true );
        $objPHPExcel->getActiveSheet()->getStyle("J5:J$i")->getFont()->setBold( true );
    }

    $objPHPExcel->getActiveSheet()->getStyle('B2:P2')->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->mergeCells("B$i:D$i");
    $objPHPExcel->getActiveSheet()->getStyle("B$i:D$i")->applyFromArray($borders);
    $objPHPExcel->getActiveSheet()->getStyle("B$i:P$i")->getFont()->setBold( true );
    $objPHPExcel->getActiveSheet()->setCellValue("B$i", "TOTAL"); 
    $objPHPExcel->getActiveSheet()->getStyle("B$i")
        ->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
}

$objPHPExcel->getActiveSheet()->getStyle('B2:N2')
->getAlignment()
->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('B3:P3')
->getAlignment()
->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

/*Hoja donde muestro el consolidado de incapacidades */

//primero nos traemos la data
$fieldConsolidado = "inc_id,emp_nombre,emd_cedula,emd_nombre,inc_fecha_inicio,inc_fecha_final,dias,inc_clasificacion,
inc_diagnostico,inc_origen,inc_valor_pagado_empresa,inc_valor_pagado_eps,inc_valor_pagado_ajuste,inc_fecha_pago_nomina,
inc_fecha_recepcion_d,inc_fecha_generada,inc_fecha_radicacion,inc_numero_radicado_v,inc_fecha_solicitud,inc_valor,  
inc_fecha_pago,inc_fecha_negacion_,set_descripcion_v,inc_estado_tramite,inc_fecha_estado,emd_codigo_nomina,emd_cargo,  
emd_salario,est_emp_desc_v,emd_fecha_ingreso,inc_ips_afiliado,emd_fecha_afiliacion_eps,inc_afp_afiliado,inc_arl_afiliado,med_nombre,med_num_registro,med_estado_rethus,  
enat_nombre_v,enat_nit_v,inc_pertenece_red,inc_tipo_generacion,atn_descripcion,creador,editor,inc_ruta_incapacidad,  
inc_ruta_incapacidad_transcrita,inc_registrada_entidad_i,inc_estado,mot_desc_v,emd_sede,emd_centro_costos,emd_subcentro_costos,  
emd_ciudad,mot_observacion_v";

$whConsolidado = "";
if ($_POST["filtroEstadoTramite"] == 0) {
    $whConsolidado = "inc_estado_tramite != 'EMPRESA' ";
} else if ($_POST["filtroEstadoTramite"] == 1) {
    $whConsolidado = "inc_estado_tramite = 'PAGADA' ";
} else if ($_POST["filtroEstadoTramite"] == 2) {
    $whConsolidado = "(inc_estado_tramite = 'POR RADICAR' OR inc_estado_tramite = 'RADICADA' OR inc_estado_tramite = 'SOLICITUD DE PAGO' OR inc_estado_tramite = 'RECHAZADA') ";
} else {
    $whConsolidado = "inc_estado_tramite = 'NEGADA' ";
}

if ($_POST["yearFilter"] != 0) {
    $whConsolidado .= "AND YEAR(str_to_date($filtroFecha,'%d/%m/%Y')) = '".$_POST["yearFilter"]."' ";
}
if ($_SESSION['cliente_id'] != 0) {
    $whConsolidado .=  "AND inc_empresa = ".$_SESSION['cliente_id'];
}
$dataConsolidado = ModeloDAO::mdlMostrarGroupAndOrder($fieldConsolidado, "reporte_consolidado_inc", $whConsolidado);
// echo print_r($dataConsolidado);
//creamos la nueva hoja y mostramos la data
$newHoja = $objPHPExcel->createSheet(1);
$newHoja->setTitle("CONSOLIDADO");


$newHoja
    ->getStyle('A1:AX1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()
    ->getStyle('A1:AX1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$newHoja->getStyle('A1:AX1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

$newHoja->getStyle('A1:AX1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);  

$newHoja->getStyle('A1:AX1')->getFont()->setBold( true );

$newHoja->setCellValue("A1", "ID"); 
$newHoja->setCellValue("B1", "EMPRESA"); 
$newHoja->setCellValue("C1", "IDENTIFICACIÓN"); 
$newHoja->setCellValue("D1", "NOMBRE COLABORADOR"); 
$newHoja->setCellValue("E1", "FECHA INICIAL"); 
$newHoja->setCellValue("F1", "FECHA FINAL"); 
$newHoja->setCellValue("G1", "DÍAS"); 
$newHoja->setCellValue("H1", "CLASIFICACIÓN"); 
$newHoja->setCellValue("I1", "DIAGNOSTICO"); 
$newHoja->setCellValue("J1", "ORIGEN"); 
$newHoja->setCellValue("K1", "VALOR PAGADO EMPRESA"); 
$newHoja->setCellValue("L1", "VALOR PAGADO ENTIDAD"); 
$newHoja->setCellValue("M1", "VALOR PAGADO AJUSTE"); 
$newHoja->setCellValue("N1", "FECHA PAGO NOMINA"); 
$newHoja->setCellValue("O1", "FECHA RECEPCIÓN"); 
$newHoja->setCellValue("P1", "FECHA GENERACIÓN"); 
$newHoja->setCellValue("Q1", "FECHA RADICACIÓN"); 
$newHoja->setCellValue("R1", "NUMERO RADICADO");
$newHoja->setCellValue("S1", "FECHA SOLICITUD"); 
$newHoja->setCellValue("T1", "VALOR PAGADO"); 
$newHoja->setCellValue("U1", "FECHA DE PAGO"); 
$newHoja->setCellValue("V1", "FECHA NEGACION"); 
$newHoja->setCellValue("W1", "SUBESTADO"); 
$newHoja->setCellValue("X1", "ESTADO TRAMITE");  
$newHoja->setCellValue("Y1", "FECHA ESTADO"); 
$newHoja->setCellValue("Z1", "CODIGO NOMINA"); 
$newHoja->setCellValue("AA1", "CARGO");  
$newHoja->setCellValue("AB1", "SALARIO");
$newHoja->setCellValue("AC1", "ESTADO EMPLEADO");
$newHoja->setCellValue("AD1", "FECHA INGRESO"); 
$newHoja->setCellValue("AE1", "EPS");
$newHoja->setCellValue("AF1", "FECHA AFILIACION DE EPS"); 
$newHoja->setCellValue("AG1", "AFP");
$newHoja->setCellValue("AH1", "ARL");
$newHoja->setCellValue("AI1", "PROFESIONAL RESPONSABLE");
$newHoja->setCellValue("AJ1", "REGISTRO MEDICO");
$newHoja->setCellValue("AK1", "ESTADO RETHUS");
$newHoja->setCellValue("AL1", "ENTIDAD QUE GENERA");
$newHoja->setCellValue("AM1", "NIT ENTIDAD QUE GENERA");
$newHoja->setCellValue("AN1", "PERTENECE A LA RED");
$newHoja->setCellValue("AO1", "DOCUMENTO");
$newHoja->setCellValue("AP1", "ATENCIÓN");
$newHoja->setCellValue("AQ1", "CREADO POR");
$newHoja->setCellValue("AR1", "EDITADO POR");
$newHoja->setCellValue("AS1", "SOPORTE");
$newHoja->setCellValue("AT1", "TRANSCRITA");
$newHoja->setCellValue("AU1", "REGISTRADA EN ENTIDAD");
$newHoja->setCellValue("AV1", "ESTADO INCAPACIDAD");
$newHoja->setCellValue("AW1", "MOTIVO RECHAZO");
$newHoja->setCellValue("AX1", "OBSERVACIÓN");

$i = 2;
foreach ($dataConsolidado as $value) {
    $newHoja->setCellValue("A".$i, $value["inc_id"]); 
    $newHoja->setCellValue("B".$i, $value["emp_nombre"]); 
    $newHoja->setCellValue("C".$i, $value["emd_cedula"]); 
    $newHoja->setCellValue("D".$i, $value["emd_nombre"]); 
    $newHoja->setCellValue("E".$i, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_inicio"])); 
    $newHoja->setCellValue("F".$i, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_final"])); 
    $newHoja->setCellValue("G".$i, $value["dias"]); 
    $newHoja->setCellValue("H".$i, $value["inc_clasificacion"]); 
    $newHoja->setCellValue("I".$i, $value["inc_diagnostico"]); 
    $newHoja->setCellValue("J".$i, $value["inc_origen"]); 
    $newHoja->setCellValue("K".$i, $value["inc_valor_pagado_empresa"]); 
    $newHoja->setCellValue("L".$i, $value["inc_valor_pagado_eps"]); 
    $newHoja->setCellValue("M".$i, $value["inc_valor_pagado_ajuste"]); 
    $newHoja->setCellValue("N".$i, (!empty($value["inc_fecha_pago_nomina"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_pago_nomina"]):""); 
    $newHoja->setCellValue("O".$i, (!empty($value["inc_fecha_recepcion_d"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_recepcion_d"]):""); 
    $newHoja->setCellValue("P".$i, (!empty($value["inc_fecha_generada"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_generada"]):""); 
    $newHoja->setCellValue("Q".$i, (!empty($value["inc_fecha_radicacion"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_radicacion"]):""); 
    $newHoja->setCellValue("R".$i, $value["inc_numero_radicado_v"]);
    $newHoja->setCellValue("S".$i, (!empty($value["inc_fecha_solicitud"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_solicitud"]):""); 
    $newHoja->setCellValue("T".$i, $value["inc_valor"]); 
    $newHoja->setCellValue("U".$i, (!empty($value["inc_fecha_pago"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_pago"]):""); 
    $newHoja->setCellValue("V".$i, (!empty($value["inc_fecha_negacion_"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_negacion_"]):""); 
    $newHoja->setCellValue("W".$i, $value["set_descripcion_v"]); 
    $newHoja->setCellValue("X".$i, $value["inc_estado_tramite"]);  
    $newHoja->setCellValue("Y".$i, (!empty($value["inc_Fecha_estado"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_Fecha_estado"]):""); 
    $newHoja->setCellValue("Z".$i, $value["emd_codigo_nomina"]); 
    $newHoja->setCellValue("AA".$i, $value["emd_cargo"]);  
    $newHoja->setCellValue("AB".$i, $value["emd_salario"]);
    $newHoja->setCellValue("AC".$i, $value["est_emp_desc_v"]);
    $newHoja->setCellValue("AD".$i, (!empty($value["emd_fecha_ingreso"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["emd_fecha_ingreso"]):"");
    $newHoja->setCellValue("AE".$i, $value["inc_ips_afiliado"]);
    $newHoja->setCellValue("AF".$i, (!empty($value["emd_fecha_afiliacion_eps"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["emd_fecha_afiliacion_eps"]):"");
    $newHoja->setCellValue("AG".$i, $value["inc_afp_afiliado"]);
    $newHoja->setCellValue("AH".$i, $value["inc_arl_afiliado"]);
    $newHoja->setCellValue("AI".$i, $value["med_nombre"]);
    $newHoja->setCellValue("AJ".$i, $value["med_num_registro"]);
    $newHoja->setCellValue("AK".$i, $value["med_estado_rethus"]);
    $newHoja->setCellValue("AL".$i, $value["enat_nombre_v"]);
    $newHoja->setCellValue("AM".$i, $value["enat_nit_v"]);
    $newHoja->setCellValue("AN".$i, $value["inc_pertenece_red"]);
    $newHoja->setCellValue("AO".$i, $value["inc_tipo_generacion"]);
    $newHoja->setCellValue("AP".$i, $value["atn_descripcion"]);
    $newHoja->setCellValue("AQ".$i, $value["creador"]);
    $newHoja->setCellValue("AR".$i, $value["editor"]);
    $newHoja->setCellValue("AS".$i, $value["inc_ruta_incapacidad"]);
    $newHoja->setCellValue("AT".$i, $value["inc_ruta_incapacidad_transcrita"]);
    $newHoja->setCellValue("AU".$i, $value["inc_registrada_entidad_i"]);
    $newHoja->setCellValue("AV".$i, $value["inc_estado"]);
    $newHoja->setCellValue("AW".$i, $value["mot_desc_v"]);
    $newHoja->setCellValue("AX".$i, $value["mot_observacion_v"]);
    $i++;
}

$newHoja->getStyle("E2:E$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("F2:F$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("N2:N$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("O2:O$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("P2:P$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("Q2:Q$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("S2:S$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("U2:U$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("V2:V$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("Y2:Y$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("AD2:AD$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("AF2:AF$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

foreach(range('A','AZ') as $columnID) {
    $newHoja->getColumnDimension($columnID)
        ->setAutoSize(true);

}


$nombreCliente = ModeloDAO::mdlMostrarUnitario("emp_nombre", "gi_empresa", "emp_id = $clienteId");
$fileName = "informe_saldo_";
if (!empty($nombreCliente)) {
    $fileName.=strtolower(str_replace(' ', '_',$nombreCliente["emp_nombre"])).".xlsx";
} else {
    $fileName="informe_saldo.xlsx";
}

$objPHPExcel->setActiveSheetIndex(0);

//Write the Excel file to filename some_excel_file.xlsx in the current directory
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$writer = new Xlsx($objPHPExcel);
$writer->save('php://output');