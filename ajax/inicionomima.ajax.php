<?php
    
    $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $nomina = $_POST['selecTNominas'];

    if($nomina < 10){
        $nomina = '0'.$nomina;
    }

    $year = $_POST['anho'].'-'.$nomina.'-01';
    $otherYear =  date("Y-m-d",strtotime($year."+ 1 month"));// $_POST['anho'].'-'.$nomina.'-31';
    $otherYear =  date("Y-m-d",strtotime($otherYear."- 1 days"));
   // echo $otherYear;

    $campos = "count(*) as estado";
    $tabla = "gi_incapacidad";
    if($_SESSION['cliente_id'] != 0){
        $condiciones = "inc_estado_tramite !=  'EMPRESA' AND inc_empresa = ".$_SESSION['cliente_id']." AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }else{
        $condiciones = "inc_estado_tramite !=  'EMPRESA' AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
    }

    $respuestaX = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
    $intVariablePorcentage = 0;
?>

        <?php include __DIR__.'/../vistas/modulos/moduloInicio.php'; ?>

        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid box-danger">
                    <div class="box-header">
                        <h3 class="box-title">
                            INCAPACIDADES NOMINA VS PAGOS RECIBIDOS - <?php echo mb_strtoupper($month[$_POST['selecTNominas'] -1])." ".$_POST['anho'];?>
                        </h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <canvas id="pieChart_4" style="height:454px"></canvas>
                          
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="box box-danger box-solid">
                    <div class="box-header">
                        <h3 class="box-title">
                            INFORME GRAFICO - TOTAL PAGOS RECIBIDOS <?php echo mb_strtoupper($month[$_POST['selecTNominas'] -1])." ".$_POST['anho'];?>
                        </h3>
                    </div>
                    <div class="box-body">
                        
                        <canvas id="pieChart_3" style="height:454px"></canvas>
                    </div>
                </div>
            </div>
        </div>

       
        <?php include __DIR__.'/../vistas/modulos/moduloPagosAdministradoraInicio.php'; ?>
        <?php include __DIR__.'/../vistas/modulos/moduloInicioAlertas.php'; ?>
        <?php /*include __DIR__.'/../vistas/modulos/moduloAdministradora.php';*/ ?>

        <script type="text/javascript">
            $(function(){

                crearDonutChartTotal_2();
                get_barras_3();
                crearDonaGenero();
            
            });

            function crearDonutChartTotal_2(){
                <?php
                    $whereCliente = '';
                    if($_SESSION['cliente_id'] != 0){
                        $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
                    }

                    $campos = 'sum(inc_valor) as total, inc_origen ';
                    $tabla  = 'gi_incapacidad';
                    $condicion =  $whereCliente." inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
                    $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY inc_origen', 'ORDER BY inc_origen ASC');
                    $i = 0;

                    $labels = '';
                    $datos = '';
                    $colors = '';

                    foreach ($respuesta as $key => $value) {

                        if( $value['total'] != '' &&  $value['total'] != null){
                            if( $labels == ''){
                                $labels = '"'.$value['inc_origen'].'"';
                                $datos   = $value['total'];
                            }else{
                                $labels .= ' , "'.$value['inc_origen'].'"';
                                $datos   .= " , ".$value['total'];
                            }
                        }
                        
                    }
                ?>

                var densityCanvas = document.getElementById("pieChart_2").getContext('2d');

                var densityData = {
                    label: 'Valor Pagado',
                    data: [<?php echo $datos; ?>],
                    borderColor:['#dd4b39','#e65100','#00a65a','#ff6384','#00c0ef'],
                    backgroundColor : ['#dd4b39', '#e65100' , '#00a65a', '#ff6384' , '#00c0ef'],
                    borderWidth: 2,
                    hoverBorderWidth: 0,
                    fill: false
                };
                <?php
                    $campos = 'sum(inc_valor) as total ';
                    $tabla  = 'gi_incapacidad';
                    $condicion =  $whereCliente." inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
                    $respuestaPagos = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
                ?>
                var barChart = new Chart(densityCanvas, {
                    type: 'pie',
                    data: {
                        labels: [<?php echo $labels; ?>],
                        datasets:[densityData]
                    },
                    options: {
    title: {
      display: true,
      text: '$<?php echo number_format($respuestaPagos['total'], 0, ',', '.'); ?>',
      fontStyle: 'bold',
      fontSize: 30
    },
    legend: false,
    tooltips: {
      callbacks: {
        // this callback is used to create the tooltip label
        label: function(tooltipItem, data) {
          // get the data label and data value to display
          // convert the data value to local string so it uses a comma seperated number
          var dataLabel = data.labels[tooltipItem.index];
          var value = ': $ ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString();

          // make this isn't a multi-line label (e.g. [["label 1 - line 1, "line 2, ], [etc...]])
          if (Chart.helpers.isArray(dataLabel)) {
            // show value on first line of multiline label
            // need to clone because we are changing the value
            dataLabel = dataLabel.slice();
            dataLabel[0] += value;
          } else {
            dataLabel += value;
          }

          // return the text to display on the tooltip
          return dataLabel;
        }
      }
    }
  }
                });
   
            }

            function get_barras_3(){
                <?php 
                
                    $whereCliente = '';
                    if($_SESSION['cliente_id'] != 0){
                        $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
                    }

                    $campos = 'sum(inc_valor) as total, inc_fecha_pago ';
                    $tabla  = 'gi_incapacidad';
                    $condicion =  $whereCliente." inc_fecha_pago BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
                    $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY inc_fecha_pago', 'ORDER BY inc_fecha_pago ASC');
                    $i = 0;
                    $valoreP = '';
                    $nombresP ='';
                    

                    $labels = '';
                    $datos = '';
                    $colors = '';

                    foreach ($respuesta as $key => $value) {

                        if( $value['total'] != '' &&  $value['total'] != null){
                            if( $labels == ''){
                                $labels = '"'.$value['inc_fecha_pago'].'"';
                                $datos   = $value['total'];
                                $colors .= "getRandomColor()";
                            }else{
                                $labels .= ' , "'.$value['inc_fecha_pago'].'"';
                                $datos   .= " , ".$value['total'];
                                $colors .= ",getRandomColor()";
                            }
                        }
                        
                    }
                ?>

                var densityCanvas = document.getElementById("pieChart_3").getContext('2d');

                var densityData = {
                    label: 'Valor Pagado',
                    data: [<?php echo $datos; ?>],
                    borderColor:'#36a2eb',
                    backgroundColor : '#36a2eb',
                    borderWidth: 2,
                    hoverBorderWidth: 0,
                    fill: false
                };

                var barChart = new Chart(densityCanvas, {
                    type: 'line',
                    data: {
                        labels: [<?php echo $labels; ?>],
                        datasets: [densityData],
                    },
                    options: {
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    return "Valor Pagado : $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                                    });
                                }
                            }
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    callback: function (value) {
                                        return addCommas(value)
                                    }
                                }
                            }]
                        }
                    }
                });

            }

            function crearDonaGenero(){
                <?php 
                    $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

                    $whereCliente = '';
                    if($_SESSION['cliente_id'] != 0){
                        $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
                    }

                    $campos = 'SUM(inc_valor_pagado_eps) as total';
                    $tabla  = 'gi_incapacidad';
                    $condicion =  $whereCliente." inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite != 'EMPRESA'";
                    $porpagar = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);

                    $campos = 'SUM(inc_valor) as total';
                    $tabla  = 'gi_incapacidad';
                    $condicion =  $whereCliente." inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
                    $pagadas = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);

                    /*Valor Administradora*/
                    $campos = "sum(inc_valor_pagado_empresa) as total";
                    $tabla = "gi_incapacidad";
                    if($_SESSION['cliente_id'] != 0){
                        $condiciones = "inc_empresa = ".$_SESSION['cliente_id']." AND inc_valor_pagado_empresa IS NOT NULL AND  inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                    }else{
                        $condiciones = " inc_valor_pagado_empresa IS NOT NULL AND inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."'";
                    }
                    $respuesta22 = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);

                    $totaValor11 = $respuesta22['total'];
                ?>

                var densityCanvas = document.getElementById("pieChart_4").getContext('2d');

                var densityData_empresa = {
                    label: 'Valor Empresa',
                    data: [<?php echo $totaValor11; ?>],
                    borderColor:'#00a65a',
                    backgroundColor : '#00a65a',
                    borderWidth: 2,
                    hoverBorderWidth: 0,
                    fill: false
                };

                var densityData = {
                    label: 'Valor Administradoras',
                    data: [<?php echo $porpagar['total'];?>],
                    borderColor:'#ff6384',
                    backgroundColor : '#ff6384',
                    borderWidth: 2,
                    hoverBorderWidth: 0,
                    fill: false
                };

                var densityData_pagadas = {
                    label: 'Pagos Recibidos',
                    data: [<?php echo $pagadas['total']; ?>],
                    borderColor:'#36a2eb',
                    backgroundColor : '#36a2eb',
                    borderWidth: 2,
                    hoverBorderWidth: 0,
                    fill: false
                };

                var barChart = new Chart(densityCanvas, {
                    type: 'bar',
                    data: {
                        labels: ["<?php echo $month[$_POST['selecTNominas'] -1]; ?>"],
                        datasets: [densityData_empresa, densityData, densityData_pagadas],
                    },
                    options: {
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex].label + " : $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;                                  
                                    });
                                }
                                /*label: function (tooltipItems, data) {
                                    var i, label = [], l = data.datasets.length;
                                    for (i = 0; i < l; i += 1) {
                                        label[i] = data.datasets[i].label + ' : ' + '$' + data.datasets[i].data[tooltipItems.index];
                                    }
                                    return label;
                                }*/
                            }
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    callback: function (value) {
                                        return addCommas(value)
                                    },
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            }

            function addCommas(nStr)
            {
                nStr += '';
                x = nStr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
            }

            function scaleLabel (valuePayload) {
                return Number(valuePayload.value).toFixed(2).replace('.',',') + '$';
            }

            function getRandomColor() {
               return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
            }


        </script>
        <script type="text/javascript">
            $('.tblOther').DataTable({
                "language" : {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando _START_ al _END_ de  _TOTAL_",
                    "sInfoEmpty":      "No hay registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "<<",
                        "sLast":     ">>",
                        "sNext":     ">",
                        "sPrevious": "<"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                "pageLength": 10
            });
        </script>