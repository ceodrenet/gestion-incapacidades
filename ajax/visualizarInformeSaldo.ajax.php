<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<?php
session_start();
$_SESSION['start'] = time();
ini_set('display_errors', 'On');
ini_set('display_errors', 1);
require_once '../controladores/mail.controlador.php';
require_once '../controladores/plantilla.controlador.php';
require_once '../controladores/incapacidades.controlador.php';
require_once '../controladores/tesoreria.controlador.php';

require_once '../modelos/dao.modelo.php';
require_once '../modelos/incapacidades.modelo.php';
require_once '../modelos/tesoreria.modelo.php';

$months = array(
    1 => "Enero", 2 => "Febrero", 3 => "Marzo", 4 => "Abril", 5 => "Mayo", 6 => "Junio", 7 => "Julio",
    8 => "Agosto", 9 => "Septiembre", 10 => "Octubre", 11 => "Noviembre", 12 => "Diciembre"
);

$filtroFecha = $_POST['tipoFecha'];

if ($_POST['year'] != 0) {

    $whereTotalPag = "(inc_estado_tramite = 5 OR inc_estado_tramite = 8)";
    $whereTotalPen = "(inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7)";
    $whereTotalSin = "inc_estado_tramite = 6";

    $fieldPagByMes = "MONTH(".$filtroFecha.") mes, SUM(inc_valor) as total, COUNT(*) as cant_mes";
    $wherePagByMes = "MONTH(".$filtroFecha.") BETWEEN 1 AND 12 AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8)";

    $fieldPenByMes = "MONTH(".$filtroFecha.") mes, SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_mes";
    $wherePenByMes = "MONTH(".$filtroFecha.") BETWEEN 1 AND 12 AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7)";

    $fieldSinByMes = "MONTH(".$filtroFecha.") mes, SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_mes";
    $whereSinByMes = "MONTH(".$filtroFecha.") BETWEEN 1 AND 12 AND (inc_estado_tramite = 6)";

    $groupBy = "GROUP BY MONTH(".$filtroFecha.")";
    $orderBy = "ORDER BY MONTH(".$filtroFecha.")";

    $fecha1 = $_POST['year'] . "-01-01";
    $fecha2 = $_POST['year'] . "-12-31";

    $whereTotalPag.=" AND ".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."'";
    $whereTotalPen.=" AND ".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."'";
    $whereTotalSin.=" AND ".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."'";

    $wherePagByMes.=" AND YEAR(".$filtroFecha.") = '".$_POST["year"]."'";
    $wherePenByMes.=" AND YEAR(".$filtroFecha.") = '".$_POST["year"]."'";
    $whereSinByMes.=" AND YEAR(".$filtroFecha.") = '".$_POST["year"]."'";


    if ($_SESSION['cliente_id'] != 0) {
        $whereTotalPag .= " AND inc_empresa = " . $_SESSION['cliente_id'];
        $whereTotalPen .= " AND inc_empresa = " . $_SESSION['cliente_id'];
        $whereTotalSin .= " AND inc_empresa = " . $_SESSION['cliente_id'];

        $wherePagByMes .= " AND inc_empresa = " . $_SESSION['cliente_id'];
        $wherePenByMes .= " AND inc_empresa = " . $_SESSION['cliente_id'];
        $whereSinByMes .= " AND inc_empresa = " . $_SESSION['cliente_id'];
    }

    $totalPagadas = ModeloTesoreria::mdlMostrarUnitario("SUM(inc_valor) as total_pag, COUNT(*) as cantidad", "gi_incapacidad", $whereTotalPag);
    $totalPendientes = ModeloTesoreria::mdlMostrarUnitario("SUM(inc_valor_pagado_eps) as total_pen, COUNT(*) as cantidad", "gi_incapacidad", $whereTotalPen);
    $totalSinReconocimiento = ModeloTesoreria::mdlMostrarUnitario("SUM(inc_valor_pagado_eps) as total_sin, COUNT(*) as cantidad", "gi_incapacidad", $whereTotalSin);

    $saldosPagByMes = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldPagByMes, "gi_incapacidad", $wherePagByMes, $groupBy, $orderBy);
    $saldosPenByMes = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldPenByMes, "gi_incapacidad", $wherePenByMes, $groupBy, $orderBy);
    $saldosSinByMes = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldSinByMes, "gi_incapacidad", $whereSinByMes, $groupBy, $orderBy);

    $listSaldosPag = [];
    $listSaldosPen = [];
    $listSaldosSin = [];
    $listTotalRecibidas = [];

    $totalMesRecibidas = 0;
    $cantMesRecibidas = 0;
    $iPag = 0;
    $iPen = 0;
    $iSin = 0;
    $j = 0;
    foreach ($months as $key => $mes) {

        $totalPag = 0;
        $totalPen = 0;
        $totalSin = 0;
        $cantPag = 0;
        $cantPen = 0;
        $cantSin = 0;
        
        if (isset($saldosPagByMes[$iPag]["mes"])) {
            if ($key == $saldosPagByMes[$iPag]["mes"]) {
                if (!empty($saldosPagByMes[$iPag]["total"])) {
                    $totalPag = $saldosPagByMes[$iPag]["total"];
                }
                if (!empty($saldosPagByMes[$iPag]["cant_mes"])) {
                    $cantPag = $saldosPagByMes[$iPag]["cant_mes"];
                }
                $listSaldosPag[$j]["valor"] = $totalPag;
                $listSaldosPag[$j]["cantidad"] = $cantPag;
                $iPag++;
            } else {
                $listSaldosPag[$j]["valor"] = 0;
                $listSaldosPag[$j]["cantidad"] = 0;
            }
        } else {
            $listSaldosPag[$j]["valor"] = 0;
            $listSaldosPag[$j]["cantidad"] = 0;
        }

        if (isset($saldosPenByMes[$iPen]["mes"])) {
            if ($key == $saldosPenByMes[$iPen]["mes"]) {
                if (!empty($saldosPenByMes[$iPen]["total"])) {
                    $totalPen = $saldosPenByMes[$iPen]["total"];
                }
                if (!empty($saldosPenByMes[$iPen]["cant_mes"])) {
                    $cantPen = $saldosPenByMes[$iPen]["cant_mes"];
                }
                $listSaldosPen[$j]["valor"] = $totalPen;
                $listSaldosPen[$j]["cantidad"] = $cantPen;
                $iPen++;
            } else {
                $listSaldosPen[$j]["valor"] = 0;
                $listSaldosPen[$j]["cantidad"] = 0;
            }
        } else {
            $listSaldosPen[$j]["valor"] = 0;
            $listSaldosPen[$j]["cantidad"] = 0;
        }

        if (isset($saldosSinByMes[$iSin]["mes"])) {
            if ($key == $saldosSinByMes[$iSin]["mes"]) {
                if (!empty($saldosSinByMes[$iSin]["total"])) {
                    $totalSin = $saldosSinByMes[$iSin]["total"];
                }
                if (!empty($saldosSinByMes[$iSin]["cant_mes"])) {
                    $cantSin = $saldosSinByMes[$iSin]["cant_mes"];
                }
                $listSaldosSin[$j]["valor"] = $totalSin;
                $listSaldosSin[$j]["cantidad"] = $cantSin;
                $iSin++;
            } else {
                $listSaldosSin[$j]["valor"] = 0;
                $listSaldosSin[$j]["cantidad"] = 0;
            }
        } else {
            $listSaldosSin[$j]["valor"] = 0;
            $listSaldosSin[$j]["cantidad"] = 0;
        }

        $totalMes = $totalPag + $totalPen + $totalSin;
        $totalCant = $cantPag + $cantPen + $cantSin;

        $totalMesRecibidas += $totalMes;
        $cantMesRecibidas += $totalCant;

        $porcentajeMesPag = 0;
        $porcentajeMesPen = 0;
        $porcentajeMesSin = 0;

        if ($totalMes > 0) {
            $porcentajeMesPag = ($totalPag * 100) / $totalMes;
            $porcentajeMesPen = ($totalPen * 100) / $totalMes;
            $porcentajeMesSin = ($totalSin * 100) / $totalMes;
        }

        $totalPorcentajeMes = $porcentajeMesPag + $porcentajeMesPen + $porcentajeMesSin;
        
        $listSaldosPag[$j]["porcentaje"] = number_format($porcentajeMesPag, 2);
        $listSaldosPen[$j]["porcentaje"] = number_format($porcentajeMesPen, 2);
        $listSaldosSin[$j]["porcentaje"] = number_format($porcentajeMesSin, 2);

        $listTotalRecibidas[$j]["valor"] = $totalMes;
        $listTotalRecibidas[$j]["cantidad"] = $totalCant;
        $listTotalRecibidas[$j]["porcentaje"] = $totalPorcentajeMes;

        $j++;
    }

    $porcentajePag = 0;
    $porcentajePen = 0;
    $porcentajeSin = 0;
    $porcentajeTotal = 0;

    if ($totalMesRecibidas > 0) {
        $porcentajePag = ($totalPagadas["total_pag"] * 100) / $totalMesRecibidas;
        $porcentajePen = ($totalPendientes["total_pen"] * 100) / $totalMesRecibidas;
        $porcentajeSin = ($totalSinReconocimiento["total_sin"] * 100) / $totalMesRecibidas;
        $porcentajeTotal = $porcentajePag + $porcentajePen + $porcentajeSin;
    }
} else {
    $fieldYears = "YEAR($filtroFecha) as year ";
    $whereYears = "(inc_estado_tramite != 1) ";
    $groupBy = "GROUP BY YEAR($filtroFecha) ";
    $orderBy = "ORDER BY YEAR($filtroFecha) DESC";

    $fieldYearsPag = "YEAR($filtroFecha) as year, SUM(inc_valor) as total, COUNT(*) as cant_year";
    $fieldYearsPen = "YEAR($filtroFecha) as year, SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_year";
    $fieldYearsSin = "YEAR($filtroFecha) as year, SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_year";
    $fieldTotalYearPag = "SUM(inc_valor) as total, COUNT(*) as cant_year";
    $fieldTotalYearPen = "SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_year";
    $fieldTotalYearSin = "SUM(inc_valor_pagado_eps) as total, COUNT(*) as cant_year";

    $whereYearsPag = "(inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereYearsPen = "(inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereYearsSin = "(inc_estado_tramite = 6) ";

    if($_SESSION['cliente_id'] != 0) {

        $whereYears.="AND inc_empresa = ".$_SESSION['cliente_id'];
        $whereYearsPag.="AND inc_empresa = ".$_SESSION['cliente_id'];
        $whereYearsPen.="AND inc_empresa = ".$_SESSION['cliente_id'];
        $whereYearsSin.="AND inc_empresa = ".$_SESSION['cliente_id'];
    }

    $totalYearPag = ModeloTesoreria::mdlMostrarUnitario($fieldTotalYearPag, "gi_incapacidad", $whereYearsPag);
    $totalYearPen = ModeloTesoreria::mdlMostrarUnitario($fieldTotalYearPen, "gi_incapacidad", $whereYearsPen);
    $totalYearSin = ModeloTesoreria::mdlMostrarUnitario($fieldTotalYearSin, "gi_incapacidad", $whereYearsSin);

    $allYears = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldYears, "gi_incapacidad", $whereYears, $groupBy, $orderBy);
    $dataYearsPag = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldYearsPag, "gi_incapacidad", $whereYearsPag, $groupBy, $orderBy);
    $dataYearsPen = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldYearsPen, "gi_incapacidad", $whereYearsPen, $groupBy, $orderBy);
    $dataYearsSin = ModeloTesoreria::mdlMostrarGroupAndOrder($fieldYearsSin, "gi_incapacidad", $whereYearsSin, $groupBy, $orderBy);

    $listSaldosYearPag = [];
    $listSaldosYearPen = [];
    $listSaldosYearSin = [];
    $listTotalYearRecibidas = [];

    $i = 0;
    foreach ($allYears as $value) {
        
        $yearPag = 0; $yearPen = 0; $yearSin = 0;
        $cantPag = 0; $cantPen = 0; $cantSin = 0;

        foreach($dataYearsPag as $yearEst1){
            if($yearEst1["year"] == $value["year"]){
                $yearPag = $yearEst1["total"];
                $cantPag = $yearEst1["cant_year"];
                $listSaldosYearPag[$i]["valor"] = $yearPag;
                $listSaldosYearPag[$i]["cantidad"] = $cantPag;
                break;
            } else {
                $listSaldosYearPag[$i]["valor"] = 0;
                $listSaldosYearPag[$i]["cantidad"] = 0;
            }
        }

        foreach($dataYearsPen as $yearEst2){
            if($yearEst2["year"] == $value["year"]){
                $yearPen = $yearEst2["total"];
                $cantPen = $yearEst2["cant_year"];
                $listSaldosYearPen[$i]["valor"] = $yearPen;
                $listSaldosYearPen[$i]["cantidad"] = $cantPen;
                break;
            } else {
                $listSaldosYearPen[$i]["valor"] = 0;
                $listSaldosYearPen[$i]["cantidad"] = 0;
            }
        }

        foreach($dataYearsSin as $yearEst3){
            if($yearEst3["year"] == $value["year"]){
                $yearSin = $yearEst3["total"];
                $cantSin = $yearEst3["cant_year"];
                $listSaldosYearSin[$i]["valor"] = $yearSin;
                $listSaldosYearSin[$i]["cantidad"] = $cantSin;
                break;
            } else {
                $listSaldosYearSin[$i]["valor"] = 0;
                $listSaldosYearSin[$i]["cantidad"] = 0;
            }
        }
        
        $totalSaldosYear = $yearPag + $yearPen + $yearSin;
        $totalCantYear = $cantPag + $cantPen + $cantSin;
        $porPag = 0;
        $porPen = 0;
        $porSin = 0;

        if($totalSaldosYear > 0){
            $porPag = ($yearPag * 100) / $totalSaldosYear;
            $porPen = ($yearPen * 100) / $totalSaldosYear;
            $porSin = ($yearSin * 100) / $totalSaldosYear;
        }

        $porTotalYear = $porPag+$porPen+$porSin;
        $listSaldosYearPag[$i]["porcentaje"] = number_format($porPag,2);
        $listSaldosYearPen[$i]["porcentaje"] = number_format($porPen,2);
        $listSaldosYearSin[$i]["porcentaje"] = number_format($porSin,2);

        $listTotalYearRecibidas[$i]["valor"] = $totalSaldosYear;
        $listTotalYearRecibidas[$i]["cantidad"] = $totalCantYear;
        $listTotalYearRecibidas[$i]["porcentaje"] = $porTotalYear;

        $i++;
    }
    $porTotalPag = 0;
    $porTotalPen = 0;
    $porTotalSin = 0;
    $porTotalAll = 0;

    $totalSaldosAll = $totalYearPag["total"] + $totalYearPen["total"] + $totalYearSin["total"];
    $totalCantAll = $totalYearPag["cant_year"] + $totalYearPen["cant_year"] + $totalYearSin["cant_year"];

    if ($totalSaldosAll > 0) {
        $porTotalPag = ($totalYearPag["total"] * 100) / $totalSaldosAll;
        $porTotalPen = ($totalYearPen["total"] * 100) / $totalSaldosAll;
        $porTotalSin = ($totalYearSin["total"] * 100) / $totalSaldosAll;
    }

    $porTotalAll = $porTotalPag + $porTotalPen + $porTotalSin;
}
                                                        
?>

<style>
    table,
    tr,
    td,
    th {
        border: none;
    }
    .scroll{
 
        overflow-x: auto;
    }

    .overlay {
        /* background-color: #E5DCDC; */
        opacity: .4;
    }

</style>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    INFORME DE SALDOS
                </h5>
            </div>
            <div class="card-body p-0 mt-3">
                <div class="d-flex justify-content-around scroll mx-0">
                    <div class="text-center p-0 m-0">
                        <strong>ESTADO INCAPACIDAD</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>AÑO</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>
                                    <?php
                                    if ($_POST['year'] != 0) {
                                        echo $_POST['year'];
                                    } else {
                                        echo "ACUMULADO";
                                    }
                                    
                                    ?>
                                    </th>
                                </tr>
                                <?php
                                if ($_POST['year'] != 0) {
                                    foreach ($months as $key => $value) {
                                        echo "<tr>";
                                        echo "<td>$value</td>";
                                        echo "</tr>";
                                    }
                                } else {
                                    foreach ($allYears as $value) {
                                        echo "<tr>";
                                        echo "<td>".$value["year"]."</td>";
                                        echo "</tr>";
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>TOTAL</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <?php if ($_POST["estadoTramite"] == "1") {?>
                    <div class="text-center p-0 m-0">
                        <strong>1. PAGADAS</strong>
                        <table class="table ">
                            <thead>
                                <tr class="p-0 m-0">
                                    <td>VALOR($)</td>
                                    <td>#</td>
                                    <td>%PERIODO</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalPagadas["total_pag"], 0, ',', '.')."</th>";
                                echo "<th>".$totalPagadas["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajePag,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosPag as $value) {
                                echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearPag["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearPag["cant_year"]."</th>";
                                echo "<th>".number_format($porTotalPag,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosYearPag as $value) {
                                 echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php 
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalPagadas["total_pag"], 0, ',', '.')."</th>";
                                echo "<th>".$totalPagadas["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajePag,2)."</th>";
                                echo "</tr>";
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearPag["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearPag["cant_year"]."</th>";
                                echo "<th>".number_format($porTotalPag,2)."</th>";
                                echo "</tr>";
                            }
                            ?>
                            </tfoot>
                        </table>
                    
                    </div>
                    <?php } else if($_POST["estadoTramite"] == "2") {?>
                    <div class="text-center p-0 m-0">
                        <strong>2. PENDIENTES</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>VALOR($)</td>
                                    <td>#</td>
                                    <td>%PERIODO</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalPendientes["total_pen"], 0, ',', '.')."</th>";
                                echo "<th>".$totalPendientes["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajePen,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosPen as $value) {
                                echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearPen["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearPen["cant_year"]."</th>";
                                echo "<th>".number_format($porTotalPen,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosYearPen as $value) {
                                 echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php 
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalPendientes["total_pen"], 0, ',', '.')."</th>";
                                echo "<th>".$totalPendientes["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajePen,2)."</th>";
                                echo "</tr>";
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearPen["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearPen["cant_year"]."</th>";
                                echo "<th>".number_format($porTotalPen,2)."</th>";
                                echo "</tr>";
                            }
                            ?>
                            </tfoot>
                        </table>
                    </div>
                    <?php } else if($_POST["estadoTramite"] == "3") {?>
                    <div class="text-center p-0 m-0">
                        <strong>3. SIN RECONOCIMIENTO</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>VALOR($)</td>
                                    <td>#</td>
                                    <td>%PERIODO</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalSinReconocimiento["total_sin"], 0, ',', '.')."</th>";
                                echo "<th>".$totalSinReconocimiento["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajeSin,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosSin as $value) {
                                echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearSin["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearSin["cant_year"]."</th>";
                                echo "<th>".number_format($porTotalSin,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosYearSin as $value) {
                                 echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalSinReconocimiento["total_sin"], 0, ',', '.')."</th>";
                                echo "<th>".$totalSinReconocimiento["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajeSin,2)."</th>";
                                echo "</tr>";
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearSin["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearSin["cant_year"]."</th>";
                                echo "<th>".number_format($porTotalSin,2)."</th>";
                                echo "</tr>";
                            } 
                            ?>
                            </tfoot>
                        </table>
                    </div>
                    <?php } else {?>
                    <div class="text-center p-0 m-0">
                        <strong>1. PAGADAS</strong>
                        <table class="table ">
                            <thead>
                                <tr class="p-0 m-0">
                                    <td>VALOR($)</td>
                                    <td>#</td>
                                    <td>%PERIODO</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalPagadas["total_pag"], 0, ',', '.')."</th>";
                                echo "<th>".$totalPagadas["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajePag,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosPag as $value) {
                                echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearPag["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearPag["cant_year"]."</th>";
                                echo "<th>".number_format($porTotalPag,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosYearPag as $value) {
                                 echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php 
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalPagadas["total_pag"], 0, ',', '.')."</th>";
                                echo "<th>".$totalPagadas["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajePag,2)."</th>";
                                echo "</tr>";
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearPag["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearPag["cant_year"]."</th>";
                                echo "<th>".number_format(0,2)."</th>";
                                echo "</tr>";
                            }
                            ?>
                            </tfoot>
                        </table>
                    
                    </div>    
                    <div class="text-center p-0 m-0">
                        <strong>2. PENDIENTES</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>VALOR($)</td>
                                    <td>#</td>
                                    <td>%PERIODO</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalPendientes["total_pen"], 0, ',', '.')."</th>";
                                echo "<th>".$totalPendientes["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajePen,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosPen as $value) {
                                echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearPen["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearPen["cant_year"]."</th>";
                                echo "<th>".number_format($porTotalPen,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosYearPen as $value) {
                                 echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php 
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalPendientes["total_pen"], 0, ',', '.')."</th>";
                                echo "<th>".$totalPendientes["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajePen,2)."</th>";
                                echo "</tr>";
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearPen["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearPen["cant_year"]."</th>";
                                echo "<th>".number_format($porTotalPen,2)."</th>";
                                echo "</tr>";
                            }
                            ?>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-center p-0 m-0">
                        <strong>3. SIN RECONOCIMIENTO</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>VALOR($)</td>
                                    <td>#</td>
                                    <td>%PERIODO</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalSinReconocimiento["total_sin"], 0, ',', '.')."</th>";
                                echo "<th>".$totalSinReconocimiento["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajeSin,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosSin as $value) {
                                echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearSin["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearSin["cant_year"]."</th>";
                                echo "<th>".number_format($porTotalSin,2)."</th>";
                                echo "</tr>";
                                foreach ($listSaldosYearSin as $value) {
                                 echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalSinReconocimiento["total_sin"], 0, ',', '.')."</th>";
                                echo "<th>".$totalSinReconocimiento["cantidad"]."</th>";
                                echo "<th>".number_format($porcentajeSin,2)."</th>";
                                echo "</tr>";
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalYearSin["total"], 0, ',', '.')."</th>";
                                echo "<th>".$totalYearSin["cant_year"]."</th>";
                                echo "<th>".number_format($porTotalSin,2)."</th>";
                                echo "</tr>";
                            } 
                            ?>
                            </tfoot>
                        </table>
                    </div>
                    <?php } ?>
                    <div class="text-center p-0 m-0">
                        <strong>TOTAL RECIBIDAS</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>VALOR ($)</td>
                                    <td>#</td>
                                    <td>% PERIODO</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalMesRecibidas, 0, ',', '.')."</th>";
                                echo "<th>".$cantMesRecibidas."</th>";
                                echo "<th>".number_format($porcentajeTotal,2)."</th>";
                                echo "</tr>";
                                foreach ($listTotalRecibidas as $value) {
                                echo "<tr>";
                                    echo "<th>".number_format($value["valor"], 0, ',', '.')."</th>";
                                    echo "<th>".$value["cantidad"]."</th>";
                                    echo "<th>".number_format($value["porcentaje"],2)."</th>";
                                echo "</tr>";
                                }
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalSaldosAll, 0, ',', '.')."</th>";
                                echo "<th>".$totalCantAll."</th>";
                                echo "<th>".number_format($porTotalAll,2)."</th>";
                                echo "</tr>";
                                foreach ($listTotalYearRecibidas as $value) {
                                 echo "<tr>";
                                    echo "<td>".number_format($value["valor"], 0, ',', '.')."</td>";
                                    echo "<td>".$value["cantidad"]."</td>";
                                    echo "<td>".number_format($value["porcentaje"],2)."</td>";
                                echo "</tr>";
                                }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php
                            if($_POST['year'] != 0){
                                echo "<tr>";
                                echo "<th>".number_format($totalMesRecibidas, 0, ',', '.')."</th>";
                                echo "<th>".$cantMesRecibidas."</th>";
                                echo "<th>".number_format($porcentajeTotal,2)."</th>";
                                echo "</tr>";
                            }else{
                                echo "<tr>";
                                echo "<th>".number_format($totalSaldosAll, 0, ',', '.')."</th>";
                                echo "<th>".$totalCantAll."</th>";
                                echo "<th>".number_format($porTotalAll,2)."</th>";
                                echo "</tr>";
                            } 
                            
                            ?>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>