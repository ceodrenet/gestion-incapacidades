<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/usuariosEps.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/usuariosEps.modelo.php';

	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxUsuariosEps
	{

		public $id_usuarioE;
		
		public function ajaxEditarUsuariosEps(){
			$item = 'use_id_i';
			$valor = $this->id_usuarioE;
			$respuesta = ControladorUsuariosEps::getData('gi_usuarios_eps', $item, $valor);
			echo json_encode($respuesta);
		}
	}
	
	
	if(isset($_POST['EditarUsuarioEpsId'])){
		if($_POST['EditarUsuarioEpsId'] != ''){
			$editar = new AjaxUsuariosEps();
			$editar->id_usuarioE = $_POST['EditarUsuarioEpsId'];
			$editar->ajaxEditarUsuariosEps();
		}	
	}


	if(isset($_POST['NuevoNombreUsuario'])){
       echo ControladorUsuariosEps::ctrCrearUsuariosEps();
	}


	if(isset($_POST["id_usuario_eps"])){
       echo ControladorUsuariosEps::ctrBorrarUsuariosEps();
	}


	if(isset($_POST['EditarNombreUsuario'])){
		 echo ControladorUsuariosEps::ctrEditarUsuariosEps();
	}

	if(isset($_GET['getDataUsuariosEps'])){
		$where = '';
		$campos = "use_id_i, ips_nombre, use_usuario_v, use_password_v, use_url_v";
		$tablas = "gi_usuarios_eps JOIN gi_ips ON use_ips_id_i = ips_id ";
		$usuariosEps = [];
		
		if($_SESSION['cliente_id'] != 0){
			$where = 'use_emp_id_i = '.$_SESSION['cliente_id'];
			$condic = $where;
			$usuariosEps = ControladorUsuariosEps::getDataFromLsql($campos,$tablas,$condic, null, null);
		} else {
			$usuariosEps = ControladorUsuariosEps::getDataFromLsql($campos,$tablas,null, null, null);
		}


echo '{
		"data" : [';
		$i=0;
		foreach ($usuariosEps as $key => $value) {
			if ($i != 0) {
				echo ",";
			}

			echo '[';
			echo '"'.preg_replace("/[\r\n|\n|\r]+/", " ", $value["ips_nombre"]).'",';
			echo '"'.preg_replace("/[\r\n|\n|\r]+/", " ", $value["use_usuario_v"]).'",';
			echo '"'.$value["use_password_v"].'",';
			if($_SESSION['cliente_id'] != 0){
				echo '"'.preg_replace("/[\r\n|\n|\r]+/", " ", $value["use_url_v"]).'",';
			}else{
				echo '"",';
			}
			echo '"'.$value["use_id_i"].'"';
			echo ']';
			$i++;
		}
		echo ']
		}';
	}
