<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/diagnostico.controlador.php';
	require_once '../modelos/diagnostico.modelo.php';

	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxDiagnostico
	{
		public $diagnostico;
		public function getDiagnosticos(){
			$valor = $this->diagnostico;
			$respuesta = ControladorDiagnostico::ctrBuscarDiagnosticos($valor);
			//var_dump($respuesta);
			$i = 0;
			$datos = array();
			foreach ($respuesta as $key => $value) {
				$datos[$i]['id'] = $value['id'];
				$datos[$i]['text'] = $value['id'] .' - '.$value['name'];
				$i++;
			} 
			echo json_encode($datos);
		}
	}


	if(isset($_GET['getDiagnosticos'])){

		$AjaxDiagnostico = new AjaxDiagnostico();
		$AjaxDiagnostico->diagnostico = $_POST['query'];
		$AjaxDiagnostico->getDiagnosticos();
	}
