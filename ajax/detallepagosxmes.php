<?php
	session_start();
	$_SESSION['start'] = time();
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
	require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
    require_once '../controladores/incapacidades.controlador.php';
    require_once '../controladores/tesoreria.controlador.php';
    
    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';
	




	$semes1 = array('01', '02', '03', '04', '05', '06');
	$semes2 = array('07', '08', '09', '10', '11', '12');
	$seme = $_POST['semestrebusq'];
	$tablaThead = '';
	if($seme == 1){
		$year = $_POST['anhoBusqueda'].'-01-01';
		$otherYear = $_POST['anhoBusqueda'].'-06-30';
		$tablaThead = '<th colspan="3" style="text-align: center;">ENERO</th><th colspan="3" style="text-align: center;">FEBRERO</th><th colspan="3" style="text-align: center;">MARZO</th><th colspan="3" style="text-align: center;">ABRIL</th><th colspan="3" style="text-align: center;">MAYO</th><th colspan="3" style="text-align: center;">JUNIO</th>';
	}else{
		$year = $_POST['anhoBusqueda'].'-07-01';
		$otherYear = $_POST['anhoBusqueda'].'-12-31';
		$tablaThead = '<th colspan="3" style="text-align: center;">JULIO</th><th colspan="3" style="text-align: center;">AGOSTO</th><th colspan="3" style="text-align: center;">SEPTIEMBRE</th><th colspan="3" style="text-align: center;">ACTUBRE</th><th colspan="3" style="text-align: center;">NOVIEMBRE</th><th colspan="3" style="text-align: center;">DICIEMBRE</th>';
	}
	/**/

?>

<div class="row">
    <div class="col-md-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    DATOS DE INCAPACIDADES GRAFICO 
                </h5>
            </div>
            <div class="card-body">
                <canvas id="pieChart_2" style="height:200px"></canvas>
            </div>
        </div>
    </div>
</div>

 <div class="card border border-danger">
    <div class="card-header bg-transparent border-danger">
        <h5 class="my-0 text-danger">
            DATOS DE INCAPACIDADES
        </h5>
    </div>
   
        <table class="table-condensed table-bordered table-hover"widht="100%">
        	<thead>
        		<tr style="text-align: center;">
        			<th>
        				
        			</th>
        			<?php echo $tablaThead; ?>
        		</tr>
        		<tr>
        			<th></th>
        			<th>VALOR</th>
        			<th>#</th>
        			<th>%</th>
        			<th>VALOR</th>
        			<th>#</th>
        			<th>%</th>
        			<th>VALOR</th>
        			<th>#</th>
        			<th>%</th>
        			<th>VALOR</th>
        			<th>#</th>
        			<th>%</th>
        			<th>VALOR</th>
        			<th>#</th>
        			<th>%</th>
        			<th>VALOR</th>
        			<th>#</th>
        			<th>%</th>
        		</tr>
        	</thead>
        	<?php
        		$empres = '';
        		$admini = '';
        		$adminP = '';
        		$ajuste = '';
        		$totalTH = '';
        		
        		$porcob = '';
        		$pagada = '';
        		$negada = '';
        		$busque = 0;
        		if($seme == 1){
        			$busque  = $semes1;
        		}else{
        			$busque  = $semes2;
        		}
        		 $whereCliente = '';
	            if($_SESSION['cliente_id'] != 0){
	                $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
	            }

        		for ($i=0; $i < 6 ; $i++) { 
        			$totalLC = 0;
        			$totalMC = 0;
        			$totalNC = 0;
        			$incapacidadesTotal = 0;

        			/*Total incapacidades de ese mes*/
        			$campos = 'COUNT(*) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda']." AND inc_estado_tramite !=  'EMPRESA'";
	            	$incapacidadesTotal = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$incapacidadesTotal = $incapacidadesTotal['total'];
	            	$totalNC = $incapacidadesTotal;



        			/*Incapaciodades Pagadas Empresa*/
        			$campos = 'SUM(inc_valor_pagado_empresa) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda']." ";
	            	
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$empres .= '<td style="text-align:justify;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
	            	$totalLC += $incapacidades['total'];
	            	$totalInEmp = $incapacidades['total'];


	            	/*Incapacidades de ese Año*/
        			$campos = 'COUNT(*) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_valor_pagado_empresa IS NOT NULL';
	            	//echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$totalInc = 0;
	            	if($incapacidades['total'] > 0){
	            		$totalInc = $incapacidades['total'];
	            	}
	            	$empres .= '<td style="text-align:justify;">'.$totalInc.'</td>';
	            	//$totalMC += $totalInc;



	            	


	            	/*Administradora*/
	            	/*Incapaciodades Pagadas*/
        			$campos = 'SUM(inc_valor_pagado_eps) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda']." AND inc_estado_tramite !=  'EMPRESA'";
	            
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$admini .= '<td style="text-align:justify;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
	            	$adminP .= '<td style="text-align:justify;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
	            	$totalLC += $incapacidades['total'];
	            	$totalInAdmi = $incapacidades['total'];



	            	/*Incapacidades de ese Año*/
        			$campos = 'COUNT(*) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." inc_estado_tramite !=  'EMPRESA' AND  MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_valor_pagado_eps IS NOT NULL ';
	            	//echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$totalInc = 0;
	            	if($incapacidades['total'] > 0){
	            		$totalInc = $incapacidades['total'];
	            	}
	            	$admini .= '<td style="text-align:justify;">'.$totalInc.'</td>';
	            	$adminP .= '<td style="text-align:justify;">'.$totalInc.'</td>';
	            	//$totalMC += $totalInc;

            	


	            	/*Ajuste*/
	            	/*Incapaciodades Pagadas*/
        			$campos = 'SUM(inc_valor_pagado_ajuste) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda'];
	            	
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$ajuste .= '<td style="text-align:justify;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
	            	$totalLC += $incapacidades['total'];
	            	$totalInAju = $incapacidades['total'];



	            	/*Incapacidades de ese Año*/
        			$campos = 'COUNT(*) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_valor_pagado_ajuste IS NOT NULL';
	            	//echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$totalInc = 0;
	            	if($incapacidades['total'] > 0){
	            		$totalInc = $incapacidades['total'];
	            	}
	            	$ajuste .= '<td style="text-align:justify;">'.$totalInc.'</td>';
	            	//$totalMC += $totalInc;

	            	
	            	if($incapacidadesTotal > 0){
	            		$total = $totalNC * 100 / $incapacidadesTotal;
	            	}

	            	$totalTH .= '<td style="text-align:justify;">$'.number_format($totalLC, 1).'</td><td>'.$incapacidadesTotal.'</td><td>'.number_format($total, 1).'</td>';




	            	/*Incapacidades pocentaje de pagos*/
	            	if($totalLC != 0){
	            		$total = $totalInAju * 100 / $totalLC;
	            	}else{
	            		$total = 0;
	            	}
	            	
	            	$ajuste .= '<td style="text-align:justify;">'.number_format($total, 1).'</td>';


	            	/*Incapacidades pocentaje de pagos*/
	            	if($totalLC != 0){
	            		$total = $totalInAdmi * 100 / $totalLC;
	            	}else{
	            		$total = 0;
	            	}
	            	
	            	$admini .= '<td style="text-align:justify;">'.number_format($total, 1).'</td>';
	            	$adminP .= '<td style="text-align:justify;">100.0</td>';


	            	/*Incapacidades pocentaje de pagos*/
	            	if($totalLC != 0){
	            		$total = $totalInEmp * 100 / $totalLC;
	            	}else{
	            		$total = 0;
	            	}
	            	
	            	$empres .= '<td style="text-align:justify;">'.number_format($total, 1).'</td>';
	            	


	            	/*POR PAGAR*/
	            	/*Incapaciodades SOLICITADAS */
        			$campos = 'SUM(inc_valor_solicitado) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda']." AND inc_estado_tramite = 'SOLICITUD DE PAGO'" ;
	            	
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$porcob .= '<td style="text-align:justify;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
	            		



	            	/*Incapacidades de ese Año*/
        			$campos = 'COUNT(*) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_estado_tramite = \'SOLICITUD DE PAGO\'';
	            	//echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$totalInc = 0;
	            	if($incapacidades['total'] > 0){
	            		$totalInc = $incapacidades['total'];
	            	}
	            	$porcob .= '<td style="text-align:justify;">'.$totalInc.'</td>';
	            	



	            	/*Incapacidades pocentaje de pagos*/
	            	if($incapacidadesTotal != 0){
	            		$total = $totalInc * 100 / $incapacidadesTotal;
	            	}else{
	            		$total = 0;
	            	}
	            	
	            	$porcob .= '<td style="text-align:justify;">'.number_format($total, 1).'</td>';



	            	/*PAGADAS*/
	            	/*Incapaciodades SOLICITADAS */
        			$campos = 'SUM(inc_valor) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda']." AND inc_estado_tramite = 'PAGADA'" ;
	            	
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$pagada .= '<td style="text-align:justify;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
	            	$valorPagado = $incapacidades['total'];
	            	



	            	/*Incapacidades de ese Año*/
        			$campos = 'COUNT(*) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_estado_tramite = \'PAGADA\'';
	            	//echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$totalInc = 0;
	            	if($incapacidades['total'] > 0){
	            		$totalInc = $incapacidades['total'];
	            	}
	            	$pagada .= '<td style="text-align:justify;">'.$totalInc.'</td>';
	            	

	            	/*Incapacidades pocentaje de pagos*/
	            	/*if($incapacidadesTotal != 0){
	            		$total = $totalInc * 100 / $incapacidadesTotal;
	            	}else{
	            		$total = 0;
	            	}*/

	            	if($totalInAdmi != 0){
	            		$total = $valorPagado * 100 / $totalInAdmi;
	            	}else{
	            		$total = 0;
	            	}
	            	
	            	$pagada .= '<td style="text-align:justify;">'.number_format($total, 1).'</td>';




	            	/*NEGADAS*/
	            	/*Incapaciodades SOLICITADAS */
        			$campos = 'SUM(inc_valor_pagado_eps) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i]." AND YEAR(inc_fecha_pago_nomina) = ".$_POST['anhoBusqueda']." AND inc_estado_tramite = 'SIN RECONOCIMIENTO'" ;
	            	
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$negada .= '<td style="text-align:justify;">$'.number_format($incapacidades['total'], 0, '.','.').'</td>';
	            	



	            	/*Incapacidades de ese Año*/
        			$campos = 'COUNT(*) as total';
	            	$tabla  = 'gi_incapacidad';
	            	$condicion =  $whereCliente." MONTH(inc_fecha_pago_nomina) = ".$busque[$i].' AND YEAR(inc_fecha_pago_nomina) = '.$_POST['anhoBusqueda'].' AND inc_estado_tramite = \'SIN RECONOCIMIENTO\'';
	            	//echo "SELECT ".$campos." FROM ".$tabla." WHERE ".$condicion;
	            	$incapacidades = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condicion);
	            	$totalInc = 0;
	            	if($incapacidades['total'] > 0){
	            		$totalInc = $incapacidades['total'];
	            	}
	            	$negada .= '<td style="text-align:justify;">'.$totalInc.'</td>';
	            	



	            	/*Incapacidades pocentaje de pagos*/
	            	if($incapacidadesTotal != 0){
	            		$total = $totalInc * 100 / $incapacidadesTotal;
	            	}else{
	            		$total = 0;
	            	}
	            	
	            	$negada .= '<td style="text-align:justify;">'.number_format($total, 1).'</td>';



    			}
        		
        	?>
        	<tbody>
        		<tr>
        			<td>EMPRESA</td>
        			<?php echo $empres;?>
        		</tr>
        		<tr>
        			<td>ADMINISTRADORA</td>
        			<?php echo $admini;?>
        		</tr>
        		<tr>
        			<td>AJUSTE</td>
        			<?php echo $ajuste;?>
        		</tr>
        		<tr>
        			<td>TOTAL</td>
        			<?php echo $totalTH;?>
        		</tr>
        		<tr>
        			<td colspan="19" style="background: #f5f5f5;">&nbsp;</td>
        		</tr>
        		<tr>
        			<td>POR COBRAR</td>
        			<?php echo $adminP;?>
        		</tr>
        		<tr>
        			<td>PAGADAS</td>
        			<?php echo $pagada;?>
        		</tr>
        		<tr>
        			<td>NEGADAS</td>
        			<?php echo $negada;?>
        		</tr>
        	</tbody>
        </table>
        <input type="hidden" name="base64" id="base64"/>
        <!--<img src="" id="JoseXImagen" alt="jose no sirve">-->
    
</div>
<script type="text/javascript">
	var barChart ;
 	$(function(){
 		getGrafica();
 	});

 	function done(){
		//alert("Proceso Terminado, Reporte Generado");
		var url=barChart.toBase64Image();
		document.getElementById("base64").value = url;
		$.unblockUI();
	}

	function getGrafica(){
		<?php
            $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

            $whereCliente = '';
            if($_SESSION['cliente_id'] != 0){
                $whereCliente =  "inc_empresa = ".$_SESSION['cliente_id']." AND ";
            }

            $campos = 'SUM(inc_valor_pagado_eps) as total , MONTH(inc_fecha_pago_nomina) as mes';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite !=  'EMPRESA' ";
            $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');

            $nombres = '';
            $valores = '';
            $valoreP = '';
            $i = 0;

            foreach ($incapacidades as $key => $value) {
                $numero = $value['mes'];
                if($i == 0){
                    $nombres .= "'".$month[$numero -1]."'";
                    $valores .= "'".$value['total']."'"; 
                }else{
                    $nombres .= " , '".$month[$numero -1]."'";
                    $valores .= " , '".$value['total']."'"; 
                }
                $i++;
            }

            $campos = 'SUM(inc_valor) as total , MONTH(inc_fecha_pago_nomina) as mes';
            $tabla  = 'gi_incapacidad';
            $condicion =  $whereCliente." inc_fecha_pago_nomina BETWEEN '".$year."' AND '".$otherYear."' AND inc_estado_tramite = 'PAGADA' ";
            $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');
            $i = 0;
            foreach ($incapacidades as $key => $value) {
                
                if($i == 0){
                    $valoreP .= "'".$value['total']."'"; 
                }else{
                    $valoreP .= ", '".$value['total']."'"; 
                }
                $i++;
            }
        ?>

        var densityCanvas = document.getElementById("pieChart_2").getContext('2d');

        var densityData = {
            label: 'Valor Pagado En Nomina',
            data: [<?php echo $valores;?>],
            borderColor:'#dd4b39',
            backgroundColor : '#dd4b39',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false
        };

        var densityData_pagadas = {
            label: 'Valor Pagado Entidades',
            data: [<?php echo $valoreP; ?>],
            borderColor:'#36a2eb',
            backgroundColor : '#36a2eb',
            borderWidth: 2,
            hoverBorderWidth: 0,
            fill: false,
            borderDash: [5, 5],
			pointRadius: 10,
			pointHoverRadius: 5
        };

        barChart = new Chart(densityCanvas, {
            type: 'line',
            data: {
                labels: [<?php echo $nombres; ?>],
                datasets: [densityData, densityData_pagadas],
            },
            options: {
            	bezierCurve : false,
    			animation: {
				    onComplete: done
			  	},
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "Valor Pagado : $" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                            });
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (value) {
                                return addCommas(value)
                            }
                        }
                    }]
                }
            }
        });    



        
        
	}

	

	function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function scaleLabel (valuePayload) {
        return Number(valuePayload.value).toFixed(2).replace('.',',') + '$';
    }

    function getRandomColor() {
       return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
    }
</script>
