<?php
	session_start();
	$_SESSION['start'] = time();
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);

    require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
    require_once '../controladores/incapacidades.controlador.php';

	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/incapacidades.modelo.php';
	require_once '../modelos/datatables.modelo.php';

	class AjaxIncapDataTables
	{
		public function ajaxDataTable(){
			$table = 'gi_incapacidad  LEFT JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips ON ips_id = inc_ips_afiliado JOIN gi_estado_tramite ON inc_estado_tramite = est_tra_id_i JOIN gi_origen ON ori_id_i = inc_origen LEFT JOIN gi_pertenencia ON per_id_i = inc_pertenece_red';
			$primaryKey = 'inc_id';
			$columns = array(
			    array( 'db' => 'emd_cedula', 		'dt' => 'emd_cedula' ),
			    array( 'db' => 'emd_nombre',  		'dt' => 'emd_nombre' ),
			    array( 'db' => 'ips_nombre',   		'dt' => 'ips_nombre' ),
			    array( 'db' => 'inc_diagnostico',   'dt' => 'inc_diagnostico' ),
		     	array( 'db' => 'inc_fecha_inicio',  'dt' => 'inc_fecha_inicio' ),
		     	array( 'db' => 'inc_fecha_final',   'dt' => 'inc_fecha_final' ),
		     	array( 'db' => 'est_tra_desc_i',	'dt' => 'est_tra_desc_i' ),
		     	array( 'db' => 'inc_numero_notificaciones_i','dt' => 'inc_numero_notificaciones_i' ),
		     	array( 'db' => 'ori_sigla_v',		'dt' => 'ori_sigla_v' ),
		     	array( 'db' => 'per_descripcion_v',		'dt' => 'per_descripcion_v' ),
		     	array( 'db' => 'emd_estado',		'dt' => 'emd_estado' ),
		     	array( 	'db' => 'inc_ruta_incapacidad_transcrita', 
		     		 	'dt' => 'inc_ruta_incapacidad_transcrita' ),
		     	array( 'db' => 'inc_ruta_incapacidad', 'dt' => 'inc_ruta_incapacidad' ),
		     	array( 'db' => 'inc_id', 'dt' => 'inc_id' ),
		     	array( 'db' => 'inc_estado', 'dt' => 'inc_estado')
			);
			echo json_encode(
		    	SSP::complex( 
		    		$_POST, 
		    		$table, 
		    		$primaryKey, 
		    		$columns, 
		    		null, 
		    		'inc_empresa = '.$_SESSION['cliente_id']
		    	)
			);
			
		}
	}


	if(isset($_GET['getAllData'])){
		$ajaxEmpresa = new AjaxIncapDataTables();
		$ajaxEmpresa->ajaxDataTable();
	}
?>