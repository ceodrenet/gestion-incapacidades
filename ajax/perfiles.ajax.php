<?php
	session_start();
	$_SESSION['start'] = time();
 	require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/perfiles.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/perfiles.modelo.php';

	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxPerfiles
	{
		public $id_Perfiles;
		public $activarId;
		public $ActivarPerfiles;
		public $validaCliente;
		
		public function ajaxEditarPerfiles(){
			$item = 'perfiles_id_i';
			$valor = $this->id_Perfiles;
			$respuesta = ControladorPerfiles::ctrMostrarPerfiles($item, $valor);
			$permisos  = ControladorPerfiles::ctrMostrarMenusPermisos('perfiles_permisos_perfil_id_i', $valor );
			$reportes  = ControladorPerfiles::ctrMostrarMenusReportesPermisos('perfiles_permisos_reportes_perfil_id_i', $valor);
			$datos = array('datosPerfil' => $respuesta , 'datosPermisos' => $permisos, 'datosReportes' => $reportes);
			echo json_encode($datos);
		}

		/* Activar cliente */
		public function ajaxActivarPerfiles(){
			$tabla = 'sys_Perfiles';
			$item1 = 'perfiles_estado_i';
			$valor1 = $this->ActivarPerfiles;
			$item2 = 'Perfiles_id_i';
			$valor2 = $this->activarId;
			$respuesta = ModeloPerfiles::mdlActualizarPerfiles($tabla, $item1, $valor1, $item2, $valor2);
			echo $respuesta;
		}

		/* validar que el usuairo no este repetido */
		/*public function ajaxValidarCliente(){
			$item = 'emp_nit';
			$valor = $this->validaCliente;
			$respuesta = ControladorPerfiles::ctrMostrarPerfiles($item, $valor);
			echo json_encode($respuesta);
		}*/

	}
	
	
	if(isset($_POST['EditarPerfilId'])){
		if($_POST['EditarPerfilId'] != ''){
			$editar = new AjaxPerfiles();
			$editar->id_Perfiles = $_POST['EditarPerfilId'];
			$editar->ajaxEditarPerfiles();
		}	
	}

	if(isset($_POST["NuevoNombre"])){
		echo ControladorPerfiles::ctrCrearPerfil();
	}

	if(isset($_POST["EditarNombre"])){
		echo ControladorPerfiles::ctrEditarPerfil();
	}

	if(isset($_POST["id_perfiles"])){
		echo ControladorPerfiles::ctrBorrarPerfil();
	}   
	

	/*if(isset($_POST['ActivarId'])){
		$activarPerfiles = new AjaxPerfiles();
		$activarPerfiles->activarId = $_POST['ActivarId'];
		$activarPerfiles->ActivarPerfiles = $_POST['estado'];
		$activarPerfiles->ajaxActivarPerfiles();
	}*/


	/*if(isset($_POST['validarNit'])){
		$activarPerfiles = new AjaxPerfiles();
		$activarPerfiles->validaCliente = $_POST['validarNit'];
		$activarPerfiles->ajaxValidarCliente();
	}*/
	