<?php
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    session_start();
    $_SESSION['start'] = time();

    require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/incapacidades.controlador.php';
	require_once '../controladores/empleados.controlador.php';
    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
	require_once '../modelos/empleados.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';
    require_once '../extenciones/Excel.php';

	if(isset($_FILES['NuevoIncapacidad']['tmp_name']) && !empty($_FILES['NuevoIncapacidad']['tmp_name']) ){
		$name   = $_FILES['NuevoIncapacidad']['name'];
		$tname  = $_FILES['NuevoIncapacidad']['tmp_name'];
		ini_set('memory_limit','1028M');
        $obj_excel = PHPExcel_IOFactory::load($tname);
        $sheetData = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
        $arr_datos = array();
        $highestColumm = $obj_excel->setActiveSheetIndex(0)->getHighestColumn(); // e.g. "EL"
        $highestRow = $obj_excel->setActiveSheetIndex(0)->getHighestRow();
        $aciertos = 0;
        $fallos   = 0;
        $total = 0;
        $existentes = 0;
        $datoCsv = array();
        $noexiste = 0;
        $incapacidadesExistentes = 0;
        $vacios = 0;
		$ciclo = 0;
        foreach ($sheetData as $index => $value) {
            if ( $index > 1 ){
                $valido = 0;
                $existe = false;
                $separador = '';
                $total++;
                $ciclo++;
                $empresa = 0;
                if((!is_null($value['A']) && !empty($value['A'])) && 
                        (!is_null($value['C']) && !empty($value['C']))
                    ){
                    /* Iniciamos la consulta Sql */
                    $datos = array();
                    $camposInsert='inc_fecha_generada,';
                    $valuesInsert="'".date('Y-m-d H:i:s')."',";
                    $valuesUpdate='';
                    $condionalUpd='';

                    /* Identificacion */
                    if(!is_null($value['A']) && !empty($value['A']) ){
                        $valido = 1;
                        $camposInsert .= 'inc_cc_afiliado,';
                        $valuesInsert .= "'".$value['A']."', ";
                        $valuesUpdate .=  "inc_cc_afiliado = '".$value['A']."', "; 
  

                        $item = 'emd_cedula';
                        $valuess = $value['A'];
                        $tabla = 'gi_empleados';
                        $value2 = $_SESSION['cliente_id'];
                       //desde aqui no modifique validar --RFB
                        $res = ModeloEmpleados::mdlMostrarEmpleadosX2($tabla, $item, $valuess, $value2);
                        if($res != false){
                            if($res['emd_cedula'] == $value['A']){
                                /*Empleado Existente*/
                                $existe = true;
                                $camposInsert .= 'inc_emd_id, inc_empresa, inc_ips_afiliado,';
                                $valuesInsert .= "'".$res['emd_id']."', '".$res['emd_emp_id']."', '".$res['emd_eps_id']."',";
                                $valuesUpdate .=  "inc_emd_id = '".$res['emd_id']."', inc_empresa = '".$res['emd_emp_id']."', inc_ips_afiliado = '".$res['emd_eps_id']."', "; 
                            
                            }
                        } else{
                            /* No existe */
                            $existe = false;
                            $datx  = array(   
                                        'filas_fallo_fila' => $index,
                                        'fila_fallo_mensaje' => 'Esta cedula => '.$value['A'].' no esta registrada en la base de datos',
                                        'fila_fallo_session_id' => $_SESSION['idSession'],
                                        'fila_fallo_cedula' => $value['A'],
                                    );

                            $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datx);

                            $noexiste++;
                        }
                    }

                    if($existe == true){
                        //Preguntamos si la incapacidad existe
                        $item1 = "inc_cc_afiliado";
                        $valor1 = $value['A'];
                        $item2 = 'inc_fecha_inicio';
                        $valor2 = $value['C'];
                        $existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2, $_SESSION['cliente_id']);
                        $incapacidaDato = null;
                        if($existeInc){
                            //Traemos los datos de la incapacidad, porque ya validamos que esta existe
                            $strCampos = "*";
                            $strTabla_ = "gi_incapacidad";
                            $strWhere_ = "inc_cc_afiliado = '".$value['A']."' AND inc_fecha_inicio = '".$value['C']."'";
                            $incapacidaDato = ModeloTesoreria::mdlMostrarUnitario($strCampos, $strTabla_, $strWhere_);
                            $condionalUpd='inc_id = '.$incapacidaDato['inc_id'];
                        }

                        //hasta aqui--- ####Arreglado!!!#####

                        /* Nombres */
                        if(!is_null($value['C']) && !empty($value['C']) ){
                            $valido = 1;
                            $camposInsert .= 'inc_fecha_inicio,';
                            $valuesInsert .= "'".$value['C']."', ";
                            $valuesUpdate .=  "inc_fecha_inicio = '".$value['C']."', ";   
                        }else{
                            $valuesUpdate .=  "inc_fecha_inicio = inc_fecha_inicio,";
                        }
                        

                        /* Fecha Nacimiento */
                        if(!is_null($value['D']) && !empty($value['D']) ){
                            $camposInsert .= 'inc_fecha_final,';
                            $valuesInsert .= "'".$value['D']."', ";
                            $valuesUpdate .=  "inc_fecha_final = '".$value['D']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_fecha_final = inc_fecha_final,";
                        }

                        /* Fecha ingreso */
                        if(!is_null($value['E']) && !empty($value['E']) ){
                            $camposInsert .= 'inc_origen,';
                            $valuesInsert .= "'".$value['E']."', ";
                            $valuesUpdate .=  "inc_origen = '".$value['E']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_origen = inc_origen,";
                        }

                        /* Diagnostico */
                        if(!is_null($value['F']) && !empty($value['F']) ){
                            $camposInsert .= 'inc_diagnostico,';
                            $valuesInsert .= "'".$value['F']."', ";
                            $valuesUpdate .=  "inc_diagnostico = '".$value['F']."', ";   
                        }else{
                            $valuesUpdate .=  "inc_diagnostico = inc_diagnostico,";
                        }

                        /* Fecha Nacimiento */
                        if(!is_null($value['G']) && !empty($value['G']) ){
                            $camposInsert .= 'inc_tipo_generacion,';
                            $valuesInsert .= "'".$value['G']."', ";
                            $valuesUpdate .=  "inc_tipo_generacion = '".$value['G']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_tipo_generacion = inc_tipo_generacion,";
                        }

                        /* Clasificacion */
                        if(!is_null($value['H']) && !empty($value['H']) ){
                            $camposInsert .= 'inc_clasificacion,';
                            $valuesInsert .= "'".$value['H']."', ";
                            $valuesUpdate .=  "inc_clasificacion = '".$value['H']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_clasificacion = inc_clasificacion,";
                        }

                        /* Valor pagado empresa  */
                        if(!is_null($value['I']) && !empty($value['I']) ){
                            $valor = str_replace('.', ',', $value['I']);
                            $camposInsert .= 'inc_valor_pagado_empresa,';
                            $valuesInsert .= "'".$valor."', ";
                            $valuesUpdate .=  "inc_valor_pagado_empresa = '".$valor."', "; 
                        }else{
                            $valuesUpdate .=  "inc_valor_pagado_empresa = inc_valor_pagado_empresa,";
                        }

                        /* Valor pagado Administradora  */
                        if(!is_null($value['J']) && !empty($value['J']) ){
                            $valor = str_replace('.', ',', $value['J']);
                            $camposInsert .= 'inc_valor_pagado_eps,';
                            $valuesInsert .= "'".$valor."', ";
                            $valuesUpdate .=  "inc_valor_pagado_eps = '".$valor."', "; 

                        }else{
                            $valuesUpdate .=  "inc_valor_pagado_eps = inc_valor_pagado_eps,";
                        }

                        /* Valor Pagado Ajuste  */
                        if(!is_null($value['K']) && !empty($value['K']) ){
                            $valor = str_replace('.', ',', $value['K']);
                            $camposInsert .= 'inc_valor_pagado_ajuste,';
                            $valuesInsert .= "'".$valor."', ";
                            $valuesUpdate .=  "inc_valor_pagado_ajuste = '".$valor."', "; 
                        }else{
                            $valuesUpdate .=  "inc_valor_pagado_ajuste = inc_valor_pagado_ajuste,";
                        }

                        /* IVL empleado */
                        if(!is_null($value['L']) && !empty($value['L']) ){
                            $valido = 1;
                            $camposInsert .= 'inc_ivl_v,';
                            $valuesInsert .= "'".$value['L']."', ";
                            $valuesUpdate .=  "inc_ivl_v = '".$value['L']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_ivl_v = inc_ivl_v,";
                        }

                        /* Fecha Pago Nomina */
                        if(!is_null($value['M']) && !empty($value['M']) ){
                            $camposInsert .= 'inc_fecha_pago_nomina,';
                            $valuesInsert .= "'".$value['M']."', ";
                            $valuesUpdate .=  "inc_fecha_pago_nomina = '".$value['M']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_fecha_pago_nomina = inc_fecha_pago_nomina,";
                        }

                        /* Observacion */
                        if(!is_null($value['N']) && !empty($value['N']) ){
                            $camposInsert .= 'inc_observacion,';
                            $valuesInsert .= "'".$value['N']."', ";
                            $valuesUpdate .=  "inc_observacion = '".$value['N']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_observacion = inc_observacion,";
                        }

                        /* Estado tramite */
                        if(!is_null($value['O']) && !empty($value['O']) ){
                            $camposInsert .= 'inc_estado_tramite,';
                            $valuesInsert .= "'".$value['O']."', ";
                            $valuesUpdate .=  "inc_estado_tramite = '".$value['O']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_estado_tramite = inc_estado_tramite,";
                        }

                        if(!is_null($value['P']) && !empty($value['P']) ){
                            $camposInsert .= 'inc_sub_estado_i,';
                            $valuesInsert .= "'".$value['P']."', ";
                            $valuesUpdate .=  "inc_sub_estado_i = '".$value['O']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_sub_estado_i = inc_sub_estado_i,";
                        }

                        /* valor pagado */
                        if(!is_null($value['Q']) && !empty($value['Q']) ){
                            $valido = 1;
                            $camposInsert .= 'inc_valor,';
                            $valuesInsert .= "'".$value['Q']."', ";
                            $valuesUpdate .=  "inc_valor = '".$value['Q']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_sub_estado_i = inc_sub_estado_i,";
                        }

                        /* Estado tramite */
                        //tambien falta validar esto 
                        if(!is_null($value['R']) && !empty($value['R']) ){
                            if($value['O'] == '3'){

                                $camposInsert .= 'inc_fecha_radicacion,';
                                $valuesInsert .= "'".$value['R']."', ";
                                $valuesUpdate .=  "inc_fecha_radicacion = '".$value['R']."', "; 
                            }else  if($value['O'] == '4'){
                                $camposInsert .= 'inc_fecha_solicitud,';
                                $valuesInsert .= "'".$value['R']."', ";
                                $valuesUpdate .=  "inc_fecha_solicitud = '".$value['R']."', "; 
                            }else if($value['O'] == '5' || $value['O'] == '8'){
                                $camposInsert .= 'inc_fecha_pago,';
                                $valuesInsert .= "'".$value['R']."', ";
                                $valuesUpdate .=  "inc_fecha_pago = '".$value['R']."', "; 
                            }else if($value['O'] == '6' || $value['O'] == '7'){
                                $datos['inc_fecha_negacion_'] = $value['R'];
                                $camposInsert .= 'inc_fecha_pago,';
                                $valuesInsert .= "'".$value['R']."', ";
                                $valuesUpdate .=  "inc_fecha_pago = '".$value['R']."', "; 
                            }
                        }else{
                            $valuesUpdate .=  "inc_fecha_pago = inc_fecha_pago, inc_fecha_solicitud= inc_fecha_solicitud, inc_fecha_radicacion = inc_fecha_radicacion, inc_fecha_negacion_= inc_fecha_negacion_, "; 
                        }
                            //hasta aqui 
                    
                        /* inc_numero_radicado_v */
                        if(!is_null($value['S']) && !empty($value['S']) ){
                            $camposInsert .= 'inc_numero_radicado_v,';
                            $valuesInsert .= "'".$value['S']."', ";
                            $valuesUpdate .=  "inc_numero_radicado_v = '".$value['S']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_numero_radicado_v = inc_numero_radicado_v,";
                        }

                        /* inc_fecha_recepcion_d */
                        if(!is_null($value['T']) && !empty($value['T']) ){
                            $camposInsert .= 'inc_fecha_recepcion_d,';
                            $valuesInsert .= "'".$value['T']."', ";
                            $valuesUpdate .=  "inc_fecha_recepcion_d = '".$value['T']."', "; 
                        }else{
                            $valuesUpdate .=  "inc_fecha_recepcion_d = inc_fecha_recepcion_d,";
                            
                        }

                        /* inc_registrada_entidad_i */
                        if(!is_null($value['U']) && !empty($value['U']) ){
                            $camposInsert .= 'inc_registrada_entidad_i';
                            $valuesInsert .= "'".$value['U']."' ";
                            $valuesUpdate .=  "inc_registrada_entidad_i = '".$value['U']."' "; 
                        }else{
                            $valuesUpdate .=  "inc_registrada_entidad_i = inc_registrada_entidad_i";
                        }

                        $tabla = "gi_incapacidad";  
                        $datos['inc_estado'] = 1;                       
                        if($valido == 1){
                            if($existe){
                                if(!$existeInc){
                                    if(isset($_POST['chekUpdateDatos'])){
                                        /*$respuesta = ModeloIncapacidades::mdlIngresarIncapacidad($tabla, $datos);*/

                                        //echo "INSERT INTO $tabla (".$camposInsert.") VALUES (".$valuesInsert.") \n";
                                        $respuesta = ModeloIncapacidades::mdlCrear($tabla, $camposInsert, $valuesInsert);
                                        //print_r($respuesta);
                                        if($respuesta != 'Error'){
                                            $aciertos++;
                                        }else{
                                            $fallos++;
                                            //var_dump($respuesta);
                                            $datos  = array(   
                                                'filas_fallo_fila' => $i,
                                                'fila_fallo_mensaje' => 'No se pudo guardar revisala, debe tener alguna celda mal configurada',
                                                'fila_fallo_session_id' => $_SESSION['idSession'],
                                                'fila_fallo_texto_error_v' => $respuesta,
                                                'fila_fallo_cedula' => $valor1,
                                            );

                                            $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);
                                        }
                                    }
                                }else{
                                    if(!isset($_POST['chekUpdateDatos'])){
                                        /*Pidieron Actualizar los datos*/                                        
                                        $tabla = "gi_incapacidad";
                                        //echo "UPDATE $tabla SET ".$valuesUpdate." WHERE ".$condionalUpd."\n";
                                        $respuesta = ModeloIncapacidades::mdlEditar($tabla, $valuesUpdate, $condionalUpd);
                                        //print_r($respuesta);
                                        if($respuesta != 'Error'){
                                            $aciertos++;
                                        }else{
                                            $fallos++;
                                            //var_dump($respuesta);
                                            $datos  = array(   
                                                'filas_fallo_fila' => $index,
                                                'fila_fallo_mensaje' => 'No se pudo guardar revisala, debe tener alguna celda mal configurada',
                                                'fila_fallo_session_id' => $_SESSION['idSession'],
                                                'fila_fallo_texto_error_v' => $respuesta,
                                                'fila_fallo_cedula' => $valor1,
                                            );

                                            $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);
                                        }
                                    }else{
                                        $incapacidadesExistentes++;
                                        $datos  = array(   
                                                'filas_fallo_fila' => $index,
                                                'fila_fallo_mensaje' => 'Cedula '.$valor1.' y fecha inicio '.$valor2.' es de una incapacidad ya guardada',
                                                'fila_fallo_cedula' => $valor1,
                                                'fila_fallo_session_id' => $_SESSION['idSession']
                                            );

                                        $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);    
                                    }
                                    
                                }   
                            }else{
                                $fallos++;
                            }
                        }
                    }

                    if($ciclo > 300){
                        $ciclo = 0;
                        usleep(2000000);
                    }
                }else{
                    $vacios++;
                    if($vacios> 2){
                        break;
                    }
                }
        
            }
        }


        $check = 0;

        if(isset($_POST['chekUpdate']) && $_POST['chekUpdate'] != 0){
        	$check = 1;
        }

        ControladorPlantilla::setAuditoria('ControladorIncapacidades', 'SoloCargarIncapacidades', 'Archivo Excel', json_encode(array('total' => $total , 'exito' => $aciertos , 'fallaron' => $fallos, 'totalesAciertosNo' => $incapacidadesExistentes, 'check' => $check)) );

        echo json_encode(array('total' => $total , 'exito' => $aciertos , 'fallaron' => $fallos, 'totalesAciertosNo' => $incapacidadesExistentes, 'check' => $check));
        
	}
?>