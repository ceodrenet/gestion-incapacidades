<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/empleados.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/empleados.modelo.php';

	class DataTablesAjax
	{	
		public $empresa;

		public function ajaxTablaEmpleados(){
			
echo '{
  	"data" : [';
  			$item = "emd_emp_id";
            $valor = $this->empresa;
            $incapacidades = ControladorEmpleados::ctrMostrarEmpleados_ByEmpresa($item, $valor);
            $i = 0;
            foreach ($incapacidades as $key => $value) {
            	if($i != 0){
            		echo ",";
            	}
	echo '[';
	echo '"'.($value["emd_nombre"]).'",';
	echo '"'.$value["emd_cedula"].'",';
	echo '"'.($value["ips_nombre"]).'",';
	echo '"'.$value["emd_fecha_ingreso"].'",';  
	if($value["emd_estado"] == '1'){
		echo '"ACTIVO",'; 
	}else{
		echo '"RETIRADO",'; 
	} 
	echo '"'.$value["emd_tipo_empleado"].'",';
	echo '"'.$value["emd_ruta_cedula"].'",'; 
	echo '"'.$value["emd_id"].'"'; 
	echo ']';
				$i++;
            }
echo ']
}';
		}
	


		public function ajaxTablaEmpleadosTotales(){
			
echo '{
  	"data" : [';
            $incapacidades = ControladorEmpleados::ctrMostrarEmpleadosTotales();
            $i = 0;
            foreach ($incapacidades as $key => $value) {
            	if($i != 0){
            		echo ",";
            	}
	echo '[';
	echo '"'.($key+1).'",';
	echo '"'.$value["emp_nombre"].'",';
	echo '"'.($value["emd_nombre"]).'",';
	echo '"'.$value["emd_cedula"].'",';
	echo '"'.($value["ips_nombre"]).'",';
	echo '"'.$value["emd_fecha_ingreso"].'",';  
	if($value["emd_estado"] == '1'){
		echo '"ACTIVO",'; 
	}else{
		echo '"INACTIVO",'; 
	}
	echo '"'.$value["emd_ruta_otro"].'",'; 
	echo '"'.$value["emd_id"].'"'; 
	echo ']';
				$i++;
            }
echo ']
}';
		}
	}

	if(isset($_GET['empresId'])){
		$ajaxTablaEmpleados = new DataTablesAjax();
		$ajaxTablaEmpleados->empresa = 	$_GET['empresId'];
		$ajaxTablaEmpleados->ajaxTablaEmpleados();	
	}else{
		$ajaxTablaEmpleados = new DataTablesAjax();
		$ajaxTablaEmpleados->ajaxTablaEmpleadosTotales();
	}
	