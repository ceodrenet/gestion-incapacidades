<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/archivos.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/archivos.modelo.php';

	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxArchivos
	{
		public $id_archivo;
		
		public function ajaxEditarArchivo(){
			$item = 'arc_id';
			$valor = $this->id_archivo;
			$respuesta = ControladorArchivos::ctrMostrarArchivos_edicion($item, $valor);
			echo json_encode($respuesta);
		}
	}
	
	
	if(isset($_POST['EditarArchivoId'])){
		if($_POST['EditarArchivoId'] != ''){
			$editar = new AjaxArchivos();
			$editar->id_archivo = $_POST['EditarArchivoId'];
			$editar->ajaxEditarArchivo();
		}	
	}

	if(isset($_GET['dame-Archivos-inicio'])){
		$item = null;
		$valor = null;
		if($_SESSION['cliente_id'] != 0){
            $item = 'arc_empresa_id_i';
            $valor = $_SESSION['cliente_id'];
        }
        $clientes = ControladorArchivos::ctrMostrarArchivos($item, $valor);
echo '{
  	"data" : [';
  			$i = 0;
		 	foreach ($clientes as $key => $value) {
		 		if($i != 0){
            		echo ",";
            	}
				echo '[';
				echo '"'.$value['arc_entidad'].'",';
				echo '"'.$value['arc_origen'].'",';
                echo '"'.$value['arc_fecha'].'",';    
				echo '"'.$value['arc_descripcion'].'",'; 
				echo '"'.$value['arc_ruta_1'].'",'; 
				echo '"'.$value['arc_ruta_2'].'",'; 
				echo '"'.$value["arc_id"].'"';
				echo ']';
            	$i++;
		 	}
		echo ']
}';
	}

	if(isset($_POST["id_Archivo"])){
		echo ControladorArchivos::ctrBorrarArchivo();
	}

	if(isset($_POST["EditarEntidad"])){
		echo ControladorArchivos::ctrEditarArchivos();
	}

	if(isset($_POST["NuevoEntidad"])){
		echo ControladorArchivos::ctrCrearArchivo();
	}