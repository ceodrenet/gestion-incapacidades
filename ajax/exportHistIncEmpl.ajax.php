<?php
require_once '../extenciones/Excel.php';
require_once '../vendor/autoload.php';
require_once '../modelos/dao.modelo.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$objPHPExcel = new Spreadsheet();
$objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('Historico');

$borders = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
        )
    ),
);

$objPHPExcel->getActiveSheet()->getStyle('B4:B8')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('B4:B8')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);


$objPHPExcel->getActiveSheet()->getStyle('B4:B8')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('B4:B8')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

$objPHPExcel->getActiveSheet()->getStyle('I4:I6')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('I4:I6')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

$objPHPExcel->getActiveSheet()->mergeCells('B2:L2');
$objPHPExcel->getActiveSheet()->setCellValue("B2", "HISTÓRICO DE INCAPACIDADES POR EMPLEADO");
$objPHPExcel->getActiveSheet()->getStyle('B2:L2')->getFont()
    ->setName('Arial')
    ->setSize(24)
    ->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B2:L2')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


/*CENTRO LAS LETRAS*/
$objPHPExcel->getActiveSheet()->getStyle('B4:B8')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E4:E8')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('I4:I8')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K4:K8')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

$fieldsDataEmpl = "emd_nombre, emd_cedula, emd_cargo, emd_fecha_ingreso, eps.ips_nombre as emd_eps, emd_fecha_afiliacion_eps,
afp.ips_nombre as emd_afp, emd_ciudad, emd_sede, emd_subcentro_costos, emd_centro_costos, emp.emp_nombre as emp_nombre, 
emp.emp_nit as emp_nit";

$tableJoins = "gi_empleados JOIN gi_ips eps ON emd_eps_id = eps.ips_id JOIN gi_ips afp ON emd_afp_id = afp.ips_id
JOIN gi_empresa emp ON emd_emp_id = emp.emp_id";

$cedula = $_POST["buscarEmpleado"]; 

$dataEmplado = ModeloDAO::mdlMostrarUnitario($fieldsDataEmpl, $tableJoins, "emd_cedula=$cedula");

$objPHPExcel->getActiveSheet()->mergeCells('B4:D4');
$objPHPExcel->getActiveSheet()->mergeCells('E4:H4');
$objPHPExcel->getActiveSheet()->mergeCells('I4:J4');
$objPHPExcel->getActiveSheet()->mergeCells('K4:L4');
$objPHPExcel->getActiveSheet()->setCellValue("B4", "EMPRESA");
$objPHPExcel->getActiveSheet()->setCellValue("E4", $dataEmplado['emp_nombre']);
$objPHPExcel->getActiveSheet()->setCellValue("I4", "NIT");
$objPHPExcel->getActiveSheet()->setCellValue("K4", $dataEmplado['emp_nit']);
$objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('I4')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->mergeCells('B5:D5');
$objPHPExcel->getActiveSheet()->mergeCells('E5:H5');
$objPHPExcel->getActiveSheet()->mergeCells('I5:J5');
$objPHPExcel->getActiveSheet()->mergeCells('K5:L5');
$objPHPExcel->getActiveSheet()->setCellValue("B5", "COLABORADOR");
$objPHPExcel->getActiveSheet()->setCellValue("E5", $dataEmplado['emd_nombre']);
$objPHPExcel->getActiveSheet()->setCellValue("I5", "IDENTIFICACION");
$objPHPExcel->getActiveSheet()->setCellValue("K5", $dataEmplado['emd_cedula']);
$objPHPExcel->getActiveSheet()->getStyle('B5')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('I5')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->mergeCells('B6:D6');
$objPHPExcel->getActiveSheet()->mergeCells('E6:H6');
$objPHPExcel->getActiveSheet()->mergeCells('I6:J6');
$objPHPExcel->getActiveSheet()->mergeCells('K6:L6');
$objPHPExcel->getActiveSheet()->setCellValue("B6", "CARGO");
$objPHPExcel->getActiveSheet()->setCellValue("E6", $dataEmplado['emd_cargo']);
$objPHPExcel->getActiveSheet()->setCellValue("I6", "FECHA DE INGRESO");
$date3 = new DateTime($dataEmplado["emd_fecha_ingreso"]);
$objPHPExcel->getActiveSheet()->setCellValue("K6", \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($date3));
$objPHPExcel->getActiveSheet()->getStyle("K6")->getNumberFormat()->setFormatCode("dd/mm/yyyy");

$objPHPExcel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('I6')->getFont()->setBold(true);

/*CENTRO LAS LETRAS*/
$objPHPExcel->getActiveSheet()->getStyle('C7:C8')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('G7:G8')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('H7:H8')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('J7:J8')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('K7:K8')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('G7:G8')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('G7:G8')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);


$objPHPExcel->getActiveSheet()->getStyle('J7:J8')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('J7:J8')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

$objPHPExcel->getActiveSheet()->mergeCells('C7:F7');
$objPHPExcel->getActiveSheet()->mergeCells('H7:I7');
$objPHPExcel->getActiveSheet()->mergeCells('K7:L7');
$objPHPExcel->getActiveSheet()->setCellValue("B7", "SEDE");
$objPHPExcel->getActiveSheet()->setCellValue("C7", $dataEmplado['emd_sede']);
$objPHPExcel->getActiveSheet()->setCellValue("G7", "CENTRO DE COSTOS");
$objPHPExcel->getActiveSheet()->setCellValue("H7", $dataEmplado['emd_centro_costos']);
$objPHPExcel->getActiveSheet()->setCellValue("J7", "CIUDAD");
$objPHPExcel->getActiveSheet()->setCellValue("K7", $dataEmplado['emd_ciudad']);
$objPHPExcel->getActiveSheet()->getStyle('B7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('G7')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('J7')->getFont()->setBold(true);

$objPHPExcel->getActiveSheet()->mergeCells('C8:F8');
$objPHPExcel->getActiveSheet()->mergeCells('H8:I8');
$objPHPExcel->getActiveSheet()->mergeCells('K8:L8');
$objPHPExcel->getActiveSheet()->setCellValue("B8", "EPS");
$objPHPExcel->getActiveSheet()->setCellValue("C8", $dataEmplado['emd_eps']);
$objPHPExcel->getActiveSheet()->setCellValue("G8", "FECHA AFILIACIÓN EPS");
$objPHPExcel->getActiveSheet()->setCellValue("H8", $dataEmplado['emd_fecha_afiliacion_eps']);
$objPHPExcel->getActiveSheet()->setCellValue("J8", "AFP");
$objPHPExcel->getActiveSheet()->setCellValue("K8", $dataEmplado['emd_afp']);

$objPHPExcel->getActiveSheet()->getStyle('B8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('G8')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('J8')->getFont()->setBold(true);

/*Incapacidades*/
$objPHPExcel->getActiveSheet()->setCellValue("A10", "ID");
$objPHPExcel->getActiveSheet()->setCellValue("B10", "FECHA INICIAL");
$objPHPExcel->getActiveSheet()->setCellValue("C10", "FECHA FINAL");
$objPHPExcel->getActiveSheet()->setCellValue("D10", "DIAS");
$objPHPExcel->getActiveSheet()->setCellValue("E10", "CLASIFICACIÓN");
$objPHPExcel->getActiveSheet()->setCellValue("F10", "DX");
$objPHPExcel->getActiveSheet()->setCellValue("G10", "ORIGEN");
$objPHPExcel->getActiveSheet()->setCellValue("H10", "ESTADO TRAMITE");
$objPHPExcel->getActiveSheet()->setCellValue("I10", "FECHA ESTADO");
$objPHPExcel->getActiveSheet()->setCellValue("J10", "No. RADICADO");
$objPHPExcel->getActiveSheet()->setCellValue("K10", "VALOR PAGADO");
$objPHPExcel->getActiveSheet()->setCellValue("L10", "FECHA PAGO");
$objPHPExcel->getActiveSheet()->getStyle('A10:L10')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A10:L10')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('A10:L10')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
$objPHPExcel->getActiveSheet()->getStyle('A10:L10')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

$fieldsDataInc = "inc_id, inc_fecha_inicio, inc_fecha_final, dias, inc_clasificacion, inc_origen, inc_diagnostico, 
inc_estado_tramite, inc_Fecha_estado, inc_numero_radicado_v, inc_valor, inc_fecha_pago";

$dataIncapacidades = ModeloDAO::mdlMostrarGroupAndOrder($fieldsDataInc, "reporte_consolidado_inc", "emd_cedula = $cedula", null , "ORDER BY str_to_date(inc_fecha_inicio, '%d/%m/%Y') ASC");

$i = 11;
foreach ($dataIncapacidades as $value) {
    $objPHPExcel->getActiveSheet()->setCellValue("A$i", $value["inc_id"]);
    $objPHPExcel->getActiveSheet()->setCellValue("B$i", \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_inicio"]));
    $objPHPExcel->getActiveSheet()->setCellValue("C$i", \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_final"]));
    $objPHPExcel->getActiveSheet()->setCellValue("D$i", $value["dias"]);
    $objPHPExcel->getActiveSheet()->setCellValue("E$i", $value["inc_clasificacion"]);
    $objPHPExcel->getActiveSheet()->setCellValue("F$i", $value["inc_diagnostico"]);
    $objPHPExcel->getActiveSheet()->setCellValue("G$i", $value["inc_origen"]);
    $objPHPExcel->getActiveSheet()->setCellValue("H$i", $value["inc_estado_tramite"]);
    $objPHPExcel->getActiveSheet()->setCellValue("I$i", (!empty($value["inc_Fecha_estado"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_Fecha_estado"]):"");
    $objPHPExcel->getActiveSheet()->setCellValue("J$i", $value["inc_numero_radicado_v"]);
    $objPHPExcel->getActiveSheet()->setCellValue("K$i", $value["inc_valor"]);
    $objPHPExcel->getActiveSheet()->setCellValue("L$i", (!empty($value["inc_fecha_pago"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_pago"]):"");

    $i++;
}
$objPHPExcel->getActiveSheet()->getStyle("B11:B$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$objPHPExcel->getActiveSheet()->getStyle("C11:C$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$objPHPExcel->getActiveSheet()->getStyle("I11:I$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$objPHPExcel->getActiveSheet()->getStyle("L11:L$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

foreach(range('A','Q') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}

$fieldsDataComentarios = "inm_inc_id_i,incm_comentario_t, inc_fecha_inicio, inc_fecha_final, inc_clasificacion, inc_origen, inc_diagnostico, inc_estado_tramite, incm_fecha_comentario";
$tableJoinsComentarios = "gi_incapacidades_comentarios JOIN reporte_consolidado_inc ON inc_id = inm_inc_id_i";
$dataComentarios = ModeloDAO::mdlMostrarGroupAndOrder($fieldsDataComentarios,$tableJoinsComentarios, "emd_cedula = $cedula", null, 'ORDER BY incm_fecha_comentario ASC');
if (!empty($dataComentarios)) {
    $newHoja = $objPHPExcel->createSheet(1);
    $newHoja->setTitle("Comentarios");


    $newHoja->mergeCells('A1:I1');
    $newHoja->setCellValue('A1', "COMENTARIOS"); 
    $newHoja->getStyle('A1')->getFont()->setBold( true );
    $newHoja->getStyle('A1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
    $newHoja->getStyle('A1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
    $newHoja->getStyle('A1')
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

    $newHoja->setCellValue('A2', "ID INC"); 
    $newHoja->setCellValue('B2', "FECHA INICIO"); 
    $newHoja->setCellValue('C2', "FECHA FINAL"); 
    $newHoja->setCellValue('D2', "CLASIFICACIÓN"); 
    $newHoja->setCellValue('E2', "DX"); 
    $newHoja->setCellValue('F2', "ORIGEN"); 
    $newHoja->setCellValue('G2', "ESTADO DE TRAMITE"); 
    $newHoja->setCellValue('H2', "FECHA COMENTARIO");
    $newHoja->setCellValue('I2', "COMENTARIO"); 
    $newHoja->getStyle('A2:H2')->getFont()->setBold( true );
    
    $k = 3;
    foreach($dataComentarios as $value){
        $newHoja->setCellValue("A".$k, $value["inm_inc_id_i"]); 
        $newHoja->setCellValue("B".$k, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_inicio"]));
        $newHoja->setCellValue("C".$k, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_final"])); 
        $newHoja->setCellValue("D".$k, $value["inc_clasificacion"]); 
        $newHoja->setCellValue("E".$k, $value["inc_origen"]); 
        $newHoja->setCellValue("F".$k, $value["inc_diagnostico"]); 
        $newHoja->setCellValue("G".$k, $value["inc_estado_tramite"]); 
        $newHoja->setCellValue("H".$k, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel(date("d-m-Y", strtotime($value["incm_fecha_comentario"]))));
        $newHoja->setCellValue("I".$k, $value["incm_comentario_t"]); 
        $k++;
    }
    $newHoja->getStyle("B3:B$k")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $newHoja->getStyle("C3:C$k")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    $newHoja->getStyle("H3:H$k")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
    
    foreach(range('A','I') as $columnID) {
        $newHoja->getColumnDimension($columnID)
            ->setAutoSize(true);
    }
}

$objPHPExcel->setActiveSheetIndex(0);

//Write the Excel file to filename some_excel_file.xlsx in the current directory
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="historico_incapacidades_' . date('Y_m_d_H_i_s') . '.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = new Xlsx($objPHPExcel);
$writer->save('php://output');
