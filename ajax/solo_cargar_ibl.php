<?php
    session_start();
    $_SESSION['start'] = time();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    ini_set('memory_limit','1024');
    ini_set('max_execution_time', 500);

    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/empleados.modelo.php';
    require_once '../extenciones/Excel.php';

    if(isset($_FILES['IblEmpleados']['tmp_name']) && !empty($_FILES['IblEmpleados']['tmp_name']) ){
        $name   = $_FILES['IblEmpleados']['name'];
        $tname  = $_FILES['IblEmpleados']['tmp_name'];
        $empresa = $_POST['empresa'];
        
        $obj_excel = PHPExcel_IOFactory::load($tname);
        $sheetData = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
        $arr_datos = array();
        $highestColumm = $obj_excel->setActiveSheetIndex(0)->getHighestColumn(); // e.g. "EL"
        $highestRow = $obj_excel->setActiveSheetIndex(0)->getHighestRow();
        
        $aciertos = 0;
        $fallos   = 0;
        $total = 0;
        $existentes = 0;
        $Noexiste = 0;
        $badFormat = 0;
        foreach ($sheetData as $index => $value) {
            if ( $index > 1 ){
                /* si es 1 es porque esta el formato de empleados */
                

                $valido = 0;
                $existe = false;
                $separador = '';
                $total++;
                $res = 'NULL';
                if( !is_null($value['A']) && !empty($value['A']) ){
                    /* Iniciamos la consulta Sql */
                    $datos = array();

                    /* Identificacion */

                    if(!is_null($value['A']) && !empty($value['A']) ){
                        $valido = 1;
                        $datos['emd_cedula'] = $value['A']; 

                        $item = 'emd_cedula';
                        $valuess = $value['A'];
                        $tabla = 'gi_empleados';
                        $value2 = $empresa;
                        $res = ModeloEmpleados::mdlMostrarEmpleadosX2($tabla, $item, $valuess, $value2);
                        if($res){
                            if($res['emd_cedula'] == $value['A']){
                                $existe = true;
                                $existentes++;
                                $datos['emd_id'] = $res['emd_id'];
                            }   
                        }else{
                            $Noexiste++;
                        }
                        
                    }else{
                        $datos['emd_cedula'] = 'NULL';
                    }


                    if(!is_null($value['C']) && !empty($value['C']) ){
                        $valido = 1;
                        $datos['emd_salario_promedio'] = $value['C'];
                    }else{
                        if($res != 'NULL'){
                            $datos['emd_salario_promedio'] = $res['emd_salario_promedio'];
                        }else{
                            $datos['emd_salario_promedio'] = 'NULL'; 
                        }
                    }


                    if(!is_null($value['D']) && !empty($value['D']) ){
                        $valido = 1;
                        $datos['emd_salario'] = $value['D'];
                    }else{
                        if($res != 'NULL'){
                            $datos['emd_salario'] = $res['emd_salario'];
                        }else{
                            $datos['emd_salario'] = 'NULL'; 
                        }
                    }

                    if(!is_null($value['E']) && !empty($value['E']) ){
                        $valido = 1;
                        $item = 'ips_codigo';
                        $valuess = $value['E'];
                        $resp = ControladorIps::ctrMostrarIpsById($item, $valuess);
                        if($resp){
                            $datos['emd_eps_id'] = $resp['ips_id'];
                        }else{
                            $datos['emd_eps_id'] = 'NULL';  
                        }
                    }else{
                        if($res != 'NULL'){
                            $datos['emd_eps_id'] = $res['emd_eps_id'];
                        }else{
                            $datos['emd_eps_id'] = 'NULL'; 
                        }
                    }

                    /* Feccha Ingreso EPS */
                    if(!is_null($value['F']) && !empty($value['F']) ){
                        $valido = 1;
                        $datos['emd_fecha_afiliacion_eps'] = $value['F'];
                    }else{
                        if($res != 'NULL'){
                            $datos['emd_fecha_afiliacion_eps'] = $res['emd_fecha_afiliacion_eps'];
                        }else{
                            $datos['emd_fecha_afiliacion_eps'] = 'NULL'; 
                        }
                    }

                    /* Feccha Retiro */
                    if(!is_null($value['G']) && !empty($value['G']) ){
                        $valido = 1;
                        $datos['emd_fecha_retiro'] = $value['G'];
                    }else{
                        if($res != 'NULL'){
                            $datos['emd_fecha_retiro'] = $res['emd_fecha_retiro'];
                        }else{
                            $datos['emd_fecha_retiro'] = 'NULL'; 
                        }
                    }

                    /* Feccha Ingreso EPS */
                    if(!is_null($value['H']) && !empty($value['H']) ){
                        $valido = 1;
                        $stado = 0;
                        if($value['H'] == 'ACTIVO'){
                            $stado = 1;
                        }
                        $datos['emd_estado'] = $stado;
                    }else{
                        if($res != 'NULL'){
                            $datos['emd_estado'] = $res['emd_estado'];
                        }else{
                            $datos['emd_estado'] = 'NULL'; 
                        }
                    }

                    if($valido == 1){
                        $datos['emd_emp_id'] = $empresa;

                        $tabla = "gi_empleados";
                        if($existe){
                            $valores = "emd_salario_promedio = '".$datos['emd_salario_promedio']."', emd_salario = '".$datos['emd_salario']."', emd_eps_id = '".$datos['emd_eps_id']."', emd_fecha_afiliacion_eps = '".$datos['emd_fecha_afiliacion_eps']."', emd_fecha_retiro = '".$datos['emd_fecha_retiro']."',  emd_estado = '".$datos['emd_estado']."' ";
                            $respuesta = ModeloTesoreria::mdlActualizar($tabla, $valores, "emd_id = ".$res['emd_id']);
                        }
                        
                        if($respuesta == 'ok'){
                            $aciertos++;
                        }else{
                            $fallos++;
                        }
                    }
                }
            
            }
        }

        echo json_encode(array('total' => $total , 'exito' => $aciertos , 'fallaron' => $fallos, 'Noexiste' => $Noexiste));
    }