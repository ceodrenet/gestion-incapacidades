<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/cartera.controlador.php';
	
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/archivos.modelo.php';
	require_once '../modelos/cartera.modelo.php';

	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxCartera
	{
		public $Cartera;
		public function getCartera(){
			$item  = 'car_id';
			$valor = $this->Cartera;
			$respuesta = ControladorCartera::ctrMostrarCartera($item, $valor);
			$newItem = 'ci_car_id';
			$newRespuesta = ControladorCartera::ctrMostrarDetalleCartera($newItem, $valor);
			echo json_encode(array('cartera' => $respuesta, 'detalle' => $newRespuesta));
		}
	}


	if(isset($_POST['getCartera'])){
		$AjaxCartera = new AjaxCartera();
		$AjaxCartera->Cartera = $_POST['getCartera'];
		$AjaxCartera->getCartera();
	}


	if(isset($_POST["NuevoEps"])){
		echo ControladorCartera::ctrCrearCartera();
	}

	if(isset($_POST["EditarEps"])){
		echo ControladorCartera::ctrEditarCartera();
	}

	if(isset($_POST["id_Cartera"])){
		echo ControladorCartera::ctrBorrarCartera();
	}


	if(isset($_GET['getDataCartera'])){
		$item = null;
		$valor = null;
	 	if($_SESSION['cliente_id'] != 0){
            $item = 'car_emp_id';
            $valor = $_SESSION['cliente_id'];
        }
        $clientes = ControladorCartera::ctrMostrarCarteraDeglosada($item, $valor);
echo '{
  	"data" : [';
  		 $i = 0;
        foreach ($clientes as $key => $value) {
        	if($i != 0){
        		echo ",";
        	}

        	$valor = 0;

            if(!empty($value['car_valor'])){
                $valor = number_format(trim($value['car_valor']), 0, ',', '.');
            }

        	echo '[';
				echo '"'.($key+1).'",';
				echo '"'.$value["ips_nombre"].'",';
				echo '"'.$value["car_paz_salvo"].'",';
				echo '"'.$valor.'",';
				echo '"'.$value["car_fecha_cre"].'",';
				echo '"'.$value["car_fecha_rad"].'",';
				echo '"'.$value["car_respuesta"].'",';
				echo '"'.$value["car_fecha_res"].'",';
				echo '"'.$value["car_id"].'",';
				echo '"'.$value["car_ruta_pdf"].'"';
        	echo ']';
			$i++;
        }
echo ']
}';

	}