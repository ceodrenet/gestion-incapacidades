<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';	
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/archivos.modelo.php';

	require_once '../controladores/entidades.controlador.php';
	require_once '../modelos/entidades.modelo.php';
	require_once '../modelos/tesoreria.modelo.php';

	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxEntidades
	{
		public $id_Entidades;
		public $activarId;
		public $ActivarEntidades;
		public $validaCliente;
		
		public function ajaxEditarEntidades(){
			$item = 'ent_id';
			$valor = $this->id_Entidades;
			$respuesta = ControladorEntidades::ctrMostrarEntidades($item, $valor);
			echo json_encode($respuesta);
		}

		/* Activar cliente */
		public function ajaxActivarEntidades(){
			$tabla = 'sys_Entidades';
			$item1 = 'Entidades_estado_i';
			$valor1 = $this->ActivarEntidades;
			$item2 = 'Entidades_id_i';
			$valor2 = $this->activarId;
			//$respuesta = ModeloEntidades::mdlActualizarEntidades($tabla, $item1, $valor1, $item2, $valor2);
			$respuesta = ModeloEntidades::mdlActualizarCliente($tabla, $item1, $valor1, $item2, $valor2);
			echo $respuesta;
		}

		/* validar que el usuairo no este repetido */
		public function ajaxEditarUsuariosEps(){
			$campos = "use_id_i, use_ips_id_i, use_usuario_v, use_password_v, use_url_v, use_observacion_v";
	        $tablas = "gi_usuarios_eps";
	        $condic = "use_id_i = ".$this->id_Entidades;
	        $clientes = ModeloTesoreria::mdlMostrarUnitario($campos,$tablas,$condic);
			echo json_encode($clientes);
		}

	}
	
	
	if(isset($_POST['EditarEntidadId'])){
		if($_POST['EditarEntidadId'] != ''){
			$editar = new AjaxEntidades();
			$editar->id_Entidades = $_POST['EditarEntidadId'];
			$editar->ajaxEditarEntidades();
		}	
	}

	if(isset($_POST['EditarUsuarioEpsId'])){
		$activarEntidades = new AjaxEntidades();
		$activarEntidades->id_Entidades = $_POST['EditarUsuarioEpsId'];
		$activarEntidades->ajaxEditarUsuariosEps();
	}

	if(isset($_POST["NuevoNombre"])){
		echo ControladorEntidades::ctrCrearEntidad();
	}

	if(isset($_POST["EditarNombre"])){
		echo ControladorEntidades::ctrEditarEntidad();
	}

	if(isset($_POST["id_entidad"])){
		echo ControladorEntidades::ctrBorrarEntidad();
	}

	if(isset($_GET['getDataEntidades'])){
		$item = null;
        $valor = null;
        $Entidades = ControladorEntidades::getData('gi_entidades', $item, $valor);
echo '{
  	"data" : [';
  		 $i = 0;
        foreach ($Entidades as $key => $value) {
        	if($i != 0){
        		echo ",";
        	}
        	echo '[';
				echo '"'.$value["ent_nombres"].'",';
				echo '"'.$value["ent_codigo"].'",';
				echo '"'.$value["ent_ciudad"].'",';
				echo '"'.$value["ent_direccion"].'",';
				echo '"'.$value["ent_telefono"].'",';
				echo '"'.$value["ent_horario"].'",';
				echo '"'.$value["ent_id"].'",';
				echo '"'.$value["ent_formato_1"].'",';
				echo '"'.$value["ent_formato_2"].'",';
				echo '"'.$value["ent_formato_3"].'"';
        	echo ']';
			$i++;
        }
echo ']
}';

	}
	