<?php
    session_start();
	$_SESSION['start'] = time();
    require_once '../modelos/tesoreria.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
	require_once '../modelos/empleados.modelo.php';
  
    if(isset($_POST['contados']) && $_POST['contados'] != 0 ){
    	$noexisten = 0;
        $listo = 0;
        $empres = $_POST['NuevoIdEmpresa'];
        $nomina = $_POST['txtNomina'];

        if(!isset($_POST['txtAnhoNomina']) || $_POST['txtAnhoNomina'] != ''){
            $anho = date('Y');    
        }else{
            $anho = $_POST['txtAnhoNomina'];
        }
        
        $contados = ($_POST['contados'] +1);
        $campos = "incn_cc_v, incn_fecha_inicio_d, incn_fecha_fin_d, incn_estado_v, incn_nomina_i, incn_anho_i, incn_emp_id, incn_razon_v, inc_img_ruta_v, inc_observacion_v";
        $tabla = "gi_incapacidades_nomina";
        for ($i=0; $i < $contados; $i++) { 
            
            $razon = 0;
           	$ruta = "";
			$ruta2 = "";
            if(isset($_POST['EditarMotivo_'.$i]) && $_POST['EditarMotivo_'.$i] != '' ){
           		$razon = $_POST['EditarMotivo_'.$i]; 
           	}
		
            if(isset($_POST['Editarcedula_'.$i]) && $_POST['Editarcedula_'.$i] != '' ){
                /* Viene la cedula ya debe estar validada */
                
				// si viene imagen o PDF
				if(isset($_FILES['EditarFile_'.$i]['tmp_name']) && !empty($_FILES['EditarFile_'.$i]['tmp_name'])){
					/* Vamos a cargar la primera imagen */

					list($ancho, $alto) = getimagesize($_FILES['EditarFile_'.$i]['tmp_name']);
					$nuevoAncho = 500;
					$nuevoAlto  = 500;
					$ruta = '';

					/* Creamos el directorio */
					$directorio = __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/";
					if (!is_dir($directorio)) {
						mkdir($directorio, 0755, true);
					}


					/* De acuerdo al tipo de imagen se le aplican funciones de php */
					if($_FILES["EditarFile_".$i]["type"] == "image/jpeg"){
						//creamos un numero aleatorio
						$aleatorio = mt_rand(1000, 9999);
						//creamos la ruta donde se va a guardar la imagen
						$ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".jpg";
						$ruta2 = "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".jpg";
						//obtenemos el origen osea el FILE
						$origen  = imagecreatefromjpeg($_FILES['EditarFile_'.$i]['tmp_name']);
						//le decimos que vamos acrear una imagen en el destino con esos ancho y alto
						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
				 
						 
						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
						imagejpeg($destino, $ruta);

					}elseif($_FILES["EditarFile_".$i]["type"] == "image/png"){
						//creamos un numero aleatorio
						$aleatorio = mt_rand(1000, 9999);
						//creamos la ruta donde se va a guardar la imagen
						$ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".png";
						$ruta2 =  "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".png";
						//obtenemos el origen osea el FILE
						$origen  = imagecreatefrompng($_FILES['EditarFile_'.$i]['tmp_name']);
						//le decimos que vamos acrear una imagen en el destino con esos ancho y alto
						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
						//cortamos esta imagen
						
						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
						imagepng($destino, $ruta);

					}else{
						//creamos un numero aleatorio
						$aleatorio = mt_rand(100, 999);
						//creamos la ruta donde se va a guardar la imagen
						$ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".pdf";
						$ruta2 =  "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".pdf";
						/* Creamos el directorio */
						$directorio = __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/";
						if (!is_dir($directorio)) {
							mkdir($directorio, 0755, true);
						}

						copy($_FILES['EditarFile_'.$i]['tmp_name'], $ruta);

					} 
				}
                //por si es aceptada o no
                $observacion = "";
                if(isset($_POST['EditarObservacion_'.$i])){
                    $observacion = $_POST['EditarObservacion_'.$i];
                }
				
				$value = "'".$_POST['Editarcedula_'.$i]."' , '".$_POST['EditarFechaInicio_'.$i]."', '".$_POST['EditarFechaFinal_'.$i]."', '".$_POST['EditarEstado_'.$i]."', '".$nomina."', '".$anho."', '".$empres."', ".$razon." , '".$ruta2."', '".$observacion."' ";

                $resultado = ModeloTesoreria::mdlInsertar($tabla, $campos, $value);
                //print_r($resultado);
                if($resultado != 'Error'){
                	/*si fue aceptada debemos registrarla en el sistema*/
                	if($_POST['EditarEstado_'.$i] == 'ACEPTADA'){
                		//Tenemos que insertar en la Incapacidades
                		//Peeeero hay que ver que no exista esta incapacidad
                		$item1 = "inc_cc_afiliado";
                        $valor1 = $_POST['Editarcedula_'.$i];
                        $item2 = 'inc_fecha_inicio';
                        $valor2 = $_POST['EditarFechaInicio_'.$i];
                        $existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2, $_SESSION['cliente_id']);
                        $incapacidaDato = null;
                        //print_r($existeInc);
                        if(!$existeInc){
                        	//No existe toca insertarla vamos a crearla (y)
	                		$item = 'emd_cedula';
	        				$valuess = $_POST['Editarcedula_'.$i];
	        				$tabla = 'gi_empleados';
	        				$res = ModeloEmpleados::mdlMostrarEmpleados($tabla, $item, $valuess);
	        				if($res != null && $res != false){
	        					$campos = "inc_emd_id, inc_empresa, inc_ips_afiliado, inc_cc_afiliado, inc_fecha_inicio, inc_fecha_final, inc_estado, inc_estado_tramite";
	        					$valore = "'".$res['emd_id']."', '".$res['emd_emp_id']."', '".$res['emd_eps_id']."', '".$_POST['Editarcedula_'.$i]."', '".$_POST['EditarFechaInicio_'.$i]."', '".$_POST['EditarFechaFinal_'.$i]."', 1, 'POR RADICAR' ";
	        					$resultadoInca = ModeloTesoreria::mdlInsertar('gi_incapacidad', $campos, $valore);
	        				}
    					}                        
                	}
                	$listo++;
                }else{
                	$noexisten++;
                }
            }
            
        }

        echo json_encode(array('total' => $contados-1 , 'fallas' => $noexisten, 'exito'=> $listo));
    
    }

    if(isset($_POST['getDataIncapacidades']) && $_POST['getDataIncapacidades'] != 0){
        $campo = "incn_cc_v, DATE_FORMAT(incn_fecha_inicio_d, '%d/%m/%Y') as incn_fecha_inicio_d  , DATE_FORMAT(incn_fecha_fin_d, '%d/%m/%Y') as incn_fecha_fin_d, incn_estado_v, incn_nomina_i, incn_anho_i, incn_emp_id, incn_razon_v, emd_nombre, ips_nombre, incn_id_i, emd_cargo, (DATEDIFF(incn_fecha_fin_d,incn_fecha_inicio_d) + 1) as dias, mot_desc_v,inc_observacion_v, inc_img_ruta_v, emd_sede , CASE emd_estado WHEN 1 THEN 'ACTIVO' WHEN 0 THEN 'NO ACTIVO' END as emd_estado";
        $tabla = "gi_incapacidades_nomina JOIN gi_empleados ON incn_cc_v = emd_cedula JOIN  gi_ips ON emd_eps_id = ips_id LEFT JOIN gi_motivos_rechazo ON mot_id_i = incn_razon_v ";
        $where = " incn_id_i = ".$_POST['getDataIncapacidades'];
        $respu = ModeloTesoreria::mdlMostrarUnitario($campo,$tabla,$where);
        echo json_encode($respu);

    }

    if(isset($_POST['getDataNotificaciones']) && $_POST['getDataNotificaciones'] != 0){
        $campo = "DATE_FORMAT(inn_fecha_notificacion, '%d/%m/%Y %H:%i:%s') as inn_fecha_notificacion ";
        $tabla = "incapacidad_notificaciones";
        $where = " inn_inc_id_i = ".$_POST['getDataNotificaciones'];
        $respu = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$where, null, 'Order By inn_fecha_notificacion asc');
        echo json_encode($respu);

    }
	
	