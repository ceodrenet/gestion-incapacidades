<?php 
	session_start();
	require_once '../modelos/dao.modelo.php';
	if (isset($_SESSION['start']) && (time() - $_SESSION['start'] > 1500)) {
		$respuesta = ModeloDAO::mdlEditar("gi_sessiones", "session_fecha_final_d = '".date('Y-m-d').' '.date('H:i:s')."' ", "session_idSession_v = '".$_SESSION['idSession']."'");
		$ultimoLogin = ModeloDAO::mdlEditar("gi_usuario", "user_online_v = 0", "user_id = '".$_SESSION['id_Usuario']."'");

	    session_unset(); 
	    session_destroy(); 
		echo json_encode(array('code' => 0, 'msg' => 'Session Terminada'));
	}else{
		echo json_encode(array('code' => 1, 'msg' => 'Todo Bien', 'tiempo' => $_SESSION['start']));
	}