<?php
session_start();
$_SESSION['start'] = time();
ini_set('display_errors', 'On');
ini_set('display_errors', 1);
require_once '../modelos/dao.modelo.php';
require_once '../modelos/tesoreria.modelo.php';

require_once '../controladores/mail.controlador.php';
require_once '../controladores/plantilla.controlador.php';
require_once '../controladores/incapacidades.controlador.php';
date_default_timezone_set('America/Bogota');

$month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

if(isset($_POST['sendMailSolicituIncapacidades']) && $_POST['sendMailSolicituIncapacidades'] != ''){
	//Primero obtenemos los datos de la incapacidad
 	$campo_ = "gi_empresa.emp_correo_incapacidades_v, gi_empresa.emp_crv_v ,gi_empresa.emp_direccion, gi_empresa.emp_nombre, gi_empresa.emp_nit, gi_empleados.emd_nombre, gi_empleados.emd_cedula, DATE_FORMAT(inc_fecha_inicio, '%d/%m/%Y') as inc_fecha_inicio, DATE_FORMAT(inc_fecha_final, '%d/%m/%Y') AS inc_fecha_final, gi_ips.ips_nombre, DATEDIFF(inc_fecha_final,inc_fecha_inicio) as total_dias, emd_ciudad , emp_telefono_gestion, emp_contacto_gestion, ips_id " ;
 	$tabla_ = " gi_incapacidad JOIN gi_empleados ON gi_empleados.emd_id = gi_incapacidad.inc_emd_id JOIN gi_empresa ON gi_empleados.emd_emp_id = gi_empresa.emp_id JOIN gi_ips ON gi_empleados.emd_eps_id = gi_ips.ips_id";
	$condi_ = " inc_id = ".$_POST['sendMailSolicituIncapacidades'];
	$resultado = ModeloTesoreria::mdlMostrarUnitario($campo_, $tabla_, $condi_ );


	if($resultado != false){
		$titulo = 'SOLICITUD DE PAGO DE INCAPACIDADES  - '.mb_strtoupper($resultado['emd_nombre']);

		$mensaje = '
<html>
	<head>
		<title>SOLICITUD DE PAGO DE INCAPACIDADES  - '.mb_strtoupper($resultado['emd_nombre']).'</title>
	</head>
	<body style="text-align:justify;">
		<p>Barranquilla, '.date('d').' de '.$month[date('m')-1].' de '.date('Y').'</p>
		<br />
		<br />
		<p>Señores</p>
		<p><b>'.mb_strtoupper($resultado['ips_nombre']).'</b><br />
			Prestaciones Económicas<br />
			Ciudad
		</p>
		<br />
	
		<p>Asunto: SOLICITUD DE PAGO DE INCAPACIDAD </P>
		<br />
	
	
		<p>Con la presente  '.mb_strtoupper($resultado['emp_nombre']).', identificada con NIT. '.$resultado['emp_nit'].', conforme a lo establecido en el artículo 1 del Decreto 2943 del 2013, artículo 227 del Código Sustantivo del Trabajo, Sentencia T-333 de 2016, Sentencia T-218 de 2018 y Sentencia T-526 de 2019 , solicitamos a '.mb_strtoupper($resultado['ips_nombre']).' el reconocimiento y pago de las incapacidad de nuestro empleado '.mb_strtoupper($resultado['emd_nombre']).' identificado con CC. '.$resultado['emd_cedula'].', toda vez que '.mb_strtoupper($resultado['ips_nombre']).' ha demorado de manera injustificada el pago de las incapacidades, incumpliendo así con su deber de pago oportuno establecido en el artículo 24 del Decreto 4023 de 2011.  </p>
		<p>DATOS DE LA INCAPACIDAD:</p>
		<br />
		<table border="1" width="35%" style="border:solid 1px;">
		 	<tr style="border:solid 1px;">
				<th align="center">FECHA INICIO</th>
				<th align="center">FECHA FINAL</th>
				<th align="center">DÍAS</th>
            </tr>
            <tr style="border:solid 1px;">
				<td align="center">'.$resultado['inc_fecha_inicio'].'</td>
				<td align="center">'.$resultado['inc_fecha_final'].'</td>
				<td align="center">'.($resultado['total_dias']+1).'</td>
            </tr>
		</table>
		<br />
		<br />
		<p>DECRETO 4023 DE 2011</p>
		<br />
		<p><b>Artículo 24.</b> Pago de prestaciones económicas. A partir de la fecha de entrada en vigencia de las cuentas maestras de recaudo, los aportantes y trabajadores independientes, no podrán deducir de las cotizaciones en salud, los valores correspondientes a incapacidades por enfermedad general y licencias de maternidad y/o paternidad. El pago de estas prestaciones económicas al aportante, será realizado directamente por la EPS y EOC, a través de reconocimiento directo o transferencia electrónica <b>en un plazo no mayor a cinco (5) días hábiles</b> contados a partir de la autorización de la prestación económica por parte de la EPS o EOC. La revisión y liquidación de las solicitudes de reconocimiento de prestaciones económicas <b>se efectuará dentro de los quince (15) días hábiles siguientes a la solicitud del aportante.</b> En todo caso, para la autorización y pago de las prestaciones económicas, las EPS y las EOC deberán verificar la cotización al Régimen Contributivo del SGSSS, efectuada por el aportante beneficiario de las mismas. 
		</p>
		<p>
			<b>Parágrafo 1°.</b> La EPS o la EOC que no cumpla con el plazo definido para el trámite y pago de las prestaciones económicas, <b>deberá realizar el reconocimiento y pago de intereses moratorios al aportante</b>, de acuerdo con lo definido en el artículo 4° del Decreto 1281 de 2002. 
		</p>
		<p>
			<b>Parágrafo 2°. De presentarse incumplimiento</b> del pago de las prestaciones económicas por parte de la EPS o EOC, el aportante <b>deberá informar a la Superintendencia Nacional de Salud, para que de acuerdo con sus competencias, esta entidad adelante las acciones a que hubiere lugar.</b>
		</p>
		<br />
		<br />
		<p>Cualquier información adicional será suministrada en el teléfono 322 5973028 o al correo electrónico '.$resultado['emp_correo_incapacidades_v'].'</p>
		<br />
		<br />
		<p>Atentamente,</p>
		<br />
		<br />
		<p>
			'.mb_strtoupper($resultado['emp_contacto_gestion']).'
			<br/>
			'.$resultado['emp_telefono_gestion'].'
			<br/>
			Gestión Humana 
		</p>
	</body>
</html>';

		$correoSuper = ModeloTesoreria::mdlMostrarUnitario('ips_correo_cartera', 'gi_ips', 'ips_id = '.$resultado['ips_id']);
		var_dump($correoSuper);
        $ctrMail= new ctrMail();
        $resultadoMail =  $ctrMail->EnviarMailWithEmailAndPass($resultado['emp_correo_incapacidades_v'], $resultado['emp_crv_v'], 'Peticiones incapacidades', $titulo, $mensaje, $correoSuper['ips_correo_cartera'], NULL , null);
        if($resultadoMail != 'ok'){
        	echo json_encode(array('code' =>0, 'message' => 'No se enviar el correo por esto => '.$resultadoMail));
        }else{
        	echo json_encode(array('code' =>1, 'message' =>'Enviado'));
        }
	}
}