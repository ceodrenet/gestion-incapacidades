<?php 
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    session_start();
    $_SESSION['start'] = time();

    require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
    require_once '../controladores/incapacidades.controlador.php';
    require_once '../controladores/empleados.controlador.php';
    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
    require_once '../modelos/empleados.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';
    require_once '../extenciones/Excel.php';

    if(isset($_FILES['NuevoIncapacidadComent']['tmp_name']) && !empty($_FILES['NuevoIncapacidadComent']['tmp_name']) ){
        $name   = $_FILES['NuevoIncapacidadComent']['name'];
        $tname  = $_FILES['NuevoIncapacidadComent']['tmp_name'];
        ini_set('memory_limit','1028M');

        $obj_excel = PHPExcel_IOFactory::load($tname);
        $sheetData = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
        $arr_datos = array();
        $highestColumm = $obj_excel->setActiveSheetIndex(0)->getHighestColumn(); // e.g. "EL"
        $highestRow = $obj_excel->setActiveSheetIndex(0)->getHighestRow();
        $fallos   = 0;
        $total = 0;
        $exito = 0;
        $noexiste = 0;
        foreach ($sheetData as $index => $value) {
            if ( $index > 1 ){
                if((!is_null($value['A']) && !empty($value['A'])) && (!is_null($value['B']) && !empty($value['B']))){
                    $total++;
                    $datos = array();
                    $item = 'emd_cedula';
                    $valuess = $value['A'];
                    $tabla = 'gi_empleados';
                    $res = ModeloEmpleados::mdlMostrarEmpleados($tabla, $item, $valuess);
                    //print_r($res);
                    $existe = false;
                    if($res['emd_cedula'] == $value['A']){
                        /*Empleado Existente*/
                        $existe = true;
                        $datos['inc_emd_id'] = $res['emd_id'];
                        $datos['inc_empresa'] = $res['emd_emp_id'];
                        $datos['inc_ips_afiliado'] = $res['emd_eps_id'];
                    }else{
                        $noexiste++;
                    }

                    if($existe){
                        $item1 = "inc_cc_afiliado";
                        $valor1 = $value['A'];
                        $item2 = 'inc_fecha_inicio';
                        $valor2 = $value['B'];
                        $existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2, $_SESSION['cliente_id']);
                        
                        if($existeInc){
                            $id_incapacidad = ModeloIncapacidades::mdlVerIdIncapacidades($item1, $valor1, $item2, $valor2, $_SESSION['cliente_id']);
                            
                            if(!is_null($value['D']) && !empty($value['D'])){
                                $campos = "inm_inc_id_i, incm_usuer_id_i, incm_comentario_t, incm_fecha_comentario";
                                $valores =  $id_incapacidad['inc_id'].", ".$_SESSION['id_Usuario'].", '".$value['D']."','".date('Y-m-d H:i:s')."' ";
                                $respu = ModeloIncapacidades::mdlCrear('gi_incapacidades_comentarios', $campos, $valores);

                                if($respu != 'error'){
                                    $exito++;
                                }else{
                                    $fallos++;
                                }
                            }
                        }
                    }
                }
            }
        }

        echo json_encode(array('total' => $total , 'exito' => $exito , 'fallaron' => $fallos, 'Noexiste' => $noexiste));
    }