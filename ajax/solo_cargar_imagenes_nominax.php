<?php
    session_start();
    $_SESSION['start'] = time();
	require_once '../modelos/dao.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';

 
    if(isset($_POST['contados']) && $_POST['contados'] != 0 ){
        $noexisten = 0;
        $listo = 0;
        $fallos = 0;
        $contados = ($_POST['contados'] +1);
        $campos = "incn_cc_v, incn_fecha_inicio_d, incn_nomina_i, incn_anho_i, incn_emp_id, incn_ruta_incapacidad,  incn_ruta_incapacidad_transcrita";
        $tabla = "gi_incapacidades_nomina";

        for ($i=0; $i < $contados; $i++) { 
            if(isset($_POST['Editarcedula_'.$i]) && $_POST['Editarcedula_'.$i] != '' ){
                /* Viene la cedula vamos apreguntar por la incapacidad a ver */
                $tieneInca = ModeloTesoreria::mdlMostrarUnitario("count(incn_id_i) as total", "gi_incapacidades_nomina" ," incn_cc_v = '".$_POST['Editarcedula_'.$i]."' AND incn_fecha_inicio_d='".$_POST['EditarFechaInicio_'.$i]."'");
                
                if($tieneInca['total'] == 0){
                    //No existe esa incapacidad todavia
                    //La creamos
                    
                        $fallos++;
                    

                }else{
                    //*Actualizamos*/

                    $ruta2 = '';
                    $ruta3 = '';
                    if(isset($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name']) && !empty($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name'])){
                        /* Vamos a cargar la primera imagen */
 
                        list($ancho, $alto) = getimagesize($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name']);
                        $nuevoAncho = 500;
                        $nuevoAlto  = 500;
                        $ruta = '';
 
                        /* Creamos el directorio */
                        $directorio = __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/";
                        if (!is_dir($directorio)) {
                            mkdir($directorio, 0755, true);
                        }
 
 
                        /* De acuerdo al tipo de imagen se le aplican funciones de php */
                        if($_FILES["EditarFilePrimerImagen_".$i]["type"] == "image/jpeg"){
                            //creamos un numero aleatorio
                            $aleatorio = mt_rand(1000, 9999);
                            //creamos la ruta donde se va a guardar la imagen
                            $ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".jpg";
                            $ruta2 = "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".jpg";
                            //obtenemos el origen osea el FILE
                            $origen  = imagecreatefromjpeg($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name']);
                            //le decimos que vamos acrear una imagen en el destino con esos ancho y alto
                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                     
                             
                            imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                            imagejpeg($destino, $ruta);
 
                        }elseif($_FILES["EditarFilePrimerImagen_".$i]["type"] == "image/png"){
                            //creamos un numero aleatorio
                            $aleatorio = mt_rand(1000, 9999);
                            //creamos la ruta donde se va a guardar la imagen
                            $ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".png";
                            $ruta2 =  "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".png";
                            //obtenemos el origen osea el FILE
                            $origen  = imagecreatefrompng($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name']);
                            //le decimos que vamos acrear una imagen en el destino con esos ancho y alto
                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                            //cortamos esta imagen
                            
                            imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                            imagepng($destino, $ruta);
 
                        }else{
                            //creamos un numero aleatorio
                            $aleatorio = mt_rand(100, 999);
                            //creamos la ruta donde se va a guardar la imagen
                            $ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".pdf";
                            $ruta2 =  "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".pdf";
                            /* Creamos el directorio */
                            $directorio = __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/";
                            if (!is_dir($directorio)) {
                                mkdir($directorio, 0755, true);
                            }
 
                            copy($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name'], $ruta);
 
                        } 
                    }
 
 
                    $ruta3 = $_POST['EditarObservacion_'.$i];

                    /*Insertamos*/ 
                    /*$valores = "incn_cc_v = '".$_POST['Editarcedula_'.$i]."', incn_fecha_inicio_d = '".$_POST['EditarFechaInicio_'.$i]."', incn_nomina_i = '".$_POST['txtNomina']."', incn_anho_i = '".date('Y')."', incn_emp_id = '".$_POST['empresa']."', inc_img_ruta_v = '".$ruta2."', inc_observacion_v = '".$ruta3."'";*/
                    
                    $valores = "inc_img_ruta_v = '".$ruta2."', inc_observacion_v = '".$ruta3."', inc_edi_user_id_i = ".$_SESSION['id_Usuario'];

                    $resultado = ModeloTesoreria::mdlActualizar($tabla, $valores, "incn_cc_v = '".$_POST['Editarcedula_'.$i]."' AND incn_fecha_inicio_d='".$_POST['EditarFechaInicio_'.$i]."'");
                    if($resultado != 'Error'){
                        $listo++;
                    }else{
                        $fallos++;
                    }
                }
            }
        }

        $valorFinal = $contados - @$fallo - 1 ;
        echo json_encode(array('total' => $contados-1 , 'fallas' => $fallos, 'exito'=> $valorFinal));
    }