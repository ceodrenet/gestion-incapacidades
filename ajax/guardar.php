<?php
    session_start();
    $_SESSION['start'] = time();
	$directorio = __DIR__."/estadisticas/";
    if (!file_exists($directorio)) {
        mkdir($directorio, 0755);
    }
    $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $baseFromJavascript));
    // Proporciona una locación a la nueva imagen (con el nombre y formato especifico)
    $filepath = $directorio.$_POST['imagen'].".png"; // or image.jpg
	// Finalmente guarda la imágen en el directorio especificado y con la informacion dada
	file_put_contents($filepath, $data);