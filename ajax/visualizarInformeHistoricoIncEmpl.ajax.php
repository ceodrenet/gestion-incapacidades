<?php 
    session_start();
    $_SESSION['start'] = time();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);

    require_once '../modelos/dao.modelo.php';

    $fieldInfoEmpl = "emd_nombre, emd_cedula, emd_fecha_nacimiento, IF(emd_fecha_nacimiento>=CURDATE(),YEAR(CURDATE())-YEAR(emd_fecha_nacimiento),(YEAR(CURDATE())-YEAR(emd_fecha_nacimiento))-1) as emd_edad,emd_genero,
    emd_cargo, emd_fecha_ingreso, eps.ips_nombre as emd_eps, emd_fecha_afiliacion_eps, afp.ips_nombre as emd_afp, emd_ciudad, emd_sede, 
    emd_subcentro_costos, emd_centro_costos";
    $tableJoinsEmpl = "gi_empleados LEFT JOIN gi_ips eps ON emd_eps_id = eps.ips_id LEFT JOIN gi_ips afp ON emd_afp_id = afp.ips_id";

    $fieldDataDiasInc = "DAY(inc_fecha_inicio) as dia_inicio,DAY(inc_fecha_final) as dia_final,MONTH(inc_fecha_inicio) as mes_inicio,MONTH(inc_fecha_final) as mes_final, DATEDIFF(inc_fecha_final,inc_fecha_inicio)+1 as dias, inc_estado_tramite as est_tramite";

    $cedulaEmpl = (isset($_POST["cedula"])) ? $_POST["cedula"] : null;

    $dataEmpleado = ModeloDAO::mdlMostrarUnitario($fieldInfoEmpl, $tableJoinsEmpl, "emd_cedula = '$cedulaEmpl'");
    $dataYearsInc = ModeloDao::mdlMostrarGroupAndOrder("DISTINCT EXTRACT(YEAR FROM inc_fecha_inicio) AS years", "gi_incapacidad", "inc_cc_afiliado = '$cedulaEmpl'", null, "ORDER BY years DESC");
    
    $dataDiasInc = [];
    if (!empty($dataYearsInc)) {
        $dataDiasInc = ModeloDAO::mdlMostrarGroupAndOrder($fieldDataDiasInc, "gi_incapacidad", "inc_cc_afiliado = '$cedulaEmpl' AND YEAR(inc_fecha_inicio) = '".$dataYearsInc[0]["years"]."'");
    }
?>

<style>
    /* table,*/
    thead{
        background-color: #C8D3D5;
        text-align: center;
    }
    tr,
    th {
        width: 100px;
        /* word-wrap: break-word; */
    } 

    td{
        text-align: center;
        color: #928484;
    }

    .scroll{
        overflow-x: auto;
    }

    .domingo{
        color: #15809C;
    }

    .inc-empresa{
        background-color: #BFBFBF;
    }

    .inc-pendiente{
        background-color: #B0D1FF;
    }

    .inc-pagada{
        background-color: #BFFF9A;
    }

    .inc-negada{
        background-color: #FF9999;
    }

    .inc-rechazada{
        background-color: #FFFD62;
    }

</style>
<div class="card border border-danger">
    <div class="card-header border border-white">
        <h5 class="text-center text-danger">HISTÓRICO DE INCAPACIDADES POR COLABORADOR</h5>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4">
            <?php if ($dataEmpleado["emd_genero"] == "MASCULINO") {
                echo '<img class="img-fluid" src="vistas/img/plantilla/avatar-male.jpg" alt="avatar">';
            } else {
                echo '<img width="370" class="img-fluid mb-2" src="vistas/img/plantilla/avatar-female.jpg" alt="avatar">';
            }
             ?>
            
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8 px-sm-4 mb-3">
            <div class="row mb-3">
                <div class="col-6">
                    <label>COLABORADOR:</label>
                    <span><?php echo strtoupper($dataEmpleado["emd_nombre"]) ?></span>
                </div>
                <div class="col-6">
                    <label>IDENTIFICACIÓN:</label>
                    <span><?php echo strtoupper($dataEmpleado["emd_cedula"]) ?></span>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-6">
                    <label>F.NACIMIENTO:</label>
                    <span><?php echo strtoupper($dataEmpleado["emd_fecha_nacimiento"]) ?></span>
                </div>
                <div class="col-6">
                    <label>EDAD:</label>
                    <span><?php echo strtoupper($dataEmpleado["emd_edad"]) ?></span>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-6">
                    <label>CARGO:</label>
                    <span><?php echo strtoupper($dataEmpleado["emd_cargo"]) ?></span>
                </div>
                <div class="col-6">
                    <label>FECHA DE INGRESO:</label>
                    <span><?php echo strtoupper($dataEmpleado["emd_fecha_ingreso"]) ?></span>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-6">
                    <label>EPS:</label>
                    <span><?php echo strtoupper($dataEmpleado["emd_eps"]) ?></span>
                </div>
                <div class="col-6">
                    <label>FECHA DE AFILIACION A EPS:</label>
                    <span><?php echo strtoupper($dataEmpleado["emd_fecha_afiliacion_eps"]) ?></span>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-6">
                    <label>AFP:</label>
                    <span><?php echo strtoupper($dataEmpleado["emd_afp"]) ?></span>
                </div>
                <div class="col-6">
                    <label>CIUDAD LABORAL:</label>
                    <span><?php echo strtoupper($dataEmpleado["emd_ciudad"]) ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <label>DEPENDENCIA:</label>
                    <span><?php echo $dataEmpleado["emd_sede"] ?></span>
                </div>
                <div class="col-4">
                    <label>REGIONAL:</label>
                    <span><?php echo $dataEmpleado["emd_subcentro_costos"] ?></span>
                </div>
                <div class="col-4">
                    <label>SEDE:</label>
                    <span><?php echo $dataEmpleado["emd_centro_costos"] ?></span>
                </div>
            </div>
        </div>
        <?php if(!empty($dataYearsInc)) {?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 scroll">
            <div class="d-flex justify-content-center">
                <span class="mx-3">empresa</span>
                <span class="mx-3" style="background-color: #BFBFBF; width: 50px; height: 15px;"></span>-
                <span class="mx-3">pagada</span>
                <span class="mx-3" style="background-color: #BFFF9A; width: 50px; height: 15px;"></span>-
                <span class="mx-3">pendiente</span>
                <span class="mx-3" style="background-color: #B0D1FF; width: 50px; height: 15px;"></span>-
                <span class="mx-3">negada</span>
                <span class="mx-3" style="background-color: #FF9999; width: 50px; height: 15px;"></span>-
                <span class="mx-3">rechazada</span>
                <span class="mx-3" style="background-color: #FFFD62; width: 50px; height: 15px;"></span>
            </div>
            <table class="" width="100%" id="table-calendar">
                <div id="result-tbl">
                    <thead id="tbl-head">
                        <tr>
                            <th>
                                <select id="selectYear">
                                <?php 
                                    foreach ($dataYearsInc as $value) {
                                        echo "<option value='".$value["years"]."'>".$value["years"]."</option>";
                                    }
                                ?>
                                </select>
                            </th>
                            <th>D</th>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                            <th>D</th>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                            <th>D</th>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                            <th>D</th>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                            <th>D</th>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                            <th>D</th>
                            <th>L</th>
                            <th>M</th>
                            <th>X</th>
                            <th>J</th>
                            <th>V</th>
                            <th>S</th>
                        </tr>
                    </thead>
                    <tbody id="tbl-body"></tbody>
                </div>
            </table>
        </div>
        <?php }?>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        var dataDias = <?php echo json_encode($dataDiasInc) ?>
        
        var year = ""
        if (dataDias.length > 0) {
            
            year = <?php echo !empty($dataYearsInc) ? json_encode($dataYearsInc[0]["years"]) : ""  ?>

            calendarGraphic(year,dataDias)
        }
        
        
        let newData = []
        $("#selectYear").change(function(){
            
            
            var year = $(this).val()
            $.ajax({
                url: 'ajax/graficoHistoricoIncEmpl.php',
                type: 'post',
                data: { 
                    year: $(this).val(), 
                    cedula: <?php echo $cedulaEmpl ?>,
                },
                dataType: 'json',
                success: function(data) {
                    $("#tbl-body > tr").remove()
                    calendarGraphic(year, data)
                }
            })
        
        })
        
    });

    function calendarGraphic(year = '2023', data = []){

        var months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]

        var tblHead = document.getElementById("tbl-head")
        var tblBody = document.getElementById("tbl-body")

        var fila = tblHead.rows[0];
        var celdas = fila.cells;

        for (let i = 0; i < months.length; i++) {

            //creamos las filas tr para cada mes
            var row = document.createElement("tr")
            row.setAttribute("id",`fila${i+1}`)
            var celdaMes = document.createElement("th")

            celdaMes.innerHTML = months[i]
            row.appendChild(celdaMes)   

            var firstDay = new Date(year, i, 1).getDay(),
                lastDate = new Date(year, i + 1, 0).getDate()

            //primero creamos td vacíos hasta el primer día del mes en caso de no comenzar un domingo
            for (let j = firstDay; j > 0; j--) {
                var celdaDia = document.createElement("td")
                row.appendChild(celdaDia)
            }

            //creamos los td de los días del mes hasta su ultimo día
            for (let k = 1; k <= lastDate; k++) {
                var date = new Date(`${year}-${i+1}-${k}`)
                var celdaDia = document.createElement("td")
                celdaDia.innerHTML = `${k}`

                row.appendChild(celdaDia)
            }

            //creamos los td restantes en la tabla
            for (var l = 0; l < (celdas.length-1)-(lastDate+firstDay); l++) {
                var celdaDia = document.createElement("td")
                row.appendChild(celdaDia)
            }

            //agregamos los tr al tbody
            tblBody.appendChild(row)
        }

        
        for (let i = 0; i < months.length; i++) {
            var getRow = document.getElementById(`fila${i+1}`)
            var TDs = getRow.getElementsByTagName("td")
            for (let t = 0; t < TDs.length; t++) {
                if (t == 0) {
                    TDs[t].classList.add('domingo')
                }

                if (t%7==0) {
                    TDs[t].classList.add('domingo')
                }

            }
        }
        
        for (let j = 0; j < data.length; j++) {
            for (let i = 0; i < months.length; i++) {
                if (i == data[j].mes_inicio-1) {
                    var getRow = document.getElementById(`fila${i+1}`)
                    var TDs = getRow.getElementsByTagName("td")
                    var index = Array.prototype.findIndex.call(TDs, function(td){
                        return td.innerHTML === data[j].dia_inicio
                    })
                    var long = parseInt(data[j].dias)+index
                    var clrClass = validarClrClassByEstado(data[j].est_tramite)
                    
                    for (let t = index; t <= long-1; t++) {
                        if (TDs[t].textContent.trim() != "") {

                            TDs[t].classList.add(clrClass)   
                        }else {
                            drawIncNextMonth(i+2, data[j].mes_final, data[j].dia_final, clrClass)
                            break
                        }
                    }
                }
            }
        }
        // console.log(data)
    }

    function drawIncNextMonth(nextMonth, untilMonth, lastDay, clrClass){

        for (let i = nextMonth; i <= 12; i++) {
            var getRow = document.getElementById(`fila${i}`)
            var TDs = getRow.getElementsByTagName("td")
            var index = Array.prototype.findIndex.call(TDs, function(td){
                return td.textContent.trim() != ""
            })   

            for (let j = index; j < TDs.length; j++) {
                if (parseInt(TDs[j].textContent) <= parseInt(lastDay) && parseInt(i) <= parseInt(untilMonth)) {
                    TDs[j].classList.add(clrClass)   
                }
            }    
        }
    }

    function validarClrClassByEstado(estado){
        if (estado == "1") {
            return "inc-empresa"
        }
        if (estado == "2" || estado == "3" ||estado == "4") {
            return "inc-pendiente"
        }
        if (estado == "5") {
            return "inc-pagada"
        }
        if (estado == "6") {
            return "inc-negada"
        }
        if (estado == "7") {
            return "inc-rechazada"
        }
    }
</script>