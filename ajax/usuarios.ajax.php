<?php
	session_start();
	$_SESSION['start'] = time();
 	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
  	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/archivos.controlador.php';
	require_once '../controladores/usuarios.controlador.php';
	require_once "../extenciones/PHPMailer/class.phpmailer.php";
 	require_once "../extenciones/PHPMailer/class.smtp.php";

	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/archivos.modelo.php';
    require_once '../modelos/usuarios.modelo.php';
	require_once '../modelos/tesoreria.modelo.php';
	
	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxUsuarios
	{
		public $id_usuario;
		public $activarId;
		public $ActivarUsuario;
		public $validaCliente;
		
		public function ajaxEditarUsuario(){
			$item = 'user_id';
			$valor = $this->id_usuario;
			$respuesta = ControladorUsuarios::getData('gi_usuario',$item, $valor);
			echo json_encode($respuesta);
		}

		/* Activar cliente */
		public function ajaxActivarUsuario(){
			$tabla = 'gi_usuario';
			$item1 = 'user_estado_i';
			$valor1 = $this->ActivarUsuario;
			$item2 = 'user_id';
			$valor2 = $this->activarId;
			$respuesta = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
			var_dump($respuesta);
		}

		/* validar que el usuairo no este repetido */
		/*public function ajaxValidarCliente(){
			$item = 'emp_nit';
			$valor = $this->validaCliente;
			$respuesta = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);
			echo json_encode($respuesta);
		}*/

		public function ajaxVerificaPassword($pass){
			$campos = 'user_password';
			$tabla  = 'gi_usuario';
			$condiciones = "user_id = ".$_SESSION['id_Usuario'];
			$usuario = ModeloTesoreria::mdlMostrarUnitario($campos, $tabla, $condiciones);
			$password = $usuario['user_password'];
			$pass = crypt($pass, '$2a$07$usesomesillystringforsalt$');
			if($password == $pass){
				echo 'ok';
			}else{
				echo 'error';
			}
		}

		public function ajaxGuardarPassword($pass){
			$pass = crypt($pass, '$2a$07$usesomesillystringforsalt$');
			$tabla = 'gi_usuario';
			$item1 = "user_password";
			$valor1 = $pass;
			$item2 = "user_id";
			$valor2 = $_SESSION['id_Usuario'];
			$resultado = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
			if($resultado === 'ok'){
				echo $resultado;
			}else{
				echo 'error';
			}
		}


		public function ajaxGuardarCambioPassword($correo){

			/* Primero obtenemos el correo registrado */
			$campos = "user_recordatorio_v, usu_correo_v, user_nombre";
			$tablas = "gi_usuario";
			$where_ = "usu_correo_v = '".$correo."'";
			
			$respuesta = ModeloTesoreria::mdlMostrarUnitario($campos, $tablas, $where_);

			//var_dump($respuesta);
			
			$para  =  $respuesta['usu_correo_v'];
			$titulo = 'Información recordatorio de clave';
			$mensaje = '
<html>
	<head>
		<title>Información recordatorio de clave</title>
	</head>
	<body style="text-align:justify;">
  		<p>Hola '.$respuesta['user_nombre'].', sus datos para ingresar al sistema son:</p>
  		<p>Correo    : '.$respuesta['usu_correo_v'].'</p>
  		<p>Contraseña : '.$respuesta['user_recordatorio_v'].'</p>
  		<p>Puedes ingresar haciendo click en el siguiente enlace <a href="https://app.incapacidades.co">app.incapacidades.co</a></p>
  		<p>Atentamente el equipo de soporte de incapacidades.co</p>
	</body>
</html>';
			/*$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
			$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$cabeceras .= 'Bcc: Luis Garcia <clientesgroup@gmail.com>' . "\r\n";
			$cabeceras .= 'From: soporte incapacidades.co <soporte@incapacidades.co>' . "\r\n";*/
			//$cabeceras .= 'Cc: birthdayarchive@example.com' . "\r\n";
			//
			$ctrMail = new ctrMail();
			$cor = $ctrMail->EnviarMail( $titulo, $mensaje, $para, null );
			//echo $cor;
            if( $cor == 'ok'){
            	echo json_encode(array('code' => 'ok', 'correo' => $respuesta['usu_correo_v']));
            }else{
            	echo json_encode(array('code' => 'error', 'msg' => 'No pudimos enviar el recordatorio'));
            }

			
		}

		public function ajaxValidarEmail(){
			$valor = $this->id_usuario;
			$respuesta = ControladorUsuarios::ctrValidarEmail($valor);
			echo ($respuesta);
		}

		

	}

	/*modificar funcion para adaptarla a usuarios*/ 

	if(isset($_GET['dame-Usuarios-inicio'])){
		$item = null;
		$valor = null;
		if($_SESSION['cliente_id'] != 0){
            $item = 'user_id';
            $valor = $_SESSION['cliente_id'];
        }
        $clientes = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);
		//print_r($clientes);
echo '{
  	"data" : [';
  			$i = 0;
		 	foreach ($clientes as $key => $value) {
		 		if($i != 0){
            		echo ",";
            	}
				echo '[';
				echo '"'.$value['user_nombre'].'",';
				echo '"'.$value['usu_correo_v'].'",';
               // echo '"'.$value['user_ruta_imagen_v'].'",';    
				echo '"'.$value['perfiles_descripcion_v'].'",'; 
				if($value['user_estado_i'] == '1'){
					echo '"ACTIVO",'; 	
				}else{
					echo '"NO ACTIVO",'; 
				}
				
				echo '"'.$value['user_ultimo_login'].'",'; 
				echo '"'.$value['user_id'].'"'; 
				echo ']';
            	$i++;
		 	}
		echo ']
}';
	}
	
	if(isset($_POST['EditarUsuarioId'])){
		if($_POST['EditarUsuarioId'] != ''){
			$editar = new AjaxUsuarios();
			$editar->id_usuario = $_POST['EditarUsuarioId'];
			$editar->ajaxEditarUsuario();
		}	
	}

	if(isset($_POST['ActivarId'])){
		$activarUsuarios = new AjaxUsuarios();
		$activarUsuarios->activarId = $_POST['ActivarId'];
		$activarUsuarios->ActivarUsuario = $_POST['estado'];
		$activarUsuarios->ajaxActivarUsuario();
	}


	/*if(isset($_POST['validarNit'])){
		$activarUsuarios = new AjaxUsuarios();
		$activarUsuarios->validaCliente = $_POST['validarNit'];
		$activarUsuarios->ajaxValidarCliente();
	}*/
	

	if(isset($_POST['VerificaPassword'])){
		$editar = new AjaxUsuarios();
		$editar->ajaxVerificaPassword($_POST['VerificaPassword']);
	}

	if(isset($_POST['GuardarPassword'])){
		$editar = new AjaxUsuarios();
		$editar->ajaxGuardarPassword($_POST['GuardarPassword']);
	}

	if(isset($_POST['GuardarPassword_nuevo'])){
		$editar = new AjaxUsuarios();
		$editar->ajaxGuardarCambioPassword($_POST['usuariotxt']);
	}

	if(isset($_POST['registroCorreo'])){
		if($_POST['registroCorreo'] != ''){
			$editar = new AjaxUsuarios();
			$editar->id_usuario = $_POST['registroCorreo'];
			$editar->ajaxValidarEmail();
		}	
	}


	if(isset($_POST['idUsuario'])){
		/* Primero obtenemos el correo registrado */
		$campos = "emu_id_usuario, emu_id_empresa";
		$tablas = "gi_empresa_usuarios";
		$where_ = "emu_id_usuario = '".$_POST['idUsuario']."'";
		
		$respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tablas, $where_, $groupBy = null, $orderBy = null);
		echo json_encode($respuesta);
	}
	
		if(isset($_POST["NuevoUsuario"])){
		echo ControladorUsuarios::ctrCrearUsuario();
	}

	if(isset($_POST["EditarUsuario"])){
		echo ControladorUsuarios::ctrEditarUsuario();
	}

	if(isset($_POST["id_Usuario"])){
		echo ControladorUsuarios::ctrBorrarUsuario();
	}   

	