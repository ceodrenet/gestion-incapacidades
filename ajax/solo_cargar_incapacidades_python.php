<?php
    session_start();
    $_SESSION['start'] = time();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    ini_set('memory_limit','1024');
    ini_set('max_execution_time', 500);

    require_once '../controladores/mail.controlador.php';
    require_once '../modelos/dao.modelo.php';
    require_once '../controladores/plantilla.controlador.php';

    function auditorias($request, $response)
    {
        ControladorPlantilla::setAuditoria('ControladorPlantilla', 'ImportarEmpleadosPython', $request, $response);
    }


    if(isset($_FILES['NuevoIncapacidad']['tmp_name']) && !empty($_FILES['NuevoIncapacidad']['tmp_name']) ){
        $aleatorio = mt_rand(100, 999);
        $ruta =  __DIR__."/../BdPython/".$aleatorio.".xlsx";
            copy($_FILES['NuevoIncapacidad']['tmp_name'], $ruta);
        $request = 'python3 /var/www/html/BdPython/importarIncapacidades.py "'.$aleatorio.".xlsx".'" "'.$_SESSION['cliente_id'].'" ';
        $result = shell_exec($request);
        auditorias($request, "El servicio se ejecuto correctamente");
        if (unlink($ruta)) {
          // file was successfully deleted
        } else {
          
        }
        echo json_encode(array('code' => 1, 'message' => 'proceso Ejecutado, revisar por favor'));

    }