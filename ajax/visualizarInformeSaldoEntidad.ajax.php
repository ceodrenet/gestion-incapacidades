<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<style>
    table,
    tr,
    td,
    th {
        border: none;
    }
    .scroll{
        max-height: 40rem;
        overflow-y: auto;
    }
</style>

<?php 
session_start();
$_SESSION['start'] = time();
ini_set('display_errors', 'On');
ini_set('display_errors', 1);
require_once '../controladores/mail.controlador.php';
require_once '../controladores/plantilla.controlador.php';
require_once '../controladores/incapacidades.controlador.php';
require_once '../controladores/tesoreria.controlador.php';

require_once '../modelos/dao.modelo.php';
require_once '../modelos/incapacidades.modelo.php';
require_once '../modelos/tesoreria.modelo.php';

$filtroFecha = $_POST['tipoFecha'];

$tablaEps ="gi_incapacidad LEFT JOIN gi_ips eps ON inc_ips_afiliado = eps.ips_id ";
$whereEps = "(inc_valor_pagado_eps != 0 OR inc_valor != 0 ) ";
$groupEps = "GROUP BY (eps_nombre)";
$orderByEps = "ORDER BY (eps_nombre)";

$tablaArl ="gi_incapacidad LEFT JOIN gi_ips arl ON inc_arl_afiliado = arl.ips_id ";
$whereArl = "(inc_valor_pagado_eps != 0 OR inc_valor != 0 ) ";
$groupArl = "GROUP BY (arl_nombre)";
$orderByArl = "ORDER BY (arl_nombre)";

$where0y30 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 0 AND 30";
$where31y60 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 31 AND 60 ";
$where61y90 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 61 AND 90 ";
$where91y180 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 91 AND 180 ";
$where181y360 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 181 AND 360 ";
$whereSup360 = "DATEDIFF(DATE(NOW()), $filtroFecha) > 360 ";

$whereArl0y30 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 0 AND 30 AND inc_origen = 4 ";
$whereArl31y60 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 31 AND 60 AND inc_origen = 4 ";
$whereArl61y90 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 61 AND 90 AND inc_origen = 4 ";
$whereArl91y180 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 91 AND 180 AND inc_origen = 4 ";
$whereArl181y360 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 181 AND 360 AND inc_origen = 4 ";
$whereArlSup360 = "DATEDIFF(DATE(NOW()), $filtroFecha) > 360 AND inc_origen = 4 ";

if ($_POST['year'] != 0) {
    $fecha1 = $_POST['year']."-01-01";
    $fecha2 = $_POST['year']."-12-31";

    $whereEps.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArl.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";

    $where0y30.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $where31y60.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $where61y90.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $where91y180.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $where181y360.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereSup360.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";

    $whereArl0y30.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArl31y60.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArl61y90.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArl91y180.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArl181y360.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArlSup360.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
}


$sumTipoValor = "";
if ($_POST["estadoTramite"] == 1) {
    $whereEps.="AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArl.="AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    
    $where0y30 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";
    $where31y60 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";
    $where61y90 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";
    $where91y180 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";
    $where181y360 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";
    $whereSup360 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";

    $whereArl0y30 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArl31y60 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArl61y90 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArl91y180 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArl181y360 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArlSup360 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";

    $sumTipoValor = "SUM(inc_valor)";
} else if ($_POST["estadoTramite"] == 2) {
    $whereEps.="AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArl.="AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";

    $where0y30 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";
    $where31y60 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";
    $where61y90 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";
    $where91y180 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";
    $where181y360 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";
    $whereSup360 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";

    $whereArl0y30 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArl31y60 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArl61y90 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArl91y180 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArl181y360 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArlSup360 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";

    $sumTipoValor = "SUM(inc_valor_pagado_eps)";
} else if ($_POST["estadoTramite"] == 3) {
    $whereEps.="AND (inc_estado_tramite = 6) ";
    $whereArl.="AND (inc_estado_tramite = 6) ";

    $where0y30 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";
    $where31y60 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";
    $where61y90 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";
    $where91y180 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";
    $where181y360 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";
    $whereSup360 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";

    $whereArl0y30 .= "AND (inc_estado_tramite = 6) ";
    $whereArl31y60 .= "AND (inc_estado_tramite = 6) ";
    $whereArl61y90 .= "AND (inc_estado_tramite = 6) ";
    $whereArl91y180 .= "AND (inc_estado_tramite = 6) ";
    $whereArl181y360 .= "AND (inc_estado_tramite = 6) ";
    $whereArlSup360 .= "AND (inc_estado_tramite = 6) ";
    
    $sumTipoValor = "SUM(inc_valor_pagado_eps)";
} else {
    $whereEps.="AND (inc_estado_tramite != 1) ";
    $whereArl.="AND (inc_estado_tramite != 1) ";

    $where0y30 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";
    $where31y60 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";
    $where61y90 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";
    $where91y180 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";
    $where181y360 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";
    $whereSup360 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";

    $whereArl0y30 .= "AND (inc_estado_tramite != 1) ";
    $whereArl31y60 .= "AND (inc_estado_tramite != 1) ";
    $whereArl61y90 .= "AND (inc_estado_tramite != 1) ";
    $whereArl91y180 .= "AND (inc_estado_tramite != 1) ";
    $whereArl181y360 .= "AND (inc_estado_tramite != 1) ";
    $whereArlSup360 .= "AND (inc_estado_tramite != 1) ";

    $sumTipoValor = "SUM(IF(inc_estado_tramite=5, inc_valor, 0)) + 
    SUM(IF(inc_estado_tramite=2 OR inc_estado_tramite=3 OR inc_estado_tramite=4 OR inc_estado_tramite=6 OR inc_estado_tramite=7, inc_valor_pagado_eps, 0))";
}

$clienteId = 0;
if ($_SESSION['cliente_id'] != 0) {
    $whereEps .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArl .= "AND inc_empresa = ".$_SESSION['cliente_id'];

    $where0y30 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $where31y60 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $where61y90 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $where91y180 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $where181y360 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereSup360 .= "AND inc_empresa = ".$_SESSION['cliente_id'];

    $whereArl0y30 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArl31y60 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArl61y90 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArl91y180 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArl181y360 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArlSup360 .= "AND inc_empresa = ".$_SESSION['cliente_id'];

    $clienteId = $_SESSION['cliente_id'];
}

$eps = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre", $tablaEps, $whereEps, $groupEps, $orderByEps);
$arl = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre", $tablaArl, $whereArl, $groupArl, $orderByArl);

$datos0y30 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $where0y30, $groupEps, $orderByEps);
$datos31y60 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $where31y60, $groupEps, $orderByEps);
$datos61y90 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $where61y90, $groupEps, $orderByEps);
$datos91y180 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $where91y180, $groupEps, $orderByEps);
$datos181y360 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $where181y360, $groupEps, $orderByEps);
$datosSup360 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $whereSup360, $groupEps, $orderByEps);

$datosArl0y30 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArl0y30, $groupArl, $orderByArl);
$datosArl31y60 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArl31y60, $groupArl, $orderByArl);
$datosArl61y90 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArl61y90, $groupArl, $orderByArl);
$datosArl91y180 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArl91y180, $groupArl, $orderByArl);
$datosArl181y360 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArl181y360, $groupArl, $orderByArl);
$datosArlSup360 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArlSup360, $groupArl, $orderByArl);

$cantTotal0y30 = 0;
$cantTotal31y60 = 0;
$cantTotal61y90 = 0;
$cantTotal91y180 = 0;
$cantTotal181y360 = 0;
$cantTotalSup360 = 0;

$valorTotal0y30 = 0;
$valorTotal31y60 = 0;
$valorTotal61y90 = 0;
$valorTotal91y180 = 0;
$valorTotal181y360 = 0;
$valorTotalSup360 = 0;

$valorTotalCant = 0;
$valorTotalSaldo = 0;

$report = array();
$row = array();

$j = 0;
foreach ($eps as $key => $value) {

    $val0y30 = 0; $val31y60 = 0; $val61y90 = 0; $val91y180 = 0; $val181y360 = 0; $valSup360 = 0;
    $cant0y30 = 0; $cant31y60 = 0; $cant61y90 = 0; $cant91y180 = 0; $cant181y360 = 0; $cantSup360 = 0;

    $row["entidad"] = $value["eps_nombre"];

    if (!empty($datos0y30)) {
        foreach ($datos0y30 as $data0) {
            if ($value["eps_nombre"] == $data0["eps_nombre"]) {
                $val0y30 = $data0["total"];
                $cant0y30 = $data0["cant"];
                $row["c0y30"] = $cant0y30;
                $row["s0y30"] = $val0y30;
                break;
            } else {
                $row["c0y30"] = 0;
                $row["s0y30"] = 0;
            }
        }
    } else {
        $row["c0y30"] = 0;
        $row["s0y30"] = 0;
    }

    if (!empty($datos31y60)) {
        foreach ($datos31y60 as $data) {
            if ($value["eps_nombre"] == $data["eps_nombre"]) {
                $val31y60 = $data["total"];
                $cant31y60 = $data["cant"];
                $row["c31y60"] = $cant31y60;
                $row["s31y60"] = $val31y60;
                break;
            } else {
                $row["c31y60"] = 0;
                $row["s31y60"] = 0;
            }
        }
    } else {
        $row["c31y60"] = 0;
        $row["s31y60"] = 0;
    }

    if (!empty($datos61y90)) {
        foreach ($datos61y90 as $data) {
            if ($value["eps_nombre"] == $data["eps_nombre"]) {
                $val61y90 = $data["total"];
                $cant61y90 = $data["cant"];
                $row["c61y90"] = $cant61y90;
                $row["s61y90"] = $val61y90;
                break;
            } else {
                $row["c61y90"] = 0;
                $row["s61y90"] = 0;
            }
        }
    } else {
        $row["c61y90"] = 0;
        $row["s61y90"] = 0;
    }

    if (!empty($datos91y180)) {
        foreach ($datos91y180 as $data2) {
            if ($value["eps_nombre"] == $data2["eps_nombre"]) {
                $val91y180 = $data2["total"];
                $cant91y180 = $data2["cant"];
                $row["c91y180"] = $cant91y180;
                $row["s91y180"] = $val91y180;
                break;
            } else {
                $row["c91y180"] = 0;
                $row["s91y180"] = 0;
            }
        }
    } else {
        $row["c91y180"] = 0;
        $row["s91y180"] = 0;
    }
    
    if (!empty($datos181y360)) {
        foreach ($datos181y360 as $data3) {
            if ($value["eps_nombre"] == $data3["eps_nombre"]) {
                $val181y360 = $data3["total"];
                $cant181y360 = $data3["cant"];
                $row["c181y360"] = $cant181y360;
                $row["s181y360"] = $val181y360;
                break;
            } else {
                $row["c181y360"] = 0;
                $row["s181y360"] = 0;
            }
        }
    } else {
        $row["c181y360"] = 0;
        $row["s181y360"] = 0;
    }

    if (!empty($datosSup360)) {
        foreach ($datosSup360 as $data4) {
            if ($value["eps_nombre"] == $data4["eps_nombre"]) {
                $valSup360 = $data4["total"];
                $cantSup360 = $data4["cant"];
                $row["cSup360"] = $cantSup360;
                $row["sSup360"] = $valSup360;
                break;
            } else {
                $row["cSup360"] = 0;
                $row["sSup360"] = 0;
            }
        }
    } else {
        $row["cSup360"] = 0;
        $row["sSup360"] = 0; 
    }

    $cantTotal0y30+=$cant0y30;
    $cantTotal31y60+=$cant31y60;
    $cantTotal61y90+=$cant61y90;
    $cantTotal91y180+=$cant91y180;
    $cantTotal181y360+=$cant181y360;
    $cantTotalSup360+=$cantSup360;

    $valorTotal0y30+=$val0y30;
    $valorTotal31y60+=$val31y60;
    $valorTotal61y90+=$val61y90;
    $valorTotal91y180+=$val91y180;
    $valorTotal181y360+=$val181y360;
    $valorTotalSup360+=$valSup360;

    $cantTotal = $cant0y30 + $cant31y60 + $cant61y90 + $cant91y180 + $cant181y360 + $cantSup360;
    $valorTotal = $val0y30 + $val31y60 + $val61y90 + $val91y180 + $val181y360 + $valSup360;

    $row["ctotal"] = $cantTotal;
    $row["stotal"] = $valorTotal;

    $valorTotalCant+=$cantTotal;
    $valorTotalSaldo+=$valorTotal;
    
    $report[$j] = $row;

    $j++;
}

foreach ($arl as $key => $value) {
    $val0y30 = 0; $val31y60 = 0; $val61y90 = 0; $val91y180 = 0; $val181y360 = 0; $valSup360 = 0;
    $cant0y30 = 0; $cant31y60 = 0; $cant61y90 = 0; $cant91y180 = 0; $cant181y360 = 0; $cantSup360 = 0;

    $row["entidad"] = $value["arl_nombre"];

    if (!empty($datosArl0y30)) {
        foreach ($datosArl0y30 as $data) {
            if ($value["arl_nombre"] == $data["arl_nombre"]) {
                $val0y30 = $data["total"];
                $cant0y30 = $data["cant"];
                $row["c0y30"] = $cant0y30;
                $row["s0y30"] = $val0y30;
                break;
            } else {
                $row["c0y30"] = 0;
                $row["s0y30"] = 0;
            }
        }
    } else {
        $row["c0y30"] = 0;
        $row["s0y30"] = 0;
    }

    if (!empty($datosArl31y60)) {
        foreach ($datosArl31y60 as $data) {
            if ($value["arl_nombre"] == $data["arl_nombre"]) {
                $val31y60 = $data["total"];
                $cant31y60 = $data["cant"];
                $row["c31y60"] = $cant31y60;
                $row["s31y60"] = $val31y60;
                break;
            } else {
                $row["c31y60"] = 0;
                $row["s31y60"] = 0;
            }
        }
    } else {
        $row["c31y60"] = 0;
        $row["s31y60"] = 0;
    }

    if (!empty($datosArl61y90)) {
        foreach ($datosArl61y90 as $data) {
            if ($value["arl_nombre"] == $data["arl_nombre"]) {
                $val61y90 = $data["total"];
                $cant61y90 = $data["cant"];
                $row["c61y90"] = $cant61y90;
                $row["s61y90"] = $val61y90;
                break;
            } else {
                $row["c61y90"] = 0;
                $row["s61y90"] = 0;
            }
        }
    } else {
        $row["c61y90"] = 0;
        $row["s61y90"] = 0;
    }

    if (!empty($datosArl91y180)) {
        foreach ($datosArl91y180 as $data) {
            if ($value["arl_nombre"] == $data["arl_nombre"]) {
                $val91y180 = $data["total"];
                $cant91y180 = $data["cant"];
                $row["c91y180"] = $cant91y180;
                $row["s91y180"] = $val91y180;
                break;
            } else {
                $row["c91y180"] = 0;
                $row["s91y180"] = 0;
            }
        }
    } else {
        $row["c91y180"] = 0;
        $row["s91y180"] = 0;   
    }

    if (!empty($datosArl181y360)) {
        foreach ($datosArl181y360 as $data) {
            if ($value["arl_nombre"] == $data["arl_nombre"]) {
                $val181y360 = $data["total"];
                $cant181y360 = $data["cant"];
                $row["c181y360"] = $cant181y360;
                $row["s181y360"] = $val181y360;
                break;
            } else {
                $row["c181y360"] = 0;
                $row["s181y360"] = 0;
            }
        }
    } else {
        $row["c181y360"] = 0;
        $row["s181y360"] = 0;   
    }

    if (!empty($datosArlSup360)) {
        foreach ($datosArlSup360 as $data) {
            if ($value["arl_nombre"] == $data["arl_nombre"]) {
                $valSup360 = $data["total"];
                $cantSup360 = $data["cant"];
                $row["cSup360"] = $cantSup360;
                $row["sSup360"] = $valSup360;
                break;
            } else {
                $row["cSup360"] = 0;
                $row["sSup360"] = 0;
            }
        }
    } else {
        $row["cSup360"] = 0;
        $row["sSup360"] = 0;   
    }
    
    $cantTotal0y30+=$cant0y30;
    $cantTotal31y60+=$cant31y60;
    $cantTotal61y90+=$cant61y90;
    $cantTotal91y180+=$cant91y180;
    $cantTotal181y360+=$cant181y360;
    $cantTotalSup360+=$cantSup360;

    $valorTotal0y30+=$val0y30;
    $valorTotal31y60+=$val31y60;
    $valorTotal61y90+=$val61y90;
    $valorTotal91y180+=$val91y180;
    $valorTotal181y360+=$val181y360;
    $valorTotalSup360+=$valSup360;

    $cantTotal = $cant0y30 + $cant31y60 + $cant61y90 + $cant91y180 + $cant181y360 + $cantSup360;
    $valorTotal = $val0y30 + $val31y60 + $val61y90 + $val91y180 + $val181y360 + $valSup360;
    
    $row["ctotal"] = $cantTotal;
    $row["stotal"] = $valorTotal;

    $valorTotalCant+=$cantTotal;
    $valorTotalSaldo+=$valorTotal;
    $report[$j] = $row;

    $j++;
}

array_multisort(array_column($report, 'stotal'), SORT_DESC, $report);
?>

<div class="row">
    <div class="col-12">
        <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    INFORME DE SALDOS POR ENTIDAD
                </h5>
            </div>
            <div class="card-body p-0 mt-3">
                <div class="d-flex justify-content-around mx-0">
                    <div class="text-center p-0 m-0">
                        <strong>Edad Inc Rango</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td><strong>Nombre Entidades</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($report as $value) {
                                    echo "<tr>";
                                    echo "<td>".$value["entidad"]."</td>";
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>TOTAL</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-center p-0 m-0">
                        <strong>1) 0 y 30 Días</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td><strong>#</strong></td>
                                    <td><strong>$</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            foreach ($report as $value) {
                            echo "<tr>";
                                echo "<td>".$value["c0y30"]."</td>";
                                echo "<td>".number_format($value["s0y30"], 0, ',', '.')."</td>";
                            echo "</tr>";
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php 
                            echo "<tr>";
                                echo "<th>".$cantTotal0y30."</th>";
                                echo "<th>".number_format($valorTotal0y30, 0, ',', '.')."</th>";
                            echo "</tr>";
                            ?>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-center p-0 m-0">
                        <strong>1) 31 y 60 Días</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td><strong>#</strong></td>
                                    <td><strong>$</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            foreach ($report as $value) {
                            echo "<tr>";
                                echo "<td>".$value["c31y60"]."</td>";
                                echo "<td>".number_format($value["s31y60"], 0, ',', '.')."</td>";
                            echo "</tr>";
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php 
                            echo "<tr>";
                                echo "<th>".$cantTotal31y60."</th>";
                                echo "<th>".number_format($valorTotal31y60, 0, ',', '.')."</th>";
                            echo "</tr>";
                            ?>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-center p-0 m-0">
                        <strong>1) 61 y 90 Días</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td><strong>#</strong></td>
                                    <td><strong>$</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            foreach ($report as $value) {
                            echo "<tr>";
                                echo "<td>".$value["c61y90"]."</td>";
                                echo "<td>".number_format($value["s61y90"], 0, ',', '.')."</td>";
                            echo "</tr>";
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php 
                            echo "<tr>";
                                echo "<th>".$cantTotal61y90."</th>";
                                echo "<th>".number_format($valorTotal61y90, 0, ',', '.')."</th>";
                            echo "</tr>";
                            ?>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-center p-0 m-0">
                        <strong>2) 91 y 180 Días</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td><strong>#</strong></td>
                                    <td><strong>$</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            foreach ($report as $value) {
                            echo "<tr>";
                                echo "<td>".$value["c91y180"]."</td>";
                                echo "<td>".number_format($value["s91y180"], 0, ',', '.')."</td>";
                            echo "</tr>";
                            }
                            ?>
                            </tbody>
                            <tfoot>
                             <?php 
                            echo "<tr>";
                                echo "<th>".$cantTotal91y180."</th>";
                                echo "<th>".number_format($valorTotal91y180, 0, ',', '.')."</th>";
                            echo "</tr>";
                            ?>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-center p-0 m-0">
                        <strong>3) 181 y 360 Días</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td><strong>#</strong></td>
                                    <td><strong>$</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            foreach ($report as $value) {
                            echo "<tr>";
                                echo "<td>".$value["c181y360"]."</td>";
                                echo "<td>".number_format($value["s181y360"], 0, ',', '.')."</td>";
                            echo "</tr>";
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php 
                            echo "<tr>";
                                echo "<th>".$cantTotal181y360."</th>";
                                echo "<th>".number_format($valorTotal181y360, 0, ',', '.')."</th>";
                            echo "</tr>";
                            ?>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-center p-0 m-0">
                        <strong>4) Sup a 360 Días</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td><strong>#</strong></td>
                                    <td><strong>$</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            foreach ($report as $value) {
                            echo "<tr>";
                                echo "<td>".$value["cSup360"]."</td>";
                                echo "<td>".number_format($value["sSup360"], 0, ',', '.')."</td>";
                            echo "</tr>";
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php 
                            echo "<tr>";
                                echo "<th>".$cantTotalSup360."</th>";
                                echo "<th>".number_format($valorTotalSup360, 0, ',', '.')."</th>";
                            echo "</tr>";
                            ?>
                            </tfoot>
                        </table>
                    </div>
                    <div class="text-center p-0 m-0">
                        <strong>Total</strong>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td><strong>#</strong></td>
                                    <td><strong>$</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            foreach ($report as $value) {
                            echo "<tr>";
                                echo "<th>".$value["ctotal"]."</th>";
                                echo "<th>".number_format($value["stotal"], 0, ',', '.')."</th>";
                            echo "</tr>";
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            <?php 
                            echo "<tr>";
                                echo "<th>".$valorTotalCant."</th>";
                                echo "<th>".number_format($valorTotalSaldo, 0, ',', '.')."</th>";
                            echo "</tr>";
                            ?>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>