<?php
    session_start();
    $_SESSION['start'] = time();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
    require_once '../controladores/incapacidades.controlador.php';
    require_once '../controladores/empleados.controlador.php';
    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
    require_once '../modelos/empleados.modelo.php';
    require_once '../modelos/usuarios.modelo.php';
 
 
    if(isset($_POST['contados']) && $_POST['contados'] != 0 ){
        $noexisten = 0;
        $listo = 0;
        $contados = ($_POST['contados'] +1);
        for ($i=0; $i < $contados; $i++) { 
            if(isset($_POST['Editarcedula_'.$i]) && $_POST['Editarcedula_'.$i] != '' ){
                /* Viene la cedula vamos apreguntar por la incapacidad a ver */
                $item1 = "inc_cc_afiliado";
                $valor1 = $_POST['Editarcedula_'.$i];
                $item2 = 'inc_fecha_inicio';
                $valor2 = $_POST['EditarFechaInicio_'.$i];
                $existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2, $_SESSION['cliente_id']);
                if($existeInc){
 
                    $existeIncw = ModeloIncapacidades::mdlVerIdIncapacidades($item1, $valor1, $item2, $valor2);
 
                    if(isset($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name']) && !empty($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name'])){
                        /* Vamos a cargar la primera imagen */
 
                        list($ancho, $alto) = getimagesize($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name']);
                        $nuevoAncho = 500;
                        $nuevoAlto  = 500;
                        $ruta = '';
 
                        /* Creamos el directorio */
                        $directorio = __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/";
                        if (!is_dir($directorio)) {
                            mkdir($directorio, 0755, true);
                        }
 
 
                        /* De acuerdo al tipo de imagen se le aplican funciones de php */
                        if($_FILES["EditarFilePrimerImagen_".$i]["type"] == "image/jpeg"){
                            //creamos un numero aleatorio
                            $aleatorio = mt_rand(1000, 9999);
                            //creamos la ruta donde se va a guardar la imagen
                            $ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".jpg";
                            $ruta2 = "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".jpg";
                            //obtenemos el origen osea el FILE
                            $origen  = imagecreatefromjpeg($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name']);
                            //le decimos que vamos acrear una imagen en el destino con esos ancho y alto
                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                     
                             
                            imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                            imagejpeg($destino, $ruta);
 
                        }elseif($_FILES["EditarFilePrimerImagen_".$i]["type"] == "image/png"){
                            //creamos un numero aleatorio
                            $aleatorio = mt_rand(1000, 9999);
                            //creamos la ruta donde se va a guardar la imagen
                            $ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".png";
                            $ruta2 =  "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".png";
                            //obtenemos el origen osea el FILE
                            $origen  = imagecreatefrompng($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name']);
                            //le decimos que vamos acrear una imagen en el destino con esos ancho y alto
                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                            //cortamos esta imagen
                            
                            imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                            imagepng($destino, $ruta);
 
                        }else{
                            //creamos un numero aleatorio
                            $aleatorio = mt_rand(100, 999);
                            //creamos la ruta donde se va a guardar la imagen
                            $ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".pdf";
                            $ruta2 =  "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".pdf";
                            /* Creamos el directorio */
                            $directorio = __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/";
                            if (!is_dir($directorio)) {
                                mkdir($directorio, 0755, true);
                            }
 
                            copy($_FILES['EditarFilePrimerImagen_'.$i]['tmp_name'], $ruta);
 
                        }
 
                        $item1 = "inc_id";
                        $valor1 = $existeIncw['inc_id'];
                        $item2 = "inc_ruta_incapacidad";
                        $valor2 = $ruta2;
                        $ultimoLogin = ModeloUsuarios::mdlActualizarUsuario('gi_incapacidad', $item2, $valor2, $item1, $valor1);
                        if($ultimoLogin == "ok"){
                            $listo++;
                        }
 
                    }
 
 
                    if(isset($_FILES['EditarSegundaImagen_'.$i]['tmp_name']) && !empty($_FILES['EditarSegundaImagen_'.$i]['tmp_name'])){
                        /* Vamos a cargar la primera imagen */
 
                        list($ancho, $alto) = getimagesize($_FILES['EditarSegundaImagen_'.$i]['tmp_name']);
                        $nuevoAncho = 500;
                        $nuevoAlto  = 500;
                        $ruta = '';
 
                        /* Creamos el directorio */
                        $directorio = __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/";
                        if (!is_dir($directorio)) {
                            mkdir($directorio, 0755, true);
                        }
 
 
                        /* De acuerdo al tipo de imagen se le aplican funciones de php */
                        if($_FILES["EditarSegundaImagen_".$i]["type"] == "image/jpeg"){
                            //creamos un numero aleatorio
                            $aleatorio = mt_rand(1000, 9999);
                            //creamos la ruta donde se va a guardar la imagen
                            $ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".jpg";
                            $ruta2 =  "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".jpg";
                            //obtenemos el origen osea el FILE
                            $origen  = imagecreatefromjpeg($_FILES['EditarSegundaImagen_'.$i]['tmp_name']);
                            //le decimos que vamos acrear una imagen en el destino con esos ancho y alto
                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                     
                             
                            imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                            imagejpeg($destino, $ruta);
 
                        }elseif($_FILES["EditarSegundaImagen_".$i]["type"] == "image/png"){
                            //creamos un numero aleatorio
                            $aleatorio = mt_rand(1000, 9999);
                            //creamos la ruta donde se va a guardar la imagen
                            $ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".png";
                            $ruta2 =  "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".png";
                            //obtenemos el origen osea el FILE
                            $origen  = imagecreatefrompng($_FILES['EditarSegundaImagen_'.$i]['tmp_name']);
                            //le decimos que vamos acrear una imagen en el destino con esos ancho y alto
                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                            //cortamos esta imagen
                            
                            imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                            imagepng($destino, $ruta);
 
                        }else{
                            //creamos un numero aleatorio
                            $aleatorio = mt_rand(100, 999);
                            //creamos la ruta donde se va a guardar la imagen
                            $ruta =  __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".pdf";
                            $ruta2 =  "vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/".$aleatorio.".pdf";
                            /* Creamos el directorio */
                            $directorio = __DIR__."/../vistas/img/empleados/".$_POST['Editarcedula_'.$i]."/";
                            if (!is_dir($directorio)) {
                                mkdir($directorio, 0755, true);
                            }

                            copy($_FILES['EditarSegundaImagen_'.$i]['tmp_name'], $ruta);
                        
                        }
 
                        $item1 = "inc_id";
                        $valor1 = $existeIncw['inc_id'];
                        $item2 = "inc_ruta_incapacidad_transcrita";
                        $valor2 = $ruta2;
                        $ultimoLogin = ModeloUsuarios::mdlActualizarUsuario('gi_incapacidad', $item2, $valor2, $item1, $valor1);
                        if($ultimoLogin == "ok"){
                            $listo++;
                        }
 
                    }
                }else{
                    $noexisten;
                }
            }
        }

        $valorFinal = $contados - $noexisten - 1 ;
        echo json_encode(array('total' => $contados-1 , 'fallas' => $noexisten, 'exito'=> $valorFinal));
    }