<?php
	ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    session_start();
    $_SESSION['start'] = time();

    require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/incapacidades.controlador.php';
	require_once '../controladores/empleados.controlador.php';
    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
	require_once '../modelos/empleados.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';
    require_once '../extenciones/Excel.php';

    function validateDate($date, $format = 'Y-m-d')
    {   
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

	if(isset($_FILES['NuevoIncapacidad']['tmp_name']) && !empty($_FILES['NuevoIncapacidad']['tmp_name']) ){
		$name   = $_FILES['NuevoIncapacidad']['name'];
		$tname  = $_FILES['NuevoIncapacidad']['tmp_name'];
		ini_set('memory_limit','1028M');
        $obj_excel = PHPExcel_IOFactory::load($tname);
        $sheetData = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
        $arr_datos = array();
        $highestColumm = $obj_excel->setActiveSheetIndex(0)->getHighestColumn(); // e.g. "EL"
        $highestRow = $obj_excel->setActiveSheetIndex(0)->getHighestRow();
        $aciertos = 0;
        $fallos   = 0;
        $total = 0;
        $existentes = 0;
        $datoCsv = array();
        $noexiste = 0;
        $incapacidadesNoExistentes = 0;
        $vacios = 0;
		
        foreach ($sheetData as $index => $value) {
            if ( $index > 1 ){
                $valido = 0;
                $existe = false;
                $separador = '';
                $total++;
                $empresa = 0;
                if((!is_null($value['A']) && !empty($value['A'])) && 
                        (!is_null($value['B']) && !empty($value['B']))
                    ){
                    /* Iniciamos la consulta Sql */
                    $datos = array();

                    /* Identificacion */
                    if(!is_null($value['A']) && !empty($value['A']) ){
                        $valido = 1;
                        $datos['inc_cc_afiliado'] = $value['A'];    

                        $item = 'emd_cedula';
                        $valuess = $value['A'];
                        $tabla = 'gi_empleados';
                        $value2 = $_SESSION['cliente_id'];
                       
                        $res = ModeloEmpleados::mdlMostrarEmpleadosX2($tabla, $item, $valuess, $value2);
                        if($res != false){
                            if($res['emd_cedula'] == $value['A']){
                                /*Empleado Existente*/
                                $existe = true;
                                $datos['inc_emd_id'] = $res['emd_id'];
                                $datos['inc_empresa'] = $res['emd_emp_id'];
                            }
                        } else{
                            /* No existe */
                            $existe = false;
                            $datx  = array(   
                                        'filas_fallo_fila' => $index,
                                        'fila_fallo_mensaje' => 'Esta cedula => '.$value['A'].' no esta registrada en la base de datos',
                                        'fila_fallo_session_id' => $_SESSION['idSession'],
                                        'fila_fallo_cedula' => $value['A'],
                                    );

                            $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datx);

                            $noexiste++;
                        }
                    }else{
                        $datos['inc_cc_afiliado'] = NULL;
                    }

                    if($existe == true){
                        //Preguntamos si la incapacidad existe
                        $item1 = "inc_cc_afiliado";
                        $valor1 = $value['A'];
                        $item2 = 'inc_fecha_inicio';
                        $valor2 = $value['B'];
                        $existeInc = ModeloIncapacidades::mdlValidarIncapacidades($item1, $valor1, $item2, $valor2, $_SESSION['cliente_id']);
                        $incapacidaDato = null;
                        if($existeInc){
                            //Traemos los datos de la incapacidad, porque ya validamos que esta existe
                            $strCampos = "*";
                            $strTabla_ = "gi_incapacidad";
                            $strWhere_ = "inc_cc_afiliado = '".$value['A']."' AND inc_fecha_inicio = '".$value['B']."'";
                            $incapacidaDato = ModeloTesoreria::mdlMostrarUnitario($strCampos, $strTabla_, $strWhere_);
                            $datos["inc_id"] = $incapacidaDato['inc_id'];
                        }

                        /* FECHA CONcILIACION */
                        if(!is_null($value['C']) && !empty($value['C']) ){
                            $valido = 1;
                            $datos['inc_fecha_consiliacion_d'] = $value['C'];   
                        }else{
                            $datos['inc_fecha_consiliacion_d'] = NULL;    
                        }

                        /* F. FACTURACIÓN */
                        if(!is_null($value['D']) && !empty($value['D']) ){
                            $datos['inc_fecha_fecturacion_d'] = $value['D'];
                            $valido = 1;
                        }else{
                            $datos['inc_fecha_fecturacion_d'] = NULL;    
                        }

                        /* COMISIÓN */
                        if(!is_null($value['E']) && !empty($value['E']) ){
                            $datos['inc_condicion_id_i'] = $value['E'];
                            $valido = 1;
                        }else{
                            $datos['inc_condicion_id_i'] = NULL;    
                        }

                        /* # FACTURA */
                        if(!is_null($value['F']) && !empty($value['F']) ){
                            $valido = 1;
                            $datos['inc_numero_factura'] = $value['F'];    
                        }else{
                            $datos['inc_numero_factura'] = NULL;    
                        }

                        /* #FECHA PAGO FACTURA */
                        if(!is_null($value['G']) && !empty($value['G']) ){
                            $datos['inc_fecha_pago_factura'] = $value['G'];
                            $valido = 1;
                        }else{
                            $datos['inc_fecha_pago_factura'] = NULL;    
                        }

                        
        
                        if($valido == 1){
                            if($existeInc){
                                $campos = " inc_fecha_edicion = '".date('Y-m-d H:i:s')."' ";
                                if(!empty($datos['inc_fecha_pago_factura'])){
                                    $campos .= " , inc_fecha_pago_factura = '".$datos['inc_fecha_pago_factura']."' ";
                                }
                                if(!empty($datos['inc_numero_factura'])){
                                    $campos .= " , inc_numero_factura = '".$datos['inc_numero_factura']."' ";
                                }
                                if(!empty($datos['inc_condicion_id_i'])){
                                    $campos .= " , inc_condicion_id_i = '".$datos['inc_condicion_id_i']."' ";
                                }
                                if(!empty($datos['inc_fecha_fecturacion_d'])){
                                    $campos .= " , inc_fecha_fecturacion_d = '".$datos['inc_fecha_fecturacion_d']."' ";
                                }
                                if(!empty($datos['inc_fecha_consiliacion_d'])){
                                    $campos .= " , inc_fecha_consiliacion_d = '".$datos['inc_fecha_consiliacion_d']."' ";
                                }if(!empty($datos['inc_fecha_pago_factura'])){
                                    $campos .= " , inc_fecha_pago_factura = '".$datos['inc_fecha_pago_factura']."' ";
                                }

                                $where_ = "inc_id = ".$datos["inc_id"] ;
                                $tabla = "gi_incapacidad";  
                                $respuesta = ModeloIncapacidades::mdlEditar($tabla, $campos, $where_);
                                if($respuesta == 'ok'){
                                    $aciertos++;
                                }else{
                                    $fallos++;
                                    print_r($respuesta);
                                    $datos  = array(   
                                        'filas_fallo_fila' => $index,
                                        'fila_fallo_mensaje' => 'Cedula '.$valor1.' y fecha inicio '.$valor2.' es de una incapacidad ya guardada',
                                        'fila_fallo_cedula' => $valor1,
                                        'fila_fallo_session_id' => $_SESSION['idSession']
                                    );

                                    $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);
                                }
                                      
                            }else{
                                $incapacidadesNoExistentes++;

                                $datos  = array(   
                                    'filas_fallo_fila' => $index,
                                    'fila_fallo_mensaje' => 'Cedula '.$valor1.' y fecha inicio '.$valor2.' es de una incapacidad que no existe',
                                    'fila_fallo_cedula' => $valor1,
                                    'fila_fallo_session_id' => $_SESSION['idSession']
                                );

                                $resultado = ModeloTesoreria::mdlCrearFallas('gi_filas_fallo' , $datos);
                            }
                        }
                    }
                }else{
                    $vacios++;
                    if($vacios> 2){
                        break;
                    }
                }
        
            }
        }


        $check = 0;

        if(isset($_POST['chekUpdate']) && $_POST['chekUpdate'] != 0){
        	$check = 1;
        }
        ControladorPlantilla::setAuditoria('ControladorIncapacidades', 'SoloCargarFacturacionIncapacidades', 'Archivo Excel', json_encode(array('total' => $total , 'exito' => $aciertos , 'fallaron' => $fallos, 'TotalNoExistente' => $incapacidadesNoExistentes, 'check' => $check)));

        echo json_encode(array('total' => $total , 'exito' => $aciertos , 'fallaron' => $fallos, 'TotalNoExistente' => $incapacidadesNoExistentes, 'check' => $check));
        
	}
?>