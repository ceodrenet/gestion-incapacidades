<?php
  session_start();
  $_SESSION['start'] = time();
  ini_set('display_errors', 'On');
  ini_set('display_errors', 1);

  require_once '../controladores/mail.controlador.php';
  require_once '../controladores/plantilla.controlador.php';
  require_once '../controladores/incapacidades.controlador.php';
  require_once '../modelos/dao.modelo.php';
  require_once '../modelos/incapacidades.modelo.php';
  
  /**
  * Enviar Correo de Notificación
  */

  if(isset($_POST['sendMailIncapacidades']) && $_POST['sendMailIncapacidades'] != ''){
      /*Primeor obtenemos la incapacidad*/
      ControladorIncapacidades::sendCorreoIncompleto($_POST['sendMailIncapacidades']);
      echo json_encode(array('code' => 1, 'message' => 'Mensaje Enviado'));
  }