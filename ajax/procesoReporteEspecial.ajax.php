<?php
    session_start();
    $_SESSION['start'] = time();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
    require_once '../controladores/incapacidades.controlador.php';
    require_once '../controladores/clientes.controlador.php';
    
    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
    require_once '../modelos/clientes.modelo.php';


    if(isset($_POST['fechaInicial']) && isset($_POST['fechaFinal'])){
        require_once __DIR__.'/../extenciones/TCPDF/tcpdf.php';

        $fechaBusqueda = "inc_fecha_pago_nomina";
        if(isset($_POST['fechaBusqueda'])){
            $fechaBusqueda = $_POST['fechaBusqueda'];
        }

        $array = array(
            'Enero'     => '01',
            'Febrero'   => '02',
            'Marzo'     => '03',
            'Abril'     => '04',
            'Mayo'      => '05',
            'Junio'     => '06',
            'Julio'     => '07',
            'Agosto'    => '08',
            'Septiembre' => '09',
            'Octubre'   => '10',
            'Noviembre' => '11',
            'Diciembre' => '12'
        );

        $arrayMonths = array(
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'  
        );


        class MYPDF extends TCPDF {

            protected $processId = 0;
            protected $header = '';
            protected $footer = '';
            static $errorMsg = '';
            //Page header
            public function Header() {
                $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
               $this->SetLineStyle( array( 'width' => 0.40, 'color' => array(0, 0, 0)));

               $this->Line(5, 5, $this->getPageWidth()-5, 5); 

               $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
               $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
               $this->Line(5, 5, 5, $this->getPageHeight()-5);

            }

            // Page footer
            public function Footer() {
                // Position at 15 mm from bottom
                $this->SetY(-15);
                // Set font
                $this->SetFont('helvetica', 'N', 8);
                // Page number
                $this->Cell(0, 10, 'www.incapacidades.co', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            }
        }


        /*empezar el proceso del PDF*/
        $obj_pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $obj_pdf->SetCreator(PDF_CREATOR);
        $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
        $obj_pdf->setPrintHeader(true);
        $obj_pdf->setPrintFooter(true);
        $obj_pdf->SetAutoPageBreak(TRUE, 20);
        $obj_pdf->SetFont('times', '', 15);
        $empres = '';
        $nit = '';
        
        $item  = 'emp_id';
        $valor = $_SESSION['cliente_id']; 
        $empresa = ControladorClientes::ctrMostrarClientes($item, $valor);
       
        $arrayMesesCompleto = array();
        $arrayMesesPagos = array();
        $arrayMesesPagos['_MoneyPagadas0_90'] = 0;
        $arrayMesesPagos['_MoneyPagadas91_120'] = 0;
        $arrayMesesPagos['_MoneyPagadas121_150'] = 0;
        $arrayMesesPagos['_MoneyPagadas151_180'] = 0;
        $arrayMesesPagos['_MoneyPagadas181_210'] = 0;
        $arrayMesesPagos['_MoneyPagadas210'] = 0;
        $num = 0;
        for($i = $array[$_POST['fechaInicial']]; $i <= $array[$_POST['fechaFinal']]; $i++){
            $j = $i;
            if($i < 10 && strlen($i) < 2){
                $j = '0'.$j;
            }
            $Inicio = $_POST['NuevoFechaInicioYear']."-".$j.'-28';
            $final = $_POST['NuevoFechaInicioYear']."-".$j.'-30';
            $arrayMesesCompleto[$num]['mes'] = $arrayMonths[$j];
            $obj_pdf->AddPage(); 
            $condicionX = '$fechaBusqueda BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'].' AND inc_estado != 0 ';

            $recibidas = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionX.' AND inc_estado_tramite != \'1\'');
            $pagadas = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'5\'');
            $negadas = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'6\'');
            $restante = $recibidas - $pagadas - $negadas;
            

            /*VALORES*/
            $_MoneyRecibidas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite != \'1\'');
            $_MoneyPagadas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'5\'');
            $_MoneyNegadas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'6\'');
            $_MoneyRestante = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite NOT IN (\'6\',\'1\',\'5\') ');
            $_PorPagadas = 0;
            $_PorNegadas = 0;
            $_PorRestant = 0;
            if($_MoneyRecibidas['total'] > 0){
                $_PorPagadas = ($_MoneyPagadas['total'] * 100) / $_MoneyRecibidas['total'];
                $_PorNegadas = ($_MoneyNegadas['total'] * 100) / $_MoneyRecibidas['total'];
                $_PorRestant = ($_MoneyRestante['total'] * 100) / $_MoneyRecibidas['total'];
            }
           

            $arrayMesesCompleto[$num]['recibi'] = $_MoneyRecibidas;
            $arrayMesesCompleto[$num]['pagada'] = $_MoneyPagadas ;
            $arrayMesesCompleto[$num]['negada'] = $_MoneyNegadas;
            $arrayMesesCompleto[$num]['restan'] = $_MoneyRestante['total'];

            $arrayMesesCompleto[$num]['Rrecibi'] = $recibidas;
            $arrayMesesCompleto[$num]['Rpagada'] = $pagadas;
            $arrayMesesCompleto[$num]['Rnegada'] = $negadas;
            $arrayMesesCompleto[$num]['Rrestan'] = $restante;

            $_MoneyPagadas0_90 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'5\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > -100 AND datediff(inc_fecha_pago, inc_fecha_generada ) <= 90');
            $_MoneyPagadas91_120 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'5\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > 90 AND datediff(inc_fecha_pago, inc_fecha_generada ) <= 120');
            $_MoneyPagadas121_150 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'5\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > 120 AND datediff(inc_fecha_pago, inc_fecha_generada ) <= 150');

            $_MoneyPagadas151_180 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'5\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > 151 AND datediff(inc_fecha_pago, inc_fecha_generada ) <= 180');
            $_MoneyPagadas181_210 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'5\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > 181 AND datediff(inc_fecha_pago, inc_fecha_generada ) <= 210');
            $_MoneyPagadas210 = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'5\' AND datediff(inc_fecha_pago, inc_fecha_generada ) > 210');
            

            $arrayMesesPagos['_MoneyPagadas0_90'] += $_MoneyPagadas0_90['total'];
            $arrayMesesPagos['_MoneyPagadas91_120'] += $_MoneyPagadas91_120['total'];
            $arrayMesesPagos['_MoneyPagadas121_150'] += $_MoneyPagadas121_150['total'];
            $arrayMesesPagos['_MoneyPagadas151_180'] += $_MoneyPagadas151_180['total'];
            $arrayMesesPagos['_MoneyPagadas181_210'] += $_MoneyPagadas181_210['total'];
            $arrayMesesPagos['_MoneyPagadas210'] += $_MoneyPagadas210['total'];


            $content = '';
            $content = '
            <br/>
            <br/>
            <table>
                <tr>
                    <td width="80%" >
                        <table style="font-family:tahoma;font-size:12;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                            <tr>
                                <td style="width:20%"></td>
                                <td style="width:60%; text-align:center; font-size:14px;"><b>INFORME DE GESTIÓN</b></td>
                                <td style="width:20%"></td>
                            </tr>
                        </table>
                        <table style="font-family:tahoma;font-size:12;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                            <tr>
                                <td style="width:20%"></td>
                                <td style="width:60%;text-align:center;">'.mb_strtoupper($empresa['emp_nombre']).'</td>
                                <td style="width:20%"></td>
                            </tr>
                        </table>
                    </td>
                    <td width="20%" align="center" valign="bottom">
                        <img src="../vistas/modulos/exportar/LOGO_2.jpg" width="85px" height="40px">
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <table style="font-family:tahoma;font-size:9;" width="10%" cellpadding="2px" cellspacing="2px" width="100%">
                <tr>
                    <td style="width:8%;text-align:justify;"><b>MES:</b></td>
                    <td style="width:50%;text-align:justify;">'.mb_strtoupper($arrayMonths[$j]).' '.$_POST['NuevoFechaInicioYear'].'</td>
                    <td style="width:42%;text-align:right;">Periodo de '.mb_strtoupper($_POST['fechaInicial']).' '.$_POST['NuevoFechaInicioYear'].' a '.mb_strtoupper($_POST['fechaFinal']).' '.$_POST['NuevoFechaInicioYear'].'</td>
                </tr>
            </table>
            <br/>
            <br/>
            <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                <tr>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>RECIBIDAS</b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>PAGADAS</b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>NEGADAS</b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>PENDIENTES</b></td>
                </tr>
                <tr>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">$ '.number_format($_MoneyRecibidas['total'], 0, '.', '.').'</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">$ '.number_format($_MoneyPagadas['total'], 0, '.', '.').'</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">$ '.number_format($_MoneyNegadas['total'], 0, '.', '.').'</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">$ '.number_format($_MoneyRestante['total'], 0, '.', '.').'</td>
                </tr>
                <tr>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>CANTIDAD / % </b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>CANTIDAD / % </b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>CANTIDAD / % </b></td>
                    <td style="width:25%;text-align:center;background-color:#dd4b39;color:white;"><b>CANTIDAD / % </b></td>
                </tr>
                <tr>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">'.$recibidas.' / 100 %</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">'.$pagadas.' / '.number_format($_PorPagadas, 2).' %</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">'.$negadas.' / '.number_format($_PorNegadas,2).' %</td>
                    <td style="width:25%;text-align:center;font-family:tahoma;font-size:9;">'.$restante.' / '.number_format($_PorRestant,2).' %</td>
                </tr>
            </table>';

            $ips = ModeloIncapacidades::mdlMostrarGroupAndOrder('ips_id, ips_nombre','gi_ips JOIN gi_incapacidad ON inc_ips_afiliado = ips_id', '$fechaBusqueda BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'], 'GROUP BY ips_id', 'ORDER BY ips_nombre ASC', null);
            $content .= '
                    <br/>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:100%;text-align:justify;background-color:#dd4b39;color:white;"><b>DETALLE POR ENTIDAD</b></td>
                        </tr>
                    </table>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:35%;text-align:center;"><b>ADMINISTRADORA</b></td>
                            <td style="width:5%;text-align:center;"><b>#</b></td>
                            <td style="width:15%;text-align:center;"><b>RECIBIDAS</b></td>
                            <td style="width:15%;text-align:center;"><b>PAGADAS</b></td>
                            <td style="width:15%;text-align:center;"><b>NEGADAS</b></td>
                            <td style="width:15%;text-align:center;"><b>PENDIENTES</b></td>
                        </tr>';
            $_TotalRecibidas = 0;
            $_TotalPagadas = 0;
            $_TotalNegadas = 0;
            $_TotalRestante = 0;
            $_TotalNum = 0;
            foreach($ips as $key => $ip){

                $_MoneyRecibidas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite != \'1\' AND inc_ips_afiliado = '.$ip['ips_id']);
                $_MoneyPagadas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'5\' AND inc_ips_afiliado = '.$ip['ips_id']);
                $_MoneyNegadas = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.' AND inc_estado_tramite = \'6\' AND inc_ips_afiliado = '.$ip['ips_id']);
                $_MoneyRestante = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionX.'  AND inc_estado_tramite NOT IN (\'6\',\'1\',\'5\') AND inc_ips_afiliado = '.$ip['ips_id']);;

                $_TotalRecibidas += $_MoneyRecibidas['total'];
                $_TotalPagadas += $_MoneyPagadas['total'];
                $_TotalNegadas += $_MoneyNegadas['total'];
                $_TotalRestante += $_MoneyRestante['total'];
                
                $recibidas = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionX.' AND inc_estado_tramite != \'1\' AND inc_ips_afiliado = '.$ip['ips_id']);
                $_TotalNum += $recibidas;
                $content .= '
                    <tr>
                            <td style="width:35%;text-align:justify;"><b>'.mb_strtoupper($ip['ips_nombre']).'</b></td>
                            <td style="width:5%;text-align:center;">'.$recibidas.'</td>
                            <td style="width:15%;text-align:center;">$ '.number_format($_MoneyRecibidas['total'], 0, '.', '.').'</td>
                            <td style="width:15%;text-align:center;">$ '.number_format($_MoneyPagadas['total'], 0, '.', '.').'</td>
                            <td style="width:15%;text-align:center;">$ '.number_format($_MoneyNegadas['total'], 0, '.', '.').'</td>
                            <td style="width:15%;text-align:center;">$ '.number_format($_MoneyRestante['total'], 0, '.', '.').'</td>
                        </tr>';
            }

            $content .= '
                    <tr>
                            <td style="width:35%;text-align:justify;"><b>TOTALES</b></td>
                            <td style="width:5%;text-align:center;"><b>'.$_TotalNum.'</b></td>
                            <td style="width:15%;text-align:center;"><b>$ '.number_format($_TotalRecibidas, 0, '.', '.').'</b></td>
                            <td style="width:15%;text-align:center;"><b>$ '.number_format($_TotalPagadas, 0, '.', '.').'</b></td>
                            <td style="width:15%;text-align:center;"><b>$ '.number_format($_TotalNegadas, 0, '.', '.').'</b></td>
                            <td style="width:15%;text-align:center;"><b>$ '.number_format($_TotalRestante, 0, '.', '.').'</b></td>
                        </tr>';


            $condicionY = '$fechaBusqueda BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'].' AND inc_estado = 0 ';

            $recibidasIn = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionY);
            $_MoneyRecibidasIn = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionY);

            $content .= '</table>
                    <br/>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:100%;text-align:justify;background-color:#dd4b39;color:white;"><b>SOPORTES INCOMPLETOS</b></td>
                        </tr>
                    </table>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:20%;text-align:justify;">
                                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%" >
                                    <tr>
                                        <th align="center"><b>RECIBIDOS / #</b></th>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            $ '.number_format($_MoneyRecibidasIn['total'], 0, '.', '.').' / '.$recibidasIn.'
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:80%;text-align:justify;">
                                <table style="font-family:tahoma;font-size:9;  border:solid 1px #FFF;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                                    <tr>
                                        <td align="justify" width="30%"><b>ADMINISTRADORA</b></td>
                                        <td align="justify" width="20%"><b>VALOR</b></td>
                                        <td align="justify" width="50%"><b>MOTIVO</b></td>
                                    </tr>';
            $motivos = ModeloIncapacidades::mdlMostrarGroupAndOrder('mot_desc_v, count(inc_mot_rech_i) as total, SUM(inc_valor_pagado_eps) as sumX, ips_nombre, ips_id','gi_incapacidad JOIN gi_motivos_rechazo ON inc_mot_rech_i = mot_id_i JOIN gi_ips ON ips_id = inc_ips_afiliado ', '$fechaBusqueda BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'].' AND inc_estado = 0  ', 'GROUP BY inc_mot_rech_i, ips_id ', 'ORDER BY mot_desc_v ASC', null);
            foreach($motivos as $motivo => $mot){
                $content .= '<tr>
                                <td align="justify">'.$mot['ips_nombre'].'</td>
                                <td align="justify">$ '.number_format($mot['sumX'], 0, '.', '.').'</td>
                                <td align="justify">'.$mot['mot_desc_v'].'</td>
        
                            </tr>';
            }
                                    
                    $content .= '            
                                </table>
                            </td>
                        </tr>
                    </table>';

            $condicionZ = '$fechaBusqueda BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'].' AND inc_estado_tramite = \'6\'';

            $recibidasNe = ModeloIncapacidades::mdlGetNumrows_number('gi_incapacidad',  $condicionZ);
            $_MoneyRecibidasNe = ModeloIncapacidades::mdlMostrarUnitario('SUM(inc_valor_pagado_eps) as total', 'gi_incapacidad',  $condicionZ);

            $content .= '
                    <br/>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:100%;text-align:justify;background-color:#dd4b39;color:white;"><b>INCAPACIDADES NEGADAS</b></td>
                        </tr>
                    </table>
                    <br/>
                    <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                        <tr>
                            <td style="width:20%;text-align:justify;">
                                <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%" >
                                    <tr>
                                        <th align="center"><b>RECIBIDOS / # </b></th>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            $ '.number_format($_MoneyRecibidasNe['total'], 0, '.', '.').' / '.$recibidasNe.'
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:80%;text-align:justify;">
                                <table style="font-family:tahoma;font-size:9;  border:solid 1px #FFF;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                                    <tr>
                                        <td align="justify" width="30%"><b>ADMINISTRADORA</b></td>
                                        <td align="justify" width="20%"><b>VALOR</b></td>
                                        <td align="justify" width="50%"><b>MOTIVO</b></td>
                                    </tr>';  

            $negadas = ModeloIncapacidades::mdlMostrarGroupAndOrder('incm_comentario_t, ips_nombre, inc_valor_pagado_eps','gi_incapacidades_comentarios JOIN gi_incapacidad ON inc_id = inm_inc_id_i JOIN gi_ips ON ips_id = inc_ips_afiliado', '$fechaBusqueda BETWEEN \''.$Inicio.'\' AND \''.$final.'\' AND inc_empresa = '.$_SESSION['cliente_id'].' AND inc_estado_tramite = \'6\' ', null, 'ORDER BY ips_nombre ASC', null);
            foreach($negadas as $nega => $neg){
                $valorN = 0;
                if($neg['inc_valor_pagado_eps'] != null){
                    $valorN = number_format($neg['inc_valor_pagado_eps'], 0, '.', '.');
                }
                $content .= '<tr>
                                <td align="justify">'.mb_strtoupper($neg['ips_nombre']).'</td>
                                <td align="justify">$ '.$valorN.'</td>
                                <td align="justify">'.$neg['incm_comentario_t'].'</td>
                            </tr>';
            }
                                    
                    $content .= '            
                                </table>
                            </td>
                        </tr>
                    </table>
                ';        

            $obj_pdf->writeHTML($content, true, false, false, false, '');
            $num++;
        }

        $obj_pdf->AddPage(); 
        $content = '<br/>
            <br/>
            <table>
                <tr>
                    <td width="80%" >
                        <table style="font-family:tahoma;font-size:12;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                            <tr>
                                <td style="width:20%"></td>
                                <td style="width:60%; text-align:center; font-size:14px;"><b>INFORME DE GESTIÓN</b></td>
                                <td style="width:20%"></td>
                            </tr>
                        </table>
                        <table style="font-family:tahoma;font-size:12;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                            <tr>
                                <td style="width:20%"></td>
                                <td style="width:60%;text-align:center;">'.mb_strtoupper($empresa['emp_nombre']).'</td>
                                <td style="width:20%"></td>
                            </tr>
                        </table>
                    </td>
                    <td width="20%" align="center" valign="bottom">
                        <img src="../exportar/LOGO_2.jpg" width="85px" height="40px">
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
            <table style="font-family:tahoma;font-size:9;" width="10%" cellpadding="2px" cellspacing="2px" width="100%">
                <tr>
                    <td style="width:8%;text-align:justify;"></td>
                    <td style="width:50%;text-align:justify;"><b>RESUMEN FINAL</b></td>
                    <td style="width:42%;text-align:right;">Periodo de '.mb_strtoupper($_POST['fechaInicial']).' '.$_POST['NuevoFechaInicioYear'].' a '.mb_strtoupper($_POST['fechaFinal']).' '.$_POST['NuevoFechaInicioYear'].'</td>
                </tr>
            </table>
            <br/>
            <br/>
            <table style="font-family:tahoma;font-size:8;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                <tr>
                    <td style="width:11%;text-align:center;background-color:#dd4b39;color:white;"><b>MES</b></td>
                    <td style="width:12%;text-align:center;background-color:#dd4b39;color:white;"><b>RECIBIDAS</b></td>
                    <td style="width:5%;text-align:center;background-color:#dd4b39;color:white;"><b>#</b></td>
                    <td style="width:12%;text-align:center;background-color:#dd4b39;color:white;"><b>PAGADAS</b></td>
                    <td style="width:5%;text-align:center;background-color:#dd4b39;color:white;"><b>#</b></td>
                    <td style="width:7%;text-align:center;background-color:#dd4b39;color:white;"><b>%</b></td>
                    <td style="width:12%;text-align:center;background-color:#dd4b39;color:white;"><b>NEGADAS</b></td>
                    <td style="width:5%;text-align:center;background-color:#dd4b39;color:white;"><b>#</b></td>
                    <td style="width:7%;text-align:center;background-color:#dd4b39;color:white;"><b>%</b></td>
                    <td style="width:12%;text-align:center;background-color:#dd4b39;color:white;"><b>PENDIENTES</b></td>
                    <td style="width:5%;text-align:center;background-color:#dd4b39;color:white;"><b>#</b></td>
                    <td style="width:7%;text-align:center;background-color:#dd4b39;color:white;"><b>%</b></td>
                    
                </tr>';
                $totoalRe = 0;
                $totoalPa = 0;
                $totoalNe = 0;
                $totoalPe = 0;
                for($i = 0; $i < count($arrayMesesCompleto); $i++){
                   
                    $Prome_Paga = 0;
                    $Prome_nega = 0;
                    $Prome_falt = 0;
                    if($arrayMesesCompleto[$i]['recibi'][0]){
                        $Prome_Paga = number_format((($arrayMesesCompleto[$i]['pagada'][0] * 100) / $arrayMesesCompleto[$i]['recibi'][0]), 2, '.', '.');
                        $Prome_nega = number_format((($arrayMesesCompleto[$i]['negada'][0] * 100) / $arrayMesesCompleto[$i]['recibi'][0]),2, '.', '.');
                        $Prome_falt = number_format((($arrayMesesCompleto[$i]['restan'] * 100) / $arrayMesesCompleto[$i]['recibi'][0]), 2, '.', '.');
                    }

                    $content .= '
                        <tr>
                            <td style="width:11%;text-align:center;font-family:tahoma;font-size:8;">'.$arrayMesesCompleto[$i]['mes'].'</td>
                            <td style="width:12%;text-align:center;font-family:tahoma;font-size:8;">$ '.number_format($arrayMesesCompleto[$i]['recibi'][0], 0, '.', '.').'</td>
                            <td style="width:5%;text-align:center;font-family:tahoma;font-size:8;"><b>'.$arrayMesesCompleto[$i]['Rrecibi'].'</b></td>


                            <td style="width:12%;text-align:center;font-family:tahoma;font-size:8;">$ '.number_format($arrayMesesCompleto[$i]['pagada'][0], 0, '.', '.').'</td>
                            <td style="width:5%;text-align:center;font-family:tahoma;font-size:8;"><b>'.$arrayMesesCompleto[$i]['Rpagada'].'</b></td>
                            <td style="width:7%;text-align:center;font-family:tahoma;font-size:8;"><b>'.$Prome_Paga.'</b></td>
                            

                            <td style="width:12%;text-align:center;font-family:tahoma;font-size:8;">$ '.number_format($arrayMesesCompleto[$i]['negada'][0], 0, '.', '.').'</td>
                            <td style="width:5%;text-align:center;font-family:tahoma;font-size:8;"><b>'.$arrayMesesCompleto[$i]['Rnegada'].'</b></td>
                            <td style="width:7%;text-align:center;font-family:tahoma;font-size:8;"><b>'.$Prome_nega.'</b></td>
                            

                            <td style="width:12%;text-align:center;font-family:tahoma;font-size:8;">$ '.number_format($arrayMesesCompleto[$i]['restan'], 0, '.', '.').'</td>
                            <td style="width:5%;text-align:center;font-family:tahoma;font-size:8;"><b>'.$arrayMesesCompleto[$i]['Rrestan'].'</b></td>
                            <td style="width:7%;text-align:center;font-family:tahoma;font-size:8;"><b>'.$Prome_falt.'</b></td>
                            
                        </tr>
                    ';

                    $totoalRe += $arrayMesesCompleto[$i]['recibi'][0];
                    $totoalPa += $arrayMesesCompleto[$i]['pagada'][0];
                    $totoalNe += $arrayMesesCompleto[$i]['negada'][0];
                    $totoalPe += $arrayMesesCompleto[$i]['restan'];
                }

            $PToRE = 0;
            $PToPA = 0;
            $PToNE = 0;
            $PToPE = 0;
            if($totoalRe > 0){
                $PToPA = number_format((($totoalPa * 100) /$totoalRe) , 2, '.', '.');
                $PToNE = number_format((($totoalNe * 100) /$totoalRe) , 2, '.', '.');
                $PToPE = number_format((($totoalPe * 100) /$totoalRe), 2, '.', '.');
            }

            $content .= '
                <tr>
                    <td style="width:11%;text-align:center;font-family:tahoma;font-size:8;"><b>TOTAL</b></td>
                    <td colspan="2" style="text-align:center;font-family:tahoma;font-size:8;"><b>$ '.number_format($totoalRe, 2, '.', '.').'</b></td>
                    <td colspan="3" style="text-align:center;font-family:tahoma;font-size:8;"><b>$ '.number_format($totoalPa, 2, '.', '.').'</b></td>
                    <td colspan="3" style="text-align:center;font-family:tahoma;font-size:8;"><b>$ '.number_format($totoalNe, 2, '.', '.').'</b></td>
                    <td colspan="3" style="text-align:center;font-family:tahoma;font-size:8;"><b>$ '.number_format($totoalPe, 2, '.', '.').'</b></td>
                </tr>
                <tr>
                    <td style="width:11%;text-align:center;font-family:tahoma;font-size:8;background-color:#dd4b39;color:white;"><b>% TOTAL</b></td>

                    <td colspan="2" style="text-align:center;font-family:tahoma;font-size:8;background-color:#dd4b39;color:white;"><b>100 %</b></td>

                    <td colspan="3"style="text-align:center;font-family:tahoma;font-size:8;background-color:#dd4b39;color:white;"><b>'.$PToPA.' %</b></td>

                    <td colspan="3" style="text-align:center;font-family:tahoma;font-size:8;background-color:#dd4b39;color:white;"><b>'.$PToNE.' %</b></td>

                    <td colspan="3" style="text-align:center;font-family:tahoma;font-size:8;background-color:#dd4b39;color:white;"><b>'.$PToPE.' %</b></td>
                </tr>
            </table>';


        
            
            $Por_90p = 0;
            $Por_120p = 0;
            $Por_150p = 0;
            $Por_180p = 0;
            $Por_210p = 0;
            $Por_210pp = 0;
            if($totoalPa > 0){
                $Por_90p = number_format($arrayMesesPagos['_MoneyPagadas0_90'] * 100 / $totoalPa, 2);
                $Por_120p =number_format( $arrayMesesPagos['_MoneyPagadas91_120'] * 100 / $totoalPa, 2);
                $Por_150p = number_format($arrayMesesPagos['_MoneyPagadas121_150'] * 100 / $totoalPa, 2);
                $Por_180p = number_format($arrayMesesPagos['_MoneyPagadas151_180'] * 100 / $totoalPa, 2);
                $Por_210p = number_format($arrayMesesPagos['_MoneyPagadas181_210'] * 100 / $totoalPa, 2);
                $Por_210pp = number_format($arrayMesesPagos['_MoneyPagadas210'] * 100 / $totoalPa , 2);
            }

            $content .='<br/>
            <br/>
            <table style="font-family:tahoma;font-size:9;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                <tr>
                    <td style="width:100%;text-align:justify;"><b>INCAPACIDADES PAGADAS</b></td>
                </tr>
            </table>
            <table style="font-family:tahoma;font-size:8;" width="100%" cellpadding="2px" cellspacing="2px" width="100%">
                <tr>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;background-color:#dd4b39;color:white;"><b>PAGADAS  ENTRE 0 Y 90 DÍAS</b></td>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;background-color:#dd4b39;color:white;"><b>PAGADAS  ENTRE 91 Y 120 DÍAS</b></td>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;background-color:#dd4b39;color:white;"><b>PAGADAS  ENTRE 121 Y 150 DÍAS</b></td>
                </tr>
                <tr>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;"><b>$ '.number_format($arrayMesesPagos['_MoneyPagadas0_90'], 2, '.','.').' /  '.$Por_90p.' %</b></td>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;"><b>$ '.number_format($arrayMesesPagos['_MoneyPagadas91_120'], 2, '.','.').' /  '.$Por_120p.' %</b></td>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;"><b>$ '.number_format($arrayMesesPagos['_MoneyPagadas121_150'], 2, '.','.').' /  '.$Por_150p.' %</b></td>
                </tr>
                <tr>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;background-color:#dd4b39;color:white;"><b>PAGADAS  ENTRE 151 Y 180 DÍAS</b></td>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;background-color:#dd4b39;color:white;"><b>PAGADAS  ENTRE 181 Y 210 DÍAS</b></td>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;background-color:#dd4b39;color:white;"><b>PAGADAS  MÁS DE 210 DÍAS</b></td>
                </tr>
                <tr>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;"><b>$ '.number_format($arrayMesesPagos['_MoneyPagadas151_180'], 2, '.','.').' /  '.$Por_180p.' %</b></td>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;"><b>$ '.number_format($arrayMesesPagos['_MoneyPagadas181_210'], 2, '.','.').' /  '.$Por_210p.' %</b></td>
                    <td style="width:33%;text-align:center;font-family:tahoma;font-size:8;"><b>$ '.number_format($arrayMesesPagos['_MoneyPagadas210'], 2, '.','.').' /  '.$Por_210pp.' %</b></td>
                </tr>
            </table>';
            //SELECT SUM(inc_valor) as total FROM `gi_incapacidad` WHERE datediff(inc_fecha_pago, inc_fecha_generada ) > -100 AND  datediff(inc_fecha_pago, inc_fecha_generada ) <= 90 AND inc_empresa = 49;

            $obj_pdf->writeHTML($content, true, false, false, false, '');
      
        ob_start();
        $nombre='Reporte_pagos_x_mes_'.date("d-m-Y H-i-s").'.pdf';
        $obj_pdf->Output($nombre, 'I');
        $xlsData = ob_get_contents();
        ob_end_clean(); 

        $response =  array(
            'op' => 'ok',
            'file' => "data:application/pdf;base64,".base64_encode($xlsData)
        );

        echo json_encode($response);
    }

