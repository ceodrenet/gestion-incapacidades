<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/incapacidades.controlador.php';
	require_once '../controladores/empleados.controlador.php';

	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/incapacidades.modelo.php';
	require_once '../modelos/empleados.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';
	require_once '../extenciones/Excel.php';

	if(isset($_POST['exportarIncapacidadesAnualesAjax'])){
		$objPHPExcel = new PHPExcel();
	    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
	    $objPHPExcel->setActiveSheetIndex(0);
	    $objPHPExcel->getProperties()->setCreator("GEIN")
	                             ->setLastModifiedBy("".$_SESSION['nombres'])
	                             ->setTitle("GEIN - Incapacidades")
	                             ->setSubject("Incapacidades Anuales")
	                             ->setDescription("Descarga del historico de incapacidades Anual registrado en el sistema")
	                             ->setKeywords("office 2007 openxml php")
	                             ->setCategory("Incapacidades");
	    $objPHPExcel->getActiveSheet()
	    ->getStyle('A1:Y1')
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    $objPHPExcel->getActiveSheet()->getStyle('A1:Y1')->getFont()->setBold( true );
	    $objPHPExcel->getActiveSheet()->setTitle('INCAPACIDADES');

	    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("B1", "MES"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("C1", "# Incapacidades"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("D1", "# ENFERMEDAD GENERAL"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("E1", "# LICENCIA MATERNIDAD"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("F1", "# LICENCIA PATERNIDAD"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("G1", "# ACCIDENTE LABORAL"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("H1", "# ACCIDENTE DE TRANSITO"); 

	    $month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

	    $whereCliente = '';
	    if($_POST['cliente_id'] != 0){
	        $whereCliente =  "inc_empresa = ".$_POST['cliente_id']." AND ";
	    }
	    $campos = 'count(*) as total , MONTH(inc_fecha_inicio) as mes';
	    $tabla  = 'gi_incapacidad';
	    $condicion =  $whereCliente." YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." ";
	    $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, 'GROUP BY mes', 'ORDER BY mes ASC');
	       
	    $nombres = '';
		$valores = '';
        $i = 2;
        $totalIncapacidades = 0;
        $totalIncaEnferGene = 0;
        $totalIncaLicenMate = 0;
        $totalIncaLicenPate = 0;
        $totalIncaAcciLabor = 0;
        $totalIncaAccoTrans = 0;


		foreach ($incapacidades as $key => $value) {
            $numero = $value['mes'];
            
            
            $tabla  = 'gi_incapacidad';
            $campos = 'count(*) as total';
            $condicion =  $whereCliente." MONTH(inc_fecha_inicio) = ".$value['mes']." AND YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." AND inc_origen = 'ENFERMEDAD GENERAL' ";
            $eg = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
            
            $condicion =  $whereCliente." MONTH(inc_fecha_inicio) = ".$value['mes']." AND YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." AND inc_origen = 'LICENCIA DE MATERNIDAD' ";
            $lm = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');
            
            $condicion =  $whereCliente." MONTH(inc_fecha_inicio) = ".$value['mes']." AND YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." AND inc_origen = 'LICENCIA DE PATERNIDAD' ";
            $lp = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

            $condicion =  $whereCliente." MONTH(inc_fecha_inicio) = ".$value['mes']." AND YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." AND inc_origen = 'ACCIDENTE LABORAL' ";
            $al = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');

            $condicion =  $whereCliente." MONTH(inc_fecha_inicio) = ".$value['mes']." AND YEAR(inc_fecha_inicio) = ".$_POST['fechaFinal']." AND inc_origen = 'ACCIDENTE TRANSITO' ";
            $at = ModeloTesoreria::mdlMostrarGroupAndOrderX($campos, $tabla, $condicion, '', '');


            $totalIncapacidades += $value['total'];
            $totalIncaEnferGene += $eg['total'];
            $totalIncaLicenMate += $lm['total'];
            $totalIncaLicenPate += $lp['total'];
            $totalIncaAcciLabor += $al['total'];
            $totalIncaAccoTrans += $at['total'];

            $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key +1)); 
	        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $month[$numero -1]); 
	        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value['total']); 
	        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $eg['total']); 
	        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $lm['total']); 
	        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $lp['total']); 
	        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $al['total']); 
	        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $at['total']); 
			$i++;
        }


        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, 'TOTAL'); 
        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $totalIncapacidades); 
        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $totalIncaEnferGene); 
        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $totalIncaLicenMate); 
        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $totalIncaLicenPate); 
        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $totalIncaAcciLabor); 
        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $totalIncaAccoTrans); 
		

		// Write the Excel file to filename some_excel_file.xlsx in the current directory
	    ob_start();
	    $writer->save("php://output");
	    $xlsData = ob_get_contents();
	    ob_end_clean(); 

	    $response =  array(
	        'op' => 'ok',
	        'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
	    );

	    echo json_encode($response);

	}	


	if(isset($_POST['exportarIncapacidadesEstadoAjax'])){
		$objPHPExcel = new PHPExcel();
	    $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
	    $objPHPExcel->setActiveSheetIndex(0);
	    $objPHPExcel->getProperties()->setCreator("GEIN")
	                             ->setLastModifiedBy("".$_SESSION['nombres'])
	                             ->setTitle("GEIN - Incapacidades")
	                             ->setSubject("Incapacidades Por Estado")
	                             ->setDescription("Descarga del historico de incapacidades Estado registrado en el sistema")
	                             ->setKeywords("office 2007 openxml php")
	                             ->setCategory("Incapacidades");
	    $objPHPExcel->getActiveSheet()
	    ->getStyle('A1:Y1')
	    ->getAlignment()
	    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	    $objPHPExcel->getActiveSheet()->getStyle('A1:Y1')->getFont()->setBold( true );
	    $objPHPExcel->getActiveSheet()->setTitle('INCAPACIDADES');

	    

        $estadoFecha = '';
        if($_POST['estado'] == 'RADICADA'){
            $estadoFecha =  "Fecha Radicación";
        }else if($_POST['estado'] == 'SOLICITUD DE PAGO'){
            $estadoFecha =  "Fecha Solicitud";
        }else if($_POST['estado'] == 'PAGADA'){
            $estadoFecha =  "Fecha de pago ";
        }else if($_POST['estado'] == 'SIN RECONOCIMIENTO'){
            $estadoFecha =  "Fecha de Negacion";
        }else{
            $estadoFecha =  "Fecha de registro";
        }
                    

	    $objPHPExcel->getActiveSheet()->setCellValue("A1", "#"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("B1", 'Empresa'); 
	    $objPHPExcel->getActiveSheet()->setCellValue("C1", 'Cedula'); 
	    $objPHPExcel->getActiveSheet()->setCellValue("D1", 'Nombre'); 
	    $objPHPExcel->getActiveSheet()->setCellValue("E1", "Administradora"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("F1", "Fecha Inicial"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("G1", "Fecha Final"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("H1", "Estado"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("I1", "Valor"); 
	    $objPHPExcel->getActiveSheet()->setCellValue("J1", $estadoFecha); 
	    $objPHPExcel->getActiveSheet()->setCellValue("K1", 'Observaciones');

	  
	    $whereCliente = '';
	    if($_POST['cliente_id'] != 0){
	        $whereCliente =  "inc_empresa = ".$_POST['cliente_id']." AND ";
	    }

	    $valor = $_POST['fechaInicial'];
        $valor2 = $_POST['fechaFinal'];
        $valor3 = $_POST['estado'];

	    $campos = 'emp_nombre, emd_cedula, emd_nombre, inc_diagnostico, inc_origen, inc_clasificacion, inc_fecha_inicio, inc_fecha_final, inc_tipo_generacion, inc_profesional_responsable, inc_prof_registro_medico, inc_donde_se_genera, inc_fecha_generada, inc_observacion, inc_estado_tramite, inc_fecha_radicacion, inc_fecha_solicitud, inc_fecha_pago, inc_valor, inc_fecha_pago_nomina, inc_valor_pagado_nomina, ip2.ips_nombre as inc_ips_afiliado, ip3.ips_nombre as inc_arl_afiliado ,  ip4.ips_nombre as inc_afp_afiliado, inc_fecha_negacion_, inc_observacion_neg';
        $tabla  = 'gi_incapacidad LEFT JOIN gi_ips as ip2 ON ip2.ips_id = inc_ips_afiliado LEFT JOIN gi_empresa ON emp_id = inc_empresa LEFT JOIN gi_empleados ON emd_id = inc_emd_id LEFT JOIN gi_ips as ip3 ON ip3.ips_id = emd_arl_id LEFT JOIN gi_ips as ip4 ON ip4.ips_id = emd_afp_id';
        $condicion = $whereCliente." inc_estado_tramite = '".$valor3."' AND inc_fecha_inicio BETWEEN '".$valor."' AND '".$valor2."' ";
        $incapacidades = ModeloTesoreria::mdlMostrarGroupAndOrder($campos, $tabla, $condicion, '', 'ORDER BY inc_id ASC');
	       
        $i = 2;
        $valorPagado = 0;
		foreach ($incapacidades as $key => $value) {
           	
           	$fecha = null;
           	$observaciones =  $value['inc_observacion'];
            if($_POST['estado'] == 'RADICADA'){
                $fecha = $value['inc_fecha_radicacion'];
            }else if($_POST['estado'] == 'SOLICITUD DE PAGO'){
                $fecha = $value['inc_fecha_solicitud'];
            }else if($_POST['estado'] == 'PAGADA'){
                $fecha = $value['inc_fecha_pago'];
            }else if($_POST['estado'] == 'SIN RECONOCIMIENTO'){
                $fecha = $value['inc_fecha_negacion_'];
                $observaciones =  $value['inc_observacion_neg'];
            }else{
                $fecha = $value['inc_fecha_generada'];
            }

            $valorEstado = 0;
            if(!empty($value["inc_valor"]) && !is_null($value["inc_valor"])){
                $valorEstado = "$ ".number_format($value["inc_valor"], 0, ',', '.');
                $valorPagado += $value["inc_valor"];
            }

         
            $administradora = '';
            if($value['inc_clasificacion'] != 4){
                if($value['inc_origen'] == 'ACCIDENTE LABORAL'){
                    $administradora = $value["inc_arl_afiliado"]; 
                }else{
                    $administradora = $value["inc_ips_afiliado"]; 
                }
            }else{
                $administradora = $value["inc_afp_afiliado"];    
            }

            $objPHPExcel->getActiveSheet()->setCellValue("A".$i, ($key +1)); 
	        $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["emp_nombre"]); 
	        $objPHPExcel->getActiveSheet()->setCellValue("C".$i, $value["emd_cedula"]);
	        $objPHPExcel->getActiveSheet()->setCellValue("D".$i, $value["emd_nombre"]);
	        $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $administradora); 
	        $objPHPExcel->getActiveSheet()->setCellValue("F".$i, explode(' ',$value["inc_fecha_inicio"])[0]); 
	        $objPHPExcel->getActiveSheet()->setCellValue("G".$i, explode(' ',$value["inc_fecha_final"])[0]); 
	        $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value["inc_estado_tramite"]); 
	        $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $valorEstado); 
	        $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $fecha);
	        $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $observaciones); 
			$i++;
        }


		
		// Write the Excel file to filename some_excel_file.xlsx in the current directory
	    ob_start();
	    $writer->save("php://output");
	    $xlsData = ob_get_contents();
	    ob_end_clean(); 

	    $response =  array(
	        'op' => 'ok',
	        'file' => "data:application/vnd.ms-excel;base64,".base64_encode($xlsData)
	    );

	    echo json_encode($response);

	}
	