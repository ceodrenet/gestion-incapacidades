<link href="vistas/assets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="vistas/assets/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="vistas/assets/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<?php
    session_start();
    $_SESSION['start'] = time();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
    require_once '../controladores/incapacidades.controlador.php';
    require_once '../controladores/tesoreria.controlador.php';
    
    require_once '../modelos/dao.modelo.php';
    require_once '../modelos/incapacidades.modelo.php';
    require_once '../modelos/tesoreria.modelo.php';

    $campos = "COUNT(*) as Total";
    $otherCampos = "SUM(inc_valor) as Total";
    $tabla_ = "gi_incapacidad";
    $where_ = "inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' and inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 5 ";
    $pagadas = ModeloDAO::mdlMostrarUnitario( $campos, $tabla_ , $where_ );
    $valorPa = ModeloDAO::mdlMostrarUnitario( $otherCampos, $tabla_ , $where_ );
?>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-xl-6 col-md-6">
                <!-- card -->
                <div class="card card-h-100">
                    <!-- card body -->
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-6">
                                <span class="text-muted mb-3 lh-1 d-block text-truncate">PAGADAS</span>
                                <h4 class="mb-3">
                                    <span class="counter-value" data-target="<?php echo $pagadas['Total']; ?>">0</span>
                                </h4>
                            </div>
                        </div>
                        <div class="text-nowrap">
                            <span class="badge bg-soft-danger text-danger">100%</span>
                            <span class="ms-1 text-muted font-size-13">INCAPACIDADES PAGADAS</span>
                        </div>
                    </div><!-- end card body -->
                </div><!-- end card -->
            </div><!-- end col -->

            <div class="col-xl-6 col-md-6">
                <!-- card -->
                <div class="card card-h-100">
                    <!-- card body -->
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-7">
                                <span class="text-muted mb-3 lh-1 d-block text-truncate">VALOR PAGADO</span>
                                <h4 class="mb-3">
                                    <span><?php echo number_format($valorPa['Total'], 0, ',', '.'); ?></span>
                                </h4>
                            </div>
                        </div>
                        <div class="text-nowrap">
                            <span class="badge bg-soft-danger text-danger">100%</span>
                            <span class="ms-1 text-muted font-size-13">DINERO RECAUDADO</span>
                        </div>
                    </div><!-- end card body -->
                </div><!-- end card -->
            </div><!-- end col -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Grafico de pagos por Administradora</h3>
            </div>
            <div class="box-body">
                <canvas id="myChart" style="height:250px" data-colors='["#2ab57d", "#ebeff2"]'></canvas>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
    <div class="card border border-danger">
            <div class="card-header bg-transparent border-danger">
                <h5 class="my-0 text-danger">
                    PAGOS INCAPACIDADES
                </h5>
            </div>
            <div class="card-body">
            <table id="tablaEmpleados" style="width:100%;" class="table table-bordered dt-responsive nowrap w-100">
                <thead>
                    <tr>
                        <th style="width: 10px;">#</th>
                        <?php 
                            if($_SESSION['cliente_id'] == 0){ 
                                echo '<th>EMPRESA</th>';
                            }
                        ?>
                        <th>EMPLEADO</th>
                        <th>CÉDULA</th>
                        <th>ADMINISTRADORA</th>
                        <th>VALOR PAGADO</th>
                        <th>FECHA DE PAGO</th>
                        <th>FECHA INICIO</th>
                        <th>DIAS</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

    $campos = "emp_nombre, emd_nombre, inc_cc_afiliado, ori_descripcion_v as inc_origen, ip2.ips_nombre as eps, ip3.ips_nombre as arl,  ip4.ips_nombre as afp, inc_valor , Date_format(inc_fecha_pago,'%d/%m/%Y') as inc_fecha_pago  , Date_format(inc_fecha_inicio,'%d/%m/%Y') as inc_fecha_inicio, 
    Date_format(inc_fecha_final,'%d/%m/%Y') as inc_fecha_final,
    (DATEDIFF(inc_fecha_final,inc_fecha_inicio) +1) as dias, inc_clasificacion ";
    $tabla_ = " gi_incapacidad ";
    $tabla_ .= " JOIN gi_empresa ON emp_id = inc_empresa 
    JOIN gi_empleados ON emd_id = inc_emd_id 
    JOIN gi_ips ip2 ON ip2.ips_id = inc_ips_afiliado 
    LEFT JOIN gi_ips ip3 ON ip3.ips_id = inc_arl_afiliado 
    LEFT JOIN gi_ips ip4 ON ip4.ips_id = inc_afp_afiliado 
    JOIN gi_origen on ori_id_i= inc_origen ";
    $where_ = "inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' and inc_empresa = ".$_SESSION['cliente_id']." AND inc_estado_tramite = 5 ";
    $pagadas = ModeloDAO::mdlMostrarGroupAndOrder($campos,$tabla_,$where_, null, 'ORDER BY inc_fecha_pago DESC', null);

                        foreach ($pagadas as $key => $value) {
                            echo '<tr>';
                                echo '<td>'.($key+1).'</td>';
                                if($_SESSION['cliente_id'] == 0){ 
                                    echo '<td>'.$value['emp_nombre'].'</td>';
                                }
                                echo '<td>'.$value['emd_nombre'].'</td>';
                                echo '<td>'.$value['inc_cc_afiliado'].'</td>';

                                if($value['inc_clasificacion'] != 4){
                                    if($value['inc_origen'] == 4){
                                        echo '<td class="text-uppercase">'.($value["arl"]).'</td>'; 
                                    }else{
                                        echo '<td class="text-uppercase">'.($value["eps"]).'</td>'; 
                                    }
                                }else{
                                    echo '<td class="text-uppercase">'.($value["afp"]).'</td>';    
                                }
                                
                                if(!empty($value['inc_valor']) && $value['inc_valor'] != ''){
                                    echo '<td>$ '.number_format($value['inc_valor'], 0, ',', '.').'</td>';
                                }else{
                                    echo '<td></td>';
                                }
                                echo '<td>'.$value['inc_fecha_pago'].'</td>';
                                echo '<td>'.$value['inc_fecha_inicio'].'</td>';
                                echo '<td>'.$value['dias'] .'</td>';
                            echo '</tr>';

                        }
                    ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<!-- Required datatable js -->
<script src="vistas/assets/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<!-- Responsive examples -->
<script src="vistas/assets/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="vistas/assets/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="vistas/assets/assets/libs/chart.js/Chart.bundle.min.js"></script>
<script type="text/javascript">
$(function() {
    crearBarrasporPagar();
    $('#tablaEmpleados').DataTable({
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }

    });
});

initCounterNumber();

function initCounterNumber() {
    var counter = document.querySelectorAll('.counter-value');
    var speed = 250; // The lower the slower
    counter.forEach(function (counter_value) {
        function updateCount() {
            var target = +counter_value.getAttribute('data-target');
            var count = +counter_value.innerText;
            var inc = target / speed;
            if (inc < 1) {
                inc = 1;
            }
            // Check if target is reached
            if (count < target) {
                // Add inc to count and output in counter_value
                counter_value.innerText = (count + inc).toFixed(0);
                // Call function every ms
                setTimeout(updateCount, 1);
            } else {
                counter_value.innerText = target;
            }
        };
        updateCount();
    });
}

function crearBarrasporPagar() {
    <?php

       /*Obtenemos las afp porque son por clasificacion +180 */        
        $campo = 'SUM(inc_valor) as total, afp_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = 'inc_clasificacion_id = 4 ';
        if($_SESSION['cliente_id'] != 0){
            $condicion .= ' AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND inc_estado_tramite_id = 5 ";
        }else{
            $condicion .= " AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND estado_tramite_id = 5";
        }

        $respuesta = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY afp_nombre', '');
        
        $label = '';
        $Qdias = '';
        $colors = '';
        foreach ($respuesta as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */

            $label .= "\"".$value['afp_nombre']."\" , ";
            $Qdias .= $value['total']." , ";
            $colors .= "getRandomColor(),";
        }

        /*Obtenemos las ARL porque son por accidente de transito*/
        $campo = 'SUM(inc_valor) as total, arl_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = "inc_clasificacion_id != 4 AND inc_origen_id = 4 ";
        if($_SESSION['cliente_id'] != 0){
            $condicion .= ' AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  AND estado_tramite_id = 5 ";
        }else{
            $condicion .= " AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  AND estado_tramite_id = 5";
        }

        $respuesta2 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY arl_nombre', '');
        
        foreach ($respuesta2 as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */

            $label .= "\"".$value['arl_nombre']."\" , ";
            $Qdias .= $value['total']." , ";
            $colors .= "getRandomColor(),";
        }

        /*Obtenemos las EPS porque son por accidente de transito*/
        $campo = 'SUM(inc_valor) as total, ips_nombre, SUM(inc_valor_pagado_nomina) as nominas ';
        $tabla = 'reporte_tesoreria';
        $condicion = "inc_clasificacion_id != 4 AND inc_origen_id != 4 ";
        if($_SESSION['cliente_id'] != 0){
            $condicion .= ' AND inc_empresa = '.$_SESSION['cliente_id']." AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."' AND estado_tramite_id = 5";
        }else{
            $condicion .= " AND inc_fecha_pago BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'  AND estado_tramite_id = 5";
        }

        $respuesta3 = ModeloTesoreria::mdlMostrarGroupAndOrder($campo,$tabla,$condicion, 'GROUP BY ips_nombre', '');
        
        foreach ($respuesta3 as $key => $value) {
            /* primero traemos las empresas que vamos a mostrar osea todas */
            /* traemos las de 15 dias */

            $label .= "\"".$value['ips_nombre']."\" , ";
            $Qdias .= $value['total']." , ";
            $colors .= "getRandomColor(),";
        }

    ?>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php echo $label; ?>],
            datasets: [{
                label: '',
                data: [<?php echo $Qdias; ?>],
                backgroundColor: [<?php echo $colors; ?>]
            }]
        },
        options: {
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        return "$" + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
                        });
                    }
                }
            }
        }
    });
}

function scaleLabel(valuePayload) {
    return Number(valuePayload.value).toFixed(2).replace('.', ',') + '$';
}

function getRandomColor() {
    return '#' + (Math.random().toString(16) + '0000000').slice(2, 8);
}
</script>

<?php
    function dias_transcurridos($fecha_i, $fecha_f)
    {
        $dias   = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias + 1;
    }

?>
