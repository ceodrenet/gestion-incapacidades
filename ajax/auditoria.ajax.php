<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
	require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/auditoria.controlador.php';
	
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/auditoria.modelo.php';

	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxAuditoria
	{
		public $Auditoria;
		public function getAuditoria(){
			$respuesta = ControladorAuditoria::ctrListarAuditoria();
            echo '{
                "data" : [';
                     $i = 0;
                  foreach ($respuesta as $key => $value) {
                      if($i != 0){
                          echo ",";
                      }
          
                      echo '[';
                          echo '"'.($key+1).'",';
                          echo '"'.$value["user_nombre"].' '.$value["user_apellidos"].'",';
                          echo '"'.$value["aud_controlador_v"].'",';
                          echo '"'.$value["aud_metodo_v"].'",';
                          echo '"'.$value["aud_fecha_d"].'",';
                          echo '"'.$value["aud_hora_d"].'",';
                          echo '"'.$value["emp_nombre"].'",';
                          echo '"'.$value["aud_id_incapacidad_i"].'",';
                          echo '"'.$value["aud_ciudad_v"].'",';
                          echo '"'.$value["aud_pais_v"].'",';
                        //  echo '"'.$value["aud_ip_i"].'",';
                          echo '"'.$value["aud_id_i"].'"';
                      echo ']';
                      $i++;
                  }
          echo ']
          }';
		}
	}

    if(isset($_GET['getDtaAudit'])){
        $auditoria = new AjaxAuditoria();
        $auditoria->getAuditoria();
    }
   