<?php
	session_start();
	$_SESSION['start'] = time();
	require_once '../controladores/mail.controlador.php';
    require_once '../controladores/plantilla.controlador.php';
	require_once '../controladores/calendario.controlador.php';
	require_once '../modelos/dao.modelo.php';
	require_once '../modelos/calendario.modelo.php';

	/**
	* Clase para utilizar con Ajax MVC
	*/
	class AjaxCalendario
	{
		public $id_calendario;
		
		public function ajaxEditarCalendario(){
			$item = 'evn_id';
			$valor = $this->id_calendario;
			$respuesta = ControladorCalendario::ctrMostrarEventos($item, $valor);
			echo json_encode($respuesta);
		}

	}
	
	
	if(isset($_POST['EditarCalendarioId'])){
		if($_POST['EditarCalendarioId'] != ''){
			$editar = new AjaxCalendario();
			$editar->id_calendario = $_POST['EditarCalendarioId'];
			$editar->ajaxEditarCalendario();
		}	
	}


		if(isset($_POST["txtNombreEvento"])){
		echo ControladorCalendario::ctrCrearEvento();
	}

	if(isset($_POST["txtEditarNombreEvento"])){
		echo ControladorCalendario::ctrEditarEvento();
	}

	if(isset($_POST["idEvento"])){
		echo ControladorCalendario::deleteEvento();
	}