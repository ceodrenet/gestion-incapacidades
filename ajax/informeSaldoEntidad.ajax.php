<?php
session_start();
$_SESSION['start'] = time();
ini_set('display_errors', 'On');
ini_set('display_errors', 1);
require_once '../controladores/mail.controlador.php';
require_once '../controladores/plantilla.controlador.php';
require_once '../controladores/incapacidades.controlador.php';
require_once '../controladores/tesoreria.controlador.php';

require_once '../modelos/dao.modelo.php';
require_once '../modelos/incapacidades.modelo.php';
require_once '../modelos/tesoreria.modelo.php';
require_once '../vendor/autoload.php';
require_once '../extenciones/Excel.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$borders = array(
    'borders' => array(
        'allborders' => array(
            'style' =>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
        )
    ),
);

$objPHPExcel = new Spreadsheet();

$objPHPExcel->getActiveSheet()->setTitle('INFORME');
$objPHPExcel->getActiveSheet()->getStyle('B1:S1')
	->getAlignment()
	->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->applyFromArray($borders);

$objPHPExcel->getActiveSheet()->mergeCells('B1:R1');
$objPHPExcel->getActiveSheet()->getStyle("B1:R1")->getFont()->setSize(16); 
$objPHPExcel->getActiveSheet()->setCellValue("B1", "INFORME DE SALDOS POR ENTIDAD"); 

$objPHPExcel->getActiveSheet()
    ->getStyle('A1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('B1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);

$objPHPExcel->getActiveSheet()->getStyle('B1:AR1')->getFont()->setBold( true );

$objPHPExcel->getActiveSheet()->mergeCells('B2:D2');
$objPHPExcel->getActiveSheet()->getStyle('B2:D2')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->setCellValue("B2", "Edad Inc Rango"); 

$objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
$objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->setCellValue("B3", "Nombre Entidades"); 

$objPHPExcel->getActiveSheet()->mergeCells('E2:F2');
$objPHPExcel->getActiveSheet()->getStyle('E2:F2')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->setCellValue("E2", "1) 0 y 30 Días");

$objPHPExcel->getActiveSheet()->mergeCells('G2:H2');
$objPHPExcel->getActiveSheet()->getStyle('G2:H2')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->setCellValue("G2", "1) 31 y 60 Días");

$objPHPExcel->getActiveSheet()->mergeCells('I2:J2');
$objPHPExcel->getActiveSheet()->getStyle('I2:J2')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->setCellValue("I2", "2) 61 y 90 Días");

$objPHPExcel->getActiveSheet()->mergeCells('K2:L2');
$objPHPExcel->getActiveSheet()->getStyle('K2:L2')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->setCellValue("K2", "3) 91 y 180 Días");

$objPHPExcel->getActiveSheet()->mergeCells('M2:N2');
$objPHPExcel->getActiveSheet()->getStyle('M2:N2')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->setCellValue("M2", "4) 181 y 360");

$objPHPExcel->getActiveSheet()->mergeCells('O2:P2');
$objPHPExcel->getActiveSheet()->getStyle('O2:P2')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->setCellValue("O2", "4) Sup a 360 Días");

$objPHPExcel->getActiveSheet()->mergeCells('Q2:R2');
$objPHPExcel->getActiveSheet()->getStyle('Q2:R2')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->setCellValue("Q2", "Total");

$objPHPExcel->getActiveSheet()->setCellValue("E3", "#");
$objPHPExcel->getActiveSheet()->setCellValue("F3", '$');
$objPHPExcel->getActiveSheet()->setCellValue("G3", "#");
$objPHPExcel->getActiveSheet()->setCellValue("H3", '$');
$objPHPExcel->getActiveSheet()->setCellValue("I3", "#");
$objPHPExcel->getActiveSheet()->setCellValue("J3", '$');
$objPHPExcel->getActiveSheet()->setCellValue("K3", "#");
$objPHPExcel->getActiveSheet()->setCellValue("L3", '$');
$objPHPExcel->getActiveSheet()->setCellValue("M3", "#");
$objPHPExcel->getActiveSheet()->setCellValue("N3", "$");
$objPHPExcel->getActiveSheet()->setCellValue("O3", "#");
$objPHPExcel->getActiveSheet()->setCellValue("P3", "$");
$objPHPExcel->getActiveSheet()->setCellValue("Q3", "#");
$objPHPExcel->getActiveSheet()->setCellValue("R3", "$");

$objPHPExcel->getActiveSheet()->getStyle('B2:R2')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B2:R2')->getFont()->setBold( true );

$objPHPExcel->getActiveSheet()->getStyle('B3:R3')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B3:S3')->getFont()->setBold( true );

$filtroFecha = $_POST['filtroFecha'];

$tablaEps ="gi_incapacidad LEFT JOIN gi_ips eps ON inc_ips_afiliado = eps.ips_id ";
$whereEps = "(inc_valor_pagado_eps != 0 OR inc_valor != 0 ) ";
$groupEps = "GROUP BY (eps_nombre)";
$orderByEps = "ORDER BY (eps_nombre)";

$tablaArl ="gi_incapacidad LEFT JOIN gi_ips arl ON inc_arl_afiliado = arl.ips_id ";
$whereArl = "(inc_valor_pagado_eps != 0 OR inc_valor != 0 ) ";
$groupArl = "GROUP BY (arl_nombre)";
$orderByArl = "ORDER BY (arl_nombre)";

$where0y30 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 0 AND 30 ";
$where31y60 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 31 AND 60 ";
$where61y90 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 61 AND 90 ";
$where91y180 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 91 AND 180 ";
$where181y360 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 181 AND 360 ";
$whereSup360 = "DATEDIFF(DATE(NOW()), $filtroFecha) > 360 ";

$whereArl0y30 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 0 AND 30 AND inc_origen = 4 ";
$whereArl31y60 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 31 AND 60 AND inc_origen = 4 ";
$whereArl61y90 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 61 AND 90 AND inc_origen = 4 ";
$whereArl91y180 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 91 AND 180 AND inc_origen = 4 ";
$whereArl181y360 = "DATEDIFF(DATE(NOW()), $filtroFecha) BETWEEN 181 AND 360 AND inc_origen = 4 ";
$whereArlSup360 = "DATEDIFF(DATE(NOW()), $filtroFecha) > 360 AND inc_origen = 4 ";

if ($_POST['yearFilter'] != 0) {
    $fecha1 = $_POST['yearFilter']."-01-01";
    $fecha2 = $_POST['yearFilter']."-12-31";

    $whereEps.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArl.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";

    $where0y30.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $where31y60.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $where61y90.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $where91y180.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $where181y360.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereSup360.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";

    $whereArl0y30.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArl31y60.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArl61y90.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArl91y180.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArl181y360.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
    $whereArlSup360.="AND (".$filtroFecha." BETWEEN '".$fecha1."' AND '".$fecha2."') ";
}

$sumTipoValor = "";
if ($_POST["filtroEstadoTramite"] == 1) {
    $whereEps.="AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArl.="AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    
    $where0y30 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";
    $where31y60 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";
    $where61y90 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";
    $where91y180 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";
    $where181y360 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";
    $whereSup360 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) AND inc_origen != 4 ";

    $whereArl0y30 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArl31y60 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArl61y90 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArl91y180 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArl181y360 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";
    $whereArlSup360 .= "AND (inc_estado_tramite = 5 OR inc_estado_tramite = 8) ";

    $sumTipoValor = "SUM(inc_valor)";
} else if ($_POST["filtroEstadoTramite"] == 2) {
    $whereEps.="AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArl.="AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";

    $where0y30 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";
    $where31y60 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";
    $where61y90 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";
    $where91y180 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";
    $where181y360 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";
    $whereSup360 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) AND inc_origen != 4 ";

    $whereArl0y30 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArl31y60 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArl61y90 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArl91y180 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArl181y360 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";
    $whereArlSup360 .= "AND (inc_estado_tramite = 2 OR inc_estado_tramite = 3 OR inc_estado_tramite = 4 OR inc_estado_tramite = 7) ";

    $sumTipoValor = "SUM(inc_valor_pagado_eps)";
} else if ($_POST["filtroEstadoTramite"] == 3) {
    $whereEps.="AND (inc_estado_tramite = 6) ";
    $whereArl.="AND (inc_estado_tramite = 6) ";

    $where0y30 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";
    $where31y60 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";
    $where61y90 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";
    $where91y180 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";
    $where181y360 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";
    $whereSup360 .= "AND (inc_estado_tramite = 6) AND inc_origen != 4 ";

    $whereArl0y30 .= "AND (inc_estado_tramite = 6) ";
    $whereArl31y60 .= "AND (inc_estado_tramite = 6) ";
    $whereArl61y90 .= "AND (inc_estado_tramite = 6) ";
    $whereArl91y180 .= "AND (inc_estado_tramite = 6) ";
    $whereArl181y360 .= "AND (inc_estado_tramite = 6) ";
    $whereArlSup360 .= "AND (inc_estado_tramite = 6) ";
    
    $sumTipoValor = "SUM(inc_valor_pagado_eps)";
} else {
    $whereEps.="AND (inc_estado_tramite != 1) ";
    $whereArl.="AND (inc_estado_tramite != 1) ";
    
    $where0y30 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";
    $where31y60 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";
    $where61y90 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";
    $where91y180 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";
    $where181y360 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";
    $whereSup360 .= "AND (inc_estado_tramite != 1) AND inc_origen != 4 ";

    $whereArl0y30 .= "AND (inc_estado_tramite != 1) ";
    $whereArl31y60 .= "AND (inc_estado_tramite != 1) ";
    $whereArl61y90 .= "AND (inc_estado_tramite != 1) ";
    $whereArl91y180 .= "AND (inc_estado_tramite != 1) ";
    $whereArl181y360 .= "AND (inc_estado_tramite != 1) ";
    $whereArlSup360 .= "AND (inc_estado_tramite != 1) ";

    $sumTipoValor = "SUM(IF(inc_estado_tramite=5, inc_valor, 0)) + 
    SUM(IF(inc_estado_tramite=2 OR inc_estado_tramite=3 OR inc_estado_tramite=4 OR inc_estado_tramite=6 OR inc_estado_tramite=7, inc_valor_pagado_eps, 0))";
}

$clienteId = 0;
if ($_SESSION['cliente_id'] != 0) {
    $whereEps .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArl .= "AND inc_empresa = ".$_SESSION['cliente_id'];

    $where0y30 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $where31y60 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $where61y90 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $where91y180 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $where181y360 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereSup360 .= "AND inc_empresa = ".$_SESSION['cliente_id'];

    $whereArl0y30 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArl31y60 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArl61y90 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArl91y180 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArl181y360 .= "AND inc_empresa = ".$_SESSION['cliente_id'];
    $whereArlSup360 .= "AND inc_empresa = ".$_SESSION['cliente_id'];

    $clienteId = $_SESSION['cliente_id'];
}

$report = array();
$row = array();

$eps = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre", $tablaEps, $whereEps, $groupEps, $orderByEps);
$arl = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre", $tablaArl, $whereArl, $groupArl, $orderByArl);

$datos0y30 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $where0y30, $groupEps, $orderByEps);
$datos31y60 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $where31y60, $groupEps, $orderByEps);
$datos61y90 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $where61y90, $groupEps, $orderByEps);
$datos91y180 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $where91y180, $groupEps, $orderByEps);
$datos181y360 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $where181y360, $groupEps, $orderByEps);
$datosSup360 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT eps.ips_nombre as eps_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaEps, $whereSup360, $groupEps, $orderByEps);

$datosArl0y30 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArl0y30, $groupArl, $orderByArl);
$datosArl31y60 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArl31y60, $groupArl, $orderByArl);
$datosArl61y90 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArl61y90, $groupArl, $orderByArl);
$datosArl91y180 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArl91y180, $groupArl, $orderByArl);
$datosArl181y360 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArl181y360, $groupArl, $orderByArl);
$datosArlSup360 = ModeloDAO::mdlMostrarGroupAndOrder("DISTINCT arl.ips_nombre as arl_nombre, $sumTipoValor total, COUNT(*) as cant", $tablaArl, $whereArlSup360, $groupArl, $orderByArl);


$cantTotal0y30 = 0;
$cantTotal31y60 = 0;
$cantTotal61y90 = 0;
$cantTotal91y180 = 0;
$cantTotal181y360 = 0;
$cantTotalSup360 = 0;

$valorTotal0y30 = 0;
$valorTotal31y60 = 0;
$valorTotal61y90 = 0;
$valorTotal91y180 = 0;
$valorTotal181y360 = 0;
$valorTotalSup360 = 0;

$valorTotalCant = 0;
$valorTotalSaldo = 0;


$j = 0;
foreach ($eps as $key => $value) {

    $val0y30 = 0; $val31y60 = 0; $val61y90 = 0; $val91y180 = 0; $val181y360 = 0; $valSup360 = 0;
    $cant0y30 = 0; $cant31y60 = 0; $cant61y90 = 0; $cant91y180 = 0; $cant181y360 = 0; $cantSup360 = 0;

    $row["entidad"] = $value["eps_nombre"];

    if (!empty($datos0y30)) {
        foreach ($datos0y30 as $data) {
            if ($value["eps_nombre"] == $data["eps_nombre"]) {
                if (!empty($data["cant"])) {
                    $cant0y30 = $data["cant"];
                }
                if (!empty($data["total"])) {
                    $val0y30 = $data["total"];
                } 
                $row["c0y30"] = $cant0y30;
                $row["s0y30"] = $val0y30;
                break;
            } else {
                $row["c0y30"] = 0;
                $row["s0y30"] = 0;
            }
        }
    } else {
        $row["c0y30"] = 0;
        $row["s0y30"] = 0;
    }

    if (!empty($datos31y60)) {
        foreach ($datos31y60 as $data) {
            if ($value["eps_nombre"] == $data["eps_nombre"]) {
                if (!empty($data["cant"])) {
                    $cant31y60 = $data["cant"];
                }
                if (!empty($data["total"])) {
                    $val31y60 = $data["total"];
                }
                $row["c31y60"] = $cant31y60;
                $row["s31y60"] = $val31y60;
                break;
            } else {
                $row["c31y60"] = 0;
                $row["s31y60"] = 0;
            }
        }
    } else {
        $row["c31y60"] = 0;
        $row["s31y60"] = 0;
    }

    if (!empty($datos61y90)) {
        foreach ($datos61y90 as $data) {
            if ($value["eps_nombre"] == $data["eps_nombre"]) {
                if (!empty($data["cant"])) {
                    $cant61y90 = $data["cant"];
                }
                if (!empty($data["total"])) {
                    $val61y90 = $data["total"];
                }
                $row["c61y90"] = $cant61y90;
                $row["s61y90"] = $val61y90;
                break;
            } else {
                $row["c61y90"] = 0;
                $row["s61y90"] = 0;
            }
        }
    } else {
        $row["c61y90"] = 0;
        $row["s61y90"] = 0;
    }

    if (!empty($datos91y180)) {
        foreach ($datos91y180 as $data2) {
            if ($value["eps_nombre"] == $data2["eps_nombre"]) {
                if (!empty($data2["cant"])) {
                    $cant91y180 = $data2["cant"];
                }
                if (!empty($data2["total"])) {
                    $val91y180 = $data2["total"];
                }
                $row["c91y180"] = $cant91y180;
                $row["s91y180"] = $val91y180;
                break;
            } else {
                $row["c91y180"] = 0;
                $row["s91y180"] = 0;
            }
        }
    } else {
        $row["c91y180"] = 0;
        $row["s91y180"] = 0;
    }
    
    if (!empty($datos181y360)) {
        foreach ($datos181y360 as $data3) {
            if ($value["eps_nombre"] == $data3["eps_nombre"]) {
                if (!empty($data3["cant"])) {
                    $cant181y360 = $data3["cant"];
                }
                if (!empty($data3["total"])) {
                    $val181y360 = $data3["total"];
                }
                $row["c181y360"] = $cant181y360;
                $row["s181y360"] = $val181y360;
                break;
            } else {
                $row["c181y360"] = 0;
                $row["s181y360"] = 0;
            }
        }
    } else {
        $row["c181y360"] = 0;
        $row["s181y360"] = 0;
    }

    if (!empty($datosSup360)) {
        foreach ($datosSup360 as $data4) {
            if ($value["eps_nombre"] == $data4["eps_nombre"]) {
                if (!empty($data4["cant"])) {
                    $cantSup360 = $data4["cant"];
                }
                if (!empty($data4["total"])) {
                    $valSup360 = $data4["total"];
                }
                $row["cSup360"] = $cantSup360;
                $row["sSup360"] = $valSup360;
                break;
            } else {
                $row["cSup360"] = 0;
                $row["sSup360"] = 0;
            }
        }
    } else {
        $row["cSup360"] = 0;
        $row["sSup360"] = 0;
    }

    $cantTotal0y30+=$cant0y30;
    $cantTotal31y60+=$cant31y60;
    $cantTotal61y90+=$cant61y90;
    $cantTotal91y180+=$cant91y180;
    $cantTotal181y360+=$cant181y360;
    $cantTotalSup360+=$cantSup360;

    $valorTotal0y30+=$val0y30;
    $valorTotal31y60+=$val31y60;
    $valorTotal61y90+=$val61y90;
    $valorTotal91y180+=$val91y180;
    $valorTotal181y360+=$val181y360;
    $valorTotalSup360+=$valSup360;

    $cantTotal = $cant0y30 + $cant31y60 + $cant61y90 + $cant91y180 + $cant181y360 + $cantSup360;
    $valorTotal = $val0y30 + $val31y60 + $val61y90 + $val91y180 + $val181y360 + $valSup360;

    $valorTotalCant+=$cantTotal;
    $valorTotalSaldo+=$valorTotal;

    $row["ctotal"] = $cantTotal;
    $row["stotal"] = $valorTotal;
    
    $report[$j] = $row;

    $j++;
}
foreach ($arl as $key => $value) {
    $val0y30 = 0; $val31y60 = 0; $val61y90 = 0; $val91y180 = 0; $val181y360 = 0; $valSup360 = 0;
    $cant0y30 = 0; $cant31y60 = 0; $cant61y90 = 0; $cant91y180 = 0; $cant181y360 = 0; $cantSup360 = 0;

    $row["entidad"] = $value["arl_nombre"];

    if (!empty($datosArl0y30)) {
        foreach ($datosArl0y30 as $data) {
            if ($value["arl_nombre"] == $data["arl_nombre"]) {
                if (!empty($data["cant"])) {
                    $cant0y30 = $data["cant"];
                }
                if (!empty($data["total"])) {
                    $val0y30 = $data["total"];
                }
                
                $row["c0y30"] = $cant0y30;
                $row["s0y30"] = $val0y30;
                break;
            } else {
                $row["c0y30"] = 0;
                $row["s0y30"] = 0;
            }
        }
    } else {
        $row["c0y30"] = 0;
        $row["s0y30"] = 0;
    }

    if (!empty($datosArl31y60)) {
        foreach ($datosArl31y60 as $data) {
            if ($value["arl_nombre"] == $data["arl_nombre"]) {
                if (!empty($data["cant"])) {
                    $cant31y60 = $data["cant"];
                }
                if (!empty($data["total"])) {
                    $val31y60 = $data["total"];
                }
                $row["c31y60"] = $cant31y60;
                $row["s31y60"] = $val31y60;
                break;
            } else {
                $row["c31y60"] = 0;
                $row["s31y60"] = 0;
            }
        }
    } else {
        $row["c31y60"] = 0;
        $row["s31y60"] = 0;
    }

    if (!empty($datosArl61y90)) {
        foreach ($datosArl61y90 as $data) {
            if ($value["arl_nombre"] == $data["arl_nombre"]) {
                if (!empty($data["cant"])) {
                    $cant61y90 = $data["cant"];
                }
                if (!empty($data["total"])) {
                    $val61y90 = $data["total"];
                }
                $row["c61y90"] = $cant61y90;
                $row["s61y90"] = $val61y90;
                break;
            } else {
                $row["c61y90"] = 0;
                $row["s61y90"] = 0;
            }
        }
    } else {
        $row["c61y90"] = 0;
        $row["s61y90"] = 0;
    }

    if (!empty($datosArl91y180)) {
        foreach ($datosArl91y180 as $data2) {
            if ($value["arl_nombre"] == $data2["arl_nombre"]) {
                if (!empty($data2["cant"])) {
                    $cant91y180 = $data2["cant"];
                }
                if (!empty($data2["total"])) {
                    $val91y180 = $data2["total"];
                }
                $row["c91y180"] = $cant91y180;
                $row["s91y180"] = $val91y180;
                break;
            } else {
                $row["c91y180"] = 0;
                $row["s91y180"] = 0;
            }
        }
    } else {
        $row["c91y180"] = 0;
        $row["s91y180"] = 0;
    }

    if (!empty($datosArl181y360)) {
        foreach ($datosArl181y360 as $data3) {
            if ($value["arl_nombre"] == $data3["arl_nombre"]) {
                if (!empty($data3["cant"])) {
                    $cant181y360 = $data3["cant"];
                }
                if (!empty($data3["total"])) {
                    $val181y360 = $data3["total"];
                }
                $row["c181y360"] = $cant181y360;
                $row["s181y360"] = $val181y360;
                break;
            } else {
                $row["c181y360"] = 0;
                $row["s181y360"] = 0;
            }
        }
    } else {
        $row["c181y360"] = 0;
        $row["s181y360"] = 0;
    }

    if (!empty($datosArlSup360)) {
        foreach ($datosArlSup360 as $data4) {
            if ($value["arl_nombre"] == $data4["arl_nombre"]) {
                if (!empty($data4["cant"])) {
                    $cantSup360 = $data4["cant"];
                }
                if (!empty($data4["total"])) {
                    $valSup360 = $data4["total"];
                }
                $row["cSup360"] = $cantSup360;
                $row["sSup360"] = $valSup360;
                break;
            } else {
                $row["cSup360"] = 0;
                $row["sSup360"] = 0;
            }
        }
    } else {
        $row["cSup360"] = 0;
        $row["sSup360"] = 0;
    }
     
    $cantTotal0y30+=$cant0y30;
    $cantTotal31y60+=$cant31y60;
    $cantTotal61y90+=$cant61y90;
    $cantTotal91y180+=$cant91y180;
    $cantTotal181y360+=$cant181y360;
    $cantTotalSup360+=$cantSup360;

    $valorTotal0y30+=$val0y30;
    $valorTotal31y60+=$val31y60;
    $valorTotal61y90+=$val61y90;
    $valorTotal91y180+=$val91y180;
    $valorTotal181y360+=$val181y360;
    $valorTotalSup360+=$valSup360;

    $cantTotal = $cant0y30 + $cant31y60 + $cant61y90 + $cant91y180 + $cant181y360 + $cantSup360;
    $valorTotal = $val0y30 + $val31y60 + $val61y90 + $val91y180 + $val181y360 + $valSup360;

    $valorTotalCant+=$cantTotal;
    $valorTotalSaldo+=$valorTotal;

    $row["ctotal"] = $cantTotal;
    $row["stotal"] = $valorTotal;
    $report[$j] = $row;

    $j++;
}

array_multisort(array_column($report, 'stotal'), SORT_DESC, $report);

$i = 4;
foreach($report as $value){

    $objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':'.'D'.$i); 
    $objPHPExcel->getActiveSheet()->getStyle('B'.$i)
        ->getAlignment()
        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->setCellValue("B".$i, $value["entidad"]);

    $objPHPExcel->getActiveSheet()->setCellValue("E".$i, $value["c0y30"]); 
    $objPHPExcel->getActiveSheet()->setCellValue("F".$i, $value["s0y30"]);

    $objPHPExcel->getActiveSheet()->setCellValue("G".$i, $value["c31y60"]);
    $objPHPExcel->getActiveSheet()->setCellValue("H".$i, $value["s31y60"]);

    $objPHPExcel->getActiveSheet()->setCellValue("I".$i, $value["c61y90"]);
    $objPHPExcel->getActiveSheet()->setCellValue("J".$i, $value["s61y90"]);

    $objPHPExcel->getActiveSheet()->setCellValue("K".$i, $value["c91y180"]);
    $objPHPExcel->getActiveSheet()->setCellValue("L".$i, $value["s91y180"]);

    $objPHPExcel->getActiveSheet()->setCellValue("M".$i, $value["c181y360"]);
    $objPHPExcel->getActiveSheet()->setCellValue("N".$i, $value["s181y360"]);

    $objPHPExcel->getActiveSheet()->setCellValue("O".$i, $value["cSup360"]);
    $objPHPExcel->getActiveSheet()->setCellValue("P".$i, $value["sSup360"]);

    $objPHPExcel->getActiveSheet()->setCellValue("Q".$i, $value["ctotal"]); 
    $objPHPExcel->getActiveSheet()->setCellValue("R".$i, $value["stotal"]);
    
    $i++;
}


$objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':'.'D'.$i); 
$objPHPExcel->getActiveSheet()->getStyle('B'.$i.':'.'S'.$i)->getFont()->setBold( true );
$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, "TOTAL"); 
$objPHPExcel->getActiveSheet()->getStyle('B'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $cantTotal0y30);
$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $cantTotal31y60); 
$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $cantTotal61y90);
$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $cantTotal91y180); 
$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $cantTotal181y360); 
$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $cantTotalSup360); 
$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $valorTotalCant);

$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $valorTotal0y30);
$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $valorTotal31y60);
$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $valorTotal61y90);
$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $valorTotal91y180);
$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $valorTotal181y360);
$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $valorTotalSup360);
$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $valorTotalSaldo);


$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$i)->getNumberFormat()->setFormatCode('#,##0');
$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)->getNumberFormat()->setFormatCode('#,##0');
$objPHPExcel->getActiveSheet()->getStyle('J4:J'.$i)->getNumberFormat()->setFormatCode('#,##0');
$objPHPExcel->getActiveSheet()->getStyle('L4:L'.$i)->getNumberFormat()->setFormatCode('#,##0');
$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$i)->getNumberFormat()->setFormatCode('#,##0');
$objPHPExcel->getActiveSheet()->getStyle('P4:P'.$i)->getNumberFormat()->setFormatCode('#,##0');
$objPHPExcel->getActiveSheet()->getStyle('R4:R'.$i)->getNumberFormat()->setFormatCode('#,##0');

$objPHPExcel->getActiveSheet()->getStyle('Q4:Q'.$i)->getFont()->setBold( true );
$objPHPExcel->getActiveSheet()->getStyle('R4:R'.$i)->getFont()->setBold( true );

$objPHPExcel->getActiveSheet()->getStyle('E4:E'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('F4:F'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('G4:G'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('H4:H'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('I4:I'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('J4:J'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('K4:K'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('L4:L'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('M4:M'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('N4:N'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('O4:O'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('P4:P'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('Q4:Q'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getStyle('R4:R'.$i)
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


/*Hoja donde muestro el consolidado de incapacidades */

//primero nos traemos la data



$fieldConsolidado = "inc_id,emp_nombre,emd_cedula,emd_nombre,inc_fecha_inicio,inc_fecha_final,dias,inc_clasificacion,
inc_diagnostico,inc_origen,inc_valor_pagado_empresa,inc_valor_pagado_eps,inc_valor_pagado_ajuste,inc_fecha_pago_nomina,
inc_fecha_recepcion_d,inc_fecha_generada,inc_fecha_radicacion,inc_numero_radicado_v,inc_fecha_solicitud,inc_valor,  
inc_fecha_pago,inc_fecha_negacion_,set_descripcion_v,inc_estado_tramite,inc_fecha_estado,emd_codigo_nomina,emd_cargo,  
emd_salario,est_emp_desc_v,emd_fecha_ingreso,inc_ips_afiliado,emd_fecha_afiliacion_eps,inc_afp_afiliado,inc_arl_afiliado,med_nombre,med_num_registro,med_estado_rethus,  
enat_nombre_v,enat_nit_v,inc_pertenece_red,inc_tipo_generacion,atn_descripcion,creador,editor,inc_ruta_incapacidad,  
inc_ruta_incapacidad_transcrita,inc_registrada_entidad_i,inc_estado,mot_desc_v,emd_sede,emd_centro_costos,emd_subcentro_costos,  
emd_ciudad,mot_observacion_v, CASE WHEN DATEDIFF(DATE(NOW()), str_to_date($filtroFecha,'%d/%m/%Y')) < 30 THEN '0-30 Días' 
WHEN DATEDIFF(DATE(NOW()), str_to_date($filtroFecha,'%d/%m/%Y')) BETWEEN 31 AND 60 THEN '31-60 Días' 
WHEN DATEDIFF(DATE(NOW()), str_to_date($filtroFecha,'%d/%m/%Y')) BETWEEN 61 AND 90 THEN '61-90 Días' 
WHEN DATEDIFF(DATE(NOW()), str_to_date($filtroFecha,'%d/%m/%Y')) BETWEEN 91 AND 180 THEN '91-180 Días'
WHEN DATEDIFF(DATE(NOW()), str_to_date($filtroFecha,'%d/%m/%Y')) BETWEEN 181 AND 360 THEN '181-360 Días'
WHEN DATEDIFF(DATE(NOW()), str_to_date($filtroFecha,'%d/%m/%Y')) > 360 THEN 'SUP 360'END AS rango";

$whConsolidado = "";
if ($_POST["filtroEstadoTramite"] == 1) {
    $whConsolidado = "inc_estado_tramite = 'PAGADA' ";
} else if ($_POST["filtroEstadoTramite"] == 2) {
    $whConsolidado = "(inc_estado_tramite = 'POR RADICAR' OR inc_estado_tramite = 'RADICADA' OR inc_estado_tramite = 'SOLICITUD DE PAGO' OR inc_estado_tramite = 'RECHAZADA') ";
} else if ($_POST["filtroEstadoTramite"] == 3) {
    $whConsolidado = "inc_estado_tramite = 'NEGADA' ";
} else {
    $whConsolidado = "inc_estado_tramite != 'EMPRESA' ";
}

if ($_POST["yearFilter"] != 0) {
    $whConsolidado .= "AND YEAR(str_to_date($filtroFecha,'%d/%m/%Y')) = '".$_POST["yearFilter"]."' ";
}
if ($_SESSION['cliente_id'] != 0) {
    $whConsolidado .=  "AND inc_empresa = ".$_SESSION['cliente_id'];
}
$dataConsolidado = ModeloDAO::mdlMostrarGroupAndOrder($fieldConsolidado, "reporte_consolidado_inc", $whConsolidado);
// echo print_r($dataConsolidado);
//creamos la nueva hoja y mostramos la data
$newHoja = $objPHPExcel->createSheet(1);
$newHoja->setTitle("CONSOLIDADO");


$newHoja
    ->getStyle('A1:AY1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()
    ->getStyle('A1:AY1')
    ->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$newHoja->getStyle('A1:AY1')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

$newHoja->getStyle('A1:AY1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);  

$newHoja->getStyle('A1:AY1')->getFont()->setBold( true );

$newHoja->setCellValue("A1", "ID"); 
$newHoja->setCellValue("B1", "EMPRESA"); 
$newHoja->setCellValue("C1", "IDENTIFICACIÓN"); 
$newHoja->setCellValue("D1", "NOMBRE COLABORADOR"); 
$newHoja->setCellValue("E1", "FECHA INICIAL"); 
$newHoja->setCellValue("F1", "FECHA FINAL"); 
$newHoja->setCellValue("G1", "DÍAS"); 
$newHoja->setCellValue("H1", "CLASIFICACIÓN"); 
$newHoja->setCellValue("I1", "DIAGNOSTICO"); 
$newHoja->setCellValue("J1", "ORIGEN"); 
$newHoja->setCellValue("K1", "VALOR PAGADO EMPRESA"); 
$newHoja->setCellValue("L1", "VALOR PAGADO ENTIDAD"); 
$newHoja->setCellValue("M1", "VALOR PAGADO AJUSTE"); 
$newHoja->setCellValue("N1", "FECHA PAGO NOMINA"); 
$newHoja->setCellValue("O1", "FECHA RECEPCIÓN"); 
$newHoja->setCellValue("P1", "FECHA GENERACIÓN"); 
$newHoja->setCellValue("Q1", "FECHA RADICACIÓN"); 
$newHoja->setCellValue("R1", "NUMERO RADICADO");
$newHoja->setCellValue("S1", "FECHA SOLICITUD"); 
$newHoja->setCellValue("T1", "VALOR PAGADO"); 
$newHoja->setCellValue("U1", "FECHA DE PAGO"); 
$newHoja->setCellValue("V1", "FECHA NEGACION"); 
$newHoja->setCellValue("W1", "SUBESTADO"); 
$newHoja->setCellValue("X1", "ESTADO TRAMITE");  
$newHoja->setCellValue("Y1", "FECHA ESTADO"); 
$newHoja->setCellValue("Z1", "CODIGO NOMINA"); 
$newHoja->setCellValue("AA1", "CARGO");  
$newHoja->setCellValue("AB1", "SALARIO");
$newHoja->setCellValue("AC1", "ESTADO EMPLEADO");
$newHoja->setCellValue("AD1", "FECHA INGRESO"); 
$newHoja->setCellValue("AE1", "EPS");
$newHoja->setCellValue("AF1", "FECHA AFILIACION DE EPS"); 
$newHoja->setCellValue("AG1", "AFP");
$newHoja->setCellValue("AH1", "ARL");
$newHoja->setCellValue("AI1", "PROFESIONAL RESPONSABLE");
$newHoja->setCellValue("AJ1", "REGISTRO MEDICO");
$newHoja->setCellValue("AK1", "ESTADO RETHUS");
$newHoja->setCellValue("AL1", "ENTIDAD QUE GENERA");
$newHoja->setCellValue("AM1", "NIT ENTIDAD QUE GENERA");
$newHoja->setCellValue("AN1", "PERTENECE A LA RED");
$newHoja->setCellValue("AO1", "DOCUMENTO");
$newHoja->setCellValue("AP1", "ATENCIÓN");
$newHoja->setCellValue("AQ1", "CREADO POR");
$newHoja->setCellValue("AR1", "EDITADO POR");
$newHoja->setCellValue("AS1", "SOPORTE");
$newHoja->setCellValue("AT1", "TRANSCRITA");
$newHoja->setCellValue("AU1", "REGISTRADA EN ENTIDAD");
$newHoja->setCellValue("AV1", "ESTADO INCAPACIDAD");
$newHoja->setCellValue("AW1", "MOTIVO RECHAZO");
$newHoja->setCellValue("AX1", "OBSERVACIÓN");
$newHoja->setCellValue("AY1", "RANGO");

$i = 2;
foreach ($dataConsolidado as $value) {

    $newHoja->setCellValue("A".$i, $value["inc_id"]); 
    $newHoja->setCellValue("B".$i, $value["emp_nombre"]); 
    $newHoja->setCellValue("C".$i, $value["emd_cedula"]); 
    $newHoja->setCellValue("D".$i, $value["emd_nombre"]); 
    $newHoja->setCellValue("E".$i, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_inicio"])); 
    $newHoja->setCellValue("F".$i, \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_final"])); 
    $newHoja->setCellValue("G".$i, $value["dias"]); 
    $newHoja->setCellValue("H".$i, $value["inc_clasificacion"]); 
    $newHoja->setCellValue("I".$i, $value["inc_diagnostico"]); 
    $newHoja->setCellValue("J".$i, $value["inc_origen"]); 
    $newHoja->setCellValue("K".$i, $value["inc_valor_pagado_empresa"]); 
    $newHoja->setCellValue("L".$i, $value["inc_valor_pagado_eps"]); 
    $newHoja->setCellValue("M".$i, $value["inc_valor_pagado_ajuste"]); 
    $newHoja->setCellValue("N".$i, (!empty($value["inc_fecha_pago_nomina"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_pago_nomina"]):""); 
    $newHoja->setCellValue("O".$i, (!empty($value["inc_fecha_recepcion_d"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_recepcion_d"]):""); 
    $newHoja->setCellValue("P".$i, (!empty($value["inc_fecha_generada"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_generada"]):""); 
    $newHoja->setCellValue("Q".$i, (!empty($value["inc_fecha_radicacion"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_radicacion"]):""); 
    $newHoja->setCellValue("R".$i, $value["inc_numero_radicado_v"]);
    $newHoja->setCellValue("S".$i, (!empty($value["inc_fecha_solicitud"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_solicitud"]):""); 
    $newHoja->setCellValue("T".$i, $value["inc_valor"]); 
    $newHoja->setCellValue("U".$i, (!empty($value["inc_fecha_pago"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_pago"]):""); 
    $newHoja->setCellValue("V".$i, (!empty($value["inc_fecha_negacion_"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_fecha_negacion_"]):""); 
    $newHoja->setCellValue("W".$i, $value["set_descripcion_v"]); 
    $newHoja->setCellValue("X".$i, $value["inc_estado_tramite"]);  
    $newHoja->setCellValue("Y".$i, (!empty($value["inc_Fecha_estado"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["inc_Fecha_estado"]):""); 
    $newHoja->setCellValue("Z".$i, $value["emd_codigo_nomina"]); 
    $newHoja->setCellValue("AA".$i, $value["emd_cargo"]);  
    $newHoja->setCellValue("AB".$i, $value["emd_salario"]);
    $newHoja->setCellValue("AC".$i, $value["est_emp_desc_v"]);
    $newHoja->setCellValue("AD".$i, (!empty($value["emd_fecha_ingreso"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["emd_fecha_ingreso"]):"");
    $newHoja->setCellValue("AE".$i, $value["inc_ips_afiliado"]);
    $newHoja->setCellValue("AF".$i, (!empty($value["emd_fecha_afiliacion_eps"]))?\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value["emd_fecha_afiliacion_eps"]):"");
    $newHoja->setCellValue("AG".$i, $value["inc_afp_afiliado"]);
    $newHoja->setCellValue("AH".$i, $value["inc_arl_afiliado"]);
    $newHoja->setCellValue("AI".$i, $value["med_nombre"]);
    $newHoja->setCellValue("AJ".$i, $value["med_num_registro"]);
    $newHoja->setCellValue("AK".$i, $value["med_estado_rethus"]);
    $newHoja->setCellValue("AL".$i, $value["enat_nombre_v"]);
    $newHoja->setCellValue("AM".$i, $value["enat_nit_v"]);
    $newHoja->setCellValue("AN".$i, $value["inc_pertenece_red"]);
    $newHoja->setCellValue("AO".$i, $value["inc_tipo_generacion"]);
    $newHoja->setCellValue("AP".$i, $value["atn_descripcion"]);
    $newHoja->setCellValue("AQ".$i, $value["creador"]);
    $newHoja->setCellValue("AR".$i, $value["editor"]);
    $newHoja->setCellValue("AS".$i, $value["inc_ruta_incapacidad"]);
    $newHoja->setCellValue("AT".$i, $value["inc_ruta_incapacidad_transcrita"]);
    $newHoja->setCellValue("AU".$i, $value["inc_registrada_entidad_i"]);
    $newHoja->setCellValue("AV".$i, $value["inc_estado"]);
    $newHoja->setCellValue("AW".$i, $value["mot_desc_v"]);
    $newHoja->setCellValue("AX".$i, $value["mot_observacion_v"]);
    $newHoja->setCellValue("AY".$i, $value["rango"]);
    $i++;
}

$newHoja->getStyle("E2:E$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("F2:F$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("N2:N$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("O2:O$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("P2:P$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("Q2:Q$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("S2:S$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("U2:U$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("V2:V$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("Y2:Y$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("AD2:AD$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
$newHoja->getStyle("AF2:AF$i")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);

foreach(range('A','AY') as $columnID) {
    $newHoja->getColumnDimension($columnID)
        ->setAutoSize(true);

}


$nombreCliente = ModeloDAO::mdlMostrarUnitario("emp_nombre", "gi_empresa", "emp_id = $clienteId");
$fileName = "entidades_saldo_";
if (!empty($nombreCliente)) {
    $fileName.=strtolower(str_replace(' ', '_',$nombreCliente["emp_nombre"])).".xlsx";
} else {
    $fileName="informe_saldo_entidades.xlsx";
}

$objPHPExcel->setActiveSheetIndex(0);

//Write the Excel file to filename some_excel_file.xlsx in the current directory
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$writer = new Xlsx($objPHPExcel);
$writer->save('php://output');