<?php
session_start();
$_SESSION['start'] = time();
ini_set('display_errors', 'On');
ini_set('display_errors', 1);
require_once '../modelos/dao.modelo.php';
require_once '../modelos/tesoreria.modelo.php';

require_once '../controladores/mail.controlador.php';
require_once '../controladores/plantilla.controlador.php';
require_once '../controladores/incapacidades.controlador.php';
date_default_timezone_set('America/Bogota');

$month = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

if(isset($_POST['sendMailIncapacidades']) && $_POST['sendMailIncapacidades'] != ''){
	//Primero obtenemos los datos de la incapacidad
 	$campo_ = "gi_empresa.emp_direccion, gi_empresa.emp_nombre, gi_empresa.emp_nit, gi_empleados.emd_nombre, gi_empleados.emd_cedula, DATE_FORMAT(inc_fecha_inicio, '%d/%m/%Y') as inc_fecha_inicio, DATE_FORMAT(inc_fecha_final, '%d/%m/%Y') AS inc_fecha_final, gi_ips.ips_nombre, DATEDIFF(inc_fecha_final,inc_fecha_inicio) as total_dias  " ;
 	$tabla_ = " gi_incapacidad JOIN gi_empleados ON gi_empleados.emd_id = gi_incapacidad.inc_emd_id JOIN gi_empresa ON gi_empleados.emd_emp_id = gi_empresa.emp_id JOIN gi_ips ON gi_empleados.emd_eps_id = gi_ips.ips_id";
	$condi_ = " inc_id = ".$_POST['sendMailIncapacidades'];
	$resultado = ModeloTesoreria::mdlMostrarUnitario($campo_, $tabla_, $condi_ );

	if($resultado != false){
		$titulo = 'QUEJA REGIMEN CONTRIBUTIVO - '.mb_strtoupper($resultado['emd_nombre']);

		$mensaje = '
<html>
	<head>
		<title>QUEJA REGIMEN CONTRIBUTIVO - '.mb_strtoupper($resultado['emd_nombre']).'</title>
	</head>
	<body style="text-align:justify;">
		<p>Bogot&aacute; D.C., '.date('d').' de '.$month[date('m')-1].' de '.date('Y').'</p>
		<br />
		<br />
		<p>Señores</p>
		<p><b>Superintendencia Nacional de Salud</b></p>
		<br />
	
		<p>Asunto: SOLICITUD - R&Eacute;GIMEN CONTRIBUTIVO - '.mb_strtoupper($resultado['ips_nombre']).'</P>
		<br />
	
		<p>PETICI&Oacute;N: SOLICITUD DE PAGO DE INCAPACIDAD Y PAGO DE INTERESES MORATORIOS (ART. 24 DEL DECRETO 4023 DE 2011)</p>
		<br />
	
		<p>Por medio de la presente yo '.mb_strtoupper($resultado['emd_nombre']).', identificado con CC. '.$resultado['emd_cedula'].', en calidad de empleado de la empresa '.mb_strtoupper($resultado['emp_nombre']).' identificada con NIT. '.$resultado['emp_nit'].', solicitamos a '.mb_strtoupper($resultado['ips_nombre']).' el pago de mis incapacidades y pago de intereses moratorios, debido a que existe demora injustificada con el pago de las incapacidades, incumpliendo así con su deber de pago oportuno establecido en el Decreto 4023 de 2011.</p>
		<p>Detalle de incapacidades:</p>
		<br />
		<table border="1" width="35%" style="border:solid 1px;">
		 	<tr style="border:solid 1px;">
				<th align="center">FECHA INICIO</th>
				<th align="center">FECHA FINAL</th>
				<th align="center">DÍAS</th>
            </tr>
            <tr style="border:solid 1px;">
				<td align="center">'.$resultado['inc_fecha_inicio'].'</td>
				<td align="center">'.$resultado['inc_fecha_final'].'</td>
				<td align="center">'.($resultado['total_dias']+1).'</td>
            </tr>
		</table>
		<br />
		<br />
		<br />
		<p>Atentamente,</p>
		<br />
		<br />
		<p>
			'.mb_strtoupper($resultado['emd_nombre']).'
			<br/>
			C&eacute;dula de Ciudadan&iacute;a:  '.$resultado['emd_cedula'].'
			<br/>
			'.$resultado['emp_direccion'].'
			<br />
			COLOMBIA
			<br />
			Tel. 3225973028
		</p>
		<p>peticionesincapaciades@gmail.com</p>
	</body>
</html>';

		$correoSuper = ModeloTesoreria::mdlMostrarUnitario('correo_v', 'gi_supersalud', '1 = 1' );

        $ctrMail= new ctrMail();
        $resultadoMail =  $ctrMail->EnviarMailSuperSalud('Peticiones incapacidades', $titulo, $mensaje, $correoSuper['correo_v'], NULL , null);
        if($resultadoMail != 'ok'){
        	echo json_encode(array('code' =>0, 'message' => 'No se enviar el correo por esto => '.$resultadoMail));
        }else{
        	echo json_encode(array('code' =>1, 'message' =>'Enviado'));
        }
	}
}