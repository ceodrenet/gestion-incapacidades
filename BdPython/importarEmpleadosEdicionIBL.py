import pandas as pd
import mysql.connector
import sys

def validarUsuario(emd_cedula, empresa):
    try:
        mi_conexion = mysql.connector.connect(host='localhost', user='incapacidades', passwd='1143231494ABc_', db='incapacidades')
        try:
            conx = mi_conexion.cursor()
            conx.execute("SELECT emd_id, emd_nombre FROM gi_empleados WHERE emd_cedula='"+emd_cedula+"' and emd_emp_id = "+empresa)
            for empleados in conx:
                return empleados[0]
            return 0
        finally:
            mi_conexion.close()
    except (mysql.connector.custom_error_exception()):
        print("Ocurrio un eror en la conexion")


def devolverId(ips_codigo):
    try:
        mi_conexion = mysql.connector.connect(host='localhost', user='incapacidades', passwd='1143231494ABc_', db='incapacidades')
        try:
            con = mi_conexion.cursor()
            con.execute(f"SELECT ips_id, ips_nombre FROM gi_ips WHERE ips_codigo='{ips_codigo}'")
            for ips in con:
                return ips[0]
        finally:
            mi_conexion.close()
    except (mysql.connector.custom_error_exception()):
        print("Ocurrio un eror en la conexion")


def is_empty(a):
    return a == set()

def cargarArchivo(archivo, empresa):
    file = archivo
    # Load the xlsx file
    excel_data = pd.read_excel(file)
    # Read the values of the file in the dataframe
    data = pd.DataFrame(excel_data)

    data['Fecha Afiliacion EPS'] = pd.to_datetime(data['Fecha Afiliacion EPS']).dt.date
    data['Fecha Retiro'] = pd.to_datetime(data['Fecha Retiro']).dt.date
    data['Fecha Nacimiento'] = pd.to_datetime(data['Fecha Nacimiento']).dt.date
    data['Fecha Ingreso'] = pd.to_datetime(data['Fecha Ingreso']).dt.date

    for i in data.itertuples():
        updateValues = "UPDATE gi_empleados SET emd_emp_id = "+empresa
        whereUpdate = " WHERE emd_id = "

        if i[1] != '' and str(i[1]) != 'nan':
            updateValues += ", emd_cedula = '" + str(i[1]) + "'"

        if i[2] != '' and str(i[2]) != 'nan':
            updateValues += ", emd_nombre = '" + str(i[2]) + "'"

        if i[3] != '' and str(i[3]) != 'nan':
            updateValues += ", emd_salario_promedio = '" + str(i[3]) + "'"

        if i[4] != '' and str(i[4]) != 'nan':
            updateValues += ", emd_salario = '" + str(i[6]) + "'"

        if i[5] != '' and str(i[5]) != 'nan':
            eps = devolverId(i[5])
            updateValues += ", emd_eps_id = '" + str(eps) + "'"

        if i[6] != '' and str(i[6]) != 'NaT':
            updateValues += ", emd_fecha_afiliacion_eps = '" + str(i[6]) + "'"

        if i[7] != '' and str(i[7]) != 'NaT':
            updateValues += ", emd_fecha_retiro = '" + str(i[7]) + "'"

        if i[8] != '' and str(i[8]) != 'nan':
            estado = 1
            if (i[8] != 'ACTIVO'):
                estado = 2
            updateValues += ", emd_estado = '" + str(estado) + "'"
    
        if i[9] != '' and str(i[9]) != 'NaT':
            updateValues += ", emd_fecha_nacimiento = '" + str(i[9]) + "'"
        
        if i[10] != '' and str(i[10]) != 'NaT':
            updateValues += ", emd_fecha_ingreso = '" + str(i[10]) + "'"
        
        if i[11] != '' and str(i[11]) != 'nan':
            updateValues += ", emd_cargo = '" + str(i[11]) + "'"
        
        if i[12] != '' and str(i[12]) != 'nan':
            updateValues += ", emd_sede = '" + str(i[12]) + "'"
        
        if i[13] != '' and str(i[13]) != 'nan':
            updateValues += ", emd_ciudad = '" + str(i[13]) + "'"

        if i[14] != '' and str(i[14]) != 'nan':
            updateValues += ", emd_codigo_nomina = '" + str(i[14]) + "'"
        
        if i[15] != '' and str(i[15]) != 'nan':
            updateValues += ", emd_centro_costos = '" + str(i[15]) + "'"
        
        if i[16] != '' and str(i[16]) != 'nan':
            updateValues += ", emd_subcentro_costos = '" + str(i[16]) + "'"

        validar = validarUsuario(str(i[1]), empresa)
        mi_conexion = mysql.connector.connect(host='localhost', user='incapacidades', passwd='1143231494ABc_', db='incapacidades')
        con = mi_conexion.cursor()
        if validar != 0:
            whereUpdate += str(validar)
            #print(updateValues + whereUpdate)
            #print(str(i[0]))
            con.execute(updateValues + whereUpdate)

        mi_conexion.commit()
    mi_conexion.close()

cargarArchivo('/var/www/html/BdPython/'+sys.argv[1], sys.argv[2])