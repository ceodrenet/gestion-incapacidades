import xlsxwriter
import mysql.connector
from datetime import datetime
import re
import sys, json

def truncate(num):
    return re.sub(r'^(\d+\.\d{,0})\d*$',r'\1',str(num))

def fetch_table_data(empresa_id):
    # The connect() constructor creates a connection to the MySQL server and returns a MySQLConnection object.
    cnx = mysql.connector.connect(
        host='localhost',
        database='incapacidades',
        user='business_intel',
        password='B6Z7vwrBMobavfhX'
    )

    cursor = cnx.cursor()
    script = " SELECT  " \
             "inc_id as ID, " \
             "emp_nombre as EMPRESA," \
             "emd_cedula as 'IDENTIFICACIÓN' ," \
             "emd_nombre as 'NOMBRE COLABORADOR' ," \
             "inc_fecha_inicio as 'FECHA INICIAL'," \
             "inc_fecha_final as 'FECHA FINAL'," \
             "dias as 'DÍAS'," \
             "inc_clasificacion as 'CLASIFICACIÓN' ," \
             "inc_diagnostico AS DIAGNOSTICO ," \
             "inc_origen AS ORIGEN ," \
             "inc_valor_pagado_empresa AS 'VALOR PAGADO EMPRESA'," \
             "inc_valor_pagado_eps AS 'VALOR PAGADO ENTIDAD' ," \
             "inc_valor_pagado_ajuste AS 'VALOR PAGADO AJUSTE' ," \
             "inc_fecha_pago_nomina AS 'FECHA PAGO NOMINA' ," \
             "inc_fecha_recepcion_d AS 'FECHA RECEPCIÓN' ," \
             "inc_fecha_generada AS 'FECHA GENERACIÓN'," \
             "inc_fecha_radicacion AS 'FECHA RADICACIÓN' ," \
             "inc_numero_radicado_v AS 'NUMERO RADICADO', " \
             "inc_fecha_solicitud AS 'FECHA SOLICITUD',  " \
             "inc_valor AS 'VALOR PAGADO' ,  " \
             "inc_fecha_pago AS 'FECHA DE PAGO',  " \
             "inc_fecha_negacion_ AS 'FECHA NEGACION',  " \
             "set_descripcion_v AS 'SUBESTADO',  " \
             "inc_estado_tramite AS 'ESTADO TRAMITE',  " \
             "inc_Fecha_estado AS 'FECHA ESTADO',  " \
             "emd_codigo_nomina AS 'CODIGO NOMINA',  " \
             "emd_cargo AS CARGO,  " \
             "emd_salario AS SALARIO,  " \
             "est_emp_desc_v AS 'ESTADO EMPLEADO',  " \
             "inc_ips_afiliado AS EPS ,  " \
             "inc_afp_afiliado AS AFP,  " \
             "inc_arl_afiliado AS ARL,  " \
             "inc_profesional_responsable AS 'PROFESIONAL RESPONSABLE',  " \
             "inc_prof_registro_medico AS 'REGISTRO MEDICO',  " \
             "inc_donde_se_genera AS 'ENTIDAD QUE GENERA',  " \
             "inc_tipo_generacion AS DOCUMENTO,  " \
             "atn_descripcion AS 'ATENCIÓN',  " \
             "creador AS 'CREADO POR' ,  " \
             "editor AS 'EDITADO POR' ,  " \
             "inc_ruta_incapacidad AS 'SOPORTE',  " \
             "inc_ruta_incapacidad_transcrita AS 'TRANSCRITA' ,  " \
             "inc_registrada_entidad_i AS 'REGISTRADA EN ENTIDAD' ,  " \
             "inc_estado AS 'ESTADO INCAPACIDAD',  " \
             "mot_desc_v AS 'MOTIVO RECHAZO',  " \
             "emd_sede AS SEDE,  " \
             "emd_centro_costos AS 'CENTRO DE COSTO',  " \
             "emd_subcentro_costos AS 'SUBCENTRO DE COSTO',  " \
             "emd_ciudad AS CIUDAD ,  " \
             "mot_observacion_v AS 'OBSERVACIÓN' " \
             " FROM reporte_consolidado_inc " \
             " WHERE inc_empresa = "+empresa_id+ " AND inc_estado = 'INCOMPLETA'" 

    cursor.execute(script)

    header = [row[0] for row in cursor.description]

    rows = cursor.fetchall()

    # Closing connection
    cnx.close()

    return header, rows


def export(idSession, empresa_id):
    # Create an new Excel file and add a worksheet.
    workbook = xlsxwriter.Workbook('/var/www/html/BdPython/Consolidado_incapacidades_incompletas_'+empresa_id+'_'+idSession+'.xlsx')
    worksheet = workbook.add_worksheet('CONSOLIDADO')

    # Create style for cells
    header_cell_format = workbook.add_format({'bg_color': '#FF0000', 'font_color': '#FFFFFF', 'bold': 1,'border': 1, 'align': 'center','valign': 'vcenter'})
    body_cell_format = workbook.add_format({'border': True})
    format2 = workbook.add_format({'num_format': 'dd/mm/yyyy', 'border': True})
    currency_format = workbook.add_format({'num_format': '$#,##0', 'border': True})
   

    header, rows = fetch_table_data(empresa_id)

    row_index = 0
    column_index = 0

    for column_name in header:
        worksheet.write(row_index, column_index, column_name, header_cell_format)
        column_index += 1

    row_index += 1
    for row in rows:
        column_index = 0
        for column in row:
            if column_index == 4 or column_index == 5 or column_index == 13 or column_index == 14 or column_index == 15 or column_index == 16 or column_index == 18 or column_index == 20 or column_index == 21 or column_index == 24:
                if column is None:
                    worksheet.write(row_index, column_index, " ", body_cell_format)
                else:
                    date_time_obj = datetime.strptime(column, '%d/%m/%Y')
                    worksheet.write(row_index, column_index, date_time_obj, format2)
            else:
                if column_index == 10 or column_index == 11 or column_index == 12 or column_index == 19 or column_index == 27:
                    if column is None or column == '':
                        worksheet.write(row_index, column_index, 0, body_cell_format)
                    else:
                        number = truncate(column)
                        worksheet.write(row_index, column_index, int(number.replace('.', '').replace(',', '')), currency_format)
                elif column_index == 2 :
                    worksheet.write(row_index, column_index, int(column), body_cell_format)
                else:
                    worksheet.write(row_index, column_index, column, body_cell_format)

            column_index += 1
        row_index += 1

    print(str(row_index) + ' rows written successfully to ' + workbook.filename)

    # Closing workbook
    workbook.close()


# Tables to be exported
export(sys.argv[1], sys.argv[2])

