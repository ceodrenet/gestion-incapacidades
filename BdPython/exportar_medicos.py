import xlsxwriter
import mysql.connector
from datetime import datetime
import sys

def fecth_data_table():
    cnx = mysql.connector.connect(
        host='localhost',
        database='incapacidades',
        user='incapacidades',
        password='1143231494ABc_'
    )

    cursor = cnx.cursor()

    script = "SELECT " \
    "med_id AS 'ID' ," \
    "med_nombre AS 'NOMBRE' ," \
    "med_identificacion AS 'IDENTIFICACIÓN' ," \
    "med_num_registro AS 'REGISTRO MÉDICO' ," \
    "med_estado_rethus AS 'ESTADO RETHUS' ," \
    "med_estado AS 'ESTADO' ," \
    "med_fecha_creacion AS 'FECHA DE CREACIÓN' ," \
    "med_usuario_crea AS 'CREADO POR' ," \
    "med_fecha_edicion AS 'FECHA DE EDICIÓN' ," \
    "med_usuario_edita AS 'EDITADO POR' " \
    "FROM reporte_medicos"
    
    cursor.execute(script)
    header = [row[0] for row in cursor.description]
    rows = cursor.fetchall()
    cnx.close()

    return header, rows

def export(id_session):
    workbook = xlsxwriter.Workbook('/var/www/html/BdPython/Exportar_Medicos_'+id_session+'.xlsx')
    worksheet = workbook.add_worksheet('MEDICOS')

    header_cell_format = workbook.add_format({'bg_color': '#FF0000', 'font_color': '#FFFFFF', 'bold': 1,'border': 1, 'align': 'center','valign': 'vcenter'})
    body_cell_format = workbook.add_format({'border': True})
    format2 = workbook.add_format({'num_format': 'dd/mm/yyyy', 'border': True})
   
    header, rows = fecth_data_table()

    row_index = 0
    column_index = 0

    for column_name in header:
        worksheet.write(row_index, column_index, column_name, header_cell_format)
        column_index += 1
    
    row_index += 1

    for row in rows:
        column_index = 0
        for column in row:
            if column_index == 6 or column_index == 8:
                if column is None:
                    worksheet.write(row_index, column_index, " ", body_cell_format)
                else:
                    date_time_obj = datetime.strptime(column, '%d/%m/%Y')
                    worksheet.write(row_index, column_index, date_time_obj, format2)
            else:
                worksheet.write(row_index, column_index, column, body_cell_format)
            
            column_index += 1
        row_index += 1

    print(str(row_index) + ' rows written successfully to ' + workbook.filename)
    workbook.close()

export(sys.argv[1]) 