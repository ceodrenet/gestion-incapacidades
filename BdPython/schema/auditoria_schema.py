from pydantic import BaseModel
from typing import Optional

class AuditoriaSchema(BaseModel):
    aud_id_i: Optional[str]
    aud_id_usuario_i: str
    aud_fecha_d: str
    aud_hora_d: str
    aud_controlador_v: str
    aud_metodo_v: str
    aud_ip_i: str
    aud_navegador_v: str
    aud_data_t: str
    aud_id_session:str




    