import pandas as pd
import mysql.connector
import sys
from datetime import datetime

def validarIncapacidad(emd_cedula, fecha_inicio, empresa):
    try:
        mi_conexion = mysql.connector.connect(host='localhost', user='incapacidades', passwd='1143231494ABc_', db='incapacidades')
        try:
            conx = mi_conexion.cursor()
            conx.execute("SELECT inc_id , inc_cc_afiliado FROM gi_incapacidad WHERE inc_cc_afiliado='"+emd_cedula+"' AND inc_empresa = "+empresa+" AND inc_fecha_inicio = '"+fecha_inicio+"'")
            for incapacidad in conx:
                return incapacidad[0]
            return 0
        finally:
            mi_conexion.close()
    except (mysql.connector.custom_error_exception()):
        print("Ocurrio un error en la conexion")

def getDatosUsuario(emd_cedula, empresa):
    try:
        mi_conexion = mysql.connector.connect(host='localhost', user='incapacidades', passwd='1143231494ABc_', db='incapacidades')
        try:
            conx = mi_conexion.cursor()
            conx.execute("SELECT emd_id , emd_eps_id, emd_afp_id, emd_arl_id FROM gi_empleados WHERE emd_cedula='"+emd_cedula+"' and emd_emp_id = "+empresa)
            for empleados in conx:
                return empleados
            return 0
        finally:
            mi_conexion.close()
    except (mysql.connector.custom_error_exception()):
        print("Ocurrio un error en la conexion")

def cargarArchivo(file, empresa):
    file =file
    # Load the xlsx file
    excel_data = pd.read_excel(file)
    # Read the values of the file in the dataframe
    data = pd.DataFrame(excel_data)
    for i in data.itertuples():
        insertInto = "insert into gi_incapacidad("
        insertValues = ") values ("
        updateValues = "UPDATE gi_incapacidad SET "
        whereUpdate = " WHERE inc_id = "

        now = datetime.now()
        format = now.strftime('%Y-%m-%d %H:%M:%S')

        insertInto += " inc_empresa, inc_fecha_generada "
        insertValues += empresa+", '"+format+"' "
        updateValues += " inc_fecha_edicion = '"+ format +"' "


        datosEmpleado = getDatosUsuario(str(i[1]), empresa)
        if (datosEmpleado != 0):
            if i[1] != '' and str(i[1]) != 'nan':
                insertInto += ", inc_cc_afiliado"
                insertValues += ", '" + str(i[1]) + "'"
                updateValues += ", inc_cc_afiliado = '" + str(i[1]) + "'"

            if i[3] != '' and str(i[3]) != 'nan':
                insertInto += ", inc_fecha_inicio"
                insertValues += ", '" + str(i[3]) + "'"
                updateValues += ", inc_fecha_inicio = '" + str(i[3]) + "'"

            if i[4] != '' and str(i[4]) != 'nan':
                insertInto += ", inc_fecha_final"
                insertValues += ", '" + str(i[4]) + "'"
                updateValues += ", inc_fecha_final = '" + str(i[4]) + "'"

            if i[6] != '' and str(i[6]) != 'nan':
                insertInto += ", inc_origen"
                insertValues += ", '" + str(i[6]) + "'"
                updateValues += ", inc_origen = '" + str(i[6]) + "'"

            if i[7] != '' and str(i[7]) != 'nan':
                insertInto += ", inc_diagnostico"
                insertValues += ", '" + str(i[7]) + "'"
                updateValues += ", inc_diagnostico = '" + str(i[7]) + "'"

            if i[8] != '' and str(i[8]) != 'nan':
                insertInto += ", inc_tipo_generacion"
                insertValues += ", '" + str(i[8]) + "'"
                updateValues += ", inc_tipo_generacion = '" + str(i[8]) + "'"

            if i[9] != '' and str(i[9]) != 'nan':
                insertInto += ", inc_clasificacion"
                insertValues += ", '" + str(i[9]) + "'"
                updateValues += ", inc_clasificacion = '" + str(i[9]) + "'"

            if i[10] != '' and str(i[10]) != 'nan':
                insertInto += ", inc_valor_pagado_empresa"
                insertValues += ", '" + str(i[10]) + "'"
                updateValues += ", inc_valor_pagado_empresa = '"+ str(+ i[10]) + "'"

            if i[11] != '' and str(i[11]) != 'nan':
                insertInto += ", inc_valor_pagado_eps"
                insertValues += ", '" + str(i[11]) + "'"
                updateValues += ", inc_valor_pagado_eps = '" + str(i[11]) + "'"

            if i[12] != '' and str(i[12]) != 'nan':
                insertInto += ", inc_valor_pagado_ajuste"
                insertValues += ", '" + str(i[12]) + "'"
                updateValues += ", inc_valor_pagado_ajuste = '" + str(i[12]) + "'"

            if i[13] != '' and str(i[13]) != 'nan':
                insertInto += ", inc_ivl_v"
                insertValues += ", '" + str(i[13]) + "'"
                updateValues += ", inc_ivl_v = '" + str(i[13]) + "'"

            if i[14] != '' and str(i[14]) != 'nan':
                insertInto += ", inc_fecha_pago_nomina"
                insertValues += ", '" + str(i[14]) + "'"
                updateValues += ", inc_fecha_pago_nomina = '" + str(i[14]) + "'"

            if i[16] != '' and str(i[16]) != 'nan':
                insertInto += ", inc_estado_tramite"
                insertValues += ", '" + str(i[16]) + "'"
                updateValues += ", inc_estado_tramite = '" + str(i[16]) + "'"

            if i[17] != '' and str(i[17]) != 'nan':
                insertInto += ", inc_sub_estado_i"
                insertValues += ", '" + str(i[17]) + "'"
                updateValues += ", inc_sub_estado_i = '" + str(i[17]) + "'"

            if i[19] != '' and str(i[19]) != 'nan':
                if i[16] == 3:
                    insertInto += ", inc_fecha_radicacion"
                    insertValues += ", '" + str(i[19]) + "'"
                    updateValues += ", inc_fecha_radicacion = '" + str(i[19]) + "'"
                
                if i[16] == 4:
                    insertInto += ", inc_fecha_solicitud"
                    insertValues += ", '" + str(i[19]) + "'"
                    updateValues += ", inc_fecha_solicitud = '" + str(i[19]) + "'"
                
                if i[16] == 5:
                    insertInto += ", inc_fecha_pago"
                    insertValues += ", '" + str(i[19]) + "'"
                    updateValues += ", inc_fecha_pago = '" + str(i[19]) + "'"
                
                if i[16] == 6:
                    insertInto += ", inc_fecha_negacion_"
                    insertValues += ", '" + str(i[19]) + "'"
                    updateValues += ", inc_fecha_negacion_ = '" + str(i[19]) + "'"

            #Falta el tema de la fecha cambio estado

            if i[18] != '' and str(i[18]) != 'nan':
                insertInto += ", inc_valor"
                insertValues += ", '" + str(i[18]) + "'"
                updateValues += ", inc_valor = '" + str(i[18]) + "'"

            if i[20] != '' and str(i[20]) != 'nan':
                insertInto += ", inc_numero_radicado_v"
                insertValues += ", '" + str(i[20]) + "'"
                updateValues += ", inc_numero_radicado_v = '" + str(i[20]) + "'"

            if i[21] != '' and str(i[21]) != 'nan':
                insertInto += ", inc_fecha_recepcion_d"
                insertValues += ", '" + str(i[21]) + "'"
                updateValues += ", inc_fecha_recepcion_d = '" + str(i[21]) + "'"

            if i[22] != '' and str(i[22]) != 'nan':
                insertInto += ", inc_registrada_entidad_i"
                insertValues += ", '" + str(i[22]) + "'"
                updateValues += ", inc_registrada_entidad_i = '" + str(i[22]) + "'"

           # campo adicionado en archivo excel para cargar info de tabla atencion RFB 31-10-2022
            if i[23] != '' and str(i[23]) != 'nan':
                insertInto += ", inc_atn_id"
                insertValues += ", '" + str(i[23]) + "'"
                updateValues += ", inc_atn_id = '" + str(i[23]) + "'"

            if i[24] != '' and str(i[24]) != 'nan':
                insertInto += ", inc_estado"
                insertValues += ", '" + str(i[24]) + "'"
                updateValues += ", inc_estado = '" + str(i[24]) + "'"


            if datosEmpleado[0] != None:
                insertInto += ", inc_emd_id"
                insertValues += ", '" + str(datosEmpleado[0]) + "' "
                updateValues += ", inc_emd_id = '" + str(datosEmpleado[0]) + "' "

            if datosEmpleado[1] != None:
                insertInto += ", inc_ips_afiliado"
                insertValues += ", '" + str(datosEmpleado[1]) + "' "
                #updateValues += ", inc_ips_afiliado = '"+str(datosEmpleado[1])+"' "

            if datosEmpleado[2] != None:
                insertInto += ", inc_afp_afiliado"
                insertValues += ", '" + str(datosEmpleado[2]) + "' "
                #updateValues += ", inc_afp_afiliado = '"+str(datosEmpleado[2])+"' "

            if datosEmpleado[3] != None:
                insertInto += ", inc_arl_afiliado"
                insertValues += ", '" + str(datosEmpleado[3]) + "' "
                #updateValues += ", inc_arl_afiliado = '"+ str(datosEmpleado[3]) +"' "


            # print(insertInto + insertValues + ")")
            validar = validarIncapacidad(str(i[1]), str(i[3]), empresa)
            mi_conexion = mysql.connector.connect(host='localhost', user='incapacidades', passwd='1143231494ABc_', db='incapacidades')
            con = mi_conexion.cursor()
            if validar == 0:
                con.execute(insertInto + insertValues + ")")
            else:
                whereUpdate += str(validar)
                print(updateValues + whereUpdate)
                con.execute(updateValues + whereUpdate)

            mi_conexion.commit()
            mi_conexion.close()
        else:
            print(f"Empleado {str(i[1])} no existe")


cargarArchivo('/var/www/html/BdPython/'+sys.argv[1], sys.argv[2])