from sqlite3 import connect
from string import whitespace
from fastapi import APIRouter
from schema.auditoria_schema import AuditoriaSchema
from config.coneccion import engine
from model.model_auditoria import auditoria


user = APIRouter()

@user.get("/")
def root():
    return{"message": "Hi,I am fastAPI with a router"}


@user.post("/api/user") 
def create_auditoria(data_audi:AuditoriaSchema):
    with engine.connect() as conn:
        new_user = data_audi.dict()
        conn.execute(auditoria.insert().values(new_user))

        return "Success"
   # print(data_audi)

# @user.put("/api/user") 
# def update_auditoria():
#     pass
