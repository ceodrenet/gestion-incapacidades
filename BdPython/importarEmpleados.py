import pandas as pd
import mysql.connector
import sys

def validarUsuario(emd_cedula, empresa):
    try:
        mi_conexion = mysql.connector.connect(host='localhost', user='incapacidades', passwd='1143231494ABc_', db='incapacidades')
        try:
            conx = mi_conexion.cursor()
            conx.execute("SELECT emd_id, emd_nombre FROM gi_empleados WHERE emd_cedula='"+emd_cedula+"' and emd_emp_id = "+empresa)
            for empleados in conx:
                return empleados[0]
            return 0
        finally:
            mi_conexion.close()
    except (mysql.connector.custom_error_exception()):
        print("Ocurrio un eror en la conexion")


def devolverId(ips_codigo):
    try:
        mi_conexion = mysql.connector.connect(host='localhost', user='incapacidades', passwd='1143231494ABc_', db='incapacidades')
        try:
            con = mi_conexion.cursor()
            con.execute(f"SELECT ips_id, ips_nombre FROM gi_ips WHERE ips_codigo='{ips_codigo}'")
            for ips in con:
                return ips[0]
        finally:
            mi_conexion.close()
    except (mysql.connector.custom_error_exception()):
        print("Ocurrio un eror en la conexion")


def is_empty(a):
    return a == set()

def cargarArchivo(file, empresa, insertarCheck):
    file = file
    # Load the xlsx file
    excel_data = pd.read_excel(file)
    # Read the values of the file in the dataframe
    data = pd.DataFrame(excel_data)

    for i in data.itertuples():
        insertInto = "insert into gi_empleados(emd_emp_id"
        insertValues = ") values ("+empresa

        updateValues = "UPDATE gi_empleados SET emd_emp_id = "+empresa
        whereUpdate = " WHERE emd_id = "

        if i[2] != '' and str(i[2]) != 'nan':
            insertInto += ",emd_tipo_identificacion "
            insertValues += ",'" + str(i[2]) + "' "
            updateValues += ",emd_tipo_identificacion = '" + str(i[2]) + "'"

        if i[3] != '' and str(i[3]) != 'nan':
            insertInto += ",emd_cedula"
            insertValues += ",'" + str(i[3]) + "'"
            updateValues += ",emd_cedula = '" + str(i[3]) + "'"

        if i[4] != '' and str(i[4]) != 'nan':
            insertInto += ",emd_nombre"
            insertValues += ",'" + str(i[4]) + "'"
            updateValues += ",emd_nombre = '" + str(i[4]) + "'"

        if i[5] != '' and str(i[5]) != 'nan':
            insertInto += ",emd_fecha_nacimiento"
            insertValues += ",'" + str(i[5]) + "'"
            updateValues += ",emd_fecha_nacimiento = '" + str(i[5]) + "'"

        if i[6] != '' and str(i[6]) != 'nan':
            insertInto += ",emd_fecha_ingreso"
            insertValues += ",'" + str(i[6]) + "'"
            updateValues += ",emd_fecha_ingreso = '" + str(i[6]) + "'"

        if i[7] != '' and str(i[7]) != 'nan':
            insertInto += ",emd_fecha_retiro"
            insertValues += ",'" + str(i[7]) + "'"
            updateValues += ",emd_fecha_retiro = '" + str(i[7]) + "'"

        if i[8] != '' and str(i[8]) != 'nan':
            insertInto += ",emd_salario"
            insertValues += ",'" + str(i[8]) + "'"
            updateValues += ",emd_salario = '" + str(i[8]) + "'"

        if i[9] != '' and str(i[9]) != 'nan':
            insertInto += ",emd_salario_promedio"
            insertValues += ",'" + str(i[9]) + "'"
            updateValues += ",emd_salario_promedio = '"+ str(+ i[9]) + "'"

        if i[10] != '' and str(i[10]) != 'nan':
            insertInto += ",emd_cargo"
            insertValues += ",'" + str(i[10]).replace("'", '') + "'"
            updateValues += ",emd_cargo = '" + str(i[10]).replace("'", '') + "'"

        if i[11] != '' and str(i[11]) != 'nan':
            insertInto += ",emd_sede"
            insertValues += ",'" + str(i[11]) + "'"
            updateValues += ",emd_sede = '" + str(i[11]) + "'"

        if i[12] != '' and str(i[12]) != 'nan':
            eps = devolverId(i[12])
            insertInto += ",emd_eps_id"
            insertValues += ",'" + str(eps) + "'"
            updateValues += ",emd_eps_id = '" + str(eps) + "'"

        if i[13] != '' and str(i[13]) != 'nan':
            insertInto += ",emd_fecha_afiliacion_eps"
            insertValues += ",'" + str(i[13]) + "'"
            updateValues += ",emd_fecha_afiliacion_eps = '" + str(i[13]) + "'"

        if i[14] != '' and str(i[14]) != 'nan':
            afp = devolverId(i[14])
            insertInto += ",emd_afp_id"
            insertValues += ",'" + str(afp) + "'"
            updateValues += ",emd_afp_id = '" + str(afp) + "'"

        if i[15] != '' and str(i[15]) != 'nan':
            arl = devolverId(i[15])
            insertInto += ",emd_arl_id"
            insertValues += ",'" + str(arl) + "'"
            updateValues += ",emd_arl_id = '" + str(arl) + "'"

        if i[16] != '' and str(i[16]) != 'nan':
            insertInto += ",emd_genero"
            insertValues += ",'" + str(i[16]) + "'"
            updateValues += ",emd_genero = '" + str(i[16]) + "'"

        if i[17] != '' and str(i[17]) != 'nan':
            insertInto += ",emd_tipo_empleado"
            insertValues += ",'" + str(i[17]) + "'"
            updateValues += ",emd_tipo_empleado = '" + str(i[17]) + "'"

        if i[18] != '' and str(i[18]) != 'nan':
            insertInto += ",emd_codigo_nomina"
            insertValues += ",'" + str(i[18]) + "'"
            updateValues += ",emd_codigo_nomina = '" + str(i[18]) + "'"

        if i[19] != '' and str(i[19]) != 'nan':
            insertInto += ",emd_centro_costos"
            insertValues += ",'" + str(i[19]) + "'"
            updateValues += ",emd_centro_costos = '" + str(i[19]) + "'"

        if i[20] != '' and str(i[20]) != 'nan':
            insertInto += ",emd_ciudad"
            insertValues += ",'" + str(i[20]).replace(',', '') + "'"
            updateValues += ",emd_ciudad = '" + str(i[20]).replace(',', '') + "'"

        if i[21] != '' and str(i[21]) != 'nan':
            insertInto += ",emd_subcentro_costos"
            insertValues += ",'" + str(i[21]).replace(',', '') + "'"
            updateValues += ",emd_subcentro_costos = '" + str(i[21]).replace(',', '') + "'"

        if i[22] != '' and str(i[22]) != 'nan':
            estado = 1
            if(i[22] != 'ACTIVO'):
                estado = 2
            insertInto += ",emd_estado"
            insertValues += ",'" + str(estado) + "'"
            updateValues += ",emd_estado = '" + str(estado) + "'"

        
        #print(insertInto + insertValues + ")")
        validar = validarUsuario(str(i[3]), empresa)
        mi_conexion = mysql.connector.connect(host='localhost', user='incapacidades', passwd='1143231494ABc_', db='incapacidades')
        con = mi_conexion.cursor()
        if validar == 0 and insertarCheck == '1':
            #print(insertInto + insertValues + ")")
            #print(str(i[0]))
            con.execute(insertInto + insertValues + ")")
        else:
            whereUpdate += str(validar)
            #print(updateValues + whereUpdate)
            #print(str(i[0]))
            con.execute(updateValues + whereUpdate)

        mi_conexion.commit()
    mi_conexion.close()


cargarArchivo('/var/www/html/BdPython/'+sys.argv[1], sys.argv[2], sys.argv[3])