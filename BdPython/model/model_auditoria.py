#from msilib import Table
#from numpy import integer
from datetime import date
from pymysql import Date
from sqlalchemy import DATE, TEXT, VARCHAR, Table,Column, text, true
from sqlalchemy.sql.sqltypes import Integer,String
from config.coneccion import engine, meta_data

auditoria = Table("gi_auditoria", meta_data,
                      Column("aud_id_i",Integer, primary_key=True),
                      Column("aud_id_usuario_i",Integer),
                      Column("aud_fecha_d",DATE),
                      Column("aud_hora_d",VARCHAR(255)),
                      Column("aud_controlador_v",VARCHAR(255)),
                      Column("aud_metodo_v",VARCHAR(255)),
                      Column("aud_ip_i",VARCHAR(255)),
                      Column("aud_navegador_v",VARCHAR(255)),
                      Column("aud_data_t",TEXT),
                      Column("aud_id_session",VARCHAR(255)),)

meta_data.create_all(engine)                      