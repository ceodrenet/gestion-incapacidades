# coding=utf-8
import xlsxwriter
import mysql.connector
from datetime import datetime
import sys


def fetch_table_data(empresa_id):
    # The connect() constructor creates a connection to the MySQL server and returns a MySQLConnection object.
    cnx = mysql.connector.connect(
        host='localhost',
        database='incapacidades',
        user='business_intel',
        password='B6Z7vwrBMobavfhX'
    )

    cursor = cnx.cursor()
    script = " SELECT " \
             "inc_id as ID, " \
             "emd_cedula AS 'IDENTIFICACIÓN', " \
             "emd_nombre AS 'NOMBRE COLABORADOR', " \
             "inc_fecha_inicio AS 'FECHA INICIAL', " \
             "inc_fecha_final AS 'FECHA FINAL', " \
             "dias AS 'DÍAS'," \
             "inc_clasificacion AS 'CLASIFICACIÓN' , " \
             "inc_diagnostico AS DIAGNOSTICO, " \
             "inc_origen AS ORIGEN , " \
             "inc_ips_afiliado AS EPS , " \
             "inc_afp_afiliado AS AFP, " \
             "inc_arl_afiliado AS ARL, " \
             "inc_estado_tramite AS 'ESTADO TRAMITE', " \
             "set_descripcion_v AS 'SUB ESTADO TRAMITE', " \
             "inc_fecha_radicacion AS 'FECHA RADICACIÓN', " \
             "inc_numero_radicado_v AS 'NUMERO RADICADO', " \
             "incm_comentario_t AS COMENTARIO, " \
             "date_format(incm_fecha_comentario,'%d/%m/%Y') AS 'FECHA COMENTARIO' " \
             "FROM gi_incapacidades_comentarios  " \
             " LEFT JOIN reporte_consolidado_inc ON inc_id = inm_inc_id_i " \
             "WHERE inc_empresa = "+ empresa_id +" " \
             " ORDER BY inc_fecha_inicio desc,incm_fecha_comentario DESC"

    cursor.execute(script)

    header = [row[0] for row in cursor.description]

    rows = cursor.fetchall()

    # Closing connection
    cnx.close()

    return header, rows


def export(idSession, empresa_id):
    # Create an new Excel file and add a worksheet.
    workbook = xlsxwriter.Workbook(
        '/var/www/html/BdPython/Exportar_Comentarios_' + empresa_id + '_' + idSession + '.xlsx')
    worksheet = workbook.add_worksheet('CONSOLIDADO')

    # Create style for cells
    header_cell_format = workbook.add_format(
        {'bg_color': '#FF0000', 'font_color': '#FFFFFF', 'bold': 1, 'border': 1, 'align': 'center',
         'valign': 'vcenter'})
    body_cell_format = workbook.add_format({'border': True})
    format2 = workbook.add_format({'num_format': 'dd/mm/yyyy', 'border': True})
    currency_format = workbook.add_format({'num_format': '$#,##0', 'border': True})

    header, rows = fetch_table_data(empresa_id)

    row_index = 0
    column_index = 0

    for column_name in header:
        worksheet.write(row_index, column_index, column_name, header_cell_format)
        column_index += 1

    row_index += 1
    for row in rows:
        column_index = 0
        for column in row:
            if column_index == 3 or column_index == 4 or column_index == 14 or column_index == 17:
                if column is None:
                    worksheet.write(row_index, column_index, " ", body_cell_format)
                else:
                    date_time_obj = datetime.strptime(column, '%d/%m/%Y')
                    worksheet.write(row_index, column_index, date_time_obj, format2)
            else:
                if column_index == 1 or column_index == 0:
                    worksheet.write(row_index, column_index, int(column), body_cell_format)
                else:
                    worksheet.write(row_index, column_index, column, body_cell_format)

            column_index += 1
        row_index += 1

    print(str(row_index) + ' rows written successfully to ' + workbook.filename)

    # Closing workbook
    workbook.close()


# Tables to be exported
export(sys.argv[1], sys.argv[2])

