#import pandas
import pandas as pd
import mysql.connector
#validar la identificacion del usuario 
def validar_usuario(emd_cedula):
    
    mi_conexion = mysql.connector.connect(host='localhost', user='root', passwd='', db='incapacidades')
    conx = mi_conexion.cursor()
    conx.execute(f"SELECT emd_cedula, emd_nombre FROM gi_empleados WHERE emd_cedula='{emd_cedula}'")
    for empleados in conx:
            return True
    
    return False

def devolver_id(ips_codigo):
    
    mi_conexion = mysql.connector.connect(host='localhost', user='root', passwd='', db='incapacidades')
    con = mi_conexion.cursor()
    con.execute(f"SELECT ips_id, ips_nombre FROM gi_ips WHERE ips_codigo='{ips_codigo}'")
    for ips in con:
        return ips[0]
    #return con.fetchone()(0)

def is_empty(a):
    return a == set()

file = r'C:\xampp\htdocs\gestion-incapacidades\BdPython\empleados.xlsx'
# Load the xlsx file
excel_data = pd.read_excel(file)
# Read the values of the file in the dataframe
data = pd.DataFrame(excel_data)
# Print the content
#print("The content of the file is:\n", data)


mi_conexion = mysql.connector.connect(host='localhost', user='root', passwd='', db='incapacidades')
con = mi_conexion.cursor()

for i in data.itertuples():
     
     if validar_usuario(i[3]) == False:
        insertInto = "insert into gi_empleados("
        insertValues = ") values ("
        if i[2] != None and is_empty(i[2]) == False :
            insertInto += "emd_tipo_identificacion,"
            insertValues += "'"+ i[2]+"',"

        if i[4] != None and is_empty(i[4]) == False :
            insertInto += "emd_nombre,"
            insertValues += "'"+ i[4]+"',"

        if i[5] != None and is_empty(i[5]) == False:
            insertInto += "emd_fecha_nacimiento,"
            insertValues += "'"+ str(i[5]) +"',"

        if i[6] != None and is_empty(i[6]) == False:
            insertInto += "emd_fecha_ingreso,"
            insertValues += "'"+ str(i[6])+"',"

        if i[7] != None and is_empty(i[7]) == False:
            insertInto += "emd_fecha_retiro,"
            insertValues += "'"+ str(i[7])+"',"    

        if i[8] != None and is_empty(i[8]) == False:
            insertInto += "emd_salario,"
            insertValues += "'"+ str(i[8])+"',"    

        if i[9] != None and is_empty(i[9]) == False:
            insertInto += "emd_salario_promedio,"
            insertValues += "'"+ str(i[9])+"'," 

        if i[10] != None and is_empty(i[10]) == False:
            insertInto += "emd_cargo,"
            insertValues += "'"+ i[10]+"'," 

        if i[11] != None and is_empty(i[11]) == False:
            insertInto += "emd_sede,"
            insertValues += "'"+ i[11]+"',"    

        if i[12] != None and is_empty(i[12]) == False:
            eps = devolver_id(i[12])
            insertInto += "emd_eps_id,"
            insertValues += "'"+ str(eps) +"'," 

        if i[13] != None and is_empty(i[13]) == False:
            insertInto += "emd_fecha_afiliacion_eps,"
            insertValues += "'"+ str(i[13])+"'," 

        if i[14] != None and is_empty(i[14]) == False:
            afp = devolver_id(i[14])
            insertInto += "emd_afp_id,"
            insertValues += "'"+ str(afp) +"',"  

        if i[15] != None and is_empty(i[15]) == False:
            arl = devolver_id(i[15])
            insertInto += "emd_arl_id,"
            insertValues += "'"+ str(arl) +"',"

        if i[16] != None and is_empty(i[16]) == False:
            insertInto += "emd_genero,"
            insertValues += "'"+ i[16]+"',"   

        if i[17] != None and is_empty(i[17]) == False:
            insertInto += "emd_tipo_empleado,"
            insertValues += "'"+ i[17]+"',"   

        if i[18] != None and is_empty(i[18]) == False:
            insertInto += "emd_codigo_nomina,"
            insertValues += "'"+ str(i[18])+"',"  

        if i[19] != None and is_empty(i[19]) == False:
            insertInto += "emd_centro_costos,"
            insertValues += "'"+ str(i[19])+"'," 

        if i[20] != None and is_empty(i[20]) == False:
            insertInto += "emd_subcentro_costos,"
            insertValues += "'"+ str(i[20])+"',"   

        if i[21] != None and is_empty(i[21]) == False:
            insertInto += "emd_estado,"
            insertValues += "'"+ str(i[21])+"'," 
             
             
        insertInto += "emd_emp_id"
        insertValues += "49"  
        

       # print(insertInto+insertValues+")")
        # arl = devolver_id(i[15])
        # afp = devolver_id(i[14])

        #print(f"insert into gi_empleados(emd_tipo_identificacion,emd_cedula,  emd_nombre, emd_fecha_nacimiento, emd_fecha_ingreso,  emd_fecha_retiro, emd_salario, emd_salario_promedio, emd_cargo, emd_sede,emd_eps_id, emd_fecha_afiliacion_eps, emd_afp_id, emd_arl_id,emd_genero, emd_tipo_empleado, emd_codigo_nomina, emd_centro_costos, emd_ciudad, emd_subcentro_costos, emd_estado,emd_emp_id) values('{i[1]}','{i[2]}','{i[3]}','{i[4]}','{i[5]}','{i[6]}','{i[7]}','{i[8]}','{i[9]}','{i[10]}','{i[11]}','{i[12]}','{i[13]}','{i[14]}','{i[15]}','{i[16]}','{i[17]}','{i[18]}','{i[19]}','{i[20]}','{i[21]}', '49')")
        
        #con.execute(f"insert into gi_empleados(emd_tipo_identificacion,emd_cedula, emd_nombre, emd_fecha_nacimiento, emd_fecha_ingreso, emd_fecha_retiro, emd_salario, emd_salario_promedio, emd_cargo, emd_sede,emd_eps_id, emd_fecha_afiliacion_eps, emd_afp_id, emd_arl_id,emd_genero, emd_tipo_empleado, emd_codigo_nomina, emd_centro_costos, emd_ciudad, emd_subcentro_costos, emd_estado,emd_emp_id) values('{i[2]}','{i[3]}','{i[4]}','{i[5]}','{i[6]}','{i[7]}','{i[8]}','{i[9]}','{i[10]}','{i[11]}','{eps}','{i[13]}','{afp}','{arl}','{i[16]}','{i[17]}','{i[18]}','{i[19]}','{i[20]}','{i[21]}','{i[22]}','49')")
        con.execute(insertInto+insertValues+")")
        mi_conexion.commit()
     else:
        print("El empleado ya existe")



mi_conexion = mysql.connector.connect(host='localhost', user='root', passwd='', db='incapacidades')
con = mi_conexion.cursor()

for i in data.itertuples():

    #  if validar_usuario(i[3]) == False:
        consulta= "UPDATE gi_empleados SET"
        where = "WHERE emd_id = 49 AND emd_cedula= '"+ i[3]+"'"
        if i[2] != None and is_empty(i[2]) == False :
            consulta += "emd_tipo_identificacion =" 
            consulta +="'"+ i[2]+"'," 
           

        if i[4] != None and is_empty(i[4]) == False :
            consulta += "emd_nombre = "
            consulta += "'"+ i[4]+"',"

        if i[5] != None and is_empty(i[5]) == False:
             consulta += "emd_fecha_nacimiento ="
             consulta += "'"+ (i[5]) +"',"

        if i[6] != None and is_empty(i[6]) == False:
             consulta += "emd_fecha_ingreso ="
             consulta += "'"+ (i[6])+"',"

        if i[7] != None and is_empty(i[7]) == False:
             consulta += "emd_fecha_retiro ="
             consulta += "'"+ (i[7])+"',"    

        if i[8] != None and is_empty(i[8]) == False:
             consulta += "emd_salario ="
             consulta += "'"+ (i[8])+"',"    

        if i[9] != None and is_empty(i[9]) == False:
             consulta += "emd_salario_promedio ="
             consulta += "'"+ (i[9])+"'," 

        if i[10] != None and is_empty(i[10]) == False:
             consulta += "emd_cargo ="
             consulta += "'"+ i[10]+"'," 

        if i[11] != None and is_empty(i[11]) == False:
             consulta += "emd_sede ="
             consulta += "'"+ i[11]+"',"    

        if i[12] != None and is_empty(i[12]) == False:
             eps = devolver_id(i[12])
             consulta += "emd_eps_id ="
             consulta += "'"+ (eps) +"'," 

        if i[13] != None and is_empty(i[13]) == False:
             consulta += "emd_fecha_afiliacion_eps ="
             consulta += "'"+ (i[13])+"'," 

        if i[14] != None and is_empty(i[14]) == False:
             afp = devolver_id(i[14])
             consulta += "emd_afp_id ="
             consulta += "'"+ (afp) +"',"  

        if i[15] != None and is_empty(i[15]) == False:
             arl = devolver_id(i[15])
             consulta += "emd_arl_id ="
             consulta += "'"+ (arl) +"',"

        if i[16] != None and is_empty(i[16]) == False:
             consulta += "emd_genero ="
             consulta += "'"+ i[16]+"',"   

        if i[17] != None and is_empty(i[17]) == False:
             consulta += "emd_tipo_empleado ="
             consulta += "'"+ i[17]+"',"   

        if i[18] != None and is_empty(i[18]) == False:
             consulta += "emd_codigo_nomina ="
             consulta += "'"+ (i[18])+"',"  

        if i[19] != None and is_empty(i[19]) == False:
             consulta += "emd_centro_costos ="
             consulta += "'"+ (i[19])+"'," 

        if i[20] != None and is_empty(i[20]) == False:
             consulta += "emd_subcentro_costos ="
             consulta += "'"+ (i[20])+"',"   

        if i[21] != None and is_empty(i[21]) == False:
             consulta += "emd_estado ="
             consulta += "'"+ (i[21])+"'," 
             
             
        # insertInto += "emd_emp_id"
        # insertValues += "49"  

       #consulta= "UPDATE gi_empleados SET  emd_nombre = emd_nombre, emd_cedula = emd_cedula, emd_emp_id = emd_emp_id, emd_fecha_ingreso = emd_fecha_ingreso, emd_salario = emd_salario, emd_tipo_identificacion = emd_tipo_identificacion, emd_fecha_retiro = emd_fecha_retiro, emd_salario_promedio = emd_salario_promedio, emd_fecha_nacimiento = emd_fecha_nacimiento, emd_eps_id = emd_eps_id, emd_fecha_afiliacion_eps = emd_fecha_afiliacion_eps, emd_afp_id = emd_afp_id, emd_arl_id = emd_arl_id, emd_fecha_calificacion_PCL = emd_fecha_calificacion_PCL, emd_entidad_calificadora = emd_entidad_calificadora, emd_diagnostico = emd_diagnostico, emd_cargo = emd_cargo, emd_sede = emd_sede , emd_ruta_cedula = emd_ruta_cedula, emd_ruta_otro = emd_ruta_otro, emd_genero = emd_genero, emd_tipo_empleado = emd_tipo_empleado, emd_codigo_nomina = emd_codigo_nomina, emd_centro_costos = emd_centro_costos, emd_ciudad = emd_ciudad, emd_subcentro_costos = emd_subcentro_costos, emd_estado = emd_estado, emd_telefono_v = emd_telefono_v, emd_correo_v = emd_correo_v, emd_caso_especial_i = emd_caso_especial_i,emd_susp_salario_i = emd_susp_salario_i,emd_fech_susp_salario = :emd_fech_susp_salario WHERE emd_id = emd_id and emd_cedula=emd_cedula"
        con.execute(consulta+where)
        mi_conexion.commit()




